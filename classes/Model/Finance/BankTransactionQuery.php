<?php

namespace Model\Finance;

use Model\Finance\Base\BankTransactionQuery as BaseBankTransactionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'bank_transaction' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BankTransactionQuery extends BaseBankTransactionQuery
{

}
