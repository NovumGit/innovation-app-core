<?php

namespace Model\Finance;

use Core\QueryMapper;
use Core\Utils;
use Model\Finance\Base\BankIbanAddressBook as BaseBankIbanAddressBook;

/**
 * Skeleton subclass for representing a row from the 'bank_iban_address_book' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BankIbanAddressBook extends BaseBankIbanAddressBook
{

    function getTotals()
    {
        $fPrice = 0;
        if($this->getId())
        {
            $sQuery = "SELECT SUM(amount)
                        FROM bank_transaction
                        WHERE counter_bank_iban_id = {$this->getId()}";

            $fPrice = QueryMapper::fetchVal($sQuery);
        }
        return $fPrice;
    }
}
