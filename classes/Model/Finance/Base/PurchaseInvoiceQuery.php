<?php

namespace Model\Finance\Base;

use \Exception;
use \PDO;
use Model\Finance\PurchaseInvoice as ChildPurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery as ChildPurchaseInvoiceQuery;
use Model\Finance\Map\PurchaseInvoiceTableMap;
use Model\Supplier\Supplier;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'purchase_invoice' table.
 *
 *
 *
 * @method     ChildPurchaseInvoiceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPurchaseInvoiceQuery orderByRemoteInvoiceId($order = Criteria::ASC) Order by the remote_invoice_id column
 * @method     ChildPurchaseInvoiceQuery orderBySupplierId($order = Criteria::ASC) Order by the supplier_id column
 * @method     ChildPurchaseInvoiceQuery orderByInvoiceDate($order = Criteria::ASC) Order by the invoice_date column
 * @method     ChildPurchaseInvoiceQuery orderByDueDate($order = Criteria::ASC) Order by the due_date column
 * @method     ChildPurchaseInvoiceQuery orderByPayDate($order = Criteria::ASC) Order by the pay_date column
 * @method     ChildPurchaseInvoiceQuery orderByIsPaid($order = Criteria::ASC) Order by the is_paid column
 * @method     ChildPurchaseInvoiceQuery orderByTotalPayableAmount($order = Criteria::ASC) Order by the total_payable_amount column
 * @method     ChildPurchaseInvoiceQuery orderByHasDocument($order = Criteria::ASC) Order by the has_document column
 * @method     ChildPurchaseInvoiceQuery orderByIsBooked($order = Criteria::ASC) Order by the is_booked column
 * @method     ChildPurchaseInvoiceQuery orderByNeedsManualBooking($order = Criteria::ASC) Order by the needs_manual_booking column
 * @method     ChildPurchaseInvoiceQuery orderByFilePath($order = Criteria::ASC) Order by the file_path column
 * @method     ChildPurchaseInvoiceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPurchaseInvoiceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPurchaseInvoiceQuery groupById() Group by the id column
 * @method     ChildPurchaseInvoiceQuery groupByRemoteInvoiceId() Group by the remote_invoice_id column
 * @method     ChildPurchaseInvoiceQuery groupBySupplierId() Group by the supplier_id column
 * @method     ChildPurchaseInvoiceQuery groupByInvoiceDate() Group by the invoice_date column
 * @method     ChildPurchaseInvoiceQuery groupByDueDate() Group by the due_date column
 * @method     ChildPurchaseInvoiceQuery groupByPayDate() Group by the pay_date column
 * @method     ChildPurchaseInvoiceQuery groupByIsPaid() Group by the is_paid column
 * @method     ChildPurchaseInvoiceQuery groupByTotalPayableAmount() Group by the total_payable_amount column
 * @method     ChildPurchaseInvoiceQuery groupByHasDocument() Group by the has_document column
 * @method     ChildPurchaseInvoiceQuery groupByIsBooked() Group by the is_booked column
 * @method     ChildPurchaseInvoiceQuery groupByNeedsManualBooking() Group by the needs_manual_booking column
 * @method     ChildPurchaseInvoiceQuery groupByFilePath() Group by the file_path column
 * @method     ChildPurchaseInvoiceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPurchaseInvoiceQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPurchaseInvoiceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPurchaseInvoiceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPurchaseInvoiceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPurchaseInvoiceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPurchaseInvoiceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPurchaseInvoiceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPurchaseInvoiceQuery leftJoinSupplier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Supplier relation
 * @method     ChildPurchaseInvoiceQuery rightJoinSupplier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Supplier relation
 * @method     ChildPurchaseInvoiceQuery innerJoinSupplier($relationAlias = null) Adds a INNER JOIN clause to the query using the Supplier relation
 *
 * @method     ChildPurchaseInvoiceQuery joinWithSupplier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Supplier relation
 *
 * @method     ChildPurchaseInvoiceQuery leftJoinWithSupplier() Adds a LEFT JOIN clause and with to the query using the Supplier relation
 * @method     ChildPurchaseInvoiceQuery rightJoinWithSupplier() Adds a RIGHT JOIN clause and with to the query using the Supplier relation
 * @method     ChildPurchaseInvoiceQuery innerJoinWithSupplier() Adds a INNER JOIN clause and with to the query using the Supplier relation
 *
 * @method     \Model\Supplier\SupplierQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPurchaseInvoice findOne(ConnectionInterface $con = null) Return the first ChildPurchaseInvoice matching the query
 * @method     ChildPurchaseInvoice findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPurchaseInvoice matching the query, or a new ChildPurchaseInvoice object populated from the query conditions when no match is found
 *
 * @method     ChildPurchaseInvoice findOneById(int $id) Return the first ChildPurchaseInvoice filtered by the id column
 * @method     ChildPurchaseInvoice findOneByRemoteInvoiceId(string $remote_invoice_id) Return the first ChildPurchaseInvoice filtered by the remote_invoice_id column
 * @method     ChildPurchaseInvoice findOneBySupplierId(int $supplier_id) Return the first ChildPurchaseInvoice filtered by the supplier_id column
 * @method     ChildPurchaseInvoice findOneByInvoiceDate(string $invoice_date) Return the first ChildPurchaseInvoice filtered by the invoice_date column
 * @method     ChildPurchaseInvoice findOneByDueDate(string $due_date) Return the first ChildPurchaseInvoice filtered by the due_date column
 * @method     ChildPurchaseInvoice findOneByPayDate(string $pay_date) Return the first ChildPurchaseInvoice filtered by the pay_date column
 * @method     ChildPurchaseInvoice findOneByIsPaid(boolean $is_paid) Return the first ChildPurchaseInvoice filtered by the is_paid column
 * @method     ChildPurchaseInvoice findOneByTotalPayableAmount(string $total_payable_amount) Return the first ChildPurchaseInvoice filtered by the total_payable_amount column
 * @method     ChildPurchaseInvoice findOneByHasDocument(boolean $has_document) Return the first ChildPurchaseInvoice filtered by the has_document column
 * @method     ChildPurchaseInvoice findOneByIsBooked(boolean $is_booked) Return the first ChildPurchaseInvoice filtered by the is_booked column
 * @method     ChildPurchaseInvoice findOneByNeedsManualBooking(boolean $needs_manual_booking) Return the first ChildPurchaseInvoice filtered by the needs_manual_booking column
 * @method     ChildPurchaseInvoice findOneByFilePath(string $file_path) Return the first ChildPurchaseInvoice filtered by the file_path column
 * @method     ChildPurchaseInvoice findOneByCreatedAt(string $created_at) Return the first ChildPurchaseInvoice filtered by the created_at column
 * @method     ChildPurchaseInvoice findOneByUpdatedAt(string $updated_at) Return the first ChildPurchaseInvoice filtered by the updated_at column *

 * @method     ChildPurchaseInvoice requirePk($key, ConnectionInterface $con = null) Return the ChildPurchaseInvoice by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOne(ConnectionInterface $con = null) Return the first ChildPurchaseInvoice matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPurchaseInvoice requireOneById(int $id) Return the first ChildPurchaseInvoice filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByRemoteInvoiceId(string $remote_invoice_id) Return the first ChildPurchaseInvoice filtered by the remote_invoice_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneBySupplierId(int $supplier_id) Return the first ChildPurchaseInvoice filtered by the supplier_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByInvoiceDate(string $invoice_date) Return the first ChildPurchaseInvoice filtered by the invoice_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByDueDate(string $due_date) Return the first ChildPurchaseInvoice filtered by the due_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByPayDate(string $pay_date) Return the first ChildPurchaseInvoice filtered by the pay_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByIsPaid(boolean $is_paid) Return the first ChildPurchaseInvoice filtered by the is_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByTotalPayableAmount(string $total_payable_amount) Return the first ChildPurchaseInvoice filtered by the total_payable_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByHasDocument(boolean $has_document) Return the first ChildPurchaseInvoice filtered by the has_document column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByIsBooked(boolean $is_booked) Return the first ChildPurchaseInvoice filtered by the is_booked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByNeedsManualBooking(boolean $needs_manual_booking) Return the first ChildPurchaseInvoice filtered by the needs_manual_booking column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByFilePath(string $file_path) Return the first ChildPurchaseInvoice filtered by the file_path column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByCreatedAt(string $created_at) Return the first ChildPurchaseInvoice filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPurchaseInvoice requireOneByUpdatedAt(string $updated_at) Return the first ChildPurchaseInvoice filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPurchaseInvoice[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPurchaseInvoice objects based on current ModelCriteria
 * @method     ChildPurchaseInvoice[]|ObjectCollection findById(int $id) Return ChildPurchaseInvoice objects filtered by the id column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByRemoteInvoiceId(string $remote_invoice_id) Return ChildPurchaseInvoice objects filtered by the remote_invoice_id column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findBySupplierId(int $supplier_id) Return ChildPurchaseInvoice objects filtered by the supplier_id column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByInvoiceDate(string $invoice_date) Return ChildPurchaseInvoice objects filtered by the invoice_date column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByDueDate(string $due_date) Return ChildPurchaseInvoice objects filtered by the due_date column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByPayDate(string $pay_date) Return ChildPurchaseInvoice objects filtered by the pay_date column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByIsPaid(boolean $is_paid) Return ChildPurchaseInvoice objects filtered by the is_paid column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByTotalPayableAmount(string $total_payable_amount) Return ChildPurchaseInvoice objects filtered by the total_payable_amount column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByHasDocument(boolean $has_document) Return ChildPurchaseInvoice objects filtered by the has_document column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByIsBooked(boolean $is_booked) Return ChildPurchaseInvoice objects filtered by the is_booked column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByNeedsManualBooking(boolean $needs_manual_booking) Return ChildPurchaseInvoice objects filtered by the needs_manual_booking column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByFilePath(string $file_path) Return ChildPurchaseInvoice objects filtered by the file_path column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPurchaseInvoice objects filtered by the created_at column
 * @method     ChildPurchaseInvoice[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPurchaseInvoice objects filtered by the updated_at column
 * @method     ChildPurchaseInvoice[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PurchaseInvoiceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Finance\Base\PurchaseInvoiceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Finance\\PurchaseInvoice', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPurchaseInvoiceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPurchaseInvoiceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPurchaseInvoiceQuery) {
            return $criteria;
        }
        $query = new ChildPurchaseInvoiceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPurchaseInvoice|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PurchaseInvoiceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PurchaseInvoiceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPurchaseInvoice A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, remote_invoice_id, supplier_id, invoice_date, due_date, pay_date, is_paid, total_payable_amount, has_document, is_booked, needs_manual_booking, file_path, created_at, updated_at FROM purchase_invoice WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPurchaseInvoice $obj */
            $obj = new ChildPurchaseInvoice();
            $obj->hydrate($row);
            PurchaseInvoiceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPurchaseInvoice|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the remote_invoice_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteInvoiceId('fooValue');   // WHERE remote_invoice_id = 'fooValue'
     * $query->filterByRemoteInvoiceId('%fooValue%', Criteria::LIKE); // WHERE remote_invoice_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteInvoiceId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByRemoteInvoiceId($remoteInvoiceId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteInvoiceId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_REMOTE_INVOICE_ID, $remoteInvoiceId, $comparison);
    }

    /**
     * Filter the query on the supplier_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySupplierId(1234); // WHERE supplier_id = 1234
     * $query->filterBySupplierId(array(12, 34)); // WHERE supplier_id IN (12, 34)
     * $query->filterBySupplierId(array('min' => 12)); // WHERE supplier_id > 12
     * </code>
     *
     * @see       filterBySupplier()
     *
     * @param     mixed $supplierId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterBySupplierId($supplierId = null, $comparison = null)
    {
        if (is_array($supplierId)) {
            $useMinMax = false;
            if (isset($supplierId['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_SUPPLIER_ID, $supplierId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($supplierId['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_SUPPLIER_ID, $supplierId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_SUPPLIER_ID, $supplierId, $comparison);
    }

    /**
     * Filter the query on the invoice_date column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceDate('2011-03-14'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate('now'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate(array('max' => 'yesterday')); // WHERE invoice_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $invoiceDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByInvoiceDate($invoiceDate = null, $comparison = null)
    {
        if (is_array($invoiceDate)) {
            $useMinMax = false;
            if (isset($invoiceDate['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceDate['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate, $comparison);
    }

    /**
     * Filter the query on the due_date column
     *
     * Example usage:
     * <code>
     * $query->filterByDueDate('2011-03-14'); // WHERE due_date = '2011-03-14'
     * $query->filterByDueDate('now'); // WHERE due_date = '2011-03-14'
     * $query->filterByDueDate(array('max' => 'yesterday')); // WHERE due_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $dueDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByDueDate($dueDate = null, $comparison = null)
    {
        if (is_array($dueDate)) {
            $useMinMax = false;
            if (isset($dueDate['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_DUE_DATE, $dueDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dueDate['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_DUE_DATE, $dueDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_DUE_DATE, $dueDate, $comparison);
    }

    /**
     * Filter the query on the pay_date column
     *
     * Example usage:
     * <code>
     * $query->filterByPayDate('2011-03-14'); // WHERE pay_date = '2011-03-14'
     * $query->filterByPayDate('now'); // WHERE pay_date = '2011-03-14'
     * $query->filterByPayDate(array('max' => 'yesterday')); // WHERE pay_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $payDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByPayDate($payDate = null, $comparison = null)
    {
        if (is_array($payDate)) {
            $useMinMax = false;
            if (isset($payDate['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_PAY_DATE, $payDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($payDate['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_PAY_DATE, $payDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_PAY_DATE, $payDate, $comparison);
    }

    /**
     * Filter the query on the is_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPaid(true); // WHERE is_paid = true
     * $query->filterByIsPaid('yes'); // WHERE is_paid = true
     * </code>
     *
     * @param     boolean|string $isPaid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByIsPaid($isPaid = null, $comparison = null)
    {
        if (is_string($isPaid)) {
            $isPaid = in_array(strtolower($isPaid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_IS_PAID, $isPaid, $comparison);
    }

    /**
     * Filter the query on the total_payable_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalPayableAmount(1234); // WHERE total_payable_amount = 1234
     * $query->filterByTotalPayableAmount(array(12, 34)); // WHERE total_payable_amount IN (12, 34)
     * $query->filterByTotalPayableAmount(array('min' => 12)); // WHERE total_payable_amount > 12
     * </code>
     *
     * @param     mixed $totalPayableAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByTotalPayableAmount($totalPayableAmount = null, $comparison = null)
    {
        if (is_array($totalPayableAmount)) {
            $useMinMax = false;
            if (isset($totalPayableAmount['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalPayableAmount['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount, $comparison);
    }

    /**
     * Filter the query on the has_document column
     *
     * Example usage:
     * <code>
     * $query->filterByHasDocument(true); // WHERE has_document = true
     * $query->filterByHasDocument('yes'); // WHERE has_document = true
     * </code>
     *
     * @param     boolean|string $hasDocument The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByHasDocument($hasDocument = null, $comparison = null)
    {
        if (is_string($hasDocument)) {
            $hasDocument = in_array(strtolower($hasDocument), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_HAS_DOCUMENT, $hasDocument, $comparison);
    }

    /**
     * Filter the query on the is_booked column
     *
     * Example usage:
     * <code>
     * $query->filterByIsBooked(true); // WHERE is_booked = true
     * $query->filterByIsBooked('yes'); // WHERE is_booked = true
     * </code>
     *
     * @param     boolean|string $isBooked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByIsBooked($isBooked = null, $comparison = null)
    {
        if (is_string($isBooked)) {
            $isBooked = in_array(strtolower($isBooked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_IS_BOOKED, $isBooked, $comparison);
    }

    /**
     * Filter the query on the needs_manual_booking column
     *
     * Example usage:
     * <code>
     * $query->filterByNeedsManualBooking(true); // WHERE needs_manual_booking = true
     * $query->filterByNeedsManualBooking('yes'); // WHERE needs_manual_booking = true
     * </code>
     *
     * @param     boolean|string $needsManualBooking The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByNeedsManualBooking($needsManualBooking = null, $comparison = null)
    {
        if (is_string($needsManualBooking)) {
            $needsManualBooking = in_array(strtolower($needsManualBooking), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_NEEDS_MANUAL_BOOKING, $needsManualBooking, $comparison);
    }

    /**
     * Filter the query on the file_path column
     *
     * Example usage:
     * <code>
     * $query->filterByFilePath('fooValue');   // WHERE file_path = 'fooValue'
     * $query->filterByFilePath('%fooValue%', Criteria::LIKE); // WHERE file_path LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filePath The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByFilePath($filePath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filePath)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_FILE_PATH, $filePath, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PurchaseInvoiceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Supplier\Supplier object
     *
     * @param \Model\Supplier\Supplier|ObjectCollection $supplier The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function filterBySupplier($supplier, $comparison = null)
    {
        if ($supplier instanceof \Model\Supplier\Supplier) {
            return $this
                ->addUsingAlias(PurchaseInvoiceTableMap::COL_SUPPLIER_ID, $supplier->getId(), $comparison);
        } elseif ($supplier instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PurchaseInvoiceTableMap::COL_SUPPLIER_ID, $supplier->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySupplier() only accepts arguments of type \Model\Supplier\Supplier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Supplier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function joinSupplier($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Supplier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Supplier');
        }

        return $this;
    }

    /**
     * Use the Supplier relation Supplier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Supplier\SupplierQuery A secondary query class using the current class as primary query
     */
    public function useSupplierQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSupplier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Supplier', '\Model\Supplier\SupplierQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPurchaseInvoice $purchaseInvoice Object to remove from the list of results
     *
     * @return $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function prune($purchaseInvoice = null)
    {
        if ($purchaseInvoice) {
            $this->addUsingAlias(PurchaseInvoiceTableMap::COL_ID, $purchaseInvoice->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the purchase_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PurchaseInvoiceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PurchaseInvoiceTableMap::clearInstancePool();
            PurchaseInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PurchaseInvoiceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PurchaseInvoiceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PurchaseInvoiceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PurchaseInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PurchaseInvoiceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PurchaseInvoiceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PurchaseInvoiceTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PurchaseInvoiceTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPurchaseInvoiceQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PurchaseInvoiceTableMap::COL_CREATED_AT);
    }

} // PurchaseInvoiceQuery
