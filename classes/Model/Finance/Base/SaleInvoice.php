<?php

namespace Model\Finance\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Finance\SaleInvoice as ChildSaleInvoice;
use Model\Finance\SaleInvoiceQuery as ChildSaleInvoiceQuery;
use Model\Finance\Map\SaleInvoiceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'sale_invoice' table.
 *
 *
 *
 * @package    propel.generator.Model.Finance.Base
 */
abstract class SaleInvoice implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Finance\\Map\\SaleInvoiceTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the invoice_number field.
     *
     * @var        string|null
     */
    protected $invoice_number;

    /**
     * The value for the external_id field.
     *
     * @var        string|null
     */
    protected $external_id;

    /**
     * The value for the external_source field.
     *
     * @var        string|null
     */
    protected $external_source;

    /**
     * The value for the customer_id field.
     *
     * @var        int|null
     */
    protected $customer_id;

    /**
     * The value for the invoice_date field.
     *
     * @var        DateTime|null
     */
    protected $invoice_date;

    /**
     * The value for the revenue_date field.
     *
     * @var        DateTime|null
     */
    protected $revenue_date;

    /**
     * The value for the vat_date field.
     *
     * @var        DateTime|null
     */
    protected $vat_date;

    /**
     * The value for the due_date field.
     *
     * @var        DateTime|null
     */
    protected $due_date;

    /**
     * The value for the is_paid field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_paid;

    /**
     * The value for the has_document field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $has_document;

    /**
     * The value for the document_ext field.
     *
     * @var        string|null
     */
    protected $document_ext;

    /**
     * The value for the total_payable_amount field.
     *
     * @var        string|null
     */
    protected $total_payable_amount;

    /**
     * The value for the outstanding_amount field.
     *
     * @var        string|null
     */
    protected $outstanding_amount;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        Customer
     */
    protected $aCustomer;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_paid = false;
        $this->has_document = false;
    }

    /**
     * Initializes internal state of Model\Finance\Base\SaleInvoice object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SaleInvoice</code> instance.  If
     * <code>obj</code> is an instance of <code>SaleInvoice</code>, delegates to
     * <code>equals(SaleInvoice)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [invoice_number] column value.
     *
     * @return string|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * Get the [external_id] column value.
     *
     * @return string|null
     */
    public function getExternalId()
    {
        return $this->external_id;
    }

    /**
     * Get the [external_source] column value.
     *
     * @return string|null
     */
    public function getExternalSource()
    {
        return $this->external_source;
    }

    /**
     * Get the [customer_id] column value.
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Get the [optionally formatted] temporal [invoice_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getInvoiceDate($format = null)
    {
        if ($format === null) {
            return $this->invoice_date;
        } else {
            return $this->invoice_date instanceof \DateTimeInterface ? $this->invoice_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [revenue_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRevenueDate($format = null)
    {
        if ($format === null) {
            return $this->revenue_date;
        } else {
            return $this->revenue_date instanceof \DateTimeInterface ? $this->revenue_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [vat_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getVatDate($format = null)
    {
        if ($format === null) {
            return $this->vat_date;
        } else {
            return $this->vat_date instanceof \DateTimeInterface ? $this->vat_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [due_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDueDate($format = null)
    {
        if ($format === null) {
            return $this->due_date;
        } else {
            return $this->due_date instanceof \DateTimeInterface ? $this->due_date->format($format) : null;
        }
    }

    /**
     * Get the [is_paid] column value.
     *
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->is_paid;
    }

    /**
     * Get the [is_paid] column value.
     *
     * @return boolean
     */
    public function isPaid()
    {
        return $this->getIsPaid();
    }

    /**
     * Get the [has_document] column value.
     *
     * @return boolean
     */
    public function getHasDocument()
    {
        return $this->has_document;
    }

    /**
     * Get the [has_document] column value.
     *
     * @return boolean
     */
    public function hasDocument()
    {
        return $this->getHasDocument();
    }

    /**
     * Get the [document_ext] column value.
     *
     * @return string|null
     */
    public function getDocumentExt()
    {
        return $this->document_ext;
    }

    /**
     * Get the [total_payable_amount] column value.
     *
     * @return string|null
     */
    public function getTotalPayableAmount()
    {
        return $this->total_payable_amount;
    }

    /**
     * Get the [outstanding_amount] column value.
     *
     * @return string|null
     */
    public function getOutstandingAmount()
    {
        return $this->outstanding_amount;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [invoice_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setInvoiceNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_number !== $v) {
            $this->invoice_number = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_INVOICE_NUMBER] = true;
        }

        return $this;
    } // setInvoiceNumber()

    /**
     * Set the value of [external_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setExternalId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->external_id !== $v) {
            $this->external_id = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_EXTERNAL_ID] = true;
        }

        return $this;
    } // setExternalId()

    /**
     * Set the value of [external_source] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setExternalSource($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->external_source !== $v) {
            $this->external_source = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_EXTERNAL_SOURCE] = true;
        }

        return $this;
    } // setExternalSource()

    /**
     * Set the value of [customer_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setCustomerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_id !== $v) {
            $this->customer_id = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_CUSTOMER_ID] = true;
        }

        if ($this->aCustomer !== null && $this->aCustomer->getId() !== $v) {
            $this->aCustomer = null;
        }

        return $this;
    } // setCustomerId()

    /**
     * Sets the value of [invoice_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setInvoiceDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->invoice_date !== null || $dt !== null) {
            if ($this->invoice_date === null || $dt === null || $dt->format("Y-m-d") !== $this->invoice_date->format("Y-m-d")) {
                $this->invoice_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_INVOICE_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setInvoiceDate()

    /**
     * Sets the value of [revenue_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setRevenueDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->revenue_date !== null || $dt !== null) {
            if ($this->revenue_date === null || $dt === null || $dt->format("Y-m-d") !== $this->revenue_date->format("Y-m-d")) {
                $this->revenue_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_REVENUE_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setRevenueDate()

    /**
     * Sets the value of [vat_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setVatDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vat_date !== null || $dt !== null) {
            if ($this->vat_date === null || $dt === null || $dt->format("Y-m-d") !== $this->vat_date->format("Y-m-d")) {
                $this->vat_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_VAT_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setVatDate()

    /**
     * Sets the value of [due_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setDueDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->due_date !== null || $dt !== null) {
            if ($this->due_date === null || $dt === null || $dt->format("Y-m-d") !== $this->due_date->format("Y-m-d")) {
                $this->due_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_DUE_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setDueDate()

    /**
     * Sets the value of the [is_paid] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setIsPaid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_paid !== $v) {
            $this->is_paid = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_IS_PAID] = true;
        }

        return $this;
    } // setIsPaid()

    /**
     * Sets the value of the [has_document] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setHasDocument($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_document !== $v) {
            $this->has_document = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_HAS_DOCUMENT] = true;
        }

        return $this;
    } // setHasDocument()

    /**
     * Set the value of [document_ext] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setDocumentExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->document_ext !== $v) {
            $this->document_ext = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_DOCUMENT_EXT] = true;
        }

        return $this;
    } // setDocumentExt()

    /**
     * Set the value of [total_payable_amount] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setTotalPayableAmount($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->total_payable_amount !== $v) {
            $this->total_payable_amount = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT] = true;
        }

        return $this;
    } // setTotalPayableAmount()

    /**
     * Set the value of [outstanding_amount] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setOutstandingAmount($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->outstanding_amount !== $v) {
            $this->outstanding_amount = $v;
            $this->modifiedColumns[SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT] = true;
        }

        return $this;
    } // setOutstandingAmount()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleInvoiceTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_paid !== false) {
                return false;
            }

            if ($this->has_document !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SaleInvoiceTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SaleInvoiceTableMap::translateFieldName('InvoiceNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SaleInvoiceTableMap::translateFieldName('ExternalId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->external_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SaleInvoiceTableMap::translateFieldName('ExternalSource', TableMap::TYPE_PHPNAME, $indexType)];
            $this->external_source = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SaleInvoiceTableMap::translateFieldName('CustomerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SaleInvoiceTableMap::translateFieldName('InvoiceDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->invoice_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SaleInvoiceTableMap::translateFieldName('RevenueDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->revenue_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SaleInvoiceTableMap::translateFieldName('VatDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->vat_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SaleInvoiceTableMap::translateFieldName('DueDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->due_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SaleInvoiceTableMap::translateFieldName('IsPaid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_paid = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SaleInvoiceTableMap::translateFieldName('HasDocument', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_document = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SaleInvoiceTableMap::translateFieldName('DocumentExt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->document_ext = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SaleInvoiceTableMap::translateFieldName('TotalPayableAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total_payable_amount = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SaleInvoiceTableMap::translateFieldName('OutstandingAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->outstanding_amount = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SaleInvoiceTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SaleInvoiceTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = SaleInvoiceTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Finance\\SaleInvoice'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCustomer !== null && $this->customer_id !== $this->aCustomer->getId()) {
            $this->aCustomer = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSaleInvoiceQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCustomer = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SaleInvoice::setDeleted()
     * @see SaleInvoice::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSaleInvoiceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(SaleInvoiceTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(SaleInvoiceTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(SaleInvoiceTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SaleInvoiceTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCustomer !== null) {
                if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
                    $affectedRows += $this->aCustomer->save($con);
                }
                $this->setCustomer($this->aCustomer);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SaleInvoiceTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SaleInvoiceTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_INVOICE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_number';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_EXTERNAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'external_id';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_EXTERNAL_SOURCE)) {
            $modifiedColumns[':p' . $index++]  = 'external_source';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_CUSTOMER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_id';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_INVOICE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_date';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_REVENUE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'revenue_date';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_VAT_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'vat_date';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_DUE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'due_date';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_IS_PAID)) {
            $modifiedColumns[':p' . $index++]  = 'is_paid';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_HAS_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = 'has_document';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_DOCUMENT_EXT)) {
            $modifiedColumns[':p' . $index++]  = 'document_ext';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'total_payable_amount';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'outstanding_amount';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO sale_invoice (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'invoice_number':
                        $stmt->bindValue($identifier, $this->invoice_number, PDO::PARAM_STR);
                        break;
                    case 'external_id':
                        $stmt->bindValue($identifier, $this->external_id, PDO::PARAM_STR);
                        break;
                    case 'external_source':
                        $stmt->bindValue($identifier, $this->external_source, PDO::PARAM_STR);
                        break;
                    case 'customer_id':
                        $stmt->bindValue($identifier, $this->customer_id, PDO::PARAM_INT);
                        break;
                    case 'invoice_date':
                        $stmt->bindValue($identifier, $this->invoice_date ? $this->invoice_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'revenue_date':
                        $stmt->bindValue($identifier, $this->revenue_date ? $this->revenue_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'vat_date':
                        $stmt->bindValue($identifier, $this->vat_date ? $this->vat_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'due_date':
                        $stmt->bindValue($identifier, $this->due_date ? $this->due_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_paid':
                        $stmt->bindValue($identifier, (int) $this->is_paid, PDO::PARAM_INT);
                        break;
                    case 'has_document':
                        $stmt->bindValue($identifier, (int) $this->has_document, PDO::PARAM_INT);
                        break;
                    case 'document_ext':
                        $stmt->bindValue($identifier, $this->document_ext, PDO::PARAM_STR);
                        break;
                    case 'total_payable_amount':
                        $stmt->bindValue($identifier, $this->total_payable_amount, PDO::PARAM_STR);
                        break;
                    case 'outstanding_amount':
                        $stmt->bindValue($identifier, $this->outstanding_amount, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleInvoiceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getInvoiceNumber();
                break;
            case 2:
                return $this->getExternalId();
                break;
            case 3:
                return $this->getExternalSource();
                break;
            case 4:
                return $this->getCustomerId();
                break;
            case 5:
                return $this->getInvoiceDate();
                break;
            case 6:
                return $this->getRevenueDate();
                break;
            case 7:
                return $this->getVatDate();
                break;
            case 8:
                return $this->getDueDate();
                break;
            case 9:
                return $this->getIsPaid();
                break;
            case 10:
                return $this->getHasDocument();
                break;
            case 11:
                return $this->getDocumentExt();
                break;
            case 12:
                return $this->getTotalPayableAmount();
                break;
            case 13:
                return $this->getOutstandingAmount();
                break;
            case 14:
                return $this->getCreatedAt();
                break;
            case 15:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SaleInvoice'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SaleInvoice'][$this->hashCode()] = true;
        $keys = SaleInvoiceTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getInvoiceNumber(),
            $keys[2] => $this->getExternalId(),
            $keys[3] => $this->getExternalSource(),
            $keys[4] => $this->getCustomerId(),
            $keys[5] => $this->getInvoiceDate(),
            $keys[6] => $this->getRevenueDate(),
            $keys[7] => $this->getVatDate(),
            $keys[8] => $this->getDueDate(),
            $keys[9] => $this->getIsPaid(),
            $keys[10] => $this->getHasDocument(),
            $keys[11] => $this->getDocumentExt(),
            $keys[12] => $this->getTotalPayableAmount(),
            $keys[13] => $this->getOutstandingAmount(),
            $keys[14] => $this->getCreatedAt(),
            $keys[15] => $this->getUpdatedAt(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTimeInterface) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCustomer) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customer';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer';
                        break;
                    default:
                        $key = 'Customer';
                }

                $result[$key] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Finance\SaleInvoice
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleInvoiceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Finance\SaleInvoice
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setInvoiceNumber($value);
                break;
            case 2:
                $this->setExternalId($value);
                break;
            case 3:
                $this->setExternalSource($value);
                break;
            case 4:
                $this->setCustomerId($value);
                break;
            case 5:
                $this->setInvoiceDate($value);
                break;
            case 6:
                $this->setRevenueDate($value);
                break;
            case 7:
                $this->setVatDate($value);
                break;
            case 8:
                $this->setDueDate($value);
                break;
            case 9:
                $this->setIsPaid($value);
                break;
            case 10:
                $this->setHasDocument($value);
                break;
            case 11:
                $this->setDocumentExt($value);
                break;
            case 12:
                $this->setTotalPayableAmount($value);
                break;
            case 13:
                $this->setOutstandingAmount($value);
                break;
            case 14:
                $this->setCreatedAt($value);
                break;
            case 15:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SaleInvoiceTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setInvoiceNumber($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setExternalId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setExternalSource($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCustomerId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setInvoiceDate($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setRevenueDate($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setVatDate($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDueDate($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsPaid($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setHasDocument($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDocumentExt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTotalPayableAmount($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setOutstandingAmount($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUpdatedAt($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Finance\SaleInvoice The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SaleInvoiceTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SaleInvoiceTableMap::COL_ID)) {
            $criteria->add(SaleInvoiceTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_INVOICE_NUMBER)) {
            $criteria->add(SaleInvoiceTableMap::COL_INVOICE_NUMBER, $this->invoice_number);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_EXTERNAL_ID)) {
            $criteria->add(SaleInvoiceTableMap::COL_EXTERNAL_ID, $this->external_id);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_EXTERNAL_SOURCE)) {
            $criteria->add(SaleInvoiceTableMap::COL_EXTERNAL_SOURCE, $this->external_source);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_CUSTOMER_ID)) {
            $criteria->add(SaleInvoiceTableMap::COL_CUSTOMER_ID, $this->customer_id);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_INVOICE_DATE)) {
            $criteria->add(SaleInvoiceTableMap::COL_INVOICE_DATE, $this->invoice_date);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_REVENUE_DATE)) {
            $criteria->add(SaleInvoiceTableMap::COL_REVENUE_DATE, $this->revenue_date);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_VAT_DATE)) {
            $criteria->add(SaleInvoiceTableMap::COL_VAT_DATE, $this->vat_date);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_DUE_DATE)) {
            $criteria->add(SaleInvoiceTableMap::COL_DUE_DATE, $this->due_date);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_IS_PAID)) {
            $criteria->add(SaleInvoiceTableMap::COL_IS_PAID, $this->is_paid);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_HAS_DOCUMENT)) {
            $criteria->add(SaleInvoiceTableMap::COL_HAS_DOCUMENT, $this->has_document);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_DOCUMENT_EXT)) {
            $criteria->add(SaleInvoiceTableMap::COL_DOCUMENT_EXT, $this->document_ext);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT)) {
            $criteria->add(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $this->total_payable_amount);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT)) {
            $criteria->add(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT, $this->outstanding_amount);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_CREATED_AT)) {
            $criteria->add(SaleInvoiceTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(SaleInvoiceTableMap::COL_UPDATED_AT)) {
            $criteria->add(SaleInvoiceTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSaleInvoiceQuery::create();
        $criteria->add(SaleInvoiceTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Finance\SaleInvoice (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setInvoiceNumber($this->getInvoiceNumber());
        $copyObj->setExternalId($this->getExternalId());
        $copyObj->setExternalSource($this->getExternalSource());
        $copyObj->setCustomerId($this->getCustomerId());
        $copyObj->setInvoiceDate($this->getInvoiceDate());
        $copyObj->setRevenueDate($this->getRevenueDate());
        $copyObj->setVatDate($this->getVatDate());
        $copyObj->setDueDate($this->getDueDate());
        $copyObj->setIsPaid($this->getIsPaid());
        $copyObj->setHasDocument($this->getHasDocument());
        $copyObj->setDocumentExt($this->getDocumentExt());
        $copyObj->setTotalPayableAmount($this->getTotalPayableAmount());
        $copyObj->setOutstandingAmount($this->getOutstandingAmount());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Finance\SaleInvoice Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Customer object.
     *
     * @param  Customer|null $v
     * @return $this|\Model\Finance\SaleInvoice The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCustomer(Customer $v = null)
    {
        if ($v === null) {
            $this->setCustomerId(NULL);
        } else {
            $this->setCustomerId($v->getId());
        }

        $this->aCustomer = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Customer object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleInvoice($this);
        }


        return $this;
    }


    /**
     * Get the associated Customer object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Customer|null The associated Customer object.
     * @throws PropelException
     */
    public function getCustomer(ConnectionInterface $con = null)
    {
        if ($this->aCustomer === null && ($this->customer_id != 0)) {
            $this->aCustomer = CustomerQuery::create()->findPk($this->customer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCustomer->addSaleInvoices($this);
             */
        }

        return $this->aCustomer;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCustomer) {
            $this->aCustomer->removeSaleInvoice($this);
        }
        $this->id = null;
        $this->invoice_number = null;
        $this->external_id = null;
        $this->external_source = null;
        $this->customer_id = null;
        $this->invoice_date = null;
        $this->revenue_date = null;
        $this->vat_date = null;
        $this->due_date = null;
        $this->is_paid = null;
        $this->has_document = null;
        $this->document_ext = null;
        $this->total_payable_amount = null;
        $this->outstanding_amount = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aCustomer = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SaleInvoiceTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildSaleInvoice The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[SaleInvoiceTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
