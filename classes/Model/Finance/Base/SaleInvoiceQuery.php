<?php

namespace Model\Finance\Base;

use \Exception;
use \PDO;
use Model\Crm\Customer;
use Model\Finance\SaleInvoice as ChildSaleInvoice;
use Model\Finance\SaleInvoiceQuery as ChildSaleInvoiceQuery;
use Model\Finance\Map\SaleInvoiceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_invoice' table.
 *
 *
 *
 * @method     ChildSaleInvoiceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleInvoiceQuery orderByInvoiceNumber($order = Criteria::ASC) Order by the invoice_number column
 * @method     ChildSaleInvoiceQuery orderByExternalId($order = Criteria::ASC) Order by the external_id column
 * @method     ChildSaleInvoiceQuery orderByExternalSource($order = Criteria::ASC) Order by the external_source column
 * @method     ChildSaleInvoiceQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildSaleInvoiceQuery orderByInvoiceDate($order = Criteria::ASC) Order by the invoice_date column
 * @method     ChildSaleInvoiceQuery orderByRevenueDate($order = Criteria::ASC) Order by the revenue_date column
 * @method     ChildSaleInvoiceQuery orderByVatDate($order = Criteria::ASC) Order by the vat_date column
 * @method     ChildSaleInvoiceQuery orderByDueDate($order = Criteria::ASC) Order by the due_date column
 * @method     ChildSaleInvoiceQuery orderByIsPaid($order = Criteria::ASC) Order by the is_paid column
 * @method     ChildSaleInvoiceQuery orderByHasDocument($order = Criteria::ASC) Order by the has_document column
 * @method     ChildSaleInvoiceQuery orderByDocumentExt($order = Criteria::ASC) Order by the document_ext column
 * @method     ChildSaleInvoiceQuery orderByTotalPayableAmount($order = Criteria::ASC) Order by the total_payable_amount column
 * @method     ChildSaleInvoiceQuery orderByOutstandingAmount($order = Criteria::ASC) Order by the outstanding_amount column
 * @method     ChildSaleInvoiceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildSaleInvoiceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildSaleInvoiceQuery groupById() Group by the id column
 * @method     ChildSaleInvoiceQuery groupByInvoiceNumber() Group by the invoice_number column
 * @method     ChildSaleInvoiceQuery groupByExternalId() Group by the external_id column
 * @method     ChildSaleInvoiceQuery groupByExternalSource() Group by the external_source column
 * @method     ChildSaleInvoiceQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildSaleInvoiceQuery groupByInvoiceDate() Group by the invoice_date column
 * @method     ChildSaleInvoiceQuery groupByRevenueDate() Group by the revenue_date column
 * @method     ChildSaleInvoiceQuery groupByVatDate() Group by the vat_date column
 * @method     ChildSaleInvoiceQuery groupByDueDate() Group by the due_date column
 * @method     ChildSaleInvoiceQuery groupByIsPaid() Group by the is_paid column
 * @method     ChildSaleInvoiceQuery groupByHasDocument() Group by the has_document column
 * @method     ChildSaleInvoiceQuery groupByDocumentExt() Group by the document_ext column
 * @method     ChildSaleInvoiceQuery groupByTotalPayableAmount() Group by the total_payable_amount column
 * @method     ChildSaleInvoiceQuery groupByOutstandingAmount() Group by the outstanding_amount column
 * @method     ChildSaleInvoiceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildSaleInvoiceQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildSaleInvoiceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleInvoiceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleInvoiceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleInvoiceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleInvoiceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleInvoiceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleInvoiceQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildSaleInvoiceQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildSaleInvoiceQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildSaleInvoiceQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildSaleInvoiceQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildSaleInvoiceQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildSaleInvoiceQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     \Model\Crm\CustomerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleInvoice findOne(ConnectionInterface $con = null) Return the first ChildSaleInvoice matching the query
 * @method     ChildSaleInvoice findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleInvoice matching the query, or a new ChildSaleInvoice object populated from the query conditions when no match is found
 *
 * @method     ChildSaleInvoice findOneById(int $id) Return the first ChildSaleInvoice filtered by the id column
 * @method     ChildSaleInvoice findOneByInvoiceNumber(string $invoice_number) Return the first ChildSaleInvoice filtered by the invoice_number column
 * @method     ChildSaleInvoice findOneByExternalId(string $external_id) Return the first ChildSaleInvoice filtered by the external_id column
 * @method     ChildSaleInvoice findOneByExternalSource(string $external_source) Return the first ChildSaleInvoice filtered by the external_source column
 * @method     ChildSaleInvoice findOneByCustomerId(int $customer_id) Return the first ChildSaleInvoice filtered by the customer_id column
 * @method     ChildSaleInvoice findOneByInvoiceDate(string $invoice_date) Return the first ChildSaleInvoice filtered by the invoice_date column
 * @method     ChildSaleInvoice findOneByRevenueDate(string $revenue_date) Return the first ChildSaleInvoice filtered by the revenue_date column
 * @method     ChildSaleInvoice findOneByVatDate(string $vat_date) Return the first ChildSaleInvoice filtered by the vat_date column
 * @method     ChildSaleInvoice findOneByDueDate(string $due_date) Return the first ChildSaleInvoice filtered by the due_date column
 * @method     ChildSaleInvoice findOneByIsPaid(boolean $is_paid) Return the first ChildSaleInvoice filtered by the is_paid column
 * @method     ChildSaleInvoice findOneByHasDocument(boolean $has_document) Return the first ChildSaleInvoice filtered by the has_document column
 * @method     ChildSaleInvoice findOneByDocumentExt(string $document_ext) Return the first ChildSaleInvoice filtered by the document_ext column
 * @method     ChildSaleInvoice findOneByTotalPayableAmount(string $total_payable_amount) Return the first ChildSaleInvoice filtered by the total_payable_amount column
 * @method     ChildSaleInvoice findOneByOutstandingAmount(string $outstanding_amount) Return the first ChildSaleInvoice filtered by the outstanding_amount column
 * @method     ChildSaleInvoice findOneByCreatedAt(string $created_at) Return the first ChildSaleInvoice filtered by the created_at column
 * @method     ChildSaleInvoice findOneByUpdatedAt(string $updated_at) Return the first ChildSaleInvoice filtered by the updated_at column *

 * @method     ChildSaleInvoice requirePk($key, ConnectionInterface $con = null) Return the ChildSaleInvoice by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOne(ConnectionInterface $con = null) Return the first ChildSaleInvoice matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleInvoice requireOneById(int $id) Return the first ChildSaleInvoice filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByInvoiceNumber(string $invoice_number) Return the first ChildSaleInvoice filtered by the invoice_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByExternalId(string $external_id) Return the first ChildSaleInvoice filtered by the external_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByExternalSource(string $external_source) Return the first ChildSaleInvoice filtered by the external_source column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByCustomerId(int $customer_id) Return the first ChildSaleInvoice filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByInvoiceDate(string $invoice_date) Return the first ChildSaleInvoice filtered by the invoice_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByRevenueDate(string $revenue_date) Return the first ChildSaleInvoice filtered by the revenue_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByVatDate(string $vat_date) Return the first ChildSaleInvoice filtered by the vat_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByDueDate(string $due_date) Return the first ChildSaleInvoice filtered by the due_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByIsPaid(boolean $is_paid) Return the first ChildSaleInvoice filtered by the is_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByHasDocument(boolean $has_document) Return the first ChildSaleInvoice filtered by the has_document column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByDocumentExt(string $document_ext) Return the first ChildSaleInvoice filtered by the document_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByTotalPayableAmount(string $total_payable_amount) Return the first ChildSaleInvoice filtered by the total_payable_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByOutstandingAmount(string $outstanding_amount) Return the first ChildSaleInvoice filtered by the outstanding_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByCreatedAt(string $created_at) Return the first ChildSaleInvoice filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleInvoice requireOneByUpdatedAt(string $updated_at) Return the first ChildSaleInvoice filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleInvoice[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleInvoice objects based on current ModelCriteria
 * @method     ChildSaleInvoice[]|ObjectCollection findById(int $id) Return ChildSaleInvoice objects filtered by the id column
 * @method     ChildSaleInvoice[]|ObjectCollection findByInvoiceNumber(string $invoice_number) Return ChildSaleInvoice objects filtered by the invoice_number column
 * @method     ChildSaleInvoice[]|ObjectCollection findByExternalId(string $external_id) Return ChildSaleInvoice objects filtered by the external_id column
 * @method     ChildSaleInvoice[]|ObjectCollection findByExternalSource(string $external_source) Return ChildSaleInvoice objects filtered by the external_source column
 * @method     ChildSaleInvoice[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildSaleInvoice objects filtered by the customer_id column
 * @method     ChildSaleInvoice[]|ObjectCollection findByInvoiceDate(string $invoice_date) Return ChildSaleInvoice objects filtered by the invoice_date column
 * @method     ChildSaleInvoice[]|ObjectCollection findByRevenueDate(string $revenue_date) Return ChildSaleInvoice objects filtered by the revenue_date column
 * @method     ChildSaleInvoice[]|ObjectCollection findByVatDate(string $vat_date) Return ChildSaleInvoice objects filtered by the vat_date column
 * @method     ChildSaleInvoice[]|ObjectCollection findByDueDate(string $due_date) Return ChildSaleInvoice objects filtered by the due_date column
 * @method     ChildSaleInvoice[]|ObjectCollection findByIsPaid(boolean $is_paid) Return ChildSaleInvoice objects filtered by the is_paid column
 * @method     ChildSaleInvoice[]|ObjectCollection findByHasDocument(boolean $has_document) Return ChildSaleInvoice objects filtered by the has_document column
 * @method     ChildSaleInvoice[]|ObjectCollection findByDocumentExt(string $document_ext) Return ChildSaleInvoice objects filtered by the document_ext column
 * @method     ChildSaleInvoice[]|ObjectCollection findByTotalPayableAmount(string $total_payable_amount) Return ChildSaleInvoice objects filtered by the total_payable_amount column
 * @method     ChildSaleInvoice[]|ObjectCollection findByOutstandingAmount(string $outstanding_amount) Return ChildSaleInvoice objects filtered by the outstanding_amount column
 * @method     ChildSaleInvoice[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildSaleInvoice objects filtered by the created_at column
 * @method     ChildSaleInvoice[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSaleInvoice objects filtered by the updated_at column
 * @method     ChildSaleInvoice[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleInvoiceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Finance\Base\SaleInvoiceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Finance\\SaleInvoice', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleInvoiceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleInvoiceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleInvoiceQuery) {
            return $criteria;
        }
        $query = new ChildSaleInvoiceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleInvoice|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleInvoiceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleInvoice A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, invoice_number, external_id, external_source, customer_id, invoice_date, revenue_date, vat_date, due_date, is_paid, has_document, document_ext, total_payable_amount, outstanding_amount, created_at, updated_at FROM sale_invoice WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleInvoice $obj */
            $obj = new ChildSaleInvoice();
            $obj->hydrate($row);
            SaleInvoiceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleInvoice|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the invoice_number column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceNumber('fooValue');   // WHERE invoice_number = 'fooValue'
     * $query->filterByInvoiceNumber('%fooValue%', Criteria::LIKE); // WHERE invoice_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByInvoiceNumber($invoiceNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_INVOICE_NUMBER, $invoiceNumber, $comparison);
    }

    /**
     * Filter the query on the external_id column
     *
     * Example usage:
     * <code>
     * $query->filterByExternalId('fooValue');   // WHERE external_id = 'fooValue'
     * $query->filterByExternalId('%fooValue%', Criteria::LIKE); // WHERE external_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $externalId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByExternalId($externalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($externalId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_EXTERNAL_ID, $externalId, $comparison);
    }

    /**
     * Filter the query on the external_source column
     *
     * Example usage:
     * <code>
     * $query->filterByExternalSource('fooValue');   // WHERE external_source = 'fooValue'
     * $query->filterByExternalSource('%fooValue%', Criteria::LIKE); // WHERE external_source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $externalSource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByExternalSource($externalSource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($externalSource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_EXTERNAL_SOURCE, $externalSource, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @see       filterByCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the invoice_date column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceDate('2011-03-14'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate('now'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate(array('max' => 'yesterday')); // WHERE invoice_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $invoiceDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByInvoiceDate($invoiceDate = null, $comparison = null)
    {
        if (is_array($invoiceDate)) {
            $useMinMax = false;
            if (isset($invoiceDate['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceDate['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_INVOICE_DATE, $invoiceDate, $comparison);
    }

    /**
     * Filter the query on the revenue_date column
     *
     * Example usage:
     * <code>
     * $query->filterByRevenueDate('2011-03-14'); // WHERE revenue_date = '2011-03-14'
     * $query->filterByRevenueDate('now'); // WHERE revenue_date = '2011-03-14'
     * $query->filterByRevenueDate(array('max' => 'yesterday')); // WHERE revenue_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $revenueDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByRevenueDate($revenueDate = null, $comparison = null)
    {
        if (is_array($revenueDate)) {
            $useMinMax = false;
            if (isset($revenueDate['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_REVENUE_DATE, $revenueDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($revenueDate['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_REVENUE_DATE, $revenueDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_REVENUE_DATE, $revenueDate, $comparison);
    }

    /**
     * Filter the query on the vat_date column
     *
     * Example usage:
     * <code>
     * $query->filterByVatDate('2011-03-14'); // WHERE vat_date = '2011-03-14'
     * $query->filterByVatDate('now'); // WHERE vat_date = '2011-03-14'
     * $query->filterByVatDate(array('max' => 'yesterday')); // WHERE vat_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $vatDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByVatDate($vatDate = null, $comparison = null)
    {
        if (is_array($vatDate)) {
            $useMinMax = false;
            if (isset($vatDate['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_VAT_DATE, $vatDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vatDate['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_VAT_DATE, $vatDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_VAT_DATE, $vatDate, $comparison);
    }

    /**
     * Filter the query on the due_date column
     *
     * Example usage:
     * <code>
     * $query->filterByDueDate('2011-03-14'); // WHERE due_date = '2011-03-14'
     * $query->filterByDueDate('now'); // WHERE due_date = '2011-03-14'
     * $query->filterByDueDate(array('max' => 'yesterday')); // WHERE due_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $dueDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByDueDate($dueDate = null, $comparison = null)
    {
        if (is_array($dueDate)) {
            $useMinMax = false;
            if (isset($dueDate['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_DUE_DATE, $dueDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dueDate['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_DUE_DATE, $dueDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_DUE_DATE, $dueDate, $comparison);
    }

    /**
     * Filter the query on the is_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPaid(true); // WHERE is_paid = true
     * $query->filterByIsPaid('yes'); // WHERE is_paid = true
     * </code>
     *
     * @param     boolean|string $isPaid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByIsPaid($isPaid = null, $comparison = null)
    {
        if (is_string($isPaid)) {
            $isPaid = in_array(strtolower($isPaid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_IS_PAID, $isPaid, $comparison);
    }

    /**
     * Filter the query on the has_document column
     *
     * Example usage:
     * <code>
     * $query->filterByHasDocument(true); // WHERE has_document = true
     * $query->filterByHasDocument('yes'); // WHERE has_document = true
     * </code>
     *
     * @param     boolean|string $hasDocument The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByHasDocument($hasDocument = null, $comparison = null)
    {
        if (is_string($hasDocument)) {
            $hasDocument = in_array(strtolower($hasDocument), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_HAS_DOCUMENT, $hasDocument, $comparison);
    }

    /**
     * Filter the query on the document_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByDocumentExt('fooValue');   // WHERE document_ext = 'fooValue'
     * $query->filterByDocumentExt('%fooValue%', Criteria::LIKE); // WHERE document_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $documentExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByDocumentExt($documentExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($documentExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_DOCUMENT_EXT, $documentExt, $comparison);
    }

    /**
     * Filter the query on the total_payable_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalPayableAmount(1234); // WHERE total_payable_amount = 1234
     * $query->filterByTotalPayableAmount(array(12, 34)); // WHERE total_payable_amount IN (12, 34)
     * $query->filterByTotalPayableAmount(array('min' => 12)); // WHERE total_payable_amount > 12
     * </code>
     *
     * @param     mixed $totalPayableAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByTotalPayableAmount($totalPayableAmount = null, $comparison = null)
    {
        if (is_array($totalPayableAmount)) {
            $useMinMax = false;
            if (isset($totalPayableAmount['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalPayableAmount['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, $totalPayableAmount, $comparison);
    }

    /**
     * Filter the query on the outstanding_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByOutstandingAmount(1234); // WHERE outstanding_amount = 1234
     * $query->filterByOutstandingAmount(array(12, 34)); // WHERE outstanding_amount IN (12, 34)
     * $query->filterByOutstandingAmount(array('min' => 12)); // WHERE outstanding_amount > 12
     * </code>
     *
     * @param     mixed $outstandingAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByOutstandingAmount($outstandingAmount = null, $comparison = null)
    {
        if (is_array($outstandingAmount)) {
            $useMinMax = false;
            if (isset($outstandingAmount['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT, $outstandingAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($outstandingAmount['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT, $outstandingAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT, $outstandingAmount, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SaleInvoiceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleInvoiceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function filterByCustomer($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(SaleInvoiceTableMap::COL_CUSTOMER_ID, $customer->getId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleInvoiceTableMap::COL_CUSTOMER_ID, $customer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleInvoice $saleInvoice Object to remove from the list of results
     *
     * @return $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function prune($saleInvoice = null)
    {
        if ($saleInvoice) {
            $this->addUsingAlias(SaleInvoiceTableMap::COL_ID, $saleInvoice->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleInvoiceTableMap::clearInstancePool();
            SaleInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleInvoiceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleInvoiceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleInvoiceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(SaleInvoiceTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(SaleInvoiceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(SaleInvoiceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(SaleInvoiceTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(SaleInvoiceTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildSaleInvoiceQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(SaleInvoiceTableMap::COL_CREATED_AT);
    }

} // SaleInvoiceQuery
