<?php

namespace Model\Finance\Base;

use \Exception;
use \PDO;
use Model\Finance\BankTransactionCategory as ChildBankTransactionCategory;
use Model\Finance\BankTransactionCategoryQuery as ChildBankTransactionCategoryQuery;
use Model\Finance\Map\BankTransactionCategoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bank_transaction_category' table.
 *
 *
 *
 * @method     ChildBankTransactionCategoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBankTransactionCategoryQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildBankTransactionCategoryQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method     ChildBankTransactionCategoryQuery groupById() Group by the id column
 * @method     ChildBankTransactionCategoryQuery groupByParentId() Group by the parent_id column
 * @method     ChildBankTransactionCategoryQuery groupByDescription() Group by the description column
 *
 * @method     ChildBankTransactionCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBankTransactionCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBankTransactionCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBankTransactionCategoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBankTransactionCategoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBankTransactionCategoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBankTransactionCategoryQuery leftJoinBankTransactionCategoryFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the BankTransactionCategoryFilter relation
 * @method     ChildBankTransactionCategoryQuery rightJoinBankTransactionCategoryFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BankTransactionCategoryFilter relation
 * @method     ChildBankTransactionCategoryQuery innerJoinBankTransactionCategoryFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the BankTransactionCategoryFilter relation
 *
 * @method     ChildBankTransactionCategoryQuery joinWithBankTransactionCategoryFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BankTransactionCategoryFilter relation
 *
 * @method     ChildBankTransactionCategoryQuery leftJoinWithBankTransactionCategoryFilter() Adds a LEFT JOIN clause and with to the query using the BankTransactionCategoryFilter relation
 * @method     ChildBankTransactionCategoryQuery rightJoinWithBankTransactionCategoryFilter() Adds a RIGHT JOIN clause and with to the query using the BankTransactionCategoryFilter relation
 * @method     ChildBankTransactionCategoryQuery innerJoinWithBankTransactionCategoryFilter() Adds a INNER JOIN clause and with to the query using the BankTransactionCategoryFilter relation
 *
 * @method     \Model\Finance\BankTransactionCategoryFilterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBankTransactionCategory findOne(ConnectionInterface $con = null) Return the first ChildBankTransactionCategory matching the query
 * @method     ChildBankTransactionCategory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBankTransactionCategory matching the query, or a new ChildBankTransactionCategory object populated from the query conditions when no match is found
 *
 * @method     ChildBankTransactionCategory findOneById(int $id) Return the first ChildBankTransactionCategory filtered by the id column
 * @method     ChildBankTransactionCategory findOneByParentId(int $parent_id) Return the first ChildBankTransactionCategory filtered by the parent_id column
 * @method     ChildBankTransactionCategory findOneByDescription(string $description) Return the first ChildBankTransactionCategory filtered by the description column *

 * @method     ChildBankTransactionCategory requirePk($key, ConnectionInterface $con = null) Return the ChildBankTransactionCategory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransactionCategory requireOne(ConnectionInterface $con = null) Return the first ChildBankTransactionCategory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankTransactionCategory requireOneById(int $id) Return the first ChildBankTransactionCategory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransactionCategory requireOneByParentId(int $parent_id) Return the first ChildBankTransactionCategory filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransactionCategory requireOneByDescription(string $description) Return the first ChildBankTransactionCategory filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankTransactionCategory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBankTransactionCategory objects based on current ModelCriteria
 * @method     ChildBankTransactionCategory[]|ObjectCollection findById(int $id) Return ChildBankTransactionCategory objects filtered by the id column
 * @method     ChildBankTransactionCategory[]|ObjectCollection findByParentId(int $parent_id) Return ChildBankTransactionCategory objects filtered by the parent_id column
 * @method     ChildBankTransactionCategory[]|ObjectCollection findByDescription(string $description) Return ChildBankTransactionCategory objects filtered by the description column
 * @method     ChildBankTransactionCategory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BankTransactionCategoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Finance\Base\BankTransactionCategoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Finance\\BankTransactionCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBankTransactionCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBankTransactionCategoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBankTransactionCategoryQuery) {
            return $criteria;
        }
        $query = new ChildBankTransactionCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBankTransactionCategory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BankTransactionCategoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BankTransactionCategoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankTransactionCategory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, parent_id, description FROM bank_transaction_category WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBankTransactionCategory $obj */
            $obj = new ChildBankTransactionCategory();
            $obj->hydrate($row);
            BankTransactionCategoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBankTransactionCategory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(BankTransactionCategoryTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(BankTransactionCategoryTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionCategoryTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionCategoryTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related \Model\Finance\BankTransactionCategoryFilter object
     *
     * @param \Model\Finance\BankTransactionCategoryFilter|ObjectCollection $bankTransactionCategoryFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function filterByBankTransactionCategoryFilter($bankTransactionCategoryFilter, $comparison = null)
    {
        if ($bankTransactionCategoryFilter instanceof \Model\Finance\BankTransactionCategoryFilter) {
            return $this
                ->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $bankTransactionCategoryFilter->getBankTransactionCategoryId(), $comparison);
        } elseif ($bankTransactionCategoryFilter instanceof ObjectCollection) {
            return $this
                ->useBankTransactionCategoryFilterQuery()
                ->filterByPrimaryKeys($bankTransactionCategoryFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBankTransactionCategoryFilter() only accepts arguments of type \Model\Finance\BankTransactionCategoryFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BankTransactionCategoryFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function joinBankTransactionCategoryFilter($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BankTransactionCategoryFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BankTransactionCategoryFilter');
        }

        return $this;
    }

    /**
     * Use the BankTransactionCategoryFilter relation BankTransactionCategoryFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\BankTransactionCategoryFilterQuery A secondary query class using the current class as primary query
     */
    public function useBankTransactionCategoryFilterQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBankTransactionCategoryFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BankTransactionCategoryFilter', '\Model\Finance\BankTransactionCategoryFilterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBankTransactionCategory $bankTransactionCategory Object to remove from the list of results
     *
     * @return $this|ChildBankTransactionCategoryQuery The current query, for fluid interface
     */
    public function prune($bankTransactionCategory = null)
    {
        if ($bankTransactionCategory) {
            $this->addUsingAlias(BankTransactionCategoryTableMap::COL_ID, $bankTransactionCategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bank_transaction_category table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionCategoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BankTransactionCategoryTableMap::clearInstancePool();
            BankTransactionCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionCategoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BankTransactionCategoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BankTransactionCategoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BankTransactionCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BankTransactionCategoryQuery
