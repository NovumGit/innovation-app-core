<?php

namespace Model\Finance\Base;

use \Exception;
use \PDO;
use Model\Finance\BankAccount as ChildBankAccount;
use Model\Finance\BankAccountQuery as ChildBankAccountQuery;
use Model\Finance\Map\BankAccountTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bank_account' table.
 *
 *
 *
 * @method     ChildBankAccountQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBankAccountQuery orderByOriginalId($order = Criteria::ASC) Order by the original_id column
 * @method     ChildBankAccountQuery orderByBankLedgerId($order = Criteria::ASC) Order by the bank_ledger_id column
 * @method     ChildBankAccountQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildBankAccountQuery orderByHasAvatar($order = Criteria::ASC) Order by the has_avatar column
 * @method     ChildBankAccountQuery orderByAvatarFileExt($order = Criteria::ASC) Order by the avatar_file_ext column
 * @method     ChildBankAccountQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method     ChildBankAccountQuery orderByCurrency($order = Criteria::ASC) Order by the currency column
 * @method     ChildBankAccountQuery orderByBalance($order = Criteria::ASC) Order by the balance column
 * @method     ChildBankAccountQuery orderByIsSavings($order = Criteria::ASC) Order by the is_savings column
 * @method     ChildBankAccountQuery orderBySavingsGoal($order = Criteria::ASC) Order by the savings_goal column
 * @method     ChildBankAccountQuery orderByIsActive($order = Criteria::ASC) Order by the is_active column
 * @method     ChildBankAccountQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildBankAccountQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildBankAccountQuery groupById() Group by the id column
 * @method     ChildBankAccountQuery groupByOriginalId() Group by the original_id column
 * @method     ChildBankAccountQuery groupByBankLedgerId() Group by the bank_ledger_id column
 * @method     ChildBankAccountQuery groupByName() Group by the name column
 * @method     ChildBankAccountQuery groupByHasAvatar() Group by the has_avatar column
 * @method     ChildBankAccountQuery groupByAvatarFileExt() Group by the avatar_file_ext column
 * @method     ChildBankAccountQuery groupByIban() Group by the iban column
 * @method     ChildBankAccountQuery groupByCurrency() Group by the currency column
 * @method     ChildBankAccountQuery groupByBalance() Group by the balance column
 * @method     ChildBankAccountQuery groupByIsSavings() Group by the is_savings column
 * @method     ChildBankAccountQuery groupBySavingsGoal() Group by the savings_goal column
 * @method     ChildBankAccountQuery groupByIsActive() Group by the is_active column
 * @method     ChildBankAccountQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildBankAccountQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildBankAccountQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBankAccountQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBankAccountQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBankAccountQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBankAccountQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBankAccountQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBankAccountQuery leftJoinBankLedger($relationAlias = null) Adds a LEFT JOIN clause to the query using the BankLedger relation
 * @method     ChildBankAccountQuery rightJoinBankLedger($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BankLedger relation
 * @method     ChildBankAccountQuery innerJoinBankLedger($relationAlias = null) Adds a INNER JOIN clause to the query using the BankLedger relation
 *
 * @method     ChildBankAccountQuery joinWithBankLedger($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BankLedger relation
 *
 * @method     ChildBankAccountQuery leftJoinWithBankLedger() Adds a LEFT JOIN clause and with to the query using the BankLedger relation
 * @method     ChildBankAccountQuery rightJoinWithBankLedger() Adds a RIGHT JOIN clause and with to the query using the BankLedger relation
 * @method     ChildBankAccountQuery innerJoinWithBankLedger() Adds a INNER JOIN clause and with to the query using the BankLedger relation
 *
 * @method     ChildBankAccountQuery leftJoinBankTransaction($relationAlias = null) Adds a LEFT JOIN clause to the query using the BankTransaction relation
 * @method     ChildBankAccountQuery rightJoinBankTransaction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BankTransaction relation
 * @method     ChildBankAccountQuery innerJoinBankTransaction($relationAlias = null) Adds a INNER JOIN clause to the query using the BankTransaction relation
 *
 * @method     ChildBankAccountQuery joinWithBankTransaction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BankTransaction relation
 *
 * @method     ChildBankAccountQuery leftJoinWithBankTransaction() Adds a LEFT JOIN clause and with to the query using the BankTransaction relation
 * @method     ChildBankAccountQuery rightJoinWithBankTransaction() Adds a RIGHT JOIN clause and with to the query using the BankTransaction relation
 * @method     ChildBankAccountQuery innerJoinWithBankTransaction() Adds a INNER JOIN clause and with to the query using the BankTransaction relation
 *
 * @method     \Model\Finance\BankLedgerQuery|\Model\Finance\BankTransactionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBankAccount findOne(ConnectionInterface $con = null) Return the first ChildBankAccount matching the query
 * @method     ChildBankAccount findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBankAccount matching the query, or a new ChildBankAccount object populated from the query conditions when no match is found
 *
 * @method     ChildBankAccount findOneById(int $id) Return the first ChildBankAccount filtered by the id column
 * @method     ChildBankAccount findOneByOriginalId(string $original_id) Return the first ChildBankAccount filtered by the original_id column
 * @method     ChildBankAccount findOneByBankLedgerId(int $bank_ledger_id) Return the first ChildBankAccount filtered by the bank_ledger_id column
 * @method     ChildBankAccount findOneByName(string $name) Return the first ChildBankAccount filtered by the name column
 * @method     ChildBankAccount findOneByHasAvatar(int $has_avatar) Return the first ChildBankAccount filtered by the has_avatar column
 * @method     ChildBankAccount findOneByAvatarFileExt(string $avatar_file_ext) Return the first ChildBankAccount filtered by the avatar_file_ext column
 * @method     ChildBankAccount findOneByIban(string $iban) Return the first ChildBankAccount filtered by the iban column
 * @method     ChildBankAccount findOneByCurrency(string $currency) Return the first ChildBankAccount filtered by the currency column
 * @method     ChildBankAccount findOneByBalance(double $balance) Return the first ChildBankAccount filtered by the balance column
 * @method     ChildBankAccount findOneByIsSavings(boolean $is_savings) Return the first ChildBankAccount filtered by the is_savings column
 * @method     ChildBankAccount findOneBySavingsGoal(double $savings_goal) Return the first ChildBankAccount filtered by the savings_goal column
 * @method     ChildBankAccount findOneByIsActive(boolean $is_active) Return the first ChildBankAccount filtered by the is_active column
 * @method     ChildBankAccount findOneByCreatedAt(string $created_at) Return the first ChildBankAccount filtered by the created_at column
 * @method     ChildBankAccount findOneByUpdatedAt(string $updated_at) Return the first ChildBankAccount filtered by the updated_at column *

 * @method     ChildBankAccount requirePk($key, ConnectionInterface $con = null) Return the ChildBankAccount by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOne(ConnectionInterface $con = null) Return the first ChildBankAccount matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankAccount requireOneById(int $id) Return the first ChildBankAccount filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByOriginalId(string $original_id) Return the first ChildBankAccount filtered by the original_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByBankLedgerId(int $bank_ledger_id) Return the first ChildBankAccount filtered by the bank_ledger_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByName(string $name) Return the first ChildBankAccount filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByHasAvatar(int $has_avatar) Return the first ChildBankAccount filtered by the has_avatar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByAvatarFileExt(string $avatar_file_ext) Return the first ChildBankAccount filtered by the avatar_file_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByIban(string $iban) Return the first ChildBankAccount filtered by the iban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByCurrency(string $currency) Return the first ChildBankAccount filtered by the currency column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByBalance(double $balance) Return the first ChildBankAccount filtered by the balance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByIsSavings(boolean $is_savings) Return the first ChildBankAccount filtered by the is_savings column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneBySavingsGoal(double $savings_goal) Return the first ChildBankAccount filtered by the savings_goal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByIsActive(boolean $is_active) Return the first ChildBankAccount filtered by the is_active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByCreatedAt(string $created_at) Return the first ChildBankAccount filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankAccount requireOneByUpdatedAt(string $updated_at) Return the first ChildBankAccount filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankAccount[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBankAccount objects based on current ModelCriteria
 * @method     ChildBankAccount[]|ObjectCollection findById(int $id) Return ChildBankAccount objects filtered by the id column
 * @method     ChildBankAccount[]|ObjectCollection findByOriginalId(string $original_id) Return ChildBankAccount objects filtered by the original_id column
 * @method     ChildBankAccount[]|ObjectCollection findByBankLedgerId(int $bank_ledger_id) Return ChildBankAccount objects filtered by the bank_ledger_id column
 * @method     ChildBankAccount[]|ObjectCollection findByName(string $name) Return ChildBankAccount objects filtered by the name column
 * @method     ChildBankAccount[]|ObjectCollection findByHasAvatar(int $has_avatar) Return ChildBankAccount objects filtered by the has_avatar column
 * @method     ChildBankAccount[]|ObjectCollection findByAvatarFileExt(string $avatar_file_ext) Return ChildBankAccount objects filtered by the avatar_file_ext column
 * @method     ChildBankAccount[]|ObjectCollection findByIban(string $iban) Return ChildBankAccount objects filtered by the iban column
 * @method     ChildBankAccount[]|ObjectCollection findByCurrency(string $currency) Return ChildBankAccount objects filtered by the currency column
 * @method     ChildBankAccount[]|ObjectCollection findByBalance(double $balance) Return ChildBankAccount objects filtered by the balance column
 * @method     ChildBankAccount[]|ObjectCollection findByIsSavings(boolean $is_savings) Return ChildBankAccount objects filtered by the is_savings column
 * @method     ChildBankAccount[]|ObjectCollection findBySavingsGoal(double $savings_goal) Return ChildBankAccount objects filtered by the savings_goal column
 * @method     ChildBankAccount[]|ObjectCollection findByIsActive(boolean $is_active) Return ChildBankAccount objects filtered by the is_active column
 * @method     ChildBankAccount[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildBankAccount objects filtered by the created_at column
 * @method     ChildBankAccount[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildBankAccount objects filtered by the updated_at column
 * @method     ChildBankAccount[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BankAccountQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Finance\Base\BankAccountQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Finance\\BankAccount', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBankAccountQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBankAccountQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBankAccountQuery) {
            return $criteria;
        }
        $query = new ChildBankAccountQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBankAccount|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BankAccountTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BankAccountTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankAccount A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, original_id, bank_ledger_id, name, has_avatar, avatar_file_ext, iban, currency, balance, is_savings, savings_goal, is_active, created_at, updated_at FROM bank_account WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBankAccount $obj */
            $obj = new ChildBankAccount();
            $obj->hydrate($row);
            BankAccountTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBankAccount|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BankAccountTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BankAccountTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the original_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalId('fooValue');   // WHERE original_id = 'fooValue'
     * $query->filterByOriginalId('%fooValue%', Criteria::LIKE); // WHERE original_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByOriginalId($originalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_ORIGINAL_ID, $originalId, $comparison);
    }

    /**
     * Filter the query on the bank_ledger_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBankLedgerId(1234); // WHERE bank_ledger_id = 1234
     * $query->filterByBankLedgerId(array(12, 34)); // WHERE bank_ledger_id IN (12, 34)
     * $query->filterByBankLedgerId(array('min' => 12)); // WHERE bank_ledger_id > 12
     * </code>
     *
     * @see       filterByBankLedger()
     *
     * @param     mixed $bankLedgerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByBankLedgerId($bankLedgerId = null, $comparison = null)
    {
        if (is_array($bankLedgerId)) {
            $useMinMax = false;
            if (isset($bankLedgerId['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_BANK_LEDGER_ID, $bankLedgerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bankLedgerId['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_BANK_LEDGER_ID, $bankLedgerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_BANK_LEDGER_ID, $bankLedgerId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the has_avatar column
     *
     * Example usage:
     * <code>
     * $query->filterByHasAvatar(1234); // WHERE has_avatar = 1234
     * $query->filterByHasAvatar(array(12, 34)); // WHERE has_avatar IN (12, 34)
     * $query->filterByHasAvatar(array('min' => 12)); // WHERE has_avatar > 12
     * </code>
     *
     * @param     mixed $hasAvatar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByHasAvatar($hasAvatar = null, $comparison = null)
    {
        if (is_array($hasAvatar)) {
            $useMinMax = false;
            if (isset($hasAvatar['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_HAS_AVATAR, $hasAvatar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hasAvatar['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_HAS_AVATAR, $hasAvatar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_HAS_AVATAR, $hasAvatar, $comparison);
    }

    /**
     * Filter the query on the avatar_file_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByAvatarFileExt('fooValue');   // WHERE avatar_file_ext = 'fooValue'
     * $query->filterByAvatarFileExt('%fooValue%', Criteria::LIKE); // WHERE avatar_file_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $avatarFileExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByAvatarFileExt($avatarFileExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($avatarFileExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_AVATAR_FILE_EXT, $avatarFileExt, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%', Criteria::LIKE); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the currency column
     *
     * Example usage:
     * <code>
     * $query->filterByCurrency('fooValue');   // WHERE currency = 'fooValue'
     * $query->filterByCurrency('%fooValue%', Criteria::LIKE); // WHERE currency LIKE '%fooValue%'
     * </code>
     *
     * @param     string $currency The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByCurrency($currency = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($currency)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_CURRENCY, $currency, $comparison);
    }

    /**
     * Filter the query on the balance column
     *
     * Example usage:
     * <code>
     * $query->filterByBalance(1234); // WHERE balance = 1234
     * $query->filterByBalance(array(12, 34)); // WHERE balance IN (12, 34)
     * $query->filterByBalance(array('min' => 12)); // WHERE balance > 12
     * </code>
     *
     * @param     mixed $balance The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByBalance($balance = null, $comparison = null)
    {
        if (is_array($balance)) {
            $useMinMax = false;
            if (isset($balance['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_BALANCE, $balance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($balance['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_BALANCE, $balance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_BALANCE, $balance, $comparison);
    }

    /**
     * Filter the query on the is_savings column
     *
     * Example usage:
     * <code>
     * $query->filterByIsSavings(true); // WHERE is_savings = true
     * $query->filterByIsSavings('yes'); // WHERE is_savings = true
     * </code>
     *
     * @param     boolean|string $isSavings The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByIsSavings($isSavings = null, $comparison = null)
    {
        if (is_string($isSavings)) {
            $isSavings = in_array(strtolower($isSavings), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_IS_SAVINGS, $isSavings, $comparison);
    }

    /**
     * Filter the query on the savings_goal column
     *
     * Example usage:
     * <code>
     * $query->filterBySavingsGoal(1234); // WHERE savings_goal = 1234
     * $query->filterBySavingsGoal(array(12, 34)); // WHERE savings_goal IN (12, 34)
     * $query->filterBySavingsGoal(array('min' => 12)); // WHERE savings_goal > 12
     * </code>
     *
     * @param     mixed $savingsGoal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterBySavingsGoal($savingsGoal = null, $comparison = null)
    {
        if (is_array($savingsGoal)) {
            $useMinMax = false;
            if (isset($savingsGoal['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_SAVINGS_GOAL, $savingsGoal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($savingsGoal['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_SAVINGS_GOAL, $savingsGoal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_SAVINGS_GOAL, $savingsGoal, $comparison);
    }

    /**
     * Filter the query on the is_active column
     *
     * Example usage:
     * <code>
     * $query->filterByIsActive(true); // WHERE is_active = true
     * $query->filterByIsActive('yes'); // WHERE is_active = true
     * </code>
     *
     * @param     boolean|string $isActive The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByIsActive($isActive = null, $comparison = null)
    {
        if (is_string($isActive)) {
            $isActive = in_array(strtolower($isActive), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_IS_ACTIVE, $isActive, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(BankAccountTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankAccountTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Finance\BankLedger object
     *
     * @param \Model\Finance\BankLedger|ObjectCollection $bankLedger The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByBankLedger($bankLedger, $comparison = null)
    {
        if ($bankLedger instanceof \Model\Finance\BankLedger) {
            return $this
                ->addUsingAlias(BankAccountTableMap::COL_BANK_LEDGER_ID, $bankLedger->getId(), $comparison);
        } elseif ($bankLedger instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BankAccountTableMap::COL_BANK_LEDGER_ID, $bankLedger->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBankLedger() only accepts arguments of type \Model\Finance\BankLedger or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BankLedger relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function joinBankLedger($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BankLedger');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BankLedger');
        }

        return $this;
    }

    /**
     * Use the BankLedger relation BankLedger object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\BankLedgerQuery A secondary query class using the current class as primary query
     */
    public function useBankLedgerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBankLedger($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BankLedger', '\Model\Finance\BankLedgerQuery');
    }

    /**
     * Filter the query by a related \Model\Finance\BankTransaction object
     *
     * @param \Model\Finance\BankTransaction|ObjectCollection $bankTransaction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBankAccountQuery The current query, for fluid interface
     */
    public function filterByBankTransaction($bankTransaction, $comparison = null)
    {
        if ($bankTransaction instanceof \Model\Finance\BankTransaction) {
            return $this
                ->addUsingAlias(BankAccountTableMap::COL_ID, $bankTransaction->getBankAccountId(), $comparison);
        } elseif ($bankTransaction instanceof ObjectCollection) {
            return $this
                ->useBankTransactionQuery()
                ->filterByPrimaryKeys($bankTransaction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBankTransaction() only accepts arguments of type \Model\Finance\BankTransaction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BankTransaction relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function joinBankTransaction($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BankTransaction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BankTransaction');
        }

        return $this;
    }

    /**
     * Use the BankTransaction relation BankTransaction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\BankTransactionQuery A secondary query class using the current class as primary query
     */
    public function useBankTransactionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBankTransaction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BankTransaction', '\Model\Finance\BankTransactionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBankAccount $bankAccount Object to remove from the list of results
     *
     * @return $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function prune($bankAccount = null)
    {
        if ($bankAccount) {
            $this->addUsingAlias(BankAccountTableMap::COL_ID, $bankAccount->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bank_account table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankAccountTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BankAccountTableMap::clearInstancePool();
            BankAccountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankAccountTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BankAccountTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BankAccountTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BankAccountTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(BankAccountTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(BankAccountTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(BankAccountTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(BankAccountTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(BankAccountTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildBankAccountQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(BankAccountTableMap::COL_CREATED_AT);
    }

} // BankAccountQuery
