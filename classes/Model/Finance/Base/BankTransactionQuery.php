<?php

namespace Model\Finance\Base;

use \Exception;
use \PDO;
use Model\Finance\BankTransaction as ChildBankTransaction;
use Model\Finance\BankTransactionQuery as ChildBankTransactionQuery;
use Model\Finance\Map\BankTransactionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'bank_transaction' table.
 *
 *
 *
 * @method     ChildBankTransactionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildBankTransactionQuery orderByOriginalId($order = Criteria::ASC) Order by the original_id column
 * @method     ChildBankTransactionQuery orderByBankAccountId($order = Criteria::ASC) Order by the bank_account_id column
 * @method     ChildBankTransactionQuery orderByBankTransactionCategoryId($order = Criteria::ASC) Order by the bank_transaction_category_id column
 * @method     ChildBankTransactionQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildBankTransactionQuery orderByDescriptionOriginal($order = Criteria::ASC) Order by the description_original column
 * @method     ChildBankTransactionQuery orderByCounterBankIbanId($order = Criteria::ASC) Order by the counter_bank_iban_id column
 * @method     ChildBankTransactionQuery orderByTransactionType($order = Criteria::ASC) Order by the transaction_type column
 * @method     ChildBankTransactionQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildBankTransactionQuery orderByTransactionTime($order = Criteria::ASC) Order by the transaction_time column
 * @method     ChildBankTransactionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildBankTransactionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildBankTransactionQuery groupById() Group by the id column
 * @method     ChildBankTransactionQuery groupByOriginalId() Group by the original_id column
 * @method     ChildBankTransactionQuery groupByBankAccountId() Group by the bank_account_id column
 * @method     ChildBankTransactionQuery groupByBankTransactionCategoryId() Group by the bank_transaction_category_id column
 * @method     ChildBankTransactionQuery groupByDescription() Group by the description column
 * @method     ChildBankTransactionQuery groupByDescriptionOriginal() Group by the description_original column
 * @method     ChildBankTransactionQuery groupByCounterBankIbanId() Group by the counter_bank_iban_id column
 * @method     ChildBankTransactionQuery groupByTransactionType() Group by the transaction_type column
 * @method     ChildBankTransactionQuery groupByAmount() Group by the amount column
 * @method     ChildBankTransactionQuery groupByTransactionTime() Group by the transaction_time column
 * @method     ChildBankTransactionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildBankTransactionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildBankTransactionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBankTransactionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBankTransactionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBankTransactionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBankTransactionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBankTransactionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBankTransactionQuery leftJoinBankIbanAddressBook($relationAlias = null) Adds a LEFT JOIN clause to the query using the BankIbanAddressBook relation
 * @method     ChildBankTransactionQuery rightJoinBankIbanAddressBook($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BankIbanAddressBook relation
 * @method     ChildBankTransactionQuery innerJoinBankIbanAddressBook($relationAlias = null) Adds a INNER JOIN clause to the query using the BankIbanAddressBook relation
 *
 * @method     ChildBankTransactionQuery joinWithBankIbanAddressBook($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BankIbanAddressBook relation
 *
 * @method     ChildBankTransactionQuery leftJoinWithBankIbanAddressBook() Adds a LEFT JOIN clause and with to the query using the BankIbanAddressBook relation
 * @method     ChildBankTransactionQuery rightJoinWithBankIbanAddressBook() Adds a RIGHT JOIN clause and with to the query using the BankIbanAddressBook relation
 * @method     ChildBankTransactionQuery innerJoinWithBankIbanAddressBook() Adds a INNER JOIN clause and with to the query using the BankIbanAddressBook relation
 *
 * @method     ChildBankTransactionQuery leftJoinBankAccount($relationAlias = null) Adds a LEFT JOIN clause to the query using the BankAccount relation
 * @method     ChildBankTransactionQuery rightJoinBankAccount($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BankAccount relation
 * @method     ChildBankTransactionQuery innerJoinBankAccount($relationAlias = null) Adds a INNER JOIN clause to the query using the BankAccount relation
 *
 * @method     ChildBankTransactionQuery joinWithBankAccount($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BankAccount relation
 *
 * @method     ChildBankTransactionQuery leftJoinWithBankAccount() Adds a LEFT JOIN clause and with to the query using the BankAccount relation
 * @method     ChildBankTransactionQuery rightJoinWithBankAccount() Adds a RIGHT JOIN clause and with to the query using the BankAccount relation
 * @method     ChildBankTransactionQuery innerJoinWithBankAccount() Adds a INNER JOIN clause and with to the query using the BankAccount relation
 *
 * @method     \Model\Finance\BankIbanAddressBookQuery|\Model\Finance\BankAccountQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBankTransaction findOne(ConnectionInterface $con = null) Return the first ChildBankTransaction matching the query
 * @method     ChildBankTransaction findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBankTransaction matching the query, or a new ChildBankTransaction object populated from the query conditions when no match is found
 *
 * @method     ChildBankTransaction findOneById(int $id) Return the first ChildBankTransaction filtered by the id column
 * @method     ChildBankTransaction findOneByOriginalId(string $original_id) Return the first ChildBankTransaction filtered by the original_id column
 * @method     ChildBankTransaction findOneByBankAccountId(int $bank_account_id) Return the first ChildBankTransaction filtered by the bank_account_id column
 * @method     ChildBankTransaction findOneByBankTransactionCategoryId(int $bank_transaction_category_id) Return the first ChildBankTransaction filtered by the bank_transaction_category_id column
 * @method     ChildBankTransaction findOneByDescription(string $description) Return the first ChildBankTransaction filtered by the description column
 * @method     ChildBankTransaction findOneByDescriptionOriginal(string $description_original) Return the first ChildBankTransaction filtered by the description_original column
 * @method     ChildBankTransaction findOneByCounterBankIbanId(int $counter_bank_iban_id) Return the first ChildBankTransaction filtered by the counter_bank_iban_id column
 * @method     ChildBankTransaction findOneByTransactionType(string $transaction_type) Return the first ChildBankTransaction filtered by the transaction_type column
 * @method     ChildBankTransaction findOneByAmount(double $amount) Return the first ChildBankTransaction filtered by the amount column
 * @method     ChildBankTransaction findOneByTransactionTime(string $transaction_time) Return the first ChildBankTransaction filtered by the transaction_time column
 * @method     ChildBankTransaction findOneByCreatedAt(string $created_at) Return the first ChildBankTransaction filtered by the created_at column
 * @method     ChildBankTransaction findOneByUpdatedAt(string $updated_at) Return the first ChildBankTransaction filtered by the updated_at column *

 * @method     ChildBankTransaction requirePk($key, ConnectionInterface $con = null) Return the ChildBankTransaction by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOne(ConnectionInterface $con = null) Return the first ChildBankTransaction matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankTransaction requireOneById(int $id) Return the first ChildBankTransaction filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByOriginalId(string $original_id) Return the first ChildBankTransaction filtered by the original_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByBankAccountId(int $bank_account_id) Return the first ChildBankTransaction filtered by the bank_account_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByBankTransactionCategoryId(int $bank_transaction_category_id) Return the first ChildBankTransaction filtered by the bank_transaction_category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByDescription(string $description) Return the first ChildBankTransaction filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByDescriptionOriginal(string $description_original) Return the first ChildBankTransaction filtered by the description_original column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByCounterBankIbanId(int $counter_bank_iban_id) Return the first ChildBankTransaction filtered by the counter_bank_iban_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByTransactionType(string $transaction_type) Return the first ChildBankTransaction filtered by the transaction_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByAmount(double $amount) Return the first ChildBankTransaction filtered by the amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByTransactionTime(string $transaction_time) Return the first ChildBankTransaction filtered by the transaction_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByCreatedAt(string $created_at) Return the first ChildBankTransaction filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBankTransaction requireOneByUpdatedAt(string $updated_at) Return the first ChildBankTransaction filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBankTransaction[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBankTransaction objects based on current ModelCriteria
 * @method     ChildBankTransaction[]|ObjectCollection findById(int $id) Return ChildBankTransaction objects filtered by the id column
 * @method     ChildBankTransaction[]|ObjectCollection findByOriginalId(string $original_id) Return ChildBankTransaction objects filtered by the original_id column
 * @method     ChildBankTransaction[]|ObjectCollection findByBankAccountId(int $bank_account_id) Return ChildBankTransaction objects filtered by the bank_account_id column
 * @method     ChildBankTransaction[]|ObjectCollection findByBankTransactionCategoryId(int $bank_transaction_category_id) Return ChildBankTransaction objects filtered by the bank_transaction_category_id column
 * @method     ChildBankTransaction[]|ObjectCollection findByDescription(string $description) Return ChildBankTransaction objects filtered by the description column
 * @method     ChildBankTransaction[]|ObjectCollection findByDescriptionOriginal(string $description_original) Return ChildBankTransaction objects filtered by the description_original column
 * @method     ChildBankTransaction[]|ObjectCollection findByCounterBankIbanId(int $counter_bank_iban_id) Return ChildBankTransaction objects filtered by the counter_bank_iban_id column
 * @method     ChildBankTransaction[]|ObjectCollection findByTransactionType(string $transaction_type) Return ChildBankTransaction objects filtered by the transaction_type column
 * @method     ChildBankTransaction[]|ObjectCollection findByAmount(double $amount) Return ChildBankTransaction objects filtered by the amount column
 * @method     ChildBankTransaction[]|ObjectCollection findByTransactionTime(string $transaction_time) Return ChildBankTransaction objects filtered by the transaction_time column
 * @method     ChildBankTransaction[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildBankTransaction objects filtered by the created_at column
 * @method     ChildBankTransaction[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildBankTransaction objects filtered by the updated_at column
 * @method     ChildBankTransaction[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BankTransactionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Finance\Base\BankTransactionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Finance\\BankTransaction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBankTransactionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBankTransactionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBankTransactionQuery) {
            return $criteria;
        }
        $query = new ChildBankTransactionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBankTransaction|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BankTransactionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BankTransactionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankTransaction A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, original_id, bank_account_id, bank_transaction_category_id, description, description_original, counter_bank_iban_id, transaction_type, amount, transaction_time, created_at, updated_at FROM bank_transaction WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBankTransaction $obj */
            $obj = new ChildBankTransaction();
            $obj->hydrate($row);
            BankTransactionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBankTransaction|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BankTransactionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BankTransactionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the original_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalId('fooValue');   // WHERE original_id = 'fooValue'
     * $query->filterByOriginalId('%fooValue%', Criteria::LIKE); // WHERE original_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByOriginalId($originalId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_ORIGINAL_ID, $originalId, $comparison);
    }

    /**
     * Filter the query on the bank_account_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBankAccountId(1234); // WHERE bank_account_id = 1234
     * $query->filterByBankAccountId(array(12, 34)); // WHERE bank_account_id IN (12, 34)
     * $query->filterByBankAccountId(array('min' => 12)); // WHERE bank_account_id > 12
     * </code>
     *
     * @see       filterByBankAccount()
     *
     * @param     mixed $bankAccountId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByBankAccountId($bankAccountId = null, $comparison = null)
    {
        if (is_array($bankAccountId)) {
            $useMinMax = false;
            if (isset($bankAccountId['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_BANK_ACCOUNT_ID, $bankAccountId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bankAccountId['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_BANK_ACCOUNT_ID, $bankAccountId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_BANK_ACCOUNT_ID, $bankAccountId, $comparison);
    }

    /**
     * Filter the query on the bank_transaction_category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBankTransactionCategoryId(1234); // WHERE bank_transaction_category_id = 1234
     * $query->filterByBankTransactionCategoryId(array(12, 34)); // WHERE bank_transaction_category_id IN (12, 34)
     * $query->filterByBankTransactionCategoryId(array('min' => 12)); // WHERE bank_transaction_category_id > 12
     * </code>
     *
     * @param     mixed $bankTransactionCategoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByBankTransactionCategoryId($bankTransactionCategoryId = null, $comparison = null)
    {
        if (is_array($bankTransactionCategoryId)) {
            $useMinMax = false;
            if (isset($bankTransactionCategoryId['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID, $bankTransactionCategoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bankTransactionCategoryId['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID, $bankTransactionCategoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID, $bankTransactionCategoryId, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the description_original column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptionOriginal('fooValue');   // WHERE description_original = 'fooValue'
     * $query->filterByDescriptionOriginal('%fooValue%', Criteria::LIKE); // WHERE description_original LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptionOriginal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByDescriptionOriginal($descriptionOriginal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptionOriginal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_DESCRIPTION_ORIGINAL, $descriptionOriginal, $comparison);
    }

    /**
     * Filter the query on the counter_bank_iban_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCounterBankIbanId(1234); // WHERE counter_bank_iban_id = 1234
     * $query->filterByCounterBankIbanId(array(12, 34)); // WHERE counter_bank_iban_id IN (12, 34)
     * $query->filterByCounterBankIbanId(array('min' => 12)); // WHERE counter_bank_iban_id > 12
     * </code>
     *
     * @see       filterByBankIbanAddressBook()
     *
     * @param     mixed $counterBankIbanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByCounterBankIbanId($counterBankIbanId = null, $comparison = null)
    {
        if (is_array($counterBankIbanId)) {
            $useMinMax = false;
            if (isset($counterBankIbanId['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, $counterBankIbanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($counterBankIbanId['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, $counterBankIbanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, $counterBankIbanId, $comparison);
    }

    /**
     * Filter the query on the transaction_type column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionType('fooValue');   // WHERE transaction_type = 'fooValue'
     * $query->filterByTransactionType('%fooValue%', Criteria::LIKE); // WHERE transaction_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByTransactionType($transactionType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_TRANSACTION_TYPE, $transactionType, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount(1234); // WHERE amount = 1234
     * $query->filterByAmount(array(12, 34)); // WHERE amount IN (12, 34)
     * $query->filterByAmount(array('min' => 12)); // WHERE amount > 12
     * </code>
     *
     * @param     mixed $amount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (is_array($amount)) {
            $useMinMax = false;
            if (isset($amount['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_AMOUNT, $amount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($amount['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_AMOUNT, $amount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the transaction_time column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionTime('2011-03-14'); // WHERE transaction_time = '2011-03-14'
     * $query->filterByTransactionTime('now'); // WHERE transaction_time = '2011-03-14'
     * $query->filterByTransactionTime(array('max' => 'yesterday')); // WHERE transaction_time > '2011-03-13'
     * </code>
     *
     * @param     mixed $transactionTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByTransactionTime($transactionTime = null, $comparison = null)
    {
        if (is_array($transactionTime)) {
            $useMinMax = false;
            if (isset($transactionTime['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_TRANSACTION_TIME, $transactionTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($transactionTime['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_TRANSACTION_TIME, $transactionTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_TRANSACTION_TIME, $transactionTime, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(BankTransactionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BankTransactionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Finance\BankIbanAddressBook object
     *
     * @param \Model\Finance\BankIbanAddressBook|ObjectCollection $bankIbanAddressBook The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByBankIbanAddressBook($bankIbanAddressBook, $comparison = null)
    {
        if ($bankIbanAddressBook instanceof \Model\Finance\BankIbanAddressBook) {
            return $this
                ->addUsingAlias(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, $bankIbanAddressBook->getId(), $comparison);
        } elseif ($bankIbanAddressBook instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, $bankIbanAddressBook->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBankIbanAddressBook() only accepts arguments of type \Model\Finance\BankIbanAddressBook or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BankIbanAddressBook relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function joinBankIbanAddressBook($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BankIbanAddressBook');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BankIbanAddressBook');
        }

        return $this;
    }

    /**
     * Use the BankIbanAddressBook relation BankIbanAddressBook object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\BankIbanAddressBookQuery A secondary query class using the current class as primary query
     */
    public function useBankIbanAddressBookQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBankIbanAddressBook($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BankIbanAddressBook', '\Model\Finance\BankIbanAddressBookQuery');
    }

    /**
     * Filter the query by a related \Model\Finance\BankAccount object
     *
     * @param \Model\Finance\BankAccount|ObjectCollection $bankAccount The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBankTransactionQuery The current query, for fluid interface
     */
    public function filterByBankAccount($bankAccount, $comparison = null)
    {
        if ($bankAccount instanceof \Model\Finance\BankAccount) {
            return $this
                ->addUsingAlias(BankTransactionTableMap::COL_BANK_ACCOUNT_ID, $bankAccount->getId(), $comparison);
        } elseif ($bankAccount instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BankTransactionTableMap::COL_BANK_ACCOUNT_ID, $bankAccount->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBankAccount() only accepts arguments of type \Model\Finance\BankAccount or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BankAccount relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function joinBankAccount($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BankAccount');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BankAccount');
        }

        return $this;
    }

    /**
     * Use the BankAccount relation BankAccount object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\BankAccountQuery A secondary query class using the current class as primary query
     */
    public function useBankAccountQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBankAccount($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BankAccount', '\Model\Finance\BankAccountQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBankTransaction $bankTransaction Object to remove from the list of results
     *
     * @return $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function prune($bankTransaction = null)
    {
        if ($bankTransaction) {
            $this->addUsingAlias(BankTransactionTableMap::COL_ID, $bankTransaction->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the bank_transaction table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BankTransactionTableMap::clearInstancePool();
            BankTransactionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BankTransactionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BankTransactionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BankTransactionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(BankTransactionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(BankTransactionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(BankTransactionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(BankTransactionTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(BankTransactionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildBankTransactionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(BankTransactionTableMap::COL_CREATED_AT);
    }

} // BankTransactionQuery
