<?php

namespace Model\Finance;

use Model\Finance\Base\SaleInvoiceQuery as BaseSaleInvoiceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sale_invoice' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleInvoiceQuery extends BaseSaleInvoiceQuery
{

}
