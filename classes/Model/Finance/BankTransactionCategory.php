<?php

namespace Model\Finance;

use Model\Finance\Base\BankTransactionCategory as BaseBankTransactionCategory;

/**
 * Skeleton subclass for representing a row from the 'bank_transaction_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BankTransactionCategory extends BaseBankTransactionCategory
{

}
