<?php

namespace Model\Finance;

use Model\Finance\Base\BankAccount as BaseBankAccount;

/**
 * Skeleton subclass for representing a row from the 'bank_account' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BankAccount extends BaseBankAccount
{

	function getSavingsPercentage()
	{
		if($this->getSavingsGoal() <= 0)
		{
			return 0;
		}
		return $this->getBalance() / ($this->getSavingsGoal() / 100);
	}
}
