<?php

namespace Model\Finance;

use Model\Finance\Base\PurchaseInvoiceQuery as BasePurchaseInvoiceQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'purchase_invoice' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PurchaseInvoiceQuery extends BasePurchaseInvoiceQuery
{

}
