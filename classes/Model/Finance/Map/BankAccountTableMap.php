<?php

namespace Model\Finance\Map;

use Model\Finance\BankAccount;
use Model\Finance\BankAccountQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'bank_account' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class BankAccountTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Finance.Map.BankAccountTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'bank_account';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Finance\\BankAccount';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Finance.BankAccount';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'bank_account.id';

    /**
     * the column name for the original_id field
     */
    const COL_ORIGINAL_ID = 'bank_account.original_id';

    /**
     * the column name for the bank_ledger_id field
     */
    const COL_BANK_LEDGER_ID = 'bank_account.bank_ledger_id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'bank_account.name';

    /**
     * the column name for the has_avatar field
     */
    const COL_HAS_AVATAR = 'bank_account.has_avatar';

    /**
     * the column name for the avatar_file_ext field
     */
    const COL_AVATAR_FILE_EXT = 'bank_account.avatar_file_ext';

    /**
     * the column name for the iban field
     */
    const COL_IBAN = 'bank_account.iban';

    /**
     * the column name for the currency field
     */
    const COL_CURRENCY = 'bank_account.currency';

    /**
     * the column name for the balance field
     */
    const COL_BALANCE = 'bank_account.balance';

    /**
     * the column name for the is_savings field
     */
    const COL_IS_SAVINGS = 'bank_account.is_savings';

    /**
     * the column name for the savings_goal field
     */
    const COL_SAVINGS_GOAL = 'bank_account.savings_goal';

    /**
     * the column name for the is_active field
     */
    const COL_IS_ACTIVE = 'bank_account.is_active';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'bank_account.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'bank_account.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OriginalId', 'BankLedgerId', 'Name', 'HasAvatar', 'AvatarFileExt', 'Iban', 'Currency', 'Balance', 'IsSavings', 'SavingsGoal', 'IsActive', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'originalId', 'bankLedgerId', 'name', 'hasAvatar', 'avatarFileExt', 'iban', 'currency', 'balance', 'isSavings', 'savingsGoal', 'isActive', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(BankAccountTableMap::COL_ID, BankAccountTableMap::COL_ORIGINAL_ID, BankAccountTableMap::COL_BANK_LEDGER_ID, BankAccountTableMap::COL_NAME, BankAccountTableMap::COL_HAS_AVATAR, BankAccountTableMap::COL_AVATAR_FILE_EXT, BankAccountTableMap::COL_IBAN, BankAccountTableMap::COL_CURRENCY, BankAccountTableMap::COL_BALANCE, BankAccountTableMap::COL_IS_SAVINGS, BankAccountTableMap::COL_SAVINGS_GOAL, BankAccountTableMap::COL_IS_ACTIVE, BankAccountTableMap::COL_CREATED_AT, BankAccountTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'original_id', 'bank_ledger_id', 'name', 'has_avatar', 'avatar_file_ext', 'iban', 'currency', 'balance', 'is_savings', 'savings_goal', 'is_active', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OriginalId' => 1, 'BankLedgerId' => 2, 'Name' => 3, 'HasAvatar' => 4, 'AvatarFileExt' => 5, 'Iban' => 6, 'Currency' => 7, 'Balance' => 8, 'IsSavings' => 9, 'SavingsGoal' => 10, 'IsActive' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'originalId' => 1, 'bankLedgerId' => 2, 'name' => 3, 'hasAvatar' => 4, 'avatarFileExt' => 5, 'iban' => 6, 'currency' => 7, 'balance' => 8, 'isSavings' => 9, 'savingsGoal' => 10, 'isActive' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(BankAccountTableMap::COL_ID => 0, BankAccountTableMap::COL_ORIGINAL_ID => 1, BankAccountTableMap::COL_BANK_LEDGER_ID => 2, BankAccountTableMap::COL_NAME => 3, BankAccountTableMap::COL_HAS_AVATAR => 4, BankAccountTableMap::COL_AVATAR_FILE_EXT => 5, BankAccountTableMap::COL_IBAN => 6, BankAccountTableMap::COL_CURRENCY => 7, BankAccountTableMap::COL_BALANCE => 8, BankAccountTableMap::COL_IS_SAVINGS => 9, BankAccountTableMap::COL_SAVINGS_GOAL => 10, BankAccountTableMap::COL_IS_ACTIVE => 11, BankAccountTableMap::COL_CREATED_AT => 12, BankAccountTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'original_id' => 1, 'bank_ledger_id' => 2, 'name' => 3, 'has_avatar' => 4, 'avatar_file_ext' => 5, 'iban' => 6, 'currency' => 7, 'balance' => 8, 'is_savings' => 9, 'savings_goal' => 10, 'is_active' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bank_account');
        $this->setPhpName('BankAccount');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Finance\\BankAccount');
        $this->setPackage('Model.Finance');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('original_id', 'OriginalId', 'VARCHAR', true, 255, null);
        $this->addForeignKey('bank_ledger_id', 'BankLedgerId', 'INTEGER', 'bank_ledger', 'id', false, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('has_avatar', 'HasAvatar', 'INTEGER', true, null, null);
        $this->addColumn('avatar_file_ext', 'AvatarFileExt', 'VARCHAR', false, 10, null);
        $this->addColumn('iban', 'Iban', 'VARCHAR', true, 255, null);
        $this->addColumn('currency', 'Currency', 'VARCHAR', true, 255, null);
        $this->addColumn('balance', 'Balance', 'FLOAT', false, null, null);
        $this->addColumn('is_savings', 'IsSavings', 'BOOLEAN', false, 1, false);
        $this->addColumn('savings_goal', 'SavingsGoal', 'FLOAT', false, null, null);
        $this->addColumn('is_active', 'IsActive', 'BOOLEAN', false, 1, false);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BankLedger', '\\Model\\Finance\\BankLedger', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':bank_ledger_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('BankTransaction', '\\Model\\Finance\\BankTransaction', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':bank_account_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'BankTransactions', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BankAccountTableMap::CLASS_DEFAULT : BankAccountTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BankAccount object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BankAccountTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BankAccountTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BankAccountTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BankAccountTableMap::OM_CLASS;
            /** @var BankAccount $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BankAccountTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BankAccountTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BankAccountTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BankAccount $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BankAccountTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BankAccountTableMap::COL_ID);
            $criteria->addSelectColumn(BankAccountTableMap::COL_ORIGINAL_ID);
            $criteria->addSelectColumn(BankAccountTableMap::COL_BANK_LEDGER_ID);
            $criteria->addSelectColumn(BankAccountTableMap::COL_NAME);
            $criteria->addSelectColumn(BankAccountTableMap::COL_HAS_AVATAR);
            $criteria->addSelectColumn(BankAccountTableMap::COL_AVATAR_FILE_EXT);
            $criteria->addSelectColumn(BankAccountTableMap::COL_IBAN);
            $criteria->addSelectColumn(BankAccountTableMap::COL_CURRENCY);
            $criteria->addSelectColumn(BankAccountTableMap::COL_BALANCE);
            $criteria->addSelectColumn(BankAccountTableMap::COL_IS_SAVINGS);
            $criteria->addSelectColumn(BankAccountTableMap::COL_SAVINGS_GOAL);
            $criteria->addSelectColumn(BankAccountTableMap::COL_IS_ACTIVE);
            $criteria->addSelectColumn(BankAccountTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(BankAccountTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.original_id');
            $criteria->addSelectColumn($alias . '.bank_ledger_id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.has_avatar');
            $criteria->addSelectColumn($alias . '.avatar_file_ext');
            $criteria->addSelectColumn($alias . '.iban');
            $criteria->addSelectColumn($alias . '.currency');
            $criteria->addSelectColumn($alias . '.balance');
            $criteria->addSelectColumn($alias . '.is_savings');
            $criteria->addSelectColumn($alias . '.savings_goal');
            $criteria->addSelectColumn($alias . '.is_active');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BankAccountTableMap::DATABASE_NAME)->getTable(BankAccountTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BankAccountTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BankAccountTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BankAccountTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BankAccount or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BankAccount object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankAccountTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Finance\BankAccount) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BankAccountTableMap::DATABASE_NAME);
            $criteria->add(BankAccountTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BankAccountQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BankAccountTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BankAccountTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the bank_account table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BankAccountQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BankAccount or Criteria object.
     *
     * @param mixed               $criteria Criteria or BankAccount object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankAccountTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BankAccount object
        }

        if ($criteria->containsKey(BankAccountTableMap::COL_ID) && $criteria->keyContainsValue(BankAccountTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BankAccountTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BankAccountQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BankAccountTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BankAccountTableMap::buildTableMap();
