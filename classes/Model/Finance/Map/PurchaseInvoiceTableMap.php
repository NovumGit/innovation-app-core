<?php

namespace Model\Finance\Map;

use Model\Finance\PurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'purchase_invoice' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PurchaseInvoiceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Finance.Map.PurchaseInvoiceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'purchase_invoice';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Finance\\PurchaseInvoice';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Finance.PurchaseInvoice';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'purchase_invoice.id';

    /**
     * the column name for the remote_invoice_id field
     */
    const COL_REMOTE_INVOICE_ID = 'purchase_invoice.remote_invoice_id';

    /**
     * the column name for the supplier_id field
     */
    const COL_SUPPLIER_ID = 'purchase_invoice.supplier_id';

    /**
     * the column name for the invoice_date field
     */
    const COL_INVOICE_DATE = 'purchase_invoice.invoice_date';

    /**
     * the column name for the due_date field
     */
    const COL_DUE_DATE = 'purchase_invoice.due_date';

    /**
     * the column name for the pay_date field
     */
    const COL_PAY_DATE = 'purchase_invoice.pay_date';

    /**
     * the column name for the is_paid field
     */
    const COL_IS_PAID = 'purchase_invoice.is_paid';

    /**
     * the column name for the total_payable_amount field
     */
    const COL_TOTAL_PAYABLE_AMOUNT = 'purchase_invoice.total_payable_amount';

    /**
     * the column name for the has_document field
     */
    const COL_HAS_DOCUMENT = 'purchase_invoice.has_document';

    /**
     * the column name for the is_booked field
     */
    const COL_IS_BOOKED = 'purchase_invoice.is_booked';

    /**
     * the column name for the needs_manual_booking field
     */
    const COL_NEEDS_MANUAL_BOOKING = 'purchase_invoice.needs_manual_booking';

    /**
     * the column name for the file_path field
     */
    const COL_FILE_PATH = 'purchase_invoice.file_path';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'purchase_invoice.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'purchase_invoice.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'RemoteInvoiceId', 'SupplierId', 'InvoiceDate', 'DueDate', 'PayDate', 'IsPaid', 'TotalPayableAmount', 'HasDocument', 'IsBooked', 'NeedsManualBooking', 'FilePath', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'remoteInvoiceId', 'supplierId', 'invoiceDate', 'dueDate', 'payDate', 'isPaid', 'totalPayableAmount', 'hasDocument', 'isBooked', 'needsManualBooking', 'filePath', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PurchaseInvoiceTableMap::COL_ID, PurchaseInvoiceTableMap::COL_REMOTE_INVOICE_ID, PurchaseInvoiceTableMap::COL_SUPPLIER_ID, PurchaseInvoiceTableMap::COL_INVOICE_DATE, PurchaseInvoiceTableMap::COL_DUE_DATE, PurchaseInvoiceTableMap::COL_PAY_DATE, PurchaseInvoiceTableMap::COL_IS_PAID, PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, PurchaseInvoiceTableMap::COL_HAS_DOCUMENT, PurchaseInvoiceTableMap::COL_IS_BOOKED, PurchaseInvoiceTableMap::COL_NEEDS_MANUAL_BOOKING, PurchaseInvoiceTableMap::COL_FILE_PATH, PurchaseInvoiceTableMap::COL_CREATED_AT, PurchaseInvoiceTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'remote_invoice_id', 'supplier_id', 'invoice_date', 'due_date', 'pay_date', 'is_paid', 'total_payable_amount', 'has_document', 'is_booked', 'needs_manual_booking', 'file_path', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'RemoteInvoiceId' => 1, 'SupplierId' => 2, 'InvoiceDate' => 3, 'DueDate' => 4, 'PayDate' => 5, 'IsPaid' => 6, 'TotalPayableAmount' => 7, 'HasDocument' => 8, 'IsBooked' => 9, 'NeedsManualBooking' => 10, 'FilePath' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'remoteInvoiceId' => 1, 'supplierId' => 2, 'invoiceDate' => 3, 'dueDate' => 4, 'payDate' => 5, 'isPaid' => 6, 'totalPayableAmount' => 7, 'hasDocument' => 8, 'isBooked' => 9, 'needsManualBooking' => 10, 'filePath' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(PurchaseInvoiceTableMap::COL_ID => 0, PurchaseInvoiceTableMap::COL_REMOTE_INVOICE_ID => 1, PurchaseInvoiceTableMap::COL_SUPPLIER_ID => 2, PurchaseInvoiceTableMap::COL_INVOICE_DATE => 3, PurchaseInvoiceTableMap::COL_DUE_DATE => 4, PurchaseInvoiceTableMap::COL_PAY_DATE => 5, PurchaseInvoiceTableMap::COL_IS_PAID => 6, PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT => 7, PurchaseInvoiceTableMap::COL_HAS_DOCUMENT => 8, PurchaseInvoiceTableMap::COL_IS_BOOKED => 9, PurchaseInvoiceTableMap::COL_NEEDS_MANUAL_BOOKING => 10, PurchaseInvoiceTableMap::COL_FILE_PATH => 11, PurchaseInvoiceTableMap::COL_CREATED_AT => 12, PurchaseInvoiceTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'remote_invoice_id' => 1, 'supplier_id' => 2, 'invoice_date' => 3, 'due_date' => 4, 'pay_date' => 5, 'is_paid' => 6, 'total_payable_amount' => 7, 'has_document' => 8, 'is_booked' => 9, 'needs_manual_booking' => 10, 'file_path' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('purchase_invoice');
        $this->setPhpName('PurchaseInvoice');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Finance\\PurchaseInvoice');
        $this->setPackage('Model.Finance');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('remote_invoice_id', 'RemoteInvoiceId', 'VARCHAR', false, 255, null);
        $this->addForeignKey('supplier_id', 'SupplierId', 'INTEGER', 'supplier', 'id', false, null, null);
        $this->addColumn('invoice_date', 'InvoiceDate', 'DATE', false, null, null);
        $this->addColumn('due_date', 'DueDate', 'DATE', false, null, null);
        $this->addColumn('pay_date', 'PayDate', 'DATE', false, null, null);
        $this->addColumn('is_paid', 'IsPaid', 'BOOLEAN', false, 1, false);
        $this->addColumn('total_payable_amount', 'TotalPayableAmount', 'DECIMAL', false, 8, null);
        $this->addColumn('has_document', 'HasDocument', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_booked', 'IsBooked', 'BOOLEAN', false, 1, false);
        $this->addColumn('needs_manual_booking', 'NeedsManualBooking', 'BOOLEAN', false, 1, false);
        $this->addColumn('file_path', 'FilePath', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Supplier', '\\Model\\Supplier\\Supplier', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':supplier_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PurchaseInvoiceTableMap::CLASS_DEFAULT : PurchaseInvoiceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PurchaseInvoice object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PurchaseInvoiceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PurchaseInvoiceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PurchaseInvoiceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PurchaseInvoiceTableMap::OM_CLASS;
            /** @var PurchaseInvoice $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PurchaseInvoiceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PurchaseInvoiceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PurchaseInvoiceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PurchaseInvoice $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PurchaseInvoiceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_ID);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_REMOTE_INVOICE_ID);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_SUPPLIER_ID);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_INVOICE_DATE);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_DUE_DATE);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_PAY_DATE);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_IS_PAID);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_HAS_DOCUMENT);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_IS_BOOKED);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_NEEDS_MANUAL_BOOKING);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_FILE_PATH);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PurchaseInvoiceTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.remote_invoice_id');
            $criteria->addSelectColumn($alias . '.supplier_id');
            $criteria->addSelectColumn($alias . '.invoice_date');
            $criteria->addSelectColumn($alias . '.due_date');
            $criteria->addSelectColumn($alias . '.pay_date');
            $criteria->addSelectColumn($alias . '.is_paid');
            $criteria->addSelectColumn($alias . '.total_payable_amount');
            $criteria->addSelectColumn($alias . '.has_document');
            $criteria->addSelectColumn($alias . '.is_booked');
            $criteria->addSelectColumn($alias . '.needs_manual_booking');
            $criteria->addSelectColumn($alias . '.file_path');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PurchaseInvoiceTableMap::DATABASE_NAME)->getTable(PurchaseInvoiceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PurchaseInvoiceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PurchaseInvoiceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PurchaseInvoiceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PurchaseInvoice or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PurchaseInvoice object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PurchaseInvoiceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Finance\PurchaseInvoice) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PurchaseInvoiceTableMap::DATABASE_NAME);
            $criteria->add(PurchaseInvoiceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PurchaseInvoiceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PurchaseInvoiceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PurchaseInvoiceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the purchase_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PurchaseInvoiceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PurchaseInvoice or Criteria object.
     *
     * @param mixed               $criteria Criteria or PurchaseInvoice object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PurchaseInvoiceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PurchaseInvoice object
        }

        if ($criteria->containsKey(PurchaseInvoiceTableMap::COL_ID) && $criteria->keyContainsValue(PurchaseInvoiceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PurchaseInvoiceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PurchaseInvoiceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PurchaseInvoiceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PurchaseInvoiceTableMap::buildTableMap();
