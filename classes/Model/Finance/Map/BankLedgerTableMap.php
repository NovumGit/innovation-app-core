<?php

namespace Model\Finance\Map;

use Model\Finance\BankLedger;
use Model\Finance\BankLedgerQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'bank_ledger' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class BankLedgerTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Finance.Map.BankLedgerTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'bank_ledger';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Finance\\BankLedger';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Finance.BankLedger';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'bank_ledger.id';

    /**
     * the column name for the external_id field
     */
    const COL_EXTERNAL_ID = 'bank_ledger.external_id';

    /**
     * the column name for the ledger_origin field
     */
    const COL_LEDGER_ORIGIN = 'bank_ledger.ledger_origin';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'bank_ledger.description';

    /**
     * the column name for the asset_liability field
     */
    const COL_ASSET_LIABILITY = 'bank_ledger.asset_liability';

    /**
     * the column name for the kind field
     */
    const COL_KIND = 'bank_ledger.kind';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'bank_ledger.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'bank_ledger.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the asset_liability field */
    const COL_ASSET_LIABILITY_ASSETS = 'Assets';
    const COL_ASSET_LIABILITY_LIABILITIES = 'Liabilities';
    const COL_ASSET_LIABILITY_EQUITY = 'Equity';
    const COL_ASSET_LIABILITY_EXPENSES = 'Expenses';
    const COL_ASSET_LIABILITY_INCOME = 'Income';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ExternalId', 'LedgerOrigin', 'Description', 'AssetLiability', 'Kind', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'externalId', 'ledgerOrigin', 'description', 'assetLiability', 'kind', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(BankLedgerTableMap::COL_ID, BankLedgerTableMap::COL_EXTERNAL_ID, BankLedgerTableMap::COL_LEDGER_ORIGIN, BankLedgerTableMap::COL_DESCRIPTION, BankLedgerTableMap::COL_ASSET_LIABILITY, BankLedgerTableMap::COL_KIND, BankLedgerTableMap::COL_CREATED_AT, BankLedgerTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'external_id', 'ledger_origin', 'description', 'asset_liability', 'kind', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ExternalId' => 1, 'LedgerOrigin' => 2, 'Description' => 3, 'AssetLiability' => 4, 'Kind' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'externalId' => 1, 'ledgerOrigin' => 2, 'description' => 3, 'assetLiability' => 4, 'kind' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(BankLedgerTableMap::COL_ID => 0, BankLedgerTableMap::COL_EXTERNAL_ID => 1, BankLedgerTableMap::COL_LEDGER_ORIGIN => 2, BankLedgerTableMap::COL_DESCRIPTION => 3, BankLedgerTableMap::COL_ASSET_LIABILITY => 4, BankLedgerTableMap::COL_KIND => 5, BankLedgerTableMap::COL_CREATED_AT => 6, BankLedgerTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'external_id' => 1, 'ledger_origin' => 2, 'description' => 3, 'asset_liability' => 4, 'kind' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                BankLedgerTableMap::COL_ASSET_LIABILITY => array(
                            self::COL_ASSET_LIABILITY_ASSETS,
            self::COL_ASSET_LIABILITY_LIABILITIES,
            self::COL_ASSET_LIABILITY_EQUITY,
            self::COL_ASSET_LIABILITY_EXPENSES,
            self::COL_ASSET_LIABILITY_INCOME,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bank_ledger');
        $this->setPhpName('BankLedger');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Finance\\BankLedger');
        $this->setPackage('Model.Finance');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('external_id', 'ExternalId', 'VARCHAR', false, 255, null);
        $this->addColumn('ledger_origin', 'LedgerOrigin', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('asset_liability', 'AssetLiability', 'ENUM', false, null, null);
        $this->getColumn('asset_liability')->setValueSet(array (
  0 => 'Assets',
  1 => 'Liabilities',
  2 => 'Equity',
  3 => 'Expenses',
  4 => 'Income',
));
        $this->addColumn('kind', 'Kind', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BankAccount', '\\Model\\Finance\\BankAccount', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':bank_ledger_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'BankAccounts', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BankLedgerTableMap::CLASS_DEFAULT : BankLedgerTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BankLedger object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BankLedgerTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BankLedgerTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BankLedgerTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BankLedgerTableMap::OM_CLASS;
            /** @var BankLedger $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BankLedgerTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BankLedgerTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BankLedgerTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BankLedger $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BankLedgerTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BankLedgerTableMap::COL_ID);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_EXTERNAL_ID);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_LEDGER_ORIGIN);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_ASSET_LIABILITY);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_KIND);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(BankLedgerTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.external_id');
            $criteria->addSelectColumn($alias . '.ledger_origin');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.asset_liability');
            $criteria->addSelectColumn($alias . '.kind');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BankLedgerTableMap::DATABASE_NAME)->getTable(BankLedgerTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BankLedgerTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BankLedgerTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BankLedgerTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BankLedger or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BankLedger object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankLedgerTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Finance\BankLedger) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BankLedgerTableMap::DATABASE_NAME);
            $criteria->add(BankLedgerTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BankLedgerQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BankLedgerTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BankLedgerTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the bank_ledger table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BankLedgerQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BankLedger or Criteria object.
     *
     * @param mixed               $criteria Criteria or BankLedger object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankLedgerTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BankLedger object
        }

        if ($criteria->containsKey(BankLedgerTableMap::COL_ID) && $criteria->keyContainsValue(BankLedgerTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BankLedgerTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BankLedgerQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BankLedgerTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BankLedgerTableMap::buildTableMap();
