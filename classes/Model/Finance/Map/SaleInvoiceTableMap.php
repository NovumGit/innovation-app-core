<?php

namespace Model\Finance\Map;

use Model\Finance\SaleInvoice;
use Model\Finance\SaleInvoiceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sale_invoice' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SaleInvoiceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Finance.Map.SaleInvoiceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sale_invoice';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Finance\\SaleInvoice';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Finance.SaleInvoice';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sale_invoice.id';

    /**
     * the column name for the invoice_number field
     */
    const COL_INVOICE_NUMBER = 'sale_invoice.invoice_number';

    /**
     * the column name for the external_id field
     */
    const COL_EXTERNAL_ID = 'sale_invoice.external_id';

    /**
     * the column name for the external_source field
     */
    const COL_EXTERNAL_SOURCE = 'sale_invoice.external_source';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'sale_invoice.customer_id';

    /**
     * the column name for the invoice_date field
     */
    const COL_INVOICE_DATE = 'sale_invoice.invoice_date';

    /**
     * the column name for the revenue_date field
     */
    const COL_REVENUE_DATE = 'sale_invoice.revenue_date';

    /**
     * the column name for the vat_date field
     */
    const COL_VAT_DATE = 'sale_invoice.vat_date';

    /**
     * the column name for the due_date field
     */
    const COL_DUE_DATE = 'sale_invoice.due_date';

    /**
     * the column name for the is_paid field
     */
    const COL_IS_PAID = 'sale_invoice.is_paid';

    /**
     * the column name for the has_document field
     */
    const COL_HAS_DOCUMENT = 'sale_invoice.has_document';

    /**
     * the column name for the document_ext field
     */
    const COL_DOCUMENT_EXT = 'sale_invoice.document_ext';

    /**
     * the column name for the total_payable_amount field
     */
    const COL_TOTAL_PAYABLE_AMOUNT = 'sale_invoice.total_payable_amount';

    /**
     * the column name for the outstanding_amount field
     */
    const COL_OUTSTANDING_AMOUNT = 'sale_invoice.outstanding_amount';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'sale_invoice.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'sale_invoice.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'InvoiceNumber', 'ExternalId', 'ExternalSource', 'CustomerId', 'InvoiceDate', 'RevenueDate', 'VatDate', 'DueDate', 'IsPaid', 'HasDocument', 'DocumentExt', 'TotalPayableAmount', 'OutstandingAmount', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'invoiceNumber', 'externalId', 'externalSource', 'customerId', 'invoiceDate', 'revenueDate', 'vatDate', 'dueDate', 'isPaid', 'hasDocument', 'documentExt', 'totalPayableAmount', 'outstandingAmount', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(SaleInvoiceTableMap::COL_ID, SaleInvoiceTableMap::COL_INVOICE_NUMBER, SaleInvoiceTableMap::COL_EXTERNAL_ID, SaleInvoiceTableMap::COL_EXTERNAL_SOURCE, SaleInvoiceTableMap::COL_CUSTOMER_ID, SaleInvoiceTableMap::COL_INVOICE_DATE, SaleInvoiceTableMap::COL_REVENUE_DATE, SaleInvoiceTableMap::COL_VAT_DATE, SaleInvoiceTableMap::COL_DUE_DATE, SaleInvoiceTableMap::COL_IS_PAID, SaleInvoiceTableMap::COL_HAS_DOCUMENT, SaleInvoiceTableMap::COL_DOCUMENT_EXT, SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT, SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT, SaleInvoiceTableMap::COL_CREATED_AT, SaleInvoiceTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'invoice_number', 'external_id', 'external_source', 'customer_id', 'invoice_date', 'revenue_date', 'vat_date', 'due_date', 'is_paid', 'has_document', 'document_ext', 'total_payable_amount', 'outstanding_amount', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'InvoiceNumber' => 1, 'ExternalId' => 2, 'ExternalSource' => 3, 'CustomerId' => 4, 'InvoiceDate' => 5, 'RevenueDate' => 6, 'VatDate' => 7, 'DueDate' => 8, 'IsPaid' => 9, 'HasDocument' => 10, 'DocumentExt' => 11, 'TotalPayableAmount' => 12, 'OutstandingAmount' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'invoiceNumber' => 1, 'externalId' => 2, 'externalSource' => 3, 'customerId' => 4, 'invoiceDate' => 5, 'revenueDate' => 6, 'vatDate' => 7, 'dueDate' => 8, 'isPaid' => 9, 'hasDocument' => 10, 'documentExt' => 11, 'totalPayableAmount' => 12, 'outstandingAmount' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(SaleInvoiceTableMap::COL_ID => 0, SaleInvoiceTableMap::COL_INVOICE_NUMBER => 1, SaleInvoiceTableMap::COL_EXTERNAL_ID => 2, SaleInvoiceTableMap::COL_EXTERNAL_SOURCE => 3, SaleInvoiceTableMap::COL_CUSTOMER_ID => 4, SaleInvoiceTableMap::COL_INVOICE_DATE => 5, SaleInvoiceTableMap::COL_REVENUE_DATE => 6, SaleInvoiceTableMap::COL_VAT_DATE => 7, SaleInvoiceTableMap::COL_DUE_DATE => 8, SaleInvoiceTableMap::COL_IS_PAID => 9, SaleInvoiceTableMap::COL_HAS_DOCUMENT => 10, SaleInvoiceTableMap::COL_DOCUMENT_EXT => 11, SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT => 12, SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT => 13, SaleInvoiceTableMap::COL_CREATED_AT => 14, SaleInvoiceTableMap::COL_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'invoice_number' => 1, 'external_id' => 2, 'external_source' => 3, 'customer_id' => 4, 'invoice_date' => 5, 'revenue_date' => 6, 'vat_date' => 7, 'due_date' => 8, 'is_paid' => 9, 'has_document' => 10, 'document_ext' => 11, 'total_payable_amount' => 12, 'outstanding_amount' => 13, 'created_at' => 14, 'updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sale_invoice');
        $this->setPhpName('SaleInvoice');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Finance\\SaleInvoice');
        $this->setPackage('Model.Finance');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('invoice_number', 'InvoiceNumber', 'VARCHAR', false, 50, null);
        $this->addColumn('external_id', 'ExternalId', 'VARCHAR', false, 50, null);
        $this->addColumn('external_source', 'ExternalSource', 'VARCHAR', false, 50, null);
        $this->addForeignKey('customer_id', 'CustomerId', 'INTEGER', 'customer', 'id', false, null, null);
        $this->addColumn('invoice_date', 'InvoiceDate', 'DATE', false, null, null);
        $this->addColumn('revenue_date', 'RevenueDate', 'DATE', false, null, null);
        $this->addColumn('vat_date', 'VatDate', 'DATE', false, null, null);
        $this->addColumn('due_date', 'DueDate', 'DATE', false, null, null);
        $this->addColumn('is_paid', 'IsPaid', 'BOOLEAN', true, 1, false);
        $this->addColumn('has_document', 'HasDocument', 'BOOLEAN', true, 1, false);
        $this->addColumn('document_ext', 'DocumentExt', 'VARCHAR', false, 5, null);
        $this->addColumn('total_payable_amount', 'TotalPayableAmount', 'DECIMAL', false, 8, null);
        $this->addColumn('outstanding_amount', 'OutstandingAmount', 'DECIMAL', false, 8, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SaleInvoiceTableMap::CLASS_DEFAULT : SaleInvoiceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SaleInvoice object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SaleInvoiceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SaleInvoiceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SaleInvoiceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SaleInvoiceTableMap::OM_CLASS;
            /** @var SaleInvoice $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SaleInvoiceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SaleInvoiceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SaleInvoiceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SaleInvoice $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SaleInvoiceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_ID);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_INVOICE_NUMBER);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_EXTERNAL_ID);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_EXTERNAL_SOURCE);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_INVOICE_DATE);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_REVENUE_DATE);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_VAT_DATE);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_DUE_DATE);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_IS_PAID);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_HAS_DOCUMENT);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_DOCUMENT_EXT);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_TOTAL_PAYABLE_AMOUNT);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_OUTSTANDING_AMOUNT);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(SaleInvoiceTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.invoice_number');
            $criteria->addSelectColumn($alias . '.external_id');
            $criteria->addSelectColumn($alias . '.external_source');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.invoice_date');
            $criteria->addSelectColumn($alias . '.revenue_date');
            $criteria->addSelectColumn($alias . '.vat_date');
            $criteria->addSelectColumn($alias . '.due_date');
            $criteria->addSelectColumn($alias . '.is_paid');
            $criteria->addSelectColumn($alias . '.has_document');
            $criteria->addSelectColumn($alias . '.document_ext');
            $criteria->addSelectColumn($alias . '.total_payable_amount');
            $criteria->addSelectColumn($alias . '.outstanding_amount');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SaleInvoiceTableMap::DATABASE_NAME)->getTable(SaleInvoiceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleInvoiceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SaleInvoiceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SaleInvoiceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SaleInvoice or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SaleInvoice object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Finance\SaleInvoice) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SaleInvoiceTableMap::DATABASE_NAME);
            $criteria->add(SaleInvoiceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SaleInvoiceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SaleInvoiceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SaleInvoiceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sale_invoice table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SaleInvoiceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SaleInvoice or Criteria object.
     *
     * @param mixed               $criteria Criteria or SaleInvoice object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleInvoiceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SaleInvoice object
        }

        if ($criteria->containsKey(SaleInvoiceTableMap::COL_ID) && $criteria->keyContainsValue(SaleInvoiceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SaleInvoiceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SaleInvoiceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SaleInvoiceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SaleInvoiceTableMap::buildTableMap();
