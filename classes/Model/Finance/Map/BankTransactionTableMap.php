<?php

namespace Model\Finance\Map;

use Model\Finance\BankTransaction;
use Model\Finance\BankTransactionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'bank_transaction' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class BankTransactionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Finance.Map.BankTransactionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'bank_transaction';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Finance\\BankTransaction';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Finance.BankTransaction';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'bank_transaction.id';

    /**
     * the column name for the original_id field
     */
    const COL_ORIGINAL_ID = 'bank_transaction.original_id';

    /**
     * the column name for the bank_account_id field
     */
    const COL_BANK_ACCOUNT_ID = 'bank_transaction.bank_account_id';

    /**
     * the column name for the bank_transaction_category_id field
     */
    const COL_BANK_TRANSACTION_CATEGORY_ID = 'bank_transaction.bank_transaction_category_id';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'bank_transaction.description';

    /**
     * the column name for the description_original field
     */
    const COL_DESCRIPTION_ORIGINAL = 'bank_transaction.description_original';

    /**
     * the column name for the counter_bank_iban_id field
     */
    const COL_COUNTER_BANK_IBAN_ID = 'bank_transaction.counter_bank_iban_id';

    /**
     * the column name for the transaction_type field
     */
    const COL_TRANSACTION_TYPE = 'bank_transaction.transaction_type';

    /**
     * the column name for the amount field
     */
    const COL_AMOUNT = 'bank_transaction.amount';

    /**
     * the column name for the transaction_time field
     */
    const COL_TRANSACTION_TIME = 'bank_transaction.transaction_time';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'bank_transaction.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'bank_transaction.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OriginalId', 'BankAccountId', 'BankTransactionCategoryId', 'Description', 'DescriptionOriginal', 'CounterBankIbanId', 'TransactionType', 'Amount', 'TransactionTime', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'originalId', 'bankAccountId', 'bankTransactionCategoryId', 'description', 'descriptionOriginal', 'counterBankIbanId', 'transactionType', 'amount', 'transactionTime', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(BankTransactionTableMap::COL_ID, BankTransactionTableMap::COL_ORIGINAL_ID, BankTransactionTableMap::COL_BANK_ACCOUNT_ID, BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID, BankTransactionTableMap::COL_DESCRIPTION, BankTransactionTableMap::COL_DESCRIPTION_ORIGINAL, BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID, BankTransactionTableMap::COL_TRANSACTION_TYPE, BankTransactionTableMap::COL_AMOUNT, BankTransactionTableMap::COL_TRANSACTION_TIME, BankTransactionTableMap::COL_CREATED_AT, BankTransactionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'original_id', 'bank_account_id', 'bank_transaction_category_id', 'description', 'description_original', 'counter_bank_iban_id', 'transaction_type', 'amount', 'transaction_time', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OriginalId' => 1, 'BankAccountId' => 2, 'BankTransactionCategoryId' => 3, 'Description' => 4, 'DescriptionOriginal' => 5, 'CounterBankIbanId' => 6, 'TransactionType' => 7, 'Amount' => 8, 'TransactionTime' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'originalId' => 1, 'bankAccountId' => 2, 'bankTransactionCategoryId' => 3, 'description' => 4, 'descriptionOriginal' => 5, 'counterBankIbanId' => 6, 'transactionType' => 7, 'amount' => 8, 'transactionTime' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(BankTransactionTableMap::COL_ID => 0, BankTransactionTableMap::COL_ORIGINAL_ID => 1, BankTransactionTableMap::COL_BANK_ACCOUNT_ID => 2, BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID => 3, BankTransactionTableMap::COL_DESCRIPTION => 4, BankTransactionTableMap::COL_DESCRIPTION_ORIGINAL => 5, BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID => 6, BankTransactionTableMap::COL_TRANSACTION_TYPE => 7, BankTransactionTableMap::COL_AMOUNT => 8, BankTransactionTableMap::COL_TRANSACTION_TIME => 9, BankTransactionTableMap::COL_CREATED_AT => 10, BankTransactionTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'original_id' => 1, 'bank_account_id' => 2, 'bank_transaction_category_id' => 3, 'description' => 4, 'description_original' => 5, 'counter_bank_iban_id' => 6, 'transaction_type' => 7, 'amount' => 8, 'transaction_time' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bank_transaction');
        $this->setPhpName('BankTransaction');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Finance\\BankTransaction');
        $this->setPackage('Model.Finance');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('original_id', 'OriginalId', 'VARCHAR', true, 255, null);
        $this->addForeignKey('bank_account_id', 'BankAccountId', 'INTEGER', 'bank_account', 'id', true, null, null);
        $this->addColumn('bank_transaction_category_id', 'BankTransactionCategoryId', 'INTEGER', false, null, null);
        $this->addColumn('description', 'Description', 'VARCHAR', true, 255, null);
        $this->addColumn('description_original', 'DescriptionOriginal', 'VARCHAR', true, 255, null);
        $this->addForeignKey('counter_bank_iban_id', 'CounterBankIbanId', 'INTEGER', 'bank_iban_address_book', 'id', false, null, null);
        $this->addColumn('transaction_type', 'TransactionType', 'VARCHAR', true, 255, null);
        $this->addColumn('amount', 'Amount', 'FLOAT', false, null, null);
        $this->addColumn('transaction_time', 'TransactionTime', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BankIbanAddressBook', '\\Model\\Finance\\BankIbanAddressBook', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':counter_bank_iban_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('BankAccount', '\\Model\\Finance\\BankAccount', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':bank_account_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BankTransactionTableMap::CLASS_DEFAULT : BankTransactionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BankTransaction object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BankTransactionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BankTransactionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BankTransactionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BankTransactionTableMap::OM_CLASS;
            /** @var BankTransaction $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BankTransactionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BankTransactionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BankTransactionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BankTransaction $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BankTransactionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BankTransactionTableMap::COL_ID);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_ORIGINAL_ID);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_BANK_ACCOUNT_ID);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_BANK_TRANSACTION_CATEGORY_ID);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_DESCRIPTION_ORIGINAL);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_COUNTER_BANK_IBAN_ID);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_TRANSACTION_TYPE);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_AMOUNT);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_TRANSACTION_TIME);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(BankTransactionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.original_id');
            $criteria->addSelectColumn($alias . '.bank_account_id');
            $criteria->addSelectColumn($alias . '.bank_transaction_category_id');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.description_original');
            $criteria->addSelectColumn($alias . '.counter_bank_iban_id');
            $criteria->addSelectColumn($alias . '.transaction_type');
            $criteria->addSelectColumn($alias . '.amount');
            $criteria->addSelectColumn($alias . '.transaction_time');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BankTransactionTableMap::DATABASE_NAME)->getTable(BankTransactionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BankTransactionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BankTransactionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BankTransactionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BankTransaction or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BankTransaction object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Finance\BankTransaction) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BankTransactionTableMap::DATABASE_NAME);
            $criteria->add(BankTransactionTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = BankTransactionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BankTransactionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BankTransactionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the bank_transaction table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BankTransactionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BankTransaction or Criteria object.
     *
     * @param mixed               $criteria Criteria or BankTransaction object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BankTransactionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BankTransaction object
        }

        if ($criteria->containsKey(BankTransactionTableMap::COL_ID) && $criteria->keyContainsValue(BankTransactionTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BankTransactionTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = BankTransactionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BankTransactionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BankTransactionTableMap::buildTableMap();
