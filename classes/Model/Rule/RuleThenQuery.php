<?php

namespace Model\Rule;

use Model\Rule\Base\RuleThenQuery as BaseRuleThenQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'rule_then' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RuleThenQuery extends BaseRuleThenQuery
{

}
