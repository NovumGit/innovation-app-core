<?php

namespace Model\Rule;

use Model\Rule\Base\RuleWhenQuery as BaseRuleWhenQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'rule_when' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RuleWhenQuery extends BaseRuleWhenQuery
{

}
