<?php

namespace Model\Rule;

use Model\Rule\Base\RuleWhen as BaseRuleWhen;

/**
 * Skeleton subclass for representing a row from the 'rule_when' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RuleWhen extends BaseRuleWhen
{

}
