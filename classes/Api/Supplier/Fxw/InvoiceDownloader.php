<?php

namespace Api\Supplier\Fxw;

use Api\Supplier\AbstractInvoiceDownloader;
use Core\Cfg;
use DateTime;
use Exception;

class InvoiceDownloader extends AbstractInvoiceDownloader
{
    public function login()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://cp.flexwebhosting.nl/login/checkLogin'); //login URL
        curl_setopt($ch, CURLOPT_POST, 1);
        $post_array = Cfg::get('FXW');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_exec($ch);
        return $ch;
    }

    /**
     * @param string $sFromDate
     * @param string $sToDate
     * @return array
     * @throws Exception
     */
    public function getInvoiceList(string $sFromDate = '2018-10-01', string $sToDate = '2018-12-31'): array
    {
        $sFrom = str_replace('-', '', $sFromDate);
        $sTo = str_replace('-', '', $sToDate);

        $aMonthMap = [
            'January'   => '01',
            'February'  => '02',
            'March'     => '03',
            'April'     => '04',
            'May'       => '05',
            'June'      => '06',
            'July'      => '07',
            'August'    => '08',
            'September' => '09',
            'October'   => '10',
            'November'  => '11',
            'December'  => '12',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://cp.flexwebhosting.nl/invoices'); //login URL
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $sInvoicesPage = curl_exec($ch);
        $sInvoicesPage = str_replace('&euro;', '€', $sInvoicesPage);

        $pattern = '/https:\/\/cp\.flexwebhosting\.nl\/invoices\/downloadInvoice\/factuur_([0-9]+)\.pdf/';
        $aMatches = [];
        preg_match_all($pattern, $sInvoicesPage, $aMatches);

        $pattern = '/[0-9]{1,2} [A-Z]{1}[a-z]+ 20[0-9]{2}/';
        $aDateMatches = [];
        preg_match_all($pattern, $sInvoicesPage, $aDateMatches);

        $pattern = '/€ ([0-9]+,[0-9]{2})/';
        $aPriceMatches = [];
        preg_match_all($pattern, $sInvoicesPage, $aPriceMatches);

        $pattern = '/Niet betaald|Voldaan/';
        $aPaidMatches = [];
        preg_match_all($pattern, $sInvoicesPage, $aPaidMatches);

        $aInvoiceDatesTs = [];
        $aPayDatesTs = [];
        $aPayBeforeTs = [];
        $bIsOdd = true;
        $c = 0;
        foreach ($aDateMatches[0] as $iId => $sDate) {
            [
                $iDay,
                $sMonth,
                $iYear,
            ] = explode(' ', $sDate);
            $iMonth = $aMonthMap[$sMonth];

            if ($bIsOdd) {
                $bIsOdd = false;
                $aInvoiceDatesTs[$c] = mktime(0, 0, 0, $iMonth, $iDay, $iYear);
                $aPayBeforeTs[$c] = mktime(0, 0, 0, $iMonth, $iDay + 14, $iYear);
            } else {
                $bIsOdd = true;
                $aPayDatesTs[$c] = mktime(0, 0, 0, $iMonth, $iDay, $iYear);
                $c++;
            }
        }
        $aMatchedInvoices = [];

        $c = 0;
        foreach ($aMatches[1] as $iMatchId => $iInvoiceId) {
            $sWorkDate = date('Ymd', $aInvoiceDatesTs[$c]);

            if ($sWorkDate >= $sFrom && $sWorkDate <= $sTo) {
                $aMatchedInvoices[] = [
                    'invoice_id'      => $iInvoiceId,
                    'invoice_date'    => new DateTime('@' . $aInvoiceDatesTs[$c]),
                    'pay_before_date' => new DateTime('@' . $aPayBeforeTs[$c]),
                    'payed_date'      => $aPaidMatches[0][$iMatchId] == 'Voldaan' ? new DateTime('@' . $aPayDatesTs[$c]) : null,
                    'price'           => $aPriceMatches[1][$iMatchId],
                    'url'             => $aMatches[0][$iMatchId],
                    'paid'            => $aPaidMatches[0][$iMatchId] == 'Voldaan',
                ];
            }
            $c++;
        }
        return $aMatchedInvoices;
    }

    public function download(string $sUrl): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl); //login URL
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return curl_exec($ch);
    }

    public function getAllInvoiceUrls(): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://cp.flexwebhosting.nl/invoices'); //login URL
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $invoicesPage = curl_exec($ch);

        $pattern = '/https:\/\/cp\.flexwebhosting\.nl\/invoices\/downloadInvoice\/factuur_([0-9]+)\.pdf/';

        $aMatches = [];
        preg_match_all($pattern, $invoicesPage, $aMatches);

        return $aMatches[0];
    }
}
