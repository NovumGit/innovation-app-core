<?php

namespace Api\Supplier\EasyHosting\Task;

use Api\AbstractTask;
use Api\Supplier\EasyHosting\InvoiceDownloader;
use Core\Config;
use Core\Setting;
use Core\Translate;
use Core\Utils;
use Model\Finance\PurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;
use Propel\Runtime\Exception\PropelException;

class FetchInvoices extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Download facturen van EasyHosting.");
    }

    /**
     * @return bool
     * @throws PropelException
     */
    public function execute(): bool
    {
        $bFromStart = true;
        $oInvoiceDownloader = new InvoiceDownloader();
        $oInvoiceDownloader->login();

        if ($bFromStart) {
            $sFromDate = '2010-01-01';
        } else {
            $sLastPull = Setting::get('last_easyhosting_invoice_pull', strtotime('2015-01-01'));
            $sFromDate = date('Y-m-d', $sLastPull);
        }
        $sToDate = date('Y-m-d', time());

        $this->output("Download invoices from $sFromDate untill $sToDate");

        $aInvoices = $oInvoiceDownloader->getInvoiceList($sFromDate, $sToDate);
        $this->output(count($aInvoices) . " invoices found");

        $sSupplierName = 'Easyhosting B.V.';
        foreach ($aInvoices as $aInvoice) {
            $oSupplierQuery = SupplierQuery::create();
            $oSupplier = $oSupplierQuery->findOneByName($sSupplierName);
            if (!$oSupplier instanceof Supplier) {
                $this->output('Create new supplier');
                $oSupplier = new Supplier();
                $oSupplier->setName($sSupplierName);
                $oSupplier->setNumber('EASY');
                $oSupplier->setEmail('support@easyhosting.nl');
                $oSupplier->save();
            }

            $oPurchaseInvoiceQuery = PurchaseInvoiceQuery::create();
            $oPurchaseInvoiceQuery->filterBySupplierId($oSupplier->getId());
            $oPurchaseInvoiceQuery->filterByRemoteInvoiceId($aInvoice['invoice_id']);

            $oPurchaseInvoice = $oPurchaseInvoiceQuery->findOne();

            if (!$oPurchaseInvoice instanceof PurchaseInvoice) {
                $oPurchaseInvoice = new PurchaseInvoice();
                $oPurchaseInvoice->setSupplierId($oSupplier->getId());
                $oPurchaseInvoice->setRemoteInvoiceId((int)trim($aInvoice['invoice_id']));
                $oPurchaseInvoice->setIsPaid(false);
            }

            $oPurchaseInvoice->setInvoiceDate($aInvoice['invoice_date']);
            $oPurchaseInvoice->setDueDate($aInvoice['invoice_date']);
            // $oPurchaseInvoice->setPayDate($aInvoice['payed_date']);

            $oPurchaseInvoice->setTotalPayableAmount(Utils::priceEuToUs($aInvoice['price']));
            $oPurchaseInvoice->setIsPaid($aInvoice['paid']);
            $oPurchaseInvoice->save();

            $this->output('Saved purchase invoice ' . $oPurchaseInvoice->getId());

            $sLocalFilePath = '/finance/purchase_invoice/easyhosting/' . date('Ymd') . '-' . $aInvoice['invoice_id'] . '.pdf';
            $sInvoiceDir = Config::getDataDir(true);
            $sPdfFilename = $sInvoiceDir . $sLocalFilePath;

            if (!file_exists($sPdfFilename)) {
                $this->output('Download ' . $aInvoice['url']);
                $sInvoiceBinary = $oInvoiceDownloader->download($aInvoice['url']);

                if (!is_dir(dirname($sPdfFilename))) {
                    mkdir(dirname($sPdfFilename), 0777, true);
                }

                $oPurchaseInvoice->setHasDocument(true);
                $oPurchaseInvoice->setFilePath($sPdfFilename);
                file_put_contents($sPdfFilename, $sInvoiceBinary);
                $oPurchaseInvoice->save();
            }
        }
        return true;
    }
}
