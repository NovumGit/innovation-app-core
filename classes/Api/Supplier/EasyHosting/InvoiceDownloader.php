<?php

namespace Api\Supplier\EasyHosting;

use Api\Supplier\AbstractInvoiceDownloader;

class InvoiceDownloader extends AbstractInvoiceDownloader
{
    public function login()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://login.easyhosting.nl/login.php'); //login URL
        curl_setopt($ch, CURLOPT_POST, 1);
        $post_array = [
            'action'    => 'login',
            'userName'  => '6469',
            'loginPass' => 'r5wsnY',
        ];

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_array);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        return $ch;
    }

    public function download(string $sUrl): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl); //login URL
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        return curl_exec($ch);
    }

    public function getInvoiceList(string $sFromDate = '2018-10-01', string $sToDate = '2018-12-31'): array
    {
        $sFrom = str_replace('-', '', $sFromDate);
        $sTo = str_replace('-', '', $sToDate);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://login.easyhosting.nl/php/invoices.php?invoiceLimit=100'); //login URL
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->getCookieFile('easyhosting-cookie.txt'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $sHtml = curl_exec($ch);

        $aMatchedInvoices = [];
        $aPriceMatches = $aMatches = $aInvoiceMatches = [];
        // <td onclick="document.location='invoice.content.php?id=330923';">10-09-2018</td>
        preg_match_all('/<td onclick=\"document\.location=\'invoice.content.php\?id=([0-9]+)\';">([0-9]+-[0-9]+-[0-9]{4})/', $sHtml, $aMatches);
        preg_match_all('/<td onclick=\"document\.location=\'invoice.content.php\?id=([0-9]+)\';">([0-9]{6})/', $sHtml, $aInvoiceMatches);

        preg_match_all('/<td onclick=\"document\.location=\'invoice.content.php\?id=([0-9]+)\';">([0-9]+,[0-9]{2})/', $sHtml, $aPriceMatches);

        $i = 0;
        foreach ($aMatches[2] as $iId => $sDate) {
            [
                $d,
                $m,
                $y,
            ] = explode('-', $sDate);

            $sWorkDate = $y . $m . $d;

            if ($sWorkDate >= $sFrom && $sWorkDate <= $sTo) {
                $InvoiceId = $aInvoiceMatches[2][$iId];

                $price = $aPriceMatches[2][$i];
                $aMatchedInvoices[] = [
                    'date'         => $sDate,
                    'invoice_date' => mktime(0, 0, 0, $m, $d, $y),
                    'price'        => $price,
                    'paid'         => true,
                    'invoice_id'   => $InvoiceId,
                    'url'          => "https://login.easyhosting.nl/php/invoice_$InvoiceId.pdf?type=PDF&action=SHOW&id=$InvoiceId",
                ];
            }

            $i = $i + 2;
        }
        return $aMatchedInvoices;
    }
}
