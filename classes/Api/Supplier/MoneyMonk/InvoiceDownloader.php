<?php

namespace Api\Supplier\MoneyMonk;

use Api\Accounting\MoneyMonk\MoneyMonkApi;
use Api\Supplier\AbstractInvoiceDownloader;
use Core\Utils;

class InvoiceDownloader extends AbstractInvoiceDownloader
{

    public function login()
    {
        MoneyMonkApi::login();
    }

    public function download(string $sUrl): string
    {
        return MoneyMonkApi::curlGetRaw($sUrl);
    }

    public function getInvoiceList(string $sFromDate = '2018-10-01', string $sToDate = '2018-12-31'): array
    {
        $sFrom = str_replace('-', '', $sFromDate);
        $sTo = str_replace('-', '', $sToDate);

        $sHtml = MoneyMonkApi::curlGetRaw('/abonnement-instellingen/facturen');

        $aDateMatches = $aInvoiceNumbers = $aAmounts = [];
        preg_match_all('/\<td\>([0-9]{2}-[0-9]{2}-20[0-9]{2})\<\/td\>/', $sHtml, $aDateMatches);
        preg_match_all('/\/abonnement-instellingen\/download-factuur\/[0-9]+/', $sHtml, $aUrlMatches);
        preg_match_all('/€ ([0-9]+,[0-9]+)/', $sHtml, $aAmounts);
        preg_match_all('/201[0-9.]{6,9}/', $sHtml, $aInvoiceNumbers);

        $aMatchedInvoices = [];
        foreach ($aDateMatches[1] as $iMatchId => $sDate) {
            [
                $d,
                $m,
                $y,
            ] = explode('-', $sDate);
            $sWorkDate = $y . $m . $d;

            //          echo $sWorkDate . ' >= ' . $sFrom . ' && ' . $sWorkDate . ' <= ' . $sTo . PHP_EOL;
            if ($sWorkDate >= $sFrom && $sWorkDate <= $sTo) {
                $sUrl = $aUrlMatches[0][$iMatchId];
                $iInvoiceId = array_reverse(explode('/', $sUrl))[0];

                $aMatchedInvoices[] = [
                    'invoice_id'     => $iInvoiceId,
                    'invoice_number' => $aInvoiceNumbers[0][$iMatchId],
                    'url'            => $sUrl,
                    'bedrag'         => Utils::priceEuToUs($aAmounts[1][$iMatchId]),
                    'invoice_date'   => mktime(0, 0, 0, $m, $d, $y),
                ];
            }
        }
        return $aMatchedInvoices;
    }
}
