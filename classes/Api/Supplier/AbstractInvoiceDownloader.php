<?php

namespace Api\Supplier;

use Core\Config;
use Exception\LogicException;

abstract class AbstractInvoiceDownloader
{
    abstract public function login();

    abstract public function getInvoiceList(string $sFromDate, string $sToDate): array;

    abstract public function download(string $sUrl): string;

    public function getCookieFile(string $sBaseName = 'fxw-cookie.txt')
    {
        $sDir = Config::getDataDir(true);
        $sCookieFile = $sDir . '/' . $sBaseName;
        if (!is_writable($sDir)) {
            throw new LogicException("Cookie dir is not writable");
        }
        if (!file_exists($sCookieFile)) {
            touch($sCookieFile);
        }
        if (!is_writable($sCookieFile)) {
            throw new LogicException("Cookie file is not writable");
        }
        return $sCookieFile;
    }
}
