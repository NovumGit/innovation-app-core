<?php

namespace Api;

use Psr\Log\AbstractLogger;

class TaskList
{
    private $aTasks;

    /**
     * TaskList constructor.
     * @param AbstractTask[] $aTasks
     */
    public function __construct(array $aTasks)
    {
        $oLogger = new class extends AbstractLogger {

            public function log($level, $message, array $context = [])
            {
                echo $message . '<br>';
            }
        };
        $this->aTasks = [];
        foreach ($aTasks as $sTaskName) {
            $this->aTasks[] = new $sTaskName($oLogger);
        }
    }

    /**
     * @return AbstractTask[]
     */
    public function getTaskObjects(): array
    {
        return $this->aTasks;
    }
}
