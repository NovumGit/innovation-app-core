<?php

namespace Api\Packagist;

use Hurah\Types\Type\Composer;
use Core\Json\JsonUtils;
use Core\Utils;
use Exception;
use GuzzleHttp\Client;
use Helper\Package\Components\IPackageInfo;
use Helper\Package\Components\PackageInfoFactory;
use Hi\Helpers\DirectoryStructure;

class Api
{


    public function getPackageDetails(string $sPackageName): IPackageInfo
    {
        $oCient = new Client(
            [
            // Base URI is used with relative requests
                'base_uri' => 'https://packagist.org',
            ]
        );

        $oResponse = $oCient->get("p/$sPackageName.json");
        $aBody     = [];
        if ($oResponse->getStatusCode() === 200) {
            $sBody = (string) $oResponse->getBody();
            $aBody = JsonUtils::decode($sBody);
        }

        /*
         * @todo we are returning just the first package we find, need to give the user a list of installable versions
         * instead.
         */
        return PackageInfoFactory::get(Composer::fromArray(current(current(current($aBody)))));
    }//end getPackageDetails()


    /**
     * @return array
     * @throws Exception
     */
    public function getInstallablePackages()
    {
        $aPackageTypes = [
            [
                'composer-type' => 'novum-domain',
                'type'          => 'domain',
            ],
            [
                'composer-type' => 'hurah-domain',
                'type'          => 'domain',
            ],

            [
                'composer-type' => 'novum-site',
                'type'          => 'site',
            ],
            [
                'composer-type' => 'hurah-site',
                'type'          => 'site',
            ],

            [
                'composer-type' => 'novum-api',
                'type'          => 'api',
            ],
            [
                'composer-type' => 'hurah-api',
                'type'          => 'api',
            ],
        ];

        $oCient = new Client(
            [
            // Base URI is used with relative requests
                'base_uri' => 'https://packagist.org',
            ]
        );

        $oDirectoryStructure = new DirectoryStructure();
        $sVendorDir          = $oDirectoryStructure->getVendorDir();
        $aOut                = [];
        foreach ($aPackageTypes as $aPackageType) {
            $aQuery    = [
                'type'     => $aPackageType['composer-type'],
                'per_page' => 100,
            ];
            $oResponse = $oCient->get('packages/list.json', ['query' => $aQuery]);
            if ($oResponse->getStatusCode() === 200) {
                $sBody = (string) $oResponse->getBody();
                $aBody = JsonUtils::decode($sBody);

                foreach ($aBody['packageNames'] as $sPackageName) {
                    $sPackageDir = Utils::makePath($sVendorDir, $sPackageName);

                    if (empty($sPackageName)) {
                        continue;
                    }

                    $aOut[] = [
                        'name'      => $sPackageName,
                        'type'      => $aPackageType['type'],
                        'url'       => "https://repo.packagist.org/p/{$sPackageName}.json",
                        'installed' => is_dir($sPackageDir),
                    ];
                }
            } else {
                throw new Exception('Got wrong statuscode ' . $oResponse->getStatusCode() . ' from packagist.');
            }//end if
        }//end foreach

        return $aOut;
    }//end getInstallablePackages()
}//end class
