<?php

namespace Api;

use Core\Config;
use DateTime;
use Exception;
use Model\Api\Task;
use Model\Api\TaskQuery;
use Propel\Runtime\Exception\PropelException;
use Psr\Log\LoggerInterface;

abstract class AbstractTask
{
    private $oTask;
    private $oLogger;

    public function output(string $string)
    {
        $this->getLogger()->info($string);
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->oLogger;
    }

    /**
     * AbstractTask constructor.
     * @param LoggerInterface $oLogger
     * @throws PropelException
     */
    public function __construct(LoggerInterface $oLogger)
    {
        $this->oLogger = $oLogger;
        $oTaskQuery = TaskQuery::create();
        $oTask = $oTaskQuery->findOneByExecutable(get_class($this));

        if (!$oTask instanceof Task) {
            $oTask = new Task();
            $oTask->setExecutable(get_class($this));
            $oTask->setRunCount(0);
            $oTask->setFinishCount(0);

            $oTask->save();
        }

        $this->oTask = $oTask;
    }

    private function getModel(): Task
    {
        return $this->oTask;
    }

    public function getId()
    {
        return $this->getModel()->getId();
    }

    public function getRunCount()
    {
        try {
            return $this->getModel()->getRunCount();
        } catch (Exception $e) {
            return 0;
        }
    }

    public function getFinishCount()
    {
        try {
            return $this->getModel()->getFinishCount();
        } catch (Exception $e) {
            return 0;
        }
    }

    /**
     * @return DateTime|string
     * @throws PropelException
     */
    public function getLastFinish()
    {
        return $this->getModel()->getLastFinish(Config::getDateTimeFormat());
    }

    /**
     * @return DateTime|string
     * @throws PropelException
     */
    public function getLastStart()
    {
        return $this->getModel()->getLastStart(Config::getDateTimeFormat());
    }

    abstract public function getDescription(): string;

    /**
     * @return bool return true on success else false
     */
    abstract public function execute(): bool;
}
