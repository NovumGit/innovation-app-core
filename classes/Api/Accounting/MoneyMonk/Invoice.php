<?php

namespace Api\Accounting\MoneyMonk;

class Invoice
{
    public static function search($iJournalId = 65636, $sStartDate = '2018-10-01', $sEndDate = '2018-12-31', $iOffset = 0, $iCount = 100): array
    {
        $aQuery = [
            'journal_id' => $iJournalId,
            'start_date' => $sStartDate,
            'end_date'   => $sEndDate,
            'offset'     => $iOffset,
            'count'      => $iCount,
        ];

        $sEndpoint = '/api/v1/administrations/12753/invoices/search';
        return MoneyMonkApi::curlGet($sEndpoint . '?' . http_build_query($aQuery));
    }

    public static function downloadPdf($iInvoiceId): string
    {
        $sUrl = "/api/v1/administrations/12753/invoices/$iInvoiceId/download";
        return MoneyMonkApi::curlGetRaw($sUrl);
    }
}
