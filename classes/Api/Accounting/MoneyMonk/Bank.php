<?php

namespace Api\Accounting\MoneyMonk;

class Bank
{
    /**
     * @param int $iJournalId ieder id heeft een uniek bankrekeningnummer
     * @param null $sSearchString
     * @param string $sStart_date
     * @param string $sEnd_date
     * @param int $iOffset
     * @param int $iCount
     *
     * @return array
     */

    public static function findBookings($iJournalId, $sSearchString = null, $sStart_date = '2018-01-01', $sEnd_date = '2018-12-31', $iOffset = 0, $iCount = 1000)
    {

        $aArguments = [
            'journal_id'    => $iJournalId,
            'search_string' => $sSearchString,
            'start_date'    => $sStart_date,
            'end_date'      => $sEnd_date,
            'offset'        => $iOffset,
            'count'         => $iCount,
        ];

        $sQuery = http_build_query($aArguments);
        return MoneyMonkApi::curlGet('/api/v1/administrations/12753/transactions?' . $sQuery);
    }
}
