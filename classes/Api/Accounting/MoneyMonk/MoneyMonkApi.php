<?php

namespace Api\Accounting\MoneyMonk;

use Core\Cfg;
use Core\Config;
use Exception\LogicException;

class MoneyMonkApi
{
    private static function getCookieFile()
    {
        $sDir = Config::getDataDir(true);
        if (!is_writable($sDir)) {
            throw new LogicException("Cookie dir $sDir is not writable");
        }
        if (!file_exists($sDir . '/moneymonk-cookie.txt')) {
            touch($sDir . '/moneymonk-cookie.txt');
        }
        if (!is_writable($sDir . '/moneymonk-cookie.txt')) {
            throw new LogicException("Cookie file is not writable");
        }
        return $sDir . '/moneymonk-cookie.txt';
    }

    public static function login($bForce = false): void
    {
        if ($bForce || filemtime(self::getCookieFile()) < time() - (60 * 5)) {
            file_exists(self::getCookieFile()) ? unlink(self::getCookieFile()) : null;
            touch(self::getCookieFile());
            self::curlPost('/api/v1/authentication/login', Cfg::get('MONEYMONK'));
        } else {
            // not logging in again, user should be already logged in.
        }
    }

    public static function curlPostRaw(string $sEndpoint, string $sPayload)
    {
        $sUrl = 'https://app.moneymonk.nl' . $sEndpoint;

        $ch = curl_init($sUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $sPayload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, self::getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, self::getCookieFile());
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json;charset=UTF-8',
            'Content-Length: ' . strlen($sPayload),
            'X-MOMO: MOMO',
        ]);
        return curl_exec($ch);
    }

    public static function curlPost(string $sEndpoint, array $aPayload): string
    {
        $sPayload = json_encode($aPayload);
        return self::curlPostRaw($sEndpoint, $sPayload);
    }

    public static function curlGetRaw($sEndpoint)
    {
        $ch = curl_init();
        $sUrl = 'https://app.moneymonk.nl' . $sEndpoint;
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, self::getCookieFile());
        curl_setopt($ch, CURLOPT_COOKIEFILE, self::getCookieFile());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return curl_exec($ch);
    }

    public static function curlGet($sEndpoint)
    {
        $sResult = self::curlGetRaw($sEndpoint);
        return json_decode($sResult, true);
    }
}
