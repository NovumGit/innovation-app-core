<?php

namespace Api\Accounting\MoneyMonk;

class Contact
{
    public static function getContacts(int $iJournalId, bool $bIsArchived = false, int $iOffset = 0, int $iCount = 100)
    {
        $aArguments = [
            'is_archived' => $bIsArchived ? '1' : '0',
            'offset'      => $iOffset,
            'count'       => $iCount,
        ];

        $sQuery = http_build_query($aArguments);
        return MoneyMonkApi::curlGet('/api/v1/administrations/' . $iJournalId . '/contacts?' . $sQuery);
    }
}
