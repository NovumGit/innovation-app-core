<?php

namespace Api\Accounting\MoneyMonk;

use Exception;

class Ledger
{
    public const HOSTING_SERVER_HUUR = 711103;
    public const PRIVE = 684361;
    public const VERZEKERING = 684412;
    public const ACCOUNTANT_ADMINISTRATIE = 684411;
    public const DOMEINREGISTRATIES = 711104;

    /**
     * @return array
     * @throws Exception
     */
    public static function getAll(): array
    {
        $aLedgers = MoneyMonkApi::curlGet('/api/v1/administrations/12753?with=general-ledger-accounts');

        if (isset($aLedgers['general_ledger_accounts'])) {
            return $aLedgers['general_ledger_accounts'];
        }
        throw new Exception("No Ledgers where returned by Moneymonk, is this a bug?");
    }

    public static function map($iLedgerAccountId, $sSearchString, $sFromDate, $sToDate)
    {
        $iPage = 0;

        while (true) {
            echo "Page $iPage " . "<br>";
            $aBankBookings = Bank::findBookings(65634, $sSearchString, $sFromDate, $sToDate, $iPage * 100, 100);

            if (empty($aBankBookings)) {
                break;
            }
            foreach ($aBankBookings as $aBankBooking) {
                echo "$sSearchString - Reconcile ledger code " . $iLedgerAccountId . ' to booking ' . $aBankBooking['id'] . "<br>";
                Ledger::reconcile($iLedgerAccountId, $aBankBooking['id']);
            }

            $iPage++;
        }
        echo "DONE <br>";
    }

    public static function reconcile(int $iLedgerAccountId, int $iTransactionId)
    {
        /*
        $aArguments = [
            'general_ledger_account_id' => $iLedgerAccountId,
            'transaction_id' => ,
            'type' => 'general-ledger-account'
        ];
            */
        $sPayload = '{type: "general-ledger-account", transaction_id: ' . $iTransactionId . ', general_ledger_account_id: ' . $iLedgerAccountId . '}';
        echo $sPayload . "<br>";
        echo MoneyMonkApi::curlPostRaw('/api/v1/administrations/12753/reconciliations', $sPayload);
    }
}
