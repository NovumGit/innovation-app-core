<?php

namespace Api\Accounting\MoneyMonk;

class File
{
    public static function getUnprocessed($iCount = 99, $iOffset = 0)
    {
        $sUrl = "/api/v1/administrations/12753/files?type=standard-file&status=unprocessed&count=$iCount&offset=$iOffset";
        return MoneyMonkApi::curlGet($sUrl);
    }

    public static function linkToTransaction($iFileId, $iTransactionId)
    {
        $aData = [
            'attachment' => [
                'type'                => 'bank-transaction-attachment',
                'bank_transaction_id' => $iTransactionId,
            ],
        ];

        MoneyMonkApi::curlPost("/api/v1/administrations/12753/files/$iFileId/change", $aData);
    }
}
