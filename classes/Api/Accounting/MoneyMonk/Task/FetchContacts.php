<?php

namespace Api\Accounting\MoneyMonk\Task;

use Api\AbstractTask;
use Api\Accounting\MoneyMonk\Contact;
use Api\Accounting\MoneyMonk\MoneyMonkApi;
use Core\Translate;
use Exception;
use Model\Crm\Customer;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressTypeQuery;
use Model\Crm\CustomerQuery;
use Model\Crm\CustomerTypeQuery;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\PayTerm;
use Model\Setting\MasterTable\PayTermQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;

class FetchContacts extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Haal alle klanten op.");
    }

    /**
     * @return bool
     * @throws PropelException
     */
    public function execute(): bool
    {
        ob_start();

        ini_set('display_errors', true);
        error_reporting(E_ALL);

        $this->output("Login");
        MoneyMonkApi::login();

        $this->output("get contacts");

        $iOffset = 0;
        $iCount = 10;
        while (true) {
            $aContacts = Contact::getContacts(12753, false, $iOffset, 10);
            $iOffset = $iOffset + $iCount;

            if (empty($aContacts)) {
                $this->output("all done");
                return true;
            }
            foreach ($aContacts as $aContact) {
                $oCustomerQuery = CustomerQuery::create();
                $oCustomerQuery->filterByImportSource('moneymonk');
                // should be filterbyaccountingid but online it is still old id
                // next time when i am coding here just replace and it should be good.
                $oCustomerQuery->filterByOldId($aContact['id']);
                $oCustomer = $oCustomerQuery->findOne();

                if (!$oCustomer instanceof Customer) {
                    $oCustomer = new Customer();
                    $oCustomer->setImportSource('moneymonk');
                    $oCustomer->setOldId($aContact['id']);
                }
                $oCustomer->setAccountingId($aContact['id']);

                $oCustomer->setDebitor($aContact['contact_number']);

                $sCode = $aContact['type'] == 'business' ? 'company' : 'person';
                $oCustomerType = CustomerTypeQuery::create()->findOneByCode($sCode);

                $oCustomer->setCustomerTypeId($oCustomerType->getId());
                echo 'Email = ' . $aContact['email_address'] . PHP_EOL;

                if (!empty($aContact['email_address'])) {
                    $oCustomerQuery = CustomerQuery::create();
                    $oCustomerQuery->filterByEmail($aContact['email_address']);
                    $oCustomerQuery->filterById($oCustomer->getId(), Criteria::NOT_EQUAL);
                    $oOtherCustomerWSameEmail = $oCustomerQuery->findOne();

                    if (!$oOtherCustomerWSameEmail instanceof Customer) {
                        $oCustomer->setEmail($aContact['email_address']);
                    }
                }

                $oCustomer->setPhone($aContact['phone_number']);

                if (isset($aContact['to_the_attention_of'])) {
                    if (strpos($aContact['to_the_attention_of'], ' ')) {
                        $aParts = explode(' ', $aContact['to_the_attention_of']);

                        $oCustomer->setFirstName($aParts[0]);
                        $oCustomer->setLastName(array_reverse($aParts)[0]);
                    } else {
                        $oCustomer->setFirstName($aContact['to_the_attention_of']);
                    }
                }

                // Further below we must have a customer id
                try {
                    $oCustomer->save();
                } catch (Exception $e) {
                    echo 'Could not save customer ' . PHP_EOL . $e->getMessage() . PHP_EOL;
                }

                if (isset($aContact['payment_profiles']['sales_invoices'])) {
                    $profile = $aContact['payment_profiles']['sales_invoices'];
                    $oPayTermQuery = PayTermQuery::create();
                    $oPayTerm = $oPayTermQuery->findOneByName($profile['name']);

                    if (!$oPayTerm instanceof PayTerm) {
                        $oPayTerm = new PayTerm();
                        $oPayTerm->setName($profile['name']);
                        $oPayTerm->setCode(strtoupper(str_replace(' ', '', $profile['name'])));
                        $oPayTerm->setTotalDays(explode(' ', $profile['name'])[0]);
                        $oPayTerm->setPeriod('DAY');
                        try {
                            $oPayTerm->save();
                        } catch (Exception $e) {
                            echo "Could not save Payterm: " . PHP_EOL;
                            echo $e->getMessage() . PHP_EOL;
                        }
                    }
                    $oCustomer->setPaytermId($oPayTerm->getId());
                }

                $aAddresses = [
                    'postal_address'   => 'invoice',
                    'visiting_address' => 'delivery',
                ];

                if (isset($aContact['addresses'])) {
                    foreach ($aAddresses as $sMoneyMonkTag => $sNuicartTag) {
                        $aAddress = $aContact['addresses'][$sMoneyMonkTag];
                        $oAddress = null;
                        if ($oCustomer->hasAddress($sNuicartTag)) {
                            if ($sNuicartTag == 'invoice') {
                                $oAddress = $oCustomer->getInvoiceAddress();
                            } elseif ($sNuicartTag == 'delivery') {
                                $oAddress = $oCustomer->getDeliveryAddress();
                            }


                            try {
                                $oType = $oAddress->getCustomerAddressType();
                            } catch (Exception $e) {
                                $this->output("Could not get Address type: ");
                                $this->output($e->getMessage());
                            }
                        } else {
                            $oAddress = new CustomerAddress();
                            $oType = CustomerAddressTypeQuery::create()->findOneByCode($sNuicartTag);
                        }

                        if (!empty($aAddress['street_name']) && !empty($aAddress['street_number'])) {
                            $oAddress->setCustomerId($oCustomer->getId());
                            $oAddress->setStreet($aAddress['street_name']);
                            $oAddress->setNumber($aAddress['street_number']);
                            $oAddress->setNumberAdd($aAddress['extension']);
                            $oAddress->setCustomerAddressTypeId($oType->getId());
                            $oAddress->setPostal($aAddress['postal_code']);
                            $oAddress->setCity($aAddress['city']);

                            if ($aAddress['city'] == 1) {
                                $oCountry = CountryQuery::create()->findOneByIso2('NL');
                                $oAddress->setCountryId($oCountry->getId());
                            }
                            try {
                                $oAddress->save();
                            } catch (Exception $e) {
                                $this->output("Could not save Address");
                                $this->output($e->getMessage());
                            }
                        }
                    }
                }

                if (isset($aContact['business_information'])) {
                    $oCustomer->setWebsite($aContact['business_information']['web_address']);
                    $oCompany = $oCustomer->getCompany();
                    $oCompany->setCompanyName($aContact['business_information']['company_name']);
                    $oCompany->setVatNumber($aContact['business_information']['vat_number']);
                    $oCompany->setChamberOfCommerce($aContact['business_information']['chamber_of_commerce_number']);
                    $oCompany->save();
                }
                $oCustomer->save();
                $this->output("Customer added / updated");
            }
        }
    }
}
