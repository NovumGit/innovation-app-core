<?php

namespace Api\Accounting\MoneyMonk\Task;

use Api\AbstractTask;
use Api\Accounting\MoneyMonk\Bank;
use Api\Accounting\MoneyMonk\Ledger;
use Api\Accounting\MoneyMonk\MoneyMonkApi;
use Core\Config;
use Core\Translate;
use Model\Finance\PurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery;
use Propel\Runtime\Exception\PropelException;

class PurchaseInvoiceReconciliation extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Boek inkoop facturen in MoneyMonk");
    }

    /**
     * @param PurchaseInvoice $oPurchaseInvoice
     * @return bool
     * @throws PropelException
     */
    public function reconcileOne(PurchaseInvoice $oPurchaseInvoice)
    {
        $aSupplierRegexes = [
            'FXW'  => ['FLEXWEBHOSTINGNL' => Ledger::HOSTING_SERVER_HUUR],
            'EASY' => ['FACTUURNUMMER [0-9]+' => Ledger::DOMEINREGISTRATIES],
            'MONK' => ['MONEYMONK' => Ledger::ACCOUNTANT_ADMINISTRATIE],
        ];
        $aBookings = Bank::findBookings(65634, $oPurchaseInvoice->getRemoteInvoiceId(), '2016-01-01', date('Y') . '-12-31');

        if (count($aBookings) === 0) {
            $oPurchaseInvoice->setNeedsManualBooking(true);
            $oPurchaseInvoice->save();
            $this->output("No bookings found for invoice " . $oPurchaseInvoice->getId() . " with number " . $oPurchaseInvoice->getRemoteInvoiceId() . " from " . $oPurchaseInvoice->getSupplier()->getName() . " paid on " . $oPurchaseInvoice->getPayDate(Config::getDateFormat()));
            return false;
        } elseif (count($aBookings) > 1) {
            $oPurchaseInvoice->setNeedsManualBooking(true);
            $oPurchaseInvoice->save();
            $this->output("Could not book invoice " . $oPurchaseInvoice->getId() . " more then one matching invoices where found at the MoneyMonk side. ");
            return false;
        }


        $oSupplier = $oPurchaseInvoice->getSupplier();

        if (!isset($aSupplierRegexes[$oSupplier->getNumber()])) {
            $oPurchaseInvoice->setNeedsManualBooking(true);
            $oPurchaseInvoice->save();
            $this->output("Could not book invoice no rules for this supplier. ");
            return false;
        }

        $bMatch = false;
        $iUseLedgerCode = null;
        foreach ($aSupplierRegexes[$oSupplier->getNumber()] as $sRegex => $iLedgerCode) {
            if (preg_match('/' . $sRegex . '/', $aBookings[0]['description'])) {
                $iUseLedgerCode = $iLedgerCode;
                $bMatch = true;
            }
        }
        if (!$bMatch) {
            $oPurchaseInvoice->setNeedsManualBooking(true);
            $oPurchaseInvoice->save();
            $this->output("No regex match found. ");
            return false;
        }

        $this->output("Reconciled " . $iUseLedgerCode . ' - ' . $aBookings[0]['id']);
        Ledger::reconcile($iUseLedgerCode, $aBookings[0]['id']);
        $oPurchaseInvoice->setIsBooked(true);
        $oPurchaseInvoice->save();
        return true;
    }

    /**
     * @return bool
     * @throws PropelException
     */
    public function execute(): bool
    {
        MoneyMonkApi::login(true);

        $oPurchaseInvoiceQuery = PurchaseInvoiceQuery::create();
        $oPurchaseInvoiceQuery->filterByIsBooked(false);
        $oPurchaseInvoiceQuery->filterByNeedsManualBooking(false);
        $oPurchaseInvoiceQuery->filterByIsPaid(true);
        $aPurchaseInvoices = $oPurchaseInvoiceQuery->find();

        $i = 0;
        $iLimit = 10;
        foreach ($aPurchaseInvoices as $oPurchaseInvoice) {
            $i++;
            $this->reconcileOne($oPurchaseInvoice);

            if ($i == $iLimit) {
                $this->output("$i done");
                exit();
            }
        }
        return true;
    }
}
