<?php

namespace Api\Accounting\MoneyMonk\Task;

use Api\AbstractTask;
use Api\Accounting\MoneyMonk\Ledger;
use Core\Translate;
use Model\Finance\BankLedgerQuery;
use Propel\Runtime\Exception\PropelException;

class NoInvoiceReconcilliation extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Boek overgebleven posten in MoneyMonk");
    }

    /**
     * @return bool
     * @throws PropelException
     */
    public function execute(): bool
    {

        $aSeachMap = [
            // HISCOX EUROPE
            'HBHPI1059515'                             => 'Verzekeringen',
            // Rekeningkosten 10 euro per maand
            'ABN AMRO BANK N.V. VIA INTERNETBANKIEREN' => 'Bankkosten',
            '0496855182'                               => 'Prive',

        ];

        foreach ($aSeachMap as $sSearchQuery => $sLedger) {
            $oLedger = BankLedgerQuery::create()->findOneByDescription($sLedger);
            Ledger::map($oLedger->getExternalId(), $sSearchQuery, '2018-01-01', date('Y') . '-12-31');
            /*
            $aBookings = Bank::findBookings(65634, $sSearchQuery, '2016-01-01', date('Y') . '-12-31');
            foreach ($aBookings as $aBooking)
            {
                $this->output("$sSearchQuery booked to $sLedger");

                Ledger::reconcile($oLedger->getExternalId(), $aBooking['id']);
            }
            */
        }

        return true;
    }
}
