<?php

namespace Api\Accounting\MoneyMonk\Task;

use Api\AbstractTask;
use Api\Accounting\MoneyMonk\Invoice;
use Api\Accounting\MoneyMonk\MoneyMonkApi;
use Core\Config;
use Core\Setting;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Finance\SaleInvoice;
use Model\Finance\SaleInvoiceQuery;
use Propel\Runtime\Exception\PropelException;

class FetchSaleInvoices extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Download nieuwe verkoop facturen");
    }

    /**
     * @return bool
     * @throws PropelException
     */
    public function execute(): bool
    {
        ini_set('display_errors', true);
        error_reporting(E_ALL);

        $this->output("Login");
        MoneyMonkApi::login();

        $bFromStart = true;

        if ($bFromStart) {
            $sFromDate = '2015-01-01';
        } else {
            $sLastPull = Setting::get('last_moneymonk_invoice_pull', strtotime('2015-01-01'));
            $sFromDate = date('Y-m-d', $sLastPull);
        }
        $sToDate = date('Y-m-d', time());
        $aInvoices = Invoice::search(65636, $sFromDate, $sToDate);

        $this->output("Download invoices from $sFromDate untill $sToDate");
        $this->output(count($aInvoices) . " invoices found");

        foreach ($aInvoices as $aInvoice) {
            $this->output("Copy invoice " . $aInvoice['id']);
            $oCustomerQuery = CustomerQuery::create();
            $oCustomerQuery->filterByImportSource('moneymonk');
            $oCustomerQuery->filterByAccountingId($aInvoice['contact_id']);
            $oCustomer = $oCustomerQuery->findOne();

            if (!$oCustomer instanceof Customer) {
                throw new LogicException("Not all invoices where imported, some customers are missing, first import them");
            }

            $oSaleInvoiceQuery = SaleInvoiceQuery::create();
            $oSaleInvoiceQuery->filterByExternalId($aInvoice['id']);
            $oSaleInvoiceQuery->filterByExternalSource('moneymonk');

            $oSaleInvoice = $oSaleInvoiceQuery->findOne();

            if (!$oSaleInvoice instanceof SaleInvoice) {
                $oSaleInvoice = new SaleInvoice();
                $oSaleInvoice->setExternalId($aInvoice['id']);
                $oSaleInvoice->setExternalSource('moneymonk');
            }

            $oSaleInvoice->setInvoiceNumber($aInvoice['invoice_number']);
            $oSaleInvoice->setCustomerId($oCustomer->getId());
            $oSaleInvoice->setInvoiceDate(strtotime($aInvoice['invoice_date']));
            $oSaleInvoice->setRevenueDate(strtotime($aInvoice['revenue_date']));
            $oSaleInvoice->setVatDate(strtotime($aInvoice['vat_date']));
            $oSaleInvoice->setDueDate(strtotime($aInvoice['due_date']));
            $oSaleInvoice->setIsPaid($aInvoice['status'] == 'paid');
            $oSaleInvoice->setTotalPayableAmount($aInvoice['total_payable_amount'] / 100);
            $oSaleInvoice->setOutstandingAmount($aInvoice['outstanding_amount_including_vat'] / 100);
            $oSaleInvoice->save();

            $sInvoiceDir = Config::getDataDir(true) . '/finance/sale_invoice';
            $sPdfFile = $sInvoiceDir . '/' . $oSaleInvoice->getId() . '.pdf';

            if (!file_exists($sPdfFile)) {
                $sInvoicePdf = Invoice::downloadPdf($aInvoice['id']);
                if (!is_dir($sInvoiceDir)) {
                    mkdir($sInvoiceDir);
                }

                file_put_contents($sInvoiceDir . '/' . $oSaleInvoice->getId() . '.pdf', $sInvoicePdf);

                $oSaleInvoice->setHasDocument(true);
                $oSaleInvoice->setDocumentExt('pdf');
                $oSaleInvoice->save();
            }
        }

        // Current time - 2 days just to have some overlap
        Setting::store('last_moneymonk_invoice_pull', time() - (60 * 60 * 24 * 2));
        return true;
    }
}
