<?php

namespace Api\Accounting\MoneyMonk\Task;

use Api\AbstractTask;
use Api\Accounting\MoneyMonk\Ledger;
use Core\Translate;
use Exception;
use LogicException;
use Model\Finance\BankLedger;
use Model\Finance\BankLedgerQuery;
use Propel\Runtime\Exception\PropelException;

class LedgerUpdate extends AbstractTask
{
    public function getDescription(): string
    {
        return Translate::fromCode("Update grootboek rekeningen");
    }

    /**
     * @return bool
     * @throws Exception
     * @throws PropelException
     */
    public function execute(): bool
    {
        $this->output("Request all ledgers from Moneymonk");

        $aLedgers = Ledger::getAll();

        foreach ($aLedgers as $aLedger) {
            $oBankLedgerQuery = BankLedgerQuery::create();
            $oBankLedgerQuery->filterByExternalId($aLedger['id']);
            $oBankLedgerQuery->filterByLedgerOrigin('moneymonk');
            $oBankLedger = $oBankLedgerQuery->findOne();

            if (!$oBankLedger instanceof BankLedger) {
                $oBankLedger = new BankLedger();
                $oBankLedger->setExternalId($aLedger['id']);
                $oBankLedger->setLedgerOrigin('moneymonk');
            }

            $oBankLedger->setDescription($aLedger['name']);

            $sAssetLiability = null;

            try {
                $oBankLedger->setAssetLiability($aLedger['element']);
            } catch (Exception $e) {
                throw new LogicException("Unsupported liability/liability/equity type: " . $aLedger['element']);
            }
            $oBankLedger->setKind($aLedger['kind']);
            $oBankLedger->save();
            $this->output($aLedger['kind'] . " created or updated");
        }
        $this->output('All done, ' . count($aLedgers) . ' ledgers have been imported.');
        return true;
    }
}
