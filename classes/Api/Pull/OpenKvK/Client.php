<?php

namespace Api\Pull\OpenKvK;

class Client
{
    public static function getClient($iDossierNummer, $iSubdossierNummer)
    {
        $sUrl = "https://overheid.io/api/kvk/$iDossierNummer/$iSubdossierNummer";
        $sFinalUrl = $sUrl . '?ovio-api-key=' . self::getApiKey();
        return self::getUrl($sFinalUrl);
    }

    /*
     * @param $aFilters see https://overheid.io/documentatie/kvk
     */

    private static function getApiKey()
    {
        if (isset($_SERVER['IS_DEVEL'])) {
            return 'dfd8e07797b605f87408c0ebf8a224a34d5591c0f0052cf4e36b7fd4756d2f6d';
        }
        return 'dfd8e07797b605f87408c0ebf8a224a34d5591c0f0052cf4e36b7fd4756d2f6d';
    }

    private static function getUrl($sUrl)
    {
        $rCurl = curl_init();
        curl_setopt($rCurl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($rCurl, CURLOPT_URL, $sUrl);
        curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($rCurl, CURLOPT_HEADER, 0);
        $sOutput = curl_exec($rCurl);
        curl_close($rCurl);

        return json_decode($sOutput, true);
    }

    public static function find($iCurrentPage = 1, $aFilters = [])
    {
        $sApiKey = self::getApiKey();
        $sUrl = 'https://overheid.io/api/kvk/';
        $aArguments = [
            'ovio-api-key' => $sApiKey,
            'size'         => 100,
            'page'         => $iCurrentPage,
            'filters'      => $aFilters,
        ];
        $sFinalUrl = $sUrl . '?' . http_build_query($aArguments);

        return self::getUrl($sFinalUrl);
    }
}
