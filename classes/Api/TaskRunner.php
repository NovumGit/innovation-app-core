<?php

namespace Api;

use Exception\LogicException;
use Model\Api\Task;
use Model\Api\TaskQuery;
use Propel\Runtime\Exception\PropelException;
use Psr\Log\AbstractLogger;

class TaskRunner
{
    private $oTask;

    public function __construct(int $iTaskId)
    {
        $oTaskQuery = TaskQuery::create();
        $this->oTask = $oTaskQuery->findOneById($iTaskId);
    }

    private function getModel(): Task
    {
        return $this->oTask;
    }

    /**
     * @throws PropelException
     */
    private function registerStart()
    {
        $oModel = $this->getModel();
        $oModel->setRunCount($oModel->getRunCount() + 1);
        $oModel->setLastFinish(null);
        $oModel->setLastStart(time());
        $oModel->save();
    }

    /**
     * @throws PropelException
     */
    private function registeEnd()
    {
        $oModel = $this->getModel();
        $oModel->setFinishCount($oModel->getFinishCount() + 1);
        $oModel->setLastFinish(time());
        $oModel->save();
    }

    /**
     * @throws PropelException
     */
    public function resetCounters()
    {
        $oModel = $this->getModel();
        $oModel->setLastStart(null);
        $oModel->setLastFinish(null);
        $oModel->setRunCount(0);
        $oModel->setFinishCount(0);
        $oModel->save();
    }

    /**
     * @throws PropelException
     */
    public function run()
    {
        $this->registerStart();

        $oTaskClass = $this->getModel()->getExecutable();

        $oLogger = new class extends AbstractLogger {
            public function log($level, $message, array $context = [])
            {
                echo $message . "<br>";
            }
        };

        $oTask = new $oTaskClass($oLogger);

        if (!$oTask instanceof AbstractTask) {
            throw new LogicException("Could not run task $oTaskClass");
        }

        // Does the actual work.
        $oTask->execute();

        $this->registeEnd();
    }
}
