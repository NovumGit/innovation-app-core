<?php

namespace Api\Info;

interface IApiInfo
{
    public function getTitle(): string;

    public function getDescription(): string;

    public function getOrganisation(): string;

    public function getApiDir(): string;

    public function getEmail(): string;

    public function getServers(): array;

    public function getContacts(): array;

    public function getEndpointUrl(): string;

    public function getDocumentationUrl(): string;

    public function getAuthorisationModel(): string;

    public function getCaCert(): string;
}
