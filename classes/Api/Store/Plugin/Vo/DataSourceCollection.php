<?php

namespace Api\Store\Plugin\Vo;

use ArrayIterator;

class DataSourceCollection extends ArrayIterator
{
    public function __construct($array = [], $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    public function findOneByComposerPackageName(string $sPackageName): ?DataSource
    {
        foreach ($this as $oDataSource) {
            if ($sPackageName === $oDataSource->getPackageName()) {
                return $oDataSource;
            }
        }
        return null;
    }

    public function add(DataSource $oDataSource)
    {
        $this->append($oDataSource);
    }

    public function current(): DataSource
    {
        return parent::current();
    }
}
