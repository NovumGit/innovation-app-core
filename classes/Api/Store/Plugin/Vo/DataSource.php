<?php

namespace Api\Store\Plugin\Vo;

use Hurah\Types\Type\Url;

class DataSource
{
    private int $id;
    private string $titel;
    private string $code;
    private string $url;
    private string $documentation;
    private string $description;
    private string $packagist_url;
    private string $docker_image;
    private string $git_repo;
    private string $avatar_url_300;

    public function __construct(array $aData)
    {
        if (is_array($aData)) {
            foreach ($aData as $sProperty => $sValue) {
                if (property_exists($this, $sProperty) && $sValue) {
                    $this->{$sProperty} = $sValue;
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitel(): string
    {
        return $this->titel;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getDocumentation(): string
    {
        return $this->documentation;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public function getGitRepo(): string
    {
        return $this->git_repo;
    }

    /**
     * @return string
     */
    public function getDockerImage(): string
    {
        return $this->docker_image;
    }

    public function getPackageName(): string
    {
        [
            $service_name,
            $vendor,
        ] = array_reverse(explode('/', $this->getPackagistUrl()));
        return "$vendor/$service_name";
    }

    /**
     * @return string
     */
    public function getPackagistUrl(): string
    {
        return $this->packagist_url;
    }

    public function getAvatarUrl300(): Url
    {
        return new Url($this->avatar_url_300);
    }
}
