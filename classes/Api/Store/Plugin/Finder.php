<?php

namespace Api\Store\Plugin;

use Api\Store\Plugin\Vo\DataSource;
use Api\Store\Plugin\Vo\DataSourceCollection;
use Core\Json\JsonUtils;
use Core\Reflection\ISystem;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Finder implements ISystem
{

    /**
     * @return array
     * @throws GuzzleException
     * @throws Exception
     */
    public function find(): DataSourceCollection
    {
        $oDataSourceCollection = new DataSourceCollection();
        $client = new Client();
        $sUrl = 'https://api.overheid.demo.novum.nu/v2/rest/datasource/';
        $request = $client->request('GET', $sUrl);
        if ($request->getStatusCode() !== 200) {
            throw new Exception("Got statuscode " . $request->getStatusCode() . " while fetching packages");
        }
        $aData = JsonUtils::decode($request->getBody());

        if (is_array($aData['results'])) {
            foreach ($aData['results'] as $aDataSource) {
                $oDataSourceCollection->add(new DataSource($aDataSource));
            }
        }
        return $oDataSourceCollection;
    }
}
