<?php

namespace Base;

use \ServiceDirectory as ChildServiceDirectory;
use \ServiceDirectoryQuery as ChildServiceDirectoryQuery;
use \Exception;
use \PDO;
use Map\ServiceDirectoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'service_directory' table.
 *
 *
 *
 * @method     ChildServiceDirectoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildServiceDirectoryQuery orderByVendor($order = Criteria::ASC) Order by the vendor column
 * @method     ChildServiceDirectoryQuery orderByServiceName($order = Criteria::ASC) Order by the service_name column
 * @method     ChildServiceDirectoryQuery orderByHomepage($order = Criteria::ASC) Order by the homepage column
 * @method     ChildServiceDirectoryQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildServiceDirectoryQuery orderByKeywords($order = Criteria::ASC) Order by the keywords column
 * @method     ChildServiceDirectoryQuery orderByLicense($order = Criteria::ASC) Order by the license column
 * @method     ChildServiceDirectoryQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildServiceDirectoryQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildServiceDirectoryQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildServiceDirectoryQuery orderByDomain($order = Criteria::ASC) Order by the domain column
 *
 * @method     ChildServiceDirectoryQuery groupById() Group by the id column
 * @method     ChildServiceDirectoryQuery groupByVendor() Group by the vendor column
 * @method     ChildServiceDirectoryQuery groupByServiceName() Group by the service_name column
 * @method     ChildServiceDirectoryQuery groupByHomepage() Group by the homepage column
 * @method     ChildServiceDirectoryQuery groupByDescription() Group by the description column
 * @method     ChildServiceDirectoryQuery groupByKeywords() Group by the keywords column
 * @method     ChildServiceDirectoryQuery groupByLicense() Group by the license column
 * @method     ChildServiceDirectoryQuery groupByFirstName() Group by the first_name column
 * @method     ChildServiceDirectoryQuery groupByLastName() Group by the last_name column
 * @method     ChildServiceDirectoryQuery groupByEmail() Group by the email column
 * @method     ChildServiceDirectoryQuery groupByDomain() Group by the domain column
 *
 * @method     ChildServiceDirectoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServiceDirectoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServiceDirectoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServiceDirectoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServiceDirectoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServiceDirectoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServiceDirectory findOne(ConnectionInterface $con = null) Return the first ChildServiceDirectory matching the query
 * @method     ChildServiceDirectory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServiceDirectory matching the query, or a new ChildServiceDirectory object populated from the query conditions when no match is found
 *
 * @method     ChildServiceDirectory findOneById(int $id) Return the first ChildServiceDirectory filtered by the id column
 * @method     ChildServiceDirectory findOneByVendor(string $vendor) Return the first ChildServiceDirectory filtered by the vendor column
 * @method     ChildServiceDirectory findOneByServiceName(string $service_name) Return the first ChildServiceDirectory filtered by the service_name column
 * @method     ChildServiceDirectory findOneByHomepage(string $homepage) Return the first ChildServiceDirectory filtered by the homepage column
 * @method     ChildServiceDirectory findOneByDescription(string $description) Return the first ChildServiceDirectory filtered by the description column
 * @method     ChildServiceDirectory findOneByKeywords(string $keywords) Return the first ChildServiceDirectory filtered by the keywords column
 * @method     ChildServiceDirectory findOneByLicense(string $license) Return the first ChildServiceDirectory filtered by the license column
 * @method     ChildServiceDirectory findOneByFirstName(string $first_name) Return the first ChildServiceDirectory filtered by the first_name column
 * @method     ChildServiceDirectory findOneByLastName(string $last_name) Return the first ChildServiceDirectory filtered by the last_name column
 * @method     ChildServiceDirectory findOneByEmail(string $email) Return the first ChildServiceDirectory filtered by the email column
 * @method     ChildServiceDirectory findOneByDomain(string $domain) Return the first ChildServiceDirectory filtered by the domain column *

 * @method     ChildServiceDirectory requirePk($key, ConnectionInterface $con = null) Return the ChildServiceDirectory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOne(ConnectionInterface $con = null) Return the first ChildServiceDirectory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServiceDirectory requireOneById(int $id) Return the first ChildServiceDirectory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByVendor(string $vendor) Return the first ChildServiceDirectory filtered by the vendor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByServiceName(string $service_name) Return the first ChildServiceDirectory filtered by the service_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByHomepage(string $homepage) Return the first ChildServiceDirectory filtered by the homepage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByDescription(string $description) Return the first ChildServiceDirectory filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByKeywords(string $keywords) Return the first ChildServiceDirectory filtered by the keywords column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByLicense(string $license) Return the first ChildServiceDirectory filtered by the license column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByFirstName(string $first_name) Return the first ChildServiceDirectory filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByLastName(string $last_name) Return the first ChildServiceDirectory filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByEmail(string $email) Return the first ChildServiceDirectory filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServiceDirectory requireOneByDomain(string $domain) Return the first ChildServiceDirectory filtered by the domain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServiceDirectory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServiceDirectory objects based on current ModelCriteria
 * @method     ChildServiceDirectory[]|ObjectCollection findById(int $id) Return ChildServiceDirectory objects filtered by the id column
 * @method     ChildServiceDirectory[]|ObjectCollection findByVendor(string $vendor) Return ChildServiceDirectory objects filtered by the vendor column
 * @method     ChildServiceDirectory[]|ObjectCollection findByServiceName(string $service_name) Return ChildServiceDirectory objects filtered by the service_name column
 * @method     ChildServiceDirectory[]|ObjectCollection findByHomepage(string $homepage) Return ChildServiceDirectory objects filtered by the homepage column
 * @method     ChildServiceDirectory[]|ObjectCollection findByDescription(string $description) Return ChildServiceDirectory objects filtered by the description column
 * @method     ChildServiceDirectory[]|ObjectCollection findByKeywords(string $keywords) Return ChildServiceDirectory objects filtered by the keywords column
 * @method     ChildServiceDirectory[]|ObjectCollection findByLicense(string $license) Return ChildServiceDirectory objects filtered by the license column
 * @method     ChildServiceDirectory[]|ObjectCollection findByFirstName(string $first_name) Return ChildServiceDirectory objects filtered by the first_name column
 * @method     ChildServiceDirectory[]|ObjectCollection findByLastName(string $last_name) Return ChildServiceDirectory objects filtered by the last_name column
 * @method     ChildServiceDirectory[]|ObjectCollection findByEmail(string $email) Return ChildServiceDirectory objects filtered by the email column
 * @method     ChildServiceDirectory[]|ObjectCollection findByDomain(string $domain) Return ChildServiceDirectory objects filtered by the domain column
 * @method     ChildServiceDirectory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServiceDirectoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ServiceDirectoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\ServiceDirectory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServiceDirectoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServiceDirectoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServiceDirectoryQuery) {
            return $criteria;
        }
        $query = new ChildServiceDirectoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServiceDirectory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServiceDirectoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServiceDirectoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServiceDirectory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, vendor, service_name, homepage, description, keywords, license, first_name, last_name, email, domain FROM service_directory WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServiceDirectory $obj */
            $obj = new ChildServiceDirectory();
            $obj->hydrate($row);
            ServiceDirectoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServiceDirectory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the vendor column
     *
     * Example usage:
     * <code>
     * $query->filterByVendor('fooValue');   // WHERE vendor = 'fooValue'
     * $query->filterByVendor('%fooValue%', Criteria::LIKE); // WHERE vendor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vendor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByVendor($vendor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vendor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_VENDOR, $vendor, $comparison);
    }

    /**
     * Filter the query on the service_name column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceName('fooValue');   // WHERE service_name = 'fooValue'
     * $query->filterByServiceName('%fooValue%', Criteria::LIKE); // WHERE service_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serviceName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByServiceName($serviceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serviceName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_SERVICE_NAME, $serviceName, $comparison);
    }

    /**
     * Filter the query on the homepage column
     *
     * Example usage:
     * <code>
     * $query->filterByHomepage('fooValue');   // WHERE homepage = 'fooValue'
     * $query->filterByHomepage('%fooValue%', Criteria::LIKE); // WHERE homepage LIKE '%fooValue%'
     * </code>
     *
     * @param     string $homepage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByHomepage($homepage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($homepage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_HOMEPAGE, $homepage, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the keywords column
     *
     * Example usage:
     * <code>
     * $query->filterByKeywords('fooValue');   // WHERE keywords = 'fooValue'
     * $query->filterByKeywords('%fooValue%', Criteria::LIKE); // WHERE keywords LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keywords The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByKeywords($keywords = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keywords)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_KEYWORDS, $keywords, $comparison);
    }

    /**
     * Filter the query on the license column
     *
     * Example usage:
     * <code>
     * $query->filterByLicense('fooValue');   // WHERE license = 'fooValue'
     * $query->filterByLicense('%fooValue%', Criteria::LIKE); // WHERE license LIKE '%fooValue%'
     * </code>
     *
     * @param     string $license The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByLicense($license = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($license)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_LICENSE, $license, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the domain column
     *
     * Example usage:
     * <code>
     * $query->filterByDomain('fooValue');   // WHERE domain = 'fooValue'
     * $query->filterByDomain('%fooValue%', Criteria::LIKE); // WHERE domain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $domain The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function filterByDomain($domain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($domain)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServiceDirectoryTableMap::COL_DOMAIN, $domain, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServiceDirectory $serviceDirectory Object to remove from the list of results
     *
     * @return $this|ChildServiceDirectoryQuery The current query, for fluid interface
     */
    public function prune($serviceDirectory = null)
    {
        if ($serviceDirectory) {
            $this->addUsingAlias(ServiceDirectoryTableMap::COL_ID, $serviceDirectory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the service_directory table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServiceDirectoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServiceDirectoryTableMap::clearInstancePool();
            ServiceDirectoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServiceDirectoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServiceDirectoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServiceDirectoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServiceDirectoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ServiceDirectoryQuery
