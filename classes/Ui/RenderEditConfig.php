<?php

namespace Ui;

use AdminModules\Field\IStyleHelper;
use Exception;

class RenderEditConfig
{
    private $sTitle;
    private $sLayout;
    private $sFieldArrayName = 'data';
    private $sEditorHeaderMenuHtml;
    private $iLabelWidth = 4;
    private $iFieldWidth = 7;
    private $bSaveOverviewbuttonVisible = true;
    private $bSingleGoogleSuggest = false;
    private $sToolTip = null;
    private $sSaveButtonLocation = 'in_form';
    private $bButtonsEnabled = true;
    private $aGlobals;
    private $bTitleLocked;
    private bool $bHasDoField = true;

    function __construct($sTitle = null, $sLayout = 'Style1')
    {
        $this->sTitle = $sTitle;
        $this->sLayout = $sLayout;
    }

    function setAddDoField(bool $bHasDoField = false)
    {
        $this->bHasDoField = $bHasDoField;
    }

    function hasDoField(): bool
    {
        return $this->bHasDoField;
    }

    function lockTitle(bool $bValue)
    {
        $this->bTitleLocked = $bValue;
    }

    function titleLocked(): bool
    {
        return $this->bTitleLocked;
    }

    function setButtonsEnabled(bool $bButtonsEnabled)
    {
        $this->bButtonsEnabled = $bButtonsEnabled;
    }

    function getButtonsEnabled(): bool
    {
        return $this->bButtonsEnabled;
    }

    function setFieldArrayName(string $sName)
    {
        $this->sFieldArrayName = $sName;
    }

    function getFieldArrayName(): string
    {
        return $this->sFieldArrayName;
    }

    function getStyleClassHelper(): IStyleHelper
    {
        $sStyleClassName = '\\AdminModules\\Field\\' . $this->getLayout() . '\\' . $this->getLayout() . 'Helper';
        $oStyleClassHelper = new $sStyleClassName($this);
        return $oStyleClassHelper;
    }

    function getLayout(): string
    {
        return $this->sLayout;
    }

    /**
     * @param $sLocation
     * @throws Exception
     */
    function setSaveButtonLocation($sLocation)
    {
        if (
            !in_array($sLocation, [
            'in_form',
            'top',
            ])
        ) {
            throw new Exception("Only in_form and top are supported");
        }
        $this->sSaveButtonLocation = $sLocation;
    }

    function getSaveButtonLocation(): string
    {
        return $this->sSaveButtonLocation;
    }

    /**
     * @param bool $bNewValue
     */
    function setEnableSingleGoogleSuggest($bNewValue = true)
    {
        $this->bSingleGoogleSuggest = $bNewValue;
    }

    function getSingleGoogleSuggestEnabled()
    {
        return $this->bSingleGoogleSuggest;
    }

    function setToolTip($sToolTip)
    {
        $this->sToolTip = $sToolTip;
    }

    public function getToolTip()
    {
        return $this->sToolTip;
    }

    public function getTitle()
    {
        return $this->sTitle;
    }

    public function setLayout($sLayout)
    {
        $this->sLayout = $sLayout;
    }

    public function getFieldWidth(): int
    {
        return $this->iFieldWidth;
    }

    public function getLabelWidth(): int
    {
        return $this->iLabelWidth;
    }

    public function setLabelAndFieldWidth(int $iLabelWidth, int $iFieldWidht)
    {
        $this->iLabelWidth = $iLabelWidth;
        $this->iFieldWidth = $iFieldWidht;
    }

    public function setSaveOverviewButtonVisibile($bSaveOverviewbuttonVisible)
    {
        $this->bSaveOverviewbuttonVisible = $bSaveOverviewbuttonVisible;
    }

    public function getSaveOverviewButtonVisibile()
    {
        return $this->bSaveOverviewbuttonVisible;
    }

    public function setEditorHeaderMenuHtml($sEditorHeaderMenuHtml)
    {
        $this->sEditorHeaderMenuHtml = $sEditorHeaderMenuHtml;
    }

    public function getEditorHeaderMenuHtml()
    {
        return $this->sEditorHeaderMenuHtml;
    }

    public function getGlobals(): ?array
    {
        return $this->aGlobals;
    }

    public function addGlobals(array $aGlobals)
    {
        $this->aGlobals = $aGlobals;
    }
}
