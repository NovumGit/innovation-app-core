<?php

namespace Ui\Form\Field\Dynamic\Helper;

use Hurah\Types\Type\Path;
use Exception\InvalidArgumentException;
use Ui\Form\Field\Dynamic\Helper\Lookup;
use Ui\Form\Field\Dynamic\Helper\Lookup\ILookupGeneratorHelper;

class LookupGeneratorFactory
{
    /**
     * @param $aInputData
     * @return ILookupGeneratorHelper
     * @throws InvalidArgumentException
     */
    public static function create($aInputData): ILookupGeneratorHelper
    {
        if (isset($aInputData['full_class'])) {
            return new Lookup\PropelMethod($aInputData['full_class'], $aInputData['method']);
        } else {
            if (isset($aInputData['url'])) {
                return new Lookup\UrlBased(new Path($aInputData['url']));
            } else {
                if (is_array($aInputData)) {
                    return new Lookup\DirectInput($aInputData['data'], $aInputData['visible']);
                }
            }
        }
        throw new InvalidArgumentException(__METHOD__ . " can only create method body if one of full_class, url or a direct array is passed.");
    }
}
