<?php

namespace Ui\Form\Field\Dynamic\Helper\Lookup;

interface ILookupGeneratorHelper
{
    public function getLookupsFunctionBody(): string;

    public function getVisibleValueFunctionBody(): string;
}
