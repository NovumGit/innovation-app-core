<?php

namespace Ui\Form\Field\Dynamic\Helper\Lookup;

use Hurah\Types\Type\Path;
use Core\Utils;

class UrlBased implements ILookupGeneratorHelper
{
    private $sPathTocall = null;
    private $sVisibleField = null;

    /**
     * Generates lookups based on an api call / url to be called. The url is called each time when lookups are requested
     * if you want them to be hard coded on code generation it is better to use DirectInput.
     *
     * The visible method has to be passed in the url scheme but is stripped off so only used internally.
     * So:
     *      https://api.gemeente.demo.novum.nu/v2/rest/land/{naam}
     * Becomes:
     *      https://api.gemeente.demo.novum.nu/v2/rest/land/
     *
     * And the system looks for an array with a key called "id" and a value of "naam"
     *
     *
     * UrlBased constructor.
     * @param Path $sPathToCall - takes the form of https://api.gemeente.demo.novum.nu/v2/rest/land/{naam}
     */
    public function __construct(Path $sPathToCall)
    {
        $aParts = [];
        preg_match_all('/\{(.+)\}/', $sPathToCall, $aParts);

        if (!isset($aParts[1][0])) {
            echo "There are issues with this url $sPathToCall " . PHP_EOL;
        }

        $this->sVisibleField = $aParts[1][0];
        $this->sPathTocall = str_replace($aParts[0][0], '', $sPathToCall);
    }

    public function getLookupsFunctionBody(): string
    {
        $sUrl = $this->sPathTocall;
        if (Utils::pathIsUrl($this->sPathTocall)) {
            $sUrl = Utils::makeUrl($this->sPathTocall, ['items_pp' => 100000]);
        }

        return join(PHP_EOL, [
            '$sUrl = "' . $sUrl . '";',
            '$sJsonRows = file_get_contents($sUrl);',
            '$aAllRows = json_decode($sJsonRows, true);',
            '$aOptions = \Core\Utils::makeSelectOptions($aAllRows["results"], "' . $this->sVisibleField . '", $mSelectedItem);',
            '$aOptions = $this->filterLookups($aOptions);',
            'return $aOptions;',
        ]);
    }

    public function getVisibleValueFunctionBody(): string
    {
        $aOut[] = '$sUrl = "' . $this->sPathTocall . '" . $iItemId;';
        $aOut[] = '$sJsonRows = file_get_contents($sUrl);';
        $aOut[] = '$aLookup = json_decode($sJsonRows, true);';
        $aOut[] = 'return $aLookup;';

        return join(PHP_EOL, $aOut);
    }
}
