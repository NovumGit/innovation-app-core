<?php

namespace Ui\Form\Field\Dynamic\Helper\Lookup;

class PropelMethod implements ILookupGeneratorHelper
{
    private $sFkClassName;
    private $sMethodToCall;

    /**
     * Assumes a Propel method that is to be called. (getName, getCountry etc). The get part of the method can be
     * omitted or passed, it will be stripped.
     *
     * PropelMethod constructor.
     * @param string $sFqClassName
     * @param string $sMethodToCall
     */
    public function __construct(string $sFqClassName, string $sMethodToCall)
    {
        $sMethodToCall = str_replace('get', '', $sMethodToCall);
        $this->sFkClassName = $sFqClassName;
        $this->sMethodToCall = $sMethodToCall;
    }

    public function getLookupsFunctionBody(): string
    {
        return join(PHP_EOL, [
            '$aAllRows = \\' . $this->sFkClassName . 'Query::create()->orderBy' . $this->sMethodToCall . '()->find();',
            '$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "get' . $this->sMethodToCall . '", $mSelectedItem, "getId");',
            '$aOptions = $this->filterLookups($aOptions);',
            'return $aOptions;',
        ]);
    }

    public function getVisibleValueFunctionBody(): string
    {
        return join(PHP_EOL, [
            'if($iItemId){',
            '    return \\' . $this->sFkClassName . 'Query::create()->findOneById($iItemId)->get' . $this->sMethodToCall . '();',
            '}',
            'return null;',
        ]);
    }
}
