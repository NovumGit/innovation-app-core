<?php

namespace Ui\Form\Field\Dynamic\Helper\Lookup;

class DirectInput implements ILookupGeneratorHelper
{

    private $aInputData;
    private $sVisibleValueColumn;

    public function __construct(array $aInputData, string $sVisibleValueColumn)
    {
        $this->sVisibleValueColumn = $sVisibleValueColumn;
        $this->aInputData = $aInputData;
    }

    public function getLookupsFunctionBody(): string
    {
        $sVisibleColumn = $this->sVisibleValueColumn;
        return join(PHP_EOL, [
            "   \$sOptions = '" . json_encode($this->aInputData) . "';",
            "   \$aOptions = json_decode(\$sOptions, true);",
            "   \$aDropDown = \Core\Utils::makeSelectOptions(\$aOptions, \"$sVisibleColumn\", \$mSelectedItem);",
            '   \$aDropDown = \$this->filterLookups(\$aDropDown);',
            "   return \$aDropDown;",
        ]);
    }

    public function getVisibleValueFunctionBody(): string
    {
        return join(PHP_EOL, [
            '   if($iItemId){',
            '       $sOptions = ' . json_encode($this->aInputData) . ';',
            "       \$aLookups = json_decode(\$sOptions, true);",
            '       if(!empty($aLookups))',
            '       {',
            '           foreach($aLookups as $aLookup)',
            '           {',
            '               if($aLookup["id"] == $iItemId)',
            '               {',
            '                   return $aLookup["label"];',
            '               }',
            '           }',
            '       }',
            '       return null;',
            '}',
        ]);
    }
}
