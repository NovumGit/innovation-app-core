<?php

namespace Ui\Form\Field;

use Hurah\Types\Type\Icon;
use Core\Utils;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;
use Ui\Form\Field\Dynamic\Helper\LookupGeneratorFactory;
use Ui\Form\GenericFieldTypes;

/**
 * Creates crud fields on the fly
 *
 * Class DynamicField
 * @package Ui\IForm\Field
 */
final class DynamicField
{
    public static function create(string $sFqn, string $sFieldName, string $sNameSpace, string $sTitle, Icon $sIcon, string $sPlaceHolder, string $sType, bool $bRequired, string $sValue, array $aLookups = null): string
    {
        $oNamespace = new PhpNamespace($sNameSpace);

        $oClass = new ClassType();
        $oClass->setName($sFqn);
        $oNamespace->add($oClass);

        $sSuperClass = GenericFieldTypes::findByKey($sType);

        $oClass->setExtends($sSuperClass);
        $oClass->setImplements([
            IFilterableField::class,
            IEditableField::class,
        ]);

        $oNamespace->addUse(IFilterableField::class);
        $oNamespace->addUse(IEditableField::class);

        if ($sType == 'lookup') {
            $oGetDatatypeMethod = $oClass->addMethod('getDataType');
            $oGetDatatypeMethod->setReturnType('string');
            $oGetDatatypeMethod->setBody("return 'lookup';");

            $oClass->addImplement(IFilterableLookupField::class);

            $oLookupGeneratorFactory = LookupGeneratorFactory::create($aLookups);

            $oGetLookupsMethod = $oClass->addMethod('getLookups');
            $oGetLookupsMethod->addParameter('mSelectedItem', null);
            $oGetLookupsMethod->setBody($oLookupGeneratorFactory->getLookupsFunctionBody());

            $oValidate = $oClass->addMethod('getVisibleValue');
            $oValidate->addParameter('iItemId', null);
            $oValidate->setBody($oLookupGeneratorFactory->getVisibleValueFunctionBody());

            $oNamespace->addUse(Utils::class);
            $oNamespace->addUse(IFilterableLookupField::class);

            if (isset($aParts['full_class'])) {
                $oNamespace->addUse($aLookups['full_class'] . 'Query');
            }
        }

        $oClass->addImplement(IRequiredField::class);
        $oNamespace->addUse(IRequiredField::class);

        $oHasValidations = $oClass->addMethod('hasValidations');
        $oHasValidations->setBody('return ' . ($bRequired ? 'true' : 'false') . ';');

        $oValidate = $oClass->addMethod('validate');
        $oValidate->addParameter('aPostedData');
        $oValidate->setBody(self::getValidateBody($sFieldName, $sTitle, $bRequired));

        $oClass->setAbstract();
        $oClass->addProperty('sFieldName', $sFieldName)->setVisibility('protected');
        $oClass->addProperty('sFieldLabel', $sTitle)->setVisibility('protected');
        $oClass->addProperty('sIcon', $sIcon)->setVisibility('protected');
        $oClass->addProperty('sPlaceHolder', $sPlaceHolder)->setVisibility('protected');
        $oClass->addProperty('sFqModelClassname', 'GenericEditTextfield::class')->setVisibility('protected');

        $oClass->addProperty('sGetter', 'getCurrentValue')->setVisibility('protected');

        $oIsValidModelMethod = $oClass->addMethod('getCurrentValue');
        $oIsValidModelMethod->setVisibility('private');
        $oIsValidModelMethod->setBody('return "' . addslashes($sValue) . '";');

        $oNamespace->addUse($sSuperClass);

        return (string)$oNamespace;
    }

    private static function getValidateBody(string $sName, string $sTitle, bool $bRequired)
    {
        $aOut = [];
        $aOut[] = '$mResponse = false;';
        if ($bRequired) {
            $aOut[] = '$mParentResponse = parent::validate($aPostedData);';
            $aOut[] = PHP_EOL;
            $aOut[] = 'if(!isset($aPostedData[\'' . $sName . '\']) || empty($aPostedData[\'' . $sTitle . '\']))';
            $aOut[] = '{';
            $aOut[] = '     $mResponse = [];';
            $aOut[] = '     $mResponse[] = \'Het veld "' . $sTitle . '" verplicht maar nog niet ingevuld.\';';
            $aOut[] = '}';
            $aOut[] = 'if(!empty($mParentResponse)){';
            $aOut[] = '     $mResponse = array_merge($mResponse, $mParentResponse);';
            $aOut[] = '}';
        } else {
            $aOut[] = '$mResponse = parent::validate();';
        }

        $aOut[] = 'return $mResponse;';

        return join(PHP_EOL, $aOut);
    }
}
