<?php

namespace Ui\Form;

use Core\Utils;
use Crud\ICrudElement;
use Ui\Form\Field\DynamicField;

class FieldFactory
{
    public static function fromArray(array $aField): ICrudElement
    {
        $sName = $aField['name'];
        $sNamespace = $aField['namespace'];
        $sTitle = $aField['title'];
        $sIcon = $aField['icon'];
        $sPlaceholder = $aField['placeholder'];
        $sType = $aField['type'];
        $bRequired = $aField['required'];
        $sValue = $aField['value'];
        $aLookups = $aField['lookups'];

        $sFqn = $sNamespace . '\\' . Utils::camelCase($sName);

        echo "Class $sFqn " . PHP_EOL;
        $sGeneratedField = DynamicField::create($sFqn, $sName, $sNamespace, $sTitle, $sIcon, $sPlaceholder, $sType, $bRequired, $sValue, $aLookups);

        eval($sGeneratedField);
        return new $sFqn();
    }
}
