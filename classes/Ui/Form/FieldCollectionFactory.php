<?php

namespace Ui\Form;

use Crud\FieldCollection;

class FieldCollectionFactory
{
    public static function fromArray(array $aArrayOfFieldItems): FieldCollection
    {

        $aTmp = [];
        foreach ($aArrayOfFieldItems as $aFieldItem) {
            $aTmp[] = FieldFactory::fromArray($aFieldItem);
        }
        return new FieldCollection($aTmp);
    }
}
