<?php

namespace Ui\Form;

use Cli\Tools\CommandUtils;
use Core\Utils;
use Exception\LogicException;

class GenericFieldTypes
{
    /**
     * Returns the fully qualified classname of a GenericField.
     * @param string $sKey
     * @return string
     */
    public static function findByKey(string $sKey): string
    {
        if (empty($sKey)) {
            throw new LogicException("Please set the form tag on your field $sKey");
        } else {
            if (!isset($aMap[$sKey])) {
                throw new LogicException("{$sKey} is not a supported form type for field $sKey");
            }
        }
        $aAllFields = self::getAll();

        return $aAllFields[$sKey];
    }

    /**
     * Returns a key value pair of GenericField classes.
     * @return array
     */
    public static function getAll(): array
    {
        $aGenericFieldTypes = glob(CommandUtils::getRoot() . '/classes/Crud/Generic/Field/*');
        $aOut = [];
        foreach ($aGenericFieldTypes as $sName) {
            $sBaseName = basename($sName);
            $sBaseNameWoExt = preg_replace('/\.php$/', '', $sBaseName);
            $sBaseNameWoExtWoGeneric = str_replace('Generic', '', $sBaseNameWoExt);
            $sFqn = '\\Crud\\Generic\\Field\\' . $sBaseNameWoExt;
            $sKey = Utils::snake_case($sBaseNameWoExtWoGeneric);

            $aOut[$sKey] = $sFqn;
        }
        print_r($aOut);
        return $aOut;
    }
}
