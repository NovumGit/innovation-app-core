<?php
namespace Lowcode\Binding\EventType;

interface IEventType
{
    function getEventTag():string;
}
