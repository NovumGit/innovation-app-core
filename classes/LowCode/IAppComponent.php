<?php
namespace LowCode;

interface IAppComponent
{
    function getName():string;
    function getDescription():string;
}
