<?php
namespace LowCode\Component;


use Hurah\Types\Type\Url;

interface IConfigurable
{
    function getProps():IHasExternalConfig;
    function getConfigUrl():Url;
}
