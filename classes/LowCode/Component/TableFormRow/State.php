<?php
namespace LowCode\Component\TableFormRow;

use LowCode\Contracts\State\ICoreState;

class State implements ICoreState
{
    private $aData;

    function __construct(array $aData = null)
    {
        $this->aData = $aData;
    }

    function toArray():array
    {
        return $this->aData;
    }
}


