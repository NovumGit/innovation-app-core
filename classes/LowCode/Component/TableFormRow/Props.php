<?php
namespace LowCode\Component\TableFormRow;

use Crud\FormManager;
use Exception\FatalErrorException;
use Exception\InvalidConfigurationException;
use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class Props extends CoreProps implements ICoreProps
{
    function getManager():FormManager
    {
        $sFqn = $this->getPropertyById('manager');

        if(empty($sFqn))
        {
            throw new InvalidConfigurationException("Element manager is missing in block " . $this->getId());
        }

        $oManager = new $sFqn;

        if(!$oManager instanceof FormManager)
        {
            throw new FatalErrorException("Could not locate manager class $sFqn");
        }
        return $oManager;
    }
    function getTableFooter():?array
    {
        $aTableFooterArguments = $this->getPropertyById('table_footer');
        return $aTableFooterArguments;
    }

    function getQueryObject():ModelCriteria
    {
        $sQueryObjectName = $this->getPropertyById('query_object');

        if(empty($sQueryObjectName))
        {
            throw new InvalidConfigurationException("Element query_object is missing in block " . $this->getId());
        }

        return $sQueryObjectName::create();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function getLayoutKey():string
    {
        $sLayoutKey = $this->getPropertyById('layout_key');

        if(empty($sLayoutKey))
        {
            throw new \Exception("Element " . $this->getPropertyById('id') . " has no property layout_key");
        }

        return $sLayoutKey;
    }
}
