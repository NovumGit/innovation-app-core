<?php
namespace LowCode\Component;

use Core\Reflector;
use Crud\IApiExposable;
use Crud\IFormManager;
use Exception\LogicException;
use Exception\NullPointerException;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\ComponentAcceptsNone;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\System\ComponentPropertyDefinition;
use LowCode\System\Parser\ComponentXml\Component;
use LowCode\System\Parser\ComponentXmlParser;
use Model\Logging\Except_log;
use Model\System\LowCode\Table\Component_table;
use Model\System\UI\UIComponentType;
use Model\System\UI\UIComponentTypeQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;

abstract class AbstractComponent implements IComponent
{
    private $aState;
    private $aProperties;

    /**
     * AbstractComponent constructor.
     * @param array|null $aProperties
     * @param array|null $aState
     * @throws \Exception
     */
    function __construct(array $aProperties = null, array $aState = null)
    {
        $this->setProps($aProperties);
        $this->aState = $aState;
    }
    function getConfig(int $iUiComponentId = null):array
    {
        $oManager = $this->getApiManager();

        $oModel = $oManager->getQueryObject()->findOneBy('UiComponentId', $iUiComponentId);

        if(!$oModel) {
            $oModel = $oManager->store(['ui_component_id' => $iUiComponentId]);
        }

        if($oModel instanceof ActiveRecordInterface) {
            $aOutPhpName = $oModel->toArray(TableMap::TYPE_PHPNAME);
            $aOutResult = $oModel->toArray(TableMap::TYPE_FIELDNAME);
            $aFieldNames = array_keys($aOutResult);

            foreach($aOutPhpName as $sCol => $sVal)
            {
                $sKeyName = array_shift($aFieldNames);

                if(strpos($sCol, 'Fk') === 0)
                {
                    $sGetter = 'get' . ucfirst(str_replace('fk_', '', $sKeyName ));
                    $sDestName = preg_replace('/fk_component_[a-z]+_/', '', $sKeyName);
                    $oChildModel = $oModel->$sGetter();
                    if($oChildModel)
                    {
                        $aOutResult[$sDestName] = $oModel->$sGetter()->getItemKey();
                    }
                }
            }

            if(isset($aOutResult['manager']))
            {
                $sManagerFqn = $aOutResult['manager'];
                $oInstanceManager = new $sManagerFqn();
                $aOutResult['query_object'] = $oInstanceManager->getQueryObject();
            }

            $aOutResult['ui_manager'] = (new Reflector($oManager))->getName();

            return $aOutResult;
        }
        return [];
    }

    function getManager():IFormManager
    {
        $oReflector = new Reflector($this);
        $sName = $oReflector->getName();
        $aParts = array_reverse(explode('\\', $sName));
        $sUniqueClassPart = strtolower($aParts[1]);
        $sManagerName = '\\Crud\\Component_' . $sUniqueClassPart . '\\CrudComponent_' . $sUniqueClassPart . 'Manager';
        return new $sManagerName();
    }
    function getApiManager():IApiExposable
    {
        $oManager = $this->getManager() ;
        if($oManager instanceof IApiExposable)
        {
            return $oManager;
        }
        throw new LogicException("Manager has to implement IApiExposable.");
    }
    function getModelName():string
    {
        return 'component_' . $this->getComponentXml()->getId();
    }

    /**
     * Returns the list of component types that can be dropped on top of this component. By default an empty collection.
     * @return ComponentAcceptsNone
     */
    function accepts():IComponentAcceptManager
    {
        return new ComponentAcceptsNone();
    }

    /**
     * @param array $aProperties
     * @throws \Exception
     */
    function setProps(array $aProperties = null):void
    {
        $oComponentPropertyLoader = new ComponentPropertyDefinition();
        $this->aProperties = $oComponentPropertyLoader->loadAndMerge($this, $aProperties);
    }
    function getProps()
    {
        return $this->aProperties;
    }
    function getState()
    {

        $aDefaultState = isset($this->aProperties['default_state']) ? $this->aProperties['default_state'] : [];

        return array_merge($aDefaultState, $this->aState);
    }

    function getUiComponentType(): UIComponentType
    {
        return $this->addUiComponentType(true);
    }
    /**
     * Tries to find the UI component type database record that belongs to this component. If none exists it can
     * create one on the fly for you.
     *
     * @param bool $bCreateIfNotExists
     * @return UIComponentType
     * @throws NullPointerException
     */
    function addUiComponentType(bool $bCreateIfNotExists = true): UIComponentType
    {
        try
        {
            $oComponentXml = $this->getComponentXml();

            $oUIComponentTypeQuery = UIComponentTypeQuery::create();
            $oUIComponentTypeQuery->filterByComponentClass($this->getClassName());

            if($bCreateIfNotExists)
            {
                $oUIComponentType = $oUIComponentTypeQuery->findOneOrCreate();

                $oUIComponentType->setIsApp($oComponentXml->getIsApp());
                $oUIComponentType->setIsWeb($oComponentXml->getisWeb());
                $oUIComponentType->setAcceptAny(true);
                $oUIComponentType->setTitle($this->getName());
                $oUIComponentType->save();
                return $oUIComponentType;
            }

            $oUIComponentType = $oUIComponentTypeQuery->findOne();

            if(!$oUIComponentType instanceof UIComponentType)
            {
                throw new NullPointerException("UIComponentType record not found.");
            }
        }
        catch (PropelException $e)
        {
            Except_log::register($e, true);
        }

        return $oUIComponentType;
    }

    /**
     * Use reflection to get the fully qualified classname of the current component.
     * @return string
     */
    function getClassName():string
    {
        $oReflector = new Reflector($this);
        return $oReflector->getName();
    }

    function getComponentXml():Component
    {
        return ComponentXmlParser::parse($this);
    }
    abstract function render();
}
