<?php
namespace LowCode\Component\ListView;

use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{
    function getSections():array
    {
        return $this->getPropertyById('sections');
    }

}
