<?php
namespace LowCode\Component\ListView;

use Exception\InvalidArgumentException;
use Exception\NullPointerException;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\ComponentAcceptsItem;
use LowCode\Behavior\FieldAcceptsAnyComponent;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;

class Component extends AbstractComponent implements IComponent
{
    function getName():string
    {
        return 'Find view';
    }
    function getDescription():string
    {
        return 'Component that acts as a container for other components';
    }
    function getProps():Props
    {
        return new Props(parent::getProps());
    }
    function getState():State
    {
        return new State(parent::getState());
    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oComponentAcceptManager->addDroppableField('view', new FieldAcceptsAnyComponent());
        return $oComponentAcceptManager;
    }


    function render(): array
    {
        $oProps = $this->getProps();
        $oState = $this->getState();
        $aState = array_merge($oProps->getDefaultState(), $oState->toArray());

        return [
            'state' => $aState,
            'props' => $oProps->toArray(),
            'result' => '',
        ];


        $sDefaultActive = $this->getProps()->getPropertyById('default_active');
        $aViewData = [];
        foreach($oProps->getSections() as $sKey => $aSectionProps)
        {
            if(!isset($aSectionProps['id']))
            {

                $aSectionProps['id'] = str_replace('.', '_', join('_', [$oProps->getId(), $sKey]));
            }

            if(!isset($aSectionProps['type']))
            {
                throw new InvalidArgumentException("Variable type is missing in " . $aSectionProps['id'] . " item");
            }

            $aViewData['state'] = $aState;
            $aViewData['props'] = $aSectionProps;
            $aViewData['result'][$sKey] = ComponentFactory::produce($aSectionProps['type'], $aSectionProps, $aState)->render();
        }
        reset($aViewData['result']);

        $aViewData['props']['first'] = key($aViewData['result']);
        return $aViewData;
    }
}
