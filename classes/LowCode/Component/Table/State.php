<?php
namespace LowCode\Component\Table;

use LowCode\IState;

class State implements IState
{
    private $aData;

    function __construct(array $aData = null)
    {
        $this->aData = $aData;
    }

    function toArray():array
    {
        return $this->aData;
    }
}


