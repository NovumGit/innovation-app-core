<?php
namespace LowCode\Component\Table;

use Crud\FormManager;
use Exception\FatalErrorException;
use Exception\InvalidConfigurationException;
use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class Props extends CoreProps implements ICoreProps
{
    function getManager():?FormManager
    {
        $sFqn = $this->getPropertyById('manager');

        if(empty($sFqn))
        {
            return null;
        }

        $oManager = new $sFqn;

        if(!$oManager instanceof FormManager)
        {
            throw new FatalErrorException("Could not locate manager class $sFqn");
        }
        return $oManager;
    }
    function getTableFooter():?array
    {
        $aTableFooterArguments = $this->getPropertyById('table_footer');
        return $aTableFooterArguments;
    }
    function getDialogs():?array
    {
        $aDialog = $this->getPropertyById('dialogs');
        return $aDialog;
    }

    function getQueryObject():?ModelCriteria
    {
        $sQueryObjectName = $this->getPropertyById('query_object');

        if(empty($sQueryObjectName))
        {
            return null;
        }

        return $sQueryObjectName::create();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function getLayoutKey():?string
    {
        $sLayoutKey = $this->getPropertyById('layout_key');

        if(empty($sLayoutKey))
        {
            return null;
        }

        return $sLayoutKey;
    }
    /*
    function getRenderStyle():string
    {
        return $this->getPropertyById('render_style');
    }
    function getButtonLocation():string
    {
        return $this->getPropertyById('button_location');
    }
    function getTitle():string
    {
        return $this->getPropertyById('title');
    }
    */
}
