<?php
namespace LowCode\Component\Form;

use Hurah\Types\Type\Url;
use Crud\FormManager;
use Exception\FatalErrorException;
use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{
    function getManager():FormManager
    {
        $sFqn = $this->getPropertyById('manager');

        if(!class_exists($sFqn))
        {
            throw new FatalErrorException("Manager class not found: $sFqn in " . $this->getPropertyById('id'));
        }

        $oManager = new $sFqn;

        if(!$oManager instanceof FormManager)
        {
            throw new FatalErrorException("Could not locate manager class $sFqn");
        }
        return $oManager;
    }
    function getDefaults():array
    {
        return $this->getPropertyById('defaults') ?? [];
    }
    function dialog():?array
    {
        return $this->getPropertyById('dialog');
    }

    function getHeadingComponents():?array
    {
        return $this->getPropertyById('heading_components');
    }
    function getPanelFooterComponents():?array
    {
        return $this->getPropertyById('panel_footer_components');
    }
    function getComponentKey():string
    {
        return $this->getPropertyById('component_key');
    }
    function getRenderStyle():string
    {
        return $this->getPropertyById('render_style');
    }
    function getLayoutKey():string
    {
        return $this->getPropertyById('layout_key');
    }
    function getButtonLocation():string
    {
        $sButtonLocation = $this->getPropertyById('button_location');
        if($sButtonLocation == 'below_form')
        {
            $sButtonLocation = 'in_form';
        }
        return $sButtonLocation ?? '';
    }

    function getTitleLocked():bool
    {
        return $this->getPropertyById('title_locked') ? true : false;
    }
    function getTitle():string
    {
        return $this->getPropertyById('title');
    }
    function getConfigUrl():Url
    {
        return $this->getManager()->getFormEditUrl($this->getRenderStyle(), $this->getTitle());
    }
}
