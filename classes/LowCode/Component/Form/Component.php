<?php
namespace LowCode\Component\Form;

use Core\Debug;
use Core\DeferredAction;
use Core\Logger;
use Core\Reflector;
use Core\Setting;
use Core\TemplateFactory;
use Core\Utils;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\FieldAcceptsCollection;
use LowCode\Behavior\FieldAcceptsItem;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;
use Ui\RenderEditConfig;

class Component extends AbstractComponent
{
    function getName(): string
    {
        return 'IForm';
    }
    function getDescription(): string
    {
        return 'Generates forms on the fly';
    }
    function getState():State
    {
        return new State(parent::getState());
    }
    function getProps(): Props
    {
        return new Props(parent::getProps());
    }


    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oFieldAcceptsItem = new FieldAcceptsItem(new \LowCode\Component\Dialog\Component());

        $oComponentAcceptManager->addDroppableField('dialog', $oFieldAcceptsItem);

        $oFieldAcceptsCollection = new FieldAcceptsCollection();
        $oFieldAcceptsCollection->addItem(new FieldAcceptsItem(new \LowCode\Component\Button\Component()));
        $oFieldAcceptsCollection->addItem(new FieldAcceptsItem(new \LowCode\Component\ButtonGroup\Component()));

        $oComponentAcceptManager->addDroppableField('heading_components', $oFieldAcceptsCollection);
        $oComponentAcceptManager->addDroppableField('panel_footer_components', $oFieldAcceptsCollection);

        return $oComponentAcceptManager;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    function render():array
    {
        $aState = $this->getState()->toArray();
        $oProps = $this->getProps();
        $oManager = $oProps->getManager();
        $aProperties = $oProps->toArray();

        /*
        if($aState['_do'] == 'Save')
        {
            // var_dump($oProps);
        }
        */
        if(isset($aState[$oProps->getLayoutKey()]))
        {
            Setting::store($oProps->getLayoutKey(), json_encode($aState[$oProps->getLayoutKey()]));
        }

        $sPrevValues = Setting::get($oProps->getLayoutKey());

        $aData = json_decode($sPrevValues, true) ?? [];

        $aData = array_merge($aData, $oProps->getDefaults(), ['ui_config' => $this->getConfig((int)$this->getProps()->getUiComponentId())], $aState ?? []);

        unset($aData['id']);
        $oModel = $oManager->getModel($aData);

        $sRenderStyle = $this->getProps()->getRenderStyle();
        $oProps->getButtonLocation();

        $oRenderEditConfig = new RenderEditConfig($oProps->getTitle(), $sRenderStyle);
        $oRenderEditConfig->setFieldArrayName($oProps->getLayoutKey());
        $oRenderEditConfig->setButtonsEnabled(false);
        $oRenderEditConfig->setAddDoField(false);

        $aTemplateData = [];
        $aCollections = ['dialog', 'heading_components', "panel_footer_components"];

        foreach($aCollections as $sKey)  {
            if(isset($aProperties[$sKey]) && is_array($aProperties[$sKey]))
            {
                foreach ($aProperties[$sKey] as $sChildKey => $aChildProperties)
                {
                    if(!isset($aChildProperties['id']))
                    {
                        $aChildProperties['id'] = $sChildKey;
                    }

                    $aState = $this->getState()->toArray();
                    if(isset($aChildProperties['state']))
                    {
                        $aState = array_merge($aState, $aChildProperties['state']);
                        unset($aChildProperties['state']);
                    }
                    if(isset($aProperties['defaults']))
                    {
                        $aChildProperties['defaults'] = array_merge($aProperties['defaults'], $aChildProperties['defaults'] ?? []);
                    }

                    $aState['context']['form']['id'] = $oProps->getId();


                    if(isset($aChildProperties['props']))
                    {

                        // Wordt waarschijnlijk niet gebruikt
                        $oChildComponent = ComponentFactory::produce($aChildProperties['props']['type'], $aChildProperties['props'], $aState);
                    }
                    else
                    {
                        $oChildComponent = ComponentFactory::produce($aChildProperties['type'], $aChildProperties, $aState);
                    }


                    if($oChildComponent instanceof IComponent)
                    {
                        $aTemplateData[$sKey] = $aTemplateData[$sKey] ?? [];
                        $aTemplateData[$sKey][] = $oChildComponent->render();
                    }
                    // Dialog actually has no output but when this changes it will be hard to find why it is not passed
                    // trough the stack
                }
            }
        }

        $oRenderEditConfig->lockTitle($oProps->getTitleLocked());
        $aTemplateData['ui_component_id'] = $this->getProps()->getUiComponentId();
        $oRenderEditConfig->addGlobals($aTemplateData);

        $sFormHtml = $oManager->getRenderedEditHtml($oModel, $oProps->getLayoutKey(), $oRenderEditConfig);

        $sCurrentDir = (new Reflector($this))->getClassDir();

        $sTemplate = file_get_contents($sCurrentDir . '/container.twig');
        $sForm = TemplateFactory::parseFromString($sTemplate, [

            'form' => $sFormHtml,
            'form_id' => $this->getProps()->getId(),
            'props' => $oProps
        ]);

        $aOut['result'] = $sForm;

        DeferredAction::register($oProps->getId(), Utils::getRequestUri());

        $aDialogProps = $this->getProps()->dialog();


        $aOut['state'] = $this->getState()->toArray();
        $aOut['props'] = $this->getProps()->toArray();
        $aOut['props']['edit_url'] =  $oManager->getFormEditUrl($oProps->getLayoutKey(), $oProps->getTitle());

        return $aOut;
    }
}


