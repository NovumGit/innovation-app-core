<?php
namespace LowCode\Component\ButtonGroup;

use Exception\InvalidArgumentException;
use Exception\NullPointerException;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\ComponentAcceptsItem;
use LowCode\Behavior\FieldAcceptsItem;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponentAcceptedChildren;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;

class Component extends AbstractComponent implements IComponent
{
    function getName():string
    {
        return 'Button group';
    }
    function getDescription():string
    {
        return 'Component that handles (nested) groups of buttons';
    }
    function getProps():Props
    {
        return new Props(parent::getProps());
    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oComponentAcceptsItem = new FieldAcceptsItem(new \LowCode\Component\Button\Component());
        $oComponentAcceptManager->addDroppableField('buttons', $oComponentAcceptsItem);

        $oComponentAcceptsItem = new FieldAcceptsItem(new \LowCode\Component\ButtonGroup\Component());
        $oComponentAcceptManager->addDroppableField('buttons', $oComponentAcceptsItem);

        return $oComponentAcceptManager;
    }

    function render(): array
    {
        return [
            'state' => [],
            'props' => [],
            'result' => '<button>tijdelijke button group</button>',
        ];
    }
}
