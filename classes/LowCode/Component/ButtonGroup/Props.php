<?php
namespace LowCode\Component\ButtonGroup;

use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{
    function getSections():array
    {
        return $this->getPropertyById('sections');
    }

}
