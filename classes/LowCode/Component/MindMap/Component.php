<?php

namespace LowCode\Component\MindMap;

use Exception\LogicException;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;
use LowCode\Utils\Web;

class Component extends AbstractComponent implements IComponent {
    function getName(): string {
        return 'Mind map';
    }

    function getDescription(): string {
        return 'A simple mindmap generator';
    }

    function getProps(): Props {
        return new Props(parent::getProps());
    }

    function render(): array {
        $oProps = $this->getProps();

        if (!$oProps instanceof Props) {
            throw new LogicException("Expected an instance of " . Props::class);
        }
        $aProps = $oProps->toArray();
        $aState = $this->getState();
        $aState['query_object'] = $oProps->getQueryObject();

        $aViewData = [
            'props' => $aProps,
            'state' => $aState,
        ];

        $oWebUtils = new Web($this);
        $aOutput = [];
        $aOutput[] = $oWebUtils->makeInlineCss(['mindmap.css']);
        $aOutput[] = $oWebUtils->makeInlineJs([
            'jsmind.js',
            'jsmind.draggable.js',
        ]);
        $aOutput[] = $oWebUtils->parse('mindmap-html.twig', $aViewData);

        return [
            'result' => join(PHP_EOL, $aOutput),
            'props'  => $aProps,
            'state'  => $aState,
        ];
    }
}
