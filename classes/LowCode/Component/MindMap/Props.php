<?php

namespace LowCode\Component\MindMap;

use Exception\InvalidConfigurationException;
use LowCode\Contracts\Property\ICoreProps;
use LowCode\CoreProps;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class Props extends CoreProps implements ICoreProps {
    function getQueryObject(): ModelCriteria {
        $sQueryObjectName = $this->getPropertyById('query_object');

        if (empty($sQueryObjectName)) {
            throw new InvalidConfigurationException("Element query_object is missing in block " . $this->getId());
        }

        return $sQueryObjectName::create();
    }

}
