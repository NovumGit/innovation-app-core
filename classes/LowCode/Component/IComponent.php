<?php
namespace LowCode\Component;

use Crud\IApiExposable;
use Crud\IFormManager;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\IAppComponent;
use LowCode\System\Parser\ComponentXml\Component;
use Model\System\UI\UIComponentType;

interface IComponent extends IAppComponent
{
    function getName():string;
    function getDescription():string;

    function getManager():IFormManager;
    function getApiManager():IApiExposable;

    function getConfig(int $iUiComponentId):array;

    /**
     * Returns the list of component types that can be dropped on top of this component. By default an empty collection.
     * @return ComponentAcceptsManager
     */
    function accepts():IComponentAcceptManager;
    function render();
    function getComponentXml():Component;
    /**
     * Use reflection to get the fully qualified classname of the current component.
     * @return string
     */
    function getClassName():string;

    /**
     * Returns the name of the UI component table that contains instance settings
     * @return string
     */
    function getModelName():string;

    /**
     * Returns the UIComponentType record that belongs to this component.
     *
     * @param bool $true
     * @return mixed
     */
    public function addUiComponentType(bool $bCreateIfNotExists = true):UIComponentType;

}
