<?php

namespace LowCode\Component\Button;

use Core\Logger;
use Core\Utils;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\FieldAcceptsAnyEvent;
use LowCode\Behavior\FieldAcceptsCollection;
use LowCode\Behavior\FieldAcceptsItem;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Event\IEvent;
use LowCode\EventFactory;

class Component extends AbstractComponent
{

    function events(): IComponentAcceptManager
    {

    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();

        $oFieldAcceptsCollection = new FieldAcceptsCollection();
        $oFieldAcceptsCollection->addItem(new FieldAcceptsAnyEvent());
        $oFieldAcceptsCollection->addItem(new FieldAcceptsItem(new \LowCode\Component\Modal\Component()));
        $oFieldAcceptsCollection->addItem(new FieldAcceptsItem(new \LowCode\Component\Dialog\Component()));

        $oComponentAcceptManager->addDroppableField('click', $oFieldAcceptsCollection);
        return $oComponentAcceptManager;
    }

    function getName(): string
    {
        return 'Button';
    }

    function getDescription(): string
    {
        return 'Generates buttons to apply actions to';
    }

    function getState(): State
    {
        return new State(parent::getState());
    }

    function getProps(): Props
    {
        return new Props(parent::getProps());
    }

    function eventTriggers(): ?array
    {
        return ['click', 'delete'];
    }

    function render(): array
    {
        $oProps = $this->getProps();
        $sAlignment = $oProps->getAlignment();
        $sBtnSize = $this->getProps()->getSize();
        $sColorType = $this->getProps()->getColorType();

        $oProps = $this->getProps();
        $aProperties = $oProps->toArray();
        $aEventData = [];

        $aCollections = ['click', 'delete'];

        $aOut = [];
        $sButtonId = $this->getProps()->getId();

        foreach ($aCollections as $sKey)
        {
            if (isset($aProperties[$sKey]) && is_array($aProperties[$sKey]))
            {
                foreach ($aProperties[$sKey] as $sChildKey => $aChildProperties)
                {

                    if (!isset($aChildProperties['id']))
                    {
                        $aChildProperties['id'] = $sChildKey;
                    }
                    $aEventState = $this->getState()->toArray();

                    if(isset($aProperties['defaults']))
                    {
                        $aChildProperties['defaults'] = array_merge($aProperties['defaults'], $aChildProperties['defaults'] ?? []);
                    }

                    $aEventProps = array_merge($aChildProperties, ['context' => ['id' => $sButtonId]]);
                    $oEvent = EventFactory::produce($aChildProperties['type'], $aEventProps, $aEventState);

                    if ($oEvent instanceof IEvent)
                    {
                        $aEventData[$sKey][$aChildProperties['id']] = $aTemplateData[$sKey] ?? [];
                        $aEventData[$sKey][$aChildProperties['id']]['actions'][] = $oEvent->render();
                    }
                }
            }
        }


        $sClasses = join(' ', [
            'event_button', 'btn', 'btn-default', 'btn-' . $sColorType, 'ml10', 'btn-' . $sBtnSize, $sAlignment]);


        $aButtonEventSteps = [];
        if ($aEventData)
        {
            foreach ($aEventData as $sEventType => $aEventProperties)
            {
                $aButtonEventSteps = [];
                foreach($aEventProperties as $sButtonEventId => $aButtonProperties)
                {
                    foreach ($aButtonProperties['actions'] as $sAction)
                    {
                        $aButtonEventSteps[] = $sAction;
                    }
                }
            }
        }

        $aOut[] = '<a data-event="' . htmlspecialchars(json_encode($aButtonEventSteps), ENT_QUOTES, 'UTF-8') . '" id="btn_' . $sButtonId . '" title="' . $this->getProps()->getLabel() . '" class="' . $sClasses . '" href="xx">';
        if ($this->getProps()->getIcon())
        {
            $aOut[] = '<i class="fa fa-' . $this->getProps()->getIcon() . '"></i>';
        }
        $aOut[] = $this->getProps()->getLabel();
        $aOut[] = '</a>';


        return [
            'state' => $this->getState()->toArray(), 'props' => $this->getProps()->toArray(), 'result' => join(PHP_EOL, $aOut),];
    }
}
