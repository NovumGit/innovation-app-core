<?php
namespace LowCode\Component\Button;

use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{
    function getEvent(): string
    {
        return $this->getPropertyById('event');
    }

    function getBehavior(): string
    {
        return $this->getPropertyById('behavior');
    }

    function getPage(): string
    {
        return $this->getPropertyById('page');
    }

    function getColorType(): string
    {
        return $this->getPropertyById('color_type') ?? 'info';
    }

    function getFlow(): string
    {
        return $this->getPropertyById('flow');
    }
    function getAlignment(): string
    {
        $sAlign = $this->getPropertyById('align');
        if($sAlign == 'left')
        {
            return 'pull-left';
        }
        else if($sAlign == 'right')
        {
            return 'pull-right';
        }
        return '';
    }
    function getSize(): string
    {
        return $this->getPropertyById('size');
    }
    function getClick(): array
    {
        return $this->getPropertyById('click');
    }
    function getIcon(): ?string
    {
        return $this->getPropertyById('icon');
    }
    function getTitle(): string
    {
        $sTitle =  $this->getPropertyById('title');
        return $sTitle ?? 'Nieuwe knop';
    }
    function getLabel(): string
    {
        $sLabel = $this->getPropertyById('label');
        return $sLabel ?? $this->getTitle();
    }

    function getInfo(): ?string
    {
        return $this->getPropertyById('info');
    }
}
