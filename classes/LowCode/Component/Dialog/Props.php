<?php
namespace LowCode\Component\Dialog;

use Crud\FormManager;
use Exception\FatalErrorException;
use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{
    function getMessage(): string
    {
        return $this->getPropertyById('message');
    }
    function getTitle(): string
    {
        return $this->getPropertyById('title');
    }
    function getColorType(): string
    {
        return $this->getPropertyById('color_type');
    }
    function getDisplay(): string
    {
        return $this->getPropertyById('display');
    }
    function getButtons():?array
    {
        return $this->getPropertyById('buttons');
    }

}
