<?php
namespace LowCode\Component\Dialog;

use Core\InlineTemplate;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\TemplateFactory;
use Core\UserRegistry;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\FieldAcceptsItem;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;
use Ui\RenderEditConfig;

class Component extends AbstractComponent implements IComponent
{
    function getName(): string
    {
        return 'Dialog';
    }
    function getDescription(): string
    {
        return 'Generates info modals on the fly';
    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oFieldAcceptsItem = new FieldAcceptsItem(new \LowCode\Component\Button\Component());
        $oComponentAcceptManager->addDroppableField('buttons', $oFieldAcceptsItem);

        return $oComponentAcceptManager;
    }


    function getState():State
    {
        return new State(parent::getState());
    }
    function getProps(): Props
    {
        return new Props(parent::getProps());
    }

    /**
     * @return mixed
     */
    /*
    function render():void
    {
        $oProps = $this->getProps();
        $aProperties = $oProps->toArray();

        $oProps->getMessage();
        $oProps->getTitle();
        $oProps->getButtons();

        if(isset($aProperties))
        {
            $aDialog = $aProperties;
            $aButtons = [];
            if(isset($aDialog['buttons']))
            {
                foreach($aDialog['buttons'] as $aButton)
                {
                    $aButtons[] = new StatusMessageButton($aButton['label'], $aButton['action'], $aButton['label'],'danger');
                }
            }
            StatusModal::info($aDialog['message'], $aDialog['title'], $aButtons);
        }
    }
    */

    /**
     * @return array
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function render(): array
    {
        $oState = $this->getState();
        $oProps = $this->getProps();
        $aProperties = $oProps->toArray();
        $aButtons = [];

        if (isset($aProperties))
        {

            if (isset($aProperties['buttons']))
            {
                foreach ($aProperties['buttons'] as $aButtonProperties)
                {
                    $oButton = ComponentFactory::produce($aButtonProperties['type'], $aButtonProperties, $this->getState()->toArray());

                    if ($oButton instanceof \LowCode\Component\Button\Component)
                    {
                        $oButtonProps = $oButton->getProps();

                        $aButtons[] = new StatusMessageButton($oButtonProps->getLabel(), $oButtonProps->getEvent(), $oButtonProps->getLabel(), $oButtonProps->getColorType());
                    }
                }
            }
        }




        $sResult = '';
        $bDisplay = false;
        $sModalShowStatusStoreKey = 'modal_' . $aProperties['id'] . '_shown';
        if($oProps->getDisplay() == 'always')
        {
            $bDisplay = true;
        }
        else if($oProps->getDisplay() == 'once_session' && !isset($_SESSION[$sModalShowStatusStoreKey]))
        {
            $_SESSION[$sModalShowStatusStoreKey] = true;
            $bDisplay = true;
        }
        else if($oProps->getDisplay() == 'once_user' && !UserRegistry::get($sModalShowStatusStoreKey))
        {
            UserRegistry::get($sModalShowStatusStoreKey, true);
            $bDisplay = true;

        }

        if($bDisplay)
        {
            $sResult = StatusModal::asString($oProps->getMessage(), $oProps->getColorType(), $oProps->getTitle(), $aButtons);
        }
        return [
            'result' => $sResult,
            'props' => $aProperties,
            'state' => $oState->toArray()
        ];
    }
}
