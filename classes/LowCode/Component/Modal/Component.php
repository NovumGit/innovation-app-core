<?php

namespace LowCode\Component\Modal;

use Core\InlineTemplate;
use Core\StatusMessageButton;
use Core\StatusModal;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\FieldAcceptsItem;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\ComponentFactory;

class Component extends AbstractComponent
{
    function getName(): string
    {
        return 'Modal';
    }

    function getDescription(): string
    {
        return 'Generates info modals on the fly';
    }

    function getState(): State
    {
        return new State(parent::getState());
    }

    function getProps(): Props
    {
        return new Props(parent::getProps());
    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oComponentAcceptsItem = new FieldAcceptsItem(new \LowCode\Component\Button\Component());
        $oComponentAcceptManager->addDroppableField('button', $oComponentAcceptsItem);

        return $oComponentAcceptManager;
    }

    /**
     * @return mixed
     */
    function render(): array
    {
        $oProps = $this->getProps();
        $aProperties = $oProps->toArray();

        if (isset($aProperties['dialog']))
        {
            $aDialog = $aProperties['dialog'];
            $aButtons = [];
            if (isset($aDialog['buttons']))
            {
                foreach ($aDialog['buttons'] as $aButton)
                {
                    $aNewProps = array_merge($this->getProps()->toArray(), $aDialog);
                    $oButton = ComponentFactory::produce($aDialog['type'], $aNewProps, $this->getState()->toArray())->render();

                    if ($oButton instanceof \LowCode\Component\Button\Component)
                    {
                        $oProps = $oButton->getProps();

                        $aButtons[] = new StatusMessageButton($oProps->getLabel(), $oProps->getEvent(), $oProps->getLabel(), $oProps->getColorType());
                    }
                }
            }
            $sModal = StatusModal::asString($oProps->getMessage(), $oProps->getColorType(), $oProps->getTitle(), $aButtons);


            return [
                'result' => $sModal,
                'props' => $aProperties,
                'state' => $oState->toArray()
            ];
        }
    }
}
