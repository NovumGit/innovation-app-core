<?php
namespace LowCode\Component\View;

use Exception\InvalidArgumentException;
use Exception\NullPointerException;
use LowCode\Behavior\ComponentAcceptsManager;
use LowCode\Behavior\ComponentAcceptsItem;
use LowCode\Behavior\FieldAcceptsAnyComponent;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;
use Model\System\UI\UIComponentQuery;

class Component extends AbstractComponent implements IComponent
{
    function getName():string
    {
        return 'View';
    }
    function getDescription():string
    {
        return 'A view is a container for other components. It has no functionality by it self but it helps you layout your app.';
    }
    function getProps():Props
    {
        return new Props(parent::getProps());
    }
    function getState():State
    {
        return new State(parent::getState());
    }

    /**
     * @return ComponentAcceptsManager
     * @throws \Exception
     */
    function accepts(): IComponentAcceptManager
    {
        $oComponentAcceptManager = new ComponentAcceptsManager();
        $oComponentAcceptManager->addDroppableField('view', new FieldAcceptsAnyComponent());
        return $oComponentAcceptManager;
    }


    function render(): array
    {
        $oProps = $this->getProps();
        $oState = $this->getState();

        $aState = array_merge($oProps->getDefaultState(), $oState->toArray());

        $sDefaultActive = $this->getProps()->getPropertyById('default_active');
        $aViewData = [];

        $aSections = UIComponentQuery::create()->findByParentId($this->getProps()->getUiComponentId());

        foreach($aSections as $oChildUiComponent)
        {
            $oUiComponentType = $oChildUiComponent->getUIComponentType();
            $oComponent = $oUiComponentType->getComponent();
            $aChildProps = $oComponent->getConfig($oChildUiComponent->getId());

            $oChildComponent = $oUiComponentType->getComponent($aChildProps, []);
            $aViewData['result'][] = $oChildComponent->render();
        }

        if($oProps->getSections())
        {
            // Legacy, comes from json, can maybe be removed later
            foreach($oProps->getSections() as $sKey => $aSectionProperties)
            {
                $aSectionProps['props'] = $aSectionProperties;

                if(!isset($aSectionProps['id']))
                {

                    $aSectionProps['id'] = str_replace('.', '_', join('_', [$oProps->getId(), $sKey]));
                }

                if(!isset($aSectionProps['props']['type']))
                {
                    throw new InvalidArgumentException("Variable type is missing in " . $aSectionProps['id'] . " item");
                }

                $aViewData['state'] = $aState;
                $aViewData['props'] = $aSectionProps;
                $aViewData['result'][$sKey] = ComponentFactory::produce($aSectionProps['props']['type'], $aSectionProps['props'], $aState)->render();
            }
            reset($aViewData['result']);
            $aViewData['props']['first'] = key($aViewData['result']);
        }



        return $aViewData;
    }
}
