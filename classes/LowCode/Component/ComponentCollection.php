<?php
namespace LowCode\Component;


class ComponentCollection implements \Iterator
{
    private $aComponents = [];

    /**
     * @param IComponent[] $aComponents
     */
    public function __construct(array $aComponents = null)
    {
        $this->aComponents = $aComponents;
    }
    public function add(IComponent $oField)
    {
        $this->aComponents[] = $oField;
    }

    function next(): void
    {
        next($this->aComponents);
    }

    function valid(): bool
    {
        $key = key($this->aComponents);
        return ($key !== null && $key !== false);
    }

    function current(): IComponent
    {
        return current($this->aComponents);
    }

    function rewind()
    {
        reset($this->aComponents);
    }

    function key(): int
    {
        return key($this->aComponents);
    }
}
