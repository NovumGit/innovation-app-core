<?php
namespace LowCode;

use Core\Logger;
use LowCode\Component\AbstractComponent;
use LowCode\Component\IComponent;

class App extends AbstractComponent implements IComponent
{
    function config()
    {
        return [
            'Formulier titel', 'title', 'tag', 'string', true
        ];
    }

    function getName(): string
    {
        return 'Root component';
    }
    function getDescription(): string
    {
        return 'This is the starting point of loading anything';
    }

    /**
     * Render is uniek voor elk component type en geeft een array terug met props en state.
     * Alles komt als een grote array terug in de controller en die maakt er een html pagina of app view van.
     *
     * @return array
     */
    function render(): array
    {
        $aProps = $this->getProps();

        $sType = $aProps['type'];

        return ComponentFactory::produce($sType, $aProps, $this->getState())->render();
    }

}
