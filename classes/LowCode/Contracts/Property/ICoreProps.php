<?php
namespace LowCode\Contracts\Property;

interface  ICoreProps
{
    function __construct(array $aData = null);
    function getId():?string;
    function getPropertyById($sKey);
    function toArray():array;
    function getDefaultState():array;
}
