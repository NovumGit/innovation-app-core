<?php
namespace LowCode\Contracts\State;

interface  ICoreState
{
    function __construct(array $aData = null);
    function toArray():array;
}
