<?php
namespace LowCode\Event\Validate;

use LowCode\Event\AbstractEvent;

class Event extends AbstractEvent
{
    /**
     * AbstractEvent constructor.
     * @param array|null $aProperties
     * @param array|null $aState
     * @throws \Exception
     */
    function __construct(array $aProperties = null, array $aState = null)
    {
        $this->setProps($aProperties);

        $aState['get'] = $_GET;
        $aState['post'] = $_POST;

        parent::__construct($aProperties, $aState);
    }

    function getName(): string
    {
        return 'Validate component';
    }
    function getDescription(): string
    {
        return 'Validate a message';
    }
    function getState():State
    {
        return new State(parent::getState());
    }
    function getProps(): Props
    {
        return new Props(parent::getProps());
    }
    function isLast(): bool
    {
        return false;
    }
    function render(): array
    {
        $oProps = $this->getProps();
        $oState = $this->getState();

        return ['name' => 'Validate', 'props' => $oProps->toArray(), 'state' => $oState->toArray()];
    }
}
