<?php
namespace LowCode\Event\Delete;

use LowCode\CoreState;
use LowCode\Contracts\State\ICoreState;

class State extends CoreState implements ICoreState
{

    function getId():?int
    {
        return $this->toArray()['id'] ?? null;
    }
}
