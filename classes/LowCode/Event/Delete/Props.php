<?php
namespace LowCode\Event\Delete;

use LowCode\CoreProps;
use LowCode\Contracts\Property\ICoreProps;

class Props extends CoreProps implements ICoreProps
{

    public function getEndpoint():string
    {
        return $this->getPropertyById('endpoint');
    }
}
