<?php
namespace LowCode\Event;

use LowCode\IAppComponent;

interface IEvent extends IAppComponent
{
    function render():array;
    function isLast():bool;
}
