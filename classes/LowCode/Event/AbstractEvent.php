<?php
namespace LowCode\Event;

use Core\Reflector;
use Exception\NullPointerException;
use LowCode\Behavior\ComponentAcceptsNone;
use LowCode\Behavior\IComponentAcceptManager;
use LowCode\System\ComponentPropertyDefinition;
use LowCode\System\Parser\ComponentXml\Component;
use LowCode\System\Parser\ComponentXmlParser;
use Model\Logging\Except_log;
use Model\System\UI\UIComponentType;
use Model\System\UI\UIComponentTypeQuery;
use Propel\Runtime\Exception\PropelException;

abstract class AbstractEvent implements IEvent
{
    private $aState;
    private $aProperties;

    /**
     * AbstractEvent constructor.
     * @param array|null $aProperties
     * @param array|null $aState
     * @throws \Exception
     */
    function __construct(array $aProperties = null, array $aState = null)
    {
        $this->setProps($aProperties);

        $aState['get'] = $_GET;
        $aState['post'] = $_POST;

        $this->aState = $aState;
    }

    function isLast(): bool
    {
        return false;
    }

    /**
     * Returns the list of component types that can be dropped on top of this component. By default an empty collection.
     * @return ComponentAcceptsNone
     */
    function accepts():IComponentAcceptManager
    {
        return new ComponentAcceptsNone();
    }

    /**
     * @param array $aProperties
     * @throws \Exception
     */
    function setProps(array $aProperties):void
    {
        $oComponentPropertyLoader = new ComponentPropertyDefinition();
        $this->aProperties = $oComponentPropertyLoader->loadAndMerge($this, $aProperties);
    }
    function getProps()
    {
        return $this->aProperties;
    }
    function getState()
    {

        $aDefaultState = isset($this->aProperties['default_state']) ? $this->aProperties['default_state'] : [];

        return array_merge($aDefaultState, $this->aState);
    }

    /**
     * Tries to find the UI component type database record that belongs to this component. If none exists it can
     * create one on the fly for you.
     *
     * @param bool $bCreateIfNotExists
     * @return UIComponentType
     * @throws NullPointerException
     */
    function addUiComponentType(bool $bCreateIfNotExists = true): UIComponentType
    {
        try
        {
            $oComponentXml = $this->getComponentXml();

            $oUIComponentTypeQuery = UIComponentTypeQuery::create();
            $oUIComponentTypeQuery->filterByComponentClass($this->getClassName());

            if($bCreateIfNotExists)
            {
                $oUIComponentType = $oUIComponentTypeQuery->findOneOrCreate();

                $oUIComponentType->setIsApp($oComponentXml->getIsApp());
                $oUIComponentType->setIsWeb($oComponentXml->getisWeb());
                $oUIComponentType->setAcceptAny(true);
                $oUIComponentType->setTitle($this->getName());
                $oUIComponentType->save();
                return $oUIComponentType;
            }

            $oUIComponentType = $oUIComponentTypeQuery->findOne();

            if(!$oUIComponentType instanceof UIComponentType)
            {
                throw new NullPointerException("UIComponentType record not found.");
            }
        }
        catch (PropelException $e)
        {
            Except_log::register($e, true);
        }

        return $oUIComponentType;
    }

    /**
     * Use reflection to get the fully qualified classname of the current component.
     * @return string
     */
    function getClassName():string
    {
        $oReflector = new Reflector($this);
        return $oReflector->getName();
    }

    function getComponentXml():Component
    {
        return ComponentXmlParser::parse($this);
    }
}
