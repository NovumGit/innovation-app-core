<?php
namespace LowCode\Event\Redirect;

use LowCode\Event\AbstractEvent;

class Event extends AbstractEvent
{
    /**
     * AbstractEvent constructor.
     * @param array|null $aProperties
     * @param array|null $aState
     * @throws \Exception
     */
    function __construct(array $aProperties = null, array $aState = null)
    {
        $this->setProps($aProperties);

        $aState['get'] = $_GET;
        $aState['post'] = $_POST;

        parent::__construct($aProperties, $aState);
    }

    function getName(): string
    {
        return 'Redirect component';
    }
    function getDescription(): string
    {
        return 'Redirect to a page';
    }
    function isLast(): bool
    {
        return false;
    }
    function getState():State
    {
        return new State(parent::getState());
    }
    function getProps(): Props
    {
        return new Props(parent::getProps());
    }


    function render(): array
    {
        $oProps = $this->getProps();
        $oState = $this->getState();

        return ['name' => 'Redirect', 'props' => $oProps->toArray(), 'state' => $oState->toArray()];
    }
}
