<?php
namespace LowCode\Event\Flow;

use Cassandra\Set;
use Core\DeferredAction;
use Core\Setting;
use Core\Utils;
use LowCode\Event\AbstractEvent;
use Ui\RenderEditConfig;

class Event extends AbstractEvent
{
    /**
     * AbstractEvent constructor.
     * @param array|null $aProperties
     * @param array|null $aState
     * @throws \Exception
     */
    function __construct(array $aProperties = null, array $aState = null)
    {
        $this->setProps($aProperties);

        $aState['get'] = $_GET;
        $aState['post'] = $_POST;

        parent::__construct($aProperties, $aState);
    }

    function getName(): string
    {
        return 'Flow component';
    }
    function isLast(): bool
    {
        return false;
    }
    function getDescription(): string
    {
        return 'Flow a message';
    }
    function getState():State
    {
        return new State(parent::getState());
    }
    function getProps(): Props
    {
        return new Props(parent::getProps());
    }

    function render(): array
    {
        $oProps = $this->getProps();
        $oState = $this->getState();

        return ['name' => 'Flow', 'props' => $oProps->toArray(), 'state' => $oState->toArray()];
    }

}
