<?php
namespace LowCode;

use Cli\Tools\CommandUtils;
use Exception\LogicException;
use LowCode\Component\ComponentCollection;
use LowCode\Component\IComponent;

class ComponentFactory
{

    static function getAll():ComponentCollection
    {

        $sComponentDir = CommandUtils::getRoot() . '/classes/LowCode/Component/*/Component.php';

        $aComponentDirFileList = glob($sComponentDir);
        $aComponents = [];
        foreach($aComponentDirFileList as $sComponentDirFile)
        {
            $sComponentDirFile = str_replace(CommandUtils::getRoot() . '/classes', '', $sComponentDirFile);
            $sComponentDirFile = preg_replace('/\.php$/', '', $sComponentDirFile);
            $sComponentFqn = str_replace('/', '\\', $sComponentDirFile);

            $oComponent = new $sComponentFqn([]);

            if(!$oComponent instanceof IComponent)
            {
                throw new LogicException("All components must implement IComponent, $sComponentFqn does not.");
            }
            $aComponents[] = $oComponent;
        }

        return new ComponentCollection($aComponents);
    }

    static function produce(string $sClassName, array $aProperties, array $aState = null):IAppComponent
    {
        return new $sClassName($aProperties, $aState);
    }
}
