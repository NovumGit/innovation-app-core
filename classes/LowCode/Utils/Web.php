<?php
namespace LowCode\Utils;

use Core\InlineTemplate;
use Core\Reflector;
use LowCode\Component\IComponent;
use Model\Logging\Except_log;

class Web
{
    private $oReflector;
    function __construct(IComponent $oComponent)
    {
        $this->oReflector = new Reflector($oComponent);
    }

    /**
     * Uses reflection to get the directory of the component and adds that to the filename
     * @param string $sLocalFile
     * @return string
     */
    function makePath(string $sLocalFile):string
    {
        return $this->oReflector->getClassDir() . '/' . $sLocalFile;
    }

    function makeInlineJs(array $aFiles):string
    {
        $aOutput = [];
        foreach ($aFiles as $sFile)
        {

            $aOutput[] = '<script>';
            $aOutput[] = '/*' . $sFile . '*/';
            $aOutput[] = file_get_contents($this->makePath($sFile));
            $aOutput[] = '</script>';
        }
        return join(PHP_EOL, $aOutput);
    }
    function makeInlineCss(array $aLocalCssFiles):string
    {
        $aOutput = [];
        foreach ($aLocalCssFiles as $sFile)
        {
            $aOutput[] = '<style>';
            $aOutput[] = '/*' . $sFile . '*/';
            $aOutput[] = file_get_contents($this->makePath($sFile));
            $aOutput[] = '</style>';
        }
        return join(PHP_EOL, $aOutput);
    }

    function parse($sLocalFile, $aArguments):string
    {
        try
        {

            $sFileName = $this->makePath($sLocalFile);



            $sFileContents = file_get_contents($sFileName);
            return InlineTemplate::parse($sFileContents , $aArguments);
        }
        catch (\Throwable $e)
        {
            Except_log::register($e, true);
        }
    }
}
