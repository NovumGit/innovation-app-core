<?php
namespace LowCode;

use Exception\FatalErrorException;
use LowCode\Contracts\Property\ICoreProps;

class CoreProps implements ICoreProps
{
    private $aData;

    function __construct(array $aData = null)
    {
        /*
        if(!isset($aData['id']))
        {
            throw new FatalErrorException("PropertyModel array must always contain an id, got  " . json_encode($aData));
        }
        */
        $this->aData = $aData;
    }
    function getId(): ?string
    {
        return $this->aData['id'] ?? null;
    }
    function getUiComponentId()
    {
        return $this->aData['ui_component_id'] ?? null;
    }
    function getPropertyById($sKey)
    {
        return $this->aData[$sKey] ?? null;
    }

    function toArray():array
    {
        // May be put on again
        unset($this->aData['type']);
        return $this->aData;
    }
    function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Contains stuff like active tab, form field content etc.
     * @return array
     */
    function getDefaultState(): array
    {
        return $this->aData['default_state'] ?? [];
    }
}
