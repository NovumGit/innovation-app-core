<?php

namespace LowCode\System;

use Exception\FatalErrorException;
use LowCode\Component\IComponent;
use LowCode\IAppComponent;
use LowCode\System\Parser\ComponentXml\Component;
use LowCode\System\Parser\ComponentXml\PropertyCollection;
use Model\Logging\Except_log;

class ComponentPropertyDefinition
{
    /**
     * @param IComponent $oComponent
     * @return string
     * @throws \ReflectionException
     */
    private function getComponentXml(IAppComponent $oComponent): string
    {
        $oReflector = new \ReflectionClass($oComponent);
        $sComponentClass = $oReflector->getFileName();
        $sComponentDir = dirname($sComponentClass);

        $oComponentFile = $sComponentDir . '/component.xml';

        return $oComponentFile;
    }

    /**
     * @param string $sXmlFilename
     * @return bool
     */
    private function validateXsd(string $sXmlFilename):bool
    {

        $oDom = new \DOMDocument();
        $oDom->load($sXmlFilename);

        $sSchemaDir = dirname(dirname(__FILE__));
        $sXsdFilename = $sSchemaDir . '/component.xsd';
        if (!$oDom->schemaValidate($sXsdFilename))
        {
            throw new FatalErrorException("Component XML ($sXmlFilename) does not validate against XSD ($sSchemaDir).");
        };

        return true;
    }

    function load(IAppComponent $oComponent):Component
    {
        try
        {
            $sComponentXmlFile = $this->getComponentXml($oComponent);

            $this->validateXsd($sComponentXmlFile);


            $oComponentDefinition = simplexml_load_file($sComponentXmlFile, Component::class, LIBXML_NOCDATA);

            if(!$oComponentDefinition instanceof Component)
            {
                throw new FatalErrorException("Could not build object out of component.xml");
            }

            return $oComponentDefinition;


        } catch (FatalErrorException $e)
        {
            echo $e->getMessage() . PHP_EOL;
            Except_log::register($e, true);
            exit();
        } catch (\ReflectionException $e)
        {
            echo $e->getMessage() . PHP_EOL;
            Except_log::register($e, true);
            exit();
        }
    }

    /**
     * @param array $aProperties
     * @param PropertyCollection $oPropertyCollection
     * @return array
     * @throws \Exception
     */
    private function merge(array $aProperties = null, PropertyCollection $oPropertyCollection = null):?array
    {

        if($oPropertyCollection === null)
        {
            return null;
        }
        foreach ($oPropertyCollection as $oProperty)
        {
            if($oProperty->isRequired() && !isset($aProperties[$oProperty->getKey()]))
            {
                throw new \Exception("A property of " . $oProperty->getKey() . " is mandatory but none set");
            }
            else if(!isset($aProperties[$oProperty->getKey()]) && $oProperty->getDefaultValue())
            {
                $aProperties[$oProperty->getKey()] = $oProperty->getDefaultValue();
            }
            else if(!isset($aProperties[$oProperty->getKey()]))
            {
                $aProperties[$oProperty->getKey()] = null;
            }
        }

        return $aProperties;
    }
    /**
     * @param IComponent $oComponent
     * @param array|null $aProperties
     * @return array
     * @throws \Exception
     */
    function loadAndMerge(IAppComponent $oComponent, array $aProperties = null): array
    {
        try
        {
            $oComponentDefinition = $this->load($oComponent);

            return $this->merge($aProperties, $oComponentDefinition->getProperties());

        } catch (FatalErrorException $e)
        {
            echo $e->getMessage() . PHP_EOL;
            Except_log::register($e, true);
            exit();
        }
    }
}
