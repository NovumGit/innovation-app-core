<?php

namespace LowCode\System;

use Crud\FormManager;
use LowCode\Component\IComponent;

class CrudComponentManagerFactory
{
    /**
     * @param IComponent $oComponent
     * @return string
     */
    static function getFqn(IComponent $oComponent): string
    {
        $sId = $oComponent->getComponentXml()->getId();
        return '\\Crud\\Component_' . $sId . '\\CrudComponent_' . $sId . 'Manager';
    }

    /**
     * @param IComponent $oComponent
     * @return string
     */
    static function load(IComponent $oComponent): FormManager
    {

        $sFqn = self::getFqn($oComponent);

        $oManager = new $sFqn;


        return $oManager;
    }
}
