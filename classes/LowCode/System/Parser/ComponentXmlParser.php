<?php
namespace LowCode\System\Parser;

use Core\Reflector;
use LowCode\Component\IComponent;
use LowCode\System\Parser\ComponentXml\Component;

class ComponentXmlParser
{
    static function parse(IComponent $oComponent):Component
    {
        $oReflector = new Reflector($oComponent);
        $sXmlFile = $oReflector->getClassDir() . '/component.xml';
        return simplexml_load_file($sXmlFile, Component::class);
    }
}
