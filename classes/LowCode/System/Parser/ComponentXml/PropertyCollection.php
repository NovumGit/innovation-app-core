<?php
namespace LowCode\System\Parser\ComponentXml;

class PropertyCollection implements \Iterator
{

    private $aProperties = [];

    /**
     * @param Property[] $aProperties
     */
    public function __construct(array $aProperties = null)
    {
        $this->aProperties = $aProperties;
    }

    public function add(Property $oField)
    {
        $this->aProperties[] = $oField;
    }

    function next(): void
    {
        next($this->aProperties);
    }

    function valid(): bool
    {
        $key = key($this->aProperties);
        return ($key !== null && $key !== false);
    }

    function current(): Property
    {
        return current($this->aProperties);
    }

    function rewind()
    {
        reset($this->aProperties);
    }

    function key(): int
    {
        return key($this->aProperties);
    }
}
