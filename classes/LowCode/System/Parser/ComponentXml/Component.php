<?php
namespace LowCode\System\Parser\ComponentXml;

use Hurah\Types\Type\Base64;
use Core\Reflector;

/**
 * Represents a component xml based on component xsd which is a copy of Mendix widget xsd.
 *
 * Class Widget
 * @package LowCode\System
 */
class Component extends \SimpleXMLElement
{
    private $icon;
    private $id;
    private $description;
    private $name;
    private $mobile;
    private $offlineCapable;

    function getId():string
    {
        return $this->attributes()->id;
    }
    function getNeedsEntityContext():bool
    {
        return (bool) $this->attributes()->needsEntityContext;
    }
    function getName():string
    {
        return $this->name;
    }
    function getDescription():string
    {
        return (string) $this->description;
    }
    function getIsApp():bool
    {
        return (string) $this->mobile == 'true';
    }
    function getisWeb():bool
    {
        return (string) $this->mobile == 'false';
    }

    function isOfflineCapable():bool
    {
        return (bool) $this->offlineCapable;
    }

    function getProperties():PropertyCollection
    {
        $aProperties = [];
        if(is_iterable($this->properties->property))
        {
            foreach($this->properties->property as $x => $property)
            {
                $aProperties[] = new Property($property);
            }
        }

        return new PropertyCollection($aProperties);
    }
    function getComponentIcon():Base64
    {
        return new Base64($this->icon);
    }


}

