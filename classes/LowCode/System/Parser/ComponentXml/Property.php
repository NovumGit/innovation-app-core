<?php
namespace LowCode\System\Parser\ComponentXml;

class Property
{
    private $property;

    private $caption;
    private $category;
    private $description;
    private $attributeTypes;
    private $enumerationValues;
    private $properties;
    private $returnType;
    private $translations;

    function __construct(Component $property)
    {
        $this->property = $property;
    }

    /**
     * @return mixed
     */
    public function getCaption()
    {
        return $this->property->caption;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->property->category;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->property->description;
    }

    /**
     * AutoNumber
    Binary
    Boolean
    DateTime
    Decimal
    Enum
    HashString
    Integer
    Long
    String
     * @return array[]
     */
    public function getAttributeTypes():array
    {

        return $this->property->attributeTypes;
    }

    /**
     * @return string[]
     */
    public function getEnumerationValues():array
    {
        if($this->getType() != 'enumeration' || !$this->property->enumerationValues)
        {
            return [];
        }
        $aOut = [];

        foreach($this->property->enumerationValues->children() as $mValue)
        {
            $aOut[] = [
                'key' => (string)$mValue['key'],
                'value' => (string) $mValue
            ];
        }

        return $aOut;
    }

    /**
     * @return mixed
     */
    public function getProperties():PropertyCollection
    {
        return $this->property->properties;
    }

    /**
     * @return mixed
     */
    public function getReturnType():ReturnType
    {
        return $this->property->returnType;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->property->translations;
    }

    /**
     * @return mixed
     */
    public function getKey():string
    {
        $sKey = (string) $this->property->attributes()->key;
        return $sKey;
    }

    /**
     * @return null|string
     */
    public function getType():?string
    {
        return (string) $this->property->attributes()->type;
    }

    /**
     * @return mixed
     */
    public function getIsList():bool
    {
        return (bool) $this->property->attributes()->isList == 'true';
    }

    /**
     * @return mixed
     */
    public function getEntityProperty():string
    {
        return (string) $this->property->attributes()->entityProperty;
    }

    /**
     * @return bool
     */
    public function isAllowNonPersistableEntities(): bool
    {
        return (bool) $this->property->attributes()->allowNonPersistableEntities == 'true';
    }

    /**
     * @return bool
     */
    public function getisPath(): bool
    {
        return (bool) $this->property->attributes()->isPath == 'true';
    }

    /**
     * @return string
     */
    public function getPathType()
    {
        return (string) $this->property->attributes()->pathType;
    }

    /**
     * @return bool
     */
    public function isParameterIsList(): bool
    {
        return (bool) $this->property->attributes()->parameterIsList == 'true';
    }

    /**
     * @return bool
     */
    public function isMultiline(): bool
    {
        return (bool) $this->property->attributes()->multiline == 'true';
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string
    {
        return (string) $this->property->attributes()->defaultValue;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return ((string) $this->property->attributes()->required === 'true');
    }

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return (bool) $this->property->attributes()->isDefault == 'true';
    }

}
