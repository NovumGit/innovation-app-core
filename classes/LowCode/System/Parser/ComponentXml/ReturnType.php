<?php
namespace LowCode\System\Parser\ComponentXml;

class ReturnType extends \SimpleXMLElement
{
    private $type; //"
    private $isList; //
    private $entityProperty;//" type="xs:string"/>

    /**
     *
     * @return string, one of Void, Boolean, Integer, Decimal, DateTime, String, Object
     */
    public function getType():string
    {
        return (string) $this->type;
    }

    /**
     * type="xs:boolean" default="false"/>
     * @return mixed
     */
    public function getisList():bool
    {
        return (bool) $this->attributes()->isList;
    }

    /**
     * @return mixed
     */
    public function getEntityProperty():string
    {
        return (string) $this->attributes()->entityProperty;
    }
}
