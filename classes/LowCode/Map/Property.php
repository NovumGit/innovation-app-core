<?php

namespace LowCode\Map;

use Core\Utils;
use LowCode\System\Parser\ComponentXml\Property as XmlProperty;
use Model\Logging\Except_log;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Model\DataModel;
use Propel\Runtime\Exception\PropelException;

class Property
{
    private static function getTypeMap(): array
    {
        return [
            'attribute' => ['VARCHAR', 'String'], 'boolean' => ['BOOLEAN', 'Boolean'], 'entity' => ['VARCHAR', 'String'], 'entityConstraint' => ['VARCHAR', 'String'], 'enumeration' => ['INTEGER', 'Lookup'], 'icon' => ['VARCHAR', 'Icon'], 'form' => ['VARCHAR', 'File'], 'image' => ['VARCHAR', 'File'], 'integer' => ['INTEGER', 'Integer'], 'microflow' => ['VARCHAR', 'Lookup'], 'nanoflow' => ['VARCHAR', 'Lookup'], 'crudManager' => ['VARCHAR', 'Lookup'], 'object' => ['VARCHAR', 'String'], 'string' => ['VARCHAR', 'String'], 'translatableString' => ['VARCHAR', 'String']];
    }

    static function toDataField(DataModel $oDataModel, XmlProperty $oProperty, array $aArguments = null):DataField
    {
        try
        {

            $sFieldType  = $aArguments['field_type'] ?? $oProperty->getType();
            $sFieldName = $aArguments['field_name'] ?? $oProperty->getKey();

            $oDataField = DataFieldQuery::create();
            $oDataField->filterByDataModelId($oDataModel->getId());

            $oDataField->filterByName($sFieldName);
            $oDataField = $oDataField->findOneOrCreate();

            $sFormFieldType = self::getFormFieldType($sFieldType);
            $oDataField->setFormFieldTypeByName($sFormFieldType);

            $oDataField->setPhpName(Utils::camelCase($sFieldName));
            $oDataField->setLabel($oProperty->getCaption());

            $sDataType = self::getPropelDataType($sFieldType);
            $oDataField->setDataTypeByName($sDataType);

            $oDataField->setIconByName('tag');
            $oDataField->setRequired(false);
            return $oDataField;

        } catch (PropelException $e)
        {
            Except_log::register($e, true);
        }
    }

    static function getPropelDataType(string $sPropertyDataType): string
    {
        return self::getTypeMap()[$sPropertyDataType][0];
    }

    static function getFormFieldType(string $sPropertyDataType): string
    {
        return self::getTypeMap()[$sPropertyDataType][1];
    }
}
