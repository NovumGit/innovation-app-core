<?php
namespace LowCode\Behavior;

use Exception\LogicException;

/**
 * A component can allow sub components to be dropped on it.
 * This class helps the system figure out what can be dropped on what.
 *
 * Class ComponentAccepts
 * @package LowCode\Component
 */
class ComponentAcceptsManager implements IComponentAcceptManager
{

    private $aComponentAcceptedChildren = [];
    /**
     * Returns an array of tags / fields that accept other components
     * @return array|null
     */
    function getDroppableSpots():?array
    {
        return array_keys($this->aComponentAcceptedChildren);
    }

    /**
     * Returns the types of components that a field can accept
     * @param string $sKey refers one of the keys that came from getDroppableFields()
     * @return IFieldAcceptedChildren
     */
    function getDroppableSpotAcceptedTypes(string $sKey):array
    {
        $oFieldAcceptedChildren = $this->aComponentAcceptedChildren[$sKey];
        if($oFieldAcceptedChildren instanceof IFieldAcceptedChildren)
        {
            return $oFieldAcceptedChildren->getCollection()->toArray();
        }
        throw new LogicException("Expected an instance of IFieldAcceptedChildren");
    }

    function addDroppableField(string $sKey, IFieldAcceptedChildren $oComponentAcceptedChild)
    {
        $this->aComponentAcceptedChildren[$sKey] = $oComponentAcceptedChild;
    }
}

