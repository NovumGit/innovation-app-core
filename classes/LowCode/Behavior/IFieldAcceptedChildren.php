<?php
namespace LowCode\Behavior;


interface IFieldAcceptedChildren
{
    function getCollection():FieldAcceptsCollection;
}
