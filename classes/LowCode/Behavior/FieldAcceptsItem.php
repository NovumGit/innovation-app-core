<?php
namespace LowCode\Behavior;

use LowCode\Behavior\IComponentAcceptedChildren;
use LowCode\Component\ComponentCollection;
use LowCode\Component\IComponent;

class FieldAcceptsItem implements IFieldAcceptedChildren
{

    private $oComponent;
    function __construct(IComponent $oComponent)
    {
        $this->oComponent = $oComponent;
    }
    function getComponent():IComponent
    {
        return $this->oComponent;
    }

    function getCollection():FieldAcceptsCollection
    {
        $oFieldAcceptsCollection = new FieldAcceptsCollection();
        $oFieldAcceptsCollection->addItem($this);
        return $oFieldAcceptsCollection;
    }
}
