<?php
namespace LowCode\Behavior;


class FieldAcceptsCollection implements IFieldAcceptedChildren
{

    private $aItems = [];


    function addItem(IFieldAcceptedChildren $oIFieldAcceptedChildren)
    {
        $this->aItems[] = $oIFieldAcceptedChildren;
    }
    function getCollection():FieldAcceptsCollection
    {
        return $this;
    }
    function toArray():array
    {
        return $this->aItems;
    }

}
