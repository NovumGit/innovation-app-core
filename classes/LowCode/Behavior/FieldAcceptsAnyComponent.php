<?php
namespace LowCode\Behavior;

use Exception\LogicException;
use LowCode\ComponentFactory;

/**
 * A component can allow sub components to be dropped on it.
 * This class helps the system figure out what can be dropped on what.
 *
 * Class ComponentAccepts
 * @package LowCode\Component
 */
class FieldAcceptsAnyComponent implements IFieldAcceptedChildren
{
    /**
     * Returns the types of components that a field can accept
     * @param string $sKey refers one of the keys that came from getDroppableFields()
     * @return array|null
     */
    function getCollection():FieldAcceptsCollection
    {
        $oFieldAcceptsCollection = new FieldAcceptsCollection();
        $aAllComponents = ComponentFactory::getAll();

        foreach($aAllComponents as $oComponent)
        {
            $oFieldAcceptsCollection->addItem(new FieldAcceptsItem($oComponent));
        }
        return $oFieldAcceptsCollection;
    }

}

