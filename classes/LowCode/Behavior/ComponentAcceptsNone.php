<?php
namespace LowCode\Behavior;

/**
 * A component can allow sub components to be dropped on it.
 * This class helps the system figure out what can be dropped on what.
 *
 * Class ComponentAccepts
 * @package LowCode\Component
 */
class ComponentAcceptsNone implements IComponentAcceptManager
{
    /**
     * Returns null
     * @return array|null
     */
    function getDroppableSpots():?array
    {
        return null;
    }

}

