<?php
namespace LowCode\Behavior;

/**
 * A component can allow sub components to be dropped on it.
 * This class helps the system figure out what can be dropped on what.
 *
 * Class ComponentAccepts
 * @package LowCode\Component
 */
interface IComponentAcceptManager
{
    function getDroppableSpots():?array;
}
