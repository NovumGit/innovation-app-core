<?php
namespace LowCode;

use LowCode\Contracts\State\ICoreState;

class CoreState implements ICoreState
{
    private $aData;
    function __construct(array $aData = null)
    {
        $this->aData = $aData;
    }
    function toArray():array
    {
        return $this->aData;
    }
    function toJson()
    {
        return json_encode($this->toArray());
    }


}
