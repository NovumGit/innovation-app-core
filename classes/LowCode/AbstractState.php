<?php
namespace LowCode;

use Core\Logger;
use Model\Logging\Except_log;

abstract class AbstractState implements ICoreState
{
    private $aData = [];
    function __construct(array $aData = null, ICoreState $oState = null)
    {
        $this->oState = $oState;
        $this->aData = $aData;
    }
    function toArray():array
    {
        return $this->aData;
    }


}


