<?php

namespace UserModel\ButtonGroup;

use UserModel\ButtonGroup\Base\Component_buttongroupQuery as BaseComponent_buttongroupQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_button_group' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_buttongroupQuery extends BaseComponent_buttongroupQuery
{

}
