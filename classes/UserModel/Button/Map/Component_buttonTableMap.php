<?php

namespace UserModel\Button\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use UserModel\Button\Component_button;
use UserModel\Button\Component_buttonQuery;


/**
 * This class defines the structure of the 'component_button' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class Component_buttonTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'UserModel.Button.Map.Component_buttonTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_button';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\UserModel\\Button\\Component_button';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'UserModel.Button.Component_button';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the info field
     */
    const COL_INFO = 'component_button.info';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'component_button.title';

    /**
     * the column name for the size field
     */
    const COL_SIZE = 'component_button.size';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'component_button.icon';

    /**
     * the column name for the label field
     */
    const COL_LABEL = 'component_button.label';

    /**
     * the column name for the event field
     */
    const COL_EVENT = 'component_button.event';

    /**
     * the column name for the page field
     */
    const COL_PAGE = 'component_button.page';

    /**
     * the column name for the flow field
     */
    const COL_FLOW = 'component_button.flow';

    /**
     * the column name for the color_type field
     */
    const COL_COLOR_TYPE = 'component_button.color_type';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_button.ui_component_id';

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_button.id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Info', 'Title', 'Size', 'Icon', 'Label', 'Event', 'Page', 'Flow', 'ColorType', 'UiComponentId', 'id', ),
        self::TYPE_CAMELNAME     => array('info', 'title', 'size', 'icon', 'label', 'event', 'page', 'flow', 'colorType', 'uiComponentId', 'id', ),
        self::TYPE_COLNAME       => array(Component_buttonTableMap::COL_INFO, Component_buttonTableMap::COL_TITLE, Component_buttonTableMap::COL_SIZE, Component_buttonTableMap::COL_ICON, Component_buttonTableMap::COL_LABEL, Component_buttonTableMap::COL_EVENT, Component_buttonTableMap::COL_PAGE, Component_buttonTableMap::COL_FLOW, Component_buttonTableMap::COL_COLOR_TYPE, Component_buttonTableMap::COL_UI_COMPONENT_ID, Component_buttonTableMap::COL_ID, ),
        self::TYPE_FIELDNAME     => array('info', 'title', 'size', 'icon', 'label', 'event', 'page', 'flow', 'color_type', 'ui_component_id', 'id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Info' => 0, 'Title' => 1, 'Size' => 2, 'Icon' => 3, 'Label' => 4, 'Event' => 5, 'Page' => 6, 'Flow' => 7, 'ColorType' => 8, 'UiComponentId' => 9, 'id' => 10, ),
        self::TYPE_CAMELNAME     => array('info' => 0, 'title' => 1, 'size' => 2, 'icon' => 3, 'label' => 4, 'event' => 5, 'page' => 6, 'flow' => 7, 'colorType' => 8, 'uiComponentId' => 9, 'id' => 10, ),
        self::TYPE_COLNAME       => array(Component_buttonTableMap::COL_INFO => 0, Component_buttonTableMap::COL_TITLE => 1, Component_buttonTableMap::COL_SIZE => 2, Component_buttonTableMap::COL_ICON => 3, Component_buttonTableMap::COL_LABEL => 4, Component_buttonTableMap::COL_EVENT => 5, Component_buttonTableMap::COL_PAGE => 6, Component_buttonTableMap::COL_FLOW => 7, Component_buttonTableMap::COL_COLOR_TYPE => 8, Component_buttonTableMap::COL_UI_COMPONENT_ID => 9, Component_buttonTableMap::COL_ID => 10, ),
        self::TYPE_FIELDNAME     => array('info' => 0, 'title' => 1, 'size' => 2, 'icon' => 3, 'label' => 4, 'event' => 5, 'page' => 6, 'flow' => 7, 'color_type' => 8, 'ui_component_id' => 9, 'id' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_button');
        $this->setPhpName('Component_button');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\UserModel\\Button\\Component_button');
        $this->setPackage('UserModel.Button');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('info', 'Info', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('size', 'Size', 'VARCHAR', false, 255, null);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('label', 'Label', 'VARCHAR', false, 255, null);
        $this->addColumn('event', 'Event', 'VARCHAR', false, 255, null);
        $this->addColumn('page', 'Page', 'VARCHAR', false, 255, null);
        $this->addColumn('flow', 'Flow', 'VARCHAR', false, 255, null);
        $this->addColumn('color_type', 'ColorType', 'VARCHAR', false, 255, null);
        $this->addColumn('ui_component_id', 'UiComponentId', 'INTEGER', true, null, null);
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 10 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_buttonTableMap::CLASS_DEFAULT : Component_buttonTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_button object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_buttonTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_buttonTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_buttonTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_buttonTableMap::OM_CLASS;
            /** @var Component_button $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_buttonTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_buttonTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_buttonTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_button $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_buttonTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_buttonTableMap::COL_INFO);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_TITLE);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_SIZE);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_ICON);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_LABEL);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_EVENT);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_PAGE);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_FLOW);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_COLOR_TYPE);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_buttonTableMap::COL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.info');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.size');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.label');
            $criteria->addSelectColumn($alias . '.event');
            $criteria->addSelectColumn($alias . '.page');
            $criteria->addSelectColumn($alias . '.flow');
            $criteria->addSelectColumn($alias . '.color_type');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_buttonTableMap::DATABASE_NAME)->getTable(Component_buttonTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_buttonTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_buttonTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_buttonTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_button or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_button object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \UserModel\Button\Component_button) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_buttonTableMap::DATABASE_NAME);
            $criteria->add(Component_buttonTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_buttonQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_buttonTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_buttonTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_buttonQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_button or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_button object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_button object
        }

        if ($criteria->containsKey(Component_buttonTableMap::COL_ID) && $criteria->keyContainsValue(Component_buttonTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_buttonTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_buttonQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_buttonTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_buttonTableMap::buildTableMap();
