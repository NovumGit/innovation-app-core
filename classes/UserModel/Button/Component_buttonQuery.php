<?php

namespace UserModel\Button;

use UserModel\Button\Base\Component_buttonQuery as BaseComponent_buttonQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_button' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_buttonQuery extends BaseComponent_buttonQuery
{

}
