<?php

namespace UserModel\Table\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use UserModel\Table\Component_table;
use UserModel\Table\Component_tableQuery;


/**
 * This class defines the structure of the 'component_table' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class Component_tableTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'UserModel.Table.Map.Component_tableTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_table';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\UserModel\\Table\\Component_table';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'UserModel.Table.Component_table';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the manager field
     */
    const COL_MANAGER = 'component_table.manager';

    /**
     * the column name for the layout_key field
     */
    const COL_LAYOUT_KEY = 'component_table.layout_key';

    /**
     * the column name for the render_style field
     */
    const COL_RENDER_STYLE = 'component_table.render_style';

    /**
     * the column name for the items_pp field
     */
    const COL_ITEMS_PP = 'component_table.items_pp';

    /**
     * the column name for the tabs_draggable field
     */
    const COL_TABS_DRAGGABLE = 'component_table.tabs_draggable';

    /**
     * the column name for the create_new_buttom field
     */
    const COL_CREATE_NEW_BUTTOM = 'component_table.create_new_buttom';

    /**
     * the column name for the default_sorting_column field
     */
    const COL_DEFAULT_SORTING_COLUMN = 'component_table.default_sorting_column';

    /**
     * the column name for the default_sorting_direction field
     */
    const COL_DEFAULT_SORTING_DIRECTION = 'component_table.default_sorting_direction';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_table.ui_component_id';

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_table.id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Manager', 'LayoutKey', 'RenderStyle', 'ItemsPp', 'TabsDraggable', 'CreateNewButtom', 'DefaultSortingColumn', 'DefaultSortingDirection', 'UiComponentId', 'id', ),
        self::TYPE_CAMELNAME     => array('manager', 'layoutKey', 'renderStyle', 'itemsPp', 'tabsDraggable', 'createNewButtom', 'defaultSortingColumn', 'defaultSortingDirection', 'uiComponentId', 'id', ),
        self::TYPE_COLNAME       => array(Component_tableTableMap::COL_MANAGER, Component_tableTableMap::COL_LAYOUT_KEY, Component_tableTableMap::COL_RENDER_STYLE, Component_tableTableMap::COL_ITEMS_PP, Component_tableTableMap::COL_TABS_DRAGGABLE, Component_tableTableMap::COL_CREATE_NEW_BUTTOM, Component_tableTableMap::COL_DEFAULT_SORTING_COLUMN, Component_tableTableMap::COL_DEFAULT_SORTING_DIRECTION, Component_tableTableMap::COL_UI_COMPONENT_ID, Component_tableTableMap::COL_ID, ),
        self::TYPE_FIELDNAME     => array('manager', 'layout_key', 'render_style', 'items_pp', 'tabs_draggable', 'create_new_buttom', 'default_sorting_column', 'default_sorting_direction', 'ui_component_id', 'id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Manager' => 0, 'LayoutKey' => 1, 'RenderStyle' => 2, 'ItemsPp' => 3, 'TabsDraggable' => 4, 'CreateNewButtom' => 5, 'DefaultSortingColumn' => 6, 'DefaultSortingDirection' => 7, 'UiComponentId' => 8, 'id' => 9, ),
        self::TYPE_CAMELNAME     => array('manager' => 0, 'layoutKey' => 1, 'renderStyle' => 2, 'itemsPp' => 3, 'tabsDraggable' => 4, 'createNewButtom' => 5, 'defaultSortingColumn' => 6, 'defaultSortingDirection' => 7, 'uiComponentId' => 8, 'id' => 9, ),
        self::TYPE_COLNAME       => array(Component_tableTableMap::COL_MANAGER => 0, Component_tableTableMap::COL_LAYOUT_KEY => 1, Component_tableTableMap::COL_RENDER_STYLE => 2, Component_tableTableMap::COL_ITEMS_PP => 3, Component_tableTableMap::COL_TABS_DRAGGABLE => 4, Component_tableTableMap::COL_CREATE_NEW_BUTTOM => 5, Component_tableTableMap::COL_DEFAULT_SORTING_COLUMN => 6, Component_tableTableMap::COL_DEFAULT_SORTING_DIRECTION => 7, Component_tableTableMap::COL_UI_COMPONENT_ID => 8, Component_tableTableMap::COL_ID => 9, ),
        self::TYPE_FIELDNAME     => array('manager' => 0, 'layout_key' => 1, 'render_style' => 2, 'items_pp' => 3, 'tabs_draggable' => 4, 'create_new_buttom' => 5, 'default_sorting_column' => 6, 'default_sorting_direction' => 7, 'ui_component_id' => 8, 'id' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_table');
        $this->setPhpName('Component_table');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\UserModel\\Table\\Component_table');
        $this->setPackage('UserModel.Table');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('manager', 'Manager', 'VARCHAR', false, 255, null);
        $this->addColumn('layout_key', 'LayoutKey', 'VARCHAR', false, 255, null);
        $this->addColumn('render_style', 'RenderStyle', 'BOOLEAN', false, 1, null);
        $this->addColumn('items_pp', 'ItemsPp', 'VARCHAR', false, 255, null);
        $this->addColumn('tabs_draggable', 'TabsDraggable', 'BOOLEAN', false, 1, null);
        $this->addColumn('create_new_buttom', 'CreateNewButtom', 'BOOLEAN', false, 1, null);
        $this->addColumn('default_sorting_column', 'DefaultSortingColumn', 'VARCHAR', false, 255, null);
        $this->addColumn('default_sorting_direction', 'DefaultSortingDirection', 'VARCHAR', false, 255, null);
        $this->addColumn('ui_component_id', 'UiComponentId', 'INTEGER', true, null, null);
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 9 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_tableTableMap::CLASS_DEFAULT : Component_tableTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_table object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_tableTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_tableTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_tableTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_tableTableMap::OM_CLASS;
            /** @var Component_table $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_tableTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_tableTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_tableTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_table $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_tableTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_tableTableMap::COL_MANAGER);
            $criteria->addSelectColumn(Component_tableTableMap::COL_LAYOUT_KEY);
            $criteria->addSelectColumn(Component_tableTableMap::COL_RENDER_STYLE);
            $criteria->addSelectColumn(Component_tableTableMap::COL_ITEMS_PP);
            $criteria->addSelectColumn(Component_tableTableMap::COL_TABS_DRAGGABLE);
            $criteria->addSelectColumn(Component_tableTableMap::COL_CREATE_NEW_BUTTOM);
            $criteria->addSelectColumn(Component_tableTableMap::COL_DEFAULT_SORTING_COLUMN);
            $criteria->addSelectColumn(Component_tableTableMap::COL_DEFAULT_SORTING_DIRECTION);
            $criteria->addSelectColumn(Component_tableTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_tableTableMap::COL_ID);
        } else {
            $criteria->addSelectColumn($alias . '.manager');
            $criteria->addSelectColumn($alias . '.layout_key');
            $criteria->addSelectColumn($alias . '.render_style');
            $criteria->addSelectColumn($alias . '.items_pp');
            $criteria->addSelectColumn($alias . '.tabs_draggable');
            $criteria->addSelectColumn($alias . '.create_new_buttom');
            $criteria->addSelectColumn($alias . '.default_sorting_column');
            $criteria->addSelectColumn($alias . '.default_sorting_direction');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_tableTableMap::DATABASE_NAME)->getTable(Component_tableTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_tableTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_tableTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_tableTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_table or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_table object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_tableTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \UserModel\Table\Component_table) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_tableTableMap::DATABASE_NAME);
            $criteria->add(Component_tableTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_tableQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_tableTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_tableTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_table table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_tableQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_table or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_table object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_tableTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_table object
        }

        if ($criteria->containsKey(Component_tableTableMap::COL_ID) && $criteria->keyContainsValue(Component_tableTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_tableTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_tableQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_tableTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_tableTableMap::buildTableMap();
