<?php

namespace UserModel\Modal;

use UserModel\Modal\Base\Component_modal as BaseComponent_modal;

/**
 * Skeleton subclass for representing a row from the 'component_modal' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_modal extends BaseComponent_modal
{

}
