<?php

namespace Map;

use \ServiceDirectory;
use \ServiceDirectoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'service_directory' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ServiceDirectoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ServiceDirectoryTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'service_directory';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ServiceDirectory';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ServiceDirectory';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id field
     */
    const COL_ID = 'service_directory.id';

    /**
     * the column name for the vendor field
     */
    const COL_VENDOR = 'service_directory.vendor';

    /**
     * the column name for the service_name field
     */
    const COL_SERVICE_NAME = 'service_directory.service_name';

    /**
     * the column name for the homepage field
     */
    const COL_HOMEPAGE = 'service_directory.homepage';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'service_directory.description';

    /**
     * the column name for the keywords field
     */
    const COL_KEYWORDS = 'service_directory.keywords';

    /**
     * the column name for the license field
     */
    const COL_LICENSE = 'service_directory.license';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'service_directory.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'service_directory.last_name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'service_directory.email';

    /**
     * the column name for the domain field
     */
    const COL_DOMAIN = 'service_directory.domain';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Vendor', 'ServiceName', 'Homepage', 'Description', 'Keywords', 'License', 'FirstName', 'LastName', 'Email', 'Domain', ),
        self::TYPE_CAMELNAME     => array('id', 'vendor', 'serviceName', 'homepage', 'description', 'keywords', 'license', 'firstName', 'lastName', 'email', 'domain', ),
        self::TYPE_COLNAME       => array(ServiceDirectoryTableMap::COL_ID, ServiceDirectoryTableMap::COL_VENDOR, ServiceDirectoryTableMap::COL_SERVICE_NAME, ServiceDirectoryTableMap::COL_HOMEPAGE, ServiceDirectoryTableMap::COL_DESCRIPTION, ServiceDirectoryTableMap::COL_KEYWORDS, ServiceDirectoryTableMap::COL_LICENSE, ServiceDirectoryTableMap::COL_FIRST_NAME, ServiceDirectoryTableMap::COL_LAST_NAME, ServiceDirectoryTableMap::COL_EMAIL, ServiceDirectoryTableMap::COL_DOMAIN, ),
        self::TYPE_FIELDNAME     => array('id', 'vendor', 'service_name', 'homepage', 'description', 'keywords', 'license', 'first_name', 'last_name', 'email', 'domain', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Vendor' => 1, 'ServiceName' => 2, 'Homepage' => 3, 'Description' => 4, 'Keywords' => 5, 'License' => 6, 'FirstName' => 7, 'LastName' => 8, 'Email' => 9, 'Domain' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'vendor' => 1, 'serviceName' => 2, 'homepage' => 3, 'description' => 4, 'keywords' => 5, 'license' => 6, 'firstName' => 7, 'lastName' => 8, 'email' => 9, 'domain' => 10, ),
        self::TYPE_COLNAME       => array(ServiceDirectoryTableMap::COL_ID => 0, ServiceDirectoryTableMap::COL_VENDOR => 1, ServiceDirectoryTableMap::COL_SERVICE_NAME => 2, ServiceDirectoryTableMap::COL_HOMEPAGE => 3, ServiceDirectoryTableMap::COL_DESCRIPTION => 4, ServiceDirectoryTableMap::COL_KEYWORDS => 5, ServiceDirectoryTableMap::COL_LICENSE => 6, ServiceDirectoryTableMap::COL_FIRST_NAME => 7, ServiceDirectoryTableMap::COL_LAST_NAME => 8, ServiceDirectoryTableMap::COL_EMAIL => 9, ServiceDirectoryTableMap::COL_DOMAIN => 10, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'vendor' => 1, 'service_name' => 2, 'homepage' => 3, 'description' => 4, 'keywords' => 5, 'license' => 6, 'first_name' => 7, 'last_name' => 8, 'email' => 9, 'domain' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('service_directory');
        $this->setPhpName('ServiceDirectory');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ServiceDirectory');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('vendor', 'Vendor', 'VARCHAR', true, 255, null);
        $this->addColumn('service_name', 'ServiceName', 'VARCHAR', true, 255, null);
        $this->addColumn('homepage', 'Homepage', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 255, null);
        $this->addColumn('keywords', 'Keywords', 'VARCHAR', false, 255, null);
        $this->addColumn('license', 'License', 'VARCHAR', false, 255, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', false, 255, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('domain', 'Domain', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ServiceDirectoryTableMap::CLASS_DEFAULT : ServiceDirectoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ServiceDirectory object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ServiceDirectoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ServiceDirectoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ServiceDirectoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ServiceDirectoryTableMap::OM_CLASS;
            /** @var ServiceDirectory $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ServiceDirectoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ServiceDirectoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ServiceDirectoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ServiceDirectory $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ServiceDirectoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_ID);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_VENDOR);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_SERVICE_NAME);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_HOMEPAGE);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_KEYWORDS);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_LICENSE);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_EMAIL);
            $criteria->addSelectColumn(ServiceDirectoryTableMap::COL_DOMAIN);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.vendor');
            $criteria->addSelectColumn($alias . '.service_name');
            $criteria->addSelectColumn($alias . '.homepage');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.keywords');
            $criteria->addSelectColumn($alias . '.license');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.domain');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ServiceDirectoryTableMap::DATABASE_NAME)->getTable(ServiceDirectoryTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ServiceDirectoryTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ServiceDirectoryTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ServiceDirectoryTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ServiceDirectory or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ServiceDirectory object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServiceDirectoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ServiceDirectory) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ServiceDirectoryTableMap::DATABASE_NAME);
            $criteria->add(ServiceDirectoryTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ServiceDirectoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ServiceDirectoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ServiceDirectoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the service_directory table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ServiceDirectoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ServiceDirectory or Criteria object.
     *
     * @param mixed               $criteria Criteria or ServiceDirectory object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServiceDirectoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ServiceDirectory object
        }

        if ($criteria->containsKey(ServiceDirectoryTableMap::COL_ID) && $criteria->keyContainsValue(ServiceDirectoryTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ServiceDirectoryTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ServiceDirectoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ServiceDirectoryTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ServiceDirectoryTableMap::buildTableMap();
