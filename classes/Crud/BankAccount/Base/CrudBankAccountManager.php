<?php
namespace Crud\BankAccount\Base;

use Core\Utils;
use Crud;
use Crud\BankAccount\FieldIterator;
use Crud\BankAccount\Field\AvatarFileExt;
use Crud\BankAccount\Field\Balance;
use Crud\BankAccount\Field\BankLedgerId;
use Crud\BankAccount\Field\Currency;
use Crud\BankAccount\Field\HasAvatar;
use Crud\BankAccount\Field\Iban;
use Crud\BankAccount\Field\IsActive;
use Crud\BankAccount\Field\IsSavings;
use Crud\BankAccount\Field\Name;
use Crud\BankAccount\Field\OriginalId;
use Crud\BankAccount\Field\SavingsGoal;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankAccount;
use Model\Finance\BankAccountQuery;
use Model\Finance\Map\BankAccountTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankAccount instead if you need to override or add functionality.
 */
abstract class CrudBankAccountManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankAccountQuery::create();
	}


	public function getTableMap(): BankAccountTableMap
	{
		return new BankAccountTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankAccount";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_account toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_account aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OriginalId', 'BankLedgerId', 'Name', 'HasAvatar', 'AvatarFileExt', 'Iban', 'Currency', 'Balance', 'IsSavings', 'SavingsGoal', 'IsActive'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OriginalId', 'BankLedgerId', 'Name', 'HasAvatar', 'AvatarFileExt', 'Iban', 'Currency', 'Balance', 'IsSavings', 'SavingsGoal', 'IsActive'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankAccount
	 */
	public function getModel(array $aData = null): BankAccount
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankAccountQuery = BankAccountQuery::create();
		     $oBankAccount = $oBankAccountQuery->findOneById($aData['id']);
		     if (!$oBankAccount instanceof BankAccount) {
		         throw new LogicException("BankAccount should be an instance of BankAccount but got something else." . __METHOD__);
		     }
		     $oBankAccount = $this->fillVo($aData, $oBankAccount);
		}
		else {
		     $oBankAccount = new BankAccount();
		     if (!empty($aData)) {
		         $oBankAccount = $this->fillVo($aData, $oBankAccount);
		     }
		}
		return $oBankAccount;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankAccount
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankAccount
	{
		$oBankAccount = $this->getModel($aData);


		 if(!empty($oBankAccount))
		 {
		     $oBankAccount = $this->fillVo($aData, $oBankAccount);
		     $oBankAccount->save();
		 }
		return $oBankAccount;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankAccount $oModel
	 * @return BankAccount
	 */
	protected function fillVo(array $aData, BankAccount $oModel): BankAccount
	{
		if(isset($aData['original_id'])) {
		     $oField = new OriginalId();
		     $mValue = $oField->sanitize($aData['original_id']);
		     $oModel->setOriginalId($mValue);
		}
		if(isset($aData['bank_ledger_id'])) {
		     $oField = new BankLedgerId();
		     $mValue = $oField->sanitize($aData['bank_ledger_id']);
		     $oModel->setBankLedgerId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['has_avatar'])) {
		     $oField = new HasAvatar();
		     $mValue = $oField->sanitize($aData['has_avatar']);
		     $oModel->setHasAvatar($mValue);
		}
		if(isset($aData['avatar_file_ext'])) {
		     $oField = new AvatarFileExt();
		     $mValue = $oField->sanitize($aData['avatar_file_ext']);
		     $oModel->setAvatarFileExt($mValue);
		}
		if(isset($aData['iban'])) {
		     $oField = new Iban();
		     $mValue = $oField->sanitize($aData['iban']);
		     $oModel->setIban($mValue);
		}
		if(isset($aData['currency'])) {
		     $oField = new Currency();
		     $mValue = $oField->sanitize($aData['currency']);
		     $oModel->setCurrency($mValue);
		}
		if(isset($aData['balance'])) {
		     $oField = new Balance();
		     $mValue = $oField->sanitize($aData['balance']);
		     $oModel->setBalance($mValue);
		}
		if(isset($aData['is_savings'])) {
		     $oField = new IsSavings();
		     $mValue = $oField->sanitize($aData['is_savings']);
		     $oModel->setIsSavings($mValue);
		}
		if(isset($aData['savings_goal'])) {
		     $oField = new SavingsGoal();
		     $mValue = $oField->sanitize($aData['savings_goal']);
		     $oModel->setSavingsGoal($mValue);
		}
		if(isset($aData['is_active'])) {
		     $oField = new IsActive();
		     $mValue = $oField->sanitize($aData['is_active']);
		     $oModel->setIsActive($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
