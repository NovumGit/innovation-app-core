<?php
namespace Crud\BankAccount\Base;

use Crud\BankAccount\ICollectionField as BankAccountField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankAccount\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankAccountField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankAccountField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankAccountField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankAccountField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankAccountField
	{
		return current($this->aFields);
	}
}
