<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankAccount\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\BankAccount as ModelObject;


class HasAvatar extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'has_avatar';
    protected $sFieldLabel = 'Heeft avatar';
    private $sGetter = 'getHasAvatar';

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'boolean';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
