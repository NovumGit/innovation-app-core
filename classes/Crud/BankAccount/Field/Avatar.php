<?php
namespace Crud\BankAccount\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\BankAccount as ModelObject;


class Avatar extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'balance';
    protected $sFieldLabel = 'Avatar';
    private $sIcon = 'photo';
    private $sPlaceHolder = '';




    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sYes = Translate::fromCode('Ja');
        $sNo = Translate::fromCode('Nee');
        return '<td class="">'.$oModelObject->getHasAvatar() ? $sYes : $sNo .'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editFileField($this->getTranslatedTitle(), $this->sFieldName, null, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
