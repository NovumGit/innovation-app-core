<?php
namespace Crud\BankAccount\Field;

use Crud\BankAccount\Field\Base\IsActive as BaseIsActive;

/**
 * Skeleton subclass for representing is_active field from the bank_account table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class IsActive extends BaseIsActive
{
}
