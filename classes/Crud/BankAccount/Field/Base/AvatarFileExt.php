<?php
namespace Crud\BankAccount\Field\Base;

use Crud\BankAccount\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'avatar_file_ext' crud field from the 'bank_account' table.
 * This class is auto generated and should not be modified.
 */
abstract class AvatarFileExt extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'avatar_file_ext';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAvatarFileExt';
	protected $sFqModelClassname = '\\\Model\Finance\BankAccount';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
