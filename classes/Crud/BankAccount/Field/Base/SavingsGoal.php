<?php
namespace Crud\BankAccount\Field\Base;

use Crud\BankAccount\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'savings_goal' crud field from the 'bank_account' table.
 * This class is auto generated and should not be modified.
 */
abstract class SavingsGoal extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'savings_goal';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSavingsGoal';
	protected $sFqModelClassname = '\\\Model\Finance\BankAccount';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
