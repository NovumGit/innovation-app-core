<?php
namespace Crud\BankAccount\Field\Base;

use Crud\BankAccount\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'has_avatar' crud field from the 'bank_account' table.
 * This class is auto generated and should not be modified.
 */
abstract class HasAvatar extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'has_avatar';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHasAvatar';
	protected $sFqModelClassname = '\\\Model\Finance\BankAccount';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['has_avatar']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
