<?php
namespace Crud\BankAccount;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankAccount;
use Model\Finance\BankAccountQuery;

class CrudBankAccountManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Mijn bankrekeningen';
    }
    function getOverviewUrl():string
    {
        return '/finance/bank/account/overview';
    }
    function getCreateNewUrl():string
    {
        return '/finance/bank/account/edit';
    }
    function getNewFormTitle():string{
        return 'Rekening toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Rekening bewerken';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
        'Balance',
        'BankLedgerId',
        'Currency',
        'HasAvatar',
        'Iban',
        'Name',
        'OriginalId',
        'SavingsGoal'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
        'Balance',
        'BankLedgerId',
        'Currency',
        'HasAvatar',
        'Iban',
        'Name',
        'OriginalId',
        'SavingsGoal'
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oBankAccountQuery = BankAccountQuery::create();
            $oBankAccount = $oBankAccountQuery->findOneById($aData['id']);

            if(!$oBankAccount instanceof BankAccount)
            {
                throw new LogicException("Project should be an instance of BankAccount but got something else." . __METHOD__);
            }
            $oBankAccount = $this->fillVo($aData, $oBankAccount);
        }
        else
        {
            $oBankAccount = new BankAccount();
            if(!empty($aData))
            {
                $oBankAccount = $this->fillVo($aData, $oBankAccount);
            }
        }
        return $oBankAccount;
    }

    /**
     * @param $aData
     * @return BankAccount
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null): BankAccount
    {
        $oBankAccount = $this->getModel($aData);
        if($oBankAccount instanceof BankAccount)
        {
            $oBankAccount = $this->fillVo($aData, $oBankAccount);
            $oBankAccount->save();
        }
        return $oBankAccount;
    }
    private function fillVo($aData, BankAccount $oModel)
    {
        if(isset($aData['balance'])){ $oModel->setBalance( $aData['balance'] ); }
        if(isset($aData['bank_ledger_id'])){ $oModel->setBankLedgerId( $aData['bank_ledger_id'] ); }
        if(isset($aData['currency'])){ $oModel->setCurrency( $aData['currency'] ); }
        if(isset($aData['has_avatar'])){ $oModel->setHasAvatar( $aData['has_avatar'] ); }
        if(isset($aData['iban'])){ $oModel->setIban( $aData['iban'] ); }
        if(isset($aData['name'])){ $oModel->setName( $aData['name'] ); }
        if(isset($aData['original_id'])){ $oModel->setOriginalId( $aData['original_id'] ); }
        if(isset($aData['savings_goal'])){ $oModel->setSavingsGoal( $aData['savings_goal'] ); }
        if(isset($aData['is_savings'])){ $oModel->setIsSavings( $aData['is_savings'] ); }
        return $oModel;

    }
}
