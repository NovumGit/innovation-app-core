<?php
namespace Crud\Country\Base;

use Core\Utils;
use Crud;
use Crud\Country\FieldIterator;
use Crud\Country\Field\IsEu;
use Crud\Country\Field\Iso2;
use Crud\Country\Field\Iso3;
use Crud\Country\Field\Latitude;
use Crud\Country\Field\Longitude;
use Crud\Country\Field\Name;
use Crud\Country\Field\ShippingZoneId;
use Crud\Country\Field\StealthAdvised;
use Crud\Country\Field\StealthMandatory;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\Map\CountryTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Country instead if you need to override or add functionality.
 */
abstract class CrudCountryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CountryQuery::create();
	}


	public function getTableMap(): CountryTableMap
	{
		return new CountryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Country";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_country toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_country aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'ShippingZoneId', 'Iso2', 'Iso3', 'IsEu', 'StealthMandatory', 'StealthAdvised', 'Latitude', 'Longitude'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'ShippingZoneId', 'Iso2', 'Iso3', 'IsEu', 'StealthMandatory', 'StealthAdvised', 'Latitude', 'Longitude'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Country
	 */
	public function getModel(array $aData = null): Country
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCountryQuery = CountryQuery::create();
		     $oCountry = $oCountryQuery->findOneById($aData['id']);
		     if (!$oCountry instanceof Country) {
		         throw new LogicException("Country should be an instance of Country but got something else." . __METHOD__);
		     }
		     $oCountry = $this->fillVo($aData, $oCountry);
		}
		else {
		     $oCountry = new Country();
		     if (!empty($aData)) {
		         $oCountry = $this->fillVo($aData, $oCountry);
		     }
		}
		return $oCountry;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Country
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Country
	{
		$oCountry = $this->getModel($aData);


		 if(!empty($oCountry))
		 {
		     $oCountry = $this->fillVo($aData, $oCountry);
		     $oCountry->save();
		 }
		return $oCountry;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Country $oModel
	 * @return Country
	 */
	protected function fillVo(array $aData, Country $oModel): Country
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['shipping_zone_id'])) {
		     $oField = new ShippingZoneId();
		     $mValue = $oField->sanitize($aData['shipping_zone_id']);
		     $oModel->setShippingZoneId($mValue);
		}
		if(isset($aData['iso2'])) {
		     $oField = new Iso2();
		     $mValue = $oField->sanitize($aData['iso2']);
		     $oModel->setIso2($mValue);
		}
		if(isset($aData['iso3'])) {
		     $oField = new Iso3();
		     $mValue = $oField->sanitize($aData['iso3']);
		     $oModel->setIso3($mValue);
		}
		if(isset($aData['is_eu'])) {
		     $oField = new IsEu();
		     $mValue = $oField->sanitize($aData['is_eu']);
		     $oModel->setIsEu($mValue);
		}
		if(isset($aData['stealth_mandatory'])) {
		     $oField = new StealthMandatory();
		     $mValue = $oField->sanitize($aData['stealth_mandatory']);
		     $oModel->setStealthMandatory($mValue);
		}
		if(isset($aData['stealth_advised'])) {
		     $oField = new StealthAdvised();
		     $mValue = $oField->sanitize($aData['stealth_advised']);
		     $oModel->setStealthAdvised($mValue);
		}
		if(isset($aData['latitude'])) {
		     $oField = new Latitude();
		     $mValue = $oField->sanitize($aData['latitude']);
		     $oModel->setLatitude($mValue);
		}
		if(isset($aData['longitude'])) {
		     $oField = new Longitude();
		     $mValue = $oField->sanitize($aData['longitude']);
		     $oModel->setLongitude($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
