<?php
namespace Crud\Country\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Country\ICollectionField as CountryField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Country\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CountryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CountryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CountryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CountryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CountryField
	{
		return current($this->aFields);
	}
}
