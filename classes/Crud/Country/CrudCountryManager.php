<?php

namespace Crud\Country;

use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\Map\CountryTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Map\TableMap;

class CrudCountryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
    function getShortDescription(): string
    {
        return 'Manages the country list';
    }

    function getTableMap(): TableMap
    {
        return new CountryTableMap();
    }

    function getQueryObject(): ModelCriteria
    {
        return CountryQuery::create();
    }

    function getEntityTitle(): string
    {
        return 'land';
    }

    function getOverviewUrl(): string
    {
        return '/setting/mastertable/country/overview';
    }

    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/country/edit';
    }

    function getNewFormTitle(): string
    {
        return 'Land toevoegen';
    }

    function getEditFormTitle(): string
    {
        return 'Land wijzigen';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Iso2',
            'Iso3',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {

        return [
            'Name',
            'Iso2',
            'Iso3',
        ];
    }

    public function getModel(array $aData = null)
    {
        if ($aData['id']) {
            $oCountryQuery = new CountryQuery();

            $oCountry = $oCountryQuery->findOneById($aData['id']);

            if (!$oCountry instanceof Country) {
                throw new LogicException("Country should be an instance of Country but got " . get_class($oCountry) . " in " . __METHOD__);
            }
        } else {
            $oCountry = new Country();

            if (!empty($aData)) {
                $oCountry = $this->fillVo($aData, $oCountry);
            }
        }

        return $oCountry;
    }

    public function store(array $aData = null)
    {
        $oCountry = $this->getModel($aData);

        if (!empty($oCountry)) {
            $oCountry = $this->fillVo($aData, $oCountry);

            $oCountry->save();
        }
        return $oCountry;
    }

    private function fillVo($aData, Country $oCountry)
    {

        if (isset($aData['name'])) {
            $oCountry->setName($aData['name']);
        }
        if (isset($aData['iso2'])) {
            $oCountry->setIso2($aData['iso2']);
        }
        if (isset($aData['iso3'])) {
            $oCountry->setIso3($aData['iso3']);
        }
        if (isset($aData['is_eu'])) {
            $oCountry->setIsEu($aData['is_eu']);
        }
        if (isset($aData['stealth_mandatory'])) {
            $oCountry->setStealthMandatory($aData['stealth_mandatory']);
        }
        if (isset($aData['stealth_advised'])) {
            $oCountry->setStealthAdvised($aData['stealth_advised']);
        }
        if (isset($aData['shipping_zone_id'])) {
            $oCountry->setShippingZoneId($aData['shipping_zone_id']);
        }

        return $oCountry;
    }
}
