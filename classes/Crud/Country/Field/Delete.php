<?php

namespace Crud\Country\Field;

use Crud\Generic\Field\GenericDelete;
use InvalidArgumentException;
use Model\Setting\MasterTable\Country;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oObject)
    {

        if (!$oObject instanceof Country) {
            throw new InvalidArgumentException('Expected an instance of Country but got ' . get_class($oObject));
        }
        $sTab = '';
        if (isset($_GET['tab'])) {
            $sTab = '&tab=' . $_GET['tab'];
        }
        return '/setting/mastertable/country/edit?_do=Delete&id=' . $oObject->getId() . $sTab;
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
