<?php
namespace Crud\Country\Field\Base;

use Crud\Country\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'latitude' crud field from the 'mt_country' table.
 * This class is auto generated and should not be modified.
 */
abstract class Latitude extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'latitude';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLatitude';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\Country';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
