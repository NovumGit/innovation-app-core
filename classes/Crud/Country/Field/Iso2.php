<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Country\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Country as ModelObject;

class Iso2 extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'iso2';
    protected $sFieldLabel = 'Iso2 landcode';
    private $sIcon = 'globe';
    private $sPlaceHolder = '';
    private $sGetter = 'getIso2';

    public function getDataType(): string
    {
        return 'string';
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($oModelObject, $bReadonly)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
