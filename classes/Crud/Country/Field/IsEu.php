<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Country\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Country as ModelObject;

class IsEu extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'is_eu';
    protected $sFieldLabel = 'Europese Unie';
    private $sGetter = 'getIsEu';

    function getDataType(): string
    {
        return 'boolean';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $sIsEu = $oModelObject->{$this->sGetter}() == '1' ? 'Ja' : 'Nee';
        return '<td class="">' . $sIsEu . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($oModelObject, $bReadOnly = false)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadOnly);
    }
}
