<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Country\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Country as ModelObject;

class StealthMandatory extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'stealth_mandatory';
    protected $sFieldLabel = 'Stealth modus verplicht';
    private $sGetter = 'getStealthMandatory';

    function getDataType(): string
    {
        return 'boolean';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $sStealthModeMandatory = $oModelObject->{$this->sGetter}() == '1' ? 'Ja' : 'Nee';
        return '<td class="">' . $sStealthModeMandatory . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($oModelObject, $bReadOnly)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadOnly);
    }
}
