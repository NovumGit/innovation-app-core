<?php

namespace Crud\Country\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\Country;
use Exception\InvalidArgumentException;
use Crud\Language\CrudLanguageManager as ManagerObject;

class Edit extends GenericEdit
{

    public function getEditUrl($oCountry)
    {

        if (!$oCountry instanceof Country) {
            throw new InvalidArgumentException('Expected an instance of Country but got ' . get_class($oCountry));
        }
        $oManager = new ManagerObject();
        return $oManager->getCreateNewUrl() . '?id=' . $oCountry->getId();
    }
}
