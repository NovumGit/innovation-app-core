<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Country\Field;

use Core\Utils;
use Crud\Field;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\ShippingZone;
use Model\Setting\MasterTable\ShippingZoneQuery;

class ShippingZoneId extends Field implements IFilterableLookupField
{

    protected $sFieldName = 'shipping_zone_id';
    protected $sFieldLabel = 'Verzend zone';
    private $sIcon = 'info-circle';
    private $sPlaceHolder = 'Kies een verzendzone';

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    function getLookups($mSelectedItem = null)
    {
        $ShippingZoneQuery = ShippingZoneQuery::create();
        $aAllZones = $ShippingZoneQuery->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllZones, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }

    public function getDataType(): string
    {
        return 'lookup';
    }

    public function getVisibleValue($iItemId)
    {
        if ($iItemId) {
            return ShippingZoneQuery::create()->findOneById($iItemId)->getName();
        }
        return null;
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oCountry)
    {
        if (!$oCountry instanceof Country) {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in " . __METHOD__);
        }
        $sShippingZoneName = '';
        $oShippingZone = $oCountry->getShippingZone();

        if ($oShippingZone instanceof ShippingZone) {
            $sShippingZoneName = $oShippingZone->getName();
        }

        return '<td class="">' . $sShippingZoneName . '</td>';
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Country) {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getShippingZoneId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getShippingZoneId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
