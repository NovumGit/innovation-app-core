<?php

namespace Crud\Country;

use Core\Reflector;

class CrudCountryShippingManager extends CrudCountryManager
{
    public function getShortDescription(): string
    {
        return 'For the countries that we ship to.';
    }

    public function getModuleName(): string
    {
        $oReflector = new Reflector($this);

        $aParts = array_reverse(explode(DIRECTORY_SEPARATOR, dirname($oReflector->getFileName())));
        return $aParts[0];
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Iso2',
            'Iso3',
            'EditShippingCost',
        ];
    }
}
