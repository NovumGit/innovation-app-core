<?php

namespace Crud\Vat;

use Core\Utils;
use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\VatQuery;
use Model\Setting\MasterTable\Vat;
use Propel\Runtime\Exception\LogicException;

class CrudVatManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/vat/edit';
    }

    function getOverviewUrl(): string
    {
        return '/setting/mastertable/vat/overview';
    }

    function getEntityTitle(): string
    {
        return 'btw';
    }

    public function getNewFormTitle(): string
    {
        return 'Btw tarief toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'BTW tarief wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNs = false): ?array
    {
        $aColumns = [
            'Name',
            'Percentage',
            'Delete',
            'Edit',
        ];
        if ($bAddNs) {
            array_walk($aColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }

    public function getDefaultEditFields(bool $bAddNs = false): ?array
    {

        $aColumns = [
            'Name',
            'Percentage',
        ];
        if ($bAddNs) {
            array_walk($aColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }

    public function getModel(array $aData = null)
    {
        if ($aData['id']) {
            $oVatQuery = new VatQuery();
            $oVat = $oVatQuery->findOneById($aData['id']);
            if (!$oVat instanceof Vat) {
                throw new LogicException("Vat should be an instance of Vat but got " . get_class($oVat) . " in " . __METHOD__);
            }
        } else {
            $oVat = $this->fillVo(new Vat(), $aData);
        }

        return $oVat;
    }

    private function fillVo(Vat $oVat, $aData)
    {
        if (isset($aData['name'])) {
            $oVat->setName($aData['name']);
        }
        if (isset($aData['percentage'])) {
            $oVat->setPercentage($aData['percentage']);
        }

        return $oVat;
    }

    public function store(array $aData = null)
    {
        $oVat = $this->getModel($aData);
        if (!empty($oVat)) {
            $oVat = $this->fillVo(new Vat(), $aData);
            $oVat->save();
        }
        return $oVat;
    }
}
