<?php

namespace Crud\Vat\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\Vat;

class Percentage extends Field
{
    protected $sFieldLabel = 'Percentage';

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        $fPercentage = (float)$aPostedData['percentage'];

        if (!($fPercentage >= 0)) {
            $mResponse[] = 'Het btw percentage moet positief zijn.';
        } else {
            if (100 < (float)$aPostedData['percentage']) {
                $mResponse[] = 'Het btw percentage kan nooit meer dan 100% zijn.';
            }
        }

        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField('Percentage', 'percentage');
    }

    public function getOverviewValue($oVat)
    {
        if (!$oVat instanceof Vat) {
            throw new InvalidArgumentException("Expected an instance of  model\\Setting\\MasterTable\Vat in " . __METHOD__);
        }

        return '<td class="">' . $oVat->getPercentage() . '%</td>';
    }

    public function getEditHtml($mData, $bReadonly)
    {

        if (!$mData instanceof Vat) {
            throw new InvalidArgumentException("Expected an instance of  model\\Setting\\MasterTable\Vat in " . __METHOD__);
        }

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="percentage" class="col-lg-3 control-label">Percentage</label>';
        $aOut[] = '    <div class="col-lg-8">';
        $aOut[] = '            <div class="input-group">';
        $aOut[] = '            <span class="input-group-addon">';
        $aOut[] = '                <i class="fa fa-tachometer "></i>';
        $aOut[] = '            </span>';
        $aOut[] = '            <input name="data[percentage]" id="percentage" class="form-control" value="' . $mData->getPercentage() . '" placeholder="Voorbeeld: 21" type="text">';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        return join(PHP_EOL, $aOut);
    }
}
