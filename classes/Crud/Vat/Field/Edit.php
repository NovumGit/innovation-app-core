<?php

namespace Crud\Vat\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\Vat;
use InvalidArgumentException;

class Edit extends GenericEdit
{

    public function getEditUrl($oVat)
    {

        if (!$oVat instanceof Vat) {
            throw new InvalidArgumentException('Expected an instance of \\model\\Setting\\MasterTable\\Vat but got ' . get_class($oVat));
        }

        return '/setting/mastertable/vat/edit?id=' . $oVat->getId();
    }
}
