<?php

namespace Crud\Vat\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Vat;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    function getDeleteUrl($oVat)
    {

        if (!$oVat instanceof Vat) {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got ' . get_class($oVat));
        }

        return '/setting/mastertable/vat/edit?_do=Delete&id=' . $oVat->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
