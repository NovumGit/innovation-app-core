<?php
namespace Crud\Vat\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Vat\FieldIterator;
use Crud\Vat\Field\Name;
use Crud\Vat\Field\Percentage;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\VatTableMap;
use Model\Setting\MasterTable\Vat;
use Model\Setting\MasterTable\VatQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Vat instead if you need to override or add functionality.
 */
abstract class CrudVatManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return VatQuery::create();
	}


	public function getTableMap(): VatTableMap
	{
		return new VatTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Vat";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_vat toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_vat aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Percentage'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Percentage'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Vat
	 */
	public function getModel(array $aData = null): Vat
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oVatQuery = VatQuery::create();
		     $oVat = $oVatQuery->findOneById($aData['id']);
		     if (!$oVat instanceof Vat) {
		         throw new LogicException("Vat should be an instance of Vat but got something else." . __METHOD__);
		     }
		     $oVat = $this->fillVo($aData, $oVat);
		}
		else {
		     $oVat = new Vat();
		     if (!empty($aData)) {
		         $oVat = $this->fillVo($aData, $oVat);
		     }
		}
		return $oVat;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Vat
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Vat
	{
		$oVat = $this->getModel($aData);


		 if(!empty($oVat))
		 {
		     $oVat = $this->fillVo($aData, $oVat);
		     $oVat->save();
		 }
		return $oVat;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Vat $oModel
	 * @return Vat
	 */
	protected function fillVo(array $aData, Vat $oModel): Vat
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['percentage'])) {
		     $oField = new Percentage();
		     $mValue = $oField->sanitize($aData['percentage']);
		     $oModel->setPercentage($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
