<?php
namespace Crud\Vat\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Vat\ICollectionField as VatField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Vat\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param VatField[] $aFields */
	private array $aFields = [];


	/**
	 * @param VatField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof VatField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(VatField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): VatField
	{
		return current($this->aFields);
	}
}
