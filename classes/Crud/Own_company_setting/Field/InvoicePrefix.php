<?php
namespace Crud\Own_company_setting\Field;


use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use model\Company\CompanySetting as ModelObject;


class InvoicePrefix extends Field implements IFilterableField, IEditableField{

	private $sFieldName = 'invoice_prefix';
    protected $sFieldLabel = 'Factuur prefix';
	private $sIcon = 'edit';
	private $sPlaceHolder = '';
	private $sGetter = 'getInvoicePrefix';

	function hasValidations()
	{
		return false;
	}
	function getFieldName()
	{
		return 'invoice_prefix';
	}

	function getDataType():string
	{
		return 'string';
	}
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}

	function getOverviewHeader()
	{
		return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
	}
	function getFieldTitle(){
		return $this->sFieldLabel;
	}

	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
	}

	function getEditHtml($mData, $bReadonly)
	{
		if(!$mData instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}

		return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
	}
}
