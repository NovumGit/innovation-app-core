<?php
namespace Crud\Own_company_setting\Field;


use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use model\Company\CompanySetting as ModelObject;


class OrderPrefix extends Field implements IFilterableField, IEditableField{

	private $sFieldName = 'order_prefix';
    protected $sFieldLabel = 'Order prefix';
	private $sIcon = 'edit';
	private $sPlaceHolder = '';
	private $sGetter = 'getOrderPrefix';

	function getFieldName()
	{
		return 'order_prefix';
	}

	function hasValidations()
	{
		return false;
	}

	function getDataType():string
	{
		return 'string';
	}
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}

	function getOverviewHeader()
	{
		return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
	}
	function getFieldTitle(){
		return $this->sFieldLabel;
	}

	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
	}

	function getEditHtml($mData, $bReadonly)
	{
		if(!$mData instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}

		return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
	}
}
