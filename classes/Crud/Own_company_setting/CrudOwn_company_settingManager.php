<?php
namespace Crud\Own_company_setting;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Company\CompanySetting;
use Model\Company\CompanySettingQuery;
use Propel\Runtime\Exception\LogicException;

class CrudOwn_company_settingManager extends FormManager implements IConfigurableCrud
{
	function getEntityTitle():string
	{
		return 'Bedrijfs instelling';
	}
	function getOverviewUrl():string
	{
		return '/company/companysettings';
	}
	function getCreateNewUrl():string
	{
		return '';
	}
	function getNewFormTitle():string{
		return '';
	}
	function getEditFormTitle():string
	{
		return 'Bedrijfs instellingen wijzigen';
	}
	function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
	{
		return [];
	}
	function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

		return [
			'OrderNumber',
			'OrderPrefix',
			'InvoiceNumber',
			'InvoicePrefix'
		];
	}

	function getModel(array $aData = null)
	{
		if($aData['id'])
		{
			$oCompanySettingQuery = new CompanySettingQuery();

			$oCompanySetting = $oCompanySettingQuery->findOneByOwnCompanyId($aData['id']);

			if($oCompanySetting == null){
                $oCompanySetting = new CompanySetting();
                $oCompanySetting->setOwnCompanyId($aData['id']);
                $oCompanySetting->save();
			}

			if(!$oCompanySetting instanceof CompanySetting){
				throw new LogicException("CompanySettings should be an instance of CompanySetting but got ".get_class($oCompanySetting)." in ".__METHOD__);
			}
		}
		else
		{
			$oCompanySetting = new CompanySetting();

			if(!empty($aData))
			{
				$oCompanySetting = $this->fillVo($aData, $oCompanySetting);
			}
		}

		return $oCompanySetting;
	}

	function store(array $aData = null)
	{
		$oCompanySetting = $this->getModel($aData);

		if(!empty($oCompanySetting))
		{
			$oCompanySetting = $this->fillVo($aData, $oCompanySetting);

			$oCompanySetting->save();
		}
		return $oCompanySetting;
	}

	private function fillVo($aData, CompanySetting $oCompanySetting)
	{
		if(isset($aData['order_number'])){$oCompanySetting->setOrderNumber($aData['order_number']);}
		if(isset($aData['order_prefix'])){$oCompanySetting->setOrderPrefix($aData['order_prefix']);}
		if(isset($aData['invoice_number'])){$oCompanySetting->setInvoiceNumber($aData['invoice_number']);}
		if(isset($aData['invoice_prefix'])){$oCompanySetting->setInvoicePrefix($aData['invoice_prefix']);}

		return $oCompanySetting;
	}
}
