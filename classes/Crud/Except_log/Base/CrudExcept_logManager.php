<?php
namespace Crud\Except_log\Base;

use Core\Utils;
use Crud;
use Crud\Except_log\FieldIterator;
use Crud\Except_log\Field\Backtrace;
use Crud\Except_log\Field\CookieString;
use Crud\Except_log\Field\Domain;
use Crud\Except_log\Field\ExceptionType;
use Crud\Except_log\Field\Fatal;
use Crud\Except_log\Field\File;
use Crud\Except_log\Field\Fingerprint;
use Crud\Except_log\Field\FirstOccurence;
use Crud\Except_log\Field\Ip;
use Crud\Except_log\Field\LastOccurence;
use Crud\Except_log\Field\Line;
use Crud\Except_log\Field\Message;
use Crud\Except_log\Field\OccurenceCount;
use Crud\Except_log\Field\Referrer;
use Crud\Except_log\Field\SerializedGet;
use Crud\Except_log\Field\SerializedPost;
use Crud\Except_log\Field\SerializedSession;
use Crud\Except_log\Field\Url;
use Crud\Except_log\Field\UserAgent;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Logging\Except_log;
use Model\Logging\Except_logQuery;
use Model\Logging\Map\Except_logTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Except_log instead if you need to override or add functionality.
 */
abstract class CrudExcept_logManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Except_logQuery::create();
	}


	public function getTableMap(): Except_logTableMap
	{
		return new Except_logTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Except_log";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "except_log toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "except_log aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ExceptionType', 'Fingerprint', 'Message', 'File', 'Line', 'Backtrace', 'FirstOccurence', 'LastOccurence', 'OccurenceCount', 'Domain', 'Url', 'Referrer', 'SerializedSession', 'SerializedGet', 'SerializedPost', 'CookieString', 'UserAgent', 'Ip', 'Fatal'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ExceptionType', 'Fingerprint', 'Message', 'File', 'Line', 'Backtrace', 'FirstOccurence', 'LastOccurence', 'OccurenceCount', 'Domain', 'Url', 'Referrer', 'SerializedSession', 'SerializedGet', 'SerializedPost', 'CookieString', 'UserAgent', 'Ip', 'Fatal'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Except_log
	 */
	public function getModel(array $aData = null): Except_log
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oExcept_logQuery = Except_logQuery::create();
		     $oExcept_log = $oExcept_logQuery->findOneById($aData['id']);
		     if (!$oExcept_log instanceof Except_log) {
		         throw new LogicException("Except_log should be an instance of Except_log but got something else." . __METHOD__);
		     }
		     $oExcept_log = $this->fillVo($aData, $oExcept_log);
		}
		else {
		     $oExcept_log = new Except_log();
		     if (!empty($aData)) {
		         $oExcept_log = $this->fillVo($aData, $oExcept_log);
		     }
		}
		return $oExcept_log;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Except_log
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Except_log
	{
		$oExcept_log = $this->getModel($aData);


		 if(!empty($oExcept_log))
		 {
		     $oExcept_log = $this->fillVo($aData, $oExcept_log);
		     $oExcept_log->save();
		 }
		return $oExcept_log;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Except_log $oModel
	 * @return Except_log
	 */
	protected function fillVo(array $aData, Except_log $oModel): Except_log
	{
		if(isset($aData['exception_type'])) {
		     $oField = new ExceptionType();
		     $mValue = $oField->sanitize($aData['exception_type']);
		     $oModel->setExceptionType($mValue);
		}
		if(isset($aData['fingerprint'])) {
		     $oField = new Fingerprint();
		     $mValue = $oField->sanitize($aData['fingerprint']);
		     $oModel->setFingerprint($mValue);
		}
		if(isset($aData['message'])) {
		     $oField = new Message();
		     $mValue = $oField->sanitize($aData['message']);
		     $oModel->setMessage($mValue);
		}
		if(isset($aData['file'])) {
		     $oField = new File();
		     $mValue = $oField->sanitize($aData['file']);
		     $oModel->setFile($mValue);
		}
		if(isset($aData['line'])) {
		     $oField = new Line();
		     $mValue = $oField->sanitize($aData['line']);
		     $oModel->setLine($mValue);
		}
		if(isset($aData['backtrace'])) {
		     $oField = new Backtrace();
		     $mValue = $oField->sanitize($aData['backtrace']);
		     $oModel->setBacktrace($mValue);
		}
		if(isset($aData['first_occurence'])) {
		     $oField = new FirstOccurence();
		     $mValue = $oField->sanitize($aData['first_occurence']);
		     $oModel->setFirstOccurence($mValue);
		}
		if(isset($aData['last_occurence'])) {
		     $oField = new LastOccurence();
		     $mValue = $oField->sanitize($aData['last_occurence']);
		     $oModel->setLastOccurence($mValue);
		}
		if(isset($aData['occurence_count'])) {
		     $oField = new OccurenceCount();
		     $mValue = $oField->sanitize($aData['occurence_count']);
		     $oModel->setOccurenceCount($mValue);
		}
		if(isset($aData['domain'])) {
		     $oField = new Domain();
		     $mValue = $oField->sanitize($aData['domain']);
		     $oModel->setDomain($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		if(isset($aData['referrer'])) {
		     $oField = new Referrer();
		     $mValue = $oField->sanitize($aData['referrer']);
		     $oModel->setReferrer($mValue);
		}
		if(isset($aData['serialized_session'])) {
		     $oField = new SerializedSession();
		     $mValue = $oField->sanitize($aData['serialized_session']);
		     $oModel->setSerializedSession($mValue);
		}
		if(isset($aData['serialized_get'])) {
		     $oField = new SerializedGet();
		     $mValue = $oField->sanitize($aData['serialized_get']);
		     $oModel->setSerializedGet($mValue);
		}
		if(isset($aData['serialized_post'])) {
		     $oField = new SerializedPost();
		     $mValue = $oField->sanitize($aData['serialized_post']);
		     $oModel->setSerializedPost($mValue);
		}
		if(isset($aData['cookie_string'])) {
		     $oField = new CookieString();
		     $mValue = $oField->sanitize($aData['cookie_string']);
		     $oModel->setCookieString($mValue);
		}
		if(isset($aData['user_agent'])) {
		     $oField = new UserAgent();
		     $mValue = $oField->sanitize($aData['user_agent']);
		     $oModel->setUserAgent($mValue);
		}
		if(isset($aData['ip'])) {
		     $oField = new Ip();
		     $mValue = $oField->sanitize($aData['ip']);
		     $oModel->setIp($mValue);
		}
		if(isset($aData['fatal'])) {
		     $oField = new Fatal();
		     $mValue = $oField->sanitize($aData['fatal']);
		     $oModel->setFatal($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
