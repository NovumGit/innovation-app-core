<?php
namespace Crud\Except_log\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Except_log\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
