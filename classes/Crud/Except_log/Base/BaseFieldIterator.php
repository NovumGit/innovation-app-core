<?php
namespace Crud\Except_log\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Except_log\ICollectionField as Except_logField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Except_log\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Except_logField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Except_logField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Except_logField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Except_logField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Except_logField
	{
		return current($this->aFields);
	}
}
