<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Except_log\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Logging\Except_log as ModelObject;


class FirstOccurence extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'first_occurence';
    protected $sFieldLabel = 'Eerste voorkomen';
    private $sIcon = '';
    private $sPlaceHolder = 'date';
    private $sGetter = 'getFirstOccurence';


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'date';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editDateTimePicker($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}());
    }
}
