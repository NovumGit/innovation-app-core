<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Except_log\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Logging\Except_log as ModelObject;


class SerializedSession extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'serialized_session';
    protected $sFieldLabel = 'Sessie variabelen';
    private $sIcon = 'code';
    private $sPlaceHolder = 'string';
    private $sGetter = 'getSerializedSession';


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aSession = unserialize($oModelObject->{$this->sGetter}());

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="fld_serialized_session" class="col-lg-4 control-label">';
        $aOut[] = '    Sessie variabelen';
        $aOut[] = '    </label>';
        $aOut[] = '    <div class="col-lg-8">';
        $aOut[] = '        <div class="input-group">';
        $aOut[] = '            <pre>'. print_r($aSession, true) .'</pre>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        return join(PHP_EOL, $aOut);
    }
}
