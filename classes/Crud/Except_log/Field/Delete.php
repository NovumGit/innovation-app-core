<?php
namespace Crud\Except_log\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Logging\Except_log as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/admin/exception/edit?_do=Delete&id='.$oModelObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}