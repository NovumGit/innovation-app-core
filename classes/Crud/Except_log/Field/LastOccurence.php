<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Except_log\Field;

use Core\Config;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Logging\Except_log as ModelObject;


class LastOccurence extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'last_occurence';
    protected $sFieldLabel = 'Laatste voorkomen';
    private $sGetter = 'getLastOccurence';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sCreatedOn = '';
        $oCreatedOn  = $oModelObject->{$this->sGetter}();
        if($oCreatedOn instanceof \DateTime)
        {
            $sCreatedOn = $oCreatedOn->format(Config::getDateTimeFormat());
        }

        return '<td class="">'.$sCreatedOn.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editDateTimePicker($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}());
    }
}
