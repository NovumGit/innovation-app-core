<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Except_log\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Logging\Except_log as ModelObject;


class SerializedGet extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'serialized_get';
    protected $sFieldLabel = 'Get variabelen';
    private $sIcon = 'code';
    private $sPlaceHolder = '';
    private $sGetter = 'getSerializedGet';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return '';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aGet = unserialize($oModelObject->{$this->sGetter}());

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="fld_serialized_session" class="col-lg-4 control-label">';
        $aOut[] = '    Get variabelen';
        $aOut[] = '    </label>';
        $aOut[] = '    <div class="col-lg-8">';
        $aOut[] = '        <div class="input-group">';
        $aOut[] = '            <pre>'. print_r($aGet, true) .'</pre>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        return join(PHP_EOL, $aOut);
    }
}
