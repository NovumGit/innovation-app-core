<?php
namespace Crud\Except_log\Field\Base;

use Crud\Except_log\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'last_occurence' crud field from the 'except_log' table.
 * This class is auto generated and should not be modified.
 */
abstract class LastOccurence extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'last_occurence';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLastOccurence';
	protected $sFqModelClassname = '\\\Model\Logging\Except_log';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
