<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Except_log\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Logging\Except_log as ModelObject;


class Fatal extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'fatal';
    protected $sFieldLabel = 'Fatal error';
    private $sIcon = '{{ fieldicon }}';
    private $sPlaceHolder = 'boolean';
    private $sGetter = 'getFatal';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'boolean';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
