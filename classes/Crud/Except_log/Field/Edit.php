<?php
namespace Crud\Except_log\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Logging\Except_log as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/admin/exception/edit?id='.$oModelObject->getId();
    }
}

