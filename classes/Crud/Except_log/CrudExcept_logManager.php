<?php
namespace Crud\Except_log;

use Crud\FormManager;
use Model\Logging\Except_logQuery;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Model\Logging\Except_log;
use Model\Logging\Map\Except_logTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Map\TableMap;

class CrudExcept_logManager extends FormManager implements IConfigurableCrud, IApiExposable
{
    function getQueryObject(): ModelCriteria
    {
        return Except_logQuery::create();
    }
    function getTableMap(): TableMap
    {
        return new Except_logTableMap();
    }
    function getShortDescription(): string
    {
        return 'The except log is where almost all exceptions occuring in the system are logged';
    }

    function getEntityTitle():string
    {
        return 'exception_log';
    }
    function getOverviewUrl():string
    {
        return '/admin/exception/overview';
    }
    function getCreateNewUrl():string
    {
        return '/admin/exception/edit';
    }
    function getNewFormTitle():string{
        return 'Uitzondering toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Uitzondering wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Message',
            'LastOccurence',
            'OccurenceCount',
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Message',
            'FirstOccurence',
            'LastOccurence',
            'OccurenceCount',
            'Backtrace',
            'Domain',
            'ExceptionType',
            'Fatal',
            'File',
            'Fingerprint',
            'Ip',
            'Line',
            'Referrer',
            'SerializedGet',
            'SerializedPost',
            'SerializedSession',
            'Url',
            'UserAgent',
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oExcept_logQuery = Except_logQuery::create();
            $oExcept_log = $oExcept_logQuery->findOneById($aData['id']);

            if(!$oExcept_log instanceof Except_log){
                throw new LogicException("Except_log should be an instance of Except_log but got ".get_class($oExcept_log)." in ".__METHOD__);
            }
        }
        else
        {
            $oExcept_log = new Except_log();
            if(!empty($aData))
            {
                $oExcept_log = $this->fillVo($aData, $oExcept_log);
            }
        }
        return $oExcept_log;
    }

    /**
     * @param $aData
     * @return Except_log
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null)
    {
        $oExcept_log = $this->getModel($aData);
        if(!empty($oExcept_log))
        {
            $oExcept_log = $this->fillVo($aData, $oExcept_log);

            $oExcept_log->save();
        }
        return $oExcept_log;
    }

    private function fillVo($aData, Except_log $oExcept_log)
    {
        if(isset($aData['backtrace'])){$oExcept_log->setBacktrace($aData['backtrace']);}
        if(isset($aData['cookie_string'])){$oExcept_log->setCookieString($aData['cookie_string']);}
        if(isset($aData['domain'])){$oExcept_log->setDomain($aData['domain']);}
        if(isset($aData['exception_type'])){$oExcept_log->setExceptionType($aData['exception_type']);}
        if(isset($aData['fatal'])){$oExcept_log->setFatal($aData['fatal']);}
        if(isset($aData['file'])){$oExcept_log->setFile($aData['file']);}
        if(isset($aData['fingerprint'])){$oExcept_log->setFingerprint($aData['fingerprint']);}
        if(isset($aData['first_occurence'])){$oExcept_log->setFirstOccurence($aData['first_occurence']);}
        if(isset($aData['ip'])){$oExcept_log->setIp($aData['ip']);}
        if(isset($aData['last_occurence'])){$oExcept_log->setLastOccurence($aData['last_occurence']);}
        if(isset($aData['line'])){$oExcept_log->setLine($aData['line']);}
        if(isset($aData['message'])){$oExcept_log->setMessage($aData['message']);}
        if(isset($aData['occurence_count'])){$oExcept_log->setOccurenceCount($aData['occurence_count']);}
        if(isset($aData['referrer'])){$oExcept_log->setReferrer($aData['referrer']);}
        if(isset($aData['serialized_get'])){$oExcept_log->setSerializedGet($aData['serialized_get']);}
        if(isset($aData['serialized_post'])){$oExcept_log->setSerializedPost($aData['serialized_post']);}
        if(isset($aData['serialized_session'])){$oExcept_log->setSerializedSession($aData['serialized_session']);}
        if(isset($aData['url'])){$oExcept_log->setUrl($aData['url']);}
        if(isset($aData['user_agent'])){$oExcept_log->setUserAgent($aData['user_agent']);}

        return $oExcept_log;
    }
}
