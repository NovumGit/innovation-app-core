<?php
namespace Crud\SiteBlog\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\SiteBlog\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
