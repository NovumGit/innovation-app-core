<?php
namespace Crud\SiteBlog\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteBlog\FieldIterator;
use Crud\SiteBlog\Field\Content;
use Crud\SiteBlog\Field\CreatedByUserId;
use Crud\SiteBlog\Field\CreatedOn;
use Crud\SiteBlog\Field\FileExt;
use Crud\SiteBlog\Field\FileMime;
use Crud\SiteBlog\Field\FileOriginalName;
use Crud\SiteBlog\Field\FileSize;
use Crud\SiteBlog\Field\LanguageId;
use Crud\SiteBlog\Field\SiteId;
use Crud\SiteBlog\Field\Tag;
use Crud\SiteBlog\Field\Teaser;
use Crud\SiteBlog\Field\Title;
use Exception\LogicException;
use Model\Cms\Map\SiteBlogTableMap;
use Model\Cms\SiteBlog;
use Model\Cms\SiteBlogQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteBlog instead if you need to override or add functionality.
 */
abstract class CrudSiteBlogManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteBlogQuery::create();
	}


	public function getTableMap(): SiteBlogTableMap
	{
		return new SiteBlogTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteBlog";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_blog toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_blog aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'CreatedOn', 'CreatedByUserId', 'Title', 'Content', 'Teaser', 'Tag', 'FileOriginalName', 'FileExt', 'FileSize', 'FileMime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'CreatedOn', 'CreatedByUserId', 'Title', 'Content', 'Teaser', 'Tag', 'FileOriginalName', 'FileExt', 'FileSize', 'FileMime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteBlog
	 */
	public function getModel(array $aData = null): SiteBlog
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteBlogQuery = SiteBlogQuery::create();
		     $oSiteBlog = $oSiteBlogQuery->findOneById($aData['id']);
		     if (!$oSiteBlog instanceof SiteBlog) {
		         throw new LogicException("SiteBlog should be an instance of SiteBlog but got something else." . __METHOD__);
		     }
		     $oSiteBlog = $this->fillVo($aData, $oSiteBlog);
		}
		else {
		     $oSiteBlog = new SiteBlog();
		     if (!empty($aData)) {
		         $oSiteBlog = $this->fillVo($aData, $oSiteBlog);
		     }
		}
		return $oSiteBlog;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteBlog
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteBlog
	{
		$oSiteBlog = $this->getModel($aData);


		 if(!empty($oSiteBlog))
		 {
		     $oSiteBlog = $this->fillVo($aData, $oSiteBlog);
		     $oSiteBlog->save();
		 }
		return $oSiteBlog;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteBlog $oModel
	 * @return SiteBlog
	 */
	protected function fillVo(array $aData, SiteBlog $oModel): SiteBlog
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['created_by_user_id'])) {
		     $oField = new CreatedByUserId();
		     $mValue = $oField->sanitize($aData['created_by_user_id']);
		     $oModel->setCreatedByUserId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['content'])) {
		     $oField = new Content();
		     $mValue = $oField->sanitize($aData['content']);
		     $oModel->setContent($mValue);
		}
		if(isset($aData['teaser'])) {
		     $oField = new Teaser();
		     $mValue = $oField->sanitize($aData['teaser']);
		     $oModel->setTeaser($mValue);
		}
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		if(isset($aData['file_original_name'])) {
		     $oField = new FileOriginalName();
		     $mValue = $oField->sanitize($aData['file_original_name']);
		     $oModel->setFileOriginalName($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		if(isset($aData['file_size'])) {
		     $oField = new FileSize();
		     $mValue = $oField->sanitize($aData['file_size']);
		     $oModel->setFileSize($mValue);
		}
		if(isset($aData['file_mime'])) {
		     $oField = new FileMime();
		     $mValue = $oField->sanitize($aData['file_mime']);
		     $oModel->setFileMime($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
