<?php
namespace Crud\SiteBlog\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteBlog\ICollectionField;

/**
 * Base class that represents the 'file_ext' crud field from the 'site_blog' table.
 * This class is auto generated and should not be modified.
 */
abstract class FileExt extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'file_ext';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFileExt';
	protected $sFqModelClassname = '\\\Model\Cms\SiteBlog';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
