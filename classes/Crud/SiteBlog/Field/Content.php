<?php
namespace Crud\SiteBlog\Field;

use Crud\SiteBlog\Field\Base\Content as BaseContent;

/**
 * Skeleton subclass for representing content field from the site_blog table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Content extends BaseContent
{
}
