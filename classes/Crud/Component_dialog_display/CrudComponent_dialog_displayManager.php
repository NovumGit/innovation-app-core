<?php
namespace Crud\Component_dialog_display;

/**
 * Skeleton subclass for representing a Component_dialog_display.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudComponent_dialog_displayManager extends Base\CrudComponent_dialog_displayManager
{
}
