<?php
namespace Crud\Component_dialog_display\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_dialog_display\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
