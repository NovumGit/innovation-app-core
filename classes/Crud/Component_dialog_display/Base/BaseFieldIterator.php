<?php
namespace Crud\Component_dialog_display\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_dialog_display\ICollectionField as Component_dialog_displayField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_dialog_display\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_dialog_displayField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_dialog_displayField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_dialog_displayField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_dialog_displayField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_dialog_displayField
	{
		return current($this->aFields);
	}
}
