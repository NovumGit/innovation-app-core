<?php
namespace Crud\Component_dialog_display\Field\Base;

use Crud\Component_dialog_display\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'item_key' crud field from the 'component_dialog_display' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_key';
	protected $sFieldLabel = 'Display';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemKey';
	protected $sFqModelClassname = '\Model\System\LowCode\Dialog\Component_dialog_display';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
