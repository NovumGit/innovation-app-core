<?php 
namespace Crud\Component_dialog_display\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Dialog\Component_dialog_display;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_dialog_display)
		{
		     return "//system/component_dialog_display/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_dialog_display)
		{
		     return "//component_dialog_display?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
