<?php
namespace Crud\Component_dialog_display\Field;

use Crud\Component_dialog_display\Field\Base\ItemLabel as BaseItemLabel;

/**
 * Skeleton subclass for representing item_label field from the component_dialog_display table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ItemLabel extends BaseItemLabel
{
}
