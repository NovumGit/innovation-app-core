<?php
namespace Crud\MimeType\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MimeType\FieldIterator;
use Crud\MimeType\Field\Name;
use Exception\LogicException;
use Model\System\Map\MimeTypeTableMap;
use Model\System\MimeType;
use Model\System\MimeTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MimeType instead if you need to override or add functionality.
 */
abstract class CrudMimeTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MimeTypeQuery::create();
	}


	public function getTableMap(): MimeTypeTableMap
	{
		return new MimeTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat soorten data, generieke datatypen binnen deze fictieve overheid.";
	}


	public function getEntityTitle(): string
	{
		return "MimeType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mime_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mime_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MimeType
	 */
	public function getModel(array $aData = null): MimeType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMimeTypeQuery = MimeTypeQuery::create();
		     $oMimeType = $oMimeTypeQuery->findOneById($aData['id']);
		     if (!$oMimeType instanceof MimeType) {
		         throw new LogicException("MimeType should be an instance of MimeType but got something else." . __METHOD__);
		     }
		     $oMimeType = $this->fillVo($aData, $oMimeType);
		}
		else {
		     $oMimeType = new MimeType();
		     if (!empty($aData)) {
		         $oMimeType = $this->fillVo($aData, $oMimeType);
		     }
		}
		return $oMimeType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MimeType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MimeType
	{
		$oMimeType = $this->getModel($aData);


		 if(!empty($oMimeType))
		 {
		     $oMimeType = $this->fillVo($aData, $oMimeType);
		     $oMimeType->save();
		 }
		return $oMimeType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MimeType $oModel
	 * @return MimeType
	 */
	protected function fillVo(array $aData, MimeType $oModel): MimeType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
