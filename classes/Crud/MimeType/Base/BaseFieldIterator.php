<?php
namespace Crud\MimeType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MimeType\ICollectionField as MimeTypeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MimeType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MimeTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MimeTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MimeTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MimeTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MimeTypeField
	{
		return current($this->aFields);
	}
}
