<?php 
namespace Crud\MimeType\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\MimeType;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof MimeType)
		{
		     return "//system/mime_type/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof MimeType)
		{
		     return "//mime_type?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
