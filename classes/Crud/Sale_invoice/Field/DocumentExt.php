<?php

namespace Crud\Sale_invoice\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\SaleInvoice;

class DocumentExt extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'document_ext';
    protected $sFieldLabel = 'Bestandsextentie';
    protected $sIcon = 'file';
    protected $sPlaceHolder = 'Bijv pdf of xls';
    protected $sGetter = 'getDocumentExt';
    protected $sFqModelClassname = SaleInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
