<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Crud\Field;
use Crud\Generic\Field\GenericEditTextfield;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\SaleInvoice as ModelObject;
use Model\Finance\SaleInvoice;


class InvoiceNumber extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'invoice_number';
    protected $sFieldLabel = 'Factuurnummer';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'Factuurnummer';
    protected $sGetter = 'getInvoiceNumber';
    protected $sFqModelClassname = SaleInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }


}
