<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Crud\Field;
use Crud\Generic\Field\GenericEditTextfield;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\SaleInvoice as ModelObject;
use Model\Finance\SaleInvoice;


class OutstandingAmount extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'outstanding_amount';
    protected $sFieldLabel = 'Openstaand bedrag';
    protected $sIcon = 'money';
    protected $sPlaceHolder = 'Openstaand bedrag';
    protected $sGetter = 'getOutstandingAmount';
    protected $sFqModelClassname = SaleInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'float';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

}
