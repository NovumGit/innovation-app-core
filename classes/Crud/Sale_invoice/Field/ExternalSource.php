<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\SaleInvoice;

class ExternalSource extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'external_source';
    protected $sFieldLabel = 'Herkomst';
    protected $sIcon = 'globe';
    protected $sPlaceHolder = 'Bijvoorbeeld Moneymonk';
    protected $sGetter = 'getExternalSource';
    protected $sFqModelClassname = SaleInvoice::class;


    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
