<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\SaleInvoice;

class ExternalId extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'external_id';
    protected $sFieldLabel = 'Extern id';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'ID van boekhoud pakket bijvoorbeeld';
    protected $sGetter = 'getExternalId';
    protected $sFqModelClassname = SaleInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
