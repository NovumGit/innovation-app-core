<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Finance\SaleInvoice;

class CustomerId extends GenericLookup implements IFilterableLookupField
{

    protected $sFieldName = 'customer_id';
    protected $sFieldLabel = 'Klant';
    protected $sIcon = 'user';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getCustomerId';
    protected $sFqModelClassname = SaleInvoice::class;

    function getVisibleValue($iItemId)
    {
        $oCustomer = CustomerQuery::create()->findOneById($iItemId);
        if ($oCustomer instanceof Customer) {
            return $oCustomer->getCompanyName();
        }
        return '';
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof SaleInvoice)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOut = [];
        $aOut[] =  '<td class="">';
        $aOut[] =   '   <a href="/crm/customer_edit?id=' . $oModelObject->getCustomerId() . '">';
        $aOut[] =   '       '.$this->getVisibleValue($oModelObject->{$this->sGetter}());
        $aOut[] =   '   </a>';
        $aOut[] =  '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getLookups($mSelectedItem = null)
    {
        $aCustomers = CustomerQuery::create()
            ->useCustomerCompanyQuery()
            ->orderByCompanyName()
            ->endUse()
            ->find();

        $aOptions = Utils::makeSelectOptions($aCustomers, 'getCompanyName', $mSelectedItem);
        return $aOptions;
    }


    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

}
