<?php

namespace Crud\Sale_invoice\Field;

use Crud\Generic\Field\GenericEdit;

class Edit extends GenericEdit
{
    function getEditUrl($oObject)
    {
        return '/sale/invoice/edit';
    }
}