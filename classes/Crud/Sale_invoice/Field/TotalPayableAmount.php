<?php
/**
 * @Author Anton Boutkam
 * @generated
 */

namespace Crud\Sale_invoice\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Crud\Generic\Field\GenericFloat;
use Crud\Generic\Field\GenericValutaEuro;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\SaleInvoice;

class TotalPayableAmount extends GenericValutaEuro implements IFilterableField, IEditableField
{

    protected $sFieldName = 'total_payable_amount';
    protected $sFieldLabel = 'Totaalbedrag';
    protected $sIcon = 'money';
    protected $sPlaceHolder = 'Totaal te betalen';
    protected $sGetter = 'getTotalPayableAmount';
    protected $sFqModelClassname = SaleInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'float';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
