<?php

namespace Crud\Sale_invoice;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\SaleInvoice;
use Model\Finance\SaleInvoiceQuery;

class CrudSale_invoiceManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Verkoop facturen';
    }

    function getOverviewUrl():string
    {
        return '/sale/invoice/overview';
    }

    function getCreateNewUrl():string
    {
        return '/sale/invoice/edit';
    }

    function getNewFormTitle():string
    {
        return 'Verkoop factuur toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Verkoop factuur bewerken';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CustomerId',
            'DocumentExt',
            'ExternalId',
            'ExternalSource',
            'InvoiceNumber',
            'OutstandingAmount',
            'TotalPayableAmount'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CustomerId',
            'DocumentExt',
            'ExternalId',
            'ExternalSource',
            'InvoiceNumber',
            'OutstandingAmount',
            'TotalPayableAmount'
        ];
    }

    function getModel(array $aData = null)
    {
        if (isset($aData['id']) && $aData['id']) {
            $oSaleInvoiceQuery = SaleInvoiceQuery::create();
            $oSaleInvoice = $oSaleInvoiceQuery->findOneById($aData['id']);

            if (!$oSaleInvoice instanceof SaleInvoice) {
                throw new LogicException("Project should be an instance of SaleInvoice but got something else." . __METHOD__);
            }
            $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
        } else {
            $oSaleInvoice = new SaleInvoice();
            if (!empty($aData)) {
                $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
            }
        }
        return $oSaleInvoice;
    }

    /**
     * @param $aData
     * @return SaleInvoice
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null): SaleInvoice
    {
        $oSaleInvoice = $this->getModel($aData);
        if ($oSaleInvoice instanceof SaleInvoice) {
            $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
            $oSaleInvoice->save();
        }
        return $oSaleInvoice;
    }

    private function fillVo($aData, SaleInvoice $oModel)
    {
        if (isset($aData['customer_id'])) {
            $oModel->setCustomerId($aData['customer_id']);
        }
        if (isset($aData['document_ext'])) {
            $oModel->setDocumentExt($aData['document_ext']);
        }
        if (isset($aData['external_id'])) {
            $oModel->setExternalId($aData['external_id']);
        }
        if (isset($aData['external_source'])) {
            $oModel->setExternalSource($aData['external_source']);
        }
        if (isset($aData['invoice_number'])) {
            $oModel->setInvoiceNumber($aData['invoice_number']);
        }
        if (isset($aData['outstanding_amount'])) {
            $oModel->setOutstandingAmount($aData['outstanding_amount']);
        }
        if (isset($aData['total_payable_amount'])) {
            $oModel->setTotalPayableAmount($aData['total_payable_amount']);
        }
        return $oModel;

    }
}
