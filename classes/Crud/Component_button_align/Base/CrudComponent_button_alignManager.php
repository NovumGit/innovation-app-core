<?php
namespace Crud\Component_button_align\Base;

use Core\Utils;
use Crud;
use Crud\Component_button_align\FieldIterator;
use Crud\Component_button_align\Field\ItemKey;
use Crud\Component_button_align\Field\ItemLabel;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Button\Component_button_align;
use Model\System\LowCode\Button\Component_button_alignQuery;
use Model\System\LowCode\Button\Map\Component_button_alignTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_button_align instead if you need to override or add functionality.
 */
abstract class CrudComponent_button_alignManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_button_alignQuery::create();
	}


	public function getTableMap(): Component_button_alignTableMap
	{
		return new Component_button_alignTableMap();
	}


	public function getShortDescription(): string
	{
		return "Used for validation and to populate dropdown values.";
	}


	public function getEntityTitle(): string
	{
		return "Component_button_align";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_button_align toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_button_align aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_button_align
	 */
	public function getModel(array $aData = null): Component_button_align
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_button_alignQuery = Component_button_alignQuery::create();
		     $oComponent_button_align = $oComponent_button_alignQuery->findOneById($aData['id']);
		     if (!$oComponent_button_align instanceof Component_button_align) {
		         throw new LogicException("Component_button_align should be an instance of Component_button_align but got something else." . __METHOD__);
		     }
		     $oComponent_button_align = $this->fillVo($aData, $oComponent_button_align);
		}
		else {
		     $oComponent_button_align = new Component_button_align();
		     if (!empty($aData)) {
		         $oComponent_button_align = $this->fillVo($aData, $oComponent_button_align);
		     }
		}
		return $oComponent_button_align;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_button_align
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_button_align
	{
		$oComponent_button_align = $this->getModel($aData);


		 if(!empty($oComponent_button_align))
		 {
		     $oComponent_button_align = $this->fillVo($aData, $oComponent_button_align);
		     $oComponent_button_align->save();
		 }
		return $oComponent_button_align;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_button_align $oModel
	 * @return Component_button_align
	 */
	protected function fillVo(array $aData, Component_button_align $oModel): Component_button_align
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_label'])) {
		     $oField = new ItemLabel();
		     $mValue = $oField->sanitize($aData['item_label']);
		     $oModel->setItemLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
