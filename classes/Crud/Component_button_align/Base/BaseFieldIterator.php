<?php
namespace Crud\Component_button_align\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_button_align\ICollectionField as Component_button_alignField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_button_align\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_button_alignField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_button_alignField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_button_alignField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_button_alignField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_button_alignField
	{
		return current($this->aFields);
	}
}
