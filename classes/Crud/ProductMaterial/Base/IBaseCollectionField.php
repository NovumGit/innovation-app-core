<?php
namespace Crud\ProductMaterial\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\ProductMaterial\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
