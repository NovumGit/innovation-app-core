<?php
namespace Crud\ProductMaterial\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductMaterial\FieldIterator;
use Crud\ProductMaterial\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ProductMaterialTableMap;
use Model\Setting\MasterTable\ProductMaterial;
use Model\Setting\MasterTable\ProductMaterialQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductMaterial instead if you need to override or add functionality.
 */
abstract class CrudProductMaterialManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductMaterialQuery::create();
	}


	public function getTableMap(): ProductMaterialTableMap
	{
		return new ProductMaterialTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductMaterial";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_material toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_material aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductMaterial
	 */
	public function getModel(array $aData = null): ProductMaterial
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductMaterialQuery = ProductMaterialQuery::create();
		     $oProductMaterial = $oProductMaterialQuery->findOneById($aData['id']);
		     if (!$oProductMaterial instanceof ProductMaterial) {
		         throw new LogicException("ProductMaterial should be an instance of ProductMaterial but got something else." . __METHOD__);
		     }
		     $oProductMaterial = $this->fillVo($aData, $oProductMaterial);
		}
		else {
		     $oProductMaterial = new ProductMaterial();
		     if (!empty($aData)) {
		         $oProductMaterial = $this->fillVo($aData, $oProductMaterial);
		     }
		}
		return $oProductMaterial;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductMaterial
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductMaterial
	{
		$oProductMaterial = $this->getModel($aData);


		 if(!empty($oProductMaterial))
		 {
		     $oProductMaterial = $this->fillVo($aData, $oProductMaterial);
		     $oProductMaterial->save();
		 }
		return $oProductMaterial;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductMaterial $oModel
	 * @return ProductMaterial
	 */
	protected function fillVo(array $aData, ProductMaterial $oModel): ProductMaterial
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
