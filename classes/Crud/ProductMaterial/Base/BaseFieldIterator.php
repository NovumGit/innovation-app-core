<?php
namespace Crud\ProductMaterial\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductMaterial\ICollectionField as ProductMaterialField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductMaterial\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductMaterialField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductMaterialField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductMaterialField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductMaterialField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductMaterialField
	{
		return current($this->aFields);
	}
}
