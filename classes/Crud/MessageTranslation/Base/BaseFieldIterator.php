<?php
namespace Crud\MessageTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MessageTranslation\ICollectionField as MessageTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MessageTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MessageTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MessageTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MessageTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MessageTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MessageTranslationField
	{
		return current($this->aFields);
	}
}
