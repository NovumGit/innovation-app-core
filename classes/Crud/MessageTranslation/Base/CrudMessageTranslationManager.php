<?php
namespace Crud\MessageTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MessageTranslation\FieldIterator;
use Crud\MessageTranslation\Field\DatasourceEndpointFromId;
use Crud\MessageTranslation\Field\DatasourceEndpointToId;
use Crud\MessageTranslation\Field\Titel;
use Exception\LogicException;
use Model\Custom\NovumOverheid\Map\MessageTranslationTableMap;
use Model\Custom\NovumOverheid\MessageTranslation;
use Model\Custom\NovumOverheid\MessageTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MessageTranslation instead if you need to override or add functionality.
 */
abstract class CrudMessageTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MessageTranslationQuery::create();
	}


	public function getTableMap(): MessageTranslationTableMap
	{
		return new MessageTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bericht vertaling.";
	}


	public function getEntityTitle(): string
	{
		return "MessageTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "message_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "message_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'DatasourceEndpointFromId', 'DatasourceEndpointToId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'DatasourceEndpointFromId', 'DatasourceEndpointToId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MessageTranslation
	 */
	public function getModel(array $aData = null): MessageTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMessageTranslationQuery = MessageTranslationQuery::create();
		     $oMessageTranslation = $oMessageTranslationQuery->findOneById($aData['id']);
		     if (!$oMessageTranslation instanceof MessageTranslation) {
		         throw new LogicException("MessageTranslation should be an instance of MessageTranslation but got something else." . __METHOD__);
		     }
		     $oMessageTranslation = $this->fillVo($aData, $oMessageTranslation);
		}
		else {
		     $oMessageTranslation = new MessageTranslation();
		     if (!empty($aData)) {
		         $oMessageTranslation = $this->fillVo($aData, $oMessageTranslation);
		     }
		}
		return $oMessageTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MessageTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MessageTranslation
	{
		$oMessageTranslation = $this->getModel($aData);


		 if(!empty($oMessageTranslation))
		 {
		     $oMessageTranslation = $this->fillVo($aData, $oMessageTranslation);
		     $oMessageTranslation->save();
		 }
		return $oMessageTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MessageTranslation $oModel
	 * @return MessageTranslation
	 */
	protected function fillVo(array $aData, MessageTranslation $oModel): MessageTranslation
	{
		if(isset($aData['titel'])) {
		     $oField = new Titel();
		     $mValue = $oField->sanitize($aData['titel']);
		     $oModel->setTitel($mValue);
		}
		if(isset($aData['datasource_endpoint_from_id'])) {
		     $oField = new DatasourceEndpointFromId();
		     $mValue = $oField->sanitize($aData['datasource_endpoint_from_id']);
		     $oModel->setDatasourceEndpointFromId($mValue);
		}
		if(isset($aData['datasource_endpoint_to_id'])) {
		     $oField = new DatasourceEndpointToId();
		     $mValue = $oField->sanitize($aData['datasource_endpoint_to_id']);
		     $oModel->setDatasourceEndpointToId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
