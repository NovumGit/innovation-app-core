<?php
namespace Crud\MessageTranslation;

/**
 * Skeleton subclass for representing a MessageTranslation.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:06:03
 */
final class CrudMessageTranslationManager extends Base\CrudMessageTranslationManager
{
}
