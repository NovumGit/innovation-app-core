<?php
namespace Crud\MessageTranslation\Field;

use Crud\MessageTranslation\Field\Base\DatasourceEndpointToId as BaseDatasourceEndpointToId;

/**
 * Skeleton subclass for representing datasource_endpoint_to_id field from the message_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:06:03
 */
final class DatasourceEndpointToId extends BaseDatasourceEndpointToId
{
}
