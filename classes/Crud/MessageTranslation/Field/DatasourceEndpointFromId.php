<?php
namespace Crud\MessageTranslation\Field;

use Crud\MessageTranslation\Field\Base\DatasourceEndpointFromId as BaseDatasourceEndpointFromId;

/**
 * Skeleton subclass for representing datasource_endpoint_from_id field from the message_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:06:03
 */
final class DatasourceEndpointFromId extends BaseDatasourceEndpointFromId
{
}
