<?php 
namespace Crud\MessageTranslation\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumOverheid\MessageTranslation;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof MessageTranslation)
		{
		     return "//services/message_translation/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof MessageTranslation)
		{
		     return "//message_translation?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
