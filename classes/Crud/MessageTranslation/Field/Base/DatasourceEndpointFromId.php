<?php
namespace Crud\MessageTranslation\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Crud\MessageTranslation\ICollectionField;
use Model\System\DataSourceQuery;

/**
 * Base class that represents the 'datasource_endpoint_from_id' crud field from the 'message_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class DatasourceEndpointFromId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'datasource_endpoint_from_id';
	protected $sFieldLabel = 'Bron';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDatasourceEndpointFromId';
	protected $sFqModelClassname = '\\\Model\Custom\NovumOverheid\MessageTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\DataSourceQuery::create()->orderByitem_url()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getitem_url", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\DataSourceQuery::create()->findOneById($iItemId)->getitem_url();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['datasource_endpoint_from_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Bron" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
