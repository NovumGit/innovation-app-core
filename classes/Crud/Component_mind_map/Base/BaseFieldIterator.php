<?php
namespace Crud\Component_mind_map\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_mind_map\ICollectionField as Component_mind_mapField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_mind_map\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_mind_mapField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_mind_mapField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_mind_mapField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_mind_mapField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_mind_mapField
	{
		return current($this->aFields);
	}
}
