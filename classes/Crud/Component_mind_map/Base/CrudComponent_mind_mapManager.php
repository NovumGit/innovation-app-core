<?php
namespace Crud\Component_mind_map\Base;

use Core\Utils;
use Crud;
use Crud\Component_mind_map\FieldIterator;
use Crud\Component_mind_map\Field\LabelValue;
use Crud\Component_mind_map\Field\QueryObject;
use Crud\Component_mind_map\Field\RootLabel;
use Crud\Component_mind_map\Field\Title;
use Crud\Component_mind_map\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\MindMap\Component_mind_map;
use Model\System\LowCode\MindMap\Component_mind_mapQuery;
use Model\System\LowCode\MindMap\Map\Component_mind_mapTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_mind_map instead if you need to override or add functionality.
 */
abstract class CrudComponent_mind_mapManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_mind_mapQuery::create();
	}


	public function getTableMap(): Component_mind_mapTableMap
	{
		return new Component_mind_mapTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Mind map component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_mind_map";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_mind_map toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_mind_map aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'RootLabel', 'QueryObject', 'LabelValue', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'RootLabel', 'QueryObject', 'LabelValue', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_mind_map
	 */
	public function getModel(array $aData = null): Component_mind_map
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_mind_mapQuery = Component_mind_mapQuery::create();
		     $oComponent_mind_map = $oComponent_mind_mapQuery->findOneById($aData['id']);
		     if (!$oComponent_mind_map instanceof Component_mind_map) {
		         throw new LogicException("Component_mind_map should be an instance of Component_mind_map but got something else." . __METHOD__);
		     }
		     $oComponent_mind_map = $this->fillVo($aData, $oComponent_mind_map);
		}
		else {
		     $oComponent_mind_map = new Component_mind_map();
		     if (!empty($aData)) {
		         $oComponent_mind_map = $this->fillVo($aData, $oComponent_mind_map);
		     }
		}
		return $oComponent_mind_map;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_mind_map
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_mind_map
	{
		$oComponent_mind_map = $this->getModel($aData);


		 if(!empty($oComponent_mind_map))
		 {
		     $oComponent_mind_map = $this->fillVo($aData, $oComponent_mind_map);
		     $oComponent_mind_map->save();
		 }
		return $oComponent_mind_map;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_mind_map $oModel
	 * @return Component_mind_map
	 */
	protected function fillVo(array $aData, Component_mind_map $oModel): Component_mind_map
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['root_label'])) {
		     $oField = new RootLabel();
		     $mValue = $oField->sanitize($aData['root_label']);
		     $oModel->setRootLabel($mValue);
		}
		if(isset($aData['query_object'])) {
		     $oField = new QueryObject();
		     $mValue = $oField->sanitize($aData['query_object']);
		     $oModel->setQueryObject($mValue);
		}
		if(isset($aData['label_value'])) {
		     $oField = new LabelValue();
		     $mValue = $oField->sanitize($aData['label_value']);
		     $oModel->setLabelValue($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
