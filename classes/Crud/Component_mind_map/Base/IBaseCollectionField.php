<?php
namespace Crud\Component_mind_map\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_mind_map\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
