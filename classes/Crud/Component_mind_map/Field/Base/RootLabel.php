<?php
namespace Crud\Component_mind_map\Field\Base;

use Crud\Component_mind_map\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'root_label' crud field from the 'component_mind_map' table.
 * This class is auto generated and should not be modified.
 */
abstract class RootLabel extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'root_label';
	protected $sFieldLabel = 'Root element label';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRootLabel';
	protected $sFqModelClassname = '\Model\System\LowCode\MindMap\Component_mind_map';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
