<?php 
namespace Crud\Component_mind_map\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\MindMap\Component_mind_map;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_mind_map)
		{
		     return "//system/component_mind_map/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_mind_map)
		{
		     return "//component_mind_map?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
