<?php
namespace Crud\Component_mind_map\Field;

use Crud\Component_mind_map\Field\Base\LabelValue as BaseLabelValue;

/**
 * Skeleton subclass for representing label_value field from the component_mind_map table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class LabelValue extends BaseLabelValue
{
}
