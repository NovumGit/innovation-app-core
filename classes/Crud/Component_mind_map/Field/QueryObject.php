<?php
namespace Crud\Component_mind_map\Field;

use Crud\Component_mind_map\Field\Base\QueryObject as BaseQueryObject;

/**
 * Skeleton subclass for representing query_object field from the component_mind_map table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class QueryObject extends BaseQueryObject
{
}
