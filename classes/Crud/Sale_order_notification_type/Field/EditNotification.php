<?php
namespace Crud\Sale_order_notification_type\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Setting\MasterTable\Sale_order_notification_type as ModelObject;

class EditNotification extends Field implements IEventField{

    protected $sFieldName = null;
    protected $sFieldLabel = 'Edit notification';

    function getIcon()
    {
        return 'magic';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }
    function getOverviewValue($mData)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new LogicException("Expected an instance of ModelObject, got ".get_class($mData));
        }
        $oSale_order_notification_type = $mData;

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Vertalingen invoeren" href="/generic/template/edit?template='.$oSale_order_notification_type->getCode().'" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }
    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
