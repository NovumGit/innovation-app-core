<?php
namespace Crud\Sale_order_notification_type\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\Base\Sale_order_notification_type as ModelObject;

class Code extends Field{

    protected $sFieldName = 'code';
    protected $sFieldLabel = 'Code';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'Code';

    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oSaleOrderStatus)
    {
        if(!$oSaleOrderStatus instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of Sale_order_status_email_type got ".get_class($oSaleOrderStatus)."  in ".__METHOD__);
        }
        return '<td class="">'.$oSaleOrderStatus->getCode().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of Sale_order_status_email_type got ".get_class($mData)." in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getCode(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}