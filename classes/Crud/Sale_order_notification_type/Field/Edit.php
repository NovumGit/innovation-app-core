<?php
namespace Crud\Sale_order_notification_type\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\Sale_order_notification_type as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }
        return '/setting/sale_order/status_email/edit?id='.$oModelObject->getId();
    }
}

