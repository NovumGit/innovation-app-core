<?php
namespace Crud\Sale_order_notification_type\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Sale_order_notification_type\ICollectionField as Sale_order_notification_typeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Sale_order_notification_type\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Sale_order_notification_typeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Sale_order_notification_typeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Sale_order_notification_typeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Sale_order_notification_typeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Sale_order_notification_typeField
	{
		return current($this->aFields);
	}
}
