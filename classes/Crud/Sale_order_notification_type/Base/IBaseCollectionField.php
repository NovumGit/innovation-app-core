<?php
namespace Crud\Sale_order_notification_type\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Sale_order_notification_type\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
