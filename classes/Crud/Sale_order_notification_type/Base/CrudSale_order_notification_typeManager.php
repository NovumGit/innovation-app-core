<?php
namespace Crud\Sale_order_notification_type\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Sale_order_notification_type\FieldIterator;
use Crud\Sale_order_notification_type\Field\Code;
use Crud\Sale_order_notification_type\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\Sale_order_notification_typeTableMap;
use Model\Setting\MasterTable\Sale_order_notification_type;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Sale_order_notification_type instead if you need to override or add functionality.
 */
abstract class CrudSale_order_notification_typeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Sale_order_notification_typeQuery::create();
	}


	public function getTableMap(): Sale_order_notification_typeTableMap
	{
		return new Sale_order_notification_typeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Sale_order_notification_type";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_sale_order_notification_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_sale_order_notification_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Code', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Code', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Sale_order_notification_type
	 */
	public function getModel(array $aData = null): Sale_order_notification_type
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSale_order_notification_typeQuery = Sale_order_notification_typeQuery::create();
		     $oSale_order_notification_type = $oSale_order_notification_typeQuery->findOneById($aData['id']);
		     if (!$oSale_order_notification_type instanceof Sale_order_notification_type) {
		         throw new LogicException("Sale_order_notification_type should be an instance of Sale_order_notification_type but got something else." . __METHOD__);
		     }
		     $oSale_order_notification_type = $this->fillVo($aData, $oSale_order_notification_type);
		}
		else {
		     $oSale_order_notification_type = new Sale_order_notification_type();
		     if (!empty($aData)) {
		         $oSale_order_notification_type = $this->fillVo($aData, $oSale_order_notification_type);
		     }
		}
		return $oSale_order_notification_type;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Sale_order_notification_type
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Sale_order_notification_type
	{
		$oSale_order_notification_type = $this->getModel($aData);


		 if(!empty($oSale_order_notification_type))
		 {
		     $oSale_order_notification_type = $this->fillVo($aData, $oSale_order_notification_type);
		     $oSale_order_notification_type->save();
		 }
		return $oSale_order_notification_type;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Sale_order_notification_type $oModel
	 * @return Sale_order_notification_type
	 */
	protected function fillVo(array $aData, Sale_order_notification_type $oModel): Sale_order_notification_type
	{
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
