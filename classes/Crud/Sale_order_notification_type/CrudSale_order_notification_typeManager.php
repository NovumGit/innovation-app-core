<?php
namespace Crud\Sale_order_notification_type;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use LogicException;
use Model\Setting\MasterTable\Sale_order_notification_type;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;

class CrudSale_order_notification_typeManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Verkoop order notificatie';
    }
    function getNewFormTitle():string{
        return 'Verkoop order notificatie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Verkoop order notificatie bewerken';
    }
    function getOverviewUrl():string
    {
        return '/setting/sale_order/status_email/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/sale_order/status_email/edit';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
        ];
    }
    /**
     * @param $aData
     * @return SaleOrderStatus
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = Sale_order_notification_typeQuery::create();
            $oSaleOrderStatus = $oQuery->findOneById($aData['id']);
            if(!$oSaleOrderStatus instanceof Sale_order_notification_type)
            {
                throw new LogicException("Sale_order_notification_type should be an instance of Sale_order_notification_type but got ".get_class($oSaleOrderStatus)." in ".__METHOD__);
            }
        }
        else
        {
            $oSaleOrderStatus = new Sale_order_notification_type();
            $oSaleOrderStatus = $this->fillVo($oSaleOrderStatus, $aData);
        }
        return $oSaleOrderStatus;
    }
    function store(array $aData = null)
    {
        $oSale_order_notification_type = $this->getModel($aData);

        if(!empty($oSale_order_notification_type))
        {

            $oSale_order_notification_type = $this->fillVo($oSale_order_notification_type, $aData);
            $oSale_order_notification_type->save();
        }
        return $oSale_order_notification_type;
    }
    function fillVo(Sale_order_notification_type $oSale_order_notification_type, $aData):Sale_order_notification_type
    {
        if(isset($aData['name'])){
            $oSale_order_notification_type->setName($aData['name']);
        }
        if(isset($aData['code'])){
            $oSale_order_notification_type->setCode($aData['code']);
        }
        return $oSale_order_notification_type;
    }
}
