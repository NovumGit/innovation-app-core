<?php
namespace Crud\MasterPackingSlip\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MasterPackingSlip\FieldIterator;
use Crud\MasterPackingSlip\Field\Format;
use Crud\MasterPackingSlip\Field\IsDefault;
use Crud\MasterPackingSlip\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\MasterPackingSlipTableMap;
use Model\Setting\MasterTable\MasterPackingSlip;
use Model\Setting\MasterTable\MasterPackingSlipQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MasterPackingSlip instead if you need to override or add functionality.
 */
abstract class CrudMasterPackingSlipManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MasterPackingSlipQuery::create();
	}


	public function getTableMap(): MasterPackingSlipTableMap
	{
		return new MasterPackingSlipTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MasterPackingSlip";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_packing_slip toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_packing_slip aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Format', 'IsDefault'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Format', 'IsDefault'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MasterPackingSlip
	 */
	public function getModel(array $aData = null): MasterPackingSlip
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMasterPackingSlipQuery = MasterPackingSlipQuery::create();
		     $oMasterPackingSlip = $oMasterPackingSlipQuery->findOneById($aData['id']);
		     if (!$oMasterPackingSlip instanceof MasterPackingSlip) {
		         throw new LogicException("MasterPackingSlip should be an instance of MasterPackingSlip but got something else." . __METHOD__);
		     }
		     $oMasterPackingSlip = $this->fillVo($aData, $oMasterPackingSlip);
		}
		else {
		     $oMasterPackingSlip = new MasterPackingSlip();
		     if (!empty($aData)) {
		         $oMasterPackingSlip = $this->fillVo($aData, $oMasterPackingSlip);
		     }
		}
		return $oMasterPackingSlip;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MasterPackingSlip
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MasterPackingSlip
	{
		$oMasterPackingSlip = $this->getModel($aData);


		 if(!empty($oMasterPackingSlip))
		 {
		     $oMasterPackingSlip = $this->fillVo($aData, $oMasterPackingSlip);
		     $oMasterPackingSlip->save();
		 }
		return $oMasterPackingSlip;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MasterPackingSlip $oModel
	 * @return MasterPackingSlip
	 */
	protected function fillVo(array $aData, MasterPackingSlip $oModel): MasterPackingSlip
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['format'])) {
		     $oField = new Format();
		     $mValue = $oField->sanitize($aData['format']);
		     $oModel->setFormat($mValue);
		}
		if(isset($aData['is_default'])) {
		     $oField = new IsDefault();
		     $mValue = $oField->sanitize($aData['is_default']);
		     $oModel->setIsDefault($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
