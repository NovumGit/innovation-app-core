<?php
namespace Crud\MasterPackingSlip\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MasterPackingSlip\ICollectionField as MasterPackingSlipField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MasterPackingSlip\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MasterPackingSlipField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MasterPackingSlipField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MasterPackingSlipField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MasterPackingSlipField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MasterPackingSlipField
	{
		return current($this->aFields);
	}
}
