<?php
namespace Crud\FormFieldType\Field;

use Crud\FormFieldType\Field\Base\Label as BaseLabel;

/**
 * Skeleton subclass for representing label field from the form_field_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Label extends BaseLabel
{
}
