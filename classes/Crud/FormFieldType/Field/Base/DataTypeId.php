<?php
namespace Crud\FormFieldType\Field\Base;

use Core\Utils;
use Crud\FormFieldType\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\DataModel\DataTypeQuery;

/**
 * Base class that represents the 'data_type_id' crud field from the 'form_field_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class DataTypeId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'data_type_id';
	protected $sFieldLabel = 'Primitive data type ';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDataTypeId';
	protected $sFqModelClassname = '\\\Model\System\FormFieldType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\DataModel\DataTypeQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\DataModel\DataTypeQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
