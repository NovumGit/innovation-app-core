<?php
namespace Crud\FormFieldType\Field\Base;

use Crud\FormFieldType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_foreign_key' crud field from the 'form_field_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsForeignKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_foreign_key';
	protected $sFieldLabel = 'Is this field a foreign key';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsForeignKey';
	protected $sFqModelClassname = '\\\Model\System\FormFieldType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
