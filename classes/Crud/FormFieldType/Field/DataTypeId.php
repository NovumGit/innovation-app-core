<?php
namespace Crud\FormFieldType\Field;

use Crud\FormFieldType\Field\Base\DataTypeId as BaseDataTypeId;

/**
 * Skeleton subclass for representing data_type_id field from the form_field_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class DataTypeId extends BaseDataTypeId
{
}
