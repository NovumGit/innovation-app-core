<?php
namespace Crud\FormFieldType\Field;

use Crud\FormFieldType\Field\Base\IsForeignKey as BaseIsForeignKey;

/**
 * Skeleton subclass for representing is_foreign_key field from the form_field_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class IsForeignKey extends BaseIsForeignKey
{
}
