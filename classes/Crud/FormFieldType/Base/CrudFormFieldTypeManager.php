<?php
namespace Crud\FormFieldType\Base;

use Core\Utils;
use Crud;
use Crud\FormFieldType\FieldIterator;
use Crud\FormFieldType\Field\DataTypeId;
use Crud\FormFieldType\Field\IsForeignKey;
use Crud\FormFieldType\Field\Label;
use Crud\FormFieldType\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\FormFieldType;
use Model\System\FormFieldTypeQuery;
use Model\System\Map\FormFieldTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify FormFieldType instead if you need to override or add functionality.
 */
abstract class CrudFormFieldTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FormFieldTypeQuery::create();
	}


	public function getTableMap(): FormFieldTypeTableMap
	{
		return new FormFieldTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint zijn formulier component typen vastgelegd .";
	}


	public function getEntityTitle(): string
	{
		return "FormFieldType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "form_field_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "form_field_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Label', 'DataTypeId', 'IsForeignKey'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Label', 'DataTypeId', 'IsForeignKey'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return FormFieldType
	 */
	public function getModel(array $aData = null): FormFieldType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFormFieldTypeQuery = FormFieldTypeQuery::create();
		     $oFormFieldType = $oFormFieldTypeQuery->findOneById($aData['id']);
		     if (!$oFormFieldType instanceof FormFieldType) {
		         throw new LogicException("FormFieldType should be an instance of FormFieldType but got something else." . __METHOD__);
		     }
		     $oFormFieldType = $this->fillVo($aData, $oFormFieldType);
		}
		else {
		     $oFormFieldType = new FormFieldType();
		     if (!empty($aData)) {
		         $oFormFieldType = $this->fillVo($aData, $oFormFieldType);
		     }
		}
		return $oFormFieldType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return FormFieldType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): FormFieldType
	{
		$oFormFieldType = $this->getModel($aData);


		 if(!empty($oFormFieldType))
		 {
		     $oFormFieldType = $this->fillVo($aData, $oFormFieldType);
		     $oFormFieldType->save();
		 }
		return $oFormFieldType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param FormFieldType $oModel
	 * @return FormFieldType
	 */
	protected function fillVo(array $aData, FormFieldType $oModel): FormFieldType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		if(isset($aData['data_type_id'])) {
		     $oField = new DataTypeId();
		     $mValue = $oField->sanitize($aData['data_type_id']);
		     $oModel->setDataTypeId($mValue);
		}
		if(isset($aData['is_foreign_key'])) {
		     $oField = new IsForeignKey();
		     $mValue = $oField->sanitize($aData['is_foreign_key']);
		     $oModel->setIsForeignKey($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
