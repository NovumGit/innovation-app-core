<?php
namespace Crud\FormFieldType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\FormFieldType\ICollectionField as FormFieldTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\FormFieldType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FormFieldTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FormFieldTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FormFieldTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FormFieldTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FormFieldTypeField
	{
		return current($this->aFields);
	}
}
