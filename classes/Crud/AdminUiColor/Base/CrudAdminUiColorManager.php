<?php
namespace Crud\AdminUiColor\Base;

use Core\Utils;
use Crud;
use Crud\AdminUiColor\FieldIterator;
use Crud\AdminUiColor\Field\AppIcon;
use Crud\AdminUiColor\Field\BackgroundColor;
use Crud\AdminUiColor\Field\ButtonColor;
use Crud\AdminUiColor\Field\LoginLogo;
use Crud\AdminUiColor\Field\MainColor;
use Crud\AdminUiColor\Field\SystemLogo;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Settings\UI\AdminUiColor;
use Model\Settings\UI\AdminUiColorQuery;
use Model\Settings\UI\Map\AdminUiColorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminUiColor instead if you need to override or add functionality.
 */
abstract class CrudAdminUiColorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return AdminUiColorQuery::create();
	}


	public function getTableMap(): AdminUiColorTableMap
	{
		return new AdminUiColorTableMap();
	}


	public function getShortDescription(): string
	{
		return "-";
	}


	public function getEntityTitle(): string
	{
		return "AdminUiColor";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "admin_ui toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "admin_ui aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MainColor', 'ButtonColor', 'BackgroundColor', 'LoginLogo', 'SystemLogo', 'AppIcon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MainColor', 'ButtonColor', 'BackgroundColor', 'LoginLogo', 'SystemLogo', 'AppIcon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return AdminUiColor
	 */
	public function getModel(array $aData = null): AdminUiColor
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oAdminUiColorQuery = AdminUiColorQuery::create();
		     $oAdminUiColor = $oAdminUiColorQuery->findOneById($aData['id']);
		     if (!$oAdminUiColor instanceof AdminUiColor) {
		         throw new LogicException("AdminUiColor should be an instance of AdminUiColor but got something else." . __METHOD__);
		     }
		     $oAdminUiColor = $this->fillVo($aData, $oAdminUiColor);
		}
		else {
		     $oAdminUiColor = new AdminUiColor();
		     if (!empty($aData)) {
		         $oAdminUiColor = $this->fillVo($aData, $oAdminUiColor);
		     }
		}
		return $oAdminUiColor;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return AdminUiColor
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): AdminUiColor
	{
		$oAdminUiColor = $this->getModel($aData);


		 if(!empty($oAdminUiColor))
		 {
		     $oAdminUiColor = $this->fillVo($aData, $oAdminUiColor);
		     $oAdminUiColor->save();
		 }
		return $oAdminUiColor;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param AdminUiColor $oModel
	 * @return AdminUiColor
	 */
	protected function fillVo(array $aData, AdminUiColor $oModel): AdminUiColor
	{
		if(isset($aData['main_color'])) {
		     $oField = new MainColor();
		     $mValue = $oField->sanitize($aData['main_color']);
		     $oModel->setMainColor($mValue);
		}
		if(isset($aData['button_color'])) {
		     $oField = new ButtonColor();
		     $mValue = $oField->sanitize($aData['button_color']);
		     $oModel->setButtonColor($mValue);
		}
		if(isset($aData['background_color'])) {
		     $oField = new BackgroundColor();
		     $mValue = $oField->sanitize($aData['background_color']);
		     $oModel->setBackgroundColor($mValue);
		}
		if(isset($aData['login_logo'])) {
		     $oField = new LoginLogo();
		     $mValue = $oField->sanitize($aData['login_logo']);
		     $oModel->setLoginLogo($mValue);
		}
		if(isset($aData['system_logo'])) {
		     $oField = new SystemLogo();
		     $mValue = $oField->sanitize($aData['system_logo']);
		     $oModel->setSystemLogo($mValue);
		}
		if(isset($aData['app_icon'])) {
		     $oField = new AppIcon();
		     $mValue = $oField->sanitize($aData['app_icon']);
		     $oModel->setAppIcon($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
