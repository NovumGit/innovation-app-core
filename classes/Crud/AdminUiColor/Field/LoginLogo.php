<?php
namespace Crud\AdminUiColor\Field;

use Crud\AdminUiColor\Field\Base\LoginLogo as BaseLoginLogo;

/**
 * Skeleton subclass for representing login_logo field from the admin_ui table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class LoginLogo extends BaseLoginLogo
{
}
