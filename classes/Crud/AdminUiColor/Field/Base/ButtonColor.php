<?php
namespace Crud\AdminUiColor\Field\Base;

use Crud\AdminUiColor\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'button_color' crud field from the 'admin_ui' table.
 * This class is auto generated and should not be modified.
 */
abstract class ButtonColor extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'button_color';
	protected $sFieldLabel = 'Knop kleur';
	protected $sIcon = 'list-alt';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getButtonColor';
	protected $sFqModelClassname = '\\\Model\Settings\UI\AdminUiColor';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['button_color']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Knop kleur" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
