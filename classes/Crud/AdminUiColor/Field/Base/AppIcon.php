<?php
namespace Crud\AdminUiColor\Field\Base;

use Crud\AdminUiColor\ICollectionField;
use Crud\Generic\Field\GenericFile;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'app_icon' crud field from the 'admin_ui' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppIcon extends GenericFile implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'app_icon';
	protected $sFieldLabel = 'App icon';
	protected $sIcon = 'picture-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppIcon';
	protected $sFqModelClassname = '\\\Model\Settings\UI\AdminUiColor';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['app_icon']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "App icon" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
