<?php
namespace Crud\AdminUiColor\Field\Base;

use Crud\AdminUiColor\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'background_color' crud field from the 'admin_ui' table.
 * This class is auto generated and should not be modified.
 */
abstract class BackgroundColor extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'background_color';
	protected $sFieldLabel = 'Achtergrond kleur';
	protected $sIcon = 'list-alt';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getBackgroundColor';
	protected $sFqModelClassname = '\\\Model\Settings\UI\AdminUiColor';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['background_color']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Achtergrond kleur" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
