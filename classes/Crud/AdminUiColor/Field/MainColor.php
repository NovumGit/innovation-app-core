<?php
namespace Crud\AdminUiColor\Field;

use Crud\AdminUiColor\Field\Base\MainColor as BaseMainColor;

/**
 * Skeleton subclass for representing main_color field from the admin_ui table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class MainColor extends BaseMainColor
{
}
