<?php
namespace Crud;

use AdminModules\Generic\Crud\Field\Item\IItemConfigContract;
use Hurah\Types\Type\Url;
use Model\Setting\CrudManager\CrudEditorBlockField;

/**
 * Is the field configurable when shown in a single entity context
 * Interface IConfigurableFieldItem
 * @package Crud
 */
interface IConfigurableFieldItem {


    /**
     * @param int $iCrudEditorBlockFieldId
     * @return Url
     */
    function getItemConfigUrl(int $iCrudEditorBlockFieldId): Url;
    function isConfigurableItem(): bool;
    function getItemConfig() : ?IItemConfigContract;
}

