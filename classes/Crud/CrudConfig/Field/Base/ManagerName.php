<?php
namespace Crud\CrudConfig\Field\Base;

use Crud\CrudConfig\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'manager_name' crud field from the 'crud_config' table.
 * This class is auto generated and should not be modified.
 */
abstract class ManagerName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'manager_name';
	protected $sFieldLabel = 'Manager naam';
	protected $sIcon = 'columns';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getManagerName';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudConfig';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
