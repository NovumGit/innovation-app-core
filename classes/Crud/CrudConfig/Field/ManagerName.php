<?php
namespace Crud\CrudConfig\Field;

use Crud\CrudConfig\Field\Base\ManagerName as BaseManagerName;

/**
 * Skeleton subclass for representing manager_name field from the crud_config table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ManagerName extends BaseManagerName
{
}
