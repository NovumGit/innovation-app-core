<?php
namespace Crud\CrudConfig\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudConfig\ICollectionField as CrudConfigField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudConfig\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudConfigField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudConfigField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudConfigField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudConfigField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudConfigField
	{
		return current($this->aFields);
	}
}
