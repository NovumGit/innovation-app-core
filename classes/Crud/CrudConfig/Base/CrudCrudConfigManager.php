<?php
namespace Crud\CrudConfig\Base;

use Core\Utils;
use Crud;
use Crud\CrudConfig\FieldIterator;
use Crud\CrudConfig\Field\ManagerName;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\Map\CrudConfigTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudConfig instead if you need to override or add functionality.
 */
abstract class CrudCrudConfigManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudConfigQuery::create();
	}


	public function getTableMap(): CrudConfigTableMap
	{
		return new CrudConfigTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint is opgeslagen welk managers (in code) verantwoordelijk zijn voor het beheren van data";
	}


	public function getEntityTitle(): string
	{
		return "CrudConfig";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_config toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_config aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ManagerName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ManagerName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudConfig
	 */
	public function getModel(array $aData = null): CrudConfig
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudConfigQuery = CrudConfigQuery::create();
		     $oCrudConfig = $oCrudConfigQuery->findOneById($aData['id']);
		     if (!$oCrudConfig instanceof CrudConfig) {
		         throw new LogicException("CrudConfig should be an instance of CrudConfig but got something else." . __METHOD__);
		     }
		     $oCrudConfig = $this->fillVo($aData, $oCrudConfig);
		}
		else {
		     $oCrudConfig = new CrudConfig();
		     if (!empty($aData)) {
		         $oCrudConfig = $this->fillVo($aData, $oCrudConfig);
		     }
		}
		return $oCrudConfig;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudConfig
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudConfig
	{
		$oCrudConfig = $this->getModel($aData);


		 if(!empty($oCrudConfig))
		 {
		     $oCrudConfig = $this->fillVo($aData, $oCrudConfig);
		     $oCrudConfig->save();
		 }
		return $oCrudConfig;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudConfig $oModel
	 * @return CrudConfig
	 */
	protected function fillVo(array $aData, CrudConfig $oModel): CrudConfig
	{
		if(isset($aData['manager_name'])) {
		     $oField = new ManagerName();
		     $mValue = $oField->sanitize($aData['manager_name']);
		     $oModel->setManagerName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
