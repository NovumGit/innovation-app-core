<?php
namespace Crud\CrudConfig\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\CrudConfig\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
