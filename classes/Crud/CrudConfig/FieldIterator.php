<?php
namespace Crud\CrudConfig;

use Crud\CrudConfig\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of CrudConfig crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
