<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Finance\BankIbanAddressBookQuery;
use Model\Finance\BankTransaction as ModelObject;
use Model\Finance\Base\BankIbanQuery;

class CounterBankIbanId extends Field implements IFilterableLookupField{

	protected $sFieldName = 'counter_bank_iban_id';
	protected $sFieldLabel = 'Tegen rekening';
	private $sIcon = 'building';
	private $sGetter = 'getCounterBankIbanId';

	function getLookups($mSelectedItem = null)
	{
		$oBankIbanQuery = BankIbanQuery::create();
		$oBankIbanQuery->orderByDescription();

		$aBankAccounts = $oBankIbanQuery->find();
		$aOptions = Utils::makeSelectOptions($aBankAccounts, 'getDescription', $mSelectedItem);
		return $aOptions;
	}
	function getVisibleValue($iItemId)
	{
		return null;
	}

	function getFieldName()
	{
		return $this->sFieldName;
	}
	function getDataType():string
	{
		return 'lookup';
	}

	function hasValidations() { return false; }
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}
	function getOverviewHeader()
	{
		return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
	}
	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		$aOut = [];
		$aOut[] = '<td class="">';

		if($oModelObject->getCounterBankIbanId()) {
			$BankIbanAddressBook = BankIbanAddressBookQuery::create()->findOneById($oModelObject->getCounterBankIbanId());
			$sTitle = Translate::fromCode('Verrijk IBAN met meer info');
			$sUrl = '/finance/bank/iban/alias?id=' . $oModelObject->getCounterBankIbanId();
			$aOut[] = ' <a style="color:black" title="' . $sTitle . '" href="' . $sUrl . '">';
			if (!empty($BankIbanAddressBook->getDescription())) {
				$aOut[] = ' ' . $BankIbanAddressBook->getDescription();
			}
			else {
				$aOut[] = ' ' . $BankIbanAddressBook->getIban();
			}
			$aOut[] = ' </a>';
		}
		$aOut[] = '</td>';
		return  join(PHP_EOL, $aOut);
	}

	function getFieldTitle(){
		return $this->getTranslatedTitle();
	}
	function getEditHtml($oModelObject, $bReadonly)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		$aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $this->sIcon, 'Maak een keuze');
	}
}
