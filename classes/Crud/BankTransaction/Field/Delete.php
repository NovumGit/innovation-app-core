<?php
namespace Crud\BankTransaction\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Crm\Customer;
use InvalidArgumentException;
use Model\Finance\BankIbanAddressBook;
use Model\Finance\BankTransaction;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof BankTransaction)
        {
            throw new InvalidArgumentException('Expected an instance of BankTransaction but got '.get_class($oObject));
        }
        $sTab = '';
        if(isset($_GET['tab']))
        {
            $sTab = '&tab='.$_GET['tab'];
        }
	    return '/finance/bank/transaction/edit?id=' . $oObject->getId() . $sTab;
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}