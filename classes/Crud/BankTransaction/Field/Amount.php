<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\BankTransaction as ModelObject;

class Amount extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'amount';
    protected $sFieldLabel = 'Bedrag';
    private $sIcon = 'money';
    private $sPlaceHolder = 'Geef het bedrag op';
    private $sGetter = 'getAmount';

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

	    $sColor = 'green';
        if($oModelObject->getAmount() < 0)
        {
        	$sColor = 'red';
        }

        return '<td style="color:' . $sColor .';text-align: right"> &euro;'
	                . number_format($oModelObject->getAmount(), 2, ',', '.') .
	            '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
