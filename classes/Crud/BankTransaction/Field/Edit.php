<?php
namespace Crud\BankTransaction\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Finance\BankTransaction;

class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof BankTransaction)
        {
            throw new InvalidArgumentException('Expected an instance of BankTransaction but got ' . get_class($oObject));
        }

        return '/finance/bank/transaction/edit?id=' . $oObject->getId();
    }
}