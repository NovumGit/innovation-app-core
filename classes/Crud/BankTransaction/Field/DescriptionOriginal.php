<?php
namespace Crud\BankTransaction\Field;

use Crud\BankTransaction\Field\Base\DescriptionOriginal as BaseDescriptionOriginal;

/**
 * Skeleton subclass for representing description_original field from the bank_transaction table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class DescriptionOriginal extends BaseDescriptionOriginal
{
}
