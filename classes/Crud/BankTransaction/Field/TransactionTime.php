<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Core\Config;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\BankTransaction as ModelObject;


class TransactionTime extends Field implements IFilterableField, IEditableField{

	protected $sFieldName = 'transaction_time';
	protected $sFieldLabel = 'Datum / tijd';
	private $sIcon = 'clock';
	private $sPlaceHolder = 'Geef de datum + het tijdstip op';
	private $sGetter = 'getTransactionTime';

	function getFieldName()
	{
		return $this->sFieldName;
	}
	function getDataType():string
	{
		return 'string';
	}

	function hasValidations() { return false; }
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}
	function getOverviewHeader()
	{
		return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
	}
	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}

		$sTime = $oModelObject->getTransactionTime(Config::getDateTimeFormat());
		return '<td class="">' . $sTime . '</td>';
	}
	function getFieldTitle(){
		return $this->getTranslatedTitle();
	}
	function getEditHtml($oModelObject, $bReadonly)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}

		return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->getTransactionTime(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
	}
}
