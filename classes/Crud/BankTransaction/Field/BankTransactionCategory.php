<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Finance\BankTransaction as ModelObject;
use Model\Finance\BankTransactionCategoryQuery;
use Model\Finance\BankTransactionQuery;
use Model\Finance\BankTransactionCategory as BankTransactionCategoryModel;

class BankTransactionCategory extends Field implements IFilterableLookupField{

    protected $sFieldName = 'bank_transaction_category_id';
    protected $sFieldLabel = 'Categorie';
    private $sGetter = 'getBankTransactionCategoryId';

    private function getRecursive($sAddColumns = '', &$aOut = [], $iParentId = null):array
    {
        $oBankTransactionCategoryQuery  = BankTransactionCategoryQuery::create();
        $oBankTransactionCategoryQuery->filterByParentId($iParentId);
        $oBankTransactionCategoryQuery->orderByDescription();
        $aBankTransactions = $oBankTransactionCategoryQuery->find();

        if($aBankTransactions->isEmpty())
        {
            return $aOut;
        }

        foreach($aBankTransactions as $oBankTransaction)
        {
            if(empty($sAddColumns))
            {
                $sLabel = $oBankTransaction->getDescription();
            }
            else
            {
                $sLabel = $sAddColumns . ' -> ' . $oBankTransaction->getDescription();
            }

            $aOut[] = ['id' => $oBankTransaction->getId(), 'selected' => false, 'label' => $sLabel ];
            $this->getRecursive($sLabel, $aOut, $oBankTransaction->getId());

        }

        return $aOut;
    }
    function getLookups($mSelectedItem = null)
    {
        $aLookups  = $this->getRecursive();
        return $aLookups;
    }
    function getVisibleValue($iItemId)
    {
        $oBankTransactionQuery = BankTransactionQuery::create();
        $oBankTransaction = $oBankTransactionQuery->findOneById($iItemId);
        $iCategoryId = $oBankTransaction->getBankTransactionCategoryId();

        $oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
        $oBankTransactionCategory = $oBankTransactionCategoryQuery->findOneById($iCategoryId);

        if(!$oBankTransactionCategory instanceof BankTransactionCategoryModel)
        {
            return '-';
        }
        return $oBankTransactionCategory->getDescription();
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->nonSortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $this->getVisibleValue($oModelObject->getId());

        $aOut = [];
        $aOut[] = '<td class="">';
        $aOut[] = $sValue;
        $aOut[] = '</td>';
        return join(PHP_EOL,$aOut);
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly);
    }
}
