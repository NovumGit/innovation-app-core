<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Finance\BankAccount;
use Model\Finance\BankTransaction as ModelObject;
use Model\Finance\Base\BankAccountQuery;

class BankAccountId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'bank_account_id';
    protected $sFieldLabel = 'Eigen rekening';
    private $sIcon = 'building';
    private $sGetter = 'getBankAccountId';

    function getLookups($mSelectedItem = null)
    {
    	$oBankAccountQuery = BankAccountQuery::create();
	    $oBankAccountQuery->orderByName();

        $aBankAccounts = $oBankAccountQuery->find();
        $aOptions = Utils::makeSelectOptions($aBankAccounts, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
	    $oBankAccount = BankAccountQuery::create()->findOneById($iItemId);
	    if(!$oBankAccount instanceof BankAccount)
	    {
	    	return '-';
	    }
    	return $oBankAccount->getName();
    }

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
    	if(isset($_GET['bank_account_id']))
	    {
	    	// We are looking at this account, no need to show that for every transaction
	    	return null;
	    }
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $this->getVisibleValue($oModelObject->getBankAccountId());

        $aOut = [];
        $sUrl = '/finance/bank/transactions?bank_account_id=' . $oModelObject->getBankAccountId();

	    $aOut[] = '<td class="">';
	    $aOut[] = ' <a title="' . Translate::fromCode("Bekijk details") . '" href="' . $sUrl . '">';
	    $aOut[] = '   ' . $sValue;
	    $aOut[] = ' </a>';
	    $aOut[] = '</td>';
        return join(PHP_EOL,$aOut);
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $this->sIcon, 'Maak een keuze');
    }
}
