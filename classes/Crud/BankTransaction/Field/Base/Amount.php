<?php
namespace Crud\BankTransaction\Field\Base;

use Crud\BankTransaction\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'amount' crud field from the 'bank_transaction' table.
 * This class is auto generated and should not be modified.
 */
abstract class Amount extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'amount';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAmount';
	protected $sFqModelClassname = '\\\Model\Finance\BankTransaction';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
