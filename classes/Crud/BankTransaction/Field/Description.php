<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankTransaction\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\BankTransaction as ModelObject;


class Description extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'description';
    protected $sFieldLabel = 'Omschrijving';
    private $sIcon = 'pencil';
    private $sPlaceHolder = 'Geef de omschrijving op';
    private $sGetter = 'getDescription';



    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->nonSortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sTitle = $oModelObject->getDescription();

        $iLen = isset($_GET['bank_account_id']) ? 80 : 50;

        if(strlen($sTitle) > $iLen)
        {
        	return  '<td class=""><a style="color:black" title="' . $sTitle . '" href="#">' . substr($sTitle, 0, $iLen)  . '</a></td>';
        }
        return '<td class="">' . $sTitle . '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
