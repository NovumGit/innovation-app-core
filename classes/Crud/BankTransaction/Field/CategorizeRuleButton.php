<?php
namespace Crud\BankTransaction\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Finance\BankTransaction;
use Model\Product;

class CategorizeRuleButton extends Field implements IEventField{

	protected $sFieldLabel = 'Categoriseer';

	function getIcon()
	{
		return 'folder';
	}

	function hasValidations() { return false; }
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}
	function getFieldTitle(){
		return $this->sFieldLabel;
	}

	function getOverviewHeader()
	{
		$aOut = [];
		$aOut[] = '<th class="iconcol">';
		$aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
		$aOut[] = '         <i class="fa fa-magic"></i> +';
		$aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
		$aOut[] = '    </a>';
		$aOut[] = '</th>';
		return join(PHP_EOL, $aOut);
	}

	function getOverviewValue($mData)
	{

		if(!$mData instanceof BankTransaction)
		{
			throw new LogicException("Expected an instance of Product, got ".get_class($mData));
		}
		$oProduct = $mData;

		$aOut = [];
		$aOut[] = '<td class="xx">';
		$aOut[] = '    <a title="Categoriseren" href="/finance/bank/categorize/define_rule?transaction_id=' . $mData->getId() .'" class="btn btn-success br2 btn-xs fs12 d">';
		$aOut[] = '         <i class="fa fa-magic"></i> + ';
		$aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';

		$aOut[] = '    </button>';
		$aOut[] = '</td>';

		return join(PHP_EOL, $aOut);
	}

	function getEditHtml($mData, $bReadonly)
	{
		throw new LogicException("Add field should not be there in edit view.");
	}
}
