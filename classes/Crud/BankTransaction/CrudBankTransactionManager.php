<?php
namespace Crud\BankTransaction;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Finance\BankTransaction;
use Model\Finance\BankTransactionQuery;
use Exception\LogicException;

class CrudBankTransactionManager extends FormManager implements IConfigurableCrud {

    function getEntityTitle():string
    {
        return 'bank_transaction';
    }
    function getOverviewUrl():string
    {
        return '/finance/bank/transactions';
    }
    function getCreateNewUrl():string
    {
        return '/finance/bank/transaction/edit';
    }
    function getNewFormTitle():string{
        return 'Transactie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Transactie wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Amount',
            'BankAccountId',
	        'Description',
	        'TransactionType'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
	        'Amount',
	        'BankAccountId',
	        'Description',
	        'TransactionType'
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
        	$oBankTransactionQuery = BankTransactionQuery::create();
	        $oBankTransaction = $oBankTransactionQuery->findOneById($aData['id']);

            if(!$oBankTransaction instanceof BankTransaction)
            {
                throw new LogicException("{ should be an instance of Brand but got ".get_class($oBankTransaction)." in ".__METHOD__);
            }
        }
        else
        {
	        $oBankTransaction = new BankTransaction();
            if(!empty($aData))
            {
	            $oBankTransaction = $this->fillVo($aData, $oBankTransaction);
            }
        }
        return $oBankTransaction;
    }

    function store(array $aData = null)
    {
	    $oBankTransaction = $this->getModel($aData);

        if(!empty($oBankTransaction))
        {
	        $oBankTransaction = $this->fillVo($aData, $oBankTransaction);
	        $oBankTransaction->save();
        }
        return $oBankTransaction;
    }
    private function fillVo($aData, BankTransaction $oBankTransaction)
    {
        if(isset($aData['bank_account_id'])){$oBankTransaction->setBankAccountId($aData['bank_account_id']);}
	    if(isset($aData['description'])){$oBankTransaction->setDescription($aData['description']);}
	    if(isset($aData['counter_iban'])){$oBankTransaction->setCounterIban($aData['counter_iban']);}
	    if(isset($aData['transaction_type'])){$oBankTransaction->setTransactionType($aData['transaction_type']);}
	    if(isset($aData['amount'])){$oBankTransaction->setAmount($aData['amount']);}

        if(isset($aData['bank_transaction_category_id'])){$oBankTransaction->setBankTransactionCategoryId($aData['bank_transaction_category_id']);}

        return $oBankTransaction;
    }
}
