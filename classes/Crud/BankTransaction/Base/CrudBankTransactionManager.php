<?php
namespace Crud\BankTransaction\Base;

use Core\Utils;
use Crud;
use Crud\BankTransaction\FieldIterator;
use Crud\BankTransaction\Field\Amount;
use Crud\BankTransaction\Field\BankAccountId;
use Crud\BankTransaction\Field\BankTransactionCategoryId;
use Crud\BankTransaction\Field\CounterBankIbanId;
use Crud\BankTransaction\Field\Description;
use Crud\BankTransaction\Field\DescriptionOriginal;
use Crud\BankTransaction\Field\OriginalId;
use Crud\BankTransaction\Field\TransactionTime;
use Crud\BankTransaction\Field\TransactionType;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankTransaction;
use Model\Finance\BankTransactionQuery;
use Model\Finance\Map\BankTransactionTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankTransaction instead if you need to override or add functionality.
 */
abstract class CrudBankTransactionManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankTransactionQuery::create();
	}


	public function getTableMap(): BankTransactionTableMap
	{
		return new BankTransactionTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankTransaction";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_transaction toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_transaction aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OriginalId', 'BankAccountId', 'BankTransactionCategoryId', 'Description', 'DescriptionOriginal', 'CounterBankIbanId', 'TransactionType', 'Amount', 'TransactionTime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OriginalId', 'BankAccountId', 'BankTransactionCategoryId', 'Description', 'DescriptionOriginal', 'CounterBankIbanId', 'TransactionType', 'Amount', 'TransactionTime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankTransaction
	 */
	public function getModel(array $aData = null): BankTransaction
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankTransactionQuery = BankTransactionQuery::create();
		     $oBankTransaction = $oBankTransactionQuery->findOneById($aData['id']);
		     if (!$oBankTransaction instanceof BankTransaction) {
		         throw new LogicException("BankTransaction should be an instance of BankTransaction but got something else." . __METHOD__);
		     }
		     $oBankTransaction = $this->fillVo($aData, $oBankTransaction);
		}
		else {
		     $oBankTransaction = new BankTransaction();
		     if (!empty($aData)) {
		         $oBankTransaction = $this->fillVo($aData, $oBankTransaction);
		     }
		}
		return $oBankTransaction;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankTransaction
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankTransaction
	{
		$oBankTransaction = $this->getModel($aData);


		 if(!empty($oBankTransaction))
		 {
		     $oBankTransaction = $this->fillVo($aData, $oBankTransaction);
		     $oBankTransaction->save();
		 }
		return $oBankTransaction;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankTransaction $oModel
	 * @return BankTransaction
	 */
	protected function fillVo(array $aData, BankTransaction $oModel): BankTransaction
	{
		if(isset($aData['original_id'])) {
		     $oField = new OriginalId();
		     $mValue = $oField->sanitize($aData['original_id']);
		     $oModel->setOriginalId($mValue);
		}
		if(isset($aData['bank_account_id'])) {
		     $oField = new BankAccountId();
		     $mValue = $oField->sanitize($aData['bank_account_id']);
		     $oModel->setBankAccountId($mValue);
		}
		if(isset($aData['bank_transaction_category_id'])) {
		     $oField = new BankTransactionCategoryId();
		     $mValue = $oField->sanitize($aData['bank_transaction_category_id']);
		     $oModel->setBankTransactionCategoryId($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['description_original'])) {
		     $oField = new DescriptionOriginal();
		     $mValue = $oField->sanitize($aData['description_original']);
		     $oModel->setDescriptionOriginal($mValue);
		}
		if(isset($aData['counter_bank_iban_id'])) {
		     $oField = new CounterBankIbanId();
		     $mValue = $oField->sanitize($aData['counter_bank_iban_id']);
		     $oModel->setCounterBankIbanId($mValue);
		}
		if(isset($aData['transaction_type'])) {
		     $oField = new TransactionType();
		     $mValue = $oField->sanitize($aData['transaction_type']);
		     $oModel->setTransactionType($mValue);
		}
		if(isset($aData['amount'])) {
		     $oField = new Amount();
		     $mValue = $oField->sanitize($aData['amount']);
		     $oModel->setAmount($mValue);
		}
		if(isset($aData['transaction_time'])) {
		     $oField = new TransactionTime();
		     $mValue = $oField->sanitize($aData['transaction_time']);
		     $oModel->setTransactionTime($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
