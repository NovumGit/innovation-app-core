<?php
namespace Crud\BankTransaction\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\BankTransaction\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
