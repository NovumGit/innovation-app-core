<?php
namespace Crud\BankTransaction\Base;

use Crud\BankTransaction\ICollectionField as BankTransactionField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankTransaction\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankTransactionField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankTransactionField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankTransactionField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankTransactionField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankTransactionField
	{
		return current($this->aFields);
	}
}
