<?php
namespace Crud\Customer;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;

use Exception\LogicException;

class CrudCustomerOrderManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Klant zoeken';
    }
    function getOverviewUrl():string
    {
        return '/order/new';
    }
    function getCreateNewUrl():string
    {
        return false;
    }
    function getNewFormTitle():string{
        return 'Klant toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Klant wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Debitor',
            'CustomerCompany_CompanyName',
            'FirstName',
            'LastName',
            'Email',
            'NewOrder'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        throw new LogicException("Dit crud venster is niet gemaakt om klanten in te bewerken.");
    }
    function store(array $aData = null)
    {
        throw new LogicException("Dit crud venster is niet gemaakt om klanten in te bewerken.");
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oCustomerQuery = new CustomerQuery();
            $oCustomer = $oCustomerQuery->findOneById($aData['id']);
            if(!$oCustomer instanceof Customer){
                throw new LogicException("Customer should be an instance of Customer but got ".get_class($oCustomer)." in ".__METHOD__);
            }
        }
        else
        {
            $oCustomer = new Customer();
            if(!empty($aData))
            {
                $oCustomer = $this->fillVo($aData, $oCustomer);
            }
        }
        return $oCustomer;
    }
    private function fillVo($aData, Customer $oCustomer)
    {
        if(isset($aData['is_deleted'])){$oCustomer->setItemDeleted($aData['is_deleted']);}
        if(isset($aData['is_newsletter'])){$oCustomer->setIsNewsletter($aData['is_newsletter']);}
        if(isset($aData['is_reseller'])){$oCustomer->setIsReseller($aData['is_reseller']);}
        if(isset($aData['can_buy_on_credit'])){$oCustomer->setCanBuyOnCredit($aData['can_buy_on_credit']);}
        if(isset($aData['company_name'])){$oCustomer->setCompanyName($aData['company_name']);}
        if(isset($aData['debitor'])){$oCustomer->setDebitor($aData['debitor']);}
        if(isset($aData['old_id'])){$oCustomer->setOldId($aData['old_id']);}
        if(isset($aData['legal_form_id'])){$oCustomer->setLegalFormId($aData['legal_form_id']);}
        if(isset($aData['payterm_id'])){$oCustomer->setPaytermId($aData['payterm_id']);}
        if(isset($aData['price_level_id'])){$oCustomer->setPriceLevelId($aData['price_level_id']);}
        if(isset($aData['bic_code'])){$oCustomer->setBicCode($aData['bic_code']);}
        if(isset($aData['phone'])){$oCustomer->setPhone($aData['phone']);}
        if(isset($aData['fax'])){$oCustomer->setFax($aData['fax']);}
        if(isset($aData['website'])){$oCustomer->setWebsite($aData['website']);}
        if(isset($aData['email'])){$oCustomer->setEmail($aData['email']);}
        if(isset($aData['general_street'])){$oCustomer->setGeneralStreet($aData['general_street']);}
        if(isset($aData['general_number'])){$oCustomer->setGeneralNumber($aData['general_number']);}
        if(isset($aData['general_number_add'])){$oCustomer->setGeneralNumberAdd($aData['general_number_add']);}
        if(isset($aData['general_postal'])){$oCustomer->setGeneralPostal($aData['general_postal']);}
        if(isset($aData['general_city'])){$oCustomer->setGeneralCity($aData['general_city']);}
        if(isset($aData['has_invoice_address'])){$oCustomer->setHasInvoiceAddress($aData['has_invoice_address']);}
        if(isset($aData['invoice_company_name'])){$oCustomer->setInvoiceCompanyName($aData['invoice_company_name']);}
        if(isset($aData['invoice_street'])){$oCustomer->setInvoiceStreet($aData['invoice_street']);}
        if(isset($aData['invoice_number'])){$oCustomer->setInvoiceNumber($aData['invoice_number']);}
        if(isset($aData['invoice_number_add'])){$oCustomer->setInvoiceNumberAdd($aData['invoice_number_add']);}
        if(isset($aData['invoice_postal'])){$oCustomer->setInvoicePostal($aData['invoice_postal']);}
        if(isset($aData['invoice_city'])){$oCustomer->setInvoiceCity($aData['invoice_city']);}
        if(isset($aData['has_delivery_address'])){$oCustomer->setHasDeliveryAddress($aData['has_delivery_address']);}
        if(isset($aData['delivery_company_name'])){$oCustomer->setDeliveryCompanyName($aData['delivery_company_name']);}
        if(isset($aData['delivery_street'])){$oCustomer->setDeliveryStreet($aData['delivery_street']);}
        if(isset($aData['delivery_number'])){$oCustomer->setDeliveryNumber($aData['delivery_number']);}
        if(isset($aData['delivery_number_add'])){$oCustomer->setDeliveryNumberAdd($aData['delivery_number_add']);}
        if(isset($aData['delivery_postal'])){$oCustomer->setDeliveryPostal($aData['delivery_postal']);}
        if(isset($aData['delivery_city'])){$oCustomer->setDeliveryCity($aData['delivery_city']);}

        return $oCustomer;
    }
}
