<?php
namespace Crud\Customer;

use Crud\IConfigurableCrud;

class CrudWebshopManager extends CrudCustomerManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'klant';
    }
    function getOverviewUrl():string
    {
        return '/crm/overview';
    }
    function getCreateNewUrl():string
    {
        return '/crm/edit';
    }
    function getNewFormTitle():string{
        return 'Gegevens wijzigen';
    }
    function getEditFormTitle():string
    {
        return 'Gegevens wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'IsNewsletter',
            'ChamberOfCommerce',
            'VatNumber',
            'CompanyName',
            'LegalFormId',
            'PaytermId',
            'BicCode',
            'Phone',
            'Fax',
            'Website',
            'Email',
            'GeneralAddress',
            'GeneralPostal',
            'GeneralCity',
            'HasInvoiceAddress',
            'InvoiceCompanyName',
            'InvoiceStreet',
            'InvoiceNumber',
            'InvoiceNumberAdd',
            'InvoicePostal',
            'InvoiceCity',
            'HasDeliveryAddress',
            'DeliveryCompanyName',
            'DeliveryStreet',
            'DeliveryNumber',
            'DeliveryNumberAdd',
            'DeliveryPostal',
            'DeliveryCity',
        ];
    }
}
