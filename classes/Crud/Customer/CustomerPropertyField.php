<?php
namespace Crud\Customer;

use Core\Translate;
use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Crm\Customer as ModelObject;
use Model\Crm\Customer;
use Model\Crm\CustomerProperty;
use Model\Crm\CustomerPropertyQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class CustomerPropertyField extends Field implements ICustomCrudField
{
    protected $sFieldName = null;
    protected $sFieldLabel = null;

    function store(ActiveRecordInterface $oObject, array $aData)
    {
        if(!$oObject instanceof Customer)
        {
            throw new LogicException("Expected an instance of Customer.");
        }
        $oCustomerPropertyQuery = CustomerPropertyQuery::create();
        $oCustomerPropertyQuery->filterByCustomerId($oObject->getId());
        $oCustomerPropertyQuery->filterByPropertyKey($this->sFieldName);
        $oCustomerProperty = $oCustomerPropertyQuery->findOne();

        if(!$oCustomerProperty instanceof CustomerProperty)
        {
            $oCustomerProperty = new CustomerProperty();
            $oCustomerProperty->setCustomerId($oObject->getId());
            $oCustomerProperty->setPropertyKey($this->sFieldName);
        }
        if(isset($aData[$this->sFieldName]) && $oCustomerProperty->getPropertyValue() != $aData[$this->sFieldName])
        {
            $oCustomerProperty->setPropertyValue($aData[$this->sFieldName]);
            $oCustomerProperty->save();
        }
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCustomerPropertyQuery = CustomerPropertyQuery::create();
        $oCustomerPropertyQuery->filterByCustomerId($oModelObject->getId());
        $oCustomerPropertyQuery->filterByPropertyKey($this->sFieldName);
        $oCustomerProperty = $oCustomerPropertyQuery->findOne();
        $sBool = '0';
        if($oCustomerProperty instanceof CustomerProperty)
        {
            $sBool = $oCustomerProperty->getPropertyValue();
        }
        $sValue = ($sBool == '1') ? Translate::fromCode('Ja') : Translate::fromCode('Nee');

        return '<td class="">'.$sValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oProductPropertyQuery = CustomerPropertyQuery::create();
        $oProductPropertyQuery->filterByCustomerId($oModelObject->getId());
        $oProductPropertyQuery->filterByPropertyKey($this->sFieldName);
        $oCustomerProperty = $oProductPropertyQuery->findOne();
        $sBool = '0';
        if($oCustomerProperty instanceof CustomerProperty)
        {
            $sBool = $oCustomerProperty->getPropertyValue();
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, ($sBool == '1'), $bReadonly);
    }
}
