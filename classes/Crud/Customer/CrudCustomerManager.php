<?php
namespace Crud\Customer;

use Core\Setting;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Crm\CustomerType;
use Model\Crm\Customer;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;
use Model\Crm\CustomerQuery;
use Exception\LogicException;
use Model\Crm\CustomerTypeQuery;

class CrudCustomerManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'klant';
    }
    function getOverviewUrl():string
    {
        return '/crm/customer_overview';
    }
    function getCreateNewUrl():string
    {
        return '/crm/customer_edit';
    }
    function getNewFormTitle():string{
        return 'Klant toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Klant wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Debitor',
            'CustomerCompany_CompanyName',
            'Email',
            'Delete',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Debitor',
            'CustomerCompany_CompanyName',
            'Email',
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']))
        {
            $oCustomer = CustomerQuery::create()->findOneById($aData['id']);

            if($oCustomer == null)
            {
                return new Customer();
            }
            if(!$oCustomer instanceof Customer)
            {
                throw new LogicException("Customer should be an instance of Customer but got ".get_class($oCustomer)." in ".__METHOD__);
            }
            $oCustomer = $this->fillVo($aData, $oCustomer);
        }
        else
        {
            $oCustomer = new Customer();

            if(!empty($aData))
            {
                $oCustomer = $this->fillVo($aData, $oCustomer);
            }
        }

        return $oCustomer;
    }

    function store(array $aData = null)
    {
        $oCustomer = $this->getModel($aData);
        if(!empty($oCustomer))
        {
            if(!$oCustomer->getId() && $oCustomer->getDebitor())
            {
                $iNextEdibitorNumber = Customer::generateNexDebitorNumber();
                $oCustomer->setDebitor($iNextEdibitorNumber);
            }
            $oCustomer = $this->fillVo($aData, $oCustomer);

            if(isset($aData['password']) && !empty($aData['password']))
            {
                $oCustomer->setSalt(substr(md5(time()), 2, 5));
                $oCustomer->setPassword(\Core\Customer::makeEncryptedPass($aData['password'], $oCustomer->getSalt()));
            }
            $oCustomer->save();

            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($oCustomer->getId());
            if($oCustomer->getCustomerType() instanceof CustomerType && $oCustomer->getCustomerType()->getCode() == 'company')
            {
                if(!$oCustomerCompany instanceof CustomerCompany)
                {
                    $oCustomerCompany = new CustomerCompany();
                }
                $oCustomerCompany->setCustomerId($oCustomer->getId());

                if(isset($aData['customer_company_company_type_id']))
                {
                    if(empty($aData['customer_company_company_type_id']))
                    {
                        $aData['customer_company_company_type_id'] = null;
                    }
                    $oCustomerCompany->setCompanyTypeId($aData['customer_company_company_type_id']);
                }
                if(isset($aData['customer_company_legal_form_id']))
                {
                    $oCustomerCompany->setLegalFormId($aData['customer_company_legal_form_id']);
                }
                if(isset($aData['customer_company_company_name']))
                {
                    $oCustomerCompany->setCompanyName($aData['customer_company_company_name']);
                }
                if(isset($aData['customer_company_chamber_of_commerce']))
                {
                    $oCustomerCompany->setChamberOfCommerce($aData['customer_company_chamber_of_commerce']);
                }
                if(isset($aData['customer_company_vat_number']))
                {
                    $oCustomerCompany->setVatNumber($aData['customer_company_vat_number']);
                }
                $oCustomerCompany->save();
            }
            else if($oCustomer->getCustomerType() instanceof CustomerType && $oCustomer->getCustomerType()->getCode() == 'person' && $oCustomerCompany instanceof CustomerCompany)
            {
                $oCustomerCompany->delete();
            }
        }
        $this->saveCustomFields($oCustomer, $aData);
        return $oCustomer;
    }

    public function fillVo($aData, Customer $oCustomer)
    {

        if(isset($aData['sector_id'])){$oCustomer->setSectorId($aData['sector_id']);}
        if(isset($aData['branche_id'])){
            if(empty($aData['branche_id']))
            {
                $aData['branche_id'] = null;
            }

            $oCustomer->setBrancheId($aData['branche_id']);
        }
        if(isset($aData['language_id'])){$oCustomer->setLanguageId($aData['language_id']);}
        if(isset($aData['can_buy_on_credit'])){$oCustomer->setCanBuyOnCredit($aData['can_buy_on_credit']);}
        if(isset($aData['customer_company_company_name']) && !empty($aData['customer_company_company_name']))
        {
            $oCustomerType =  CustomerTypeQuery::create()->findOneByCode('company');
            if($oCustomerType instanceof CustomerType)
            {
                $aData['customer_type_id'] = $oCustomerType->getId();
            }

        }

        if(isset($aData['is_intracommunautair'])){$oCustomer->setIsIntracommunautair($aData['is_intracommunautair']);}

        if(isset($aData['customer_type_id']))
        {
            $oCustomer->setCustomerTypeId($aData['customer_type_id']);
        }


        if(isset($aData['is_deleted'])){$oCustomer->setItemDeleted($aData['is_deleted']);}
        if(isset($aData['email'])){$oCustomer->setEmail($aData['email']);}
        if(isset($aData['fax'])){$oCustomer->setFax($aData['fax']);}
        if(isset($aData['gender'])){$oCustomer->setGender($aData['gender']);}
        if(isset($aData['first_name'])){$oCustomer->setFirstName($aData['first_name']);}
        if(isset($aData['insertion'])){$oCustomer->setInsertion($aData['insertion']);}
        if(isset($aData['last_name'])){$oCustomer->setLastName($aData['last_name']);}
        if(isset($aData['iban'])){$oCustomer->setIban($aData['iban']);}
        if(isset($aData['is_wholesale'])){$oCustomer->setIsWholesale($aData['is_wholesale']);}
        if(isset($aData['is_wholesale_approved'])){$oCustomer->setIsWholesaleApproved($aData['is_wholesale_approved']);}
        if(isset($aData['last_name'])){$oCustomer->setLastName($aData['last_name']);}
        if(isset($aData['tag'])){$oCustomer->setTag($aData['tag']);}
        if(isset($aData['payterm_id'])){$oCustomer->setPaytermId($aData['payterm_id']);}
        if(isset($aData['phone'])){$oCustomer->setPhone($aData['phone']);}
        if(isset($aData['mobile'])){$oCustomer->setMobile($aData['mobile']);}
        if(isset($aData['website'])){$oCustomer->setWebsite($aData['website']);}
        if(isset($aData['once_password_reset'])){$oCustomer->setOncePasswordReset($aData['once_password_reset']);}
        if(isset($aData['website'])){$oCustomer->setWebsite($aData['website']);}

        if(isset($aData['debitor']))
        {
            $oCustomer->setDebitor($aData['debitor']);
        }
        else if(!$oCustomer->getId())
        {
            if(Setting::get('customer_auto_debitor_numbering') == '1')
            {
                $oCustomer->setDebitor(Customer::generateNexDebitorNumber());
            }
            else
            {
                $oCustomer->setDebitor('');
            }
        }

        return $oCustomer;
    }
}
