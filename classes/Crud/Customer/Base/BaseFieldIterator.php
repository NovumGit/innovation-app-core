<?php
namespace Crud\Customer\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Customer\ICollectionField as CustomerField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Customer\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerField
	{
		return current($this->aFields);
	}
}
