<?php
namespace Crud\Customer\Base;

use Core\Utils;
use Crud;
use Crud\Customer\FieldIterator;
use Crud\Customer\Field\AccountingId;
use Crud\Customer\Field\BrancheId;
use Crud\Customer\Field\CanBuyOnCredit;
use Crud\Customer\Field\CreatedOn;
use Crud\Customer\Field\CustomerTypeId;
use Crud\Customer\Field\Debitor;
use Crud\Customer\Field\Email;
use Crud\Customer\Field\Fax;
use Crud\Customer\Field\FirstName;
use Crud\Customer\Field\Gender;
use Crud\Customer\Field\Iban;
use Crud\Customer\Field\ImportSource;
use Crud\Customer\Field\Insertion;
use Crud\Customer\Field\Ip;
use Crud\Customer\Field\IsIntracommunautair;
use Crud\Customer\Field\IsNewsletterSubscriber;
use Crud\Customer\Field\IsVerified;
use Crud\Customer\Field\IsWholesale;
use Crud\Customer\Field\IsWholesaleApproved;
use Crud\Customer\Field\ItemDeleted;
use Crud\Customer\Field\LanguageId;
use Crud\Customer\Field\LastName;
use Crud\Customer\Field\Mobile;
use Crud\Customer\Field\OldId;
use Crud\Customer\Field\OncePasswordReset;
use Crud\Customer\Field\Password;
use Crud\Customer\Field\PaytermId;
use Crud\Customer\Field\Phone;
use Crud\Customer\Field\Salt;
use Crud\Customer\Field\SectorId;
use Crud\Customer\Field\Tag;
use Crud\Customer\Field\VerificationKey;
use Crud\Customer\Field\Website;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Crm\Map\CustomerTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Customer instead if you need to override or add functionality.
 */
abstract class CrudCustomerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerQuery::create();
	}


	public function getTableMap(): CustomerTableMap
	{
		return new CustomerTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Customer";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ImportSource', 'AccountingId', 'OldId', 'Ip', 'CreatedOn', 'ItemDeleted', 'IsNewsletterSubscriber', 'LanguageId', 'IsIntracommunautair', 'PaytermId', 'BrancheId', 'SectorId', 'CustomerTypeId', 'Debitor', 'Gender', 'FirstName', 'Insertion', 'LastName', 'Email', 'Password', 'Salt', 'OncePasswordReset', 'IsWholesale', 'IsWholesaleApproved', 'CanBuyOnCredit', 'Iban', 'Phone', 'Mobile', 'Fax', 'Website', 'Tag', 'IsVerified', 'VerificationKey'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ImportSource', 'AccountingId', 'OldId', 'Ip', 'CreatedOn', 'ItemDeleted', 'IsNewsletterSubscriber', 'LanguageId', 'IsIntracommunautair', 'PaytermId', 'BrancheId', 'SectorId', 'CustomerTypeId', 'Debitor', 'Gender', 'FirstName', 'Insertion', 'LastName', 'Email', 'Password', 'Salt', 'OncePasswordReset', 'IsWholesale', 'IsWholesaleApproved', 'CanBuyOnCredit', 'Iban', 'Phone', 'Mobile', 'Fax', 'Website', 'Tag', 'IsVerified', 'VerificationKey'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Customer
	 */
	public function getModel(array $aData = null): Customer
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerQuery = CustomerQuery::create();
		     $oCustomer = $oCustomerQuery->findOneById($aData['id']);
		     if (!$oCustomer instanceof Customer) {
		         throw new LogicException("Customer should be an instance of Customer but got something else." . __METHOD__);
		     }
		     $oCustomer = $this->fillVo($aData, $oCustomer);
		}
		else {
		     $oCustomer = new Customer();
		     if (!empty($aData)) {
		         $oCustomer = $this->fillVo($aData, $oCustomer);
		     }
		}
		return $oCustomer;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Customer
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Customer
	{
		$oCustomer = $this->getModel($aData);


		 if(!empty($oCustomer))
		 {
		     $oCustomer = $this->fillVo($aData, $oCustomer);
		     $oCustomer->save();
		 }
		return $oCustomer;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Customer $oModel
	 * @return Customer
	 */
	protected function fillVo(array $aData, Customer $oModel): Customer
	{
		if(isset($aData['import_source'])) {
		     $oField = new ImportSource();
		     $mValue = $oField->sanitize($aData['import_source']);
		     $oModel->setImportSource($mValue);
		}
		if(isset($aData['accounting_id'])) {
		     $oField = new AccountingId();
		     $mValue = $oField->sanitize($aData['accounting_id']);
		     $oModel->setAccountingId($mValue);
		}
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['ip'])) {
		     $oField = new Ip();
		     $mValue = $oField->sanitize($aData['ip']);
		     $oModel->setIp($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['is_deleted'])) {
		     $oField = new ItemDeleted();
		     $mValue = $oField->sanitize($aData['is_deleted']);
		     $oModel->setItemDeleted($mValue);
		}
		if(isset($aData['is_newsletter_subscriber'])) {
		     $oField = new IsNewsletterSubscriber();
		     $mValue = $oField->sanitize($aData['is_newsletter_subscriber']);
		     $oModel->setIsNewsletterSubscriber($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['is_intracommunautair'])) {
		     $oField = new IsIntracommunautair();
		     $mValue = $oField->sanitize($aData['is_intracommunautair']);
		     $oModel->setIsIntracommunautair($mValue);
		}
		if(isset($aData['payterm_id'])) {
		     $oField = new PaytermId();
		     $mValue = $oField->sanitize($aData['payterm_id']);
		     $oModel->setPaytermId($mValue);
		}
		if(isset($aData['branche_id'])) {
		     $oField = new BrancheId();
		     $mValue = $oField->sanitize($aData['branche_id']);
		     $oModel->setBrancheId($mValue);
		}
		if(isset($aData['sector_id'])) {
		     $oField = new SectorId();
		     $mValue = $oField->sanitize($aData['sector_id']);
		     $oModel->setSectorId($mValue);
		}
		if(isset($aData['customer_type_id'])) {
		     $oField = new CustomerTypeId();
		     $mValue = $oField->sanitize($aData['customer_type_id']);
		     $oModel->setCustomerTypeId($mValue);
		}
		if(isset($aData['debitor'])) {
		     $oField = new Debitor();
		     $mValue = $oField->sanitize($aData['debitor']);
		     $oModel->setDebitor($mValue);
		}
		if(isset($aData['gender'])) {
		     $oField = new Gender();
		     $mValue = $oField->sanitize($aData['gender']);
		     $oModel->setGender($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['insertion'])) {
		     $oField = new Insertion();
		     $mValue = $oField->sanitize($aData['insertion']);
		     $oModel->setInsertion($mValue);
		}
		if(isset($aData['last_name'])) {
		     $oField = new LastName();
		     $mValue = $oField->sanitize($aData['last_name']);
		     $oModel->setLastName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['password'])) {
		     $oField = new Password();
		     $mValue = $oField->sanitize($aData['password']);
		     $oModel->setPassword($mValue);
		}
		if(isset($aData['salt'])) {
		     $oField = new Salt();
		     $mValue = $oField->sanitize($aData['salt']);
		     $oModel->setSalt($mValue);
		}
		if(isset($aData['once_password_reset'])) {
		     $oField = new OncePasswordReset();
		     $mValue = $oField->sanitize($aData['once_password_reset']);
		     $oModel->setOncePasswordReset($mValue);
		}
		if(isset($aData['is_wholesale'])) {
		     $oField = new IsWholesale();
		     $mValue = $oField->sanitize($aData['is_wholesale']);
		     $oModel->setIsWholesale($mValue);
		}
		if(isset($aData['is_wholesale_approved'])) {
		     $oField = new IsWholesaleApproved();
		     $mValue = $oField->sanitize($aData['is_wholesale_approved']);
		     $oModel->setIsWholesaleApproved($mValue);
		}
		if(isset($aData['can_buy_on_credit'])) {
		     $oField = new CanBuyOnCredit();
		     $mValue = $oField->sanitize($aData['can_buy_on_credit']);
		     $oModel->setCanBuyOnCredit($mValue);
		}
		if(isset($aData['iban'])) {
		     $oField = new Iban();
		     $mValue = $oField->sanitize($aData['iban']);
		     $oModel->setIban($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['mobile'])) {
		     $oField = new Mobile();
		     $mValue = $oField->sanitize($aData['mobile']);
		     $oModel->setMobile($mValue);
		}
		if(isset($aData['fax'])) {
		     $oField = new Fax();
		     $mValue = $oField->sanitize($aData['fax']);
		     $oModel->setFax($mValue);
		}
		if(isset($aData['website'])) {
		     $oField = new Website();
		     $mValue = $oField->sanitize($aData['website']);
		     $oModel->setWebsite($mValue);
		}
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		if(isset($aData['is_verified'])) {
		     $oField = new IsVerified();
		     $mValue = $oField->sanitize($aData['is_verified']);
		     $oModel->setIsVerified($mValue);
		}
		if(isset($aData['verification_key'])) {
		     $oField = new VerificationKey();
		     $mValue = $oField->sanitize($aData['verification_key']);
		     $oModel->setVerificationKey($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
