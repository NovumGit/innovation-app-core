<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Crud\Field;
use InvalidArgumentException;
use Model\Crm\Customer;
use Model\Setting\MasterTable\Branche;
use Model\Setting\MasterTable\BrancheQuery;


class BrancheId extends Field implements IFilterableField, IEditableField, IFilterableLookupField
{

    protected $sFieldLabel = 'Branche';
    protected $sFieldName = 'branche_id';

    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oBrancheQuery = BrancheQuery::create();
        $oBranche = $oBrancheQuery->findOneById($iItemId);

        if($oBranche instanceof Branche)
        {
            return $oBranche->getDescription();
        }
        return '';
    }

    function hasValidations()
    {

        if(BrancheQuery::create()->count() === 0)
        {
            return false;
        }
        return true;
    }

    function validate($aPostedData)
    {
        if(BrancheQuery::create()->count() === 0)
        {
            return false;
        }
        $mResponse = false;

        if(empty($aPostedData['branche_id']))
        {
            $mResponse[] = "Het veld branche is verplicht.";
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'language_name');
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }

        $oBranche = $mData->getBranche();

        $sDescription = '';
        if($oBranche instanceof Branche)
        {
            $sDescription = $oBranche->getDescription();
        }

        return '<td class="">' . $sDescription . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {

        $aBranches = BrancheQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aBranches, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getBrancheId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getBrancheId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
