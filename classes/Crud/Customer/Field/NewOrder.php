<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Crm\Customer;

class NewOrder extends Field implements IEventField{


    protected $sFieldName = null;
    protected $sFieldLabel = 'Nieuwe order';

    function getIcon()
    {
        return 'magic';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof Customer)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oCustomer = $mData;


        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Nieuwe order invoeren" href="/order/new?_do=SelectCustomerAddresses&customer_id='.$oCustomer->getId().'" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
