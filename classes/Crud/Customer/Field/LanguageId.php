<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\Customer;
use InvalidArgumentException;
use Crud\Field;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

class LanguageId extends Field implements IFilterableField, IEditableField, IFilterableLookupField
{

    protected $sFieldLabel = 'Taal';
    protected $sFieldName = 'language_id';

    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oLanguageQuery = LanguageQuery::create();
        $oLanguage = $oLanguageQuery->findOneById($iItemId);

        if($oLanguage instanceof Language)
        {
            return $oLanguage->getDescription();
        }
        return '';
    }

    function hasValidations()
    {
        return true;
    }

    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['language_id']))
        {
            $mResponse[] = "Het veld taal is verplicht.";
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'language_name');
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }

        $oLanguage = $mData->getLanguage();

        $sDescription = '';
        if($oLanguage instanceof Language)
        {
            $sDescription = $oLanguage->getDescription();
        }

        return '<td class="">' . $sDescription . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {

        $aLanguages = LanguageQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aLanguages, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getLanguageId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getLanguageId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
