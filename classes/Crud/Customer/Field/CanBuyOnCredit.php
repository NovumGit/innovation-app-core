<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Model\Crm\Customer as ModelObject;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

class CanBuyOnCredit extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldLabel = 'Mag op rekening kopen';
    protected $sFieldName = 'can_buy_on_credit';
    protected $sGetter = 'getCanBuyOnCredit';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getDataType():string
    {
        return 'boolean';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oObject)
    {
        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject, got ".get_class($oObject)." in ".__METHOD__);
        }
        $sVisibleValue = $oObject->{$this->sGetter}()?'Ja':'Nee';
        return '<td class="">'.$sVisibleValue.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\Product\\Product in ".__METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCanBuyOnCredit(), $bReadonly);
    }
}

