<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;

class Website extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'website';
    protected $sFieldLabel = 'Website';
    private $sIcon = 'globe';
    private $sPlaceHolder = '';
    private $sGetter = 'getWebsite';

    function getGetter()
    {
        return $this->sGetter;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getWebsite().'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getWebsite(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
