<?php
namespace Crud\Customer\Field;

use Core\Setting;
use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;



class Debitor extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'debitor';
    public $sFieldLabel = 'Debiteur';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getDebitor';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getDebitor().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        if (Setting::get('customer_auto_debitor_numbering') == 1)
        {
            $bReadonly = true;
        }

        if($mData->getId())
        {
            if($bReadonly)
            {
                $sValue = 'Debiteur: '.$mData->getDebitor();
            }
            else
            {
                $sValue = $mData->getDebitor();
            }
        }
        else if(Setting::get('customer_auto_debitor_numbering') == 1)
        {
            $sValue = ModelObject::generateNexDebitorNumber().' (proforma)';
        }
        else
        {
            $sValue = '';
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sValue, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
