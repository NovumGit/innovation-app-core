<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Base\CustomerQuery;
use Model\Crm\Customer as ModelObject;


class Email extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'email';
    protected $sFieldLabel = 'Algemeen e-mailadres';
    private $sIcon = 'envelope';
    private $sPlaceHolder = '';
    private $sGetter = 'getEmail';

    function getGetter()
    {
        return $this->sGetter;
    }

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(isset($aPostedData['email']))
        {
            if(!isset($aPostedData['email']) || empty($aPostedData['email']))
            {
                $mResponse[] = 'E-mailadres is een verplicht veld';
            }
            $aCustomers = CustomerQuery::create()->findByEmail($aPostedData['email']);
            foreach ($aCustomers as $oCustomer)
            {
                if($oCustomer->getId() != $aPostedData['id'])
                {
                    $mResponse[] = 'Klant '.$oCustomer->getCompanyName().' maakt al gebruik van dat e-mailadres.';
                }
            }
        }
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getEmail().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getEmail(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
