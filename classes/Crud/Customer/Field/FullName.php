<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;


class FullName extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'full_name';
    protected $sFieldLabel = 'Klantnaam';
    private $sIcon = 'person';
    private $sPlaceHolder = '';
    private $sGetter = '    ';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">' . $oModelObject->getFirstName() . ' ' . $oModelObject->getLastName() . '</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getFirstName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
