<?php
namespace Crud\Customer\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;


class CustomerCompany_ChamberOfCommerce extends Field{

    protected $sFieldName = 'customer_company_chamber_of_commerce';
    protected $sFieldLabel = 'KvK';
    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sChamberOfCommerce = '';
        if($oModelObject->getId())
        {

            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($oModelObject->getId());
            if ($oCustomerCompany instanceof CustomerCompany)
            {
                $sChamberOfCommerce = $oCustomerCompany->getChamberOfCommerce();
            }
        }
        return '<td class="">'.$sChamberOfCommerce.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sChamberOfCommerce = '';
        if($mData->getId())
        {

            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($mData->getId());
            if ($oCustomerCompany instanceof CustomerCompany)
            {
                $sChamberOfCommerce = $oCustomerCompany->getChamberOfCommerce();
            }
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sChamberOfCommerce, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
