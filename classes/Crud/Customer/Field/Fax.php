<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;


class Fax extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'fax';
    protected $sFieldLabel = 'Fax (algemeen)';
    private $sIcon = 'print';
    private $sPlaceHolder = '';
    private $sGetter = 'getFax';

    function getGetter()
    {
        return $this->sGetter;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getFax().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getFax(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
