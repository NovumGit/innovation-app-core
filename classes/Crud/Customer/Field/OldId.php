<?php
namespace Crud\Customer\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;

class OldId extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'old_id';
    protected $sFieldLabel = 'Oud id';
    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function hasValidations()
    {
        return false;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getOldId().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        /* Old id is always read-only */
        $bReadonly = true;
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getOldId(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
