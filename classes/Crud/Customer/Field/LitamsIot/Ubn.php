<?php
namespace Crud\Customer\Field\LitamsIot;

class Ubn extends AbstractProperty
{
    function getIcon()
    {
        return 'tag';
    }
    public function getFieldTitle(): string
    {
        return 'UBN';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'ubn';
    }
}

