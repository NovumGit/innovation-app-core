<?php
namespace Crud\Customer\Field\LitamsIot;

class HasHorse extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft paarden';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_horse';
    }
}

