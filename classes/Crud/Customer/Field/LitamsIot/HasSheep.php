<?php
namespace Crud\Customer\Field\LitamsIot;

class HasSheep extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft schapen';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_sheep';
    }
}

