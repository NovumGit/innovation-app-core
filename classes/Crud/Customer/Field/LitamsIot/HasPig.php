<?php
namespace Crud\Customer\Field\LitamsIot;

class HasPig extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft varkens';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_pigs';
    }
}

