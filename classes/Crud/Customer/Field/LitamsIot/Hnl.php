<?php
namespace Crud\Customer\Field\LitamsIot;

class Hnl extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Hln';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'hnl';
    }
    function getIcon()
    {
        return 'tag';
    }
}

