<?php
namespace Crud\Customer\Field\LitamsIot;

class Status extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Status';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'status';
    }
}

