<?php
namespace Crud\Customer\Field\LitamsIot;

class HasDeer extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft herten';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_deer';
    }
    function getIcon()
    {
        return 'barcode';
    }
}

