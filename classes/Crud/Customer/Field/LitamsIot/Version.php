<?php
namespace Crud\Customer\Field\LitamsIot;

class Version extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Versie';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'version';
    }
    function getIcon()
    {
        return 'code';
    }
}

