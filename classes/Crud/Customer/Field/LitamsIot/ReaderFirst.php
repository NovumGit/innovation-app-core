<?php
namespace Crud\Customer\Field\LitamsIot;

class ReaderFirst extends AbstractProperty
{

    public function getFieldTitle(): string
    {
        return 'Reader 1';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'reader_1';
    }
    function getIcon()
    {
        return 'barcode';
    }
}

