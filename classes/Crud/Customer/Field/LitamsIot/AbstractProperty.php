<?php
namespace Crud\Customer\Field\LitamsIot;

use Core\Translate;
use Crud\Field;
use Crud\ICustomCrudField;
use Model\Crm\Customer as ModelObject;
use InvalidArgumentException;
use Model\Crm\Customer;
use Model\Crm\CustomerProperty;
use Model\Crm\CustomerPropertyQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class AbstractProperty extends Field implements ICustomCrudField {

    abstract function getFieldTitle():string;

    function getIcon()
    {
        return null;
    }
    function getTranslatedTitle()
    {
        return Translate::fromCode($this->getFieldTitle());
    }
    /**
     * @param ActiveRecordInterface $oObject
     * @param array $aData
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(ActiveRecordInterface $oObject, array $aData):void
    {

        if(!$oObject instanceof Customer)
        {
            return;
        }
        if(!isset($aData[$this->getFieldName()]))
        {
            return;
        }
        $oCustomerPropertyQuery = CustomerPropertyQuery::create();
        $oCustomerPropertyQuery->filterByCustomerId($oObject->getId());
        $oCustomerPropertyQuery->filterByPropertyKey($this->getFieldName());
        $oCustomerProperty = $oCustomerPropertyQuery->findOne();

        if(!$oCustomerProperty instanceof CustomerProperty)
        {
            $oCustomerProperty = new CustomerProperty();
            $oCustomerProperty->setCustomerId($oObject->getId());
            $oCustomerProperty->setPropertyKey($this->getFieldName());
        }

        $oCustomerProperty->setPropertyValue($aData[$this->getFieldName()]);
        $oCustomerProperty->save();
        return;
    }
    abstract function getDataType():string;
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }


    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->getFieldName());
    }
    function getOverviewValue($oObject)
    {
        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject, got ".get_class($oObject)." in ".__METHOD__);
        }

        $oCustomerPropertyQuery = CustomerPropertyQuery::create();
        $oCustomerPropertyQuery->filterByCustomerId($oObject->getId());
        $oCustomerPropertyQuery->filterByPropertyKey($this->getFieldName());
        $sCustomerProperty = $sCustomerProperty = $oCustomerPropertyQuery->findOne();

        $sValue = '';
        if($sCustomerProperty instanceof CustomerProperty)
        {
            $sValue = $sCustomerProperty->getPropertyValue();
        }

        if($this->getDataType() == 'boolean')
        {
            $sValue = '';
            if($sCustomerProperty === false)
            {
                $sValue = 'Nee';
            }
            else if($sCustomerProperty === true)
            {
                $sValue = 'Ja';
            }
        }

        return '<td class="">' . $sValue . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\Product\\Product in ".__METHOD__);
        }

        $oCustomerPropertyQuery = CustomerPropertyQuery::create();
        $oCustomerPropertyQuery->filterByCustomerId($mData->getId());
        $oCustomerPropertyQuery->filterByPropertyKey($this->sFieldName);
        $oCustomerProperty = $oCustomerPropertyQuery->findOne();

        if(!$oCustomerProperty instanceof CustomerProperty)
        {
            $oCustomerProperty = new CustomerProperty();
        }

        if($this->getDataType() == 'boolean')
        {

            $aDropdownOptions = [
                ['id' => 'Onbekend', 'selected' =>  ($oCustomerProperty->getPropertyValue() == 'Onbekend' ? 'selected' : null), 'label' => 'Onbekend'],
                ['id' => 'Ja', 'selected' => ($oCustomerProperty->getPropertyValue() == 'Ja' ? 'selected' : null), 'label' => 'Ja'],
                ['id' => 'Nee', 'selected' => ($oCustomerProperty->getPropertyValue() == 'Nee' ? 'selected' : null), 'label' => 'Nee'],
            ];

            return $this->editLookupField($this->getTranslatedTitle(), $this->getFieldName(), $oCustomerProperty->getPropertyValue(), $aDropdownOptions, false, $this->getIcon());

        }
        else
        {
            return $this->editTextField($this->getTranslatedTitle(), $this->getFieldName(), $oCustomerProperty->getPropertyValue(), $bReadonly, $this->getIcon());

        }
    }
}

