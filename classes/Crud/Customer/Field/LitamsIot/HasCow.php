<?php
namespace Crud\Customer\Field\LitamsIot;

class HasCow extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft koeien';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_cow';
    }
}

