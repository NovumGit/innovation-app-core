<?php
namespace Crud\Customer\Field\LitamsIot;

class HasGoat extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Heeft geiten';
    }
    function getDataType():string
    {
        return 'boolean';
    }
    public function getFieldName(): string
    {
        return 'has_goat';
    }
}

