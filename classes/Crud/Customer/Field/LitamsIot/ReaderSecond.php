<?php
namespace Crud\Customer\Field\LitamsIot;

class ReaderSecond extends AbstractProperty
{
    public function getFieldTitle(): string
    {
        return 'Reader 2';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'reader_1';
    }
    function getIcon()
    {
        return 'barcode';
    }

}

