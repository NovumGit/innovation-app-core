<?php
namespace Crud\Customer\Field\LitamsIot;

class StockQuantity extends AbstractProperty
{
    function getIcon()
    {
        return 'tachometer';
    }
    public function getFieldTitle(): string
    {
        return 'Aantal dieren';
    }
    function getDataType():string
    {
        return 'string';
    }
    public function getFieldName(): string
    {
        return 'stock_quantity';
    }
}

