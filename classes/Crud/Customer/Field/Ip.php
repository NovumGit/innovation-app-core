<?php
namespace Crud\Customer\Field;

use Crud\Customer\Field\Base\Ip as BaseIp;

/**
 * Skeleton subclass for representing ip field from the customer table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:28:10
 */
final class Ip extends BaseIp
{
}
