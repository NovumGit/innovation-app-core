<?php
namespace Crud\Customer\Field;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;

class GenericSearchfield extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'generic_search_field';
    protected $sFieldLabel = 'Algemeen zoekveld';

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return null;
    }
    function getOverviewValue($oModelObject)
    {
        return null;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        return null;
    }
}
