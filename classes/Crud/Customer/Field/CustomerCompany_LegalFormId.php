<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\Field;

use InvalidArgumentException;
use Model\Crm\Customer as ModelObject;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;
use Model\Setting\MasterTable\LegalForm;
use Model\Setting\MasterTable\LegalFormQuery;

class CustomerCompany_LegalFormId extends Field{

    protected $sFieldName = 'customer_company_legal_form_id';
    protected $sFieldLabel = 'Rechtsvorm';
    private $sIcon = 'legal';


    function hasValidations() { return false; }
    function validate($aPostedData)
    {

        $mResponse = false;
        /*
         * @todo weer aanzetten zodra de validatie niet plaatsvind op ieder veld maar alleen de velden die ook daadwerkelijk op het scherm staan.
        if(empty($aPostedData['legal_form_id']))
        {
            $mResponse[] = "Het veld rechtsvorm is verplicht.";
        }
        else
        {
            $oLegalForm = LegalFormQuery::create()->findOneById($aPostedData['legal_form_id']);

            if(!$oLegalForm instanceof LegalForm)
            {
                StatusMessage::danger("Er is iets fout gegaan, het veld rechtsvorm is wel opgegeven maar komt niet voor in de stam tabellen.");
            }
        }
        */
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getLookups($iSelectedItem = null)
    {
        $aLegalForms = LegalFormQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aLegalForms, 'getName', $iSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return LegalFormQuery::create()->findOneById($iItemId)->getName();
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sLegalForm = '';
        if($oModelObject->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($oModelObject->getId());
            if($oCustomerCompany instanceof CustomerCompany)
            {
                $iLegalFormId = $oCustomerCompany->getLegalFormId();

                if($iLegalFormId)
                {
                    $oLegalForm = LegalFormQuery::create()->findOneById($iLegalFormId);

                    if($oLegalForm instanceof LegalForm)
                    {
                        $sLegalForm = $oLegalForm->getName();
                    }
                }
            }
        }
        return '<td class="">'.$sLegalForm.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $iLegalFormId = null;
        if($mData->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($mData->getId());
            if($oCustomerCompany instanceof CustomerCompany)
            {
                $iLegalFormId = $oCustomerCompany->getLegalFormId();
            }
        }

        $aOptions = $this->getLookups($iLegalFormId);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $iLegalFormId, $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}