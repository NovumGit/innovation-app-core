<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Crm\Customer;

class ApproveWholesaleButton extends Field implements IEventField{

    protected $sFieldLabel = 'Wholesale goedkeuren button';
    function getIcon()
    {
        return 'list-ol';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-thumbs-up"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {
        if(!$mData instanceof Customer)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oCustomer = $mData;
        $sTab = '';
        if(isset($_GET['tab']))
        {
            $sTab = '&tab='.$_GET['tab'];
        }
        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Goedkeuren" href="/crm/customer_edit?id='.$oCustomer->getId().'&_do=ApproveWholesale'.$sTab.'" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-thumbs-up"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
