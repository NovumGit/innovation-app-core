<?php
namespace Crud\Customer\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Crm\Customer;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof Customer)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }
        $sTab = '';
        if(isset($_GET['tab']))
        {
            $sTab = '&tab='.$_GET['tab'];

        }
        return '/crm/customer_edit?_do=Delete&id='.$oObject->getId().$sTab;
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}