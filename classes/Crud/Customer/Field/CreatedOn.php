<?php
namespace Crud\Customer\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;


class CreatedOn extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'created_on';
    protected $sFieldLabel = 'Aangemaakt op';
    private $sIcon = 'calendar';
    private $sPlaceHolder = '';

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function hasValidations()
    {
        return false;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCreatedOn = $oModelObject->getCreatedOn();
        $sCreatedOn = '';
        if($oCreatedOn instanceof CreatedOn)
        {
            $sCreatedOn = $oModelObject->getCreatedOn()->format(Config::getDateFormat());
        }

        return '<td class="">'.$sCreatedOn.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        /* this field is always read only */
        $bReadonly = true;

        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oCreatedOn = $mData->getCreatedOn();
        $sCreatedOn = '';
        if($oCreatedOn instanceof CreatedOn)
        {
            $sCreatedOn = $oCreatedOn->format(Config::getDateFormat());
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sCreatedOn, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
