<?php
namespace Crud\Customer\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Crm\Customer;

class DisapproveWholesaleButton extends Field implements IEventField{

    protected $sFieldLabel = 'Wholesale afkeuren button';
    function getIcon()
    {
        return 'list-ol';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-thumbs-down"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {
        if(!$mData instanceof Customer)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oCustomer = $mData;

        $sTab = '';
        if(isset($_GET['tab']))
        {
            $sTab = '&tab='.$_GET['tab'];
        }


        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Afkeuren" href="/crm/customer_edit?id='.$oCustomer->getId().'&_do=DisapproveWholesale'.$sTab.'" class="btn btn-warning br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-thumbs-down"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
