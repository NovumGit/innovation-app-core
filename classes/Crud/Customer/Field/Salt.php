<?php
namespace Crud\Customer\Field;

use Crud\Customer\Field\Base\Salt as BaseSalt;

/**
 * Skeleton subclass for representing salt field from the customer table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:28:10
 */
final class Salt extends BaseSalt
{
}
