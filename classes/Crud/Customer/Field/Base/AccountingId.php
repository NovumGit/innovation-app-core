<?php
namespace Crud\Customer\Field\Base;

use Crud\Customer\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'accounting_id' crud field from the 'customer' table.
 * This class is auto generated and should not be modified.
 */
abstract class AccountingId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'accounting_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAccountingId';
	protected $sFqModelClassname = '\\\Model\Crm\Customer';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
