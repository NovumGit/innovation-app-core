<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Crud\Field;
use InvalidArgumentException;
use Model\Crm\Customer;
use Model\Crm\CustomerSector;
use Model\Crm\CustomerSectorQuery;

class SectorId extends Field implements IFilterableField, IEditableField, IFilterableLookupField
{

    protected $sFieldLabel = 'Sector';
    protected $sFieldName = 'sector_id';

    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oSectorQuery = CustomerSectorQuery::create();
        $oSector = $oSectorQuery->findOneById($iItemId);

        if($oSector instanceof CustomerSector)
        {
            return $oSector->getDescription();
        }
        return '';
    }

    function hasValidations()
    {
        return true;
    }

    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['sector_id']))
        {
            $mResponse[] = "Het veld sector is verplicht.";
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'sector_id');
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }

        $oSector = $mData->getCustomerSector();

        $sDescription = '';
        if($oSector instanceof CustomerSector)
        {
            $sDescription = $oSector->getDescription();
        }

        return '<td class="">' . $sDescription . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {

        $aSectors = CustomerSectorQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aSectors, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getSectorId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getSectorId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
