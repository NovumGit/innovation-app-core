<?php
namespace Crud\Customer\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Crm\Customer;

class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof Customer)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/crm/customer_edit?id='.$oObject->getId();
    }
}