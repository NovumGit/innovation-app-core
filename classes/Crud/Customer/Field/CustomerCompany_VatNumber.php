<?php
namespace Crud\Customer\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;

class CustomerCompany_VatNumber extends Field{

    protected $sFieldName = 'customer_company_vat_number';
    protected $sFieldLabel = 'BTW Nummer';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sVatNumber = '';
        if($oModelObject->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($oModelObject->getId());
            if ($oCustomerCompany instanceof CustomerCompany)
            {
                $sVatNumber = $oCustomerCompany->getVatNumber();
            }
        }
        return '<td class="">'.$sVatNumber.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sVatNumber = '';
        if($mData->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($mData->getId());
            if ($oCustomerCompany instanceof CustomerCompany)
            {
                $sVatNumber = $oCustomerCompany->getVatNumber();
            }
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sVatNumber, $this->sPlaceHolder, $this->sIcon, false);
    }
}
