<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Crud\Field;
use InvalidArgumentException;
use Model\Company\Company;
use Model\Crm\CompanyCategory;
use Model\Crm\CompanyCategoryQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerCompany;

class CustomerCompany_CompanyTypeId extends Field implements IFilterableField, IEditableField, IFilterableLookupField
{

    protected $sFieldLabel = 'Bedrijfstype';
    protected $sFieldName = 'customer_company_company_type_id';

    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oCompanyCategoryQuery = CompanyCategoryQuery::create();
        $oCompanyCategory = $oCompanyCategoryQuery->findOneById($iItemId);

        if($oCompanyCategory instanceof CompanyCategory)
        {
            return $oCompanyCategory->getDescription();
        }
        return '';
    }
    function hasValidations()
    {
        if(CompanyCategoryQuery::create()->count() > 0)
        {
            return true;
        }
        return false;

    }
    function validate($aPostedData)
    {
        if(CompanyCategoryQuery::create()->count() === 0)
        {
            return false;
        }
        $mResponse = false;
        if(empty($aPostedData['customer_company_company_type_id']))
        {
            $mResponse[] = 'Het veld bedrijfstype is een verplicht veld.';
        }
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'customer_company_company_type_id');
    }
    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }

        $sDescription = '';
        if($mData->getCompany() instanceof Company)
        {
            $oCompanyCategory = $mData->getCompany()->getCompanyCategory();
            if($oCompanyCategory instanceof CompanyCategory)
            {
                $sDescription = $oCompanyCategory->getDescription();
            }
        }
        return '<td class="">' . $sDescription . '</td>';
    }
    function getLookups($mSelectedItem = null)
    {
        $CompanyCategory = CompanyCategoryQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($CompanyCategory, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }
        $iCompanyTypeId = null;
        $oCompany = $mData->getCompany();

        if($oCompany instanceof CustomerCompany)
        {
            $iCompanyTypeId = $oCompany->getCompanyTypeId();
        }
        if(isset($_POST['data']) && isset($_POST['data']['customer_company_company_type_id']))
        {
            $iCompanyTypeId = $_POST['data']['customer_company_company_type_id'];
        }

        $aDropdownOptions = $this->getLookups($iCompanyTypeId);
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $iCompanyTypeId, $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
