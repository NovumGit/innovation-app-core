<?php
namespace Crud\Customer\Field;

use Core\StatusMessage;
use Core\Utils;
use Crud\Field;
use Crud\IFilterableLookupField;
use InvalidArgumentException;
use Model\Crm\Customer as ModelObject;
use Model\Setting\MasterTable\PayTermQuery;
use Model\Setting\MasterTable\PayTerm;

class PaytermId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'payterm_id';
    protected $sFieldLabel = 'Betalingstermijn';
    private $sIcon = 'calendar';


    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations()
    {
        return true;
    }
    function validate($aPostedData)
    {

        $mResponse = false;

        // @todo weer aanzetten zodra de validatie niet plaatsvind op ieder veld maar alleen de velden die ook daadwerkelijk op het scherm staan.
        if(empty($aPostedData[$this->sFieldName]))
        {
            $mResponse[] = "Het veld betaaltermijn is verplicht.";
        }
        else
        {
            $oPayTerm = PayTermQuery::create()->findOneById($aPostedData[$this->sFieldName]);

            if(!$oPayTerm instanceof PayTerm)
            {
                StatusMessage::danger("Er is iets fout gegaan, het veld betaaltermijn is wel opgegeven maar komt niet voor in de stam tabellen.");
            }
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getLookups($iSelectedItem = null)
    {
        $aPayTerms = PayTermQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aPayTerms, 'getName', $iSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return PayTermQuery::create()->findOneById($iItemId)->getName();
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getPaytermId().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $aOptions = $this->getLookups($mData->getPaytermId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getPaytermId(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
