<?php
namespace Crud\Customer\Field;

use Core\Utils;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\Customer;
use Model\Crm\CustomerType;
use Model\Crm\CustomerTypeQuery;
use InvalidArgumentException;
use Crud\Field;

class CustomerTypeId extends Field implements IFilterableField, IEditableField, IFilterableLookupField, IDisplayableField
{

    protected $sFieldLabel = 'Klant type';
    protected $sFieldName = 'customer_type_id';
    private $sGetter = 'getCustomerTypeId';
    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return 'Klant type';
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {

        $oQuery = CustomerTypeQuery::create();
        $oCustomerType = $oQuery->findOneById($iItemId);

        if($oCustomerType instanceof CustomerType)
        {
            return $oCustomerType->getDescription();
        }
        return '';
    }

    function hasValidations()
    {
        return true;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        if(!isset($aPostedData['customer_type_id']) || empty($aPostedData['customer_type_id']))
        {
            $mResponse = [];
            $mResponse[] = 'Het veld klanttype is leeg, geef aan of het een bedrijf of particulier betreft';
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'role_name');
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }
        return '<td class="">' . $mData->getCustomerType()->getDescription() . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {
        $aCustomerTypes = CustomerTypeQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aCustomerTypes, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getCustomerTypeId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCustomerTypeId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
