<?php
namespace Crud\Customer\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\Customer as ModelObject;


class Mobile extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'mobile';
    protected $sFieldLabel = 'Mobile';
    private $sIcon = 'mobile';
    private $sPlaceHolder = '';
    private $sGetter = 'getMobile';

    function getGetter()
    {
        return $this->sGetter;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getPhone().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getPhone(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
