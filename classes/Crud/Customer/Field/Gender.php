<?php
namespace Crud\Customer\Field;

use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\Customer;
use InvalidArgumentException;
use Crud\Field;

class Gender extends Field implements IFilterableField, IEditableField, IFilterableLookupField, IDisplayableField
{

    protected $sFieldLabel = 'Geslacht';
    protected $sFieldName = 'gender';

    private $sPlaceHolder;
    private $sIcon = 'bolt';
    private $sGetter = 'getDebitor';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        if($iItemId == 'male')
        {
            return 'Man';
        }
        else if($iItemId == 'female')
        {
            return 'Vrouw';
        }
        else if($iItemId == 'other')
        {
            return 'Overig';
        }
        else
        {
            return 'Onbekend';
        }
    }

    function hasValidations()
    {
        return true;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), 'gender');
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer in " . __METHOD__);
        }

        return '<td class="">' . $this->getVisibleValue($mData->getGender()) . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {
        $aSelected['male'] = $mSelectedItem == 'male' ? 'selcted' : '';
        $aSelected['female'] = $mSelectedItem == 'female' ? 'selcted' : '';
        $aSelected['other'] = $mSelectedItem == 'other' ? 'selcted' : '';
        $aSelected['undefined'] = $mSelectedItem == '' ? 'selcted' : '';

        $aLookukups = [];
        $aLookukups[] = ['id' => 'male', 'label' => 'Man', 'selected' => $aSelected['male']];
        $aLookukups[] = ['id' => 'female', 'label' => 'Vrouw', 'selected' => $aSelected['female']];
        $aLookukups[] = ['id' => 'other', 'label' => 'Overig', 'selected' => $aSelected['other']];
        $aLookukups[] = ['id' => '', 'label' => 'Onbekend', 'selected' => $aSelected['undefined']];

        return $aLookukups;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Customer)
        {
            throw new InvalidArgumentException("Expected an instance of Customer " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getLanguageId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getGender(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
