<?php
namespace Crud\Customer\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Crm\Customer as ModelObject;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;

class CustomerCompany_CompanyName extends Field{

    protected $sFieldName = 'customer_company_company_name';
    protected $sFieldLabel = 'Bedrijfsnaam';
    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sCompanyName = '';
        if($oModelObject->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($oModelObject->getId());
            if($oCustomerCompany instanceof CustomerCompany)
            {
                $sCompanyName = $oCustomerCompany->getCompanyName();
            }
        }

        return '<td class="">'.$sCompanyName.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sCompanyName = '';
        if($mData->getId())
        {
            $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($mData->getId());

            if($oCustomerCompany instanceof CustomerCompany)
            {
                $sCompanyName = $oCustomerCompany->getCompanyName();
            }
        }
        else
        {
            if(isset($_POST['data']) && isset($_POST['data']['customer_company_company_name']))
            {
                $sCompanyName = $_POST['data']['customer_company_company_name'];
            }

        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sCompanyName, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}