<?php
namespace Crud\UniqueFieldGroupField\Field;

use Crud\UniqueFieldGroupField\Field\Base\DataFieldId as BaseDataFieldId;

/**
 * Skeleton subclass for representing data_field_id field from the unique_field_group_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Mon 12:56:38
 */
final class DataFieldId extends BaseDataFieldId
{
}
