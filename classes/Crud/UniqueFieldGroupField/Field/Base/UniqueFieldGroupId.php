<?php
namespace Crud\UniqueFieldGroupField\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Crud\UniqueFieldGroupField\ICollectionField;
use Model\System\DataModel\Model\DataModelQuery;

/**
 * Base class that represents the 'unique_field_group_id' crud field from the 'unique_field_group_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class UniqueFieldGroupId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'unique_field_group_id';
	protected $sFieldLabel = 'Id';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUniqueFieldGroupId';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\UniqueFieldGroupField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\DataModel\Model\DataModelQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\DataModel\Model\DataModelQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['unique_field_group_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Id" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
