<?php
namespace Crud\UniqueFieldGroupField\Field\Base;

use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UniqueFieldGroupField\ICollectionField;

/**
 * Base class that represents the 'sorting' crud field from the 'unique_field_group_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class Sorting extends GenericLookup implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sorting';
	protected $sFieldLabel = 'Volgorde';
	protected $sIcon = 'sort';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSorting';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\UniqueFieldGroupField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
