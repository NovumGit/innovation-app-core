<?php
namespace Crud\UniqueFieldGroupField\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UniqueFieldGroupField\FieldIterator;
use Crud\UniqueFieldGroupField\Field\DataFieldId;
use Crud\UniqueFieldGroupField\Field\Sorting;
use Crud\UniqueFieldGroupField\Field\UniqueFieldGroupId;
use Exception\LogicException;
use Model\System\DataModel\Model\Map\UniqueFieldGroupFieldTableMap;
use Model\System\DataModel\Model\UniqueFieldGroupField;
use Model\System\DataModel\Model\UniqueFieldGroupFieldQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UniqueFieldGroupField instead if you need to override or add functionality.
 */
abstract class CrudUniqueFieldGroupFieldManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UniqueFieldGroupFieldQuery::create();
	}


	public function getTableMap(): UniqueFieldGroupFieldTableMap
	{
		return new UniqueFieldGroupFieldTableMap();
	}


	public function getShortDescription(): string
	{
		return "Velden binnen een groep die samen uniek moeten zijn.";
	}


	public function getEntityTitle(): string
	{
		return "UniqueFieldGroupField";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "unique_field_group_field toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "unique_field_group_field aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Sorting', 'UniqueFieldGroupId', 'DataFieldId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Sorting', 'UniqueFieldGroupId', 'DataFieldId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UniqueFieldGroupField
	 */
	public function getModel(array $aData = null): UniqueFieldGroupField
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUniqueFieldGroupFieldQuery = UniqueFieldGroupFieldQuery::create();
		     $oUniqueFieldGroupField = $oUniqueFieldGroupFieldQuery->findOneById($aData['id']);
		     if (!$oUniqueFieldGroupField instanceof UniqueFieldGroupField) {
		         throw new LogicException("UniqueFieldGroupField should be an instance of UniqueFieldGroupField but got something else." . __METHOD__);
		     }
		     $oUniqueFieldGroupField = $this->fillVo($aData, $oUniqueFieldGroupField);
		}
		else {
		     $oUniqueFieldGroupField = new UniqueFieldGroupField();
		     if (!empty($aData)) {
		         $oUniqueFieldGroupField = $this->fillVo($aData, $oUniqueFieldGroupField);
		     }
		}
		return $oUniqueFieldGroupField;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UniqueFieldGroupField
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UniqueFieldGroupField
	{
		$oUniqueFieldGroupField = $this->getModel($aData);


		 if(!empty($oUniqueFieldGroupField))
		 {
		     $oUniqueFieldGroupField = $this->fillVo($aData, $oUniqueFieldGroupField);
		     $oUniqueFieldGroupField->save();
		 }
		return $oUniqueFieldGroupField;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UniqueFieldGroupField $oModel
	 * @return UniqueFieldGroupField
	 */
	protected function fillVo(array $aData, UniqueFieldGroupField $oModel): UniqueFieldGroupField
	{
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['unique_field_group_id'])) {
		     $oField = new UniqueFieldGroupId();
		     $mValue = $oField->sanitize($aData['unique_field_group_id']);
		     $oModel->setUniqueFieldGroupId($mValue);
		}
		if(isset($aData['data_field_id'])) {
		     $oField = new DataFieldId();
		     $mValue = $oField->sanitize($aData['data_field_id']);
		     $oModel->setDataFieldId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
