<?php
namespace Crud\UniqueFieldGroupField\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UniqueFieldGroupField\ICollectionField as UniqueFieldGroupFieldField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UniqueFieldGroupField\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UniqueFieldGroupFieldField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UniqueFieldGroupFieldField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UniqueFieldGroupFieldField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UniqueFieldGroupFieldField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UniqueFieldGroupFieldField
	{
		return current($this->aFields);
	}
}
