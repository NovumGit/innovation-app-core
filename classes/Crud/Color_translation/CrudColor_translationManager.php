<?php
namespace Crud\Color_translation;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\ColorTranslation;
use Model\Setting\MasterTable\ColorTranslationQuery;
use Exception\LogicException;

class CrudColor_translationManager extends FormManager implements IConfigurableCrud {
    function getEntityTitle():string
    {
        return 'kleur taal';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/color/translation/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/color/translation/edit';
    }
    function getNewFormTitle():string{
        return 'Kleur toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Kleur wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name'
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oColorTranslationQuery = ColorTranslationQuery::create();
            $oColorTranslation = $oColorTranslationQuery->findOneById($aData['id']);
            if(!$oColorTranslation instanceof ColorTranslation)
            {
                throw new LogicException("ColorTraqnslation should be an instance of Color but got ".get_class($oColorTranslation)." in ".__METHOD__);
            }
        }
        else
        {
            $oColorTranslation = new ColorTranslation();
            if(!empty($aData))
            {
                $oColorTranslation = $this->fillVo($aData, $oColorTranslation);
            }
        }
        return $oColorTranslation;
    }
    function store(array $aData = null)
    {
        $oColor = $this->getModel($aData);
        if(!empty($oColor))
        {
            $oColor = $this->fillVo($aData, $oColor);
            $oColor->save();
        }
        return $oColor;
    }
    private function fillVo($aData, ColorTranslation $oColorTranslation)
    {

        if(isset($aData['language_id'])) { $oColorTranslation->setLanguageId($aData['language_id']); }
        if(isset($aData['color_id'])) { $oColorTranslation->setColorId($aData['color_id']); }
        if(isset($aData['name'])){$oColorTranslation->setName($aData['name']);}
        return $oColorTranslation;
    }
}
