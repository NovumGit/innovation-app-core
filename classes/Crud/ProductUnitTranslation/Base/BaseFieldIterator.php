<?php
namespace Crud\ProductUnitTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductUnitTranslation\ICollectionField as ProductUnitTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductUnitTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductUnitTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductUnitTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductUnitTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductUnitTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductUnitTranslationField
	{
		return current($this->aFields);
	}
}
