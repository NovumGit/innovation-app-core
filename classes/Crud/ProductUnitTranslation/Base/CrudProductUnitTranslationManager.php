<?php
namespace Crud\ProductUnitTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductUnitTranslation\FieldIterator;
use Crud\ProductUnitTranslation\Field\LanguageId;
use Crud\ProductUnitTranslation\Field\Name;
use Crud\ProductUnitTranslation\Field\NamePlural;
use Crud\ProductUnitTranslation\Field\UnitId;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ProductUnitTranslationTableMap;
use Model\Setting\MasterTable\ProductUnitTranslation;
use Model\Setting\MasterTable\ProductUnitTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductUnitTranslation instead if you need to override or add functionality.
 */
abstract class CrudProductUnitTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductUnitTranslationQuery::create();
	}


	public function getTableMap(): ProductUnitTranslationTableMap
	{
		return new ProductUnitTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductUnitTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_unit_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_unit_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'UnitId', 'Name', 'NamePlural'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'UnitId', 'Name', 'NamePlural'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductUnitTranslation
	 */
	public function getModel(array $aData = null): ProductUnitTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductUnitTranslationQuery = ProductUnitTranslationQuery::create();
		     $oProductUnitTranslation = $oProductUnitTranslationQuery->findOneById($aData['id']);
		     if (!$oProductUnitTranslation instanceof ProductUnitTranslation) {
		         throw new LogicException("ProductUnitTranslation should be an instance of ProductUnitTranslation but got something else." . __METHOD__);
		     }
		     $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
		}
		else {
		     $oProductUnitTranslation = new ProductUnitTranslation();
		     if (!empty($aData)) {
		         $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
		     }
		}
		return $oProductUnitTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductUnitTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductUnitTranslation
	{
		$oProductUnitTranslation = $this->getModel($aData);


		 if(!empty($oProductUnitTranslation))
		 {
		     $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
		     $oProductUnitTranslation->save();
		 }
		return $oProductUnitTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductUnitTranslation $oModel
	 * @return ProductUnitTranslation
	 */
	protected function fillVo(array $aData, ProductUnitTranslation $oModel): ProductUnitTranslation
	{
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['unit_id'])) {
		     $oField = new UnitId();
		     $mValue = $oField->sanitize($aData['unit_id']);
		     $oModel->setUnitId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['name_plural'])) {
		     $oField = new NamePlural();
		     $mValue = $oField->sanitize($aData['name_plural']);
		     $oModel->setNamePlural($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
