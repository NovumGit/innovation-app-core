<?php
namespace Crud;

interface IEditableField extends IField {

    function getDataType():string;
    function getFieldName();
    function setEditHtmlStyle($sEditHtmlStyle);
    function getEditHtmlStyle();
}
