<?php
namespace Crud\ProductReview\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductReview\ICollectionField as ProductReviewField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductReview\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductReviewField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductReviewField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductReviewField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductReviewField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductReviewField
	{
		return current($this->aFields);
	}
}
