<?php
namespace Crud\ProductReview\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductReview\FieldIterator;
use Crud\ProductReview\Field\Approved;
use Crud\ProductReview\Field\CreatedDate;
use Crud\ProductReview\Field\CustomerId;
use Crud\ProductReview\Field\Description;
use Crud\ProductReview\Field\FirstName;
use Crud\ProductReview\Field\FromCity;
use Crud\ProductReview\Field\OldId;
use Crud\ProductReview\Field\ProductId;
use Crud\ProductReview\Field\Rating;
use Exception\LogicException;
use Model\Map\ProductReviewTableMap;
use Model\ProductReview;
use Model\ProductReviewQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductReview instead if you need to override or add functionality.
 */
abstract class CrudProductReviewManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductReviewQuery::create();
	}


	public function getTableMap(): ProductReviewTableMap
	{
		return new ProductReviewTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductReview";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_review toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_review aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'OldId', 'CustomerId', 'FirstName', 'FromCity', 'Description', 'Rating', 'CreatedDate', 'Approved'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'OldId', 'CustomerId', 'FirstName', 'FromCity', 'Description', 'Rating', 'CreatedDate', 'Approved'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductReview
	 */
	public function getModel(array $aData = null): ProductReview
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductReviewQuery = ProductReviewQuery::create();
		     $oProductReview = $oProductReviewQuery->findOneById($aData['id']);
		     if (!$oProductReview instanceof ProductReview) {
		         throw new LogicException("ProductReview should be an instance of ProductReview but got something else." . __METHOD__);
		     }
		     $oProductReview = $this->fillVo($aData, $oProductReview);
		}
		else {
		     $oProductReview = new ProductReview();
		     if (!empty($aData)) {
		         $oProductReview = $this->fillVo($aData, $oProductReview);
		     }
		}
		return $oProductReview;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductReview
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductReview
	{
		$oProductReview = $this->getModel($aData);


		 if(!empty($oProductReview))
		 {
		     $oProductReview = $this->fillVo($aData, $oProductReview);
		     $oProductReview->save();
		 }
		return $oProductReview;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductReview $oModel
	 * @return ProductReview
	 */
	protected function fillVo(array $aData, ProductReview $oModel): ProductReview
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['from_city'])) {
		     $oField = new FromCity();
		     $mValue = $oField->sanitize($aData['from_city']);
		     $oModel->setFromCity($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['rating'])) {
		     $oField = new Rating();
		     $mValue = $oField->sanitize($aData['rating']);
		     $oModel->setRating($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		if(isset($aData['approved'])) {
		     $oField = new Approved();
		     $mValue = $oField->sanitize($aData['approved']);
		     $oModel->setApproved($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
