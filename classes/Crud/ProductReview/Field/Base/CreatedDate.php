<?php
namespace Crud\ProductReview\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductReview\ICollectionField;

/**
 * Base class that represents the 'created_date' crud field from the 'product_review' table.
 * This class is auto generated and should not be modified.
 */
abstract class CreatedDate extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'created_date';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCreatedDate';
	protected $sFqModelClassname = '\\\Model\ProductReview';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
