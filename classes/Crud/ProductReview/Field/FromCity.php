<?php
namespace Crud\ProductReview\Field;

use Crud\ProductReview\Field\Base\FromCity as BaseFromCity;

/**
 * Skeleton subclass for representing from_city field from the product_review table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class FromCity extends BaseFromCity
{
}
