<?php
namespace Crud\Product_translation\Field;

use Core\Translate;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Ui\RenderEditConfig;
use InvalidArgumentException;
use Model\ProductTranslation;

class Title extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'title';
    protected $sFieldLabel = 'Titel';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oProductTranslation)
    {
        if(!$oProductTranslation instanceof ProductTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProductTranslation->getTitle().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ProductTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }



        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getTitle(), Translate::fromCode('Translation'),'tag', $bReadonly);
    }
}
