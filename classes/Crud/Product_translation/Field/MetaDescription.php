<?php
namespace Crud\Product_translation\Field;

use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\ProductTranslation;

class MetaDescription extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'meta_description';
    protected $sFieldLabel = 'Meta omschrijving';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oProductTranslation)
    {
        if(!$oProductTranslation instanceof ProductTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProductTranslation->getMetaDescription().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ProductTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return $this->editTextArea($this->getTranslatedTitle(), $this->sFieldName, $mData->getMetaDescription(), $bReadonly);
    }
}
