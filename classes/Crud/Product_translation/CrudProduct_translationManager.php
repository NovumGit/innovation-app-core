<?php
namespace Crud\Product_translation;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use LogicException;

class CrudProduct_translationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'product_translation';
    }
    function getNewFormTitle():string{
        return 'Product vertaling toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Product vertaling bewerken';
    }
    function getOverviewUrl():string
    {
        return '/product/edit?id=xxx&category_id=yyy';
    }
    function getCreateNewUrl():string
    {
        return '/product/edit';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Title',
            'Description'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Title',
            'Description'
        ];
    }

    /**
     * @param $aData
     * @return ProductTranslation
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oProductTranslationQuery = new ProductTranslationQuery();

            $oProductTranslation = $oProductTranslationQuery->findOneById($aData['id']);

            if(!$oProductTranslation instanceof ProductTranslation)
            {
                throw new LogicException("Product should be an instance of ProductTranslation but got ".get_class($oProductTranslation)." in ".__METHOD__);
            }
        }
        else if(isset($aData['product_id']) && isset($aData['language_id']))
        {
            $oProductTranslationQuery = new ProductTranslationQuery();
            $oProductTranslationQuery->filterByLanguageId($aData['language_id']);
            $oProductTranslationQuery->filterByProductId($aData['product_id']);
            $oProductTranslation = $oProductTranslationQuery->findOne();
        }

        if(!isset($oProductTranslation) || !$oProductTranslation instanceof ProductTranslation)
        {
            $oProductTranslation = $this->fillVo(new ProductTranslation(), $aData);
        }
        return $oProductTranslation;
    }

    function store(array $aData = null)
    {
        $oProductTranslation = $this->getModel($aData);

        if(!empty($oProductTranslation))
        {
            $oProductTranslation = $this->fillVo($oProductTranslation, $aData);
            $oProductTranslation->save();

            // ProductHelper::makeDirectoryStructure($oProduct);
        }
        return $oProductTranslation;
    }
    function fillVo(ProductTranslation $oProductTranslation, $aData)
    {

        if(isset($aData['product_id'])){ $oProductTranslation->setProductId($aData['product_id']); }
        if(isset($aData['language_id'])){ $oProductTranslation->setLanguageId($aData['language_id']); }
        if(isset($aData['title'])){ $oProductTranslation->setTitle($aData['title']); }
        if(isset($aData['description'])){ $oProductTranslation->setDescription($aData['description']); }

        if(isset($aData['page_title'])){ $oProductTranslation->setPageTitle($aData['page_title']); }
        if(isset($aData['meta_keywords'])){ $oProductTranslation->setMetaKeywords($aData['meta_keywords']); }
        if(isset($aData['meta_description'])){ $oProductTranslation->setMetaDescription($aData['meta_description']); }


        return $oProductTranslation;
    }
}
