<?php 
namespace Crud\Component_button_page\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Button\Component_button_page;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_page)
		{
		     return "//system/component_button_page/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_page)
		{
		     return "//component_button_page?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
