<?php
namespace Crud\Component_button_page\Field\Base;

use Crud\Component_button_page\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'item_key' crud field from the 'component_button_page' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_key';
	protected $sFieldLabel = 'Page to go to';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemKey';
	protected $sFqModelClassname = '\Model\System\LowCode\Button\Component_button_page';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
