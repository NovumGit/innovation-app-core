<?php
namespace Crud\Component_button_page\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_button_page\ICollectionField as Component_button_pageField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_button_page\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_button_pageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_button_pageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_button_pageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_button_pageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_button_pageField
	{
		return current($this->aFields);
	}
}
