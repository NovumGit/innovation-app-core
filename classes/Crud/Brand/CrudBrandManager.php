<?php

namespace Crud\Brand;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\BrandQuery;
use Model\Setting\MasterTable\Brand;
use Exception\LogicException;

class CrudBrandManager extends FormManager implements IConfigurableCrud
{

    function getEntityTitle(): string
    {
        return 'merk';
    }

    function getOverviewUrl(): string
    {
        return '/setting/mastertable/brand/overview';
    }

    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/brand/edit';
    }

    public function getNewFormTitle(): string
    {
        return 'Merk toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Merk wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Edit',
            'Delete',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
        ];
    }

    public function getModel(array $aData = null)
    {
        if (isset($aData['id']) && $aData['id']) {
            $oBrandQuery = BrandQuery::create();
            $oBrand = $oBrandQuery->findOneById($aData['id']);

            if (!$oBrand instanceof Brand) {
                throw new LogicException("Brand should be an instance of Brand but got " . get_class($oBrand) . " in " . __METHOD__);
            }
        } else {
            $oBrand = new Brand();
            if (!empty($aData)) {
                $oBrand = $this->fillVo($aData, $oBrand);
            }
        }
        return $oBrand;
    }

    public function store(array $aData = null)
    {
        $oBrand = $this->getModel($aData);

        if (!empty($oBrand)) {
            $oBrand = $this->fillVo($aData, $oBrand);
            $oBrand->save();
        }
        return $oBrand;
    }

    private function fillVo($aData, Brand $oBrand)
    {
        if (isset($aData['name'])) {
            $oBrand->setName($aData['name']);
        }

        return $oBrand;
    }
}
