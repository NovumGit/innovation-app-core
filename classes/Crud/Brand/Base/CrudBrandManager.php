<?php
namespace Crud\Brand\Base;

use Core\Utils;
use Crud;
use Crud\Brand\FieldIterator;
use Crud\Brand\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\Brand;
use Model\Setting\MasterTable\BrandQuery;
use Model\Setting\MasterTable\Map\BrandTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Brand instead if you need to override or add functionality.
 */
abstract class CrudBrandManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BrandQuery::create();
	}


	public function getTableMap(): BrandTableMap
	{
		return new BrandTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Brand";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_brand toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_brand aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Brand
	 */
	public function getModel(array $aData = null): Brand
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBrandQuery = BrandQuery::create();
		     $oBrand = $oBrandQuery->findOneById($aData['id']);
		     if (!$oBrand instanceof Brand) {
		         throw new LogicException("Brand should be an instance of Brand but got something else." . __METHOD__);
		     }
		     $oBrand = $this->fillVo($aData, $oBrand);
		}
		else {
		     $oBrand = new Brand();
		     if (!empty($aData)) {
		         $oBrand = $this->fillVo($aData, $oBrand);
		     }
		}
		return $oBrand;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Brand
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Brand
	{
		$oBrand = $this->getModel($aData);


		 if(!empty($oBrand))
		 {
		     $oBrand = $this->fillVo($aData, $oBrand);
		     $oBrand->save();
		 }
		return $oBrand;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Brand $oModel
	 * @return Brand
	 */
	protected function fillVo(array $aData, Brand $oModel): Brand
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
