<?php
namespace Crud\Brand\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Brand\ICollectionField as BrandField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Brand\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BrandField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BrandField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BrandField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BrandField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BrandField
	{
		return current($this->aFields);
	}
}
