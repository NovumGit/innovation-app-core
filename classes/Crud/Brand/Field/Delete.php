<?php

namespace Crud\Brand\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Brand;
use Exception\InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oBrand)
    {

        if (!$oBrand instanceof Brand) {
            throw new InvalidArgumentException('Expected an instance of Brand but got ' . get_class($oBrand));
        }
        return '/setting/mastertable/brand/edit?_do=Delete&id=' . $oBrand->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
