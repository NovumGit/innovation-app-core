<?php

namespace Crud\Brand\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Setting\MasterTable\Brand;

class Edit extends GenericEdit
{

    public function getEditUrl($oBrand)
    {

        if (!$oBrand instanceof Brand) {
            throw new InvalidArgumentException('Expected an instance of Brand but got ' . get_class($oBrand));
        }

        return '/setting/mastertable/brand/edit?id=' . $oBrand->getId();
    }
}
