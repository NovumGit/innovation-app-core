<?php
namespace Crud\ShippingZone\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ShippingZone\FieldIterator;
use Crud\ShippingZone\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ShippingZoneTableMap;
use Model\Setting\MasterTable\ShippingZone;
use Model\Setting\MasterTable\ShippingZoneQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ShippingZone instead if you need to override or add functionality.
 */
abstract class CrudShippingZoneManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ShippingZoneQuery::create();
	}


	public function getTableMap(): ShippingZoneTableMap
	{
		return new ShippingZoneTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ShippingZone";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_shipping_zone toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_shipping_zone aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ShippingZone
	 */
	public function getModel(array $aData = null): ShippingZone
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oShippingZoneQuery = ShippingZoneQuery::create();
		     $oShippingZone = $oShippingZoneQuery->findOneById($aData['id']);
		     if (!$oShippingZone instanceof ShippingZone) {
		         throw new LogicException("ShippingZone should be an instance of ShippingZone but got something else." . __METHOD__);
		     }
		     $oShippingZone = $this->fillVo($aData, $oShippingZone);
		}
		else {
		     $oShippingZone = new ShippingZone();
		     if (!empty($aData)) {
		         $oShippingZone = $this->fillVo($aData, $oShippingZone);
		     }
		}
		return $oShippingZone;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ShippingZone
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ShippingZone
	{
		$oShippingZone = $this->getModel($aData);


		 if(!empty($oShippingZone))
		 {
		     $oShippingZone = $this->fillVo($aData, $oShippingZone);
		     $oShippingZone->save();
		 }
		return $oShippingZone;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ShippingZone $oModel
	 * @return ShippingZone
	 */
	protected function fillVo(array $aData, ShippingZone $oModel): ShippingZone
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
