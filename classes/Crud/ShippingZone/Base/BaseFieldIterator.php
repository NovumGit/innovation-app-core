<?php
namespace Crud\ShippingZone\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ShippingZone\ICollectionField as ShippingZoneField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ShippingZone\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ShippingZoneField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ShippingZoneField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ShippingZoneField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ShippingZoneField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ShippingZoneField
	{
		return current($this->aFields);
	}
}
