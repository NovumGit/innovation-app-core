<?php
namespace Crud\SaleOrderPayment\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderPayment\FieldIterator;
use Crud\SaleOrderPayment\Field\IsPaid;
use Crud\SaleOrderPayment\Field\OurSecretHash;
use Crud\SaleOrderPayment\Field\PayUrl;
use Crud\SaleOrderPayment\Field\PaymentMethodId;
use Crud\SaleOrderPayment\Field\PaymentReference;
use Crud\SaleOrderPayment\Field\SaleOrderId;
use Crud\SaleOrderPayment\Field\StartedOn;
use Crud\SaleOrderPayment\Field\TotalPrice;
use Crud\SaleOrderPayment\Field\TraceReference;
use Crud\SaleOrderPayment\Field\TransactionReference;
use Crud\SaleOrderPayment\Field\Trxid;
use Exception\LogicException;
use Model\Sale\Map\SaleOrderPaymentTableMap;
use Model\Sale\SaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderPayment instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderPaymentManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderPaymentQuery::create();
	}


	public function getTableMap(): SaleOrderPaymentTableMap
	{
		return new SaleOrderPaymentTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderPayment";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_payment toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_payment aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderId', 'PaymentMethodId', 'TotalPrice', 'OurSecretHash', 'Trxid', 'TraceReference', 'PaymentReference', 'TransactionReference', 'PayUrl', 'StartedOn', 'IsPaid'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderId', 'PaymentMethodId', 'TotalPrice', 'OurSecretHash', 'Trxid', 'TraceReference', 'PaymentReference', 'TransactionReference', 'PayUrl', 'StartedOn', 'IsPaid'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderPayment
	 */
	public function getModel(array $aData = null): SaleOrderPayment
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderPaymentQuery = SaleOrderPaymentQuery::create();
		     $oSaleOrderPayment = $oSaleOrderPaymentQuery->findOneById($aData['id']);
		     if (!$oSaleOrderPayment instanceof SaleOrderPayment) {
		         throw new LogicException("SaleOrderPayment should be an instance of SaleOrderPayment but got something else." . __METHOD__);
		     }
		     $oSaleOrderPayment = $this->fillVo($aData, $oSaleOrderPayment);
		}
		else {
		     $oSaleOrderPayment = new SaleOrderPayment();
		     if (!empty($aData)) {
		         $oSaleOrderPayment = $this->fillVo($aData, $oSaleOrderPayment);
		     }
		}
		return $oSaleOrderPayment;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderPayment
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderPayment
	{
		$oSaleOrderPayment = $this->getModel($aData);


		 if(!empty($oSaleOrderPayment))
		 {
		     $oSaleOrderPayment = $this->fillVo($aData, $oSaleOrderPayment);
		     $oSaleOrderPayment->save();
		 }
		return $oSaleOrderPayment;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderPayment $oModel
	 * @return SaleOrderPayment
	 */
	protected function fillVo(array $aData, SaleOrderPayment $oModel): SaleOrderPayment
	{
		if(isset($aData['sale_order_id'])) {
		     $oField = new SaleOrderId();
		     $mValue = $oField->sanitize($aData['sale_order_id']);
		     $oModel->setSaleOrderId($mValue);
		}
		if(isset($aData['payment_method_id'])) {
		     $oField = new PaymentMethodId();
		     $mValue = $oField->sanitize($aData['payment_method_id']);
		     $oModel->setPaymentMethodId($mValue);
		}
		if(isset($aData['total_price'])) {
		     $oField = new TotalPrice();
		     $mValue = $oField->sanitize($aData['total_price']);
		     $oModel->setTotalPrice($mValue);
		}
		if(isset($aData['our_secret_hash'])) {
		     $oField = new OurSecretHash();
		     $mValue = $oField->sanitize($aData['our_secret_hash']);
		     $oModel->setOurSecretHash($mValue);
		}
		if(isset($aData['trxid'])) {
		     $oField = new Trxid();
		     $mValue = $oField->sanitize($aData['trxid']);
		     $oModel->setTrxid($mValue);
		}
		if(isset($aData['trace_reference'])) {
		     $oField = new TraceReference();
		     $mValue = $oField->sanitize($aData['trace_reference']);
		     $oModel->setTraceReference($mValue);
		}
		if(isset($aData['payment_reference'])) {
		     $oField = new PaymentReference();
		     $mValue = $oField->sanitize($aData['payment_reference']);
		     $oModel->setPaymentReference($mValue);
		}
		if(isset($aData['transaction_reference'])) {
		     $oField = new TransactionReference();
		     $mValue = $oField->sanitize($aData['transaction_reference']);
		     $oModel->setTransactionReference($mValue);
		}
		if(isset($aData['pay_url'])) {
		     $oField = new PayUrl();
		     $mValue = $oField->sanitize($aData['pay_url']);
		     $oModel->setPayUrl($mValue);
		}
		if(isset($aData['started_on'])) {
		     $oField = new StartedOn();
		     $mValue = $oField->sanitize($aData['started_on']);
		     $oModel->setStartedOn($mValue);
		}
		if(isset($aData['is_paid'])) {
		     $oField = new IsPaid();
		     $mValue = $oField->sanitize($aData['is_paid']);
		     $oModel->setIsPaid($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
