<?php
namespace Crud\SaleOrderPayment\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderPayment\ICollectionField as SaleOrderPaymentField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderPayment\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderPaymentField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderPaymentField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderPaymentField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderPaymentField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderPaymentField
	{
		return current($this->aFields);
	}
}
