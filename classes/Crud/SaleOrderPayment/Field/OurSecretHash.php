<?php
namespace Crud\SaleOrderPayment\Field;

use Crud\SaleOrderPayment\Field\Base\OurSecretHash as BaseOurSecretHash;

/**
 * Skeleton subclass for representing our_secret_hash field from the sale_order_payment table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class OurSecretHash extends BaseOurSecretHash
{
}
