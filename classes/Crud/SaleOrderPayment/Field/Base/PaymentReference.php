<?php
namespace Crud\SaleOrderPayment\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderPayment\ICollectionField;

/**
 * Base class that represents the 'payment_reference' crud field from the 'sale_order_payment' table.
 * This class is auto generated and should not be modified.
 */
abstract class PaymentReference extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'payment_reference';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPaymentReference';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrderPayment';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
