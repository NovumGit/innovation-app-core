<?php
namespace Crud\ProductImage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductImage\ICollectionField as ProductImageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductImage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductImageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductImageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductImageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductImageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductImageField
	{
		return current($this->aFields);
	}
}
