<?php
namespace Crud\ProductImage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductImage\FieldIterator;
use Crud\ProductImage\Field\FileExt;
use Crud\ProductImage\Field\FileSize;
use Crud\ProductImage\Field\FileType;
use Crud\ProductImage\Field\IsOnline;
use Crud\ProductImage\Field\OriginalName;
use Crud\ProductImage\Field\ProductId;
use Crud\ProductImage\Field\Sorting;
use Crud\ProductImage\Field\SourceUrl;
use Exception\LogicException;
use Model\Product\Image\Map\ProductImageTableMap;
use Model\Product\Image\ProductImage;
use Model\Product\Image\ProductImageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductImage instead if you need to override or add functionality.
 */
abstract class CrudProductImageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductImageQuery::create();
	}


	public function getTableMap(): ProductImageTableMap
	{
		return new ProductImageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductImage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_image toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_image aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'Sorting', 'FileType', 'FileSize', 'IsOnline', 'OriginalName', 'SourceUrl', 'FileExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'Sorting', 'FileType', 'FileSize', 'IsOnline', 'OriginalName', 'SourceUrl', 'FileExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductImage
	 */
	public function getModel(array $aData = null): ProductImage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductImageQuery = ProductImageQuery::create();
		     $oProductImage = $oProductImageQuery->findOneById($aData['id']);
		     if (!$oProductImage instanceof ProductImage) {
		         throw new LogicException("ProductImage should be an instance of ProductImage but got something else." . __METHOD__);
		     }
		     $oProductImage = $this->fillVo($aData, $oProductImage);
		}
		else {
		     $oProductImage = new ProductImage();
		     if (!empty($aData)) {
		         $oProductImage = $this->fillVo($aData, $oProductImage);
		     }
		}
		return $oProductImage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductImage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductImage
	{
		$oProductImage = $this->getModel($aData);


		 if(!empty($oProductImage))
		 {
		     $oProductImage = $this->fillVo($aData, $oProductImage);
		     $oProductImage->save();
		 }
		return $oProductImage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductImage $oModel
	 * @return ProductImage
	 */
	protected function fillVo(array $aData, ProductImage $oModel): ProductImage
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['file_type'])) {
		     $oField = new FileType();
		     $mValue = $oField->sanitize($aData['file_type']);
		     $oModel->setFileType($mValue);
		}
		if(isset($aData['file_size'])) {
		     $oField = new FileSize();
		     $mValue = $oField->sanitize($aData['file_size']);
		     $oModel->setFileSize($mValue);
		}
		if(isset($aData['is_online'])) {
		     $oField = new IsOnline();
		     $mValue = $oField->sanitize($aData['is_online']);
		     $oModel->setIsOnline($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['source_url'])) {
		     $oField = new SourceUrl();
		     $mValue = $oField->sanitize($aData['source_url']);
		     $oModel->setSourceUrl($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
