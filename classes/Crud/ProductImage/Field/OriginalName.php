<?php
namespace Crud\ProductImage\Field;

use Crud\ProductImage\Field\Base\OriginalName as BaseOriginalName;

/**
 * Skeleton subclass for representing original_name field from the product_image table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class OriginalName extends BaseOriginalName
{
}
