<?php
namespace Crud\ProductImage\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductImage\ICollectionField;

/**
 * Base class that represents the 'source_url' crud field from the 'product_image' table.
 * This class is auto generated and should not be modified.
 */
abstract class SourceUrl extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'source_url';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSourceUrl';
	protected $sFqModelClassname = '\\\Model\Product\Image\ProductImage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
