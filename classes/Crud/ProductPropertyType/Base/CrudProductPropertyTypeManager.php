<?php
namespace Crud\ProductPropertyType\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductPropertyType\FieldIterator;
use Exception\LogicException;
use Model\Map\ProductPropertyTypeTableMap;
use Model\ProductPropertyType;
use Model\ProductPropertyTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductPropertyType instead if you need to override or add functionality.
 */
abstract class CrudProductPropertyTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductPropertyTypeQuery::create();
	}


	public function getTableMap(): ProductPropertyTypeTableMap
	{
		return new ProductPropertyTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductPropertyType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_property_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_property_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = [''];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = [''];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductPropertyType
	 */
	public function getModel(array $aData = null): ProductPropertyType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductPropertyTypeQuery = ProductPropertyTypeQuery::create();
		     $oProductPropertyType = $oProductPropertyTypeQuery->findOneById($aData['id']);
		     if (!$oProductPropertyType instanceof ProductPropertyType) {
		         throw new LogicException("ProductPropertyType should be an instance of ProductPropertyType but got something else." . __METHOD__);
		     }
		     $oProductPropertyType = $this->fillVo($aData, $oProductPropertyType);
		}
		else {
		     $oProductPropertyType = new ProductPropertyType();
		     if (!empty($aData)) {
		         $oProductPropertyType = $this->fillVo($aData, $oProductPropertyType);
		     }
		}
		return $oProductPropertyType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductPropertyType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductPropertyType
	{
		$oProductPropertyType = $this->getModel($aData);


		 if(!empty($oProductPropertyType))
		 {
		     $oProductPropertyType = $this->fillVo($aData, $oProductPropertyType);
		     $oProductPropertyType->save();
		 }
		return $oProductPropertyType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductPropertyType $oModel
	 * @return ProductPropertyType
	 */
	protected function fillVo(array $aData, ProductPropertyType $oModel): ProductPropertyType
	{
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
