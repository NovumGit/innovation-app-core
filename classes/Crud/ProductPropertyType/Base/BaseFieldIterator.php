<?php
namespace Crud\ProductPropertyType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductPropertyType\ICollectionField as ProductPropertyTypeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductPropertyType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductPropertyTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductPropertyTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductPropertyTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductPropertyTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductPropertyTypeField
	{
		return current($this->aFields);
	}
}
