<?php
namespace Crud\MigrationScripts\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MigrationScripts\FieldIterator;
use Crud\MigrationScripts\Field\FileContents;
use Crud\MigrationScripts\Field\FileName;
use Exception\LogicException;
use Model\System\Map\MigrationScriptsTableMap;
use Model\System\MigrationScripts;
use Model\System\MigrationScriptsQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MigrationScripts instead if you need to override or add functionality.
 */
abstract class CrudMigrationScriptsManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MigrationScriptsQuery::create();
	}


	public function getTableMap(): MigrationScriptsTableMap
	{
		return new MigrationScriptsTableMap();
	}


	public function getShortDescription(): string
	{
		return "Contains all the scripts that the system has runned in chronological order so repeatable.";
	}


	public function getEntityTitle(): string
	{
		return "MigrationScripts";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "migration_scripts toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "migration_scripts aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['FileName', 'FileContents'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['FileName', 'FileContents'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MigrationScripts
	 */
	public function getModel(array $aData = null): MigrationScripts
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMigrationScriptsQuery = MigrationScriptsQuery::create();
		     $oMigrationScripts = $oMigrationScriptsQuery->findOneById($aData['id']);
		     if (!$oMigrationScripts instanceof MigrationScripts) {
		         throw new LogicException("MigrationScripts should be an instance of MigrationScripts but got something else." . __METHOD__);
		     }
		     $oMigrationScripts = $this->fillVo($aData, $oMigrationScripts);
		}
		else {
		     $oMigrationScripts = new MigrationScripts();
		     if (!empty($aData)) {
		         $oMigrationScripts = $this->fillVo($aData, $oMigrationScripts);
		     }
		}
		return $oMigrationScripts;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MigrationScripts
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MigrationScripts
	{
		$oMigrationScripts = $this->getModel($aData);


		 if(!empty($oMigrationScripts))
		 {
		     $oMigrationScripts = $this->fillVo($aData, $oMigrationScripts);
		     $oMigrationScripts->save();
		 }
		return $oMigrationScripts;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MigrationScripts $oModel
	 * @return MigrationScripts
	 */
	protected function fillVo(array $aData, MigrationScripts $oModel): MigrationScripts
	{
		if(isset($aData['file_name'])) {
		     $oField = new FileName();
		     $mValue = $oField->sanitize($aData['file_name']);
		     $oModel->setFileName($mValue);
		}
		if(isset($aData['file_contents'])) {
		     $oField = new FileContents();
		     $mValue = $oField->sanitize($aData['file_contents']);
		     $oModel->setFileContents($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
