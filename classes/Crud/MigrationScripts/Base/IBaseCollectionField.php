<?php
namespace Crud\MigrationScripts\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\MigrationScripts\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
