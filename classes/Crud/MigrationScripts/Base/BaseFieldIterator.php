<?php
namespace Crud\MigrationScripts\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MigrationScripts\ICollectionField as MigrationScriptsField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MigrationScripts\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MigrationScriptsField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MigrationScriptsField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MigrationScriptsField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MigrationScriptsField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MigrationScriptsField
	{
		return current($this->aFields);
	}
}
