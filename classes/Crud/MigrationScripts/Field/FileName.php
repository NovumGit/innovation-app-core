<?php
namespace Crud\MigrationScripts\Field;

use Crud\MigrationScripts\Field\Base\FileName as BaseFileName;

/**
 * Skeleton subclass for representing file_name field from the migration_scripts table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FileName extends BaseFileName
{
}
