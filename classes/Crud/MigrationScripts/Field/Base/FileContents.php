<?php
namespace Crud\MigrationScripts\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MigrationScripts\ICollectionField;

/**
 * Base class that represents the 'file_contents' crud field from the 'migration_scripts' table.
 * This class is auto generated and should not be modified.
 */
abstract class FileContents extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'file_contents';
	protected $sFieldLabel = 'File content';
	protected $sIcon = 'code';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFileContents';
	protected $sFqModelClassname = '\\\Model\System\MigrationScripts';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
