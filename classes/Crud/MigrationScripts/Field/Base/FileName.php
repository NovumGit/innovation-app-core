<?php
namespace Crud\MigrationScripts\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\MigrationScripts\ICollectionField;

/**
 * Base class that represents the 'file_name' crud field from the 'migration_scripts' table.
 * This class is auto generated and should not be modified.
 */
abstract class FileName extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'file_name';
	protected $sFieldLabel = 'File name';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFileName';
	protected $sFqModelClassname = '\\\Model\System\MigrationScripts';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['file_name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "File name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
