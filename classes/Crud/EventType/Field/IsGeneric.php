<?php
namespace Crud\EventType\Field;

use Crud\EventType\Field\Base\IsGeneric as BaseIsGeneric;

/**
 * Skeleton subclass for representing is_generic field from the event_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class IsGeneric extends BaseIsGeneric
{
}
