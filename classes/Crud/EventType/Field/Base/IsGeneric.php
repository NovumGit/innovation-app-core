<?php
namespace Crud\EventType\Field\Base;

use Crud\EventType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'is_generic' crud field from the 'event_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsGeneric extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'is_generic';
	protected $sFieldLabel = 'Is generic';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsGeneric';
	protected $sFqModelClassname = '\\\Model\System\Event\EventType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['is_generic']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Is generic" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
