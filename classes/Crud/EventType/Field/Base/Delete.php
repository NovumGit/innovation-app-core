<?php 
namespace Crud\EventType\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\Event\EventType;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof EventType)
		{
		     return "//system/event_type/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof EventType)
		{
		     return "//event_type?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
