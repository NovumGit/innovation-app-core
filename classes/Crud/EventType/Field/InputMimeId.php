<?php
namespace Crud\EventType\Field;

use Crud\EventType\Field\Base\InputMimeId as BaseInputMimeId;

/**
 * Skeleton subclass for representing input_mime_id field from the event_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class InputMimeId extends BaseInputMimeId
{
}
