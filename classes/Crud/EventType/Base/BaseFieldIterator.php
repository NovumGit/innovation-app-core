<?php
namespace Crud\EventType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\EventType\ICollectionField as EventTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\EventType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param EventTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param EventTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof EventTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(EventTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): EventTypeField
	{
		return current($this->aFields);
	}
}
