<?php
namespace Crud\EventType\Base;

use Core\Utils;
use Crud;
use Crud\EventType\FieldIterator;
use Crud\EventType\Field\Executable;
use Crud\EventType\Field\InputMimeId;
use Crud\EventType\Field\IsGeneric;
use Crud\EventType\Field\OutputMimeId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\Event\EventType;
use Model\System\Event\EventTypeQuery;
use Model\System\Event\Map\EventTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify EventType instead if you need to override or add functionality.
 */
abstract class CrudEventTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return EventTypeQuery::create();
	}


	public function getTableMap(): EventTypeTableMap
	{
		return new EventTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "In deze tabel zijn alle event typen vastgelegd.";
	}


	public function getEntityTitle(): string
	{
		return "EventType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "event_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "event_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Executable', 'IsGeneric', 'InputMimeId', 'OutputMimeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Executable', 'IsGeneric', 'InputMimeId', 'OutputMimeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return EventType
	 */
	public function getModel(array $aData = null): EventType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oEventTypeQuery = EventTypeQuery::create();
		     $oEventType = $oEventTypeQuery->findOneById($aData['id']);
		     if (!$oEventType instanceof EventType) {
		         throw new LogicException("EventType should be an instance of EventType but got something else." . __METHOD__);
		     }
		     $oEventType = $this->fillVo($aData, $oEventType);
		}
		else {
		     $oEventType = new EventType();
		     if (!empty($aData)) {
		         $oEventType = $this->fillVo($aData, $oEventType);
		     }
		}
		return $oEventType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return EventType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): EventType
	{
		$oEventType = $this->getModel($aData);


		 if(!empty($oEventType))
		 {
		     $oEventType = $this->fillVo($aData, $oEventType);
		     $oEventType->save();
		 }
		return $oEventType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param EventType $oModel
	 * @return EventType
	 */
	protected function fillVo(array $aData, EventType $oModel): EventType
	{
		if(isset($aData['executable'])) {
		     $oField = new Executable();
		     $mValue = $oField->sanitize($aData['executable']);
		     $oModel->setExecutable($mValue);
		}
		if(isset($aData['is_generic'])) {
		     $oField = new IsGeneric();
		     $mValue = $oField->sanitize($aData['is_generic']);
		     $oModel->setIsGeneric($mValue);
		}
		if(isset($aData['input_mime_id'])) {
		     $oField = new InputMimeId();
		     $mValue = $oField->sanitize($aData['input_mime_id']);
		     $oModel->setInputMimeId($mValue);
		}
		if(isset($aData['output_mime_id'])) {
		     $oField = new OutputMimeId();
		     $mValue = $oField->sanitize($aData['output_mime_id']);
		     $oModel->setOutputMimeId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
