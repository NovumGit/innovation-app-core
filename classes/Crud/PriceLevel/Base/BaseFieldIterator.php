<?php
namespace Crud\PriceLevel\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\PriceLevel\ICollectionField as PriceLevelField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\PriceLevel\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PriceLevelField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PriceLevelField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PriceLevelField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PriceLevelField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PriceLevelField
	{
		return current($this->aFields);
	}
}
