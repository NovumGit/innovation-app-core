<?php
namespace Crud\PriceLevel\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\PriceLevel\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
