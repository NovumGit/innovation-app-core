<?php
namespace Crud\PriceLevel\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PriceLevel\FieldIterator;
use Crud\PriceLevel\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\PriceLevelTableMap;
use Model\Setting\MasterTable\PriceLevel;
use Model\Setting\MasterTable\PriceLevelQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PriceLevel instead if you need to override or add functionality.
 */
abstract class CrudPriceLevelManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PriceLevelQuery::create();
	}


	public function getTableMap(): PriceLevelTableMap
	{
		return new PriceLevelTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PriceLevel";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_price_level toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_price_level aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PriceLevel
	 */
	public function getModel(array $aData = null): PriceLevel
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPriceLevelQuery = PriceLevelQuery::create();
		     $oPriceLevel = $oPriceLevelQuery->findOneById($aData['id']);
		     if (!$oPriceLevel instanceof PriceLevel) {
		         throw new LogicException("PriceLevel should be an instance of PriceLevel but got something else." . __METHOD__);
		     }
		     $oPriceLevel = $this->fillVo($aData, $oPriceLevel);
		}
		else {
		     $oPriceLevel = new PriceLevel();
		     if (!empty($aData)) {
		         $oPriceLevel = $this->fillVo($aData, $oPriceLevel);
		     }
		}
		return $oPriceLevel;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PriceLevel
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PriceLevel
	{
		$oPriceLevel = $this->getModel($aData);


		 if(!empty($oPriceLevel))
		 {
		     $oPriceLevel = $this->fillVo($aData, $oPriceLevel);
		     $oPriceLevel->save();
		 }
		return $oPriceLevel;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PriceLevel $oModel
	 * @return PriceLevel
	 */
	protected function fillVo(array $aData, PriceLevel $oModel): PriceLevel
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
