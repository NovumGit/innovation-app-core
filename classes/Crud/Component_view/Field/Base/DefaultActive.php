<?php
namespace Crud\Component_view\Field\Base;

use Crud\Component_view\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'default_active' crud field from the 'component_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class DefaultActive extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'default_active';
	protected $sFieldLabel = 'The name of the view that is active by default';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDefaultActive';
	protected $sFqModelClassname = '\Model\System\LowCode\View\Component_view';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
