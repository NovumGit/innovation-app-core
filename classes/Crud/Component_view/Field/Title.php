<?php
namespace Crud\Component_view\Field;

use Crud\Component_view\Field\Base\Title as BaseTitle;

/**
 * Skeleton subclass for representing title field from the component_view table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Title extends BaseTitle
{
}
