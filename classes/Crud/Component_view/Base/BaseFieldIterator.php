<?php
namespace Crud\Component_view\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_view\ICollectionField as Component_viewField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_view\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_viewField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_viewField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_viewField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_viewField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_viewField
	{
		return current($this->aFields);
	}
}
