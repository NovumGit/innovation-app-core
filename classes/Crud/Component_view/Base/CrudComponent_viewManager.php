<?php
namespace Crud\Component_view\Base;

use Core\Utils;
use Crud;
use Crud\Component_view\FieldIterator;
use Crud\Component_view\Field\DefaultActive;
use Crud\Component_view\Field\Title;
use Crud\Component_view\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\View\Component_view;
use Model\System\LowCode\View\Component_viewQuery;
use Model\System\LowCode\View\Map\Component_viewTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_view instead if you need to override or add functionality.
 */
abstract class CrudComponent_viewManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_viewQuery::create();
	}


	public function getTableMap(): Component_viewTableMap
	{
		return new Component_viewTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van View component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_view";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_view toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_view aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'DefaultActive', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'DefaultActive', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_view
	 */
	public function getModel(array $aData = null): Component_view
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_viewQuery = Component_viewQuery::create();
		     $oComponent_view = $oComponent_viewQuery->findOneById($aData['id']);
		     if (!$oComponent_view instanceof Component_view) {
		         throw new LogicException("Component_view should be an instance of Component_view but got something else." . __METHOD__);
		     }
		     $oComponent_view = $this->fillVo($aData, $oComponent_view);
		}
		else {
		     $oComponent_view = new Component_view();
		     if (!empty($aData)) {
		         $oComponent_view = $this->fillVo($aData, $oComponent_view);
		     }
		}
		return $oComponent_view;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_view
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_view
	{
		$oComponent_view = $this->getModel($aData);


		 if(!empty($oComponent_view))
		 {
		     $oComponent_view = $this->fillVo($aData, $oComponent_view);
		     $oComponent_view->save();
		 }
		return $oComponent_view;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_view $oModel
	 * @return Component_view
	 */
	protected function fillVo(array $aData, Component_view $oModel): Component_view
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['default_active'])) {
		     $oField = new DefaultActive();
		     $mValue = $oField->sanitize($aData['default_active']);
		     $oModel->setDefaultActive($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
