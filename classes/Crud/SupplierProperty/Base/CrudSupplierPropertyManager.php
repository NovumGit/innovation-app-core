<?php
namespace Crud\SupplierProperty\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SupplierProperty\FieldIterator;
use Crud\SupplierProperty\Field\PropertyKey;
use Crud\SupplierProperty\Field\PropertyValue;
use Crud\SupplierProperty\Field\SupplierId;
use Exception\LogicException;
use Model\Supplier\Map\SupplierPropertyTableMap;
use Model\Supplier\SupplierProperty;
use Model\Supplier\SupplierPropertyQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SupplierProperty instead if you need to override or add functionality.
 */
abstract class CrudSupplierPropertyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SupplierPropertyQuery::create();
	}


	public function getTableMap(): SupplierPropertyTableMap
	{
		return new SupplierPropertyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SupplierProperty";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "supplier_property toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "supplier_property aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SupplierId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SupplierId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SupplierProperty
	 */
	public function getModel(array $aData = null): SupplierProperty
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSupplierPropertyQuery = SupplierPropertyQuery::create();
		     $oSupplierProperty = $oSupplierPropertyQuery->findOneById($aData['id']);
		     if (!$oSupplierProperty instanceof SupplierProperty) {
		         throw new LogicException("SupplierProperty should be an instance of SupplierProperty but got something else." . __METHOD__);
		     }
		     $oSupplierProperty = $this->fillVo($aData, $oSupplierProperty);
		}
		else {
		     $oSupplierProperty = new SupplierProperty();
		     if (!empty($aData)) {
		         $oSupplierProperty = $this->fillVo($aData, $oSupplierProperty);
		     }
		}
		return $oSupplierProperty;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SupplierProperty
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SupplierProperty
	{
		$oSupplierProperty = $this->getModel($aData);


		 if(!empty($oSupplierProperty))
		 {
		     $oSupplierProperty = $this->fillVo($aData, $oSupplierProperty);
		     $oSupplierProperty->save();
		 }
		return $oSupplierProperty;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SupplierProperty $oModel
	 * @return SupplierProperty
	 */
	protected function fillVo(array $aData, SupplierProperty $oModel): SupplierProperty
	{
		if(isset($aData['supplier_id'])) {
		     $oField = new SupplierId();
		     $mValue = $oField->sanitize($aData['supplier_id']);
		     $oModel->setSupplierId($mValue);
		}
		if(isset($aData['property_key'])) {
		     $oField = new PropertyKey();
		     $mValue = $oField->sanitize($aData['property_key']);
		     $oModel->setPropertyKey($mValue);
		}
		if(isset($aData['property_value'])) {
		     $oField = new PropertyValue();
		     $mValue = $oField->sanitize($aData['property_value']);
		     $oModel->setPropertyValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
