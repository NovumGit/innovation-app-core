<?php
namespace Crud\SupplierProperty\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SupplierProperty\ICollectionField as SupplierPropertyField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SupplierProperty\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SupplierPropertyField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SupplierPropertyField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SupplierPropertyField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SupplierPropertyField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SupplierPropertyField
	{
		return current($this->aFields);
	}
}
