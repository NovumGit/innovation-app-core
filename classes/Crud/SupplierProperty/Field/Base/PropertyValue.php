<?php
namespace Crud\SupplierProperty\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SupplierProperty\ICollectionField;

/**
 * Base class that represents the 'property_value' crud field from the 'supplier_property' table.
 * This class is auto generated and should not be modified.
 */
abstract class PropertyValue extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'property_value';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPropertyValue';
	protected $sFqModelClassname = '\\\Model\Supplier\SupplierProperty';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
