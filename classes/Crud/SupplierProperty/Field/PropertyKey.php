<?php
namespace Crud\SupplierProperty\Field;

use Crud\SupplierProperty\Field\Base\PropertyKey as BasePropertyKey;

/**
 * Skeleton subclass for representing property_key field from the supplier_property table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class PropertyKey extends BasePropertyKey
{
}
