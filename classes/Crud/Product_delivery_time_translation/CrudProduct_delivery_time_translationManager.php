<?php
namespace Crud\Product_delivery_time_translation;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslation;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery;

class CrudProduct_delivery_time_translationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'levertijd';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/product_delivery_time/translation/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/product_delivery_time/translation/edit';
    }
    function getNewFormTitle():string{
        return 'Levertijd vertaling toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Levertijd vertaling wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name'
        ];
    }

    function getModel(array $aData = null)
    {
        $oProductDeliveryTimeTranslation = null;

        if(isset($aData['id']) && $aData['id'])
        {
            $oProductDeliveryTimeTranslationQuery = new ProductDeliveryTimeTranslationQuery();
            $oProductDeliveryTimeTranslation = $oProductDeliveryTimeTranslationQuery->findOneById($aData['id']);
        }

        if(!$oProductDeliveryTimeTranslation instanceof ProductDeliveryTimeTranslation && isset($aData['language_id']) && isset($aData['delivery_time_id'])){

            $oProductDeliveryTimeQuery = new ProductDeliveryTimeTranslationQuery();
            $oProductDeliveryTimeQuery->filterByLanguageId($aData['language_id']);
            $oProductDeliveryTimeQuery->filterByDeliveryTimeId($aData['delivery_time_id']);
            $oProductDeliveryTimeTranslation = $oProductDeliveryTimeQuery->findOne();
        }

        if(!$oProductDeliveryTimeTranslation instanceof ProductDeliveryTimeTranslation)
        {
            $oProductDeliveryTimeTranslation = new ProductDeliveryTimeTranslation();
        }

        if(!empty($aData))
        {
            $oProductDeliveryTimeTranslation = $this->fillVo($aData, $oProductDeliveryTimeTranslation);
        }

        return $oProductDeliveryTimeTranslation;
    }

    function store(array $aData = null)
    {
        $oProductDeliveryTimeTranslation = $this->getModel($aData);

        if(!empty($oProductDeliveryTimeTranslation))
        {
            $oProductDeliveryTimeTranslation = $this->fillVo($aData, $oProductDeliveryTimeTranslation);

            $oProductDeliveryTimeTranslation->save();
        }
        return $oProductDeliveryTimeTranslation;
    }

    private function fillVo($aData, ProductDeliveryTimeTranslation $oProductDeliveryTimeTranslation)
    {
        if(isset($aData['name'])){$oProductDeliveryTimeTranslation->setName($aData['name']);}
        if(isset($aData['language_id'])){$oProductDeliveryTimeTranslation->setLanguageId($aData['language_id']);}
        if(isset($aData['delivery_time_id'])){$oProductDeliveryTimeTranslation->setDeliveryTimeId($aData['delivery_time_id']);}

        return $oProductDeliveryTimeTranslation;
    }
}
