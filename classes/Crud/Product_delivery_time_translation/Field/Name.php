<?php
namespace Crud\Product_delivery_time_translation\Field;

use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslation;

class Name extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Naam/titel';
    private $sIcon = 'tag';
    private $sPlaceHolder = 'Artikelnummer';

    function getDataType():string
    {
        return 'string';
    }

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(!isset($aPostedData['name']) || empty($aPostedData['name']))
        {
            $mResponse[] = 'Vul een naam/titel in voor deze levertijd.';
        }
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oProductDeliveryTimeTranslation)
    {
        if(!$oProductDeliveryTimeTranslation instanceof ProductDeliveryTimeTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProductDeliveryTimeTranslation->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof ProductDeliveryTimeTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__." got ".get_class($mData));
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
