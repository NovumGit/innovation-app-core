<?php
namespace Crud\ActivityType\Field;

use Crud\ActivityType\Field\Base\ModuleName as BaseModuleName;

/**
 * Skeleton subclass for representing module_name field from the activity_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class ModuleName extends BaseModuleName
{
}
