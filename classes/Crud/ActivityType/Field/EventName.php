<?php
namespace Crud\ActivityType\Field;

use Crud\ActivityType\Field\Base\EventName as BaseEventName;

/**
 * Skeleton subclass for representing event_name field from the activity_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class EventName extends BaseEventName
{
}
