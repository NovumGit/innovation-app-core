<?php
namespace Crud\ActivityType\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\ActivityType\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
