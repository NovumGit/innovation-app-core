<?php
namespace Crud\ActivityType\Base;

use Core\Utils;
use Crud;
use Crud\ActivityType\FieldIterator;
use Crud\ActivityType\Field\EventName;
use Crud\ActivityType\Field\ModuleName;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\ActivityType;
use Model\Setting\MasterTable\ActivityTypeQuery;
use Model\Setting\MasterTable\Map\ActivityTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ActivityType instead if you need to override or add functionality.
 */
abstract class CrudActivityTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ActivityTypeQuery::create();
	}


	public function getTableMap(): ActivityTypeTableMap
	{
		return new ActivityTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ActivityType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "activity_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "activity_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ModuleName', 'EventName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ModuleName', 'EventName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ActivityType
	 */
	public function getModel(array $aData = null): ActivityType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oActivityTypeQuery = ActivityTypeQuery::create();
		     $oActivityType = $oActivityTypeQuery->findOneById($aData['id']);
		     if (!$oActivityType instanceof ActivityType) {
		         throw new LogicException("ActivityType should be an instance of ActivityType but got something else." . __METHOD__);
		     }
		     $oActivityType = $this->fillVo($aData, $oActivityType);
		}
		else {
		     $oActivityType = new ActivityType();
		     if (!empty($aData)) {
		         $oActivityType = $this->fillVo($aData, $oActivityType);
		     }
		}
		return $oActivityType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ActivityType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ActivityType
	{
		$oActivityType = $this->getModel($aData);


		 if(!empty($oActivityType))
		 {
		     $oActivityType = $this->fillVo($aData, $oActivityType);
		     $oActivityType->save();
		 }
		return $oActivityType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ActivityType $oModel
	 * @return ActivityType
	 */
	protected function fillVo(array $aData, ActivityType $oModel): ActivityType
	{
		if(isset($aData['module_name'])) {
		     $oField = new ModuleName();
		     $mValue = $oField->sanitize($aData['module_name']);
		     $oModel->setModuleName($mValue);
		}
		if(isset($aData['event_name'])) {
		     $oField = new EventName();
		     $mValue = $oField->sanitize($aData['event_name']);
		     $oModel->setEventName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
