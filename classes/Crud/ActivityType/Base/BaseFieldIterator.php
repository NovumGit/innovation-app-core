<?php
namespace Crud\ActivityType\Base;

use Crud\ActivityType\ICollectionField as ActivityTypeField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ActivityType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ActivityTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ActivityTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ActivityTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ActivityTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ActivityTypeField
	{
		return current($this->aFields);
	}
}
