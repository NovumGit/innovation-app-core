<?php
namespace Crud\BulkStock\Base;

use Core\Utils;
use Crud;
use Crud\BulkStock\FieldIterator;
use Crud\BulkStock\Field\Name;
use Crud\BulkStock\Field\Quantity;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\BulkStock;
use Model\BulkStockQuery;
use Model\Map\BulkStockTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BulkStock instead if you need to override or add functionality.
 */
abstract class CrudBulkStockManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BulkStockQuery::create();
	}


	public function getTableMap(): BulkStockTableMap
	{
		return new BulkStockTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BulkStock";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bulk_stock toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bulk_stock aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Quantity'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Quantity'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BulkStock
	 */
	public function getModel(array $aData = null): BulkStock
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBulkStockQuery = BulkStockQuery::create();
		     $oBulkStock = $oBulkStockQuery->findOneById($aData['id']);
		     if (!$oBulkStock instanceof BulkStock) {
		         throw new LogicException("BulkStock should be an instance of BulkStock but got something else." . __METHOD__);
		     }
		     $oBulkStock = $this->fillVo($aData, $oBulkStock);
		}
		else {
		     $oBulkStock = new BulkStock();
		     if (!empty($aData)) {
		         $oBulkStock = $this->fillVo($aData, $oBulkStock);
		     }
		}
		return $oBulkStock;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BulkStock
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BulkStock
	{
		$oBulkStock = $this->getModel($aData);


		 if(!empty($oBulkStock))
		 {
		     $oBulkStock = $this->fillVo($aData, $oBulkStock);
		     $oBulkStock->save();
		 }
		return $oBulkStock;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BulkStock $oModel
	 * @return BulkStock
	 */
	protected function fillVo(array $aData, BulkStock $oModel): BulkStock
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['quantity'])) {
		     $oField = new Quantity();
		     $mValue = $oField->sanitize($aData['quantity']);
		     $oModel->setQuantity($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
