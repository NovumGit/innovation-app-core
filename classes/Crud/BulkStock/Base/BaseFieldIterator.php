<?php
namespace Crud\BulkStock\Base;

use Crud\BaseCrudFieldIterator;
use Crud\BulkStock\ICollectionField as BulkStockField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BulkStock\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BulkStockField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BulkStockField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BulkStockField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BulkStockField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BulkStockField
	{
		return current($this->aFields);
	}
}
