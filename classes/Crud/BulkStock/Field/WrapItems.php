<?php
namespace Crud\BulkStock\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Base\BulkStock as ModelObject;

class WrapItems extends Field implements IEventField{

    protected $sFieldName = null;
    protected $sFieldLabel = 'Verpakken';

    function getIcon()
    {
        return 'cubes';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($oData)
    {

        if(!$oData instanceof ModelObject)
        {
            throw new LogicException("Expected an instance of ModelObject, got ".get_class($oData));
        }

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Create packages" href="/stock/bulk/wrap_items?bulk_stock_id='.$oData->getId().'" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
