<?php
namespace Crud\BulkStock\Field;

use Crud\Generic\Field\GenericDelete;
use Model\BulkStock;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof BulkStock)
        {
            throw new InvalidArgumentException('Expected an instance of BulkStock but got '.get_class($oObject));
        }

        return '/stock/bulk/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}