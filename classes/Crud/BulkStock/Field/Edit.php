<?php
namespace Crud\BulkStock\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\BulkStock;

class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof BulkStock)
        {
            throw new InvalidArgumentException('Expected an instance of BulkStock but got '.get_class($oObject));
        }

        return '/stock/bulk/edit?id='.$oObject->getId();
    }
}