<?php
namespace Crud\BulkStock;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\BulkStock;
use Model\BulkStockQuery;
use Propel\Runtime\Exception\LogicException;

class CrudBulkStockManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'bulk_stock';
    }
    function getOverviewUrl():string
    {
        return '/stock/bulk/overview';
    }
    function getCreateNewUrl():string
    {
        return '/stock/bulk/edit';
    }
    function getNewFormTitle():string{
        return 'Bulk voorraadhouder toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Bulk voorraadhouder wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Quantity'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'Quantity'
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oBulkStockQuery = new BulkStockQuery();

            $oBulkStock = $oBulkStockQuery->findOneById($aData['id']);

            if(!$oBulkStock instanceof BulkStock){
                throw new LogicException("BulkStock should be an instance of BulkStock but got ".get_class($oBulkStock)." in ".__METHOD__);
            }
        }
        else
        {
            $oBulkStock = new BulkStock();

            if(!empty($aData))
            {
                $oBulkStock = $this->fillVo($aData, $oBulkStock);
            }
        }
        return $oBulkStock;
    }

    function store(array $aData = null)
    {
        $oBulkStock = $this->getModel($aData);

        if(!empty($oBulkStock))
        {
            $oBulkStock = $this->fillVo($aData, $oBulkStock);
            $oBulkStock->save();
        }
        return $oBulkStock;
    }
    private function fillVo($aData, BulkStock $oBulkStock)
    {
        if(isset($aData['name'])){$oBulkStock->setName($aData['name']);}
        if(isset($aData['quantity'])){$oBulkStock->setQuantity($aData['quantity']);}

        return $oBulkStock;
    }
}
