<?php

namespace Crud\Role;

use Crud\IApiAlwaysExpose;
use Crud\IApiExposable;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\Map\RoleTableMap;
use Model\Setting\MasterTable\RoleQuery;
use Model\Setting\MasterTable\Role;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Exception\LogicException;
use Crud\IConfigurableCrud;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;

class CrudRoleManager extends FormManager implements IEditableCrud, IConfigurableCrud, IApiExposable, IApiAlwaysExpose
{
    function getTableMap(): TableMap
    {
        return new RoleTableMap();
    }

    function getQueryObject(): ModelCriteria
    {
        return RoleQuery::create();
    }

    function getShortDescription(): string
    {
        return 'Each user belongs to a group, on the group level access to modules is defined.';
    }

    function getCreateNewUrl(): string
    {
        return '/setting/user_role/edit';
    }

    public function getOverviewUrl(): string
    {
        return '/setting/user_role/overview';
    }

    public function getEntityTitle(): string
    {
        return 'rol';
    }

    public function getNewFormTitle(): string
    {
        return 'Gebruikersrol toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Gebruikersrol wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Delete',
            'Edit',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {

        return [
            'Name',
        ];
    }

    public function getModel(array $aData = null)
    {
        if ($aData['id']) {
            $oRoleQuery = new RoleQuery();

            $oRole = $oRoleQuery->findOneById($aData['id']);

            if (!$oRole instanceof Role) {
                throw new LogicException("Role should be an instance of Role but got " . get_class($oRole) . " in " . __METHOD__);
            }
        } else {
            $oRole = $this->fillVo(new Role(), $aData);
        }

        return $oRole;
    }

    /**
     * @param $aData
     * @return Role
     * @throws PropelException
     */
    public function store(array $aData = null)
    {
        $oRole = $this->getModel($aData);

        if (!empty($oRole)) {
            $oRole = $this->fillVo($oRole, $aData);
            $oRole->save();
        }
        return $oRole;
    }

    public function fillVo(Role $oRole, $aData)
    {
        if (isset($aData['name'])) {
            $oRole->setName($aData['name']);
        }
        if (isset($aData['is_deletable'])) {
            $oRole->setIsDeletable($aData['is_deletable']);
        }
        return $oRole;
    }
}
