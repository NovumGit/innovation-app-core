<?php

namespace Crud\Role\Field;

use Crud\Field;
use Crud\IEditableField;
use InvalidArgumentException;
use Respect\Validation\Validator;
use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;

class Name extends Field implements IEditableField
{

    protected $sFieldLabel = 'Rol naam';

    function hasValidations()
    {
        return true;
    }

    function getDataType(): string
    {
        return 'integer';
    }

    function validate($aPostedData)
    {
        $mResponse = false;

        $oValidator = Validator::alpha();
        if (empty($aPostedData['id'])) {
            $oRole = RoleQuery::create()->findOneByName($aPostedData['name']);

            if ($oRole instanceof Role) {
                $mResponse[] = 'Er is al een rol met deze naam.';
            }
        }
        $sName = trim($aPostedData['name']);

        if (empty($sName)) {
            $mResponse[] = 'Het rol naam mag niet leeg zijn.';
        } else {
            if ($oValidator->length(2, 75)->validate($aPostedData['name'])) {
                $mResponse[] = 'Het veld rol naam moet minimaal 2 en mag maximaal 75 tekens lang zijn.';
            }
        }

        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField('Rol naam', 'name');
    }

    public function getOverviewValue($oRole)
    {
        if (!$oRole instanceof Role) {
            throw new InvalidArgumentException("Expected an instance of  model\Setting\MasterTable\Role in " . __METHOD__);
        }
        return '<td class="">' . $oRole->getName() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getEditHtml($mData, $bReadonly)
    {

        if (!$mData instanceof Role) {
            throw new InvalidArgumentException("Expected an instance of model\Setting\MasterTable\Role in " . __METHOD__);
        }

        return $this->editTextField('Naam van de gebruikersrol', 'name', $mData->getName(), 'Geef de rol een naam', 'group', false, []);
    }
}
