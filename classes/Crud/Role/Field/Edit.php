<?php

namespace Crud\Role\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\Role;
use InvalidArgumentException;

class Edit extends GenericEdit
{

    public function getEditUrl($oRole)
    {

        if (!$oRole instanceof Role) {
            throw new InvalidArgumentException('Expected an instance of Role but got ' . get_class($oRole));
        }

        return '/setting/user_role/edit?id=' . $oRole->getId();
    }
}
