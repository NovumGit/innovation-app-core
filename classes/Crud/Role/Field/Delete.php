<?php

namespace Crud\Role\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Role;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    function getDeleteUrl($oRole)
    {
        return false;
    }
    function getUnDeleteUrl($oUnused)
    {
        return false;
    }

    function getOverviewValue($oObject)
    {
        if (!$oObject instanceof Role) {
            throw new InvalidArgumentException('Expected an instance of Role but got ' . get_class($oObject));
        }


        $aOut = [];
        $aOut[] = '<td class="">';

        if ($oObject->getIsDeletable()) {
            $aOut[] = '<a href="/setting/user_role/edit?_do=Delete&id=' . $oObject->getId() . '" class="btn btn-danger br2 btn-xs fs12"> <i class="fa fa-trash-o"></i> </a>';
        } else {
            $aOut[] = '<a title="Je kunt de rol ' . $oObject->getName() . ' niet verwijderen." href="#" class="btn btn-default br2 btn-xs fs12 disabled"> <i class="fa fa-trash-o"></i> </a>';
        }

        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }
}
