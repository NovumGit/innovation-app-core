<?php

namespace Crud\Role\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Setting\MasterTable\Role;

class ConfigureModulesButton extends Field implements IEventField
{

    protected $sFieldLabel = 'Klant type';

    public function getIcon()
    {
        return 'gears';
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() . '"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($mData)
    {
        if (!$mData instanceof Role) {
            throw new LogicException("Expected an instance of Customer, got " . get_class($mData));
        }
        $oRole = $mData;

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Modules in- of uitschakelen" href="/admin/module/configure?role_id=' . $oRole->getId() . '" class="btn btn-dark br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-' . $this->getIcon() . '"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly)
    {
        return;
    }
}
