<?php

namespace Crud\Role\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;

class IsDeletable extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'is_deletable';
    protected $sFieldLabel = 'Kan verwijderd worden';

    function hasValidations()
    {
        return true;
    }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getDataType(): string
    {
        return 'boolean';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Kan verwijderd worden', 'is_deletable');
    }
    function getOverviewValue($oRole)
    {
        if (!$oRole instanceof Role) {
            throw new InvalidArgumentException("Expected an instance of  model\\Setting\\MasterTable\\Role in " . __METHOD__);
        }
        $sVisible = $oRole->getIsDeletable() ? 'Ja' : 'Nee';
        return '<td class="">' . $sVisible . '</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Role) {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\Role in " . __METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $mData->getIsDeletable(), $bReadonly);
    }
}
