<?php
namespace Crud\Role\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Role\ICollectionField as RoleField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Role\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param RoleField[] $aFields */
	private array $aFields = [];


	/**
	 * @param RoleField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof RoleField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(RoleField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): RoleField
	{
		return current($this->aFields);
	}
}
