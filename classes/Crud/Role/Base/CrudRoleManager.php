<?php
namespace Crud\Role\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Role\FieldIterator;
use Crud\Role\Field\CreatedOn;
use Crud\Role\Field\IsDeletable;
use Crud\Role\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\RoleTableMap;
use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Role instead if you need to override or add functionality.
 */
abstract class CrudRoleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return RoleQuery::create();
	}


	public function getTableMap(): RoleTableMap
	{
		return new RoleTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Role";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "role toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "role aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'Name', 'IsDeletable'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'Name', 'IsDeletable'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Role
	 */
	public function getModel(array $aData = null): Role
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oRoleQuery = RoleQuery::create();
		     $oRole = $oRoleQuery->findOneById($aData['id']);
		     if (!$oRole instanceof Role) {
		         throw new LogicException("Role should be an instance of Role but got something else." . __METHOD__);
		     }
		     $oRole = $this->fillVo($aData, $oRole);
		}
		else {
		     $oRole = new Role();
		     if (!empty($aData)) {
		         $oRole = $this->fillVo($aData, $oRole);
		     }
		}
		return $oRole;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Role
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Role
	{
		$oRole = $this->getModel($aData);


		 if(!empty($oRole))
		 {
		     $oRole = $this->fillVo($aData, $oRole);
		     $oRole->save();
		 }
		return $oRole;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Role $oModel
	 * @return Role
	 */
	protected function fillVo(array $aData, Role $oModel): Role
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['is_deletable'])) {
		     $oField = new IsDeletable();
		     $mValue = $oField->sanitize($aData['is_deletable']);
		     $oModel->setIsDeletable($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
