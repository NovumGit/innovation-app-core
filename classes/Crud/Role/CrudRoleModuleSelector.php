<?php

namespace Crud\Role;

use Crud\IEditableCrud;

class CrudRoleModuleSelector extends CrudRoleManager implements IEditableCrud
{
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'ConfigureModulesButton',
        ];
    }
}
