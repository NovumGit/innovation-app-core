<?php
namespace Crud\Usage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Usage\ICollectionField as UsageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Usage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UsageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UsageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UsageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UsageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UsageField
	{
		return current($this->aFields);
	}
}
