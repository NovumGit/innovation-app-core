<?php
namespace Crud\Usage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Usage\FieldIterator;
use Crud\Usage\Field\Name;
use Crud\Usage\Field\OldId;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\UsageTableMap;
use Model\Setting\MasterTable\Usage;
use Model\Setting\MasterTable\UsageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Usage instead if you need to override or add functionality.
 */
abstract class CrudUsageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UsageQuery::create();
	}


	public function getTableMap(): UsageTableMap
	{
		return new UsageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Usage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_usage toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_usage aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Usage
	 */
	public function getModel(array $aData = null): Usage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUsageQuery = UsageQuery::create();
		     $oUsage = $oUsageQuery->findOneById($aData['id']);
		     if (!$oUsage instanceof Usage) {
		         throw new LogicException("Usage should be an instance of Usage but got something else." . __METHOD__);
		     }
		     $oUsage = $this->fillVo($aData, $oUsage);
		}
		else {
		     $oUsage = new Usage();
		     if (!empty($aData)) {
		         $oUsage = $this->fillVo($aData, $oUsage);
		     }
		}
		return $oUsage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Usage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Usage
	{
		$oUsage = $this->getModel($aData);


		 if(!empty($oUsage))
		 {
		     $oUsage = $this->fillVo($aData, $oUsage);
		     $oUsage->save();
		 }
		return $oUsage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Usage $oModel
	 * @return Usage
	 */
	protected function fillVo(array $aData, Usage $oModel): Usage
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
