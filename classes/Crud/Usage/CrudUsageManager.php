<?php

namespace Crud\Usage;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\Usage;
use Model\Setting\MasterTable\UsageQuery;
use Exception\LogicException;

class CrudUsageManager extends FormManager implements IConfigurableCrud
{

    function getEntityTitle(): string
    {
        return 'gebruik';
    }

    function getOverviewUrl(): string
    {
        return '/setting/mastertable/usage/overview';
    }

    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/usage/edit';
    }

    public function getNewFormTitle(): string
    {
        return 'Gebruik toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Gebruik wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Edit',
            'Delete',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
        ];
    }

    public function getModel(array $aData = null)
    {
        if (isset($aData['id']) && $aData['id']) {
            $oUsageQuery = UsageQuery::create();
            $oUsage = $oUsageQuery->findOneById($aData['id']);

            if (!$oUsage instanceof Usage) {
                throw new LogicException("Usage should be an instance of Usage but got " . get_class($oUsage) . " in " . __METHOD__);
            }
        } else {
            $oUsage = new Usage();

            if (!empty($aData)) {
                $oUsage = $this->fillVo($aData, $oUsage);
            }
        }

        return $oUsage;
    }

    public function store(array $aData = null)
    {
        $oUsage = $this->getModel($aData);

        if (!empty($oUsage)) {
            $oUsage = $this->fillVo($aData, $oUsage);

            $oUsage->save();
        }
        return $oUsage;
    }

    private function fillVo($aData, Usage $oUsage)
    {
        if (isset($aData['name'])) {
            $oUsage->setName($aData['name']);
        }

        return $oUsage;
    }
}
