<?php

namespace Crud\Usage\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Usage as ModelObject;

class Name extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Naam';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getName';

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function getDataType(): string
    {
        return 'string';
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($oModelObject, $bReadonly)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject, got " . get_class($oModelObject) . " in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
