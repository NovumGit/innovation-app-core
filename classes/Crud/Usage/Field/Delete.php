<?php

namespace Crud\Usage\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Color;
use Exception\InvalidArgumentException;
use Model\Setting\MasterTable\Usage;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oUsage)
    {

        if (!$oUsage instanceof Usage) {
            throw new InvalidArgumentException('Expected an instance of Usage but got ' . get_class($oUsage));
        }
        return '/setting/mastertable/usage/edit?_do=Delete&id=' . $oUsage->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
