<?php

namespace Crud\Usage\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Setting\MasterTable\Usage;

class Edit extends GenericEdit
{

    public function getEditUrl($oUsage)
    {

        if (!$oUsage instanceof Usage) {
            throw new InvalidArgumentException('Expected an instance of Color but got ' . get_class($oUsage));
        }

        return '/setting/mastertable/usage/edit?id=' . $oUsage->getId();
    }
}
