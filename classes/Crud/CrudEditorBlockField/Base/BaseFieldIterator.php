<?php
namespace Crud\CrudEditorBlockField\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudEditorBlockField\ICollectionField as CrudEditorBlockFieldField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudEditorBlockField\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudEditorBlockFieldField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudEditorBlockFieldField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudEditorBlockFieldField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudEditorBlockFieldField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudEditorBlockFieldField
	{
		return current($this->aFields);
	}
}
