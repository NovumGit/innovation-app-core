<?php
namespace Crud\CrudEditorBlockField\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditorBlockField\FieldIterator;
use Crud\CrudEditorBlockField\Field\CrudEditorBlockId;
use Crud\CrudEditorBlockField\Field\Field;
use Crud\CrudEditorBlockField\Field\ReadonlyField;
use Crud\CrudEditorBlockField\Field\Sorting;
use Crud\CrudEditorBlockField\Field\Width;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\Map\CrudEditorBlockFieldTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditorBlockField instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorBlockFieldManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorBlockFieldQuery::create();
	}


	public function getTableMap(): CrudEditorBlockFieldTableMap
	{
		return new CrudEditorBlockFieldTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn alle door gebruikers gedefinieerde knoppen opgeslagen die van toepassing zijn op collecties";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditorBlockField";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor_block_field toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor_block_field aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorBlockId', 'Sorting', 'ReadonlyField', 'Width', 'Field'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorBlockId', 'Sorting', 'ReadonlyField', 'Width', 'Field'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditorBlockField
	 */
	public function getModel(array $aData = null): CrudEditorBlockField
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorBlockFieldQuery = CrudEditorBlockFieldQuery::create();
		     $oCrudEditorBlockField = $oCrudEditorBlockFieldQuery->findOneById($aData['id']);
		     if (!$oCrudEditorBlockField instanceof CrudEditorBlockField) {
		         throw new LogicException("CrudEditorBlockField should be an instance of CrudEditorBlockField but got something else." . __METHOD__);
		     }
		     $oCrudEditorBlockField = $this->fillVo($aData, $oCrudEditorBlockField);
		}
		else {
		     $oCrudEditorBlockField = new CrudEditorBlockField();
		     if (!empty($aData)) {
		         $oCrudEditorBlockField = $this->fillVo($aData, $oCrudEditorBlockField);
		     }
		}
		return $oCrudEditorBlockField;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditorBlockField
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditorBlockField
	{
		$oCrudEditorBlockField = $this->getModel($aData);


		 if(!empty($oCrudEditorBlockField))
		 {
		     $oCrudEditorBlockField = $this->fillVo($aData, $oCrudEditorBlockField);
		     $oCrudEditorBlockField->save();
		 }
		return $oCrudEditorBlockField;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditorBlockField $oModel
	 * @return CrudEditorBlockField
	 */
	protected function fillVo(array $aData, CrudEditorBlockField $oModel): CrudEditorBlockField
	{
		if(isset($aData['crud_editor_block_id'])) {
		     $oField = new CrudEditorBlockId();
		     $mValue = $oField->sanitize($aData['crud_editor_block_id']);
		     $oModel->setCrudEditorBlockId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['readonly_field'])) {
		     $oField = new ReadonlyField();
		     $mValue = $oField->sanitize($aData['readonly_field']);
		     $oModel->setReadonlyField($mValue);
		}
		if(isset($aData['width'])) {
		     $oField = new Width();
		     $mValue = $oField->sanitize($aData['width']);
		     $oModel->setWidth($mValue);
		}
		if(isset($aData['field'])) {
		     $oField = new Field();
		     $mValue = $oField->sanitize($aData['field']);
		     $oModel->setField($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
