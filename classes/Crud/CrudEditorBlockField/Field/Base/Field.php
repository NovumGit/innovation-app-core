<?php
namespace Crud\CrudEditorBlockField\Field\Base;

use Crud\CrudEditorBlockField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'field' crud field from the 'crud_editor_block_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class Field extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'field';
	protected $sFieldLabel = 'Veld';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getField';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorBlockField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['field']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Veld" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
