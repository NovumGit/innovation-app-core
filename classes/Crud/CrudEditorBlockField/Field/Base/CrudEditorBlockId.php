<?php
namespace Crud\CrudEditorBlockField\Field\Base;

use Core\Utils;
use Crud\CrudEditorBlockField\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\Setting\CrudManager\CrudEditorBlockQuery;

/**
 * Base class that represents the 'crud_editor_block_id' crud field from the 'crud_editor_block_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudEditorBlockId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'crud_editor_block_id';
	protected $sFieldLabel = 'Formulier blok';
	protected $sIcon = 'columns';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudEditorBlockId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorBlockField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\CrudEditorBlockQuery::create()->orderBytitle()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "gettitle", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\CrudEditorBlockQuery::create()->findOneById($iItemId)->gettitle();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['crud_editor_block_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Formulier blok" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
