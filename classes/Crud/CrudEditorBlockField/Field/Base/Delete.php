<?php 
namespace Crud\CrudEditorBlockField\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditorBlockField;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorBlockField)
		{
		     return "//system/crud_editor_block_field/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorBlockField)
		{
		     return "//crud_editor_block_field?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
