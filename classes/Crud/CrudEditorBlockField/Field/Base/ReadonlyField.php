<?php
namespace Crud\CrudEditorBlockField\Field\Base;

use Crud\CrudEditorBlockField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'readonly_field' crud field from the 'crud_editor_block_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class ReadonlyField extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'readonly_field';
	protected $sFieldLabel = 'Is read-only';
	protected $sIcon = 'archive';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getReadonlyField';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorBlockField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
