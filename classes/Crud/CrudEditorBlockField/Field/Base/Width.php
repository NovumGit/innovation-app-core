<?php
namespace Crud\CrudEditorBlockField\Field\Base;

use Crud\CrudEditorBlockField\ICollectionField;
use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'width' crud field from the 'crud_editor_block_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class Width extends GenericInteger implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'width';
	protected $sFieldLabel = 'Breedte';
	protected $sIcon = 'arrow-circle-left';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getWidth';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorBlockField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['width']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Breedte" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
