<?php
namespace Crud\CrudEditorBlockField\Field;

use Crud\CrudEditorBlockField\Field\Base\Field as BaseField;

/**
 * Skeleton subclass for representing field field from the crud_editor_block_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Field extends BaseField
{
}
