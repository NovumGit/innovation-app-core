<?php
namespace Crud\CrudEditorBlockField\Field;

use Crud\CrudEditorBlockField\Field\Base\Width as BaseWidth;

/**
 * Skeleton subclass for representing width field from the crud_editor_block_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Width extends BaseWidth
{
}
