<?php
namespace Crud;

use Model\Logging\Except_log;

class CrudViewSorter
{

    static function changeTabOrder(FormManager $oManager, array $aNewSorting):void
    {
        $aCrudViews = CrudViewManager::getViews($oManager);
        if(is_array($aNewSorting))
        {
            foreach($aCrudViews as $oCrudView)
            {
                try
                {
                    $oCrudView->setSorting(array_search($oCrudView->getId(), $aNewSorting));
                    $oCrudView->save();
                }
                catch (\Exception $e)
                {
                    Except_log::register($e, false);
                }

            }
        }
        return;
    }
}