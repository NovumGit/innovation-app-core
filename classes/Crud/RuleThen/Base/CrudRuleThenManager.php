<?php
namespace Crud\RuleThen\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\RuleThen\FieldIterator;
use Crud\RuleThen\Field\RuleId;
use Crud\RuleThen\Field\Title;
use Exception\LogicException;
use Model\Rule\Map\RuleThenTableMap;
use Model\Rule\RuleThen;
use Model\Rule\RuleThenQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify RuleThen instead if you need to override or add functionality.
 */
abstract class CrudRuleThenManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return RuleThenQuery::create();
	}


	public function getTableMap(): RuleThenTableMap
	{
		return new RuleThenTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "RuleThen";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "rule_then toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "rule_then aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RuleId', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RuleId', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return RuleThen
	 */
	public function getModel(array $aData = null): RuleThen
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oRuleThenQuery = RuleThenQuery::create();
		     $oRuleThen = $oRuleThenQuery->findOneById($aData['id']);
		     if (!$oRuleThen instanceof RuleThen) {
		         throw new LogicException("RuleThen should be an instance of RuleThen but got something else." . __METHOD__);
		     }
		     $oRuleThen = $this->fillVo($aData, $oRuleThen);
		}
		else {
		     $oRuleThen = new RuleThen();
		     if (!empty($aData)) {
		         $oRuleThen = $this->fillVo($aData, $oRuleThen);
		     }
		}
		return $oRuleThen;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return RuleThen
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): RuleThen
	{
		$oRuleThen = $this->getModel($aData);


		 if(!empty($oRuleThen))
		 {
		     $oRuleThen = $this->fillVo($aData, $oRuleThen);
		     $oRuleThen->save();
		 }
		return $oRuleThen;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param RuleThen $oModel
	 * @return RuleThen
	 */
	protected function fillVo(array $aData, RuleThen $oModel): RuleThen
	{
		if(isset($aData['rule_id'])) {
		     $oField = new RuleId();
		     $mValue = $oField->sanitize($aData['rule_id']);
		     $oModel->setRuleId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
