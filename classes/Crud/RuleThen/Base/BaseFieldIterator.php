<?php
namespace Crud\RuleThen\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\RuleThen\ICollectionField as RuleThenField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\RuleThen\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param RuleThenField[] $aFields */
	private array $aFields = [];


	/**
	 * @param RuleThenField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof RuleThenField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(RuleThenField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): RuleThenField
	{
		return current($this->aFields);
	}
}
