<?php 
namespace Crud\Component_button_group_size\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\ButtonGroup\Component_button_group_size;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_group_size)
		{
		     return "//system/component_button_group_size/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_group_size)
		{
		     return "//component_button_group_size?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
