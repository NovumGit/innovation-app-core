<?php
namespace Crud\Payment_method;

use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\PaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Propel\Runtime\Exception\LogicException;

class CrudPayment_methodManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getOverviewUrl():string
    {
        return '/setting/mastertable/payment_method/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/payment_method/edit';
    }
    function getEntityTitle():string
    {
        return 'betaalmethode';
    }
    function getNewFormTitle():string{
        return 'Betaalmethode toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Betaalmethode wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Code',
            'IsEnabledWebshop',
            'Delete',
            'Edit'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'Code',
            'IsEnabledWebshop'
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {

            $oPaymentMethodQuery = PaymentMethodQuery::create();

            $oPaymentMethod = $oPaymentMethodQuery->findOneById($aData['id']);

            if(!$oPaymentMethod instanceof PaymentMethod){
                throw new LogicException("PaymentMethod should be an instance of PayTerm but got ".get_class($oPaymentMethod)." in ".__METHOD__);
            }
        }
        else
        {
            $oPaymentMethod = new PaymentMethod();

            if(isset($aData['name']))
            {
                $oPaymentMethod->setName($aData['name']);
            }
            if(isset($aData['code']))
            {
                $oPaymentMethod->setCode($aData['code']);
            }

        }
        return $oPaymentMethod;
    }

    function store(array $aData = null)
    {
        $oPaymentMethod = $this->getModel($aData);

        if(!empty($oPaymentMethod))
        {
            $oPaymentMethod = $this->fillVo($aData, $oPaymentMethod);
            $oPaymentMethod->save();
        }
        return $oPaymentMethod;
    }

    private function fillVo($aData, PaymentMethod $oPaymentMethod):PaymentMethod
    {
        if(isset($aData['psp'])) $oPaymentMethod->setPsp($aData['psp']);
        if(isset($aData['name'])) $oPaymentMethod->setName($aData['name']);
        if(isset($aData['code'])) $oPaymentMethod->setCode($aData['code']);
        if(isset($aData['is_enabled_webshop'])) $oPaymentMethod->setIsEnabledWebshop($aData['is_enabled_webshop']);

        return $oPaymentMethod;
    }
}
