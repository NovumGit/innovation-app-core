<?php
namespace Crud\Payment_method\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PaymentMethod as ModelObject;

use Crud\IFilterableField;
use Crud\IEditableField;

class IsEnabledWebshop extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'is_enabled_webshop';
    protected $sFieldLabel = 'Beschikbaar in webshop';

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'boolean';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField('In webshop', 'is_enabled_webshop');
    }
    function getOverviewValue($oPaymentMethod)
    {
        if(!$oPaymentMethod instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  Model\\Setting\\MasterTable\\PaymentMethod in ".__METHOD__);
        }
        $sEnabledWebshop = $oPaymentMethod->getIsEnabledWebshop() ? 'Ja' : 'Nee';
        return '<td class="">'.$sEnabledWebshop.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of Model\\Setting\\MasterTable\\PaymentMethodRole in ".__METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $mData->getIsEnabledWebshop(), $bReadonly);
    }
}

