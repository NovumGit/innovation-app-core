<?php
namespace Crud\Payment_method\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\PaymentMethod as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oObject));
        }

        return '/setting/mastertable/payment_method/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}