<?php
namespace Crud\Payment_method\Field;

use Core\Translate;
use Crud\Field;
use Crud\IFilterableLookupField;
use InvalidArgumentException;
use Model\Setting\MasterTable\PaymentMethod as ModelObject;

class Psp extends Field implements IFilterableLookupField{

    protected $sFieldName = 'psp';
    protected $sFieldLabel = 'Betaal provider';
    private $sIcon = 'calendar-o ';
    private $sPlaceHolder = '';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function getLookups($mSelectedItem = null)
    {
        $aClasses = glob('../classes/Psp/*');
        $aLookups = [];
        if(!empty($aClasses))
        {
            $aLookups[] = [
                'id' =>  '',
                'label' => Translate::fromCode('Maak een keuze'),
                'selected' => null
            ];


            foreach($aClasses as $sFile)
            {
                $sClassName = str_replace('.php', '', basename($sFile));
                if(strpos($sClassName,'Psp') === 0)
                {
                    $sVisibleName = preg_replace('/^Psp/', '', $sClassName);
                    $aLookups[] = [
                        'id' =>  $sVisibleName,
                        'label' => $sVisibleName,
                        'selected' => ($sVisibleName == $mSelectedItem)
                    ];

                }
            }
        }
        return $aLookups;
    }
    function getVisibleValue($iItemId)
    {
        return $iItemId;
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: ".get_class($oModelObject).__METHOD__);
        }
        return '<td class="">'.$oModelObject->getPsp().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups($mData->getPsp());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getPsp(), $aOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
