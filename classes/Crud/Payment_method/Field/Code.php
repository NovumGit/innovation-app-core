<?php
namespace Crud\Payment_method\Field;

use Crud\Field;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\PaymentMethod as ModelObject;
use Psp\AbstractPsp;

class Code extends Field{

	protected $sFieldName = 'code';
	protected $sFieldLabel = 'Code';
	private $sIcon = 'edit';
	private $sPlaceHolder = '';
	private $sGetter = 'getCode';

	function hasValidations()
	{
		return false;
	}

	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}

    function getLookups($mVisibleValue)
    {

        if($mVisibleValue instanceof ModelObject && $mVisibleValue->getPsp())
        {
            $sClassName = '\\Psp\\Psp'.$mVisibleValue->getPsp();

            $oPsp = new $sClassName;

            if(!$oPsp instanceof AbstractPsp)
            {
                throw new LogicException("Expected an instance of AbstractPsp");
            }
            $aResponse = $oPsp->getPaymethods('nl', 'EUR', 100, new SaleOrder());

            if(!empty($aResponse))
            {
                foreach($aResponse as $iKey => $aLookup)
                {
                    $aLookups[] = [
                        'id' =>  $iKey,
                        'label' => $iKey,
                        'selected' => $iKey == $mVisibleValue->getCode()
                    ];
                }
            }
            else
            {
                $aLookups[] = [
                    'id' =>  '',
                    'label' => 'Geen verdere opties'
                ];
            }
        }

        if(empty($aLookups))
        {
            $aLookups[] = [
                'id' =>  '',
                'label' => 'Kies eerst een Pay provider'
            ];
        }
        return $aLookups;
    }

	function getOverviewHeader()
	{
		return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
	}

	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in got: ".get_class($oModelObject).__METHOD__);
		}
		return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
	}
	function getFieldTitle(){
		return $this->getTranslatedTitle();
	}

	function getEditHtml($mData, $bReadonly)
	{
		if(!$mData instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $this->getLookups($mData), false, $this->sIcon, $this->sPlaceHolder);
	}
}