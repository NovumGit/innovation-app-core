<?php
namespace Crud\Payment_method\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PaymentMethod as ModelObject;

class Name extends Field{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Betaalmethode omschrijving';
    private $sIcon = 'calendar-o ';
    private $sPlaceHolder = '';

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: ".get_class($oModelObject).__METHOD__);
        }
        return '<td class="">'.$oModelObject->getName().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}