<?php
namespace Crud\Payment_method\Field;

use Model\Setting\MasterTable\PaymentMethod as ModelObject;
use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;


class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oObject));
        }

        return '/setting/mastertable/payment_method/edit?id='.$oObject->getId();
    }
}