<?php
namespace Crud\CrudEditorButtonVisibileFilter\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditorButtonVisibileFilter\FieldIterator;
use Crud\CrudEditorButtonVisibileFilter\Field\CrudEditorButtonId;
use Crud\CrudEditorButtonVisibileFilter\Field\EventId;
use Crud\CrudEditorButtonVisibileFilter\Field\FilterName;
use Crud\CrudEditorButtonVisibileFilter\Field\FilterOperatorId;
use Crud\CrudEditorButtonVisibileFilter\Field\FilterValue;
use Crud\CrudEditorButtonVisibileFilter\Field\Sorting;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonVisibileFilterTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditorButtonVisibileFilter instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorButtonVisibileFilterManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonVisibileFilterQuery::create();
	}


	public function getTableMap(): CrudEditorButtonVisibileFilterTableMap
	{
		return new CrudEditorButtonVisibileFilterTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint is opgeslagen welke filters er actief worden als iemand op een knop drukt";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditorButtonVisibileFilter";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor_button_visibile_filter toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor_button_visibile_filter aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorButtonId', 'EventId', 'Sorting', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorButtonId', 'EventId', 'Sorting', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditorButtonVisibileFilter
	 */
	public function getModel(array $aData = null): CrudEditorButtonVisibileFilter
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorButtonVisibileFilterQuery = CrudEditorButtonVisibileFilterQuery::create();
		     $oCrudEditorButtonVisibileFilter = $oCrudEditorButtonVisibileFilterQuery->findOneById($aData['id']);
		     if (!$oCrudEditorButtonVisibileFilter instanceof CrudEditorButtonVisibileFilter) {
		         throw new LogicException("CrudEditorButtonVisibileFilter should be an instance of CrudEditorButtonVisibileFilter but got something else." . __METHOD__);
		     }
		     $oCrudEditorButtonVisibileFilter = $this->fillVo($aData, $oCrudEditorButtonVisibileFilter);
		}
		else {
		     $oCrudEditorButtonVisibileFilter = new CrudEditorButtonVisibileFilter();
		     if (!empty($aData)) {
		         $oCrudEditorButtonVisibileFilter = $this->fillVo($aData, $oCrudEditorButtonVisibileFilter);
		     }
		}
		return $oCrudEditorButtonVisibileFilter;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditorButtonVisibileFilter
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditorButtonVisibileFilter
	{
		$oCrudEditorButtonVisibileFilter = $this->getModel($aData);


		 if(!empty($oCrudEditorButtonVisibileFilter))
		 {
		     $oCrudEditorButtonVisibileFilter = $this->fillVo($aData, $oCrudEditorButtonVisibileFilter);
		     $oCrudEditorButtonVisibileFilter->save();
		 }
		return $oCrudEditorButtonVisibileFilter;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditorButtonVisibileFilter $oModel
	 * @return CrudEditorButtonVisibileFilter
	 */
	protected function fillVo(array $aData, CrudEditorButtonVisibileFilter $oModel): CrudEditorButtonVisibileFilter
	{
		if(isset($aData['crud_editor_button_id'])) {
		     $oField = new CrudEditorButtonId();
		     $mValue = $oField->sanitize($aData['crud_editor_button_id']);
		     $oModel->setCrudEditorButtonId($mValue);
		}
		if(isset($aData['event_id'])) {
		     $oField = new EventId();
		     $mValue = $oField->sanitize($aData['event_id']);
		     $oModel->setEventId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['filter_operator_id'])) {
		     $oField = new FilterOperatorId();
		     $mValue = $oField->sanitize($aData['filter_operator_id']);
		     $oModel->setFilterOperatorId($mValue);
		}
		if(isset($aData['filter_name'])) {
		     $oField = new FilterName();
		     $mValue = $oField->sanitize($aData['filter_name']);
		     $oModel->setFilterName($mValue);
		}
		if(isset($aData['filter_value'])) {
		     $oField = new FilterValue();
		     $mValue = $oField->sanitize($aData['filter_value']);
		     $oModel->setFilterValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
