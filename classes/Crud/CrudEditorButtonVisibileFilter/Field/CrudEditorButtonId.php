<?php
namespace Crud\CrudEditorButtonVisibileFilter\Field;

use Crud\CrudEditorButtonVisibileFilter\Field\Base\CrudEditorButtonId as BaseCrudEditorButtonId;

/**
 * Skeleton subclass for representing crud_editor_button_id field from the crud_editor_button_visibile_filter table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudEditorButtonId extends BaseCrudEditorButtonId
{
}
