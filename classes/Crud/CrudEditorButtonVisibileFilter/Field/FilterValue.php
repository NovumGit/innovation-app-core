<?php
namespace Crud\CrudEditorButtonVisibileFilter\Field;

use Crud\CrudEditorButtonVisibileFilter\Field\Base\FilterValue as BaseFilterValue;

/**
 * Skeleton subclass for representing filter_value field from the crud_editor_button_visibile_filter table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FilterValue extends BaseFilterValue
{
}
