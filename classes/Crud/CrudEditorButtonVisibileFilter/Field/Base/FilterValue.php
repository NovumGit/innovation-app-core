<?php
namespace Crud\CrudEditorButtonVisibileFilter\Field\Base;

use Crud\CrudEditorButtonVisibileFilter\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'filter_value' crud field from the 'crud_editor_button_visibile_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class FilterValue extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'filter_value';
	protected $sFieldLabel = 'Naam';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFilterValue';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['filter_value']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Naam" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
