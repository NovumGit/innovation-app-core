<?php
namespace Crud\CrudEditorButtonVisibileFilter\Field\Base;

use Crud\CrudEditorButtonVisibileFilter\ICollectionField;
use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'sorting' crud field from the 'crud_editor_button_visibile_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class Sorting extends GenericInteger implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sorting';
	protected $sFieldLabel = 'Volgorde';
	protected $sIcon = 'sort';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSorting';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
