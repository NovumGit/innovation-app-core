<?php 
namespace Crud\CrudEditorButtonVisibileFilter\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButtonVisibileFilter)
		{
		     return "//system/crud_editor_button_visibile_filter/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButtonVisibileFilter)
		{
		     return "//crud_editor_button_visibile_filter?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
