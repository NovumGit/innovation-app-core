<?php
namespace Crud\CrudEditorButtonVisibileFilter\Field\Base;

use Core\Utils;
use Crud\CrudEditorButtonVisibileFilter\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\EventQuery;

/**
 * Base class that represents the 'event_id' crud field from the 'crud_editor_button_visibile_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class EventId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'event_id';
	protected $sFieldLabel = 'Event';
	protected $sIcon = 'flash';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEventId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\EventQuery::create()->orderByexecutable()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getexecutable", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\EventQuery::create()->findOneById($iItemId)->getexecutable();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
