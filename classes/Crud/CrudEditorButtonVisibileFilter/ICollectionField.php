<?php
namespace Crud\CrudEditorButtonVisibileFilter;

/**
 * Skeleton interface used for grouping fields that are belong to CrudEditorButtonVisibileFilter.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this interface to meet your application requirements.
 * This interface will only be generated once / when it does not exist already.
 */
interface ICollectionField extends Base\IBaseCollectionField
{
}
