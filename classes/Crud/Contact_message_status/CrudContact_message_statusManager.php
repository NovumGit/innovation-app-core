<?php
namespace Crud\Contact_message_status;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessageStatus;
use Model\ContactMessageStatusQuery;

class CrudContact_message_statusManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'bericht status';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/contact_message_status/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/contact_message_status/edit';
    }
    function getNewFormTitle():string{
        return 'Contact status toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Contact status wijzigen';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name',
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oContactMessageStatusQuery = ContactMessageStatusQuery::create();
            $oContactMessageStatus = $oContactMessageStatusQuery->findOneById($aData['id']);

            if(!$oContactMessageStatus instanceof ContactMessageStatus)
            {
                throw new LogicException("ContactMessageStatus should be an instance of ContactMessageStatus but got ".get_class($oContactMessageStatus)." in ".__METHOD__);
            }
        }
        else
        {
            $oContactMessageStatus = new ContactMessageStatus();
            if(!empty($aData))
            {
                $oContactMessageStatus = $this->fillVo($aData, $oContactMessageStatus);
            }
        }
        return $oContactMessageStatus;
    }

    function store(array $aData = null)
    {
        $oContactMessageStatus = $this->getModel($aData);
        if(!empty($oContactMessageStatus))
        {
            $oContactMessageStatus = $this->fillVo($aData, $oContactMessageStatus);
            $oContactMessageStatus->save();
        }
        return $oContactMessageStatus;
    }

    private function fillVo($aData, ContactMessageStatus $oContactMessageStatus)
    {

        if(isset($aData['name']))
        {
            $oContactMessageStatus->setName($aData['name']);
        }
        return $oContactMessageStatus;
    }
}
