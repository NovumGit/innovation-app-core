<?php
namespace Crud\Contact_message_status\Field;

use Crud\Generic\Field\GenericEdit;
use Model\ContactMessageStatus;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oContactMessageStatus){

        if(!$oContactMessageStatus instanceof ContactMessageStatus)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessageStatus but got '.get_class($oContactMessageStatus));
        }

        return '/setting/mastertable/contact_message_status/edit?id='.$oContactMessageStatus->getId();
    }
}