<?php
namespace Crud\Contact_message_status\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\ContactMessageStatus;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oContactMessageStatus){

        if(!$oContactMessageStatus instanceof ContactMessageStatus)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessageStatus but got '.get_class($oContactMessageStatus));
        }

        return '/setting/mastertable/contact_message_status/edit?_do=Delete&id='.$oContactMessageStatus->getId();
    }

    function getUnDeleteUrl($oContactMessageStatus)
    {
        throw new LogicException("Product delivery time cannot be undeleted");
    }
}