<?php
namespace Crud\Site_blog;

use Core\StatusMessage;
use Core\User;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Cms\SiteBlog;
use Model\Cms\SiteBlogQuery;
use Model\Cms\SiteQuery;

class CrudSite_blogManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'site_blog';
    }
    function getNewFormTitle():string
    {
        return 'Blog toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Blog bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/blog/overview?site='.$_GET['site'];
        }
        return '/cms/blog/overview?site='.$this->getArgument('site');
    }
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/blog/edit?site='.$_GET['site'];
        }
        return '/cms/blog/edit?site='.$this->getArgument('site');
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Title',
            'Content',
            'Edit',
            'Delete'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Tag',
            'Title',
            'Content',
            'Image',
            'SelectedImage'
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SiteBlogQuery();

            $oSiteBlog = $oQuery->findOneById($aData['id']);

            if(!$oSiteBlog instanceof SiteBlog)
            {
                throw new LogicException("SiteBlog should be an instance of Site Blog but got ".get_class($oSiteBlog)." in ".__METHOD__);
            }
        }
        else
        {
            $oSiteBlog = new SiteBlog();
            $oSiteBlog = $this->fillVo($oSiteBlog, $aData);
        }
        return $oSiteBlog;
    }

    function store(array $aData = null)
    {
        $sSite = $this->getArgument('site');
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        $aData['site_id'] = $oSite->getId();
        $oSiteBlog = $this->getModel($aData);

        if(!empty($oSiteBlog))
        {
            $oSiteBlog = $this->fillVo($oSiteBlog, $aData);
            $oSiteBlog->save();
        }
        return $oSiteBlog;
    }
    function delete($iBlogId)
    {
        $oBlogModel = SiteBlogQuery::create()->findOneById($iBlogId);
        $oBlogModel->delete();
        StatusMessage::success("Blog verwijderd.");
    }
    function fillVo(SiteBlog $oSiteBlog, $aData)
    {
        if($oSiteBlog->isNew())
        {
            $oSiteBlog->setCreatedOn(time());
        }

        if(isset($aData['user_id']))
        {
            $oSiteBlog->setCreatedByUserId($aData['user_id']);
        }
        if(isset($aData['site_id']))
        {
            $oSiteBlog->setSiteId($aData['site_id']);
        }
        if(User::isSignedIn())
        {
            if(isset($aData['created_on']))
            {
                $oSiteBlog->setCreatedOn($aData['created_on']);
            }
            $oUser = User::getMember();
            if(isset($oUser))
            {
                $oSiteBlog->setCreatedByUserId($oUser->getId());
            }
        }
        if(isset($aData['teaser']))
        {
            if(strlen($aData['teaser']) > 255)
            {
                $aData['teaser'] = substr($aData['teaser'], 0, 255);
            }
            $oSiteBlog->setTeaser($aData['teaser']);
        }
        if(isset($aData['tag']))
        {
            $oSiteBlog->setTag($aData['tag']);
        }
        if(isset($aData['language_id']))
        {
            $oSiteBlog->setLanguageId($aData['language_id']);
        }
        if(isset($aData['title']))
        {
            $oSiteBlog->setTitle($aData['title']);
        }

        if(isset($aData['content']))
        {
            $oSiteBlog->setContent($aData['content']);
        }
        return $oSiteBlog;
    }
}
