<?php
namespace Crud\Site_blog\Field;

use Crud\Field;

class Image extends Field{

    private $sTitle = 'Afbeelding';
    private $sFieldName = 'in_webshop';
    private $sIcon = 'picture-o';


    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle()
    {
        return $this->sTitle;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sTitle, $this->sFieldName);
    }
    function getOverviewValue($oSiteBlog)
    {
        return false;
    }

    function getEditHtml($mData, $bReadonly)
    {
        return $this->editFileField($this->sTitle, null, null, '', $this->sIcon, $bReadonly);

    }

}

