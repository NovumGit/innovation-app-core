<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_blog\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteBlog as ModelObject;


class Teaser extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'teaser';
    protected $sFieldLabel = 'Teaser';
    private $sGetter = 'getTeaser';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;

        $sTitleP1 = Translate::fromCode('De teaser mag maximaal 255 tekens lang zijn, uw teaser is');
        $sTitleP2 = ' '.(strlen($aPostedData['teaser']) - 255).' ';
        $sTitleP3 = Translate::fromCode("te lang");

        if($aPostedData['teaser'] > 255)
        {
            $mResponse = [$sTitleP1.$sTitleP2.$sTitleP3];
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="fld_teaser" class="col-lg-12 ">Teaser</label>';
        $aOut[] = '    <div class="col-lg-12">';
        $aOut[] = '        <textarea                                
                                class="form-control" 
                                id="fld_teaser"
                                style="height:50px;width:100%;border:1px solid #ddd; border-radius: 3px;" 
                                name="data['.$this->getFieldName().']">'.$mData->getTeaser().'</textarea>';
        $aOut[] = '    <span id="fld_count"></span>';
        $aOut[] = '    <script>';

        $sCharactersLeft = Translate::fromCode('tekens over');

        $aOut[] = '    document.getElementById("fld_teaser").onkeyup = function () {';
        $aOut[] = '         document.getElementById("fld_count").innerHTML = "'.addslashes($sCharactersLeft).': " + (255 - this.value.length);';
        $aOut[] = '    };';
        $aOut[] = '    document.getElementById("fld_count").innerHTML = "'.addslashes($sCharactersLeft).': " + (255 - document.getElementById("fld_teaser").value.length);';
        $aOut[] = '    </script>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}
