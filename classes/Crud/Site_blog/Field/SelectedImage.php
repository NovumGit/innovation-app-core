<?php
namespace Crud\Site_blog\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Model\Cms\SiteBlog;

class SelectedImage extends Field{

    private $sTitle = 'Afbeelding preview';
    private $sFieldName = 'in_webshop';

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        return false;
    }

    function getFieldTitle()
    {
        return $this->sTitle;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sTitle, $this->sFieldName);
    }
    function getOverviewValue($oProductType)
    {
        return false;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SiteBlog){
            throw new InvalidArgumentException("Excected SiteBlog but instead got ".$mData);
        }

        $aHtml = [];
        $aHtml[] = '<div class="form-group">';

        $aHtml[] = '    <label for="product_type" class="col-lg-4 control-label">Afbeelding thumbnail</label>';

        $aHtml[] = '    <div class="col-lg-8">';

        $aHtml[] = '        <div class="input-group">';
        $aHtml[] = $mData->getFileSize() == null?'No image selected':'<img src="/img/blog/'.$mData->getId().'.'.$mData->getFileExt().'" width="200">';
        $aHtml[] = '        </div>';
        $aHtml[] = '    </div>';
        $aHtml[] = '</div>';


        return join(PHP_EOL, $aHtml);
    }

}

