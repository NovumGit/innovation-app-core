<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_blog\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteBlog as ModelObject;


class Content extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'content';
    protected $sFieldLabel = 'Inhoud';
    private $sGetter = 'getContent';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sStyle = isset($_POST['_do']) && $_POST['_do']  == 'EditPlain' ? 'plain' : 'wysiwyg';

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <script>';
        $aOut[] = '    function editToggle()';
        $aOut[] = '    {';
        $aOut[] = '         var doField = document.getElementById("fld_do");';
        if($sStyle == 'plain')
        {
            $aOut[] = '    doField.value = "EditPlain"';
            $sLabel = 'Edit plain text';
            $sType = 'plain';
        }
        else
        {
            $aOut[] = '    doField.value = "EditWysiwyg"';
            $sLabel = 'Edit wysiwyg';
            $sType = 'wysiwyg';
        }
        $aOut[] = '         doField.form.submit()';
        $aOut[] = '         return false;';
        $aOut[] = '    }';
        $aOut[] = '    </script>';
        $aOut[] = '    <label for="fld_{{ field_db }}" class="col-lg-12 ">Blogpost <a href="" onclick="event.preventDefault(); editToggle()">'.$sLabel.'</a></label>';
        $aOut[] = '    <div class="col-lg-12">';


        $aOut[] = '        <textarea 
                                
                                class="form-control '.$sType.'" 
                                style="height:500px;width:100%;border:1px solid #ddd; border-radius: 3px;" 
                                name="data['.$this->getFieldName().']"
                                >'.$mData->getContent().'</textarea>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);


        // return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
        // return $this->editRichTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
    }
}
