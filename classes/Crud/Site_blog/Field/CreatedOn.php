<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_blog\Field;

use Core\Config;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteBlog as ModelObject;


class CreatedOn extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'created_on';
    protected $sFieldLabel = 'Aanmaakdatum';
    private $sGetter = 'getCreatedOn';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCreatedOn = $oModelObject->getCreatedOn();


        $sCreatedOn = '';
        if($oCreatedOn instanceof \DateTime)
        {
            $sCreatedOn = $oModelObject->getCreatedOn()->format(Config::getDateFormat());
        }

        return '<td class="">'.$sCreatedOn.'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sDateTime = null;
        if($mData->getId())
        {
            $sDateTime = $mData->{$this->sGetter}();
        }
        if($sDateTime)
        {
            $oDateTime = new \DateTime();
            $oDateTime = $oDateTime->setTimestamp(strtotime($sDateTime));
        }
        else
        {
            $oDateTime = new \DateTime();
        }

        return $this->editDateTimePicker($this->sFieldLabel, $this->sFieldName, $oDateTime);
    }
}
