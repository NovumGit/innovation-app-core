<?php
namespace Crud\LegalForm\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\LegalForm\FieldIterator;
use Crud\LegalForm\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\LegalForm;
use Model\Setting\MasterTable\LegalFormQuery;
use Model\Setting\MasterTable\Map\LegalFormTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify LegalForm instead if you need to override or add functionality.
 */
abstract class CrudLegalFormManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return LegalFormQuery::create();
	}


	public function getTableMap(): LegalFormTableMap
	{
		return new LegalFormTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "LegalForm";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_legal_form toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_legal_form aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return LegalForm
	 */
	public function getModel(array $aData = null): LegalForm
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oLegalFormQuery = LegalFormQuery::create();
		     $oLegalForm = $oLegalFormQuery->findOneById($aData['id']);
		     if (!$oLegalForm instanceof LegalForm) {
		         throw new LogicException("LegalForm should be an instance of LegalForm but got something else." . __METHOD__);
		     }
		     $oLegalForm = $this->fillVo($aData, $oLegalForm);
		}
		else {
		     $oLegalForm = new LegalForm();
		     if (!empty($aData)) {
		         $oLegalForm = $this->fillVo($aData, $oLegalForm);
		     }
		}
		return $oLegalForm;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return LegalForm
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): LegalForm
	{
		$oLegalForm = $this->getModel($aData);


		 if(!empty($oLegalForm))
		 {
		     $oLegalForm = $this->fillVo($aData, $oLegalForm);
		     $oLegalForm->save();
		 }
		return $oLegalForm;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param LegalForm $oModel
	 * @return LegalForm
	 */
	protected function fillVo(array $aData, LegalForm $oModel): LegalForm
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
