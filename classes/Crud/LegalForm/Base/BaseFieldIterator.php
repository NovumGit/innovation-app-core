<?php
namespace Crud\LegalForm\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\LegalForm\ICollectionField as LegalFormField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\LegalForm\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param LegalFormField[] $aFields */
	private array $aFields = [];


	/**
	 * @param LegalFormField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof LegalFormField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(LegalFormField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): LegalFormField
	{
		return current($this->aFields);
	}
}
