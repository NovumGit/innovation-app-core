<?php

namespace Crud;

use AdminModules\Field\AbstractStyleHelper;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockQuery;
use Model\Setting\CrudManager\CrudEditorQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use ReflectionClass;
use Ui\RenderEditConfig;

class RenderCrudTranslationForm {

    /**
     * @param ModelCriteria $oQueryObject
     * @param FormManager $oManager
     * @param string $sRendering
     * @return null|string
     */
    static public function renderForm(ModelCriteria $oQueryObject, FormManager $oManager, $sRendering = null): string {

        $aTranslatedModels = self::getTranslatedModels($oQueryObject, $oManager);

        if (empty($aTranslatedModels)) {
            return '';
        }
        $aOut = [];

        foreach ($aTranslatedModels as $oTranslatedModel) {
            $aOut[] = self::getRenderedEditHtml($oTranslatedModel, $oManager, $sRendering);
        }
        return join(PHP_EOL, $aOut);
    }

    /**
     * @param ActiveRecordInterface $oData
     * @param FormManager $oManager
     * @return string
     */
    private static function getEditHtml(ActiveRecordInterface $oData, FormManager $oManager) {
        $iLanguageId = null;
        if (method_exists($oData, 'getLanguageId')) {
            $iLanguageId = $oData->getLanguageId();
        } else {
            throw new LogicException("When rendering a crud translation i need an Object that has the getLanguageId method but this one does not.");
        }

        $aEditHtml = [];

        $iId = null;

        if (method_exists($oData, 'getId')) {

            $iId = $oData->getId();
        }

        $sFormTitle = $oManager->getNewFormTitle();
        if ($iId) {
            $sFormTitle = $oManager->getEditFormTitle();
        }

        $aEditHtml[] = '<div class="panel generated">';
        $aEditHtml[] = '    <div class="panel-heading">';
        $aEditHtml[] = '        <span class="panel-title">' . $sFormTitle . '</span>';
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '    <div class="panel-body">';

        $aDefaultEditFields = $oManager->getDefaultEditFields();

        $aCrudFields = $oManager->loadCrudFields($aDefaultEditFields);

        foreach ($aCrudFields as $oCrudField) {
            $oCrudField->setFieldArrayName('data[translation][' . $iLanguageId . ']');
            $aEditHtml[] = $oCrudField->getEditHtml($oData, false);
        }
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';

        return join(PHP_EOL, $aEditHtml);
    }

    private static function getRenderedEditHtml($mData, FormManager $oManager, string $sRendering = null) {
        $iLanguageId = null;
        if (method_exists($mData, 'getLanguageId')) {
            $iLanguageId = $mData->getLanguageId();
        } else {
            throw new LogicException("When rendering a crud translation i need an Object that has the getLanguageId method but this one does not.");
        }

        $sManagerName = get_class($oManager);

        $oCrudEditor = CrudEditorManager::getCrudEditor($sManagerName, $sRendering);

        if ($oCrudEditor === null) {
            return 'De layout van formulier is nog niet geconfigureerd, klik op het bankschroef icoontje rechts boven.<Br>';
            //return self::getEditHtml($mData, $oManager);
        }

        $oCrudEditorBlockQuery = CrudEditorBlockQuery::create();
        $oCrudEditorBlockQuery->filterByCrudEditorId($oCrudEditor->getId());
        $aCrudEditorBlocks = $oCrudEditorBlockQuery->orderBySorting()->find();

        $aEditHtml = [];
        $iId = null;

        if (method_exists($mData, 'getId')) {
            $iId = $mData->getId();
        }

        $sStyleClassName = '\\AdminModules\\Field\\Style1\\Style1Helper';
        $oStyleClassHelper = new $sStyleClassName(new RenderEditConfig());

        if (!$oStyleClassHelper instanceof AbstractStyleHelper) {
            throw new LogicException('$oStyleClassHelper must implement AbstractStyleHelper');
        }

        $aEditHtml[] = $oStyleClassHelper->getEditorHeader();

        $iEditorBlockCount = 0;
        foreach ($aCrudEditorBlocks as $oCrudEditorBlock) {
            $oCurrentBlock = clone $oCrudEditorBlock;
            $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);
            if (!isset($sInitialTitle)) {
                $sInitialTitle = $oCurrentBlock->getTitle();
            }
            $sFlagImg = '<img src="' . $oLanguage->getFlagIconPath(24) . '" />';
            $oCurrentBlock->setTitle($sFlagImg . $sInitialTitle . ' (' . $oLanguage->getDescription() . ')');

            $iEditorBlockCount++;
            $aEditHtml[] = $oStyleClassHelper->getBlockHeader($oCurrentBlock, $iEditorBlockCount);

            $oCrudEditorBlockFieldQuery = CrudEditorBlockFieldQuery::create();
            $oCrudEditorBlockFieldQuery->filterByCrudEditorBlockId($oCurrentBlock->getId());
            $aCrudEditorBlockFields = $oCrudEditorBlockFieldQuery->orderBySorting()->find();

            $aShortNames = [];
            $aBlockFieldsByClass = [];
            foreach ($aCrudEditorBlockFields as $oCrudEditorBlockField) {
                if (!class_exists($oCrudEditorBlockField->getField())) {
                    CrudEditorBlockFieldQuery::create()->findOneById($oCrudEditorBlockField->getId())->delete();
                    throw new LogicException("We konden de klasse " . $oCrudEditorBlockField->getField() . ' niet vinden, het record is automatisch uit het overzicht gehaald, je kan op F5 drukken.');
                }

                $oReflectionClass = new ReflectionClass($oCrudEditorBlockField->getField());
                $sShort = $oReflectionClass->getShortName();
                $aShortNames[] = $sShort;
                $aBlockFieldsByClass[$sShort] = $oCrudEditorBlockField;
            }

            $aCrudFields = $oManager->loadCrudFields($aShortNames);

            foreach ($aCrudFields as $oCrudField) {

                $oReflectionClass = new ReflectionClass($oCrudField);
                $sShort = $oReflectionClass->getShortName();

                if (!isset($aBlockFieldsByClass[$sShort])) {
                    throw new LogicException("Trying to load a class that does not belong to this output, this must be a bug.");
                }
                $oCrudEditorBlockField = $aBlockFieldsByClass[$sShort];

                if (!$oCrudEditorBlockField instanceof CrudEditorBlockField) {
                    throw new LogicException("Expected an instance of CrudEditorBlockField.");
                }

                if (!$oCrudField instanceof Field) {
                    throw new LogicException("Crudfield must be an instance of Field.");
                }

                $oCrudField->setEditHtmlStyle('Style1');
                $oCrudField->setFieldArrayName('data[translation][' . $iLanguageId . ']');
                $aEditHtml[] = $oCrudField->getEditHtml($mData, $oCrudEditorBlockField->isReadonlyField());
            }

            $aEditHtml[] = $oStyleClassHelper->getBlockFooter($iEditorBlockCount);
        }
        return join(PHP_EOL, $aEditHtml);
    }

    private static function getTranslatedModels(ModelCriteria $oQueryObject, FormManager $oManager) {

        $aOut = [];
        $oLanguageQuery = new LanguageQuery();
        $aAllLanguages = $oLanguageQuery->findByIsEnabledWebshopOrIsEnabledCms();

        foreach ($aAllLanguages as $oLanguage) {
            if (!$oLanguage instanceof Language) {
                throw new LogicException("Expected an instance of Language, got " . get_class($oLanguage) . ".");
            } else {
                $oQueryObjectClone = clone $oQueryObject;
                if (method_exists($oQueryObjectClone, 'filterByLanguageId')) {
                    $oQueryObjectClone->filterByLanguageId($oLanguage->getId());
                } else {
                    throw new LogicException("Query object must have a filterByLanguageId method.");
                }

                if (!$oQueryObjectClone instanceof ModelCriteria) {
                    throw new LogicException("Expected an instance of ModelCriteria, got " . get_class($oLanguage) . ".");
                }

                $oObject = $oQueryObjectClone->findOne();

                if ($oObject) {
                    $aOut[] = $oObject;
                } else {
                    $oModel = $oManager->getModel([]);
                    if (method_exists($oModel, 'setLanguageId')) {
                        $oModel->setLanguageId($oLanguage->getId());
                    } else {
                        throw new LogicException("Method setLanguageId is missing on model object.");
                    }
                    $aOut[] = $oModel;
                }
            }
        }
        return $aOut;
    }
}
