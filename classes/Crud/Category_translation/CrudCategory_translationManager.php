<?php
namespace Crud\Category_translation;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Category\CategoryTranslation;
use Model\Category\CategoryTranslationQuery;
use ForceUTF8\Encoding;

class CrudCategory_translationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'category_translation';
    }
    function getOverviewUrl():string
    {
        return'/setting/category/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/category/edit';
    }
    function getNewFormTitle():string{
        return 'Categorie vertaling toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Categorie vertaling wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Description'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'Description',
        ];
    }
    function getModel(array $aData = null)
    {
        $oCategoryTranslation = null;

        if(isset($aData['id']) && $aData['id'])
        {
            $oCategoryTranslation = CategoryTranslationQuery::create()->findOneById($aData['id']);
        }

        if(!$oCategoryTranslation instanceof CategoryTranslation && isset($aData['language_id']) && isset($aData['category_id']))
        {
            $oCategoryTranslationQuery = CategoryTranslationQuery::create();
            $oCategoryTranslationQuery->filterByLanguageId($aData['language_id']);
            $oCategoryTranslationQuery->filterByCategoryId($aData['category_id']);
            $oCategoryTranslation = $oCategoryTranslationQuery->findOne();
        }

        if(!$oCategoryTranslation instanceof CategoryTranslation)
        {
            $oCategoryTranslation = new CategoryTranslation();
        }

        if(!empty($aData))
        {
            $oCategoryTranslation = $this->fillVo($aData, $oCategoryTranslation);
        }

        return $oCategoryTranslation;
    }

    function store(array $aData = null)
    {
        $oCategory = $this->getModel($aData);

        if(!empty($oCategory))
        {
            $oCategory = $this->fillVo($aData, $oCategory);
            $oCategory->save();
        }
        return $oCategory;
    }
    private function fillVo($aData, CategoryTranslation $oCategoryTranslation)
    {
        if(isset($aData['name'])){$oCategoryTranslation->setName($aData['name']);}
        if(isset($aData['meta_description'])){$oCategoryTranslation->setMetaDescription($aData['meta_description']);}
        if(isset($aData['url'])){$oCategoryTranslation->setUrl($aData['url']);}
        if(isset($aData['alt'])){$oCategoryTranslation->setAlt($aData['alt']);}

        if(isset($aData['description'])){$oCategoryTranslation->setDescription($aData['description']);}

        if(isset($aData['language_id'])){$oCategoryTranslation->setLanguageId($aData['language_id']);}
        if(isset($aData['category_id'])){$oCategoryTranslation->setCategoryId($aData['category_id']);}
        return $oCategoryTranslation;
    }
}
