<?php
namespace Crud;

interface IFilterableLookupField extends IFilterableField{

    function getLookups($mSelectedItem = null);
    function getVisibleValue($iItemId);
}
