<?php 
namespace Crud\UIComponentTypeAccepts\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\UI\UIComponentTypeAccepts;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponentTypeAccepts)
		{
		     return "//system/ui_component_type_accepts/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponentTypeAccepts)
		{
		     return "//ui_component_type_accepts?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
