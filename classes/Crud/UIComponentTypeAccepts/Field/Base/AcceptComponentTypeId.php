<?php
namespace Crud\UIComponentTypeAccepts\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\UI\UiComponentTypeQuery;

/**
 * Base class that represents the 'accept_component_type_id' crud field from the 'ui_component_type_accepts' table.
 * This class is auto generated and should not be modified.
 */
abstract class AcceptComponentTypeId extends GenericLookup implements IFilterableField, IEditableField, IFilterableLookupField
{
	protected $sFieldName = 'accept_component_type_id';

	protected $sFieldLabel = 'Child component';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getAcceptComponentTypeId';

	protected $sFqModelClassname = '\Model\System\UI\UIComponentTypeAccepts';


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\UiComponentTypeQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\UiComponentTypeQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
