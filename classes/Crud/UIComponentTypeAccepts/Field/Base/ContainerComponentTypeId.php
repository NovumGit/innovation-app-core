<?php
namespace Crud\UIComponentTypeAccepts\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\UIComponentTypeAccepts\ICollectionField;
use Model\System\UI\UiComponentTypeQuery;

/**
 * Base class that represents the 'container_component_type_id' crud field from the 'ui_component_type_accepts' table.
 * This class is auto generated and should not be modified.
 */
abstract class ContainerComponentTypeId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'container_component_type_id';
	protected $sFieldLabel = 'Container component';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getContainerComponentTypeId';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponentTypeAccepts';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\UiComponentTypeQuery::create()->orderBytitle()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "gettitle", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\UiComponentTypeQuery::create()->findOneById($iItemId)->gettitle();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
