<?php
namespace Crud\UIComponentTypeAccepts\Field;

use Crud\UIComponentTypeAccepts\Field\Base\ContainerComponentTypeId as BaseContainerComponentTypeId;

/**
 * Skeleton subclass for representing container_component_type_id field from the ui_component_type_accepts table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ContainerComponentTypeId extends BaseContainerComponentTypeId
{
}
