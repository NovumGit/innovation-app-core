<?php
namespace Crud\UIComponentTypeAccepts\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UIComponentTypeAccepts\ICollectionField as UIComponentTypeAcceptsField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UIComponentTypeAccepts\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UIComponentTypeAcceptsField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UIComponentTypeAcceptsField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UIComponentTypeAcceptsField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UIComponentTypeAcceptsField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UIComponentTypeAcceptsField
	{
		return current($this->aFields);
	}
}
