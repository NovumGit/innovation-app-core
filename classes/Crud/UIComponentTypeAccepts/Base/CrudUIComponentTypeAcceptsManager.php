<?php
namespace Crud\UIComponentTypeAccepts\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UIComponentTypeAccepts\FieldIterator;
use Crud\UIComponentTypeAccepts\Field\AcceptsComponentTypeId;
use Crud\UIComponentTypeAccepts\Field\ContainerComponentTypeId;
use Exception\LogicException;
use Model\System\UI\Map\UIComponentTypeAcceptsTableMap;
use Model\System\UI\UIComponentTypeAccepts;
use Model\System\UI\UIComponentTypeAcceptsQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UIComponentTypeAccepts instead if you need to override or add functionality.
 */
abstract class CrudUIComponentTypeAcceptsManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UIComponentTypeAcceptsQuery::create();
	}


	public function getTableMap(): UIComponentTypeAcceptsTableMap
	{
		return new UIComponentTypeAcceptsTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint omschrijft wat voor componenten je toe kunt voegen aan dit component.";
	}


	public function getEntityTitle(): string
	{
		return "UIComponentTypeAccepts";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "ui_component_type_accepts toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "ui_component_type_accepts aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ContainerComponentTypeId', 'AcceptsComponentTypeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ContainerComponentTypeId', 'AcceptsComponentTypeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UIComponentTypeAccepts
	 */
	public function getModel(array $aData = null): UIComponentTypeAccepts
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUIComponentTypeAcceptsQuery = UIComponentTypeAcceptsQuery::create();
		     $oUIComponentTypeAccepts = $oUIComponentTypeAcceptsQuery->findOneById($aData['id']);
		     if (!$oUIComponentTypeAccepts instanceof UIComponentTypeAccepts) {
		         throw new LogicException("UIComponentTypeAccepts should be an instance of UIComponentTypeAccepts but got something else." . __METHOD__);
		     }
		     $oUIComponentTypeAccepts = $this->fillVo($aData, $oUIComponentTypeAccepts);
		}
		else {
		     $oUIComponentTypeAccepts = new UIComponentTypeAccepts();
		     if (!empty($aData)) {
		         $oUIComponentTypeAccepts = $this->fillVo($aData, $oUIComponentTypeAccepts);
		     }
		}
		return $oUIComponentTypeAccepts;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UIComponentTypeAccepts
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UIComponentTypeAccepts
	{
		$oUIComponentTypeAccepts = $this->getModel($aData);


		 if(!empty($oUIComponentTypeAccepts))
		 {
		     $oUIComponentTypeAccepts = $this->fillVo($aData, $oUIComponentTypeAccepts);
		     $oUIComponentTypeAccepts->save();
		 }
		return $oUIComponentTypeAccepts;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UIComponentTypeAccepts $oModel
	 * @return UIComponentTypeAccepts
	 */
	protected function fillVo(array $aData, UIComponentTypeAccepts $oModel): UIComponentTypeAccepts
	{
		if(isset($aData['container_component_type_id'])) {
		     $oField = new ContainerComponentTypeId();
		     $mValue = $oField->sanitize($aData['container_component_type_id']);
		     $oModel->setContainerComponentTypeId($mValue);
		}
		if(isset($aData['accepts_component_type_id'])) {
		     $oField = new AcceptsComponentTypeId();
		     $mValue = $oField->sanitize($aData['accepts_component_type_id']);
		     $oModel->setAcceptsComponentTypeId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
