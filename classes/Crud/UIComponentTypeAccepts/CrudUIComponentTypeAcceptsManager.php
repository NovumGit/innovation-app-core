<?php
namespace Crud\UIComponentTypeAccepts;

/**
 * Skeleton subclass for representing a UIComponentTypeAccepts.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudUIComponentTypeAcceptsManager extends Base\CrudUIComponentTypeAcceptsManager
{
}
