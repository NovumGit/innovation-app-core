<?php
namespace Crud\UniqueFieldGroup\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UniqueFieldGroup\ICollectionField as UniqueFieldGroupField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UniqueFieldGroup\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UniqueFieldGroupField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UniqueFieldGroupField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UniqueFieldGroupField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UniqueFieldGroupField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UniqueFieldGroupField
	{
		return current($this->aFields);
	}
}
