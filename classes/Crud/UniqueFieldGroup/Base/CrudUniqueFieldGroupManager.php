<?php
namespace Crud\UniqueFieldGroup\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UniqueFieldGroup\FieldIterator;
use Crud\UniqueFieldGroup\Field\DataModelId;
use Crud\UniqueFieldGroup\Field\Name;
use Exception\LogicException;
use Model\System\DataModel\Model\Map\UniqueFieldGroupTableMap;
use Model\System\DataModel\Model\UniqueFieldGroup;
use Model\System\DataModel\Model\UniqueFieldGroupQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UniqueFieldGroup instead if you need to override or add functionality.
 */
abstract class CrudUniqueFieldGroupManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UniqueFieldGroupQuery::create();
	}


	public function getTableMap(): UniqueFieldGroupTableMap
	{
		return new UniqueFieldGroupTableMap();
	}


	public function getShortDescription(): string
	{
		return "Groep van velden die samen uniek moeten zijn.";
	}


	public function getEntityTitle(): string
	{
		return "UniqueFieldGroup";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "unique_field_group toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "unique_field_group aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'DataModelId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'DataModelId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UniqueFieldGroup
	 */
	public function getModel(array $aData = null): UniqueFieldGroup
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUniqueFieldGroupQuery = UniqueFieldGroupQuery::create();
		     $oUniqueFieldGroup = $oUniqueFieldGroupQuery->findOneById($aData['id']);
		     if (!$oUniqueFieldGroup instanceof UniqueFieldGroup) {
		         throw new LogicException("UniqueFieldGroup should be an instance of UniqueFieldGroup but got something else." . __METHOD__);
		     }
		     $oUniqueFieldGroup = $this->fillVo($aData, $oUniqueFieldGroup);
		}
		else {
		     $oUniqueFieldGroup = new UniqueFieldGroup();
		     if (!empty($aData)) {
		         $oUniqueFieldGroup = $this->fillVo($aData, $oUniqueFieldGroup);
		     }
		}
		return $oUniqueFieldGroup;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UniqueFieldGroup
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UniqueFieldGroup
	{
		$oUniqueFieldGroup = $this->getModel($aData);


		 if(!empty($oUniqueFieldGroup))
		 {
		     $oUniqueFieldGroup = $this->fillVo($aData, $oUniqueFieldGroup);
		     $oUniqueFieldGroup->save();
		 }
		return $oUniqueFieldGroup;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UniqueFieldGroup $oModel
	 * @return UniqueFieldGroup
	 */
	protected function fillVo(array $aData, UniqueFieldGroup $oModel): UniqueFieldGroup
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['data_model_id'])) {
		     $oField = new DataModelId();
		     $mValue = $oField->sanitize($aData['data_model_id']);
		     $oModel->setDataModelId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
