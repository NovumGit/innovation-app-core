<?php
namespace Crud\UniqueFieldGroup\Field;

use Crud\UniqueFieldGroup\Field\Base\Name as BaseName;

/**
 * Skeleton subclass for representing name field from the unique_field_group table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Mon 12:56:38
 */
final class Name extends BaseName
{
}
