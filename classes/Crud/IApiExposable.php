<?php
namespace Crud;

use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

interface IApiExposable extends IFormManager
{
    function getShortDescription():string;
    function getQueryObject():ModelCriteria;
    function getTableMap():TableMap;
    function getModuleName():string ;
    function getApiEndpoint():string;
    function getAllFieldsAsArray(callable $filterFunction = null):?array;
}
