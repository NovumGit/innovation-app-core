<?php
namespace Crud;

use Core\Logger;
use Core\StatusMessage;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use LogicException;
use Propel\Runtime\Map\TableMap;

class CrudViewManager
{
    /**
     * Deze functie kijkt of er voor dit type crud al wat is geconfigureerd in de database.
     * Als dat het niet geval blijkt dan maakt het systeem een standaard configuratie voor een tabje aan.
     *
     * @param BaseManager $oCrudManager
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    static function init(BaseManager $oCrudManager):void
    {
        Logger::custom('CrudViewManager->init: ' . $oCrudManager->getCrudLayoutKey(), Logger::getLogdir().'/crudview');

        $oCrudConfig = CrudConfigManager::getOrCreate($oCrudManager);

        if($oCrudManager->getCrudLayoutKey())
        {
            self::findOrCreateByLayoutKey($oCrudManager, $oCrudManager->getCrudLayoutKey());
        }
        else
        {
            self::createViewIfNoneExists($oCrudManager);
        }

        if ($oCrudConfig instanceof CrudConfig) {
            return;
        }
    }

    /**
     * @param IBaseManager $oCrudManager
     * @param array $aOptions
     * @return CrudView
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function createViewIfNoneExists(IBaseManager $oCrudManager, array $aOptions = null):CrudView
    {
        $oCrudConfig = CrudConfigManager::getOrCreate($oCrudManager);
        $oCrudViewQuery = CrudViewQuery::create();
        if(isset($aOptions['code']))
        {
            $oCrudViewQuery->filterByCode($aOptions['code']);
        }
        $oCrudViewQuery->filterByCrudConfigId($oCrudConfig->getId());
        $oCrudView = $oCrudViewQuery->findOneOrCreate();

        if($oCrudView->isNew())
        {
            StatusMessage::success("Nieuw default overzicht aangemaakt.");

            if(isset($aOptions['code']))
            {
                $oCrudView->setCode($aOptions['code']);
            }
            if(isset($aOptions['init_hidden']))
            {
                $oCrudView->setIsHidden(true);
            }
            else
            {
                $oCrudView->setIsHidden(false);
            }
            $oCrudView->setDefaultOrderBy('id');
            $oCrudView->setDefaultOrderDir('desc');
            $oCrudView->setSorting(0);
            // Id needed in next step
            $oCrudView->save();

            $oCrudView->setFields($oCrudManager->getDefaultOverviewFields());
            $oCrudView->save();
        }
        return $oCrudView;
    }

    /**
     * Tries to load a CrudView by code, if it does not exist it creates one and sets the code. If no fields are
     *  specified it will add the default overview fields as stored in the ManagerClass to the view.
     *
     * @param BaseManager $oCrudManager
     * @param string $sCode
     * @param array $aFieldsOnCreation
     *
     * @return CrudView
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    static function findOrCreateByLayoutKey(IBaseManager $oCrudManager, string $sCode, array $aFieldsOnCreation = null):CrudView
    {
        Logger::custom('CrudViewManager->findOrCreateByCode: ' . $sCode, Logger::getLogdir().'/crudview');
        $oCrudConfig = CrudConfigManager::getOrCreate($oCrudManager);
        $oCrudViewQuery = CrudViewQuery::create();
        $oCrudViewQuery->filterByCode($sCode);
        $oCrudViewQuery->filterByCrudConfigId($oCrudConfig->getId());

        $oCrudView = $oCrudViewQuery->findOneOrCreate();

        if($oCrudView->isNew())
        {

            $oCrudView->setSorting(0);
            $oCrudView->setDefaultOrderBy('id');
            $oCrudView->setDefaultOrderDir('desc');

            if(!empty($aFieldsOnCreation))
            {
                $oCrudView->fromArray($aFieldsOnCreation);
            }
            else if(is_iterable($oCrudManager->getDefaultOverviewFields()))
            {
                $oCrudView->save();

                $oCrudView->setFields($oCrudManager->getDefaultOverviewFields());
            }
            StatusMessage::success("Nieuw overzicht \"$sCode\" aangemaakt.");
            $oCrudView->save();
        }


        return $oCrudView;
    }

    /**
     * If $iCrudViewId is not passed this will always create a new view!!
     *
     * @param BaseManager $oCrudManager
     * @param $sCrudViewTabTitle
     * @param $aFields
     * @param null $iCrudViewId
     * @param array $aArguments allows you to configure the CrudView
     * @return $iCrudViewId
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    static function createOrEditView(BaseManager $oCrudManager, $sCrudViewTabTitle, $aFields, $iCrudViewId = null, $aArguments = []):int
    {
        Logger::custom('CrudViewManager->createOrEditView: ' . $oCrudManager->getCrudLayoutKey(), Logger::getLogdir().'/crudview');

        $oCrudConfig = CrudConfigManager::getOrCreate($oCrudManager);

        $oCrudView = ($iCrudViewId) ? CrudViewQuery::create()->findOneById($iCrudViewId) :  new CrudView();
        $oCrudView->setName($sCrudViewTabTitle);
        $oCrudView->setCrudConfigId($oCrudConfig->getId());

        if(!$oCrudView->getSorting())
        {
            $oCrudView->setSorting(0);
        }


        if(!empty($aArguments))
        {
            $oCrudView->fromArray($aArguments, TableMap::TYPE_FIELDNAME);
        }

        $oCrudView->save();

        if(is_iterable($aFields))
        {
            $oCrudView->setFields($aFields);
        }
        return $oCrudView->getId();
    }


    /**
     * @param int $iCrudViewId
     * @return \Crud\Field[]
     */
    public static function getFieldsByCrudViewId(int $iCrudViewId):array
    {
        $oCrudView = CrudViewQuery::create()->findOneById($iCrudViewId);
        return $oCrudView->getFields();
    }

    /**
     * @param CrudView $oCrudView
     * @return CrudViewField[]
     * @deprecated use CrudView->getFields() instead
     */
    public static function getFields(CrudView $oCrudView)
    {
        return $oCrudView->getFields();
    }

    public static function getViewByCode(FormManager $oCrudManager, $sCode = null):?CrudView
    {
        $oCrudViewQuery = CrudViewQuery::create();
        $oCrudViewQuery->
        useCrudConfigQuery()
            ->filterByManagerName($oCrudManager->getCrudLayoutKey())
            ->endUse();

        $oCrudViewQuery->filterByCode($sCode);
        return $oCrudViewQuery->findOne();
    }
    /**
     * @param FormManager $oCrudManager
     * @param null $iTabId
     * @return CrudView
     */
    public static function getView(FormManager $oCrudManager, $iTabId = null):CrudView
    {
        $aCrudViews = self::getViews($oCrudManager, true);

        if(!$iTabId)
        {
            $oCrudView = current($aCrudViews);
            if(!$oCrudView instanceof CrudView)
            {
                $sWhat = '';
                if(is_object($oCrudView))
                {
                    $sWhat = get_class($oCrudView);
                }
                else if(is_string($oCrudView))
                {
                    $sWhat = $oCrudView;
                }
                throw new LogicException("Was looking for a CrudView but got \"$sWhat\"");
            }
        }
        else
        {
            $oCrudView = $aCrudViews[$iTabId];
        }
        return $oCrudView;
    }

    /**
     * Geeft alle geconfigureerde overzichten terug voor een crud
     * @param FormManager $oCrudManager
     * @param bool $bIncludeHiddenViews
     * @return \model\Setting\CrudManager\CrudView[]|\Propel\Runtime\Collection\ObjectCollection
     */
    public static function getViews(FormManager $oCrudManager, $bIncludeHiddenViews = false)
    {
        $oCrudConfigQuery = CrudConfigQuery::create();
        $CrudConfig = $oCrudConfigQuery->findOneByManagerName($oCrudManager->getCrudLayoutKey());

        $oCrudViewQuery = CrudViewQuery::create();
        $oCrudViewQuery->orderBySorting();

        if(!$bIncludeHiddenViews)
        {
            $oCrudViewQuery->filterByIsHidden(false);
        }
        $aCrudViews = $oCrudViewQuery->findByCrudConfigId($CrudConfig->getId());



        $aCrudViewsById = [];

        foreach($aCrudViews as $oCrudView)
        {
            $aCrudViewsById[$oCrudView->getId()] = $oCrudView;
        }
        return $aCrudViewsById;
    }
}
