<?php
namespace Crud\Component_button_group\Base;

use Core\Utils;
use Crud;
use Crud\Component_button_group\FieldIterator;
use Crud\Component_button_group\Field\AsDropdown;
use Crud\Component_button_group\Field\DefaultActive;
use Crud\Component_button_group\Field\FkComponentButtonGroupRole;
use Crud\Component_button_group\Field\FkComponentButtonGroupSize;
use Crud\Component_button_group\Field\Icon;
use Crud\Component_button_group\Field\Label;
use Crud\Component_button_group\Field\Title;
use Crud\Component_button_group\Field\Tooltip;
use Crud\Component_button_group\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\ButtonGroup\Component_button_group;
use Model\System\LowCode\ButtonGroup\Component_button_groupQuery;
use Model\System\LowCode\ButtonGroup\Map\Component_button_groupTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_button_group instead if you need to override or add functionality.
 */
abstract class CrudComponent_button_groupManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_button_groupQuery::create();
	}


	public function getTableMap(): Component_button_groupTableMap
	{
		return new Component_button_groupTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Button group component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_button_group";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_button_group toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_button_group aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'FkComponentButtonGroupSize', 'FkComponentButtonGroupRole', 'Icon', 'Label', 'Tooltip', 'AsDropdown', 'DefaultActive', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'FkComponentButtonGroupSize', 'FkComponentButtonGroupRole', 'Icon', 'Label', 'Tooltip', 'AsDropdown', 'DefaultActive', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_button_group
	 */
	public function getModel(array $aData = null): Component_button_group
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_button_groupQuery = Component_button_groupQuery::create();
		     $oComponent_button_group = $oComponent_button_groupQuery->findOneById($aData['id']);
		     if (!$oComponent_button_group instanceof Component_button_group) {
		         throw new LogicException("Component_button_group should be an instance of Component_button_group but got something else." . __METHOD__);
		     }
		     $oComponent_button_group = $this->fillVo($aData, $oComponent_button_group);
		}
		else {
		     $oComponent_button_group = new Component_button_group();
		     if (!empty($aData)) {
		         $oComponent_button_group = $this->fillVo($aData, $oComponent_button_group);
		     }
		}
		return $oComponent_button_group;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_button_group
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_button_group
	{
		$oComponent_button_group = $this->getModel($aData);


		 if(!empty($oComponent_button_group))
		 {
		     $oComponent_button_group = $this->fillVo($aData, $oComponent_button_group);
		     $oComponent_button_group->save();
		 }
		return $oComponent_button_group;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_button_group $oModel
	 * @return Component_button_group
	 */
	protected function fillVo(array $aData, Component_button_group $oModel): Component_button_group
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['fk_component_button_group_size'])) {
		     $oField = new FkComponentButtonGroupSize();
		     $mValue = $oField->sanitize($aData['fk_component_button_group_size']);
		     $oModel->setFkComponentButtonGroupSize($mValue);
		}
		if(isset($aData['fk_component_button_group_role'])) {
		     $oField = new FkComponentButtonGroupRole();
		     $mValue = $oField->sanitize($aData['fk_component_button_group_role']);
		     $oModel->setFkComponentButtonGroupRole($mValue);
		}
		if(isset($aData['icon'])) {
		     $oField = new Icon();
		     $mValue = $oField->sanitize($aData['icon']);
		     $oModel->setIcon($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		if(isset($aData['tooltip'])) {
		     $oField = new Tooltip();
		     $mValue = $oField->sanitize($aData['tooltip']);
		     $oModel->setTooltip($mValue);
		}
		if(isset($aData['as_dropdown'])) {
		     $oField = new AsDropdown();
		     $mValue = $oField->sanitize($aData['as_dropdown']);
		     $oModel->setAsDropdown($mValue);
		}
		if(isset($aData['default_active'])) {
		     $oField = new DefaultActive();
		     $mValue = $oField->sanitize($aData['default_active']);
		     $oModel->setDefaultActive($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
