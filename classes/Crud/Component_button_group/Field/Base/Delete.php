<?php 
namespace Crud\Component_button_group\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\ButtonGroup\Component_button_group;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_group)
		{
		     return "//system/component_button_group/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_button_group)
		{
		     return "//component_button_group?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
