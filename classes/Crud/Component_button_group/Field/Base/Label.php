<?php
namespace Crud\Component_button_group\Field\Base;

use Crud\Component_button_group\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'label' crud field from the 'component_button_group' table.
 * This class is auto generated and should not be modified.
 */
abstract class Label extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'label';
	protected $sFieldLabel = 'Label';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLabel';
	protected $sFqModelClassname = '\Model\System\LowCode\ButtonGroup\Component_button_group';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
