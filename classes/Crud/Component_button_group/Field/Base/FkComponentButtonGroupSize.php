<?php
namespace Crud\Component_button_group\Field\Base;

use Core\Utils;
use Crud\Component_button_group\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery;

/**
 * Base class that represents the 'fk_component_button_group_size' crud field from the 'component_button_group' table.
 * This class is auto generated and should not be modified.
 */
abstract class FkComponentButtonGroupSize extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'fk_component_button_group_size';
	protected $sFieldLabel = 'Size';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFkComponentButtonGroupSize';
	protected $sFqModelClassname = '\Model\System\LowCode\ButtonGroup\Component_button_group';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery::create()->orderByItemLabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getItemLabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery::create()->findOneById($iItemId)->getItemLabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
