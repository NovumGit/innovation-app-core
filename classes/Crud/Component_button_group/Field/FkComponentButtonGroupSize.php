<?php
namespace Crud\Component_button_group\Field;

use Crud\Component_button_group\Field\Base\FkComponentButtonGroupSize as BaseFkComponentButtonGroupSize;

/**
 * Skeleton subclass for representing fk_component_button_group_size field from the component_button_group table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentButtonGroupSize extends BaseFkComponentButtonGroupSize
{
}
