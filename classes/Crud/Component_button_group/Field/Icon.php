<?php
namespace Crud\Component_button_group\Field;

use Crud\Component_button_group\Field\Base\Icon as BaseIcon;

/**
 * Skeleton subclass for representing icon field from the component_button_group table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Icon extends BaseIcon
{
}
