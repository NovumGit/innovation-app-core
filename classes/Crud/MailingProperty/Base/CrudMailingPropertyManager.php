<?php
namespace Crud\MailingProperty\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailingProperty\FieldIterator;
use Crud\MailingProperty\Field\MailingId;
use Crud\MailingProperty\Field\PropertyKey;
use Crud\MailingProperty\Field\PropertyValue;
use Exception\LogicException;
use Model\Marketing\MailingProperty;
use Model\Marketing\MailingPropertyQuery;
use Model\Marketing\Map\MailingPropertyTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailingProperty instead if you need to override or add functionality.
 */
abstract class CrudMailingPropertyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingPropertyQuery::create();
	}


	public function getTableMap(): MailingPropertyTableMap
	{
		return new MailingPropertyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailingProperty";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing_property toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing_property aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailingProperty
	 */
	public function getModel(array $aData = null): MailingProperty
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingPropertyQuery = MailingPropertyQuery::create();
		     $oMailingProperty = $oMailingPropertyQuery->findOneById($aData['id']);
		     if (!$oMailingProperty instanceof MailingProperty) {
		         throw new LogicException("MailingProperty should be an instance of MailingProperty but got something else." . __METHOD__);
		     }
		     $oMailingProperty = $this->fillVo($aData, $oMailingProperty);
		}
		else {
		     $oMailingProperty = new MailingProperty();
		     if (!empty($aData)) {
		         $oMailingProperty = $this->fillVo($aData, $oMailingProperty);
		     }
		}
		return $oMailingProperty;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailingProperty
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailingProperty
	{
		$oMailingProperty = $this->getModel($aData);


		 if(!empty($oMailingProperty))
		 {
		     $oMailingProperty = $this->fillVo($aData, $oMailingProperty);
		     $oMailingProperty->save();
		 }
		return $oMailingProperty;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailingProperty $oModel
	 * @return MailingProperty
	 */
	protected function fillVo(array $aData, MailingProperty $oModel): MailingProperty
	{
		if(isset($aData['mailing_id'])) {
		     $oField = new MailingId();
		     $mValue = $oField->sanitize($aData['mailing_id']);
		     $oModel->setMailingId($mValue);
		}
		if(isset($aData['property_key'])) {
		     $oField = new PropertyKey();
		     $mValue = $oField->sanitize($aData['property_key']);
		     $oModel->setPropertyKey($mValue);
		}
		if(isset($aData['property_value'])) {
		     $oField = new PropertyValue();
		     $mValue = $oField->sanitize($aData['property_value']);
		     $oModel->setPropertyValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
