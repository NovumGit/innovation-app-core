<?php
namespace Crud\MailingProperty\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailingProperty\ICollectionField as MailingPropertyField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailingProperty\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailingPropertyField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailingPropertyField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailingPropertyField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailingPropertyField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailingPropertyField
	{
		return current($this->aFields);
	}
}
