<?php

namespace Crud;

use _DefaultApi\AnyEndpointFilter;
use Core\Cfg;
use Model\Logging\Except_log;

class CrudFactory
{
    static function getEndpointFilter(): ?IApiEndpointFilter
    {
        if (!function_exists('getSiteSettings'))
        {
            return null;
        }
        $aSettings = getSiteSettings();

        $aPotentialFilterClasses = [
            $aSettings['namespace'] . '\\EndpointFilter', '_DefaultApi\\EndpointFilter',];

        foreach ($aPotentialFilterClasses as $sPotentialClassname)
        {
            if (class_exists($sPotentialClassname))
            {
                return new $sPotentialClassname();
            }
        }
        return null;
    }

    /**
     * @param $sResourceKey
     * @return BaseManager
     * @throws \ReflectionException
     */
    static function getByResourceKey($sResourceKey): BaseManager
    {
        $aAll = self::getAll(null, new AnyEndpointFilter());

        if (!isset($aAll[$sResourceKey]))
        {
            echo "<h1>" . $sResourceKey . "</h1>";
            var_dump(array_keys($aAll));
        }

        return $aAll[$sResourceKey]['crud_manager'];
    }

    static function create($sFqn): ?BaseManager
    {
        if (is_subclass_of($sFqn, BaseManager::class))
        {
            $oObject = new $sFqn();
            return $oObject;
        }
        return null;
    }

    /**
     * @param array|null $aFilterByInterfaces
     * @param IApiEndpointFilter|null $oEndpointFilter
     * @return FormManager[]|null
     */
    static function getAll(array $aFilterByInterfaces = null, IApiEndpointFilter $oEndpointFilter = null): ?array
    {
        try
        {
            if ($oEndpointFilter == null)
            {
                $oEndpointFilter = self::getEndpointFilter();
            }
            $aAllManagers = [];

            $aCrudDirs = self::getCrudDirs();

            foreach ($aCrudDirs as $sCrudDir)
            {
                $sCrudManagersPath = $sCrudDir . '/Crud*Manager.php';

                $aManagers = glob($sCrudManagersPath);
                foreach ($aManagers as $sManager)
                {

                    $fqnIntermediate = str_replace(Cfg::get('ABSOLUTE_ROOT') . '/classes', '', $sManager);
                    $fqnPhp = str_replace("/", '\\', $fqnIntermediate);
                    $fqn = str_replace('.php', '', basename($fqnPhp));

                    $sManagerClassName = str_replace('.php', '', basename($sManager));
                    $oManagerObject = self::create($fqn);
                    if (!empty($aFilterByInterfaces))
                    {
                        foreach ($aFilterByInterfaces as $sInterfaceName)
                        {
                            if (!class_implements($oManagerObject, $sInterfaceName))
                            {
                                continue 2;
                            }
                        }
                    }
                    if (!$oManagerObject instanceof IApiExposable)
                    {
                        continue;
                    }
                    if (!$oManagerObject instanceof FormManager)
                    {
                        continue;
                    }
                    if ($oEndpointFilter && !$oEndpointFilter->filter($oManagerObject))
                    {
                        continue;
                    }

                    $sModule = strtolower($oManagerObject->getModuleName());

                    $aAllManagers[$sModule] = [
                        'module' => $sModule, 'class_name' => $sManagerClassName, 'crud_manager' => $oManagerObject, 'fields' => $oManagerObject->getAllAvailableFields(),

                        'fqn' => $fqn];
                }
            }

            return $aAllManagers;
        } catch (\ReflectionException $e)
        {
            Except_log::register($e, true);
        }
    }

    private static function getCrudDirs()
    {
        $aCrudPaths = [];
        $aCrudPaths[] = Cfg::get('ABSOLUTE_ROOT') . '/classes/Crud/Custom/' . Cfg::get('CUSTOM_NAMESPACE') . '/*';
        $aCrudPaths[] = Cfg::get('ABSOLUTE_ROOT') . '/classes/Crud/*';

        $aOut = [];

        foreach ($aCrudPaths as $sCrudPath)
        {
            $aCrudDirs = glob($sCrudPath);

            $aSkipRegexes = [
                '/Crud\\' . DIRECTORY_SEPARATOR . 'Generic$/',];

            if (!strpos($sCrudPath, 'Custom'))
            {
                $aSkipRegexes[] = '/Crud\\' . DIRECTORY_SEPARATOR . 'Custom$/';
            }

            foreach ($aCrudDirs as $sDir)
            {
                foreach ($aSkipRegexes as $sRegex)
                {
                    if (preg_match($sRegex, $sDir))
                    {
                        continue 2;
                    }
                }

                if (is_dir($sDir))
                {
                    $aOut[] = $sDir;
                }
            }
        }
        return $aOut;
    }

}
