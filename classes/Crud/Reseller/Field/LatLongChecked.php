<?php
namespace Crud\Reseller\Field;

use Crud\Reseller\Field\Base\LatLongChecked as BaseLatLongChecked;

/**
 * Skeleton subclass for representing lat_long_checked field from the reseller table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class LatLongChecked extends BaseLatLongChecked
{
}
