<?php
namespace Crud\Reseller\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Reseller\Reseller as ModelObject;
use Model\Setting\MasterTable\CountryQuery;

class Country extends Field implements IFilterableLookupField{

    protected $sFieldName = 'country_id';
    protected $sFieldLabel = 'Land';
    private $sIcon = 'globe';
    private $sGetter = 'getCountryId';


    function getLookups($mSelectedItem = null)
    {
        $aCountries = CountryQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aCountries, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return CountryQuery::create()->findOneById($iItemId)->getName();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(isset($aPostedData['country_id']) && empty($aPostedData['country_id']))
        {
            $mResponse = [Translate::fromCode('The field ountry is mandatory')];
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $iCountryId = $oModelObject->{$this->sGetter}();
        $sCountryName = '';
        if($iCountryId)
        {
            $sCountryName = $this->getVisibleValue($iCountryId);
        }
        return '<td class="">'.$sCountryName.'</td>';
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
