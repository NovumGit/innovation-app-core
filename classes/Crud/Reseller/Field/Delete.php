<?php
namespace Crud\Reseller\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Reseller\Reseller;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oRole){
        return false;
    }
    function getUnDeleteUrl($oUnused){
        return false;
    }

    function getOverviewValue($oReseller)
    {
        if(!$oReseller instanceof Reseller)
        {
            throw new InvalidArgumentException('Expected an instance of Reseller but got '.get_class($oReseller));
        }

        $sAddUrl = '?_do=Delete&id='.$oReseller->getId();
        if(isset($_GET['site']))
        {
            $sAddUrl .= '&site='.$_GET['site'];
        }
        $aOut = [];
        $aOut[] = '<td class="">';
        $aOut[] = ' <a href="/cms/reseller/overview'.$sAddUrl.'" class="btn btn-danger br2 btn-xs fs12"> <i class="fa fa-trash-o"></i> </a>';
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }
}