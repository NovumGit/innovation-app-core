<?php
namespace Crud\Reseller\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Reseller\Reseller;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oReseller){

        if(!$oReseller instanceof Reseller)
        {
            throw new InvalidArgumentException('Expected an instance of Reseller but got '.get_class($oReseller));
        }

        $sAddUrl = '?id='.$oReseller->getId();
        if(isset($_GET['site']))
        {
            $sAddUrl .= '&site='.$_GET['site'];
        }
        return "/cms/reseller/edit$sAddUrl";
    }
}