<?php
namespace Crud\Reseller\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Reseller\ICollectionField;

/**
 * Base class that represents the 'postal' crud field from the 'reseller' table.
 * This class is auto generated and should not be modified.
 */
abstract class Postal extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'postal';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPostal';
	protected $sFqModelClassname = '\\\Model\Reseller\Reseller';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
