<?php
namespace Crud\Reseller;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Reseller\Reseller;
use Model\Reseller\ResellerQuery;
use Propel\Runtime\Exception\LogicException;
use Crud\IConfigurableCrud;

class CrudResellerManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getCreateNewUrl():string
    {
        return '/cms/reseller/edit?site='.$_GET['site'];
    }
    function getOverviewUrl():string
    {
        return '/cms/reseller/overview?site='.$_GET['site'];
    }
    function getEntityTitle():string
    {
        return 'reseller';
    }
    function getNewFormTitle():string{
        return 'Resller toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Resller wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'City',
            'Country',
            'Edit',
            'Delete'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'AddressL1',
            'AddressL2',
            'Postal',
            'City',
            'Country',
            'Email',
            'Phone',
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oResellerQuery = ResellerQuery::create();

            $oReseller = $oResellerQuery->findOneById($aData['id']);

            if(!$oReseller instanceof Reseller)
            {
                throw new LogicException("Reseller should be an instance of Reseller but got ".get_class($oReseller)." in ".__METHOD__);
            }
        }
        else
        {
            $oReseller = $this->fillVo(new Reseller(), $aData);
        }

        return $oReseller;
    }

    function store(array $aData = null)
    {
        $oReseller = $this->getModel($aData);

        if(!empty($oReseller))
        {
            $oReseller = $this->fillVo($oReseller, $aData);
            $oReseller->save();
        }
        return $oReseller;
    }

    function fillVo(Reseller $oReseller, $aData)
    {
        if(isset($aData['name']))
        {
            $oReseller->setName($aData['name']);
        }
        if(isset($aData['address_l1']))
        {
            $oReseller->setAddressL1($aData['address_l1']);
        }
        if(isset($aData['address_l2']))
        {
            $oReseller->setAddressL2($aData['address_l2']);
        }
        if(isset($aData['postal']))
        {
            $oReseller->setPostal($aData['postal']);
        }
        if(isset($aData['city']))
        {
            $oReseller->setCity($aData['city']);
        }
        if(isset($aData['country_id']))
        {
            $oReseller->setCountryId($aData['country_id']);
        }
        if(isset($aData['url']))
        {
            $oReseller->setUrl($aData['url']);
        }
        if(isset($aData['email']))
        {
            $oReseller->setEmail($aData['email']);
        }
        if(isset($aData['phone']))
        {
            $oReseller->setPhone($aData['phone']);
        }
        $oReseller->getLatLongChecked(false);
        return $oReseller;
    }
}
