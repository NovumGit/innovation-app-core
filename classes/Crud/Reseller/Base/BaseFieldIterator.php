<?php
namespace Crud\Reseller\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Reseller\ICollectionField as ResellerField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Reseller\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ResellerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ResellerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ResellerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ResellerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ResellerField
	{
		return current($this->aFields);
	}
}
