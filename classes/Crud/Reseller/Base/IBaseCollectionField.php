<?php
namespace Crud\Reseller\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Reseller\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
