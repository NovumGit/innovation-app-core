<?php
namespace Crud\Reseller\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Reseller\FieldIterator;
use Crud\Reseller\Field\AddressL1;
use Crud\Reseller\Field\AddressL2;
use Crud\Reseller\Field\City;
use Crud\Reseller\Field\CountryId;
use Crud\Reseller\Field\Email;
use Crud\Reseller\Field\LatLongChecked;
use Crud\Reseller\Field\Latitude;
use Crud\Reseller\Field\Longtitude;
use Crud\Reseller\Field\Name;
use Crud\Reseller\Field\Phone;
use Crud\Reseller\Field\Postal;
use Crud\Reseller\Field\Url;
use Exception\LogicException;
use Model\Reseller\Map\ResellerTableMap;
use Model\Reseller\Reseller;
use Model\Reseller\ResellerQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Reseller instead if you need to override or add functionality.
 */
abstract class CrudResellerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ResellerQuery::create();
	}


	public function getTableMap(): ResellerTableMap
	{
		return new ResellerTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Reseller";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "reseller toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "reseller aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'AddressL1', 'AddressL2', 'City', 'Postal', 'Phone', 'Email', 'Url', 'Latitude', 'Longtitude', 'LatLongChecked', 'CountryId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'AddressL1', 'AddressL2', 'City', 'Postal', 'Phone', 'Email', 'Url', 'Latitude', 'Longtitude', 'LatLongChecked', 'CountryId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Reseller
	 */
	public function getModel(array $aData = null): Reseller
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oResellerQuery = ResellerQuery::create();
		     $oReseller = $oResellerQuery->findOneById($aData['id']);
		     if (!$oReseller instanceof Reseller) {
		         throw new LogicException("Reseller should be an instance of Reseller but got something else." . __METHOD__);
		     }
		     $oReseller = $this->fillVo($aData, $oReseller);
		}
		else {
		     $oReseller = new Reseller();
		     if (!empty($aData)) {
		         $oReseller = $this->fillVo($aData, $oReseller);
		     }
		}
		return $oReseller;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Reseller
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Reseller
	{
		$oReseller = $this->getModel($aData);


		 if(!empty($oReseller))
		 {
		     $oReseller = $this->fillVo($aData, $oReseller);
		     $oReseller->save();
		 }
		return $oReseller;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Reseller $oModel
	 * @return Reseller
	 */
	protected function fillVo(array $aData, Reseller $oModel): Reseller
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['address_l1'])) {
		     $oField = new AddressL1();
		     $mValue = $oField->sanitize($aData['address_l1']);
		     $oModel->setAddressL1($mValue);
		}
		if(isset($aData['address_l2'])) {
		     $oField = new AddressL2();
		     $mValue = $oField->sanitize($aData['address_l2']);
		     $oModel->setAddressL2($mValue);
		}
		if(isset($aData['city'])) {
		     $oField = new City();
		     $mValue = $oField->sanitize($aData['city']);
		     $oModel->setCity($mValue);
		}
		if(isset($aData['postal'])) {
		     $oField = new Postal();
		     $mValue = $oField->sanitize($aData['postal']);
		     $oModel->setPostal($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		if(isset($aData['latitude'])) {
		     $oField = new Latitude();
		     $mValue = $oField->sanitize($aData['latitude']);
		     $oModel->setLatitude($mValue);
		}
		if(isset($aData['longtitude'])) {
		     $oField = new Longtitude();
		     $mValue = $oField->sanitize($aData['longtitude']);
		     $oModel->setLongtitude($mValue);
		}
		if(isset($aData['lat_long_checked'])) {
		     $oField = new LatLongChecked();
		     $mValue = $oField->sanitize($aData['lat_long_checked']);
		     $oModel->setLatLongChecked($mValue);
		}
		if(isset($aData['country_id'])) {
		     $oField = new CountryId();
		     $mValue = $oField->sanitize($aData['country_id']);
		     $oModel->setCountryId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
