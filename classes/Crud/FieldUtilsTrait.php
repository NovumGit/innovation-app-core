<?php
namespace Crud;

use Core\Cfg;
use Core\Translate;
use Exception\InvalidArgumentException;

trait FieldUtilsTrait
{
    /**
     * @param $sFieldName
     * @param $sModule string - refereerd naar een submap in de crud folder
     * @return Field
     */
    function getField($sFieldName, $sModule)
    {
        $sFieldClassName = "\\Crud\\$sModule\\Field\\$sFieldName";
        if(Cfg::get('CUSTOM_NAMESPACE'))
        {
            $sCustomFieldClassName = "\\Crud\\$sModule\\Field\\".Cfg::get('CUSTOM_NAMESPACE')."\\$sFieldName";

            if(class_exists($sCustomFieldClassName))
            {
                $sFieldClassName = $sCustomFieldClassName;
            }

            $sCustomFieldClassName2 = "\\Crud\\Custom\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sModule\\Field\\$sFieldName";
            if(class_exists($sCustomFieldClassName2))
            {
                $sFieldClassName = $sCustomFieldClassName2;
            }
        }
        $oField = new $sFieldClassName;

        if(!$oField instanceof Field)
        {
            throw new InvalidArgumentException(Translate::fromCode("Propbeerde de CRUD velden van schijf te laden om de bijbehorende titelatuur op te halen maar bij laden krijg ik een onjuist object terug."));
        }
        return $oField;
    }

    function getFilterableFieldAsLookup(FormManager $oCrudManager, $sFilterField)
    {
        $sModule = $oCrudManager->getModuleName();

        $aFields = [];
        $oSelectedField = null;
        foreach($oCrudManager->getAllAvailableFields() as $sFieldName)
        {
            $oField = $this->getField($sFieldName, $sModule);
            if($oField instanceof IFilterableField)
            {
                $sFqFieldname = '\\' . get_class($oField);
                $sSelected = '';
                if($sFilterField == $sFqFieldname)
                {
                    $sSelected = 'selected="selected"';
                }

                $aField = [
                    'name' => $sFieldName,
                    'full_name' => $sFqFieldname,
                    'selected' => $sSelected ,
                    'title' => $oField->getFieldTitle()
                ];
                $aFields[] = $aField;
            }
        }
        return $aFields;
    }
}
