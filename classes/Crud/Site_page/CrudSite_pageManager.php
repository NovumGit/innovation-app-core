<?php
namespace Crud\Site_page;

use Core\Utils;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;

use Propel\Generator\Exception\LogicException;

class CrudSite_pageManager extends FormManager  implements IEditableCrud, IConfigurableCrud
{
    function getOverviewUrl():string
    {
        $sSite = $_GET['site'];
        return '/cms/page/overview?site='.$sSite;
    }
    function getCreateNewUrl():string
    {
        $sSite = $this->getArgument('site');
        return '/cms/page/edit?site='.$sSite;
    }
    function getEntityTitle():string
    {
        return 'page';
    }
    function getNewFormTitle():string
    {
        return 'Pagina toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Pagina wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Tag',
            'Title',
            'Content',
            'About',
            'MinPictures',
            'MaxPictures',
            'HasTitle',
            'HasContent',
            'IsDeletable',
            'IsEditable',
            'IsVisible',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Tag',
            'Title',
            'Content',
            'About',
            'MinPictures',
            'MaxPictures',
            'HasTitle',
            'HasContent',
            'IsDeletable',
            'IsEditable',
            'IsVisible'
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oSitePageQuery = new SitePageQuery();
            $oSite = $oSitePageQuery->findOneById($aData['id']);
            if(!$oSite instanceof SitePage){
                throw new LogicException("Site page should be an instance of SitePage but got".get_class($oSite)." in ".__METHOD__);
            }
        }else
        {
            $oSite = $this->fillVo(new SitePage(), $aData);
        }
        return $oSite;
    }
    function store(array $aData = null)
    {
        $oSitePage = $this->getModel($aData);
        if(!empty($oSitePage))
        {
            $oSitePage = $this->fillVo($oSitePage, $aData);

            if(isset($_FILES['data']) && isset($_FILES['data']['name']['cover_image']) && $_FILES['data']['error']['cover_image'] === 0)
            {
                if($oSitePage->getHasCoverImage() && file_exists($oSitePage->getCoverImageFileName()))
                {
                    unlink($oSitePage->getCoverImageFileName());
                }
                $sExt = pathinfo($_FILES['data']['name']['cover_image'], PATHINFO_EXTENSION);
                $oSitePage->setHasCoverImage(true);
                $oSitePage->setCoverImageExt($sExt);
                move_uploaded_file($_FILES['data']['tmp_name']['cover_image'], $oSitePage->getCoverImageFileName());
            }
            $oSitePage->save();
        }
        return $oSitePage;
    }

    function fillVo(SitePage $oSitePage, $aData)
    {
        if(isset($aData['language_id']))
        {
            $oSitePage->setLanguageId($aData['language_id']);
        }
        if(isset($aData['navigation_id']))
        {
            if($aData['navigation_id'] === '')
            {
                $aData['navigation_id'] = null;
            }
            $oSitePage->setNavigationId($aData['navigation_id']);
        }
        if(isset($aData['site_id']))
        {
            $oSitePage->setSiteId($aData['site_id']);
        }
        if(isset($aData['title_pos_x']))
        {
            $oSitePage->setTitlePosX($aData['title_pos_x']);
        }
        if(isset($aData['title_pos_y']))
        {
            $oSitePage->setTitlePosY($aData['title_pos_y']);
        }
        if(isset($aData['title_color']))
        {
            $oSitePage->setTitleColor($aData['title_color']);
        }
        if(isset($aData['title_background_color']))
        {
            $oSitePage->setTitleBackgroundColor($aData['title_background_color']);
        }
        if(isset($aData['has_title_color_editor']))
        {
            $oSitePage->setHasTitleColorEditor($aData['has_title_color_editor']);
        }
        if(isset($aData['has_title_backgroundcolor_editor']))
        {
            $oSitePage->setHasTitleBackgroundcolorEditor($aData['has_title_backgroundcolor_editor']);
        }

        if(isset($aData['has_title_positioning_fields']))
        {
            $oSitePage->setHasTitlePositioningFields($aData['has_title_positioning_fields']);
        }

        if(isset($aData['tag']))
        {
            $oSitePage->setTag($aData['tag']);
        }
        if(isset($aData['title']))
        {
            $oSitePage->setTitle($aData['title']);
        }
        if(isset($aData['meta_description']))
        {
            $oSitePage->setMetaDescription($aData['meta_description']);
        }
        if(isset($aData['meta_keywords']))
        {
            $oSitePage->setMetaKeywords($aData['meta_keywords']);
        }
        if(isset($aData['content']))
        {
            $oSitePage->setContent($aData['content']);
        }
        if(isset($aData['about']))
        {
            $oSitePage->setAbout($aData['about']);
        }
        if(isset($aData['min_pictures']))
        {
            $oSitePage->setMinPictures($aData['min_pictures']);
        }
        if(isset($aData['max_pictures']))
        {
            $oSitePage->setMaxPictures($aData['max_pictures']);
        }
        if(isset($aData['has_content']))
        {
            $oSitePage->setHasContent($aData['has_content']);
        }
        if(!$oSitePage->getId())
        {
            $oSitePage->setIsDeletable(true);
        }
        if(isset($aData['is_deletable']))
        {
            $oSitePage->setIsDeletable($aData['is_deletable']);
        }
        if(isset($aData['is_editable']))
        {
            $oSitePage->setIsEditable($aData['is_editable']);
        }
        if(isset($aData['is_visible']))
        {
            $oSitePage->setIsVisible($aData['is_visible']);
        }
        if(isset($aData['has_title']))
        {
            $oSitePage->setHasTitle($aData['has_title']);
        }

        if(isset($aData['url']))
        {
            if(($aData['url'] == '/s//n-a' || trim($aData['url']) == '') && !$aData['id'])
            {
                $aData['url'] = '';
            }
            else if(($aData['url'] == '/s//n-a' || $aData['url'] == '') && $aData['id'])
            {
                $aData['url'] = '/s/'.Utils::slugify($aData['title']).'-'.$aData['id'];
            }


            $oSitePage->setUrl($aData['url']);
        }
        if(isset($aData['has_changeable_url']))
        {
            $oSitePage->setHasChangeableUrl($aData['has_changeable_url']);
        }


        return $oSitePage;
    }
}
