<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_page\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage as ModelObject;

class CoverImage extends Field implements IFilterableField, IEditableField{
    private $sFieldName = 'cover_image';
    protected $sFieldLabel = 'Cover image';
    protected $sIcon = 'picture-o';

    function getDataType():string
    {
        return 'file';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Heeft cover image', $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sHasCoverImage = $oModelObject->getHasCoverImage() ? 'Ja' : 'Nee';
        return '<td class="">'.$sHasCoverImage.'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aHtml = [];
        if($mData->getHasCoverImage())
        {
            $aHtml[] = '<div class="form-group">';
            $aHtml[] = '<label class="col-lg-1 control-label">';
            $aHtml[] = '    Cover image preview';
            $aHtml[] = '</label>';
            $aHtml[] = '<div class="col-lg-11">';
            if(method_exists($mData, 'getCoverImagePath'))
            {
                $aHtml[] = '    <img style="width:150px" src="'.$mData->getCoverImagePath().'">';
            }

            $aHtml[] = '</div>';
            $aHtml[] = '</div>';
        }
        $aHtml[] = $this->editFileField($this->sFieldLabel, $this->sFieldName, null, 'Cover image', $this->sIcon, $bReadonly);
        return join(PHP_EOL, $aHtml);
    }
}
