<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_page\Field;

use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage as ModelObject;

class TitlePosY extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'title_pos_y';
    protected $sFieldLabel = 'Titel positie Y';
    private $sIcon = 'arrow-circle-down';
    private $sPlaceHolder = '';
    private $sGetter = 'getTitlePosY';

    function getDataType():string
    {
        return 'number';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        if($mData->hasTitlePositioningFields())
        {
            $oRenderEditConfig = new RenderEditConfig();
            $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
            $this->setRenderEditConfig($oRenderEditConfig);

            return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
        }
        return '';
    }
}
