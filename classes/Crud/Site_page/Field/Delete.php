<?php
namespace Crud\Site_page\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Cms\SitePage as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/cms/page/overview?_do=Delete&site='.$this->getArgument('site').'&id='.$oModelObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}