<?php
namespace Crud\Site_page\Field;

use Core\Utils;
use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Cms\SitePage as ModelObject;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Language;

class LanguageId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'language_id';
    protected $sFieldLabel = 'Taal';
    private $sIcon = 'globe';
    private $sGetter = 'getLanguageId';

    function getLookups($mSelectedItem = null)
    {
        $aLanguages = LanguageQuery::create()->orderByDescription()->find();
        $aOptions = Utils::makeSelectOptions($aLanguages, 'getDescription', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        $sValue = '';
        if($iItemId)
        {
            $oLanguage = LanguageQuery::create()->findOneById($iItemId);
            if($oLanguage instanceof Language)
            {
                $sValue = $oLanguage->getDescription();
            }

        }
        return $sValue;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(!isset($aPostedData['language_id']) || empty($aPostedData['language_id']))
        {
            $mResponse[] = "Taal is een verplicht veld.";
        }


        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sDescription = '';
        if($oModelObject->getLanguageId())
        {
            $oLanguage = LanguageQuery::create()->findOneById($oModelObject->getLanguageId());
            $sDescription = $oLanguage->getDescription();
        }
        return '<td class="">'.$sDescription.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOptions = $this->getLookups($oModelObject->getLanguageId());

        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
        $this->setRenderEditConfig($oRenderEditConfig);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon);
    }
}
