<?php
namespace Crud\Site_page\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Cms\Base\Site_navigationQuery;
use Model\Cms\Site_navigation;
use Model\Cms\SitePage as ModelObject;

class NavigationId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'navigation_id';
    protected $sFieldLabel = 'Navigatie';
    private $sIcon = 'globe';
    private $sGetter = 'getNavigationId';

    function getLookups($mSelectedItem = null, $iParentId = null, $aOut = null, $sCurrentItemAdd = null)
    {
        $oSite_navigationQuery = Site_navigationQuery::create();
        $oSite_navigationQuery->filterByParentId($iParentId);
        $aSite_navigation = $oSite_navigationQuery->find();

        if($iParentId == null)
        {
            $aOut[] = [
                'id' => null,
                'selected' => ($mSelectedItem ==null) ? 'selected' : '',
                'label' => Translate::fromCode("Niet opnemen in navigatie")
            ];
        }


        foreach($aSite_navigation as $oSite_navigation)
        {
            if($sCurrentItemAdd)
            {
                $sLabel = $sCurrentItemAdd . ' -> '.$oSite_navigation->getName();
            }
            else
            {
                $sLabel = $oSite_navigation->getName();
            }
            $aOut[] = [
                'id' => $oSite_navigation->getId(),
                'selected' => $oSite_navigation->getId() == $mSelectedItem ? 'selected' : '',
                'label' => $sLabel
            ];
            $aOut = $this->getLookups($mSelectedItem, $oSite_navigation->getId(), $aOut, $sLabel);
        }
        return $aOut;
    }
    function getVisibleValue($iItemId)
    {
        $sValue = '';
        if($iItemId)
        {
            $oSite_navigation = Site_navigationQuery::create()->findOneById($iItemId);
            if($oSite_navigation instanceof Site_navigation)
            {
                $sValue = $oSite_navigation->getDescription();
            }
        }
        return $sValue;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sDescription = '';
        if($oModelObject->getNavigationId())
        {
            $oSite_navigation = Site_navigationQuery::create()->findOneById($oModelObject->getNavigationId());
            $sDescription = $oSite_navigation->getDescription();

            if($oSite_navigation->getParentId())
            {
                $sParentDescription = Site_navigationQuery::create()->findOneById($oSite_navigation->getParentId());
                $sDescription = $sParentDescription.' -> '.$sDescription;
            }
        }
        return '<td class="">'.$sDescription.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        if(!$oModelObject ->hasNavigation())
        {
            return '';
        }
        $aOptions = $this->getLookups($oModelObject->getLanguageId());

        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
        $this->setRenderEditConfig($oRenderEditConfig);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon);
    }
}
