<?php
namespace Crud\Site_page\Field;

use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage;

class About extends Field implements IFilterableField, IEditableField
{
    private $sFieldName = 'about';
    protected $sFieldLabel = 'Over';
    private $sIcon = 'info';
    private $sPlaceHolder = '';
    private $sGetter = 'getAbout';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(isset($aPostedData['about']) && empty(trim($aPostedData['about'])))
        {
            $mResponse[] = 'Het veld about mag niet leeg zijn.';
        }
        return $mResponse;
    }

    function getFieldTitle()
    {
        return $this->sFieldName;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Over', 'about');
    }

    function getOverviewValue($oSitePage)
    {
        if(!$oSitePage instanceof SitePage)
            throw new InvalidArgumentException("Expected an instance of  model\\Cms\\SitePage in ".__METHOD__);

        return '<td class="">'.$oSitePage->{$this->sGetter}().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SitePage)
            throw new InvalidArgumentException("Expected an instance of  model\\Cms\\SitePage in ".__METHOD__);

        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
        $this->setRenderEditConfig($oRenderEditConfig);

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getAbout(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
