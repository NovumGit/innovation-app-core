<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_page\Field;

use Core\Translate;
use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage as ModelObject;


class TitleColor extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'title_color';
    protected $sFieldLabel = 'Titel kleur';
    private $sGetter = 'getTitleColor';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        if($mData->hasTitleColorEditor())
        {
            $oRenderEditConfig = new RenderEditConfig();
            $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
            $this->setRenderEditConfig($oRenderEditConfig);

            return $this->editColorField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), Translate::fromCode('Titel kleur'));
        }
        return '';

    }
}
