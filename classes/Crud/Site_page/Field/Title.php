<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_page\Field;

use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage as ModelObject;

class Title extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'title';
    protected $sFieldLabel = 'Titel';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getTitle';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        if($mData->hasTitle())
        {
            $oRenderEditConfig = new RenderEditConfig();
            $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
            $this->setRenderEditConfig($oRenderEditConfig);

            return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
        }
        return '';
    }
}
