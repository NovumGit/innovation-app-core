<?php
namespace Crud\Site_page\Field;

use Core\Cfg;
use Core\Config;
use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SitePage;

class Url extends Field implements IFilterableField, IEditableField
{
    private $sFieldName = 'url';
    protected $sFieldLabel = 'Url';
    private $sIcon = 'globe';
    private $sPlaceHolder = '/voorbeeld';
    private $sGetter = 'getUrl';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getFieldTitle()
    {
        return $this->sFieldName;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Url', 'about');
    }

    function getOverviewValue($oSitePage)
    {
        if(!$oSitePage instanceof SitePage)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\Cms\\SitePage in ".__METHOD__);
        }
        $oSite = $oSitePage->getSite();

        $sUrlPath = $oSitePage->{$this->sGetter}();
        $sUrl = Cfg::get('PROTOCOL').'://'.$oSite->getDomain().$sUrlPath;

        return '<td class=""><a title="Klik hier om de url te openen in een nieuw venster." target="_blank" href="'.$sUrl.'">'.$sUrlPath.'</a></td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SitePage)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\Cms\\SitePage in " . __METHOD__);
        }
        if(!$mData->hasUrl())
        {
            return '';
        }

        $oSite = $mData->getSite();


        $sValue = $mData->getUrl();
        if(!$mData->hasChangeableUrl())
        {
            $bReadonly = true;
            $sValue = '<i class="fa fa-lock"></i>&nbsp;&nbsp; <a href="'.Cfg::get('PROTOCOL').'://'.$oSite->getDomain().$sValue.'" target="_blank">'.$sValue. '</a> (deze url is gelockt)';
        }

        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
        $this->setRenderEditConfig($oRenderEditConfig);
        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $sValue, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
