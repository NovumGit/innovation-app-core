<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_page\Field;

use Crud\Field;
use Ui\RenderEditConfig;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use model\Cms\SitePage as ModelObject;


class ContentWysiwyg extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'content';
    protected $sFieldLabel = 'Inhoud (rich text editor)';
    private $sGetter = 'getContent';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {

        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(2, 10);
        $this->setRenderEditConfig($oRenderEditConfig);

        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        if($mData->hasContent())
        {
            return $this->editRichTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
        }
        else
        {
            return '';
        }
    }
}
