<?php
namespace Crud\SiteFooterBlockTranslationSitePage\Field;

use Crud\SiteFooterBlockTranslationSitePage\Field\Base\SitePageId as BaseSitePageId;

/**
 * Skeleton subclass for representing site_page_id field from the site_footer_block_translation_site_page table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class SitePageId extends BaseSitePageId
{
}
