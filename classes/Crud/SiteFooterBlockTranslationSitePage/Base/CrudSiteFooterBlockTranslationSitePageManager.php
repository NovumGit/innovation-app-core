<?php
namespace Crud\SiteFooterBlockTranslationSitePage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFooterBlockTranslationSitePage\FieldIterator;
use Crud\SiteFooterBlockTranslationSitePage\Field\SiteFooterBlockTranslationId;
use Crud\SiteFooterBlockTranslationSitePage\Field\SitePageId;
use Exception\LogicException;
use Model\Cms\Map\SiteFooterBlockTranslationSitePageTableMap;
use Model\Cms\SiteFooterBlockTranslationSitePage;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFooterBlockTranslationSitePage instead if you need to override or add functionality.
 */
abstract class CrudSiteFooterBlockTranslationSitePageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFooterBlockTranslationSitePageQuery::create();
	}


	public function getTableMap(): SiteFooterBlockTranslationSitePageTableMap
	{
		return new SiteFooterBlockTranslationSitePageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFooterBlockTranslationSitePage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_footer_block_translation_site_page toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_footer_block_translation_site_page aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteFooterBlockTranslationId', 'SitePageId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteFooterBlockTranslationId', 'SitePageId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFooterBlockTranslationSitePage
	 */
	public function getModel(array $aData = null): SiteFooterBlockTranslationSitePage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFooterBlockTranslationSitePageQuery = SiteFooterBlockTranslationSitePageQuery::create();
		     $oSiteFooterBlockTranslationSitePage = $oSiteFooterBlockTranslationSitePageQuery->findOneById($aData['id']);
		     if (!$oSiteFooterBlockTranslationSitePage instanceof SiteFooterBlockTranslationSitePage) {
		         throw new LogicException("SiteFooterBlockTranslationSitePage should be an instance of SiteFooterBlockTranslationSitePage but got something else." . __METHOD__);
		     }
		     $oSiteFooterBlockTranslationSitePage = $this->fillVo($aData, $oSiteFooterBlockTranslationSitePage);
		}
		else {
		     $oSiteFooterBlockTranslationSitePage = new SiteFooterBlockTranslationSitePage();
		     if (!empty($aData)) {
		         $oSiteFooterBlockTranslationSitePage = $this->fillVo($aData, $oSiteFooterBlockTranslationSitePage);
		     }
		}
		return $oSiteFooterBlockTranslationSitePage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFooterBlockTranslationSitePage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFooterBlockTranslationSitePage
	{
		$oSiteFooterBlockTranslationSitePage = $this->getModel($aData);


		 if(!empty($oSiteFooterBlockTranslationSitePage))
		 {
		     $oSiteFooterBlockTranslationSitePage = $this->fillVo($aData, $oSiteFooterBlockTranslationSitePage);
		     $oSiteFooterBlockTranslationSitePage->save();
		 }
		return $oSiteFooterBlockTranslationSitePage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFooterBlockTranslationSitePage $oModel
	 * @return SiteFooterBlockTranslationSitePage
	 */
	protected function fillVo(array $aData, SiteFooterBlockTranslationSitePage $oModel): SiteFooterBlockTranslationSitePage
	{
		if(isset($aData['site_footer_block_translation_id'])) {
		     $oField = new SiteFooterBlockTranslationId();
		     $mValue = $oField->sanitize($aData['site_footer_block_translation_id']);
		     $oModel->setSiteFooterBlockTranslationId($mValue);
		}
		if(isset($aData['site_page_id'])) {
		     $oField = new SitePageId();
		     $mValue = $oField->sanitize($aData['site_page_id']);
		     $oModel->setSitePageId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
