<?php
namespace Crud\Component_button_size\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_button_size\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
