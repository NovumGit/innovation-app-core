<?php
namespace Crud\CrudViewField\Field;

use Crud\CrudViewField\Field\Base\FieldName as BaseFieldName;

/**
 * Skeleton subclass for representing field_name field from the crud_view_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FieldName extends BaseFieldName
{
}
