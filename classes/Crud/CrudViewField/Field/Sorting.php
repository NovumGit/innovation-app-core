<?php
namespace Crud\CrudViewField\Field;

use Crud\CrudViewField\Field\Base\Sorting as BaseSorting;

/**
 * Skeleton subclass for representing sorting field from the crud_view_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Sorting extends BaseSorting
{
}
