<?php
namespace Crud\CrudViewField\Field\Base;

use Crud\CrudViewField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'field_name' crud field from the 'crud_view_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class FieldName extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'field_name';
	protected $sFieldLabel = 'Name';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFieldName';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['field_name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
