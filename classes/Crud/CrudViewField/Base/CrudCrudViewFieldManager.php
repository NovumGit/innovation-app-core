<?php
namespace Crud\CrudViewField\Base;

use Core\Utils;
use Crud;
use Crud\CrudViewField\FieldIterator;
use Crud\CrudViewField\Field\CrudViewId;
use Crud\CrudViewField\Field\FieldName;
use Crud\CrudViewField\Field\Sorting;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewField;
use Model\Setting\CrudManager\CrudViewFieldQuery;
use Model\Setting\CrudManager\Map\CrudViewFieldTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudViewField instead if you need to override or add functionality.
 */
abstract class CrudCrudViewFieldManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewFieldQuery::create();
	}


	public function getTableMap(): CrudViewFieldTableMap
	{
		return new CrudViewFieldTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint is opgeslagen welke velden er in een overzicht worden weergegeven";
	}


	public function getEntityTitle(): string
	{
		return "CrudViewField";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view_field toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view_field aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FieldName', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FieldName', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudViewField
	 */
	public function getModel(array $aData = null): CrudViewField
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewFieldQuery = CrudViewFieldQuery::create();
		     $oCrudViewField = $oCrudViewFieldQuery->findOneById($aData['id']);
		     if (!$oCrudViewField instanceof CrudViewField) {
		         throw new LogicException("CrudViewField should be an instance of CrudViewField but got something else." . __METHOD__);
		     }
		     $oCrudViewField = $this->fillVo($aData, $oCrudViewField);
		}
		else {
		     $oCrudViewField = new CrudViewField();
		     if (!empty($aData)) {
		         $oCrudViewField = $this->fillVo($aData, $oCrudViewField);
		     }
		}
		return $oCrudViewField;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudViewField
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudViewField
	{
		$oCrudViewField = $this->getModel($aData);


		 if(!empty($oCrudViewField))
		 {
		     $oCrudViewField = $this->fillVo($aData, $oCrudViewField);
		     $oCrudViewField->save();
		 }
		return $oCrudViewField;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudViewField $oModel
	 * @return CrudViewField
	 */
	protected function fillVo(array $aData, CrudViewField $oModel): CrudViewField
	{
		if(isset($aData['crud_view_id'])) {
		     $oField = new CrudViewId();
		     $mValue = $oField->sanitize($aData['crud_view_id']);
		     $oModel->setCrudViewId($mValue);
		}
		if(isset($aData['field_name'])) {
		     $oField = new FieldName();
		     $mValue = $oField->sanitize($aData['field_name']);
		     $oModel->setFieldName($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
