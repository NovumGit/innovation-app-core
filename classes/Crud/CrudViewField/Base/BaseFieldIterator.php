<?php
namespace Crud\CrudViewField\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudViewField\ICollectionField as CrudViewFieldField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudViewField\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudViewFieldField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudViewFieldField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudViewFieldField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudViewFieldField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudViewFieldField
	{
		return current($this->aFields);
	}
}
