<?php
namespace Crud\PaymentMethod\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\PaymentMethod\ICollectionField as PaymentMethodField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\PaymentMethod\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PaymentMethodField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PaymentMethodField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PaymentMethodField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PaymentMethodField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PaymentMethodField
	{
		return current($this->aFields);
	}
}
