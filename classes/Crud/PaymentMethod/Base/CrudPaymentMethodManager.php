<?php
namespace Crud\PaymentMethod\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PaymentMethod\FieldIterator;
use Crud\PaymentMethod\Field\Code;
use Crud\PaymentMethod\Field\IsEnabledWebshop;
use Crud\PaymentMethod\Field\Name;
use Crud\PaymentMethod\Field\Psp;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\PaymentMethodTableMap;
use Model\Setting\MasterTable\PaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PaymentMethod instead if you need to override or add functionality.
 */
abstract class CrudPaymentMethodManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PaymentMethodQuery::create();
	}


	public function getTableMap(): PaymentMethodTableMap
	{
		return new PaymentMethodTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PaymentMethod";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "payment_method toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "payment_method aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'Psp', 'IsEnabledWebshop'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'Psp', 'IsEnabledWebshop'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PaymentMethod
	 */
	public function getModel(array $aData = null): PaymentMethod
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPaymentMethodQuery = PaymentMethodQuery::create();
		     $oPaymentMethod = $oPaymentMethodQuery->findOneById($aData['id']);
		     if (!$oPaymentMethod instanceof PaymentMethod) {
		         throw new LogicException("PaymentMethod should be an instance of PaymentMethod but got something else." . __METHOD__);
		     }
		     $oPaymentMethod = $this->fillVo($aData, $oPaymentMethod);
		}
		else {
		     $oPaymentMethod = new PaymentMethod();
		     if (!empty($aData)) {
		         $oPaymentMethod = $this->fillVo($aData, $oPaymentMethod);
		     }
		}
		return $oPaymentMethod;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PaymentMethod
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PaymentMethod
	{
		$oPaymentMethod = $this->getModel($aData);


		 if(!empty($oPaymentMethod))
		 {
		     $oPaymentMethod = $this->fillVo($aData, $oPaymentMethod);
		     $oPaymentMethod->save();
		 }
		return $oPaymentMethod;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PaymentMethod $oModel
	 * @return PaymentMethod
	 */
	protected function fillVo(array $aData, PaymentMethod $oModel): PaymentMethod
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['psp'])) {
		     $oField = new Psp();
		     $mValue = $oField->sanitize($aData['psp']);
		     $oModel->setPsp($mValue);
		}
		if(isset($aData['is_enabled_webshop'])) {
		     $oField = new IsEnabledWebshop();
		     $mValue = $oField->sanitize($aData['is_enabled_webshop']);
		     $oModel->setIsEnabledWebshop($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
