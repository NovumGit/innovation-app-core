<?php
namespace Crud\PaymentMethod\Field;

use Crud\PaymentMethod\Field\Base\Psp as BasePsp;

/**
 * Skeleton subclass for representing psp field from the payment_method table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Psp extends BasePsp
{
}
