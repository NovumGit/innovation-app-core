<?php
namespace Crud\PaymentMethod\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\PaymentMethod\ICollectionField;

/**
 * Base class that represents the 'is_enabled_webshop' crud field from the 'payment_method' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsEnabledWebshop extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_enabled_webshop';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsEnabledWebshop';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\PaymentMethod';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
