<?php
namespace Crud\CustomerAddressType\Base;

use Core\Utils;
use Crud;
use Crud\CustomerAddressType\FieldIterator;
use Crud\CustomerAddressType\Field\Code;
use Crud\CustomerAddressType\Field\Description;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerAddressType;
use Model\Crm\CustomerAddressTypeQuery;
use Model\Crm\Map\CustomerAddressTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerAddressType instead if you need to override or add functionality.
 */
abstract class CrudCustomerAddressTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerAddressTypeQuery::create();
	}


	public function getTableMap(): CustomerAddressTypeTableMap
	{
		return new CustomerAddressTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerAddressType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_customer_address_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_customer_address_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerAddressType
	 */
	public function getModel(array $aData = null): CustomerAddressType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerAddressTypeQuery = CustomerAddressTypeQuery::create();
		     $oCustomerAddressType = $oCustomerAddressTypeQuery->findOneById($aData['id']);
		     if (!$oCustomerAddressType instanceof CustomerAddressType) {
		         throw new LogicException("CustomerAddressType should be an instance of CustomerAddressType but got something else." . __METHOD__);
		     }
		     $oCustomerAddressType = $this->fillVo($aData, $oCustomerAddressType);
		}
		else {
		     $oCustomerAddressType = new CustomerAddressType();
		     if (!empty($aData)) {
		         $oCustomerAddressType = $this->fillVo($aData, $oCustomerAddressType);
		     }
		}
		return $oCustomerAddressType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerAddressType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerAddressType
	{
		$oCustomerAddressType = $this->getModel($aData);


		 if(!empty($oCustomerAddressType))
		 {
		     $oCustomerAddressType = $this->fillVo($aData, $oCustomerAddressType);
		     $oCustomerAddressType->save();
		 }
		return $oCustomerAddressType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerAddressType $oModel
	 * @return CustomerAddressType
	 */
	protected function fillVo(array $aData, CustomerAddressType $oModel): CustomerAddressType
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
