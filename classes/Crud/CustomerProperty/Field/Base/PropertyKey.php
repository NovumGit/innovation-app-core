<?php
namespace Crud\CustomerProperty\Field\Base;

use Crud\CustomerProperty\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'property_key' crud field from the 'customer_property' table.
 * This class is auto generated and should not be modified.
 */
abstract class PropertyKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'property_key';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPropertyKey';
	protected $sFqModelClassname = '\\\Model\Crm\CustomerProperty';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
