<?php
namespace Crud\CustomerProperty\Field;

use Crud\CustomerProperty\Field\Base\PropertyKey as BasePropertyKey;

/**
 * Skeleton subclass for representing property_key field from the customer_property table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class PropertyKey extends BasePropertyKey
{
}
