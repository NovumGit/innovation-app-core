<?php
namespace Crud\CustomerProperty\Base;

use Core\Utils;
use Crud;
use Crud\CustomerProperty\FieldIterator;
use Crud\CustomerProperty\Field\CustomerId;
use Crud\CustomerProperty\Field\PropertyKey;
use Crud\CustomerProperty\Field\PropertyValue;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerProperty;
use Model\Crm\CustomerPropertyQuery;
use Model\Crm\Map\CustomerPropertyTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerProperty instead if you need to override or add functionality.
 */
abstract class CrudCustomerPropertyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerPropertyQuery::create();
	}


	public function getTableMap(): CustomerPropertyTableMap
	{
		return new CustomerPropertyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerProperty";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_property toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_property aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'PropertyKey', 'PropertyValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerProperty
	 */
	public function getModel(array $aData = null): CustomerProperty
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerPropertyQuery = CustomerPropertyQuery::create();
		     $oCustomerProperty = $oCustomerPropertyQuery->findOneById($aData['id']);
		     if (!$oCustomerProperty instanceof CustomerProperty) {
		         throw new LogicException("CustomerProperty should be an instance of CustomerProperty but got something else." . __METHOD__);
		     }
		     $oCustomerProperty = $this->fillVo($aData, $oCustomerProperty);
		}
		else {
		     $oCustomerProperty = new CustomerProperty();
		     if (!empty($aData)) {
		         $oCustomerProperty = $this->fillVo($aData, $oCustomerProperty);
		     }
		}
		return $oCustomerProperty;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerProperty
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerProperty
	{
		$oCustomerProperty = $this->getModel($aData);


		 if(!empty($oCustomerProperty))
		 {
		     $oCustomerProperty = $this->fillVo($aData, $oCustomerProperty);
		     $oCustomerProperty->save();
		 }
		return $oCustomerProperty;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerProperty $oModel
	 * @return CustomerProperty
	 */
	protected function fillVo(array $aData, CustomerProperty $oModel): CustomerProperty
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['property_key'])) {
		     $oField = new PropertyKey();
		     $mValue = $oField->sanitize($aData['property_key']);
		     $oModel->setPropertyKey($mValue);
		}
		if(isset($aData['property_value'])) {
		     $oField = new PropertyValue();
		     $mValue = $oField->sanitize($aData['property_value']);
		     $oModel->setPropertyValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
