<?php
namespace Crud\CustomerProperty\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerProperty\ICollectionField as CustomerPropertyField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerProperty\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerPropertyField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerPropertyField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerPropertyField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerPropertyField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerPropertyField
	{
		return current($this->aFields);
	}
}
