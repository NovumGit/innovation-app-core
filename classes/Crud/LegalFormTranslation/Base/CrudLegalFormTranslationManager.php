<?php
namespace Crud\LegalFormTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\LegalFormTranslation\FieldIterator;
use Crud\LegalFormTranslation\Field\LanguageId;
use Crud\LegalFormTranslation\Field\LegalFormId;
use Crud\LegalFormTranslation\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\LegalFormTranslation;
use Model\Setting\MasterTable\LegalFormTranslationQuery;
use Model\Setting\MasterTable\Map\LegalFormTranslationTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify LegalFormTranslation instead if you need to override or add functionality.
 */
abstract class CrudLegalFormTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return LegalFormTranslationQuery::create();
	}


	public function getTableMap(): LegalFormTranslationTableMap
	{
		return new LegalFormTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "LegalFormTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_legal_form_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_legal_form_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LegalFormId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LegalFormId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return LegalFormTranslation
	 */
	public function getModel(array $aData = null): LegalFormTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oLegalFormTranslationQuery = LegalFormTranslationQuery::create();
		     $oLegalFormTranslation = $oLegalFormTranslationQuery->findOneById($aData['id']);
		     if (!$oLegalFormTranslation instanceof LegalFormTranslation) {
		         throw new LogicException("LegalFormTranslation should be an instance of LegalFormTranslation but got something else." . __METHOD__);
		     }
		     $oLegalFormTranslation = $this->fillVo($aData, $oLegalFormTranslation);
		}
		else {
		     $oLegalFormTranslation = new LegalFormTranslation();
		     if (!empty($aData)) {
		         $oLegalFormTranslation = $this->fillVo($aData, $oLegalFormTranslation);
		     }
		}
		return $oLegalFormTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return LegalFormTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): LegalFormTranslation
	{
		$oLegalFormTranslation = $this->getModel($aData);


		 if(!empty($oLegalFormTranslation))
		 {
		     $oLegalFormTranslation = $this->fillVo($aData, $oLegalFormTranslation);
		     $oLegalFormTranslation->save();
		 }
		return $oLegalFormTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param LegalFormTranslation $oModel
	 * @return LegalFormTranslation
	 */
	protected function fillVo(array $aData, LegalFormTranslation $oModel): LegalFormTranslation
	{
		if(isset($aData['legal_form_id'])) {
		     $oField = new LegalFormId();
		     $mValue = $oField->sanitize($aData['legal_form_id']);
		     $oModel->setLegalFormId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
