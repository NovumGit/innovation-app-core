<?php
namespace Crud\LegalFormTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\LegalFormTranslation\ICollectionField as LegalFormTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\LegalFormTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param LegalFormTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param LegalFormTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof LegalFormTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(LegalFormTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): LegalFormTranslationField
	{
		return current($this->aFields);
	}
}
