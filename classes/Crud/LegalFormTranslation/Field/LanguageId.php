<?php
namespace Crud\LegalFormTranslation\Field;

use Crud\LegalFormTranslation\Field\Base\LanguageId as BaseLanguageId;

/**
 * Skeleton subclass for representing language_id field from the mt_legal_form_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class LanguageId extends BaseLanguageId
{
}
