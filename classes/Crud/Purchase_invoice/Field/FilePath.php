<?php
/**
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Purchase_invoice\Field;

use Crud\Field;
use Crud\Generic\Field\GenericEditTextfield;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Finance\PurchaseInvoice as ModelObject;
use Model\Finance\PurchaseInvoice;

class FilePath extends GenericEditTextfield implements IFilterableField, IEditableField{

    protected $sFieldName = 'file_path';
    protected $sFieldLabel = 'Factuur lokaal bestand';
    protected $sIcon = 'file';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getFilePath';
    protected $sFqModelClassname = PurchaseInvoice::class;

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

}
