<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericDate;
use Model\Finance\PurchaseInvoice;

class DueDate extends GenericDate
{
    protected $sFieldName = 'due_date';
    protected $sFieldLabel = 'Uiterste datum';
    protected $sIcon = 'calendar';
    protected $sPlaceHolder = null;
    protected $sGetter = 'getDueDate';
    protected $sFqModelClassname = PurchaseInvoice::class;
}