<?php
/**
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\PurchaseInvoice;

class RemoteInvoiceId extends GenericEditTextfield implements IFilterableField, IEditableField
{

    protected $sFieldName = 'remote_invoice_id';
    protected $sFieldLabel = 'Factuurnummer';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getRemoteInvoiceId';
    protected $sFqModelClassname = PurchaseInvoice::class;

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
