<?php

namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericEdit;

class Edit extends GenericEdit
{
    function getEditUrl($oObject)
    {
        return '/purchase/invoice/edit';
    }
}