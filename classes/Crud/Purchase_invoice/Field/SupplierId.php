<?php
namespace Crud\Purchase_invoice\Field;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IFilterableLookupField;
use Model\Finance\PurchaseInvoice;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;

class SupplierId extends GenericLookup implements IFilterableLookupField{

    protected $sFieldName = 'supplier_id';
    protected $sFieldLabel = 'Leverancier';
    protected $sIcon = 'user';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getSupplierId';
    protected $sFqModelClassname = PurchaseInvoice::class;

    function getVisibleValue($iItemId)
    {
        $oSupplier = SupplierQuery::create()->findOneById($iItemId);

        if($oSupplier instanceof Supplier)
        {
            return $oSupplier->getName();
        }
        return null;

    }
    function getLookups($iSelectedItem = null)
    {
        $aSuppliers = SupplierQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aSuppliers, 'getName', $iSelectedItem);
        return $aOptions;
    }

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

}
