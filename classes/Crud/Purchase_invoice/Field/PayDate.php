<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericDate;
use Model\Finance\PurchaseInvoice;

class PayDate extends GenericDate
{
    protected $sFieldName = 'pay_date';
    protected $sFieldLabel = 'Betaaldatum';
    protected $sIcon = 'calendar';
    protected $sPlaceHolder = null;
    protected $sGetter = 'getPayDate';
    protected $sFqModelClassname = PurchaseInvoice::class;

}