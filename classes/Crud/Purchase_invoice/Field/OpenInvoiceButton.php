<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Finance\PurchaseInvoice;
use Model\Sale\SaleOrder;

class OpenInvoiceButton extends Field implements IEventField{

    protected $sFieldLabel = 'Factuur openen';
    function getIcon()
    {
        return 'file-o';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof PurchaseInvoice)
        {
            throw new LogicException("Expected an instance of Customer, got " . get_class($mData));
        }
        $oPurchaseInvoice = $mData;


        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a target="_blank" title="Factuur openen" href="/purchase/invoice/pdf_viewer?invloice_id=' . $oPurchaseInvoice->getId() . '&r=purchase_invoice_back_url" class="btn btn-danger br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        return 'overview_only_field';
    }
}
