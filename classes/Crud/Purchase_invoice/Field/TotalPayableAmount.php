<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericValutaEuro;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\PurchaseInvoice;

class TotalPayableAmount extends GenericValutaEuro implements IFilterableField, IEditableField{

    protected $sFieldName = 'total_payable_amount';
    protected $sFieldLabel = 'Totaalbedrag';
    protected $sIcon = 'money';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getTotalPayableAmount';
    protected $sFqModelClassname = PurchaseInvoice::class;

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

}
