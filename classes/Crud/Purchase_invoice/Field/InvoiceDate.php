<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericDate;
use Model\Finance\PurchaseInvoice;

class InvoiceDate extends GenericDate
{
    protected $sFieldName = 'invoice_date';
    protected $sFieldLabel = 'Factuurdatum';
    protected $sIcon = 'calendar';
    protected $sPlaceHolder = null;
    protected $sGetter = 'getInvoiceDate';
    protected $sFqModelClassname = PurchaseInvoice::class;
}