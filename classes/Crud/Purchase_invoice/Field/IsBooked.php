<?php
namespace Crud\Purchase_invoice\Field;

use Crud\Generic\Field\GenericBoolean;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\PurchaseInvoice;
use Exception\InvalidArgumentException;

class IsBooked extends GenericBoolean implements IFilterableField, IEditableField
{

    protected $sFieldName = 'is_booked';
    protected $sFieldLabel = 'Geboekt';
    protected $sIcon = 'file';
    protected $sPlaceHolder = 'Bijv pdf of xls';
    protected $sGetter = 'getIsBooked';
    protected $sFqModelClassname = PurchaseInvoice::class;

    function getOverviewValue($oModelObject)
    {
        if(!$this->isValidModel($oModelObject))
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}() ? 'check' : 'times-circle';
        $sColor = $oModelObject->{$this->sGetter}() ? 'success' : 'danger';

        $aOut = [];
        $aOut[] = '<td>';
        $aOut[] = '    <div class="btn btn-xs btn-' . $sColor . '">';
        $aOut[] = '        <i class="fa fa-' . $sValue . '"></i>';
        $aOut[] = '    </div>';
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }


    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
}
