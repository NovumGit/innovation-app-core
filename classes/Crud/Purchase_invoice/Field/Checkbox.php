<?php
namespace Crud\Purchase_invoice\Field;

use Model\Finance\PurchaseInvoice as ModelObject;;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Exception\LogicException;

class Checkbox extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'Multi purpose checkbox';
    protected $sFieldLabel = 'Checkbox';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getOverviewHeader()
    {
        return $this->nonSortableHeaderField('<a class="btn btn-default br2 btn-xs"><i class="toggle_check fa fa-edit"></i></a>', 'iconcol');
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">
                    <input class="multifunctional_checkbox" name="items['.$oModelObject->getId().']" value="1" type="checkbox" />
                </td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
      throw new LogicException("Checkbox is intended only for overview.");
    }
}
