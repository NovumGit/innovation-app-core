<?php

namespace Crud\Purchase_invoice;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\PurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery;

class CrudPurchase_invoiceManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Inkoop facturen';
    }

    function getOverviewUrl():string
    {
        return '/purchase/invoice/overview';
    }

    function getCreateNewUrl():string
    {
        return '/purchase/invoice/edit';
    }

    function getNewFormTitle():string
    {
        return 'Nieuwe inkoop factuur';
    }

    function getEditFormTitle():string
    {
        return 'Inkoop factuur bewerken';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'FilePath',
            'RemoteInvoiceId',
            'SupplierId',
            'TotalPayableAmount'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'FilePath',
            'RemoteInvoiceId',
            'SupplierId',
            'TotalPayableAmount'
        ];
    }

    function getModel(array $aData = null)
    {
        if (isset($aData['id']) && $aData['id']) {
            $oPurchaseInvoiceQuery = PurchaseInvoiceQuery::create();
            $oPurchaseInvoice = $oPurchaseInvoiceQuery->findOneById($aData['id']);

            if (!$oPurchaseInvoice instanceof PurchaseInvoice) {
                throw new LogicException("Project should be an instance of PurchaseInvoice but got something else." . __METHOD__);
            }
            $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
        } else {
            $oPurchaseInvoice = new PurchaseInvoice();
            if (!empty($aData)) {
                $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
            }
        }
        return $oPurchaseInvoice;
    }

    /**
     * @param $aData
     * @return PurchaseInvoice
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null): PurchaseInvoice
    {
        $oPurchaseInvoice = $this->getModel($aData);
        if ($oPurchaseInvoice instanceof PurchaseInvoice) {
            $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
            $oPurchaseInvoice->save();
        }
        return $oPurchaseInvoice;
    }

    private function fillVo($aData, PurchaseInvoice $oModel)
    {
        if (isset($aData['file_path'])) {
            $oModel->setFilePath($aData['file_path']);
        }
        if (isset($aData['remote_invoice_id'])) {
            $oModel->setRemoteInvoiceId($aData['remote_invoice_id']);
        }
        if (isset($aData['supplier_id'])) {
            $oModel->setSupplierId($aData['supplier_id']);
        }
        if (isset($aData['total_payable_amount'])) {
            $oModel->setTotalPayableAmount($aData['total_payable_amount']);
        }
        if (isset($aData['needs_manual_booking'])) {
            $oModel->setNeedsManualBooking($aData['needs_manual_booking']);
        }

        return $oModel;

    }
}
