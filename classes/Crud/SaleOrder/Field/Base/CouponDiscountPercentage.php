<?php
namespace Crud\SaleOrder\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrder\ICollectionField;

/**
 * Base class that represents the 'coupon_discount_percentage' crud field from the 'sale_order' table.
 * This class is auto generated and should not be modified.
 */
abstract class CouponDiscountPercentage extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'coupon_discount_percentage';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCouponDiscountPercentage';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrder';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
