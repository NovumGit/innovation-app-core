<?php
namespace Crud\SaleOrder\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrder\ICollectionField;

/**
 * Base class that represents the 'our_general_country' crud field from the 'sale_order' table.
 * This class is auto generated and should not be modified.
 */
abstract class OurGeneralCountry extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'our_general_country';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getOurGeneralCountry';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrder';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
