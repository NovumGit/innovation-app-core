<?php
namespace Crud\SaleOrder\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrder\ICollectionField;

/**
 * Base class that represents the 'customer_delivery_attn_name' crud field from the 'sale_order' table.
 * This class is auto generated and should not be modified.
 */
abstract class CustomerDeliveryAttnName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'customer_delivery_attn_name';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCustomerDeliveryAttnName';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrder';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
