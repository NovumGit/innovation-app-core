<?php
namespace Crud\SaleOrder\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrder\ICollectionField;

/**
 * Base class that represents the 'cp_first_name' crud field from the 'sale_order' table.
 * This class is auto generated and should not be modified.
 */
abstract class CpFirstName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'cp_first_name';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCpFirstName';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrder';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
