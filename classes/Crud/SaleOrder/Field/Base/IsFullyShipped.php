<?php
namespace Crud\SaleOrder\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrder\ICollectionField;

/**
 * Base class that represents the 'is_fully_shipped' crud field from the 'sale_order' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsFullyShipped extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_fully_shipped';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsFullyShipped';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrder';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
