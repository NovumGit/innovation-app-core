<?php
namespace Crud\SaleOrder\Field;

use Crud\SaleOrder\Field\Base\IsPushedToAccounting as BaseIsPushedToAccounting;

/**
 * Skeleton subclass for representing is_pushed_to_accounting field from the sale_order table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class IsPushedToAccounting extends BaseIsPushedToAccounting
{
}
