<?php
namespace Crud\SaleOrder\Field;

use Crud\SaleOrder\Field\Base\ShippingOrderNote as BaseShippingOrderNote;

/**
 * Skeleton subclass for representing shipping_order_note field from the sale_order table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ShippingOrderNote extends BaseShippingOrderNote
{
}
