<?php
namespace Crud\SaleOrder\Field;

use Crud\SaleOrder\Field\Base\CustomerDeliveryAddressL1 as BaseCustomerDeliveryAddressL1;

/**
 * Skeleton subclass for representing customer_delivery_address_l1 field from the sale_order table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class CustomerDeliveryAddressL1 extends BaseCustomerDeliveryAddressL1
{
}
