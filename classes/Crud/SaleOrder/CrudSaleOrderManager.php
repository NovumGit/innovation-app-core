<?php
namespace Crud\SaleOrder;

/**
 * Skeleton subclass for representing a SaleOrder.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class CrudSaleOrderManager extends Base\CrudSaleOrderManager
{
}
