<?php
namespace Crud\SaleOrder\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrder\ICollectionField as SaleOrderField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrder\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderField
	{
		return current($this->aFields);
	}
}
