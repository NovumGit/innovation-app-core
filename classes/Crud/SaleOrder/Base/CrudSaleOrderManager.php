<?php
namespace Crud\SaleOrder\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrder\FieldIterator;
use Crud\SaleOrder\Field\CompanyName;
use Crud\SaleOrder\Field\CouponCode;
use Crud\SaleOrder\Field\CouponDiscountAmount;
use Crud\SaleOrder\Field\CouponDiscountPercentage;
use Crud\SaleOrder\Field\CpFirstName;
use Crud\SaleOrder\Field\CpLastName;
use Crud\SaleOrder\Field\CpSallutation;
use Crud\SaleOrder\Field\CreatedByUserId;
use Crud\SaleOrder\Field\CreatedOn;
use Crud\SaleOrder\Field\CustomerChamberOfCommerce;
use Crud\SaleOrder\Field\CustomerDeliveryAddressId;
use Crud\SaleOrder\Field\CustomerDeliveryAddressL1;
use Crud\SaleOrder\Field\CustomerDeliveryAddressL2;
use Crud\SaleOrder\Field\CustomerDeliveryAttnName;
use Crud\SaleOrder\Field\CustomerDeliveryCity;
use Crud\SaleOrder\Field\CustomerDeliveryCompanyName;
use Crud\SaleOrder\Field\CustomerDeliveryCountry;
use Crud\SaleOrder\Field\CustomerDeliveryNumber;
use Crud\SaleOrder\Field\CustomerDeliveryNumberAdd;
use Crud\SaleOrder\Field\CustomerDeliveryPostal;
use Crud\SaleOrder\Field\CustomerDeliveryStreet;
use Crud\SaleOrder\Field\CustomerDeliveryUsaState;
use Crud\SaleOrder\Field\CustomerEmail;
use Crud\SaleOrder\Field\CustomerFax;
use Crud\SaleOrder\Field\CustomerId;
use Crud\SaleOrder\Field\CustomerInvoiceAddressId;
use Crud\SaleOrder\Field\CustomerInvoiceAddressL1;
use Crud\SaleOrder\Field\CustomerInvoiceAddressL2;
use Crud\SaleOrder\Field\CustomerInvoiceAttnName;
use Crud\SaleOrder\Field\CustomerInvoiceCity;
use Crud\SaleOrder\Field\CustomerInvoiceCompanyName;
use Crud\SaleOrder\Field\CustomerInvoiceCountry;
use Crud\SaleOrder\Field\CustomerInvoiceNumber;
use Crud\SaleOrder\Field\CustomerInvoiceNumberAdd;
use Crud\SaleOrder\Field\CustomerInvoicePostal;
use Crud\SaleOrder\Field\CustomerInvoiceStreet;
use Crud\SaleOrder\Field\CustomerInvoiceUsaState;
use Crud\SaleOrder\Field\CustomerOrderNote;
use Crud\SaleOrder\Field\CustomerOrderPlacedBy;
use Crud\SaleOrder\Field\CustomerOrderReference;
use Crud\SaleOrder\Field\CustomerPhone;
use Crud\SaleOrder\Field\CustomerVatNumber;
use Crud\SaleOrder\Field\Debitor;
use Crud\SaleOrder\Field\EnvelopeWeightGrams;
use Crud\SaleOrder\Field\EstimatedDeliveryDate;
use Crud\SaleOrder\Field\HandledBy;
use Crud\SaleOrder\Field\HasWrap;
use Crud\SaleOrder\Field\HideUntill;
use Crud\SaleOrder\Field\InvoiceByPostal;
use Crud\SaleOrder\Field\InvoiceDate;
use Crud\SaleOrder\Field\InvoiceNumber;
use Crud\SaleOrder\Field\IsClosed;
use Crud\SaleOrder\Field\IsCompleted;
use Crud\SaleOrder\Field\IsFullyPaid;
use Crud\SaleOrder\Field\IsFullyShipped;
use Crud\SaleOrder\Field\IsPushedToAccounting;
use Crud\SaleOrder\Field\IsReadyForShipping;
use Crud\SaleOrder\Field\IsShippingChecked;
use Crud\SaleOrder\Field\ItemDeleted;
use Crud\SaleOrder\Field\LanguageId;
use Crud\SaleOrder\Field\OrderNumber;
use Crud\SaleOrder\Field\OurBicNumber;
use Crud\SaleOrder\Field\OurChamberOfCommerce;
use Crud\SaleOrder\Field\OurCustomFolder;
use Crud\SaleOrder\Field\OurEmail;
use Crud\SaleOrder\Field\OurFax;
use Crud\SaleOrder\Field\OurGeneralCity;
use Crud\SaleOrder\Field\OurGeneralCompanyName;
use Crud\SaleOrder\Field\OurGeneralCountry;
use Crud\SaleOrder\Field\OurGeneralNumber;
use Crud\SaleOrder\Field\OurGeneralNumberAdd;
use Crud\SaleOrder\Field\OurGeneralPoBox;
use Crud\SaleOrder\Field\OurGeneralPostal;
use Crud\SaleOrder\Field\OurGeneralStreet;
use Crud\SaleOrder\Field\OurIbanNumber;
use Crud\SaleOrder\Field\OurPhone;
use Crud\SaleOrder\Field\OurSlogan;
use Crud\SaleOrder\Field\OurVatNumber;
use Crud\SaleOrder\Field\OurWebsite;
use Crud\SaleOrder\Field\PackageAmount;
use Crud\SaleOrder\Field\PackingSlipType;
use Crud\SaleOrder\Field\PayDate;
use Crud\SaleOrder\Field\PaymethodCode;
use Crud\SaleOrder\Field\PaymethodId;
use Crud\SaleOrder\Field\PaymethodName;
use Crud\SaleOrder\Field\PaytermDays;
use Crud\SaleOrder\Field\PaytermOriginalId;
use Crud\SaleOrder\Field\PaytermString;
use Crud\SaleOrder\Field\Reference;
use Crud\SaleOrder\Field\SaleOrderStatusId;
use Crud\SaleOrder\Field\ShipDate;
use Crud\SaleOrder\Field\ShippingCarrier;
use Crud\SaleOrder\Field\ShippingHasTrackTrace;
use Crud\SaleOrder\Field\ShippingId;
use Crud\SaleOrder\Field\ShippingMethodId;
use Crud\SaleOrder\Field\ShippingNote;
use Crud\SaleOrder\Field\ShippingOrderNote;
use Crud\SaleOrder\Field\ShippingPrice;
use Crud\SaleOrder\Field\ShippingRegistrationDate;
use Crud\SaleOrder\Field\SiteId;
use Crud\SaleOrder\Field\Source;
use Crud\SaleOrder\Field\TrackingCode;
use Crud\SaleOrder\Field\TrackingUrl;
use Crud\SaleOrder\Field\WebshopUserSessionId;
use Crud\SaleOrder\Field\WorkInProgress;
use Crud\SaleOrder\Field\WrappingPrice;
use Exception\LogicException;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrder instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderQuery::create();
	}


	public function getTableMap(): SaleOrderTableMap
	{
		return new SaleOrderTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrder";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'IsClosed', 'IsFullyPaid', 'PayDate', 'IsReadyForShipping', 'IsShippingChecked', 'IsFullyShipped', 'IsPushedToAccounting', 'ShipDate', 'SaleOrderStatusId', 'HandledBy', 'Source', 'IsCompleted', 'CreatedByUserId', 'CreatedOn', 'EstimatedDeliveryDate', 'CustomerId', 'EnvelopeWeightGrams', 'WebshopUserSessionId', 'InvoiceNumber', 'InvoiceDate', 'PaytermDays', 'PaytermOriginalId', 'PaytermString', 'OrderNumber', 'ItemDeleted', 'CompanyName', 'Debitor', 'OurCustomFolder', 'OurSlogan', 'OurPhone', 'OurFax', 'OurWebsite', 'OurEmail', 'OurVatNumber', 'OurChamberOfCommerce', 'OurIbanNumber', 'OurBicNumber', 'OurGeneralCompanyName', 'OurGeneralStreet', 'OurGeneralNumber', 'OurGeneralNumberAdd', 'OurGeneralPostal', 'OurGeneralPoBox', 'OurGeneralCity', 'OurGeneralCountry', 'CustomerPhone', 'CustomerFax', 'CustomerEmail', 'CustomerChamberOfCommerce', 'CustomerVatNumber', 'CustomerOrderReference', 'CustomerOrderPlacedBy', 'CustomerInvoiceAddressId', 'CustomerInvoiceCompanyName', 'CustomerInvoiceAttnName', 'CustomerInvoiceStreet', 'CustomerInvoiceNumber', 'CustomerInvoiceNumberAdd', 'CustomerInvoicePostal', 'CustomerInvoiceCity', 'CustomerInvoiceCountry', 'CustomerInvoiceUsaState', 'CustomerInvoiceAddressL1', 'CustomerInvoiceAddressL2', 'CustomerDeliveryAddressId', 'CustomerDeliveryCompanyName', 'CustomerDeliveryAttnName', 'CustomerDeliveryStreet', 'CustomerDeliveryNumber', 'CustomerDeliveryNumberAdd', 'CustomerDeliveryPostal', 'CustomerDeliveryCity', 'CustomerDeliveryCountry', 'CustomerDeliveryUsaState', 'CustomerDeliveryAddressL1', 'CustomerDeliveryAddressL2', 'CpFirstName', 'CpLastName', 'CpSallutation', 'Reference', 'ShippingNote', 'CustomerOrderNote', 'ShippingOrderNote', 'HasWrap', 'WrappingPrice', 'ShippingCarrier', 'ShippingPrice', 'ShippingMethodId', 'ShippingHasTrackTrace', 'ShippingId', 'TrackingCode', 'TrackingUrl', 'PaymethodName', 'PaymethodCode', 'PaymethodId', 'ShippingRegistrationDate', 'InvoiceByPostal', 'PackingSlipType', 'WorkInProgress', 'PackageAmount', 'CouponCode', 'CouponDiscountAmount', 'CouponDiscountPercentage', 'HideUntill'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'IsClosed', 'IsFullyPaid', 'PayDate', 'IsReadyForShipping', 'IsShippingChecked', 'IsFullyShipped', 'IsPushedToAccounting', 'ShipDate', 'SaleOrderStatusId', 'HandledBy', 'Source', 'IsCompleted', 'CreatedByUserId', 'CreatedOn', 'EstimatedDeliveryDate', 'CustomerId', 'EnvelopeWeightGrams', 'WebshopUserSessionId', 'InvoiceNumber', 'InvoiceDate', 'PaytermDays', 'PaytermOriginalId', 'PaytermString', 'OrderNumber', 'ItemDeleted', 'CompanyName', 'Debitor', 'OurCustomFolder', 'OurSlogan', 'OurPhone', 'OurFax', 'OurWebsite', 'OurEmail', 'OurVatNumber', 'OurChamberOfCommerce', 'OurIbanNumber', 'OurBicNumber', 'OurGeneralCompanyName', 'OurGeneralStreet', 'OurGeneralNumber', 'OurGeneralNumberAdd', 'OurGeneralPostal', 'OurGeneralPoBox', 'OurGeneralCity', 'OurGeneralCountry', 'CustomerPhone', 'CustomerFax', 'CustomerEmail', 'CustomerChamberOfCommerce', 'CustomerVatNumber', 'CustomerOrderReference', 'CustomerOrderPlacedBy', 'CustomerInvoiceAddressId', 'CustomerInvoiceCompanyName', 'CustomerInvoiceAttnName', 'CustomerInvoiceStreet', 'CustomerInvoiceNumber', 'CustomerInvoiceNumberAdd', 'CustomerInvoicePostal', 'CustomerInvoiceCity', 'CustomerInvoiceCountry', 'CustomerInvoiceUsaState', 'CustomerInvoiceAddressL1', 'CustomerInvoiceAddressL2', 'CustomerDeliveryAddressId', 'CustomerDeliveryCompanyName', 'CustomerDeliveryAttnName', 'CustomerDeliveryStreet', 'CustomerDeliveryNumber', 'CustomerDeliveryNumberAdd', 'CustomerDeliveryPostal', 'CustomerDeliveryCity', 'CustomerDeliveryCountry', 'CustomerDeliveryUsaState', 'CustomerDeliveryAddressL1', 'CustomerDeliveryAddressL2', 'CpFirstName', 'CpLastName', 'CpSallutation', 'Reference', 'ShippingNote', 'CustomerOrderNote', 'ShippingOrderNote', 'HasWrap', 'WrappingPrice', 'ShippingCarrier', 'ShippingPrice', 'ShippingMethodId', 'ShippingHasTrackTrace', 'ShippingId', 'TrackingCode', 'TrackingUrl', 'PaymethodName', 'PaymethodCode', 'PaymethodId', 'ShippingRegistrationDate', 'InvoiceByPostal', 'PackingSlipType', 'WorkInProgress', 'PackageAmount', 'CouponCode', 'CouponDiscountAmount', 'CouponDiscountPercentage', 'HideUntill'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrder
	 */
	public function getModel(array $aData = null): SaleOrder
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderQuery = SaleOrderQuery::create();
		     $oSaleOrder = $oSaleOrderQuery->findOneById($aData['id']);
		     if (!$oSaleOrder instanceof SaleOrder) {
		         throw new LogicException("SaleOrder should be an instance of SaleOrder but got something else." . __METHOD__);
		     }
		     $oSaleOrder = $this->fillVo($aData, $oSaleOrder);
		}
		else {
		     $oSaleOrder = new SaleOrder();
		     if (!empty($aData)) {
		         $oSaleOrder = $this->fillVo($aData, $oSaleOrder);
		     }
		}
		return $oSaleOrder;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrder
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrder
	{
		$oSaleOrder = $this->getModel($aData);


		 if(!empty($oSaleOrder))
		 {
		     $oSaleOrder = $this->fillVo($aData, $oSaleOrder);
		     $oSaleOrder->save();
		 }
		return $oSaleOrder;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrder $oModel
	 * @return SaleOrder
	 */
	protected function fillVo(array $aData, SaleOrder $oModel): SaleOrder
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['is_closed'])) {
		     $oField = new IsClosed();
		     $mValue = $oField->sanitize($aData['is_closed']);
		     $oModel->setIsClosed($mValue);
		}
		if(isset($aData['is_fully_paid'])) {
		     $oField = new IsFullyPaid();
		     $mValue = $oField->sanitize($aData['is_fully_paid']);
		     $oModel->setIsFullyPaid($mValue);
		}
		if(isset($aData['pay_date'])) {
		     $oField = new PayDate();
		     $mValue = $oField->sanitize($aData['pay_date']);
		     $oModel->setPayDate($mValue);
		}
		if(isset($aData['is_ready_for_shipping'])) {
		     $oField = new IsReadyForShipping();
		     $mValue = $oField->sanitize($aData['is_ready_for_shipping']);
		     $oModel->setIsReadyForShipping($mValue);
		}
		if(isset($aData['is_shipping_checked'])) {
		     $oField = new IsShippingChecked();
		     $mValue = $oField->sanitize($aData['is_shipping_checked']);
		     $oModel->setIsShippingChecked($mValue);
		}
		if(isset($aData['is_fully_shipped'])) {
		     $oField = new IsFullyShipped();
		     $mValue = $oField->sanitize($aData['is_fully_shipped']);
		     $oModel->setIsFullyShipped($mValue);
		}
		if(isset($aData['is_pushed_to_accounting'])) {
		     $oField = new IsPushedToAccounting();
		     $mValue = $oField->sanitize($aData['is_pushed_to_accounting']);
		     $oModel->setIsPushedToAccounting($mValue);
		}
		if(isset($aData['ship_date'])) {
		     $oField = new ShipDate();
		     $mValue = $oField->sanitize($aData['ship_date']);
		     $oModel->setShipDate($mValue);
		}
		if(isset($aData['sale_order_status_id'])) {
		     $oField = new SaleOrderStatusId();
		     $mValue = $oField->sanitize($aData['sale_order_status_id']);
		     $oModel->setSaleOrderStatusId($mValue);
		}
		if(isset($aData['handled_by'])) {
		     $oField = new HandledBy();
		     $mValue = $oField->sanitize($aData['handled_by']);
		     $oModel->setHandledBy($mValue);
		}
		if(isset($aData['source'])) {
		     $oField = new Source();
		     $mValue = $oField->sanitize($aData['source']);
		     $oModel->setSource($mValue);
		}
		if(isset($aData['is_completed'])) {
		     $oField = new IsCompleted();
		     $mValue = $oField->sanitize($aData['is_completed']);
		     $oModel->setIsCompleted($mValue);
		}
		if(isset($aData['created_by_user_id'])) {
		     $oField = new CreatedByUserId();
		     $mValue = $oField->sanitize($aData['created_by_user_id']);
		     $oModel->setCreatedByUserId($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['estimated_delivery_date'])) {
		     $oField = new EstimatedDeliveryDate();
		     $mValue = $oField->sanitize($aData['estimated_delivery_date']);
		     $oModel->setEstimatedDeliveryDate($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['envelope_weight_grams'])) {
		     $oField = new EnvelopeWeightGrams();
		     $mValue = $oField->sanitize($aData['envelope_weight_grams']);
		     $oModel->setEnvelopeWeightGrams($mValue);
		}
		if(isset($aData['webshop_user_session_id'])) {
		     $oField = new WebshopUserSessionId();
		     $mValue = $oField->sanitize($aData['webshop_user_session_id']);
		     $oModel->setWebshopUserSessionId($mValue);
		}
		if(isset($aData['invoice_number'])) {
		     $oField = new InvoiceNumber();
		     $mValue = $oField->sanitize($aData['invoice_number']);
		     $oModel->setInvoiceNumber($mValue);
		}
		if(isset($aData['invoice_date'])) {
		     $oField = new InvoiceDate();
		     $mValue = $oField->sanitize($aData['invoice_date']);
		     $oModel->setInvoiceDate($mValue);
		}
		if(isset($aData['payterm_days'])) {
		     $oField = new PaytermDays();
		     $mValue = $oField->sanitize($aData['payterm_days']);
		     $oModel->setPaytermDays($mValue);
		}
		if(isset($aData['payterm_original_id'])) {
		     $oField = new PaytermOriginalId();
		     $mValue = $oField->sanitize($aData['payterm_original_id']);
		     $oModel->setPaytermOriginalId($mValue);
		}
		if(isset($aData['payterm_string'])) {
		     $oField = new PaytermString();
		     $mValue = $oField->sanitize($aData['payterm_string']);
		     $oModel->setPaytermString($mValue);
		}
		if(isset($aData['order_number'])) {
		     $oField = new OrderNumber();
		     $mValue = $oField->sanitize($aData['order_number']);
		     $oModel->setOrderNumber($mValue);
		}
		if(isset($aData['is_deleted'])) {
		     $oField = new ItemDeleted();
		     $mValue = $oField->sanitize($aData['is_deleted']);
		     $oModel->setItemDeleted($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['debitor'])) {
		     $oField = new Debitor();
		     $mValue = $oField->sanitize($aData['debitor']);
		     $oModel->setDebitor($mValue);
		}
		if(isset($aData['our_custom_folder'])) {
		     $oField = new OurCustomFolder();
		     $mValue = $oField->sanitize($aData['our_custom_folder']);
		     $oModel->setOurCustomFolder($mValue);
		}
		if(isset($aData['our_slogan'])) {
		     $oField = new OurSlogan();
		     $mValue = $oField->sanitize($aData['our_slogan']);
		     $oModel->setOurSlogan($mValue);
		}
		if(isset($aData['our_phone'])) {
		     $oField = new OurPhone();
		     $mValue = $oField->sanitize($aData['our_phone']);
		     $oModel->setOurPhone($mValue);
		}
		if(isset($aData['our_fax'])) {
		     $oField = new OurFax();
		     $mValue = $oField->sanitize($aData['our_fax']);
		     $oModel->setOurFax($mValue);
		}
		if(isset($aData['our_website'])) {
		     $oField = new OurWebsite();
		     $mValue = $oField->sanitize($aData['our_website']);
		     $oModel->setOurWebsite($mValue);
		}
		if(isset($aData['our_email'])) {
		     $oField = new OurEmail();
		     $mValue = $oField->sanitize($aData['our_email']);
		     $oModel->setOurEmail($mValue);
		}
		if(isset($aData['our_vat_number'])) {
		     $oField = new OurVatNumber();
		     $mValue = $oField->sanitize($aData['our_vat_number']);
		     $oModel->setOurVatNumber($mValue);
		}
		if(isset($aData['our_chamber_of_commerce'])) {
		     $oField = new OurChamberOfCommerce();
		     $mValue = $oField->sanitize($aData['our_chamber_of_commerce']);
		     $oModel->setOurChamberOfCommerce($mValue);
		}
		if(isset($aData['our_iban_number'])) {
		     $oField = new OurIbanNumber();
		     $mValue = $oField->sanitize($aData['our_iban_number']);
		     $oModel->setOurIbanNumber($mValue);
		}
		if(isset($aData['our_bic_number'])) {
		     $oField = new OurBicNumber();
		     $mValue = $oField->sanitize($aData['our_bic_number']);
		     $oModel->setOurBicNumber($mValue);
		}
		if(isset($aData['our_general_company_name'])) {
		     $oField = new OurGeneralCompanyName();
		     $mValue = $oField->sanitize($aData['our_general_company_name']);
		     $oModel->setOurGeneralCompanyName($mValue);
		}
		if(isset($aData['our_general_street'])) {
		     $oField = new OurGeneralStreet();
		     $mValue = $oField->sanitize($aData['our_general_street']);
		     $oModel->setOurGeneralStreet($mValue);
		}
		if(isset($aData['our_general_number'])) {
		     $oField = new OurGeneralNumber();
		     $mValue = $oField->sanitize($aData['our_general_number']);
		     $oModel->setOurGeneralNumber($mValue);
		}
		if(isset($aData['our_general_number_add'])) {
		     $oField = new OurGeneralNumberAdd();
		     $mValue = $oField->sanitize($aData['our_general_number_add']);
		     $oModel->setOurGeneralNumberAdd($mValue);
		}
		if(isset($aData['our_general_postal'])) {
		     $oField = new OurGeneralPostal();
		     $mValue = $oField->sanitize($aData['our_general_postal']);
		     $oModel->setOurGeneralPostal($mValue);
		}
		if(isset($aData['our_general_po_box'])) {
		     $oField = new OurGeneralPoBox();
		     $mValue = $oField->sanitize($aData['our_general_po_box']);
		     $oModel->setOurGeneralPoBox($mValue);
		}
		if(isset($aData['our_general_city'])) {
		     $oField = new OurGeneralCity();
		     $mValue = $oField->sanitize($aData['our_general_city']);
		     $oModel->setOurGeneralCity($mValue);
		}
		if(isset($aData['our_general_country'])) {
		     $oField = new OurGeneralCountry();
		     $mValue = $oField->sanitize($aData['our_general_country']);
		     $oModel->setOurGeneralCountry($mValue);
		}
		if(isset($aData['customer_phone'])) {
		     $oField = new CustomerPhone();
		     $mValue = $oField->sanitize($aData['customer_phone']);
		     $oModel->setCustomerPhone($mValue);
		}
		if(isset($aData['customer_fax'])) {
		     $oField = new CustomerFax();
		     $mValue = $oField->sanitize($aData['customer_fax']);
		     $oModel->setCustomerFax($mValue);
		}
		if(isset($aData['customer_email'])) {
		     $oField = new CustomerEmail();
		     $mValue = $oField->sanitize($aData['customer_email']);
		     $oModel->setCustomerEmail($mValue);
		}
		if(isset($aData['customer_chamber_of_commerce'])) {
		     $oField = new CustomerChamberOfCommerce();
		     $mValue = $oField->sanitize($aData['customer_chamber_of_commerce']);
		     $oModel->setCustomerChamberOfCommerce($mValue);
		}
		if(isset($aData['customer_vat_number'])) {
		     $oField = new CustomerVatNumber();
		     $mValue = $oField->sanitize($aData['customer_vat_number']);
		     $oModel->setCustomerVatNumber($mValue);
		}
		if(isset($aData['customer_order_reference'])) {
		     $oField = new CustomerOrderReference();
		     $mValue = $oField->sanitize($aData['customer_order_reference']);
		     $oModel->setCustomerOrderReference($mValue);
		}
		if(isset($aData['customer_order_placed_by'])) {
		     $oField = new CustomerOrderPlacedBy();
		     $mValue = $oField->sanitize($aData['customer_order_placed_by']);
		     $oModel->setCustomerOrderPlacedBy($mValue);
		}
		if(isset($aData['customer_invoice_address_id'])) {
		     $oField = new CustomerInvoiceAddressId();
		     $mValue = $oField->sanitize($aData['customer_invoice_address_id']);
		     $oModel->setCustomerInvoiceAddressId($mValue);
		}
		if(isset($aData['customer_invoice_company_name'])) {
		     $oField = new CustomerInvoiceCompanyName();
		     $mValue = $oField->sanitize($aData['customer_invoice_company_name']);
		     $oModel->setCustomerInvoiceCompanyName($mValue);
		}
		if(isset($aData['customer_invoice_attn_name'])) {
		     $oField = new CustomerInvoiceAttnName();
		     $mValue = $oField->sanitize($aData['customer_invoice_attn_name']);
		     $oModel->setCustomerInvoiceAttnName($mValue);
		}
		if(isset($aData['customer_invoice_street'])) {
		     $oField = new CustomerInvoiceStreet();
		     $mValue = $oField->sanitize($aData['customer_invoice_street']);
		     $oModel->setCustomerInvoiceStreet($mValue);
		}
		if(isset($aData['customer_invoice_number'])) {
		     $oField = new CustomerInvoiceNumber();
		     $mValue = $oField->sanitize($aData['customer_invoice_number']);
		     $oModel->setCustomerInvoiceNumber($mValue);
		}
		if(isset($aData['customer_invoice_number_add'])) {
		     $oField = new CustomerInvoiceNumberAdd();
		     $mValue = $oField->sanitize($aData['customer_invoice_number_add']);
		     $oModel->setCustomerInvoiceNumberAdd($mValue);
		}
		if(isset($aData['customer_invoice_postal'])) {
		     $oField = new CustomerInvoicePostal();
		     $mValue = $oField->sanitize($aData['customer_invoice_postal']);
		     $oModel->setCustomerInvoicePostal($mValue);
		}
		if(isset($aData['customer_invoice_city'])) {
		     $oField = new CustomerInvoiceCity();
		     $mValue = $oField->sanitize($aData['customer_invoice_city']);
		     $oModel->setCustomerInvoiceCity($mValue);
		}
		if(isset($aData['customer_invoice_country'])) {
		     $oField = new CustomerInvoiceCountry();
		     $mValue = $oField->sanitize($aData['customer_invoice_country']);
		     $oModel->setCustomerInvoiceCountry($mValue);
		}
		if(isset($aData['customer_invoice_usa_state'])) {
		     $oField = new CustomerInvoiceUsaState();
		     $mValue = $oField->sanitize($aData['customer_invoice_usa_state']);
		     $oModel->setCustomerInvoiceUsaState($mValue);
		}
		if(isset($aData['customer_invoice_address_l1'])) {
		     $oField = new CustomerInvoiceAddressL1();
		     $mValue = $oField->sanitize($aData['customer_invoice_address_l1']);
		     $oModel->setCustomerInvoiceAddressL1($mValue);
		}
		if(isset($aData['customer_invoice_address_l2'])) {
		     $oField = new CustomerInvoiceAddressL2();
		     $mValue = $oField->sanitize($aData['customer_invoice_address_l2']);
		     $oModel->setCustomerInvoiceAddressL2($mValue);
		}
		if(isset($aData['customer_delivery_address_id'])) {
		     $oField = new CustomerDeliveryAddressId();
		     $mValue = $oField->sanitize($aData['customer_delivery_address_id']);
		     $oModel->setCustomerDeliveryAddressId($mValue);
		}
		if(isset($aData['customer_delivery_company_name'])) {
		     $oField = new CustomerDeliveryCompanyName();
		     $mValue = $oField->sanitize($aData['customer_delivery_company_name']);
		     $oModel->setCustomerDeliveryCompanyName($mValue);
		}
		if(isset($aData['customer_delivery_attn_name'])) {
		     $oField = new CustomerDeliveryAttnName();
		     $mValue = $oField->sanitize($aData['customer_delivery_attn_name']);
		     $oModel->setCustomerDeliveryAttnName($mValue);
		}
		if(isset($aData['customer_delivery_street'])) {
		     $oField = new CustomerDeliveryStreet();
		     $mValue = $oField->sanitize($aData['customer_delivery_street']);
		     $oModel->setCustomerDeliveryStreet($mValue);
		}
		if(isset($aData['customer_delivery_number'])) {
		     $oField = new CustomerDeliveryNumber();
		     $mValue = $oField->sanitize($aData['customer_delivery_number']);
		     $oModel->setCustomerDeliveryNumber($mValue);
		}
		if(isset($aData['customer_delivery_number_add'])) {
		     $oField = new CustomerDeliveryNumberAdd();
		     $mValue = $oField->sanitize($aData['customer_delivery_number_add']);
		     $oModel->setCustomerDeliveryNumberAdd($mValue);
		}
		if(isset($aData['customer_delivery_postal'])) {
		     $oField = new CustomerDeliveryPostal();
		     $mValue = $oField->sanitize($aData['customer_delivery_postal']);
		     $oModel->setCustomerDeliveryPostal($mValue);
		}
		if(isset($aData['customer_delivery_city'])) {
		     $oField = new CustomerDeliveryCity();
		     $mValue = $oField->sanitize($aData['customer_delivery_city']);
		     $oModel->setCustomerDeliveryCity($mValue);
		}
		if(isset($aData['customer_delivery_country'])) {
		     $oField = new CustomerDeliveryCountry();
		     $mValue = $oField->sanitize($aData['customer_delivery_country']);
		     $oModel->setCustomerDeliveryCountry($mValue);
		}
		if(isset($aData['customer_delivery_usa_state'])) {
		     $oField = new CustomerDeliveryUsaState();
		     $mValue = $oField->sanitize($aData['customer_delivery_usa_state']);
		     $oModel->setCustomerDeliveryUsaState($mValue);
		}
		if(isset($aData['customer_delivery_address_l1'])) {
		     $oField = new CustomerDeliveryAddressL1();
		     $mValue = $oField->sanitize($aData['customer_delivery_address_l1']);
		     $oModel->setCustomerDeliveryAddressL1($mValue);
		}
		if(isset($aData['customer_delivery_address_l2'])) {
		     $oField = new CustomerDeliveryAddressL2();
		     $mValue = $oField->sanitize($aData['customer_delivery_address_l2']);
		     $oModel->setCustomerDeliveryAddressL2($mValue);
		}
		if(isset($aData['cp_first_name'])) {
		     $oField = new CpFirstName();
		     $mValue = $oField->sanitize($aData['cp_first_name']);
		     $oModel->setCpFirstName($mValue);
		}
		if(isset($aData['cp_last_name'])) {
		     $oField = new CpLastName();
		     $mValue = $oField->sanitize($aData['cp_last_name']);
		     $oModel->setCpLastName($mValue);
		}
		if(isset($aData['cp_sallutation'])) {
		     $oField = new CpSallutation();
		     $mValue = $oField->sanitize($aData['cp_sallutation']);
		     $oModel->setCpSallutation($mValue);
		}
		if(isset($aData['reference'])) {
		     $oField = new Reference();
		     $mValue = $oField->sanitize($aData['reference']);
		     $oModel->setReference($mValue);
		}
		if(isset($aData['shipping_note'])) {
		     $oField = new ShippingNote();
		     $mValue = $oField->sanitize($aData['shipping_note']);
		     $oModel->setShippingNote($mValue);
		}
		if(isset($aData['customer_order_note'])) {
		     $oField = new CustomerOrderNote();
		     $mValue = $oField->sanitize($aData['customer_order_note']);
		     $oModel->setCustomerOrderNote($mValue);
		}
		if(isset($aData['shipping_order_note'])) {
		     $oField = new ShippingOrderNote();
		     $mValue = $oField->sanitize($aData['shipping_order_note']);
		     $oModel->setShippingOrderNote($mValue);
		}
		if(isset($aData['has_wrap'])) {
		     $oField = new HasWrap();
		     $mValue = $oField->sanitize($aData['has_wrap']);
		     $oModel->setHasWrap($mValue);
		}
		if(isset($aData['wrapping_price'])) {
		     $oField = new WrappingPrice();
		     $mValue = $oField->sanitize($aData['wrapping_price']);
		     $oModel->setWrappingPrice($mValue);
		}
		if(isset($aData['shipping_carrier'])) {
		     $oField = new ShippingCarrier();
		     $mValue = $oField->sanitize($aData['shipping_carrier']);
		     $oModel->setShippingCarrier($mValue);
		}
		if(isset($aData['shipping_price'])) {
		     $oField = new ShippingPrice();
		     $mValue = $oField->sanitize($aData['shipping_price']);
		     $oModel->setShippingPrice($mValue);
		}
		if(isset($aData['shipping_method_id'])) {
		     $oField = new ShippingMethodId();
		     $mValue = $oField->sanitize($aData['shipping_method_id']);
		     $oModel->setShippingMethodId($mValue);
		}
		if(isset($aData['shipping_has_track_trace'])) {
		     $oField = new ShippingHasTrackTrace();
		     $mValue = $oField->sanitize($aData['shipping_has_track_trace']);
		     $oModel->setShippingHasTrackTrace($mValue);
		}
		if(isset($aData['shipping_id'])) {
		     $oField = new ShippingId();
		     $mValue = $oField->sanitize($aData['shipping_id']);
		     $oModel->setShippingId($mValue);
		}
		if(isset($aData['tracking_code'])) {
		     $oField = new TrackingCode();
		     $mValue = $oField->sanitize($aData['tracking_code']);
		     $oModel->setTrackingCode($mValue);
		}
		if(isset($aData['tracking_url'])) {
		     $oField = new TrackingUrl();
		     $mValue = $oField->sanitize($aData['tracking_url']);
		     $oModel->setTrackingUrl($mValue);
		}
		if(isset($aData['paymethod_name'])) {
		     $oField = new PaymethodName();
		     $mValue = $oField->sanitize($aData['paymethod_name']);
		     $oModel->setPaymethodName($mValue);
		}
		if(isset($aData['paymethod_code'])) {
		     $oField = new PaymethodCode();
		     $mValue = $oField->sanitize($aData['paymethod_code']);
		     $oModel->setPaymethodCode($mValue);
		}
		if(isset($aData['paymethod_id'])) {
		     $oField = new PaymethodId();
		     $mValue = $oField->sanitize($aData['paymethod_id']);
		     $oModel->setPaymethodId($mValue);
		}
		if(isset($aData['shipping_registration_date'])) {
		     $oField = new ShippingRegistrationDate();
		     $mValue = $oField->sanitize($aData['shipping_registration_date']);
		     $oModel->setShippingRegistrationDate($mValue);
		}
		if(isset($aData['invoice_by_postal'])) {
		     $oField = new InvoiceByPostal();
		     $mValue = $oField->sanitize($aData['invoice_by_postal']);
		     $oModel->setInvoiceByPostal($mValue);
		}
		if(isset($aData['packing_slip_type'])) {
		     $oField = new PackingSlipType();
		     $mValue = $oField->sanitize($aData['packing_slip_type']);
		     $oModel->setPackingSlipType($mValue);
		}
		if(isset($aData['work_in_progress'])) {
		     $oField = new WorkInProgress();
		     $mValue = $oField->sanitize($aData['work_in_progress']);
		     $oModel->setWorkInProgress($mValue);
		}
		if(isset($aData['package_amount'])) {
		     $oField = new PackageAmount();
		     $mValue = $oField->sanitize($aData['package_amount']);
		     $oModel->setPackageAmount($mValue);
		}
		if(isset($aData['coupon_code'])) {
		     $oField = new CouponCode();
		     $mValue = $oField->sanitize($aData['coupon_code']);
		     $oModel->setCouponCode($mValue);
		}
		if(isset($aData['coupon_discount_amount'])) {
		     $oField = new CouponDiscountAmount();
		     $mValue = $oField->sanitize($aData['coupon_discount_amount']);
		     $oModel->setCouponDiscountAmount($mValue);
		}
		if(isset($aData['coupon_discount_percentage'])) {
		     $oField = new CouponDiscountPercentage();
		     $mValue = $oField->sanitize($aData['coupon_discount_percentage']);
		     $oModel->setCouponDiscountPercentage($mValue);
		}
		if(isset($aData['hide_untill'])) {
		     $oField = new HideUntill();
		     $mValue = $oField->sanitize($aData['hide_untill']);
		     $oModel->setHideUntill($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
