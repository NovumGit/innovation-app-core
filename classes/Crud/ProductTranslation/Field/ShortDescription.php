<?php
namespace Crud\ProductTranslation\Field;

use Crud\ProductTranslation\Field\Base\ShortDescription as BaseShortDescription;

/**
 * Skeleton subclass for representing short_description field from the product_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ShortDescription extends BaseShortDescription
{
}
