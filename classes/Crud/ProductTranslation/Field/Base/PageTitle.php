<?php
namespace Crud\ProductTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductTranslation\ICollectionField;

/**
 * Base class that represents the 'page_title' crud field from the 'product_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class PageTitle extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'page_title';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPageTitle';
	protected $sFqModelClassname = '\\\Model\ProductTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
