<?php
namespace Crud\ProductTranslation\Field;

use Crud\ProductTranslation\Field\Base\Modified as BaseModified;

/**
 * Skeleton subclass for representing modified field from the product_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Modified extends BaseModified
{
}
