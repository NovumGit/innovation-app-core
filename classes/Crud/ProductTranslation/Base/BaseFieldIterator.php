<?php
namespace Crud\ProductTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductTranslation\ICollectionField as ProductTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductTranslationField
	{
		return current($this->aFields);
	}
}
