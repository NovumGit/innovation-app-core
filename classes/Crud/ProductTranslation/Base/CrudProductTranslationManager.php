<?php
namespace Crud\ProductTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductTranslation\FieldIterator;
use Crud\ProductTranslation\Field\Description;
use Crud\ProductTranslation\Field\LanguageId;
use Crud\ProductTranslation\Field\MetaDescription;
use Crud\ProductTranslation\Field\MetaKeywords;
use Crud\ProductTranslation\Field\Modified;
use Crud\ProductTranslation\Field\PageTitle;
use Crud\ProductTranslation\Field\ProductId;
use Crud\ProductTranslation\Field\ShortDescription;
use Crud\ProductTranslation\Field\Title;
use Exception\LogicException;
use Model\Map\ProductTranslationTableMap;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductTranslation instead if you need to override or add functionality.
 */
abstract class CrudProductTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductTranslationQuery::create();
	}


	public function getTableMap(): ProductTranslationTableMap
	{
		return new ProductTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'LanguageId', 'Title', 'Description', 'ShortDescription', 'PageTitle', 'MetaKeywords', 'MetaDescription', 'Modified'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'LanguageId', 'Title', 'Description', 'ShortDescription', 'PageTitle', 'MetaKeywords', 'MetaDescription', 'Modified'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductTranslation
	 */
	public function getModel(array $aData = null): ProductTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductTranslationQuery = ProductTranslationQuery::create();
		     $oProductTranslation = $oProductTranslationQuery->findOneById($aData['id']);
		     if (!$oProductTranslation instanceof ProductTranslation) {
		         throw new LogicException("ProductTranslation should be an instance of ProductTranslation but got something else." . __METHOD__);
		     }
		     $oProductTranslation = $this->fillVo($aData, $oProductTranslation);
		}
		else {
		     $oProductTranslation = new ProductTranslation();
		     if (!empty($aData)) {
		         $oProductTranslation = $this->fillVo($aData, $oProductTranslation);
		     }
		}
		return $oProductTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductTranslation
	{
		$oProductTranslation = $this->getModel($aData);


		 if(!empty($oProductTranslation))
		 {
		     $oProductTranslation = $this->fillVo($aData, $oProductTranslation);
		     $oProductTranslation->save();
		 }
		return $oProductTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductTranslation $oModel
	 * @return ProductTranslation
	 */
	protected function fillVo(array $aData, ProductTranslation $oModel): ProductTranslation
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['short_description'])) {
		     $oField = new ShortDescription();
		     $mValue = $oField->sanitize($aData['short_description']);
		     $oModel->setShortDescription($mValue);
		}
		if(isset($aData['page_title'])) {
		     $oField = new PageTitle();
		     $mValue = $oField->sanitize($aData['page_title']);
		     $oModel->setPageTitle($mValue);
		}
		if(isset($aData['meta_keywords'])) {
		     $oField = new MetaKeywords();
		     $mValue = $oField->sanitize($aData['meta_keywords']);
		     $oModel->setMetaKeywords($mValue);
		}
		if(isset($aData['meta_description'])) {
		     $oField = new MetaDescription();
		     $mValue = $oField->sanitize($aData['meta_description']);
		     $oModel->setMetaDescription($mValue);
		}
		if(isset($aData['modified'])) {
		     $oField = new Modified();
		     $mValue = $oField->sanitize($aData['modified']);
		     $oModel->setModified($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
