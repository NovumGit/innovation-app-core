<?php
namespace Crud;


interface IConfigurableCrud extends IEditableCrud{
    function getOverviewUrl():string;
    function getAllAvailableFields(); // Loop over de fields map om deze op te halen zie ook Product crud manager
}
