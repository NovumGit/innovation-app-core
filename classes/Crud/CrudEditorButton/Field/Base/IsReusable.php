<?php
namespace Crud\CrudEditorButton\Field\Base;

use Crud\CrudEditorButton\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_reusable' crud field from the 'crud_editor_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsReusable extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_reusable';
	protected $sFieldLabel = 'Herbruikbaar';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsReusable';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButton';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
