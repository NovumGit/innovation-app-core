<?php
namespace Crud\CrudEditorButton\Field\Base;

use Crud\CrudEditorButton\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'color_after_click' crud field from the 'crud_editor_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class ColorAfterClick extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'color_after_click';
	protected $sFieldLabel = 'Kleur na klik';
	protected $sIcon = 'eye';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getColorAfterClick';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButton';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
