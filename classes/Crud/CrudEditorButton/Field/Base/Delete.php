<?php 
namespace Crud\CrudEditorButton\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditorButton;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButton)
		{
		     return "//system/crud_editor_button/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButton)
		{
		     return "//crud_editor_button?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
