<?php
namespace Crud\CrudEditorButton\Field\Base;

use Crud\CrudEditorButton\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'icon_after_click' crud field from the 'crud_editor_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class IconAfterClick extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'icon_after_click';
	protected $sFieldLabel = 'Icoon na klik';
	protected $sIcon = 'picture-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIconAfterClick';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButton';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
