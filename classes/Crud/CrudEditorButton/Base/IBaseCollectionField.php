<?php
namespace Crud\CrudEditorButton\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\CrudEditorButton\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
