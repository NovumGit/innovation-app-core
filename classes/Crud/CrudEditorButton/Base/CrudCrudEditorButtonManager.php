<?php
namespace Crud\CrudEditorButton\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditorButton\FieldIterator;
use Crud\CrudEditorButton\Field\ColorAfterClick;
use Crud\CrudEditorButton\Field\ColorBeforeClick;
use Crud\CrudEditorButton\Field\CrudEditorId;
use Crud\CrudEditorButton\Field\IconAfterClick;
use Crud\CrudEditorButton\Field\IconBeforeClick;
use Crud\CrudEditorButton\Field\IsReusable;
use Crud\CrudEditorButton\Field\Sorting;
use Crud\CrudEditorButton\Field\Title;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditorButton instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorButtonManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonQuery::create();
	}


	public function getTableMap(): CrudEditorButtonTableMap
	{
		return new CrudEditorButtonTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn alle door gebruikers gedefinieerde knoppen opgeslagen die van toepassing zijn op een enkel item";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditorButton";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor_button toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor_button aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'IsReusable', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'IsReusable', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditorButton
	 */
	public function getModel(array $aData = null): CrudEditorButton
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorButtonQuery = CrudEditorButtonQuery::create();
		     $oCrudEditorButton = $oCrudEditorButtonQuery->findOneById($aData['id']);
		     if (!$oCrudEditorButton instanceof CrudEditorButton) {
		         throw new LogicException("CrudEditorButton should be an instance of CrudEditorButton but got something else." . __METHOD__);
		     }
		     $oCrudEditorButton = $this->fillVo($aData, $oCrudEditorButton);
		}
		else {
		     $oCrudEditorButton = new CrudEditorButton();
		     if (!empty($aData)) {
		         $oCrudEditorButton = $this->fillVo($aData, $oCrudEditorButton);
		     }
		}
		return $oCrudEditorButton;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditorButton
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditorButton
	{
		$oCrudEditorButton = $this->getModel($aData);


		 if(!empty($oCrudEditorButton))
		 {
		     $oCrudEditorButton = $this->fillVo($aData, $oCrudEditorButton);
		     $oCrudEditorButton->save();
		 }
		return $oCrudEditorButton;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditorButton $oModel
	 * @return CrudEditorButton
	 */
	protected function fillVo(array $aData, CrudEditorButton $oModel): CrudEditorButton
	{
		if(isset($aData['crud_editor_id'])) {
		     $oField = new CrudEditorId();
		     $mValue = $oField->sanitize($aData['crud_editor_id']);
		     $oModel->setCrudEditorId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['icon_before_click'])) {
		     $oField = new IconBeforeClick();
		     $mValue = $oField->sanitize($aData['icon_before_click']);
		     $oModel->setIconBeforeClick($mValue);
		}
		if(isset($aData['icon_after_click'])) {
		     $oField = new IconAfterClick();
		     $mValue = $oField->sanitize($aData['icon_after_click']);
		     $oModel->setIconAfterClick($mValue);
		}
		if(isset($aData['color_before_click'])) {
		     $oField = new ColorBeforeClick();
		     $mValue = $oField->sanitize($aData['color_before_click']);
		     $oModel->setColorBeforeClick($mValue);
		}
		if(isset($aData['color_after_click'])) {
		     $oField = new ColorAfterClick();
		     $mValue = $oField->sanitize($aData['color_after_click']);
		     $oModel->setColorAfterClick($mValue);
		}
		if(isset($aData['is_reusable'])) {
		     $oField = new IsReusable();
		     $mValue = $oField->sanitize($aData['is_reusable']);
		     $oModel->setIsReusable($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
