<?php
namespace Crud\CrudEditorButton\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudEditorButton\ICollectionField as CrudEditorButtonField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudEditorButton\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudEditorButtonField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudEditorButtonField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudEditorButtonField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudEditorButtonField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudEditorButtonField
	{
		return current($this->aFields);
	}
}
