<?php
namespace Crud\Desktop_widgets;

use Crud\Link;

abstract class AbstractDestopWidgetField extends Link
{
    function getLinkUrl()
    {
        return null;
    }

    abstract function getHtml();

}