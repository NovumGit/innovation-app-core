<?php
namespace Crud\Desktop_widgets\Field;

use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;
use Model\Sale\SaleOrderQuery;

class Day_turnover extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Dag omzet';


    function getHtml()
    {
        $sSearchDate = date('Y-m-d');;
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrderQuery->filterByPayDate(["min" => $sSearchDate." 00:00:00", "max" => $sSearchDate." 23:59:59"]);
        $fTotal = 0;

        $aSaleOrders = $oSaleOrderQuery->find();

        foreach($aSaleOrders as $oSaleOrder)
        {
            $fTotal = $fTotal + $oSaleOrder->getTotalPrice();
        }

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-3">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/clipart1.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                    <h6 class="text-muted">'.Translate::fromCode('DAGOMZET').'</h6>';
        $aOut[] = '                    <h2 class="fs40 mt5 mbn">&euro; '.number_format($fTotal, 2, ',', '.').'</h2>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}