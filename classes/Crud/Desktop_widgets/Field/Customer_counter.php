<?php
namespace Crud\Desktop_widgets\Field;

use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;
use Model\Crm\CustomerQuery;

class Customer_counter extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Aantal klanten teller';

    function getHtml()
    {
        $iCustomerCount = CustomerQuery::create()->count();

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-3">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/clipart2.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                     <h6 class="text-muted">'.Translate::fromCode('KLANTEN').'</h6>';
        $aOut[] = '                    <h3 class="fs40 mt5 mbn">'.number_format($iCustomerCount, 0, ",", ".").'</h3>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}