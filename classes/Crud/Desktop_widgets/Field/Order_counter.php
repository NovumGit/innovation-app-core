<?php
namespace Crud\Desktop_widgets\Field;

use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;
use Model\Sale\SaleOrderQuery;

class Order_counter extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Aantal bestellingen';

    function getHtml()
    {
        $iTotalSaleOrderCount = SaleOrderQuery::create()->filterByIsFullyPaid(true)->count();

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-3">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/clipart0.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                    <h6 class="text-muted">'.Translate::fromCode('BESTELLINGEN').'</h6>';
        $aOut[] = '                    <h3 class="fs40 mt5 mbn">'.number_format($iTotalSaleOrderCount, 0, ',', '.').'</h3>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}