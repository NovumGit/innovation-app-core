<?php
namespace Crud\Desktop_widgets\Field\LitamsIot;

use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;

class DownloadImage extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Image downloaden';

    function getHtml()
    {

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-4">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/download.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                    <h6 class="text-muted">'.Translate::fromCode('Nieuwste image downloaden').'</h6>';
        $aOut[] = '                    <h7 class="fs20 mt5 mbn">';
        $aOut[] = '                         <a href="https://downloads.litams.com/ads2.0-latest.img">Download hier</a>';
        $aOut[] = '                    </h7>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}