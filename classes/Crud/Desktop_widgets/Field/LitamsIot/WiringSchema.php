<?php
namespace Crud\Desktop_widgets\Field\LitamsIot;

use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;
use Model\Sale\SaleOrderQuery;

class WiringSchema extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Aansluitschema';

    function getHtml()
    {

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-4">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/pcb.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                    <h6 class="text-muted">'.Translate::fromCode('Aansluitschema').'</h6>';
        $aOut[] = '                    <a href="/custom/litamsiot/ads/wiring">';
        $aOut[] = '                     Bekijk aansluitschema';
        $aOut[] = '                    </a>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}