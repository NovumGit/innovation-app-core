<?php
namespace Crud\Desktop_widgets\Field\LitamsIot;

use Core\QueryMapper;
use Core\Translate;
use Crud\Desktop_widgets\AbstractDestopWidgetField;

class Lifenumbers_today extends AbstractDestopWidgetField
{
    protected $sFieldLabel = 'Levensnummers vandaag';

    function getHtml()
    {
        $sQuery  = 'SELECT 
                      COUNT(ip) unique_visitors_today 
                    FROM 
                      unique_visitor 
                    WHERE 
                    `visit_date` = "' . date('Y-m-d') . '" ';

        $iUniqueVistorsToday  = QueryMapper::fetchVal($sQuery);

        $aOut = [];
        $aOut[] = '<div class="col-sm-6 col-xl-4">';
        $aOut[] = '    <div class="panel panel-tile">';
        $aOut[] = '        <div class="panel-body">';
        $aOut[] = '            <div class="row pv10">';
        $aOut[] = '                <div class="col-xs-5 ph10">';
        $aOut[] = '                    <img src="/assets/img/pages/graph.png" class="img-responsive mauto" alt=""/>';
        $aOut[] = '                </div>';
        $aOut[] = '                <div class="col-xs-7 pl5">';
        $aOut[] = '                    <h6 class="text-muted">'.Translate::fromCode('SORTERINGEN VANDAAG').'</h6>';
        $aOut[] = '                    <h2 class="fs40 mt5 mbn">x</h2>';
        $aOut[] = '                </div>';
        $aOut[] = '            </div>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';

        return join(PHP_EOL, $aOut);
    }
}