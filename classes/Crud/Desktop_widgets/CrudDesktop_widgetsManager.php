<?php
namespace Crud\Desktop_widgets;

use Crud\Link_blockManager;

class CrudDesktop_widgetsManager extends Link_blockManager  {

    function getDefaultEditFields(bool $bAddNs = false):?array
    {
        return $this->getDefaultOverviewFields( $bAddNs);
    }
    function getDefaultOverviewFields(bool $bAddNs = false):?array
    {
        return [
            /*
            'Customer_counter',
            'Order_counter',
            'Day_turnover',
            'Unique_visitors',
            */
        ];
    }
}
