<?php
namespace Crud\Link_block_menu_products;

use Core\QueryMapper;
use Model\ProductList;
use Model\ProductListQuery;
use Model\CustomerQuery;

trait ProductListTrait
{
    abstract function getListName():string;
    abstract function removeFromList(int $iItemId);

    function getLinkUrl()
    {
        return '/product/lists/editor?list_name='.$this->getListName();
    }

    function getList(): array
    {
        $sQuery = "SELECT 
                        pp.product_id, 
                        pl.sorting,
                        pl.created_at
                    FROM                         
                        product_property pp
                        LEFT JOIN product_list pl ON pl.product_id = pp.product_id AND pl.list_name = pp.field  
                    WHERE
                        pp.field = '".$this->getListName()."'
                    AND  pp.value = 1
                    ORDER BY pl.sorting ASC";

        $aProducts = QueryMapper::fetchArray($sQuery);

        $aResultInOrder = [];

        if(is_array($aProducts))
        {
            foreach($aProducts as $aProduct)
            {
                $oProduct = CustomerQuery::create()->findOneById($aProduct['product_id']);
                $oProduct->setArgument('created_at', $aProduct['created_at']);
                $aResultInOrder[] = $oProduct;
            }
        }
        return $aResultInOrder;
    }



    function storeListOrder(array $aProductIds)
    {
        $oProductListQuery = ProductListQuery::create();
        $oProductListQuery->filterByListName($this->getListName());
        $oProductListQuery->delete();

        foreach($aProductIds as $iIndex => $iProductId)
        {
            $oProductList = new ProductList();
            $oProductList->setSorting($iIndex);
            $oProductList->setListName($this->getListName());
            $oProductList->setProductId($iProductId);
            $oProductList->save();
        }
    }
}