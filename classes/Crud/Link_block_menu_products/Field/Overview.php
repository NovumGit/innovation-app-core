<?php
namespace Crud\Link_block_menu_products\Field;

use Crud\Link;

class Overview extends Link
{
    protected $sFieldLabel = 'Overzicht';

    function getLinkUrl()
    {
        return '/product/overview';
    }
}