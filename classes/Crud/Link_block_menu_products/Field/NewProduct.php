<?php
namespace Crud\Link_block_menu_products\Field;

use Crud\Link;

class NewProduct extends Link
{
    protected $sFieldLabel = '[+] New product';

    function getLinkUrl()
    {
        return '/product/edit';
    }
}