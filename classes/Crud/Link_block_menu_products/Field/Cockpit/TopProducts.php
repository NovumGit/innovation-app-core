<?php
namespace Crud\Link_block_menu_products\Field\Cockpit;

use Crud\Link;

class TopProducts extends Link
{
    protected $sFieldLabel = 'Top ingrediënten';

    function getLinkUrl()
    {
        return '/custom/cockpit/foodbox/ingredient/top_ingredients';
    }
}