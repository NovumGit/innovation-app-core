<?php
namespace Crud\Link_block_menu_products\Field\Cockpit;

use Crud\Link;

class Overview extends Link
{
    protected $sFieldLabel = 'Overzicht';

    function getLinkUrl()
    {
        return '/custom/cockpit/foodbox/product/overview';
    }
}
