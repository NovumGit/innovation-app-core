<?php
namespace Crud\Link_block_menu_products;

use Crud\Link_blockManager;

class CrudLink_block_menu_productsManager extends Link_blockManager  {


    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return $this->getDefaultOverviewFields($bAddNamespaceUnimplemented);
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Find',
            'NewProduct',
        ];
    }
}
