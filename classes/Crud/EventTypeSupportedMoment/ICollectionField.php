<?php
namespace Crud\EventTypeSupportedMoment;

/**
 * Skeleton interface used for grouping fields that are belong to EventTypeSupportedMoment.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this interface to meet your application requirements.
 * This interface will only be generated once / when it does not exist already.
 */
interface ICollectionField extends Base\IBaseCollectionField
{
}
