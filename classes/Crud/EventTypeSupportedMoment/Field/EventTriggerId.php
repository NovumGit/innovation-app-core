<?php
namespace Crud\EventTypeSupportedMoment\Field;

use Crud\EventTypeSupportedMoment\Field\Base\EventTriggerId as BaseEventTriggerId;

/**
 * Skeleton subclass for representing event_trigger_id field from the event_type_supported_trigger table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EventTriggerId extends BaseEventTriggerId
{
}
