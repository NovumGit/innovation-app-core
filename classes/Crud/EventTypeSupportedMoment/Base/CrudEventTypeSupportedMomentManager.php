<?php
namespace Crud\EventTypeSupportedMoment\Base;

use Core\Utils;
use Crud;
use Crud\EventTypeSupportedMoment\FieldIterator;
use Crud\EventTypeSupportedMoment\Field\EventTriggerId;
use Crud\EventTypeSupportedMoment\Field\EventTypeId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\Event\EventTypeSupportedMoment;
use Model\System\Event\EventTypeSupportedMomentQuery;
use Model\System\Event\Map\EventTypeSupportedMomentTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify EventTypeSupportedMoment instead if you need to override or add functionality.
 */
abstract class CrudEventTypeSupportedMomentManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return EventTypeSupportedMomentQuery::create();
	}


	public function getTableMap(): EventTypeSupportedMomentTableMap
	{
		return new EventTypeSupportedMomentTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit is vastgelegd wanneer een event getriggered kan worden.";
	}


	public function getEntityTitle(): string
	{
		return "EventTypeSupportedMoment";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "event_type_supported_trigger toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "event_type_supported_trigger aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['EventTypeId', 'EventTriggerId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['EventTypeId', 'EventTriggerId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return EventTypeSupportedMoment
	 */
	public function getModel(array $aData = null): EventTypeSupportedMoment
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oEventTypeSupportedMomentQuery = EventTypeSupportedMomentQuery::create();
		     $oEventTypeSupportedMoment = $oEventTypeSupportedMomentQuery->findOneById($aData['id']);
		     if (!$oEventTypeSupportedMoment instanceof EventTypeSupportedMoment) {
		         throw new LogicException("EventTypeSupportedMoment should be an instance of EventTypeSupportedMoment but got something else." . __METHOD__);
		     }
		     $oEventTypeSupportedMoment = $this->fillVo($aData, $oEventTypeSupportedMoment);
		}
		else {
		     $oEventTypeSupportedMoment = new EventTypeSupportedMoment();
		     if (!empty($aData)) {
		         $oEventTypeSupportedMoment = $this->fillVo($aData, $oEventTypeSupportedMoment);
		     }
		}
		return $oEventTypeSupportedMoment;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return EventTypeSupportedMoment
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): EventTypeSupportedMoment
	{
		$oEventTypeSupportedMoment = $this->getModel($aData);


		 if(!empty($oEventTypeSupportedMoment))
		 {
		     $oEventTypeSupportedMoment = $this->fillVo($aData, $oEventTypeSupportedMoment);
		     $oEventTypeSupportedMoment->save();
		 }
		return $oEventTypeSupportedMoment;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param EventTypeSupportedMoment $oModel
	 * @return EventTypeSupportedMoment
	 */
	protected function fillVo(array $aData, EventTypeSupportedMoment $oModel): EventTypeSupportedMoment
	{
		if(isset($aData['event_type_id'])) {
		     $oField = new EventTypeId();
		     $mValue = $oField->sanitize($aData['event_type_id']);
		     $oModel->setEventTypeId($mValue);
		}
		if(isset($aData['event_trigger_id'])) {
		     $oField = new EventTriggerId();
		     $mValue = $oField->sanitize($aData['event_trigger_id']);
		     $oModel->setEventTriggerId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
