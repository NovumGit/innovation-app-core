<?php
namespace Crud\EventTypeSupportedMoment\Base;

use Crud\BaseCrudFieldIterator;
use Crud\EventTypeSupportedMoment\ICollectionField as EventTypeSupportedMomentField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\EventTypeSupportedMoment\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param EventTypeSupportedMomentField[] $aFields */
	private array $aFields = [];


	/**
	 * @param EventTypeSupportedMomentField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof EventTypeSupportedMomentField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(EventTypeSupportedMomentField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): EventTypeSupportedMomentField
	{
		return current($this->aFields);
	}
}
