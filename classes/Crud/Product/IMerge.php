<?php
namespace Crud\Product;

/**
 * Interface IMerge
 *
 * Use this interface to be able to merge products without any questions asked.
 * You should write all the merge code inside.
 *
 * @package Crud\Product
 */
interface IMerge
{
	public function merge($iProductFrom, $iProductTo);
}