<?php
namespace Crud\Product;

use Exception\LogicException;
use Crud\IConfigurableCrud;

class CrudProductPromoSelectorManager extends CrudProductManager implements IConfigurableCrud
{
    function getOverviewUrl():string
    {
        return '/cms/promoproducts/overview?site='.$_GET['site'];
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Number',
            'Title',
            'Edit',
            'AddPromo',
        ];
    }
    function store(array $aData = null)
    {
        throw new LogicException(__CLASS__.'::'.__METHOD__.' should never be called');
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        throw new LogicException(__CLASS__.'::'.__METHOD__.' should never be called');
    }
}
