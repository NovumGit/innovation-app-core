<?php
namespace Crud\Product;

use Exception\LogicException;
use Crud\IConfigurableCrud;

class CrudProductOrderSelectorManager extends CrudProductManager implements IConfigurableCrud
{
    function getOverviewUrl():string
    {
        return '/product/overview';
    }
	function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
	{
		return [
			'Number',
			'Description',
			'VatId',
			'Width',
			'Height',
			'Thickness',
			'Add',
		];
	}
	function store(array $aData = null)
	{
		throw new LogicException(__CLASS__.'::'.__METHOD__.' should never be called');
	}
	function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
		throw new LogicException(__CLASS__.'::'.__METHOD__.' should never be called');
	}
}
