<?php
namespace Crud\Product;

use Exception\LogicException;
use Crud\IConfigurableCrud;

class CrudProductWebsiteInfoManager extends CrudProductManager implements IConfigurableCrud
{
    function getOverviewUrl():string
    {
        return null;
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Number',
            'Description',
        ];
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return null;
    }
    function store(array $aData = null)
    {
        throw new LogicException(__CLASS__.'::'.__METHOD__.' should never be called');
    }
}
