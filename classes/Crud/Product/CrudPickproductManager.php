<?php
namespace Crud\Product;

use Crud\IConfigurableCrud;

class CrudPickproductManager extends CrudProductManager implements IConfigurableCrud
{
    public $sOverviewUrl = '/product/pickproduct?r=after_pick_product';

    function setOverviewUrl($sOverviewUrl)
    {
        $this->sOverviewUrl = $sOverviewUrl;
    }
    function getOverviewUrl():string
    {
        return $this->sOverviewUrl;
    }

}
