<?php
namespace Crud\Product\Field;

use Crud\Product\Field\Base\ImportTag as BaseImportTag;

/**
 * Skeleton subclass for representing import_tag field from the product table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ImportTag extends BaseImportTag
{
}
