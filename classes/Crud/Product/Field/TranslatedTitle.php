<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\User;
use Crud\Field;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Product as ModelObject;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;

class TranslatedTitle extends Field {

    protected $sFieldName = 'Vertaalde titel';
    protected $sFieldLabel = 'Vertaalde titel';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        $iLanguageId = null;
        if(User::isSignedIn())
        {
            $oUser = User::getMember();
            $iLanguageId = $oUser->getPreferedLanguageId();
        }
        else
        {
            throw new LogicException("The Translated Title field can at the moment only be used in the admin panel.");
        }

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $iTranslatedProductProductId = $oModelObject->getDerrivedFromId();

        if(!$iTranslatedProductProductId)
        {
            $iTranslatedProductProductId = $oModelObject->getId();
        }

        $sTitle = '';
        $oProductTranslation = ProductTranslationQuery::create()
                                ->filterByProductId($iTranslatedProductProductId)
                                ->filterByLanguageId($iLanguageId)
                                ->findOne();
        if($oProductTranslation instanceof ProductTranslation)
        {
            $sTitle = $oProductTranslation->getTitle();
        }
        if(!$sTitle)
        {
            $sTitle = $oModelObject->getTitle();
        }

        return '<td class="">'.$sTitle.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        return 'overview only';
    }
}