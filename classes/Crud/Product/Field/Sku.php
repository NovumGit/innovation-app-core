<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;

class Sku extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'sku';
    protected $sFieldLabel = 'Stock keeping unit (sku)';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getSku';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td>'.$oModelObject->getSku().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->getSku(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
