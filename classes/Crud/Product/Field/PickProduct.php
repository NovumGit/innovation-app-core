<?php
namespace Crud\Product\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;

class PickProduct extends Field implements IEventField{

    protected $sFieldLabel = 'Product kiezen';

    function getIcon()
    {
        return 'magic';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }
    function getOverviewValue($mData)
    {
        if(!$mData instanceof Product)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $oProduct = $mData;

        $sReturnUrl = DeferredAction::get('after_pick_product');

        if(strpos($sReturnUrl, '?'))
        {
            $sReturnUrl = $sReturnUrl.'&product_id='.$oProduct->getId();
        }
        else
        {
            $sReturnUrl = $sReturnUrl.'?product_id='.$oProduct->getId();
        }

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = "    <a href=\"$sReturnUrl\" class=\"btn btn-success br2 btn-xs fs12 d\">";
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </a>';

        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }
    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
