<?php
namespace Crud\Product\Field;

use Core\DeferredAction;
use Crud\CrudViewManager;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Crud\Product\CrudDerrivedProductManager;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Base\ProductQuery;
use Model\Product;
use Model\Setting\CrudManager\CrudView;

class DerrivedProductsEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'new_derrived_product';
    protected $sFieldLabel = 'Product variaties editor';


    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $aDerrivedProductsQuery = null;
        if($mData->getId())
        {
            $aDerrivedProductsQuery = ProductQuery::create()->filterByDerrivedFromId($mData->getId())->find();
        }

        if(!$aDerrivedProductsQuery)
        {
            return 'U kunt pas variaties op een product maken als het product is opgelagen.';
        } else {

            $oCrudDerrivedProductManager = new CrudDerrivedProductManager();

            $aCrudViews = CrudViewManager::getViews($oCrudDerrivedProductManager);
            $oCrudview = current($aCrudViews);
            $sTabId = '';
            if($oCrudview instanceof CrudView)
            {
                $sTabId = '&tab='.$oCrudview->getId();
            }

            // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
            if($aDerrivedProductsQuery->isEmpty())
            {
                $sHtmlTable = '<p>Er zijn nog geen product variaties gedefinieerd.</p><!--aftercrud-->';
            }
            else
            {

                $aFields = CrudViewManager::getFields($oCrudview);
                $oCrudDerrivedProductManager->setOverviewData($aDerrivedProductsQuery);

                $sHtmlTable = $oCrudDerrivedProductManager->getOverviewHtml($aFields);
            }

            DeferredAction::register('return_product_edit', '/product/edit?id=' . $mData->getId());

            $aEditLink[] = '    <div class="pull-left">';
            $aEditLink[] = '        <a href="/product/add_derrived?from_product_id='.$mData->getId().'">';
            $aEditLink[] = '            <i class="fa fa-plus"></i> Variatie toevoegen';
            $aEditLink[] = '        </a>';
            $aEditLink[] = '    </div>';
            $aEditLink[] = '    <div class="pull-right">';
            $aEditLink[] = '        <a href="/generic/crud/create?module=Product&manager=DerrivedProductManager&r=return_product_edit'.$sTabId.'">';
            $aEditLink[] = '            configureer deze tabel <i class="fa fa-gear"></i>';
            $aEditLink[] = '        </a>';
            $aEditLink[] = '    </div>';
            $sEditLink = join(PHP_EOL, $aEditLink);

            $sHtmlTable = str_replace('<!--aftercrud-->', $sEditLink, $sHtmlTable);

            return $sHtmlTable;
        }
    }
}
