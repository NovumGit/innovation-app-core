<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class Description extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'description';
    protected $sFieldLabel = 'Omschrijving';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getGetter()
    {
        return 'getDescription';
    }

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getDescription().'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return $this->editTextArea($this->getTranslatedTitle(), $this->sFieldName, $mData->getDescription(), $bReadonly);
    }
}
