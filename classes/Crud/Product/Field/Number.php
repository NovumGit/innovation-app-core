<?php
namespace Crud\Product\Field;

use Core\Setting;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;
use Model\CustomerQuery;

class Number extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'number';
    protected $sFieldLabel = 'Artikelnummer';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';

    function getGetter()
    {
        return 'getNumber';
    }

    function getDataType():string
    {
        return 'string';
    }

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function hasValidations() {
        return true;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['id']) && isset($aPostedData['number']))
        {

            $oProduct = CustomerQuery::create()->findOneByNumber($aPostedData['number']);

            if($oProduct instanceof Product)
            {
                $mResponse[] = 'Er is al een product met dit artikelnummer ('.$aPostedData['number'].') of in het verleden al een product met dit artikelnummer geweest.';
            }

            $sNumber = trim($aPostedData['number']);
            if(empty($sNumber))
            {

                $mResponse[] = 'Het veld artikelnummer mag niet leeg zijn.';


            }else if(strlen($aPostedData['number']) < 2){

                $mResponse[] = 'Het veld type artikelnummer moet minimaal 2 tekens lang zijn.';
            }
            else if(strlen($aPostedData['number']) > 120)
            {

                $mResponse[] = 'Het veld type artikelnummer mag maximaal 120 tekens lang zijn, en is nu '.strlen($aPostedData['number']).' tekens lang.';
            }
        }
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getNumber().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(Setting::get('customer_auto_product_numbering') == '1')
        {
            $bReadonly = true;
        }

        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__." got ".get_class($mData));
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getNumber(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
