<?php
namespace Crud\Product\Field;

use Crud\Generic\Field\GenericBoolean;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Product;

class VirtualStock extends GenericBoolean implements IFilterableField, IEditableField
{
    protected $sFieldName = 'virtual_stock';
    protected $sFieldLabel = 'Virtuele voorraad';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = null;
    protected $sGetter = 'getVirtualStock';
    protected $sFqModelClassname = Product::class;
}
