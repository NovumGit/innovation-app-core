<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\Supplier\Supplier as SupplierModel;
use Model\Product as ModelObject;
use Model\Supplier\SupplierQuery;

class SupplierId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'supplier';
    protected $sFieldLabel = 'Leverancier';
    private $sGetter = 'getSupplier';
    private $sPlaceholder = 'Leverancier';
    private $sIcon = 'car';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getGetter()
    {
        return 'getSupplier';
    }

    function getVisibleValue($iItemId)
    {
        $oSupplierQuery = SupplierQuery::create();
        $oSupplier = $oSupplierQuery->findOneById($iItemId);

        if($oSupplier instanceof SupplierModel)
        {
            return $oSupplier->getName();
        }
        return null;
    }
    function getLookups($mSelectedItem = null)
    {
        $oSupplierQuery = SupplierQuery::create()->orderByName();
        $oSupplierQuery->orderByName();
        $aSuppliers = $oSupplierQuery->find();
        return Utils::makeSelectOptions($aSuppliers, 'getName', $mSelectedItem);
    }

    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }


        $iSupplier = $oModelObject->getSupplierId();

        $oSupplier = new SupplierModel();
        if($iSupplier)
        {
            $oSupplier = SupplierQuery::create()->findOneById($iSupplier);
        }

        return '<td class="">' . $oSupplier->getName() . '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sValue = $oModelObject->{$this->sGetter}();
        $aOptions = $this->getLookups($sValue);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $sValue, $aOptions, $bReadonly, $this->sIcon, $this->sPlaceholder);
    }
}
