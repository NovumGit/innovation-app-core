<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Product;
use Model\Product\Image\ProductImageQuery;
use Model\Product\Image\ProductImage;

class ImageEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'new_product_image';
    protected $sFieldLabel = 'Product afbeeldingen editor';


    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aOut = [];

        $aProductImages = null;
        if($mData->getId())
        {
            $oProductImageQuery = ProductImageQuery::create();
            $oProductImageQuery->filterByProductId($mData->getId());
            $oProductImageQuery->orderBySorting();
            $aProductImages = $oProductImageQuery->find();
        }

        $aOut[] = '<ul class="sortable_container product_picker" data-method="SortImages" data-product_id="' . $mData->getId() . '">';

        if($aProductImages && $aProductImages->count())
        {

            foreach($aProductImages as $oProductImage)
            {
                if($oProductImage instanceof ProductImage)
                {
                    $aOut[] = '    <li class="sortable_item product_picker" data-data="{image_id : ' . $oProductImage->getId() . ' }">';
/*
                    $aOut[] = '         <a href="/generic/image_editor/editor?file=product/' . $oProductImage->getId() . '.' . $oProductImage->getFileExt() . '&sizes=120x120,75x75,200x150">';
                    $aOut[] = '            <i class="fa fa-crop icon"></i>';
                    $aOut[] = '         </a>';

                    $aOut[] = '         <a href="/product/edit?image_id='.$oProductImage->getId().'&_do=DeleteImage">';
                    $aOut[] = '            <i class="fa fa-edit icon"></i>';
                    $aOut[] = '         </a>';
*/
                    $aOut[] = '         <a href="/product/edit?image_id='.$oProductImage->getId().'&_do=DeleteImage">';
                    $aOut[] = '            <i class="fa fa-trash icon"></i>';
                    $aOut[] = '         </a>';

                    $aOut[] = '        <img class="image_item" src="/img/product/300x300/'.$oProductImage->getId().'.'.$oProductImage->getFileExt().'">';
                    $aOut[] = '    </li>';

                }
                else
                {
                    throw new LogicException('\$oProductImage should be an instance of ProductImage.');
                }
            }
            $aOut[] = '    <div class="cl"></div>';
        }
        else
        {
            //$aOut[] = '    <li class="fileupload-preview thumbnail mb20" style="padding:50px;">';
            $aOut[] = '        Er zijn nog geen afbeelding toegevoegd.';
            //$aOut[] = '    </li>';
        }

        $aOut[] = ' </ul>';
        $aOut[] = '    <div class="row">';
        $aOut[] = '         <button type="submit" class="btn ladda-button btn-dark pull-right" data-style="expand-right">';
        $aOut[] = '             <span class="ladda-label"><i class="fa fa-upload"></i> Uploaden</span>';
        $aOut[] = '         </button>';
        $aOut[] = '         <div class="pull-right" style="margin-right:11px;padding-right:0;">';
        $aOut[] = '            <span class="btn btn-primary btn-file btn-block">';
        $aOut[] = '                <span class="fileupload-new"><i class="fa fa-picture-o"></i> Bestand kiezen</span>';
        // $aOut[] = '                <span class="fileupload-exists">Change</span>';
        $aOut[] = '                <input type="file" name="file['.$this->getFieldName().']">';
        $aOut[] = '            </span>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';


        return join(PHP_EOL, $aOut);
    }
}
