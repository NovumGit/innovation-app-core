<?php
namespace Crud\Product\Field;

use Crud\IDisplayableField;
use Model\Category\Category;
use InvalidArgumentException;
use Crud\Field;
use Model\Category\CategoryQuery;
use Crud\IFilterableLookupField;
use Model\Product;
use Core\Utils;

class CategoryId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'category_id';
    protected $sFieldLabel = 'Categorie';
    private $sIcon = 'info-circle';
    private $sPlaceHolder = 'Kies een categorie';

    function getGetter()
    {
        return 'getCategoryId';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getLookups($mSelectedItem = null)
    {
        $oCategoryQuery = new CategoryQuery();
        $aAllCategories = $oCategoryQuery->filterByParentId(null)->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllCategories, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        if($iItemId)
        {
            return CategoryQuery::create()->findOneById($iItemId)->getName();
        }
        return null;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;


        if(empty($aPostedData['category_id']))
        {
            $mResponse[] = 'U moet verplicht een categorie uitkiezen.';
        }
        else if(!CategoryQuery::create()->findOneById($aPostedData['category_id']) instanceof Category)
        {
            $mResponse[] = 'De opgegeven categorie is ongeldig.';
        }

        return $mResponse;
    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax\
     */
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return '<td class="">'.$oProduct->getCategory()->getName().'</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $iCategoryId = null;
        if(isset($_GET['category_id']))
        {
            $iCategoryId = $_GET['category_id'];
        }
        else if($mData->getCategoryId())
        {
            $iCategoryId = $mData->getCategoryId();
        }

        $aDropdownOptions = $this->getLookups($iCategoryId);
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCategoryId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }

}
