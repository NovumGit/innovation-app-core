<?php
namespace Crud\Product\Field;

use Core\Cfg;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;


class Thickness extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'thickness';
    protected $sFieldLabel = 'Dikte';
    private $sIcon = 'expand';
    private $sPlaceHolder = 'Dikte';
    private $sGetter = 'getThickness';
    function getVisibleValue($iItemId)
    {
        $aThicknesses = Cfg::get('PRODUCT_THICKNESSES');
        if($aThicknesses == null)
        {
            return null;
        }
        foreach($aThicknesses as $aThickness)
        {
            if($aThickness[0] == $iItemId)
            {
                return $aThickness[1];
            }
        }
        return null;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'number';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {
        $aThicknesses = Cfg::get('PRODUCT_THICKNESSES');
        if($aThicknesses == null)
        {
            return null;
        }
        $aOption = [];
        foreach($aThicknesses as $aThickness)
        {
            $aOption[] = [
                'id' => $aThickness[0],
                'label' => $aThickness[1],
                'selected' => ($aThickness[0] == $mSelectedItem) ? 'selected' : '',
            ];
        }
        return $aOption;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getThickness().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aThicknesses = Cfg::get('PRODUCT_THICKNESSES');

        if($aThicknesses === null)
        {
            return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getThickness(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
        }

        $aLookups = $this->getLookups($mData->getThickness());
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getThickness(), $aLookups, $bReadonly, $this->sIcon);
    }
}
