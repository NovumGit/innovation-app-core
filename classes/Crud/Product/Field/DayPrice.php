<?php
namespace Crud\Product\Field;

use Crud\Field;
use Core\Utils;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class DayPrice extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'day_price';
    protected $sFieldLabel = 'Dagprijs';
    private $sPlaceholder = '00,00';
    private $sIcon = 'money';

    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return 'getDayPrice';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        if(isset($aPostedData['day_price']))
        {
            $fSalePrice = str_replace(',', '.', $aPostedData['day_price']);
            if(!is_numeric($fSalePrice))
            {
                $mResponse[] = 'Het veld dagprijs moet een nummerieke waarde bevatten.';
            }
            $mResponse = false;
            return $mResponse;
        }
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }
        return '<td class="">&euro; '.Utils::priceDisplay($oProduct->getDayPrice()).'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }

        $sSalePrice = str_replace('.', ',', $mData->getDayPrice());
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sSalePrice, $this->sPlaceholder, $this->sIcon, $bReadonly);
    }
}
