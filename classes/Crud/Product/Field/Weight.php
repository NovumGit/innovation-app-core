<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class Weight extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'weight';
    protected $sFieldLabel = 'Gewicht';
    private $sIcon = 'expand';
    private $sPlaceHolder = '';
    private $sGetter = 'getWeight';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return $this->sGetter;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getWidth().'</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getWeight(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
