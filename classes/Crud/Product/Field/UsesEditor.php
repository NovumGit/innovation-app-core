<?php
namespace Crud\Product\Field;

use Core\Translate;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Product;
use Model\ProductUsage;
use Model\Setting\MasterTable\ColorQuery;
use Model\Setting\MasterTable\UsageQuery;

class UsesEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'uses_editor';
    protected $sFieldLabel = 'Product gebruiken editor';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
    function getEditHtml($mData, $bReadonly)
    {
        $oProduct = $mData;
        unset($mData);

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        if(!$oProduct->getId())
        {
            return Translate::fromCode('U kunt pas kleuren toevoegen als het product is opgeslagen.');
        }

        $aProductUsages = $oProduct->getProductUsages();

        $aOut = [];
        $aOut[] = '<ul class="tag_collection" style="width:100%!important;">';

        if(!$aProductUsages->isEmpty())
        {
            foreach($aProductUsages as $oProductUsage)
            {
                if($oProductUsage instanceof ProductUsage)
                {


                    $oUsage = $oProductUsage ->getUsage();
                    $aOut[] = '    <li class="sortable_item">';
                    $aOut[] = '         <a title="'.Translate::fromCode('Klik om te verwijderen').'" class="btn btn-info" href="/product/edit?id='.$oProduct->getId().'&usage_id='.$oUsage->getId().'&_do=DeleteUsage">';
                    $aOut[] = '            <i class="fa fa-trash" style="position:color:red; right:6px;top:200px;"></i>';
                    $aOut[] = '       '.$oUsage->getName();
                    $aOut[] = '         </a>';

                    $aOut[] = '    </li>';
                }
                else
                {
                    throw new LogicException('\$oProductImage should be an instance of ProductImage.');
                }
            }
        }
        $aOut[] = '    <li>';
        $aOut[] = '    <select name="new_usage">' ;
        $aOut[] = '    <option value="">'.Translate::fromCode('Kies een gebruik').'</option>';

        $aUsages = UsageQuery::create()->orderByName()->find();

        foreach($aUsages as $oUsage)
        {
            $aOut[] = '    <option value="'.$oUsage->getId().'">'.$oUsage->getName().'</option>';
        }

        $aOut[] = '    </select>';
        $aOut[] = '    <button class="btn btn-primary ml10" id="add_usage">';
        $aOut[] = '    <i class="fa fa-plus"></i>';
        $aOut[] = '    Gebruik toevoegen';
        $aOut[] = '    </button>';

        $aOut[] = '    </li>';
        $aOut[] = '    <div style="clear:both;margin-bottom:10px"></div>';
        return join(PHP_EOL, $aOut);
    }
}
