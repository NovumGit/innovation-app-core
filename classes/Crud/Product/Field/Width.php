<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class Width extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'width';
    protected $sFieldLabel = 'Breedte';
    private $sIcon = 'expand';
    private $sPlaceHolder = '';
    private $sGetter = 'getWidth';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getWidth().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getWidth(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
