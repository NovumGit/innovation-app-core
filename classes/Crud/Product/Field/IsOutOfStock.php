<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Translate;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;

class IsOutOfStock extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'is_out_of_stock';
    protected $sFieldLabel = 'Is uitverkocht';
    private $sGetter = 'getIsOutOfStock';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sVisibleValue = $oModelObject->{$this->sGetter}() ? '<span style="color:red">'.Translate::fromCode('Ja').'</span>' : Translate::fromCode('Nee');
        return '<td class="">'.$sVisibleValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
