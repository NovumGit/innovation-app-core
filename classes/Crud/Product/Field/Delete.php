<?php
namespace Crud\Product\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Product;
use InvalidArgumentException;

class Delete extends GenericDelete{

    protected $sFieldLabel = 'Verwijderen knop (permanent)';

    function getDeleteUrl($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oProduct));
        }
        return '/product/edit?_do=Delete&id='.$oProduct->getId();
    }

    function getUnDeleteUrl($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oProduct));
        }
        return '/product/edit?_do=Undelete&id='.$oProduct->getId();
    }
}