<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;


class PurchasePrice extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'purchase_price';
    protected $sFieldLabel = 'Inkoop prijs';
    private $sIcon = 'money';
    private $sPlaceHolder = '';
    private $sGetter = 'getPurchasePrice';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sPurchasePrice = Utils::priceDisplay($oModelObject->getPurchasePrice());
        return '<td class="numeric">&euro; '.$sPurchasePrice.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sPurchasePrice = str_replace('.', ',', $oModelObject->getPurchasePrice());
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sPurchasePrice, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
