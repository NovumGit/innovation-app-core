<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\LogicException;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Product;

class IdShowOnly extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'id';
    protected $sFieldLabel = 'Id';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getGetter()
    {
        return 'getId';
    }
    function getDataType():string
    {
        return 'number';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getId().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException('id is not an editable field');
    }
}
