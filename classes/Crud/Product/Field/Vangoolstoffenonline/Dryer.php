<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class Dryer extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'dryer';
    protected $sFieldLabel = 'Droger';
}
