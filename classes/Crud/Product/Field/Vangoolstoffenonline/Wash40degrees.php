<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class Wash40degrees extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'wash_40_degrees';
    protected $sFieldLabel = '40 graden wassen';
}
