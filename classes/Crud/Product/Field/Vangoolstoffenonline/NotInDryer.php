<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class NotInDryer extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'not_in_dryer';
    protected $sFieldLabel = 'Niet in droger';
}
