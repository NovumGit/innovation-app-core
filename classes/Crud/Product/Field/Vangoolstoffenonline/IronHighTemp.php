<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class IronHighTemp extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'iron_high_temp';
    protected $sFieldLabel = 'Hoge temperatuur strijken';
}
