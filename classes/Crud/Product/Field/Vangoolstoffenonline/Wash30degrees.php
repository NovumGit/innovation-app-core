<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class Wash30degrees extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'wash_30_degrees';
    protected $sFieldLabel = '30 graden wassen';
}
