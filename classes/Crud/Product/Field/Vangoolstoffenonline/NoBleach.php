<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class NoBleach extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'no_bleach';
    protected $sFieldLabel = 'Geen bleek';
}
