<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class IronLowTemp extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'iron_low_temp';
    protected $sFieldLabel = 'Lage temperatuur strijken';
}
