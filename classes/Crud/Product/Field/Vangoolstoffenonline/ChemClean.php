<?php
namespace Crud\Product\Field\Vangoolstoffenonline;

use Crud\ICustomCrudField;
use Crud\Product\ProductPropertyField;
use Model\Product as ModelObject;

class ChemClean extends ProductPropertyField implements ICustomCrudField
{
    protected $sFieldName = 'chem_clean';
    protected $sFieldLabel = 'Chemisch reinigen';
}
