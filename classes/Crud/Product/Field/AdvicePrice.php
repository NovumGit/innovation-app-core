<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;


class AdvicePrice extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'advice_price';
    protected $sFieldLabel = 'Adviesprijs';
    private $sIcon = 'money';
    private $sPlaceHolder = '';
    private $sGetter = 'getAdvicePrice';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'number';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sAdvicePrice = number_format($oModelObject->{$this->sGetter}(), 2, ",", '');

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sAdvicePrice, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
