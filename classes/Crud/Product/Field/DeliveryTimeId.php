<?php
namespace Crud\Product\Field;

use Crud\Product\Field\Base\DeliveryTimeId as BaseDeliveryTimeId;

/**
 * Skeleton subclass for representing delivery_time_id field from the product table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class DeliveryTimeId extends BaseDeliveryTimeId
{
}
