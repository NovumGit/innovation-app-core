<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Product;

class Height extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'height';
    protected $sFieldLabel = 'Hoogte';
    private $sIcon = 'expand';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getGetter()
    {
        return 'getHeight';
    }
    function getDataType():string
    {
        return 'number';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getHeight().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getHeight(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
