<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;


class ResellerPrice extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'reseller_price';
    protected $sFieldLabel = 'Groothandelsprijs';
    private $sIcon = 'money';
    private $sPlaceHolder = '';
    private $sGetter = 'getResellerPrice';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sResellerPrice = str_replace('.', ',', $oModelObject->getResellerPrice());
        return '<td class="">'.$sResellerPrice.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sResellerPrice = str_replace('.', ',', $oModelObject->getResellerPrice());
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sResellerPrice, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
