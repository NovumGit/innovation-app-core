<?php
namespace Crud\Product\Field;

use Core\Translate;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Model\Setting\MasterTable\Brand;
use Model\Setting\MasterTable\BrandQuery;
use InvalidArgumentException;
use Crud\Field;
use Model\Product;
use Core\Utils;

class BrandId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'brand_id';
    protected $sFieldLabel = 'Merk';
    private $sIcon = 'tag';
    private $sPlaceHolder = 'Kies een merk';

    function getGetter()
    {
        return 'getBrandId';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['brand_id']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("Merk is een verplicht veld!");
        }

        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {

        $oBrandQuery = BrandQuery::create();
        $aAllBrands = $oBrandQuery->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllBrands, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oBrand  = BrandQuery::create()->findOneById($iItemId);

        if($oBrand instanceof Brand)
        {
            return $oBrand->getName();
        }
        return null;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $iBrandId = $oProduct->getBrandId();
        $oBrand = new Brand();

        if($iBrandId)
        {
            $oBrand = BrandQuery::create()->findOneById($iBrandId);
        }
        $sTxt = '';
        if($oBrand instanceof Brand)
        {
            $sTxt = $oBrand->getName();
        }

        return '<td class="">'.$sTxt.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData->getBrandId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getBrandId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
