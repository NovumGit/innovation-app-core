<?php
namespace Crud\Product\Field;

use Core\DeferredAction;
use Crud\Generic\Field\GenericDelete;
use Model\Product;
use InvalidArgumentException;

class DeleteMarkAs extends GenericDelete{

    protected $sFieldLabel = 'Verwijderen knop (markeren als)';

    function getDeleteUrl($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oProduct));
        }

        $sUrlAdd = '';
        if(isset($_GET['category_id']))
        {
            $sUrlAdd = '?category_id='.$_GET['category_id'];
        }

        DeferredAction::register('after_delete', '/product/overview'.$sUrlAdd);
        return '/product/edit?_do=MarkDeleted&id='.$oProduct->getId();
    }

    function getUnDeleteUrl($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oProduct));
        }
        return '/product/edit?_do=Undelete&id='.$oProduct->getId();
    }
}


