<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use Helper\ProductMultiCategoryHelper;
use InvalidArgumentException;
use Model\Product;
#use Model\Product\Image\ProductImageQuery;
#use Model\Product\Image\ProductImage;

class CategoryTreeEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'new_product_image';
    protected $sFieldLabel = 'Categorie boom editor';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $aOut = [];

        if($mData->getId())
        {
            $aOut[] = "        <div id='jstree'>";
            $aOut[] = '            <ul>';
            $aOut[] =                   ProductMultiCategoryHelper::getTree(null, $mData->getId());
            $aOut[] = '            </ul>';
            $aOut[] = '        </div>';
        }
        else
        {
            $aOut[] = '<div>';
            $aOut[] = '<span>U moet het product eerst opslaan voordat u de categorieën in kan stellen</span>';
            $aOut[] = '</div>';
        }
        return join(PHP_EOL, $aOut);
    }
}
