<?php
namespace Crud\Product\Field;

use Core\Setting;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Model\Setting\MasterTable\Vat;
use Respect\Validation\Validator;
use InvalidArgumentException;
use Crud\Field;
use Model\Setting\MasterTable\VatQuery;
use Model\Product;
use Core\Utils;

class VatId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'vat_id';
    protected $sFieldLabel = 'BTW tarief';
    private $sIcon = 'money';
    private $sPlaceHolder = 'Kies een btw tarief';
    private $sGetter = 'getVatId';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(isset($aPostedData['vat_id']))
        {
            $oValidator = Validator::numeric();

            if(empty($aPostedData['vat_id']))
            {
                $mResponse[] = 'U moet verplicht een btw tarief uitkiezen.';
            }
            else if(!$oValidator->intVal()->validate($aPostedData['vat_id']))
            {
                $mResponse[] = 'Het opgegeven btw tarief is ongeldig.';
            }
        }
        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {

        if($mSelectedItem == null)
        {
            $mSelectedItem = Setting::get('product_default_vat_level', null);
        }


        $oVatQuery = new VatQuery();
        $aAllTypes = $oVatQuery->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllTypes, 'getName', $mSelectedItem);

        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        return VatQuery::create()->findOneById($iItemId)->getName();
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $oVat = $oProduct->getVat();

        $sTxt = '';
        if($oVat instanceof Vat)
        {
            $sTxt = $oVat->getName().' ('.$oVat->getPercentage().'%)' ;
        }

        return '<td class="">'.$sTxt.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData->getVatId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getVatId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
