<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;

class Add extends Field implements IEventField{

    protected $sFieldLabel = 'Toevoegen aan order';

    function getIcon()
    {
        return 'magic';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof Product)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $oProduct = $mData;

        if(!isset($_GET['order_id']))
        {
            return '';
        }
        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <form method="post" action="/order/add_product?order_id='.$_GET['order_id'].'&_do=AddProduct&product_id='.$oProduct->getId().'">';
        $aOut[] = '    <button type="submit" title="Product toevoegen aan de huidige order" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </button>';
        $aOut[] = '    <input style="height:20px;width:30px;margin-top:2px;display:block;float:right;" type="text" name="quantity" size="4" value="1"/>';
        $aOut[] = ' </form>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
