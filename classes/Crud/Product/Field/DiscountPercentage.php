<?php
namespace Crud\Product\Field;

use Crud\Field;
use Core\Utils;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class DiscountPercentage extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'discount_percentage';
    protected $sFieldLabel = 'Kortingspercentage';
    private $sPlaceholder = '00,00';
    private $sIcon = 'money';

    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return 'getDiscountPercentage';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return false; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(isset($aPostedData['sale_price']))
        {
            $mResponse = [];
            $fSalePrice = str_replace(',', '.', $aPostedData['sale_price']);
            if(!is_numeric($fSalePrice))
            {
                $mResponse[] = 'Het veld dagprijs moet een nummerieke waarde bevatten.';
            }
        }
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }
        return '<td class="">'.Utils::priceDisplay($oProduct->getDiscountPercentage()).'%</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }
        $sDiscountPercentage = number_format($mData->getDiscountPercentage(), 2, ",", '');

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sDiscountPercentage, $this->sPlaceholder, $this->sIcon, $bReadonly);
    }
}
