<?php
namespace Crud\Product\Field;

use Core\Cfg;
use Core\Utils;
use Crud\Field;
use Crud\ICustomCrudField;
use Crud\IDisplayableField;
use Crud\IEventField;
use Exception\InvalidArgumentException;
use Model\DtcLease\CarFuelType;
use Model\DtcLease\CarFuelTypeQuery;
use Model\Product;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class GoToPage extends Field implements ICustomCrudField, IEventField, IDisplayableField
{
    protected $sFieldName = 'go_to_page';
    protected $sFieldLabel = "Openen in website";

    function store(ActiveRecordInterface $oObject, array $aData)
    {
        return null;
    }

    function getGetter()
    {
        return 'getProductUrl';
    }

    function getLookups($mSelectedItem = null)
    {
        $oCarFuelTypeQuery = CarFuelTypeQuery::create();
        $oCarFuelTypeQuery->orderByName();
        $aCarFuelTypes =  $oCarFuelTypeQuery->find();

        return Utils::makeSelectOptions($aCarFuelTypes, 'getName', $mSelectedItem);
    }
    function hasValidations()
    {
        return false;
    }
    function validate($aPostedData)
    {
        return null;
    }

    function getVisibleValue($iItemId)
    {
        $oCarFuelTypeQuery = CarFuelTypeQuery::create();
        $oCarFuelType = $oCarFuelTypeQuery->findOneById($iItemId);
        if($oCarFuelType instanceof CarFuelType)
        {
            return $oCarFuelType->getName();
        }
        return null;
    }

    function getIcon()
    {
        return 'globe';
    }
    function getDataType():string
    {
        return 'link';
    }

    function getFieldLabel()
    {
        return 'Url / link naar pagina';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of Product in ".__METHOD__);
        }

        $sUrl = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN').$mData->getProductUrl();



        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = "    <a target='_blank' href=\"$sUrl\" class=\"btn btn-success br2 btn-xs fs12 d\">";
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sUrl = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN').$oModelObject->getProductUrl();
        return $this->editLinkField($this->getTranslatedTitle(), $this->getFieldName(), $sUrl, $this->getTranslatedTitle());
    }
}
