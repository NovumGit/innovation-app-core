<?php
namespace Crud\Product\Field;

use Core\Translate;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Product;
use Model\ProductColor;
use Model\Setting\MasterTable\ColorQuery;

class ColorEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'color_editor';
    protected $sFieldLabel = 'Product kleuren editor';


    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getEditHtml($mData, $bReadonly)
    {
        $oProduct = $mData;
        unset($mData);

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        if(!$oProduct->getId())
        {
            return Translate::fromCode('U kunt pas kleuren toevoegen als het product is opgeslagen.');
        }

        $aProductColors = $oProduct->getProductColors();

        $aOut = [];
        $aOut[] = '<ul class="tag_collection" style="width:100%!important;">';

        if(!$aProductColors->isEmpty())
        {
            foreach($aProductColors as $oProductColor)
            {
                if($oProductColor instanceof ProductColor)
                {
                    $oColor = $oProductColor ->getColor();
                    $aOut[] = '    <li class="sortable_item" style="                                                
                                                
                                                ">';
                    $aOut[] = '         <a title="'.Translate::fromCode('Klik om te verwijderen').'" class="btn btn-info" href="/product/edit?id='.$oProduct->getId().'&color_id='.$oColor->getId().'&_do=DeleteColor">';
                    $aOut[] = '            <i class="fa fa-trash" style="position:color:red; right:6px;top:200px;"></i>';
                    $aOut[] = '       '.$oColor->getName();
                    $aOut[] = '         </a>';

                    $aOut[] = '    </li>';
                }
                else
                {
                    throw new LogicException('\$oProductImage should be an instance of ProductImage.');
                }
            }
        }
        $aOut[] = '    <li>';
        $aOut[] = '    <select name="new_color">' ;
        $aOut[] = '    <option value="">'.Translate::fromCode('Kies een kleur').'</option>';
        $aColors = ColorQuery::create()->orderByName()->find();

        foreach($aColors as $oColor)
        {
            $aOut[] = '    <option value="'.$oColor->getId().'">'.$oColor->getName().'</option>';
        }

        $aOut[] = '    </select>';
        $aOut[] = '    <button class="btn btn-primary ml10" id="add_color">';
        $aOut[] = '    <i class="fa fa-plus"></i>';
        $aOut[] = '    Kleur toevoegen';
        $aOut[] = '    </button>';
        $aOut[] = '    </li>';
        $aOut[] = '    <div style="clear:both;margin-bottom:10px"></div>';

        return join(PHP_EOL, $aOut);
    }
}
