<?php
namespace Crud\Product\Field;

use Crud\IDisplayableField;
use Model\Base\ProductQuery;
use InvalidArgumentException;
use Crud\Field;
use Crud\IFilterableLookupField;
use Model\Product;
use Core\Utils;

class DerrivedFrom extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'derrived_from_id';
    protected $sFieldLabel = 'Afgeleid van';
    private $sIcon = 'cubes';
    private $sPlaceHolder = 'Kies een product';


    function getGetter()
    {
        return 'getDerrivedFromId';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getLookups($mSelectedItem = null)
    {
        // A product should only be derrived from another product in the same category.
        // So the expected fields match up.
        $oProductQuery = ProductQuery::create();
        if(isset($_GET['category_id']))
        {
            $iCategoryId = $_GET['category_id'];
            $oProductQuery->filterByCategoryId($iCategoryId);
        }
        else if(isset($_GET['product_id']))
        {
            $oProduct = ProductQuery::create()->findOneById($_GET['product_id']);
            $iCategoryId = $oProduct->getCategoryId();
            $oProductQuery->filterByCategoryId($iCategoryId);
        }
        $oProductQuery->where('derrived_from_id IS NULL');
        $aProducts = $oProductQuery->orderByNumber();
        $aDropdownOptions = Utils::makeSelectOptions($aProducts, 'getNumber', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        return ProductQuery::create()->findOneById($iItemId)->getNumber();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        return false;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $iDerrivedFrom = $oProduct->getDerrivedFromId();
        $sValue = '';
        if($iDerrivedFrom)
        {
            $oProduct = ProductQuery::create()->findOneById($iDerrivedFrom);
            if($oProduct instanceof Product)
            {
                $sValue = $oProduct->getNumber();
            }
            $oProduct->getNumber();
        }
        return '<td class="">'.$sValue.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getDerrivedFromId());
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getDerrivedFromId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }

}
