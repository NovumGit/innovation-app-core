<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;

class AddPromo extends Field implements IEventField{

    protected $sFieldLabel = 'Toevoegen aan order';

    function getIcon()
    {
        return 'magic';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }
    function getOverviewValue($mData)
    {
        if(!$mData instanceof Product)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $oProduct = $mData;

        $aOut = [];
        $aOut[] = '<td class="xx">';
        if(isset($_GET['site']))
        {
            $aOut[] = '    <a href="/cms/promoproducts/edit?site='.$_GET['site'].'&product_id='.$oProduct->getId().'" class="btn btn-success br2 btn-xs fs12 d">';
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
        }
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }
    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
