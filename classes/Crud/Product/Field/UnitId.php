<?php
namespace Crud\Product\Field;

use Crud\IDisplayableField;
use Model\Setting\MasterTable\ProductUnit;
use Model\Setting\MasterTable\ProductUnitQuery;
use Model\Product;
use Respect\Validation\Validator;
use InvalidArgumentException;
use Crud\Field;
use Crud\IFilterableLookupField;
use Core\Utils;

class UnitId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'unit_id';
    protected $sFieldLabel = 'Eenheid';
    private $sIcon = 'info-circle';
    private $sPlaceHolder = 'Kies een eenheid';
    private $sGetter = 'getUnitId';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getLookups($mSelectedItem = null)
    {
        $oProductUnitQuery = new ProductUnitQuery();
        $aAllUnits = $oProductUnitQuery->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllUnits, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        return ProductUnitQuery::create()->findOneById($iItemId)->getName();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {

        $mResponse = false;
        $oValidator = Validator::numeric();

        if(empty($aPostedData['unit_id']))
        {
            $mResponse[] = 'U moet verplicht een eenheid uitkiezen.';
        }
        else if(!$oValidator->intVal()->validate($aPostedData['unit_id']))
        {
            $mResponse[] = 'Het opgegeven eenheid is ongeldig.';
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        $oProductUnit = $oProduct->getProductUnit();

        $sProductUnit = '';
        if($oProductUnit instanceof ProductUnit)
        {
            $sProductUnit = $oProductUnit->getName();
        }
        return '<td class="">'.$sProductUnit.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getUnitId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getUnitId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }

}
