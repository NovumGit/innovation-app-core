<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Translate;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Product as ModelObject;


class StockQuantity extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'stock_quantity';
    protected $sFieldLabel = 'Voorraad aantal';
    private $sIcon = 'gift';
    private $sPlaceHolder = '';
    private $sGetter = 'getStockQuantity';


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getGetter()
    {
        return $this->sGetter;
    }

    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        if($bReadonly && $oModelObject->getBulkStockId())
        {
            return $this->editLinkField(
                $this->getTranslatedTitle(),
                'Voorraad: '.$oModelObject->getStockQuantity().' '.
                '<i class="fa fa-plus"></i> Toevoegen',
                '/stock/bulk/wrap_items?bulk_stock_id='.$oModelObject->getBulkStockId(),
                '');
        }
        else
        {
            if($oModelObject->getVirtualStock())
            {
                return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, null, Translate::fromCode('altijd voorradig'), $this->sIcon, true);
            }
            return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
        }
    }
}
