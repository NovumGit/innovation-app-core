<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use Exception\InvalidArgumentException;
use Model\Supplier\Supplier as SupplierModel;
use Model\Product as ModelObject;
use Model\Supplier\SupplierQuery;

class Supplier extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'supplier';
    protected $sFieldLabel = 'Leverancier';
    private $sGetter = 'getSupplier';
    private $sPlaceholder = 'Leverancier';
    private $sIcon = 'car';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $iSupplier = $oModelObject->getSupplier();

        $oSupplier = new SupplierModel();
        if($iSupplier)
        {
            $oSupplier = SupplierQuery::create()->findOneById($iSupplier);
        }
        return '<td class="">' . $oSupplier->getName() . '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}();
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sValue,  $this->sPlaceholder, $this->sIcon, $bReadonly);
    }
}
