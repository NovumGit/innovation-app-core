<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;

class Copy extends Field implements IEventField{

    protected $sFieldLabel = 'Product kopieren';

    function getIcon()
    {
        return 'copy';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {
        if(!$mData instanceof Product)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $oProduct = $mData;

        $aOut = [];
        $aOut[] = '<td>';
        $aOut[] = '    <a href="/product/copy?product_id='.$oProduct->getId().'" title="Product kopiëren" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </a>';
        /*
        $aOut[] = ' <form method="post" action="/order/add_product?order_id='.$_GET['order_id'].'&_do=AddProduct&product_id='.$oProduct->getId().'">';

        $aOut[] = '    <input style="height:20px;width:30px;margin-top:2px;display:block;float:right;" type="text" name="quantity" size="4" value="1"/>';
        $aOut[] = ' </form>';
        */
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
