<?php
namespace Crud\Product\Field;

use Crud\Field;
use Core\Utils;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class SalePrice extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'sale_price';
    protected $sFieldLabel = 'Prijs';
    private $sPlaceholder = '00,00';
    private $sIcon = 'money';

    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return 'getOriginalSalePrice';
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(isset($aPostedData['sale_price']))
        {
            $mResponse = [];
            $fSalePrice = str_replace(',', '.', $aPostedData['sale_price']);
            if(!is_numeric($fSalePrice))
            {
                $mResponse[] = 'Het veld dagprijs moet een nummerieke waarde bevatten.';
            }
        }
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }
        return '<td class="">&euro; '.Utils::priceDisplay($oProduct->getSalePrice()).'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product in ".__METHOD__);
        }
        $sSalePrice = number_format($mData->getOriginalSalePrice(), 2, ",", '');

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sSalePrice, $this->sPlaceholder, $this->sIcon, $bReadonly);
    }
}
