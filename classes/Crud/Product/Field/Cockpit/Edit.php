<?php
namespace Crud\Product\Field\Cockpit;

use Crud\Generic\Field\GenericEdit;
use Model\Product;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit{

    protected $sFieldLabel = 'Bewerken knop';

    function getEditUrl($oProduct){

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product but got '.get_class($oProduct));
        }

        return '/custom/cockpit/foodbox/product/edit?id=' . $oProduct->getId();
    }
}
