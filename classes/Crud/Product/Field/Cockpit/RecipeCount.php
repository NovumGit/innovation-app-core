<?php
namespace Crud\Product\Field\Cockpit;

use Core\Translate;
use Crud\Field;
use Crud\IEventField;
use Model\Cockpit\Foodbox\RecipeProductQuery;
use Model\Product;

class RecipeCount extends Field implements IEventField
{
    protected $sFieldName = 'recipe_count';
    protected $sFieldLabel = 'Aantal recepten';

    function getFieldTitle()
    {
	    return $this->sFieldLabel;
    }
    function getIcon()
    {
	    return 'cutlery';
    }
    function getEditHtml($mData, $bReadonly)
    {
	    return __METHOD__ . ' is not intended to be used in editor';
    }
    function getOverviewHeader()
    {
	    return $this->nonSortableHeaderField('#RCPT', $this->sFieldName, $_GET);
    }
    function getOverviewValue($mData)
    {
	    $iCount = 0;
	    $sAllPricedColor = 'success';
		if($mData instanceof Product)
		{

		    $aRecipeProducts = $mData->getRecipeProducts();
		    if(!$aRecipeProducts->isEmpty())
            {
                foreach($aRecipeProducts as $oRecipeProduct)
                {
                    if(!$oRecipeProduct->getQuantity() || !$oRecipeProduct->getUnitId())
                    {
                        $sAllPricedColor = 'danger';
                    }
                    $iCount++;
                }
            }


		}

		$sTitle = Translate::fromCode("Bekijk alle recepten met dit ingredient");
		$aOut = [];
	    $aOut[] = '<td class="">';
	    $aOut[] = ' <a class="btn btn-xs btn-' . $sAllPricedColor . '" href="/custom/cockpit/foodbox/recipe/overview?filter[ingredient]=' . $mData->getId() . '" title="' . htmlentities($sTitle) . '">';
	    $aOut[] = '     '.$iCount . ' recepten';
	    $aOut[] = ' </a>';
	    $aOut[] = '</td>';

	    return join($aOut, PHP_EOL);
    }
    function hasValidations()
    {
	    return false;
    }
    function validate($aPostedData)
    {
	    return null;
    }

}
