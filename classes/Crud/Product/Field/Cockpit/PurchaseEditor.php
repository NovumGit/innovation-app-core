<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use Helper\ProductMultiCategoryHelper;
use InvalidArgumentException;
use Model\Product;

class PurchaseEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'purchhase';
    protected $sFieldLabel = 'Inkoop tarieven';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }
	function getEditHtml($mData, $bReadonly)
	{
		/*
		$oRecipe = $mData;
		unset($mData);

		if(!$oRecipe instanceof Recipe)
		{
			throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
		}
		if(!$oRecipe->getId())
		{
			return Translate::fromCode('U kunt pas ingredienten toevoegen als het recept is opgeslagen.');
		}


		$sSort = $_GET['sort'] ?? 'asc';
		$sDirection = $sSort == 'asc' ? Criteria::ASC : Criteria::DESC;
		$sSortNext = $sSort == 'asc' ? 'desc' : 'asc';


		$oRecipeProductQuery = RecipeProductQuery::create();
		$oRecipeProductQuery->filterByRecipeId($oRecipe->getId());

		$sSort = $_GET['ingredient_sort'] ?? 'ingredient';


		if($sSort == 'ingredient')
		{
			$oRecipeProductQuery->useProductQuery()->orderByTitle($sDirection)->endUse();
		}
		else if($sSort == 'department')
		{
			$oRecipeProductQuery->useProductQuery()->useCategoryQuery()->orderByName($sDirection)->endUse()->endUse();
		}
		else if($sSort == 'quantity')
		{
			$oRecipeProductQuery->orderByQuantity($sDirection);
		}
		else if($sSort == 'unit')
		{
			$oRecipeProductQuery->useProductUnitQuery()->orderByCode($sDirection)->endUse();
		}

		$aRecipeProducts = $oRecipeProductQuery->find();

		$aOut = [];
		$aOut[] = '<table class="table">';



		$aOut[] = '<tr class="bg-light">';

		$aOut[] = '	<th>';
		$aOut[] = '	    <a class="preserve_scroll" href="' . Utils::getRequestUri(true, ['ingredient_sort' => 'ingredient', 'sort' => $sSortNext]) . '">';
		$aOut[] = '	    	Naam';
		$aOut[] = '	    	<i class="fa fa-sort"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';


		$aOut[] = '	<th>';
		$aOut[] = '	    <a class="preserve_scroll" href="' . Utils::getRequestUri(true, ['ingredient_sort' => 'department', 'sort' => $sSortNext]) . '">';
		$aOut[] = '	    	Afdeling';
		$aOut[] = '	    	<i class="fa fa-sort"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';


		$aOut[] = '	<th>';
		$aOut[] = '	    <a class="preserve_scroll" href="' . Utils::getRequestUri(true, ['ingredient_sort' => 'quantity', 'sort' => $sSortNext]) . '">';
		$aOut[] = '	    	Aantal';
		$aOut[] = '	    	<i class="fa fa-sort"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';



		$aOut[] = '	<th>';
		$aOut[] = '	    <a class="preserve_scroll" href="' . Utils::getRequestUri(true, ['ingredient_sort' => 'unit', 'sort' => $sSortNext]) . '">';
		$aOut[] = '	    	Eenheid';
		$aOut[] = '	    	<i class="fa fa-sort"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';

		$aOut[] = '	<th class="iconcol">';
		$aOut[] = '	    <a class="btn btn-default btn-xs" href="#">';
		$aOut[] = '	    	<i class="fa fa-edit"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';

		$aOut[] = '	<th class="iconcol">';
		$aOut[] = '	    <a class="btn btn-default btn-xs" href="#">';
		$aOut[] = '	    	<i class="fa fa-gears"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';

		$aOut[] = '	<th class="iconcol">';
		$aOut[] = '	    <a class="btn btn-default btn-xs" href="#">';
		$aOut[] = '	    	<i class="fa fa-trash"></i>';
		$aOut[] = '	    </a>';
		$aOut[] = ' </th>';


		$aOut[] = "</tr>";


		foreach($aRecipeProducts as $oRecipeProduct)
		{

			if(!$oRecipeProduct instanceof RecipeProduct)
			{
				throw new LogicException("Expected an instance of RecipeProduct");
			}
			$oProductUnit = ProductUnitQuery::create()->findOneById($oRecipeProduct->getUnitId());
			$aOut[] = "<tr>";

			$aOut[] = "	<td>" . $oRecipeProduct->getProduct()->getTitle() . "</td>";
			$aOut[] = "	<td>" . $oRecipeProduct->getProduct()->getCategory()->getName() . "</td>";
			$aOut[] = "	<td>" . $oRecipeProduct->getQuantity() . "</td>";

			if($oProductUnit instanceof ProductUnit)
			{
				$aOut[] = "	<td>" . $oProductUnit->getName() . "</td>";
			}
			else
			{
				$aOut[] = "	<td></td>";
			}

			$aOut[] = '	<td>';
			$aOut[] = '	    <a title="' . Translate::fromCode('Ingredient bewerken') . '" class="preserve_scroll btn btn-xs btn-dark" href="/custom/cockpit/foodbox/ingredient/edit?id=' . $oRecipeProduct->getId(). '&r=recipe_main_url">';
			$aOut[] = '	    	<i class="fa fa-edit"></i>';
			$aOut[] = '	    </a>';
			$aOut[] = ' </td>';

			$aOut[] = '	<td>';
			$aOut[] = '	    <a title="' . Translate::fromCode('Ga naar product') . '" class="preserve_scroll btn btn-xs btn-dark" href="/product/edit?id=' . $oRecipeProduct->getProductId(). '&r=recipe_main_url">';
			$aOut[] = '	    	<i class="fa fa-gears"></i>';
			$aOut[] = '	    </a>';
			$aOut[] = ' </td>';


			$aOut[] = '	<td class="iconcol">';
			$aOut[] = '	    <a class="preserve_scroll btn btn-danger btn-xs" href="/custom/cockpit/foodbox/ingredient/edit?id=' . $oRecipeProduct->getId(). '&r=recipe_main_url">';
			$aOut[] = '	    	<i class="fa fa-trash"></i>';
			$aOut[] = '	    </a>';
			$aOut[] = ' </td>';

			$aOut[] = "</tr>";
		}
		$aOut[] = "</table>";
		$aOut[] = '    <div style="clear:both;margin-bottom:10px"></div>';
		return join(PHP_EOL, $aOut);
		*/
	}
}
