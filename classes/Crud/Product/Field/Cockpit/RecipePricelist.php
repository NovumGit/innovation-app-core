<?php
namespace Crud\Product\Field\Cockpit;

use Core\Translate;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;

class RecipePricelist extends Field implements IEventField
{
    protected $sFieldName = 'recipe_pricelist';
    protected $sFieldLabel = 'Recepten met dit ingredient';

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }
    function getIcon()
    {
        return 'list';
    }
    function getEditHtml($mData, $bReadonly)
    {
        return __METHOD__ . ' is not intended to be used in editor';
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '        <i class="fa fa-list"></i>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {
        if(!$mData instanceof Product)
        {
            throw new LogicException("Expected an instance of Product");
        }
        $sTitle = Translate::fromCode("Dit ingredient voor recepten aanpassen.");
        $aOut = [];
        $aOut[] = '<td class="iconcol">';
        $aOut[] = ' <a class="btn btn-xs btn-dark" href="/custom/cockpit/foodbox/ingredient/bulk_edit?product_id=' . $mData->getId() . '&origin=product_overview&scroll=350" title="' . htmlentities($sTitle) . '">';
        $aOut[] = '    <i class="fa fa-list"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join($aOut, PHP_EOL);
    }
    function hasValidations()
    {
        return false;
    }
    function validate($aPostedData)
    {
        return null;
    }

}
