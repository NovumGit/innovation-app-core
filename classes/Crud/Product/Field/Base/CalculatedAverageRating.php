<?php
namespace Crud\Product\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Product\ICollectionField;

/**
 * Base class that represents the 'calculated_average_rating' crud field from the 'product' table.
 * This class is auto generated and should not be modified.
 */
abstract class CalculatedAverageRating extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'calculated_average_rating';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCalculatedAverageRating';
	protected $sFqModelClassname = '\\\Model\Product';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
