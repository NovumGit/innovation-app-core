<?php
namespace Crud\Product\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Product\ICollectionField;

/**
 * Base class that represents the 'advertiser_user_id' crud field from the 'product' table.
 * This class is auto generated and should not be modified.
 */
abstract class AdvertiserUserId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'advertiser_user_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAdvertiserUserId';
	protected $sFqModelClassname = '\\\Model\Product';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
