<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;

use Model\Product as ModelObject;
use Model\Setting\MasterTable\Base\ProductDeliveryTimeQuery;


class DeliveryTime extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'delivery_time_id';
    protected $sFieldLabel = 'Levertijd';
    private $sIcon = 'clock';
    private $sGetter = 'getDeliveryTimeId';

    function getGetter()
    {
        return 'getDayPrice';
    }

    function getLookups($mSelectedItem = null)
    {
        $aDeliveryTimes = ProductDeliveryTimeQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aDeliveryTimes, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return ProductDeliveryTimeQuery::create()->findOneById($iItemId)->getName();
    }


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $this->sIcon, 'Maak een keuze');
    }
}
