<?php
namespace Crud\Product\Field;

use Crud\IDisplayableField;
use Model\BulkStock;
use Model\BulkStockQuery;
use InvalidArgumentException;
use Crud\Field;
use Crud\IFilterableLookupField;
use Model\Product;
use Core\Utils;

class BulkStockId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'bulk_stock_id';
    protected $sFieldLabel = 'Voorraadhouder';
    private $sIcon = 'cubes';
    private $sPlaceHolder = 'Kies een voorraadhouder';

    function getGetter()
    {
        return 'getBulkStockId';
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getLookups($mSelectedItem = null)
    {
        $aAllBulkStockItems =  BulkStockQuery::create()->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllBulkStockItems, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        return BulkStockQuery::create()->findOneById($iItemId)->getName();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        return false;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $oBulkStock = $oProduct->getBulkStock();
        $sValue = '';
        if($oBulkStock instanceof BulkStock)
        {
            $sValue = $oBulkStock->getName();
        }
        return '<td class="">'.$sValue.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getBulkStockId());
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getBulkStockId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }

}
