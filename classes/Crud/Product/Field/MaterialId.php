<?php
namespace Crud\Product\Field;

use Crud\IDisplayableField;
use Respect\Validation\Validator;
use InvalidArgumentException;
use Crud\Field;
use Model\Setting\MasterTable\ProductMaterialQuery;
use Crud\IFilterableLookupField;
use Model\Product;
use Core\Utils;

class MaterialId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'material_id';
    protected $sFieldLabel = 'Materiaal';
    private $sIcon = 'info-circle';
    private $sPlaceHolder = 'Kies een materiaal';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getLookups($mSelectedItem = null)
    {
        $oProductMaterialQuery = new ProductMaterialQuery();
        $aAllTypes = $oProductMaterialQuery->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aAllTypes, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        return ProductMaterialQuery::create()->findOneById($iItemId)->getName();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {

        $mResponse = false;
        $oValidator = Validator::numeric();

        if(empty($aPostedData['material_id']))
        {
            $mResponse[] = 'U moet verplicht een materiaal uitkiezen.';
        }
        else if(!$oValidator->intVal()->validate($aPostedData['material_id']))
        {
            $mResponse[] = 'Het opgegeven materiaal is ongeldig.';
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return '<td class="">'.$oProduct->getProductMaterial()->getName().'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getMaterialId());


        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getMaterialId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }

}
