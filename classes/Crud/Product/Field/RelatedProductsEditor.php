<?php
namespace Crud\Product\Field;

use Core\DeferredAction;
use Core\Translate;
use Core\Utils;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Product;
use Model\Product\Image\ProductImageQuery;
use Model\Product\Image\ProductImage;
use Model\Product\RelatedProductQuery;
use Model\Product\RelatedProduct;
use Model\CustomerQuery;

class RelatedProductsEditor extends Field implements IFilterableField, IEditableField, IInlineEditor {

    protected $sFieldName = 'related_products';
    protected $sFieldLabel = 'Gerelateerde producten editor';


    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        if(!$mData->getId())
        {
            return Translate::fromCode('U kunt pas producten relateren als het product is opgeslagen.');
        }
        $oRelatedProductsQuery = RelatedProductQuery::create();
        $oRelatedProductsQuery->orderBySorting();
        $aRelatedProducts = $oRelatedProductsQuery->findByProductId($mData->getId());

        $aOut = [];
        $aOut[] = '<ul class="sortable_container product_picker" data-method="SortRelatedProducts" data-product_id="' . $mData->getId() . '">';

        if($aRelatedProducts->count())
        {
            foreach($aRelatedProducts as $oRelatedProduct)
            {
                if($oRelatedProduct instanceof RelatedProduct)
                {
                    $oProduct = CustomerQuery::create()->findOneById($oRelatedProduct->getOtherProductId());
                    $oProductImage = $oProduct->getPrimaryImage();

                    $aOut[] = '    <li class="sortable_item product_picker" data-data="{product_id : ' . $oRelatedProduct->getId() . ' }">';
                    $aOut[] = '         <a class="trash_link" href="/product/edit?id='.$mData->getId().'&related_id='.$oRelatedProduct->getOtherProductId().'&_do=DeleteRelation">';
                    $aOut[] = '            <i class="fa fa-trash"></i>';
                    $aOut[] = '         </a>';

                    if($oProductImage instanceof ProductImage)
                    {
                        $aOut[] = '        <img class="image_item" src="/img/product/300x300/'.$oProductImage->getId().'.'.$oProductImage->getFileExt().'">';
                    }
                    else
                    {
                        $aOut[] = '        <img class="image_item" src="/img/file-not-found.jpg" style="height:195px;display: block;">';
                    }

                    $aOut[] = '    </li>';
                }
                else
                {
                    throw new LogicException('\$oProductImage should be an instance of ProductImage.');
                }
            }
            $aOut[] = '    <div class="cl"></div>';
        }
        else
        {
            $aOut[] = Translate::fromCode('Er zijn nog geen producten gerelateerd.');
        }

        DeferredAction::register('after_pick_product', Utils::getRequestUri().'&_do=RelateProduct');
        // This is related to an event in the products picker window
        DeferredAction::register('after_pick_multi_product', Utils::getRequestUri().'&_do=RelateProducts');

        $aOut[] = ' </ul>';
        $aOut[] = '    <div class="row">';
        $aOut[] = '         <a href="/product/pickproduct?r=after_pick_product" class="btn ladda-button btn-primary pull-right" data-style="expand-right">';
        $aOut[] = '             <span class="ladda-label"><i class="fa fa-magic"></i> Product kiezen</span>';
        $aOut[] = '         </a>';
        $aOut[] = '    </div>';
        $aOut[] = ' </ul>';
        return join(PHP_EOL, $aOut);
    }
}
