<?php
namespace Crud\Product\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;

class SupplierProductNumber extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'supplier_product_number';
    protected $sFieldLabel = 'Leverancier artikelnummer';
    private $sIcon = 'expand';
    private $sGetter = 'getSupplierProductNumber';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getDataType():string
    {
        return 'number';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData){return false;}
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }

    function getOverviewValue($oProduct)
    {
        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProduct->getThickness().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Product)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getSupplierProductNumber(), null, $this->sIcon, $bReadonly);
    }
}
