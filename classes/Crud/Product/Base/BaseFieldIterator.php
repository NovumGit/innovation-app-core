<?php
namespace Crud\Product\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Product\ICollectionField as ProductField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Product\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductField
	{
		return current($this->aFields);
	}
}
