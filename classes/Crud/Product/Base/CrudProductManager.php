<?php
namespace Crud\Product\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Product\FieldIterator;
use Crud\Product\Field\AdvertiserUserId;
use Crud\Product\Field\AdvicePrice;
use Crud\Product\Field\AvgRating;
use Crud\Product\Field\BrandId;
use Crud\Product\Field\BulkStockId;
use Crud\Product\Field\CalculatedAverageRating;
use Crud\Product\Field\CategoryId;
use Crud\Product\Field\Composition;
use Crud\Product\Field\CreatedOn;
use Crud\Product\Field\DayPrice;
use Crud\Product\Field\DeletedByUserId;
use Crud\Product\Field\DeletedOn;
use Crud\Product\Field\DeliveryTimeId;
use Crud\Product\Field\DerrivedFromId;
use Crud\Product\Field\Description;
use Crud\Product\Field\DiscountPercentage;
use Crud\Product\Field\Ean;
use Crud\Product\Field\HasImages;
use Crud\Product\Field\Hashtag;
use Crud\Product\Field\Height;
use Crud\Product\Field\ImportTag;
use Crud\Product\Field\InSale;
use Crud\Product\Field\InSpotlight;
use Crud\Product\Field\InWebshop;
use Crud\Product\Field\IsBestseller;
use Crud\Product\Field\IsOffer;
use Crud\Product\Field\IsOutOfStock;
use Crud\Product\Field\Length;
use Crud\Product\Field\MaterialId;
use Crud\Product\Field\MetaDescription;
use Crud\Product\Field\MetaKeyword;
use Crud\Product\Field\MetaTitle;
use Crud\Product\Field\Note;
use Crud\Product\Field\Number;
use Crud\Product\Field\OldId;
use Crud\Product\Field\Popularity;
use Crud\Product\Field\PurchasePrice;
use Crud\Product\Field\QuantityPerPack;
use Crud\Product\Field\ResellerPrice;
use Crud\Product\Field\SalePrice;
use Crud\Product\Field\Sku;
use Crud\Product\Field\StockQuantity;
use Crud\Product\Field\Supplier;
use Crud\Product\Field\SupplierId;
use Crud\Product\Field\SupplierProductNumber;
use Crud\Product\Field\Thickness;
use Crud\Product\Field\Title;
use Crud\Product\Field\UnitId;
use Crud\Product\Field\VatId;
use Crud\Product\Field\VirtualStock;
use Crud\Product\Field\Weight;
use Crud\Product\Field\Width;
use Crud\Product\Field\YoutubeUrl;
use Exception\LogicException;
use Model\Map\ProductTableMap;
use Model\Product;
use Model\ProductQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Product instead if you need to override or add functionality.
 */
abstract class CrudProductManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductQuery::create();
	}


	public function getTableMap(): ProductTableMap
	{
		return new ProductTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Product";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DerrivedFromId', 'OldId', 'ImportTag', 'Sku', 'Ean', 'DeletedOn', 'DeletedByUserId', 'Popularity', 'AvgRating', 'Supplier', 'SupplierId', 'SupplierProductNumber', 'InSale', 'InSpotlight', 'InWebshop', 'IsOffer', 'IsBestseller', 'IsOutOfStock', 'HasImages', 'CategoryId', 'Number', 'AdvertiserUserId', 'DeliveryTimeId', 'Thickness', 'Height', 'Width', 'Length', 'Weight', 'Composition', 'MaterialId', 'BrandId', 'Title', 'Description', 'Note', 'CreatedOn', 'DayPrice', 'SalePrice', 'DiscountPercentage', 'PurchasePrice', 'ResellerPrice', 'AdvicePrice', 'BulkStockId', 'StockQuantity', 'VirtualStock', 'QuantityPerPack', 'YoutubeUrl', 'MetaTitle', 'MetaDescription', 'MetaKeyword', 'Hashtag', 'CalculatedAverageRating', 'VatId', 'UnitId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DerrivedFromId', 'OldId', 'ImportTag', 'Sku', 'Ean', 'DeletedOn', 'DeletedByUserId', 'Popularity', 'AvgRating', 'Supplier', 'SupplierId', 'SupplierProductNumber', 'InSale', 'InSpotlight', 'InWebshop', 'IsOffer', 'IsBestseller', 'IsOutOfStock', 'HasImages', 'CategoryId', 'Number', 'AdvertiserUserId', 'DeliveryTimeId', 'Thickness', 'Height', 'Width', 'Length', 'Weight', 'Composition', 'MaterialId', 'BrandId', 'Title', 'Description', 'Note', 'CreatedOn', 'DayPrice', 'SalePrice', 'DiscountPercentage', 'PurchasePrice', 'ResellerPrice', 'AdvicePrice', 'BulkStockId', 'StockQuantity', 'VirtualStock', 'QuantityPerPack', 'YoutubeUrl', 'MetaTitle', 'MetaDescription', 'MetaKeyword', 'Hashtag', 'CalculatedAverageRating', 'VatId', 'UnitId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Product
	 */
	public function getModel(array $aData = null): Product
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductQuery = ProductQuery::create();
		     $oProduct = $oProductQuery->findOneById($aData['id']);
		     if (!$oProduct instanceof Product) {
		         throw new LogicException("Product should be an instance of Product but got something else." . __METHOD__);
		     }
		     $oProduct = $this->fillVo($aData, $oProduct);
		}
		else {
		     $oProduct = new Product();
		     if (!empty($aData)) {
		         $oProduct = $this->fillVo($aData, $oProduct);
		     }
		}
		return $oProduct;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Product
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Product
	{
		$oProduct = $this->getModel($aData);


		 if(!empty($oProduct))
		 {
		     $oProduct = $this->fillVo($aData, $oProduct);
		     $oProduct->save();
		 }
		return $oProduct;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Product $oModel
	 * @return Product
	 */
	protected function fillVo(array $aData, Product $oModel): Product
	{
		if(isset($aData['derrived_from_id'])) {
		     $oField = new DerrivedFromId();
		     $mValue = $oField->sanitize($aData['derrived_from_id']);
		     $oModel->setDerrivedFromId($mValue);
		}
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['import_tag'])) {
		     $oField = new ImportTag();
		     $mValue = $oField->sanitize($aData['import_tag']);
		     $oModel->setImportTag($mValue);
		}
		if(isset($aData['sku'])) {
		     $oField = new Sku();
		     $mValue = $oField->sanitize($aData['sku']);
		     $oModel->setSku($mValue);
		}
		if(isset($aData['ean'])) {
		     $oField = new Ean();
		     $mValue = $oField->sanitize($aData['ean']);
		     $oModel->setEan($mValue);
		}
		if(isset($aData['deleted_on'])) {
		     $oField = new DeletedOn();
		     $mValue = $oField->sanitize($aData['deleted_on']);
		     $oModel->setDeletedOn($mValue);
		}
		if(isset($aData['deleted_by_user_id'])) {
		     $oField = new DeletedByUserId();
		     $mValue = $oField->sanitize($aData['deleted_by_user_id']);
		     $oModel->setDeletedByUserId($mValue);
		}
		if(isset($aData['popularity'])) {
		     $oField = new Popularity();
		     $mValue = $oField->sanitize($aData['popularity']);
		     $oModel->setPopularity($mValue);
		}
		if(isset($aData['avg_rating'])) {
		     $oField = new AvgRating();
		     $mValue = $oField->sanitize($aData['avg_rating']);
		     $oModel->setAvgRating($mValue);
		}
		if(isset($aData['supplier'])) {
		     $oField = new Supplier();
		     $mValue = $oField->sanitize($aData['supplier']);
		     $oModel->setSupplier($mValue);
		}
		if(isset($aData['supplier_id'])) {
		     $oField = new SupplierId();
		     $mValue = $oField->sanitize($aData['supplier_id']);
		     $oModel->setSupplierId($mValue);
		}
		if(isset($aData['supplier_product_number'])) {
		     $oField = new SupplierProductNumber();
		     $mValue = $oField->sanitize($aData['supplier_product_number']);
		     $oModel->setSupplierProductNumber($mValue);
		}
		if(isset($aData['in_sale'])) {
		     $oField = new InSale();
		     $mValue = $oField->sanitize($aData['in_sale']);
		     $oModel->setInSale($mValue);
		}
		if(isset($aData['in_spotlight'])) {
		     $oField = new InSpotlight();
		     $mValue = $oField->sanitize($aData['in_spotlight']);
		     $oModel->setInSpotlight($mValue);
		}
		if(isset($aData['in_webshop'])) {
		     $oField = new InWebshop();
		     $mValue = $oField->sanitize($aData['in_webshop']);
		     $oModel->setInWebshop($mValue);
		}
		if(isset($aData['is_offer'])) {
		     $oField = new IsOffer();
		     $mValue = $oField->sanitize($aData['is_offer']);
		     $oModel->setIsOffer($mValue);
		}
		if(isset($aData['is_bestseller'])) {
		     $oField = new IsBestseller();
		     $mValue = $oField->sanitize($aData['is_bestseller']);
		     $oModel->setIsBestseller($mValue);
		}
		if(isset($aData['is_out_of_stock'])) {
		     $oField = new IsOutOfStock();
		     $mValue = $oField->sanitize($aData['is_out_of_stock']);
		     $oModel->setIsOutOfStock($mValue);
		}
		if(isset($aData['has_images'])) {
		     $oField = new HasImages();
		     $mValue = $oField->sanitize($aData['has_images']);
		     $oModel->setHasImages($mValue);
		}
		if(isset($aData['category_id'])) {
		     $oField = new CategoryId();
		     $mValue = $oField->sanitize($aData['category_id']);
		     $oModel->setCategoryId($mValue);
		}
		if(isset($aData['number'])) {
		     $oField = new Number();
		     $mValue = $oField->sanitize($aData['number']);
		     $oModel->setNumber($mValue);
		}
		if(isset($aData['advertiser_user_id'])) {
		     $oField = new AdvertiserUserId();
		     $mValue = $oField->sanitize($aData['advertiser_user_id']);
		     $oModel->setAdvertiserUserId($mValue);
		}
		if(isset($aData['delivery_time_id'])) {
		     $oField = new DeliveryTimeId();
		     $mValue = $oField->sanitize($aData['delivery_time_id']);
		     $oModel->setDeliveryTimeId($mValue);
		}
		if(isset($aData['thickness'])) {
		     $oField = new Thickness();
		     $mValue = $oField->sanitize($aData['thickness']);
		     $oModel->setThickness($mValue);
		}
		if(isset($aData['height'])) {
		     $oField = new Height();
		     $mValue = $oField->sanitize($aData['height']);
		     $oModel->setHeight($mValue);
		}
		if(isset($aData['width'])) {
		     $oField = new Width();
		     $mValue = $oField->sanitize($aData['width']);
		     $oModel->setWidth($mValue);
		}
		if(isset($aData['length'])) {
		     $oField = new Length();
		     $mValue = $oField->sanitize($aData['length']);
		     $oModel->setLength($mValue);
		}
		if(isset($aData['weight'])) {
		     $oField = new Weight();
		     $mValue = $oField->sanitize($aData['weight']);
		     $oModel->setWeight($mValue);
		}
		if(isset($aData['composition'])) {
		     $oField = new Composition();
		     $mValue = $oField->sanitize($aData['composition']);
		     $oModel->setComposition($mValue);
		}
		if(isset($aData['material_id'])) {
		     $oField = new MaterialId();
		     $mValue = $oField->sanitize($aData['material_id']);
		     $oModel->setMaterialId($mValue);
		}
		if(isset($aData['brand_id'])) {
		     $oField = new BrandId();
		     $mValue = $oField->sanitize($aData['brand_id']);
		     $oModel->setBrandId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['note'])) {
		     $oField = new Note();
		     $mValue = $oField->sanitize($aData['note']);
		     $oModel->setNote($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['day_price'])) {
		     $oField = new DayPrice();
		     $mValue = $oField->sanitize($aData['day_price']);
		     $oModel->setDayPrice($mValue);
		}
		if(isset($aData['sale_price'])) {
		     $oField = new SalePrice();
		     $mValue = $oField->sanitize($aData['sale_price']);
		     $oModel->setSalePrice($mValue);
		}
		if(isset($aData['discount_percentage'])) {
		     $oField = new DiscountPercentage();
		     $mValue = $oField->sanitize($aData['discount_percentage']);
		     $oModel->setDiscountPercentage($mValue);
		}
		if(isset($aData['purchase_price'])) {
		     $oField = new PurchasePrice();
		     $mValue = $oField->sanitize($aData['purchase_price']);
		     $oModel->setPurchasePrice($mValue);
		}
		if(isset($aData['reseller_price'])) {
		     $oField = new ResellerPrice();
		     $mValue = $oField->sanitize($aData['reseller_price']);
		     $oModel->setResellerPrice($mValue);
		}
		if(isset($aData['advice_price'])) {
		     $oField = new AdvicePrice();
		     $mValue = $oField->sanitize($aData['advice_price']);
		     $oModel->setAdvicePrice($mValue);
		}
		if(isset($aData['bulk_stock_id'])) {
		     $oField = new BulkStockId();
		     $mValue = $oField->sanitize($aData['bulk_stock_id']);
		     $oModel->setBulkStockId($mValue);
		}
		if(isset($aData['stock_quantity'])) {
		     $oField = new StockQuantity();
		     $mValue = $oField->sanitize($aData['stock_quantity']);
		     $oModel->setStockQuantity($mValue);
		}
		if(isset($aData['virtual_stock'])) {
		     $oField = new VirtualStock();
		     $mValue = $oField->sanitize($aData['virtual_stock']);
		     $oModel->setVirtualStock($mValue);
		}
		if(isset($aData['quantity_per_pack'])) {
		     $oField = new QuantityPerPack();
		     $mValue = $oField->sanitize($aData['quantity_per_pack']);
		     $oModel->setQuantityPerPack($mValue);
		}
		if(isset($aData['youtube_url'])) {
		     $oField = new YoutubeUrl();
		     $mValue = $oField->sanitize($aData['youtube_url']);
		     $oModel->setYoutubeUrl($mValue);
		}
		if(isset($aData['meta_title'])) {
		     $oField = new MetaTitle();
		     $mValue = $oField->sanitize($aData['meta_title']);
		     $oModel->setMetaTitle($mValue);
		}
		if(isset($aData['meta_description'])) {
		     $oField = new MetaDescription();
		     $mValue = $oField->sanitize($aData['meta_description']);
		     $oModel->setMetaDescription($mValue);
		}
		if(isset($aData['meta_keyword'])) {
		     $oField = new MetaKeyword();
		     $mValue = $oField->sanitize($aData['meta_keyword']);
		     $oModel->setMetaKeyword($mValue);
		}
		if(isset($aData['hashtag'])) {
		     $oField = new Hashtag();
		     $mValue = $oField->sanitize($aData['hashtag']);
		     $oModel->setHashtag($mValue);
		}
		if(isset($aData['calculated_average_rating'])) {
		     $oField = new CalculatedAverageRating();
		     $mValue = $oField->sanitize($aData['calculated_average_rating']);
		     $oModel->setCalculatedAverageRating($mValue);
		}
		if(isset($aData['vat_id'])) {
		     $oField = new VatId();
		     $mValue = $oField->sanitize($aData['vat_id']);
		     $oModel->setVatId($mValue);
		}
		if(isset($aData['unit_id'])) {
		     $oField = new UnitId();
		     $mValue = $oField->sanitize($aData['unit_id']);
		     $oModel->setUnitId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
