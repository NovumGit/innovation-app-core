<?php
namespace Crud\Product;

use Core\Translate;
use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Model\Product as ModelObject;
use Model\ProductProperty;
use Model\ProductPropertyQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class ProductPropertyField extends Field implements ICustomCrudField
{
    protected $sFieldName = null;
    protected $sFieldLabel = null;

    function store(ActiveRecordInterface $oObject, array $aData)
    {

        $oProductPropertyQuery = ProductPropertyQuery::create();
        $oProductPropertyQuery->filterByProductId($oObject->getId());
        $oProductPropertyQuery->filterByField($this->sFieldName);
        $oProductProperty = $oProductPropertyQuery->findOne();

        if(!$oProductProperty instanceof ProductProperty)
        {
            $oProductProperty = new ProductProperty();
            $oProductProperty->setProductId($oObject->getId());
            $oProductProperty->setField($this->sFieldName);
        }
        if(isset($aData[$this->sFieldName]) && $oProductProperty->getValue() != $aData[$this->sFieldName])
        {
            $oProductProperty->setValue($aData[$this->sFieldName]);
            $oProductProperty->save();
        }
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $_GET);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oProductPropertyQuery = ProductPropertyQuery::create();
        $oProductPropertyQuery->filterByProductId($oModelObject->getId());
        $oProductPropertyQuery->filterByField($this->sFieldName);
        $oProductProperty = $oProductPropertyQuery->findOne();
        $sBool = '0';
        if($oProductProperty instanceof ProductProperty)
        {
            $sBool = $oProductProperty->getValue();
        }
        $sValue = ($sBool == '1') ? Translate::fromCode('Ja') : Translate::fromCode('Nee');

        return '<td class="">'.$sValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oProductPropertyQuery = ProductPropertyQuery::create();
        $oProductPropertyQuery->filterByProductId($oModelObject->getId());
        $oProductPropertyQuery->filterByField($this->sFieldName);
        $oProductProperty = $oProductPropertyQuery->findOne();

        $sBool = '0';
        if($oProductProperty instanceof ProductProperty)
        {
            $sBool = $oProductProperty->getValue();
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, ($sBool == '1'), $bReadonly);
    }

}
