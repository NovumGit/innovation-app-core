<?php
namespace Crud\Product;

use Core\Setting;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\Product;
use Model\CustomerQuery;
use Model\Product\Image\ProductImageQuery;
use Model\Product\Image\ProductImage;
use Propel\Runtime\ActiveQuery\Criteria;
use Exception\LogicException;

class CrudProductManager extends FormManager implements IConfigurableCrud
{
    private $createNewUrl = '/product/edit';

    function getEntityTitle():string
    {
        return 'product';
    }
    function getNewFormTitle():string{
        return 'Product toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Product bewerken';
    }
    function getOverviewUrl():string
    {
        return '/product/overview';
    }
    function setCreateNewUrl($sUrl)
    {
        $this->createNewUrl = $sUrl;
    }
    function getCreateNewUrl():string
    {
        return $this->createNewUrl;
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Number',
            'Description',
            'VatId',
            'Width',
            'Height',
            'Thickness',
            'Delete',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Number',
            'Description',
            'VatId',
            'Width',
            'Height',
            'Thickness'
        ];
    }
    /**
     * @param $aData
     * @return Product
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oProduct = CustomerQuery::create()->findOneById($aData['id']);

            if(!$oProduct instanceof Product)
            {
                throw new LogicException("Product should be an instance of Product but got ".get_class($oProduct)." in ".__METHOD__);
            }
        }
        else
        {
            $oProduct = $this->fillVo(new Product(), $aData);
        }
        return $oProduct;
    }

    function store(array $aData = null)
    {

        $oProduct = $this->getModel($aData);

        if(!empty($oProduct))
        {
            $oProduct = $this->fillVo($oProduct, $aData);
            $oProduct->save();

            if(isset($_FILES['file']) && isset($_FILES['file']['name']['new_product_image']) && $_FILES['file']['error']['new_product_image'] === 0)
            {
                $oProductImageSortLatest = ProductImageQuery::create()->filterByProductId($oProduct->getId())->orderBySorting(Criteria::DESC)->findOne();
                $iSorting = 1;

                if($oProductImageSortLatest instanceof ProductImage)
                {
                    $iSorting = $oProductImageSortLatest->getSorting() + 1;
                }

                if(!is_writable('../data/img/product/'))
                {
                    throw new LogicException("Folder ../data/img/product/ is not writable, please fix!!");
                }
                $oProductImage = new ProductImage();
                $oProductImage->setProductId($oProduct->getId());
                $oProductImage->setSorting($iSorting);
                $oProductImage->setFileSize($_FILES['file']['size']['new_product_image']);
                $oProductImage->setIsOnline(1);
                $oProductImage->setFileType(0);
                $oProductImage->setOriginalName($_FILES['file']['name']['new_product_image']);

                $sExt = pathinfo($_FILES['file']['name']['new_product_image'], PATHINFO_EXTENSION);
                $oProductImage->setFileExt($sExt);
                $oProductImage->save();
                move_uploaded_file($_FILES['file']['tmp_name']['new_product_image'], "../data/img/product/{$oProductImage->getId()}.$sExt");
            }
        }
        $this->saveCustomFields($oProduct, $aData);
        return $oProduct;
    }
    function fillVo(Product $oProduct, $aData)
    {
        if(isset($aData['category_id'])){
            $oProduct->setCategoryId($aData['category_id']);
        }

        if(isset($aData['number']))
        {
            $oProduct->setNumber($aData['number']);
        }
        else if(!$oProduct->getId())
        {

            if(Setting::get('customer_auto_product_numbering') == '1')
            {
                $oProduct->setNumber(Product::generateNexProductNumber());
            }
            else
            {
                $oProduct->setNumber('');
            }
        }

        if(isset($aData['brand_id']))
        {
            $oProduct->setBrandId($aData['brand_id']);
        }

        if(isset($aData['number']))
        {
            $oProduct->setNumber($aData['number']);
        }
        else if(!isset($aData['number']) && $oProduct->getId() == null)
        {
            $oProduct->setNumber(null);
        }

        if(isset($aData['bulk_stock_id'])){
            $oProduct->setBulkStockId($aData['bulk_stock_id']);
        }
        if(isset($aData['note'])) {
            $oProduct->setNote($aData['note']);
        }
        if(isset($aData['description'])) {
            $oProduct->setDescription($aData['description']);
        }
        if(isset($aData['day_price'])) {
            $aData['day_price'] = str_replace(",", ".", $aData['day_price']);
            $oProduct->setDayPrice($aData['day_price']);
        }

        if(isset($aData['unit_id'])) {
            if(!$aData['unit_id'])
            {
                $aData['unit_id'] = null;
            }
            $oProduct->setUnitId($aData['unit_id']);
        }
        if(isset($aData['discount_percentage'])) {
            if(!$aData['discount_percentage'])
            {
                $aData['discount_percentage'] = null;
            }
            $oProduct->setDiscountPercentage($aData['discount_percentage']);
        }

        if(isset($aData['ean'])) {
            if(!$aData['ean'])
            {
                $aData['ean'] = null;
            }
            $oProduct->setEan($aData['ean']);
        }
        if(isset($aData['sku'])) {
            if(!$aData['sku'])
            {
                $aData['sku'] = null;
            }
            $oProduct->setSku($aData['sku']);
        }

        if(isset($aData['weight'])) {
            if(!$aData['weight'])
            {
                $aData['weight'] = null;
            }
            $oProduct->setWeight($aData['weight']);
        }
        if(isset($aData['length'])) {
            if(!$aData['length'])
            {
                $aData['length'] = null;
            }
            $oProduct->setLength($aData['length']);
        }
        if(isset($aData['width'])) {
            if(!$aData['width'])
            {
                $aData['width'] = null;
            }
            $oProduct->setWidth($aData['width']);
        }
        if(isset($aData['height'])) {
            if(!$aData['height'])
            {
                $aData['height'] = null;
            }
            $oProduct->setHeight($aData['height']);
        }
        if(isset($aData['thickness'])) {
            if(!$aData['thickness'])
            {
                $aData['thickness'] = null;
            }
            $oProduct->setThickness($aData['thickness']);
        }
        if(isset($aData['material_id'])) {
            if(!$aData['material_id'])
            {
                $aData['material_id'] = null;
            }
            $oProduct->setMaterialId($aData['material_id']);
        }
        if(isset($aData['vat_id'])) {
            if(!$aData['vat_id'])
            {
                $aData['vat_id'] = null;
            }
            $oProduct->setVatId($aData['vat_id']);
        }

        if(isset($aData['virtual_stock'])) {
            $oProduct->setVirtualStock($aData['virtual_stock']);
        }
        if(isset($aData['stock_quantity'])) {
            if(!$aData['stock_quantity'])
            {
                $aData['stock_quantity'] = null;
            }
            $oProduct->setStockQuantity($aData['stock_quantity']);
        }
        if(isset($aData['quantity_per_pack'])) {
            if(!$aData['quantity_per_pack'])
            {
                $aData['quantity_per_pack'] = null;
            }
            $oProduct->setQuantityPerPack($aData['quantity_per_pack']);
        }
        if(isset($aData['youtube_url'])) {
            if(!$aData['youtube_url'])
            {
                $aData['youtube_url'] = null;
            }

            $oProduct->setYoutubeUrl($aData['youtube_url']);
        }
        if(isset($aData['supplier'])) {
            if(!$aData['supplier'])
            {
                $aData['supplier'] = null;
            }
            $oProduct->setSupplier($aData['supplier']);
        }

        if(isset($aData['in_sale'])) {
            if(!$aData['in_sale'])
            {
                $aData['in_sale'] = null;
            }
            $oProduct->setInSale($aData['in_sale']);
        }
        if(isset($aData['in_spotlight'])) {
            if(!$aData['in_spotlight'])
            {
                $aData['in_spotlight'] = null;
            }
            $oProduct->setInSpotlight($aData['in_spotlight']);
        }
        if(isset($aData['delivery_time_id'])) {
            if(!$aData['delivery_time_id'])
            {
                $aData['delivery_time_id'] = null;
            }
            $oProduct->setDeliveryTimeId($aData['delivery_time_id']);
        }

        if(isset($aData['is_bestseller']))
        {
            if(!$aData['is_bestseller'])
            {
                $aData['is_bestseller'] = null;
            }
            $oProduct->setIsBestseller($aData['is_bestseller']);
        }

        if(isset($aData['is_out_of_stock'])) {
            if(!$aData['is_out_of_stock'])
            {
                $aData['is_out_of_stock'] = null;
            }
            $oProduct->setIsOutOfStock($aData['is_out_of_stock']);
        }

        if(isset($aData['meta_title'])) {
            if(!$aData['meta_title'])
            {
                $aData['meta_title'] = null;
            }
            $oProduct->setMetaTitle($aData['meta_title']);
        }
        if(isset($aData['title'])) {
            if(!$aData['title'])
            {
                $aData['title'] = null;
            }
            $oProduct->setTitle($aData['title']);
        }
        if(isset($aData['meta_description'])) {
            if(!$aData['meta_description'])
            {
                $aData['meta_description'] = null;
            }
            $oProduct->setMetaDescription($aData['meta_description']);
        }

        if(isset($aData['meta_keyword'])) {
            if(!$aData['meta_keyword'])
            {
                $aData['meta_keyword'] = null;
            }
            $oProduct->setMetaKeyword($aData['meta_keyword']);
        }

        if(isset($aData['deleted_by'])) {
            if(!$aData['deleted_by'])
            {
                $aData['deleted_by'] = null;
            }
            $oProduct->setDeletedByUserId($aData['deleted_by']);
        }

        if(isset($aData['derrived_from_id'])) {
            if(!$aData['derrived_from_id'])
            {
                $aData['derrived_from_id'] = null;
            }
            $oProduct->setDerrivedFromId($aData['derrived_from_id']);
        }
        if(isset($aData['hashtag'])) {
            if(!$aData['hashtag'])
            {
                $aData['hashtag'] = null;
            }
            $oProduct->setHashtag($aData['hashtag']);
        }

        if(isset($aData['sale_price'])) {
            if(!$aData['sale_price'])
            {
                $aData['sale_price'] = null;
            }
            $aData['sale_price'] = str_replace(",", ".", $aData['sale_price']);
            $oProduct->setSalePrice($aData['sale_price']);
        }
        if(isset($aData['purchase_price'])) {
            if(!$aData['purchase_price'])
            {
                $aData['purchase_price'] = null;
            }
            $aData['purchase_price'] = str_replace(",", ".", $aData['purchase_price']);
            $oProduct->setPurchasePrice($aData['purchase_price']);
        }
        if(isset($aData['reseller_price'])) {
            if(!$aData['reseller_price'])
            {
                $aData['reseller_price'] = null;
            }
            $aData['reseller_price'] = str_replace(",", ".", $aData['reseller_price']);
            $oProduct->setResellerPrice($aData['reseller_price']);
        }
        if(isset($aData['advice_price'])) {
            if(!$aData['advice_price'])
            {
                $aData['advice_price'] = null;
            }
            $aData['advice_price'] = str_replace(",", ".", $aData['advice_price']);
            $oProduct->setAdvicePrice($aData['advice_price']);
        }
        if(isset($aData['in_webshop'])) {
            if(!$aData['in_webshop'])
            {
                $aData['in_webshop'] = null;
            }
            $oProduct->setInWebshop($aData['in_webshop']);
        }
        if(isset($aData['in_spotlight'])) {
            if(!$aData['in_spotlight'])
            {
                $aData['in_spotlight'] = null;
            }
            $oProduct->setInSpotlight($aData['in_spotlight']);
        }

        if(isset($aData['is_offer'])) {
            if(!$aData['is_offer'])
            {
                $aData['is_offer'] = null;
            }
            $oProduct->setIsOffer($aData['is_offer']);
        }
        if(isset($aData['composition'])) {
            if(!$aData['composition'])
            {
                $aData['composition'] = null;
            }
            $oProduct->setComposition($aData['composition']);
        }
        return $oProduct;
    }
}
