<?php
namespace Crud\ProductDeliveryTimeTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductDeliveryTimeTranslation\FieldIterator;
use Crud\ProductDeliveryTimeTranslation\Field\DeliveryTimeId;
use Crud\ProductDeliveryTimeTranslation\Field\LanguageId;
use Crud\ProductDeliveryTimeTranslation\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ProductDeliveryTimeTranslationTableMap;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslation;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductDeliveryTimeTranslation instead if you need to override or add functionality.
 */
abstract class CrudProductDeliveryTimeTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductDeliveryTimeTranslationQuery::create();
	}


	public function getTableMap(): ProductDeliveryTimeTranslationTableMap
	{
		return new ProductDeliveryTimeTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductDeliveryTimeTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_delivery_time_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_delivery_time_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'DeliveryTimeId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'DeliveryTimeId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductDeliveryTimeTranslation
	 */
	public function getModel(array $aData = null): ProductDeliveryTimeTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductDeliveryTimeTranslationQuery = ProductDeliveryTimeTranslationQuery::create();
		     $oProductDeliveryTimeTranslation = $oProductDeliveryTimeTranslationQuery->findOneById($aData['id']);
		     if (!$oProductDeliveryTimeTranslation instanceof ProductDeliveryTimeTranslation) {
		         throw new LogicException("ProductDeliveryTimeTranslation should be an instance of ProductDeliveryTimeTranslation but got something else." . __METHOD__);
		     }
		     $oProductDeliveryTimeTranslation = $this->fillVo($aData, $oProductDeliveryTimeTranslation);
		}
		else {
		     $oProductDeliveryTimeTranslation = new ProductDeliveryTimeTranslation();
		     if (!empty($aData)) {
		         $oProductDeliveryTimeTranslation = $this->fillVo($aData, $oProductDeliveryTimeTranslation);
		     }
		}
		return $oProductDeliveryTimeTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductDeliveryTimeTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductDeliveryTimeTranslation
	{
		$oProductDeliveryTimeTranslation = $this->getModel($aData);


		 if(!empty($oProductDeliveryTimeTranslation))
		 {
		     $oProductDeliveryTimeTranslation = $this->fillVo($aData, $oProductDeliveryTimeTranslation);
		     $oProductDeliveryTimeTranslation->save();
		 }
		return $oProductDeliveryTimeTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductDeliveryTimeTranslation $oModel
	 * @return ProductDeliveryTimeTranslation
	 */
	protected function fillVo(array $aData, ProductDeliveryTimeTranslation $oModel): ProductDeliveryTimeTranslation
	{
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['delivery_time_id'])) {
		     $oField = new DeliveryTimeId();
		     $mValue = $oField->sanitize($aData['delivery_time_id']);
		     $oModel->setDeliveryTimeId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
