<?php
namespace Crud\ProductDeliveryTimeTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductDeliveryTimeTranslation\ICollectionField as ProductDeliveryTimeTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductDeliveryTimeTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductDeliveryTimeTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductDeliveryTimeTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductDeliveryTimeTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductDeliveryTimeTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductDeliveryTimeTranslationField
	{
		return current($this->aFields);
	}
}
