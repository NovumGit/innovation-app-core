<?php
namespace Crud\EventHandlerSequence\Base;

use Core\Utils;
use Crud;
use Crud\EventHandlerSequence\FieldIterator;
use Crud\EventHandlerSequence\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\Event\EventHandlerSequence;
use Model\System\Event\EventHandlerSequenceQuery;
use Model\System\Event\Map\EventHandlerSequenceTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify EventHandlerSequence instead if you need to override or add functionality.
 */
abstract class CrudEventHandlerSequenceManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return EventHandlerSequenceQuery::create();
	}


	public function getTableMap(): EventHandlerSequenceTableMap
	{
		return new EventHandlerSequenceTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bepaald de volgorde van events.";
	}


	public function getEntityTitle(): string
	{
		return "EventHandlerSequence";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "event toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "event aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return EventHandlerSequence
	 */
	public function getModel(array $aData = null): EventHandlerSequence
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oEventHandlerSequenceQuery = EventHandlerSequenceQuery::create();
		     $oEventHandlerSequence = $oEventHandlerSequenceQuery->findOneById($aData['id']);
		     if (!$oEventHandlerSequence instanceof EventHandlerSequence) {
		         throw new LogicException("EventHandlerSequence should be an instance of EventHandlerSequence but got something else." . __METHOD__);
		     }
		     $oEventHandlerSequence = $this->fillVo($aData, $oEventHandlerSequence);
		}
		else {
		     $oEventHandlerSequence = new EventHandlerSequence();
		     if (!empty($aData)) {
		         $oEventHandlerSequence = $this->fillVo($aData, $oEventHandlerSequence);
		     }
		}
		return $oEventHandlerSequence;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return EventHandlerSequence
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): EventHandlerSequence
	{
		$oEventHandlerSequence = $this->getModel($aData);


		 if(!empty($oEventHandlerSequence))
		 {
		     $oEventHandlerSequence = $this->fillVo($aData, $oEventHandlerSequence);
		     $oEventHandlerSequence->save();
		 }
		return $oEventHandlerSequence;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param EventHandlerSequence $oModel
	 * @return EventHandlerSequence
	 */
	protected function fillVo(array $aData, EventHandlerSequence $oModel): EventHandlerSequence
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
