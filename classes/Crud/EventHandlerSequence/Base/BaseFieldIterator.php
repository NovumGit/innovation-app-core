<?php
namespace Crud\EventHandlerSequence\Base;

use Crud\BaseCrudFieldIterator;
use Crud\EventHandlerSequence\ICollectionField as EventHandlerSequenceField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\EventHandlerSequence\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param EventHandlerSequenceField[] $aFields */
	private array $aFields = [];


	/**
	 * @param EventHandlerSequenceField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof EventHandlerSequenceField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(EventHandlerSequenceField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): EventHandlerSequenceField
	{
		return current($this->aFields);
	}
}
