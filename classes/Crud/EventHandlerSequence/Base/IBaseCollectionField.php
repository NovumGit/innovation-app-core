<?php
namespace Crud\EventHandlerSequence\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\EventHandlerSequence\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
