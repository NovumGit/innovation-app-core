<?php

namespace Crud\Icon;

/**
 * Skeleton subclass for representing a Icon.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudIconManager extends Base\CrudIconManager
{
}
