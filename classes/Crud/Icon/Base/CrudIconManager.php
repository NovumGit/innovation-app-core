<?php
namespace Crud\Icon\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Icon\FieldIterator;
use Crud\Icon\Field\Name;
use Exception\LogicException;
use Model\System\UI\Icon;
use Model\System\UI\IconQuery;
use Model\System\UI\Map\IconTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Icon instead if you need to override or add functionality.
 */
abstract class CrudIconManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return IconQuery::create();
	}


	public function getTableMap(): IconTableMap
	{
		return new IconTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat een lijst met iconen die in alle systemen beschikbaar zijn";
	}


	public function getEntityTitle(): string
	{
		return "Icon";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "icon toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "icon aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Icon
	 */
	public function getModel(array $aData = null): Icon
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oIconQuery = IconQuery::create();
		     $oIcon = $oIconQuery->findOneById($aData['id']);
		     if (!$oIcon instanceof Icon) {
		         throw new LogicException("Icon should be an instance of Icon but got something else." . __METHOD__);
		     }
		     $oIcon = $this->fillVo($aData, $oIcon);
		}
		else {
		     $oIcon = new Icon();
		     if (!empty($aData)) {
		         $oIcon = $this->fillVo($aData, $oIcon);
		     }
		}
		return $oIcon;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Icon
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Icon
	{
		$oIcon = $this->getModel($aData);


		 if(!empty($oIcon))
		 {
		     $oIcon = $this->fillVo($aData, $oIcon);
		     $oIcon->save();
		 }
		return $oIcon;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Icon $oModel
	 * @return Icon
	 */
	protected function fillVo(array $aData, Icon $oModel): Icon
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
