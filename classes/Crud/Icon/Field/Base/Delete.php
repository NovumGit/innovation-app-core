<?php 
namespace Crud\Icon\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\UI\Icon;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Icon)
		{
		     return "//system/icon/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Icon)
		{
		     return "//icon?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
