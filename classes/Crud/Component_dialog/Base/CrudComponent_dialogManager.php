<?php
namespace Crud\Component_dialog\Base;

use Core\Utils;
use Crud;
use Crud\Component_dialog\FieldIterator;
use Crud\Component_dialog\Field\ComponentKey;
use Crud\Component_dialog\Field\FkComponentDialogColorType;
use Crud\Component_dialog\Field\FkComponentDialogDisplay;
use Crud\Component_dialog\Field\Message;
use Crud\Component_dialog\Field\Title;
use Crud\Component_dialog\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Dialog\Component_dialog;
use Model\System\LowCode\Dialog\Component_dialogQuery;
use Model\System\LowCode\Dialog\Map\Component_dialogTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_dialog instead if you need to override or add functionality.
 */
abstract class CrudComponent_dialogManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_dialogQuery::create();
	}


	public function getTableMap(): Component_dialogTableMap
	{
		return new Component_dialogTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Dialog component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_dialog";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_dialog toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_dialog aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ComponentKey', 'Title', 'Message', 'FkComponentDialogDisplay', 'FkComponentDialogColorType', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ComponentKey', 'Title', 'Message', 'FkComponentDialogDisplay', 'FkComponentDialogColorType', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_dialog
	 */
	public function getModel(array $aData = null): Component_dialog
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_dialogQuery = Component_dialogQuery::create();
		     $oComponent_dialog = $oComponent_dialogQuery->findOneById($aData['id']);
		     if (!$oComponent_dialog instanceof Component_dialog) {
		         throw new LogicException("Component_dialog should be an instance of Component_dialog but got something else." . __METHOD__);
		     }
		     $oComponent_dialog = $this->fillVo($aData, $oComponent_dialog);
		}
		else {
		     $oComponent_dialog = new Component_dialog();
		     if (!empty($aData)) {
		         $oComponent_dialog = $this->fillVo($aData, $oComponent_dialog);
		     }
		}
		return $oComponent_dialog;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_dialog
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_dialog
	{
		$oComponent_dialog = $this->getModel($aData);


		 if(!empty($oComponent_dialog))
		 {
		     $oComponent_dialog = $this->fillVo($aData, $oComponent_dialog);
		     $oComponent_dialog->save();
		 }
		return $oComponent_dialog;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_dialog $oModel
	 * @return Component_dialog
	 */
	protected function fillVo(array $aData, Component_dialog $oModel): Component_dialog
	{
		if(isset($aData['component_key'])) {
		     $oField = new ComponentKey();
		     $mValue = $oField->sanitize($aData['component_key']);
		     $oModel->setComponentKey($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['message'])) {
		     $oField = new Message();
		     $mValue = $oField->sanitize($aData['message']);
		     $oModel->setMessage($mValue);
		}
		if(isset($aData['fk_component_dialog_display'])) {
		     $oField = new FkComponentDialogDisplay();
		     $mValue = $oField->sanitize($aData['fk_component_dialog_display']);
		     $oModel->setFkComponentDialogDisplay($mValue);
		}
		if(isset($aData['fk_component_dialog_color_type'])) {
		     $oField = new FkComponentDialogColorType();
		     $mValue = $oField->sanitize($aData['fk_component_dialog_color_type']);
		     $oModel->setFkComponentDialogColorType($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
