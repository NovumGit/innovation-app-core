<?php
namespace Crud\Component_dialog\Field;

use Crud\Component_dialog\Field\Base\Message as BaseMessage;

/**
 * Skeleton subclass for representing message field from the component_dialog table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Message extends BaseMessage
{
}
