<?php 
namespace Crud\Component_dialog\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Dialog\Component_dialog;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_dialog)
		{
		     return "//system/component_dialog/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_dialog)
		{
		     return "//component_dialog?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
