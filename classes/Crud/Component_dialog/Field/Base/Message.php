<?php
namespace Crud\Component_dialog\Field\Base;

use Crud\Component_dialog\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'message' crud field from the 'component_dialog' table.
 * This class is auto generated and should not be modified.
 */
abstract class Message extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'message';
	protected $sFieldLabel = 'Text';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMessage';
	protected $sFqModelClassname = '\Model\System\LowCode\Dialog\Component_dialog';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
