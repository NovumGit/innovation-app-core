<?php
namespace Crud\Component_dialog\Field\Base;

use Core\Utils;
use Crud\Component_dialog\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\LowCode\Dialog\Component_dialog_displayQuery;

/**
 * Base class that represents the 'fk_component_dialog_display' crud field from the 'component_dialog' table.
 * This class is auto generated and should not be modified.
 */
abstract class FkComponentDialogDisplay extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'fk_component_dialog_display';
	protected $sFieldLabel = 'Display';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFkComponentDialogDisplay';
	protected $sFqModelClassname = '\Model\System\LowCode\Dialog\Component_dialog';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\LowCode\Dialog\Component_dialog_displayQuery::create()->orderByItemLabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getItemLabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\LowCode\Dialog\Component_dialog_displayQuery::create()->findOneById($iItemId)->getItemLabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
