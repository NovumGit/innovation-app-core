<?php
namespace Crud\Component_dialog;

/**
 * Skeleton subclass for representing a Component_dialog.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudComponent_dialogManager extends Base\CrudComponent_dialogManager
{
}
