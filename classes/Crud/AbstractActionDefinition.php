<?php
namespace Crud;

use Core\AbstractEvent;
use Core\Mime\VoidMime;

abstract class AbstractActionDefinition extends AbstractEvent implements IActionDefinition {

    abstract function trigger($mArguments):void;
    abstract function getDescription():string;
    abstract function getExampleRequest():string;
    abstract function getExampleAnswer():string;

    // CrudActions are compatible with events but they should never directly send content to the screen.
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }

}
