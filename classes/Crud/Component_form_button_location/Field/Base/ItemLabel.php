<?php
namespace Crud\Component_form_button_location\Field\Base;

use Crud\Component_form_button_location\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'item_label' crud field from the 'component_form_button_location' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemLabel extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_label';
	protected $sFieldLabel = 'Button location';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemLabel';
	protected $sFqModelClassname = '\Model\System\LowCode\Form\Component_form_button_location';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
