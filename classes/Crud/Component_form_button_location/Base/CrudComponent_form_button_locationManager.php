<?php
namespace Crud\Component_form_button_location\Base;

use Core\Utils;
use Crud;
use Crud\Component_form_button_location\FieldIterator;
use Crud\Component_form_button_location\Field\ItemKey;
use Crud\Component_form_button_location\Field\ItemLabel;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Form\Component_form_button_location;
use Model\System\LowCode\Form\Component_form_button_locationQuery;
use Model\System\LowCode\Form\Map\Component_form_button_locationTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_form_button_location instead if you need to override or add functionality.
 */
abstract class CrudComponent_form_button_locationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_form_button_locationQuery::create();
	}


	public function getTableMap(): Component_form_button_locationTableMap
	{
		return new Component_form_button_locationTableMap();
	}


	public function getShortDescription(): string
	{
		return "Used for validation and to populate dropdown values.";
	}


	public function getEntityTitle(): string
	{
		return "Component_form_button_location";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_form_button_location toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_form_button_location aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_form_button_location
	 */
	public function getModel(array $aData = null): Component_form_button_location
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_form_button_locationQuery = Component_form_button_locationQuery::create();
		     $oComponent_form_button_location = $oComponent_form_button_locationQuery->findOneById($aData['id']);
		     if (!$oComponent_form_button_location instanceof Component_form_button_location) {
		         throw new LogicException("Component_form_button_location should be an instance of Component_form_button_location but got something else." . __METHOD__);
		     }
		     $oComponent_form_button_location = $this->fillVo($aData, $oComponent_form_button_location);
		}
		else {
		     $oComponent_form_button_location = new Component_form_button_location();
		     if (!empty($aData)) {
		         $oComponent_form_button_location = $this->fillVo($aData, $oComponent_form_button_location);
		     }
		}
		return $oComponent_form_button_location;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_form_button_location
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_form_button_location
	{
		$oComponent_form_button_location = $this->getModel($aData);


		 if(!empty($oComponent_form_button_location))
		 {
		     $oComponent_form_button_location = $this->fillVo($aData, $oComponent_form_button_location);
		     $oComponent_form_button_location->save();
		 }
		return $oComponent_form_button_location;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_form_button_location $oModel
	 * @return Component_form_button_location
	 */
	protected function fillVo(array $aData, Component_form_button_location $oModel): Component_form_button_location
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_label'])) {
		     $oField = new ItemLabel();
		     $mValue = $oField->sanitize($aData['item_label']);
		     $oModel->setItemLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
