<?php
namespace Crud\Product_delivery_time;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\ProductDeliveryTime;
use Model\Setting\MasterTable\ProductDeliveryTimeQuery;
use Propel\Runtime\Exception\LogicException;

class CrudProduct_delivery_timeManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'levertijd';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/product_delivery_time/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/product_delivery_time/edit';
    }
    function getNewFormTitle():string{
        return 'Levertijd toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Levertijd wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name'
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oProductDeliveryTimeQuery = new ProductDeliveryTimeQuery();

            $oProductDeliveryTime = $oProductDeliveryTimeQuery->findOneById($aData['id']);

            if(!$oProductDeliveryTime instanceof ProductDeliveryTime){
                throw new LogicException("ProductDeliveryTime should be an instance of ProductDeliveryTime but got ".get_class($oProductDeliveryTime)." in ".__METHOD__);
            }
        }
        else
        {
            $oProductDeliveryTime = new ProductDeliveryTime();

            if(!empty($aData))
            {
                $oProductDeliveryTime = $this->fillVo($aData, $oProductDeliveryTime);
            }
        }

        return $oProductDeliveryTime;
    }

    function store(array $aData = null)
    {
        $oProductDeliveryTime = $this->getModel($aData);

        if(!empty($oProductDeliveryTime))
        {
            $oProductDeliveryTime = $this->fillVo($aData, $oProductDeliveryTime);

            $oProductDeliveryTime->save();
        }
        return $oProductDeliveryTime;
    }

    private function fillVo($aData, ProductDeliveryTime $oProductDeliveryTime)
    {
        if(isset($aData['name'])){$oProductDeliveryTime->setName($aData['name']);}

        return $oProductDeliveryTime;
    }
}
