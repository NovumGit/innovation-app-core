<?php
namespace Crud\Product_delivery_time\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\Setting\MasterTable\ProductDeliveryTime;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oProductDeliveryTime){

        if(!$oProductDeliveryTime instanceof ProductDeliveryTime)
        {
            throw new InvalidArgumentException('Expected an instance of ProductDeliveryTime but got '.get_class($oProductDeliveryTime));
        }

        return '/setting/mastertable/product_delivery_time/edit?_do=Delete&id='.$oProductDeliveryTime->getId();
    }

    function getUnDeleteUrl($oProductDeliveryTime){
        throw new LogicException("Product delivery time cannot be undeleted");
    }
}