<?php
namespace Crud\Product_delivery_time\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\ProductDeliveryTime;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oProductDeliveryTime){

        if(!$oProductDeliveryTime instanceof ProductDeliveryTime)
        {
            throw new InvalidArgumentException('Expected an instance of ProductDeliveryTime but got '.get_class($oProductDeliveryTime));
        }

        return '/setting/mastertable/product_delivery_time/edit?id='.$oProductDeliveryTime->getId();
    }
}