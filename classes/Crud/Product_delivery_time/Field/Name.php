<?php
namespace Crud\Product_delivery_time\Field;

use Core\Setting;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Product;
use Model\CustomerQuery;
use Model\Setting\MasterTable\Base\ProductDeliveryTime;
use Model\Setting\MasterTable\Base\ProductDeliveryTimeQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class Name extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Naam/titel';
    private $sIcon = 'tag';
    private $sPlaceHolder = 'Artikelnummer';

    function getDataType():string
    {
        return 'string';
    }

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function hasValidations() { return true; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(!isset($aPostedData['name']) || empty($aPostedData['name']))
        {
            $mResponse[] = 'Vul een naam/titel in voor deze levertijd.';
        }
        else if(isset($aPostedData['name']))
        {
            $oProductDeliveryTimeQuery = ProductDeliveryTimeQuery::create()->filterByName($aPostedData['name']);

            if(isset($aPostedData['id']) && !empty($aPostedData['name']))
            {
                $oProductDeliveryTimeQuery->filterById($aPostedData['id'], Criteria::NOT_EQUAL);
            }
            $oProductDeliveryTime = $oProductDeliveryTimeQuery->findOne();

            if($oProductDeliveryTime instanceof ProductDeliveryTime)
            {
                $mResponse[] = 'Er is al een andere leverijd met deze titel, om verwarring te voorkomen is dat niet toegestaan.';
            }
        }
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oProductDeliveryTime)
    {
        if(!$oProductDeliveryTime instanceof ProductDeliveryTime)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__);
        }
        return '<td class="">'.$oProductDeliveryTime->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof ProductDeliveryTime)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Product\\Product in ".__METHOD__." got ".get_class($mData));
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
