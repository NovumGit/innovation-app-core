<?php
namespace Crud\Link_block_customer_details;

use Crud\Link_blockManager;

class CrudLink_block_customer_detailsManager extends Link_blockManager  {

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Description',
            'Keywords',
            'SocialMedia'
        ];
    }
}

