<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Description extends Link {

    protected $sFieldLabel = 'Bedrijfsomschrijving';

    function getLinkUrl()
    {
        return '/crm/details/description';
    }


}