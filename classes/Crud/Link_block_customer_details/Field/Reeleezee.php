<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Reeleezee extends Link {

    protected $sFieldLabel = 'Reeleezee koppeling';

    function getLinkUrl()
    {
        return '/details/reeleezee';
    }

}