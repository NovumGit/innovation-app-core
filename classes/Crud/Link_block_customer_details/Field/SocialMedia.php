<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class SocialMedia extends Link {

    protected $sFieldLabel = 'Social media';

    function getLinkUrl()
    {
        return '/crm/details/socialmedia';
    }
}