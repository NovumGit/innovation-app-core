<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Photogallery extends Link {

    protected $sFieldLabel = 'Bedrijfsfoto\'s';

    function getLinkUrl()
    {
        return '/crm/details/photogallery';
    }



}