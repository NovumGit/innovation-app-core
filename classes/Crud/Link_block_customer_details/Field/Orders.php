<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Orders extends Link {

    protected $sFieldLabel = 'Bestellingen';

    function getLinkUrl()
    {
        return '/crm/orders/overview';
    }


}