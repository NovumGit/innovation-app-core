<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Paymentmethod extends Link {

    protected $sFieldLabel = 'Betaalmethoden';

    function getLinkUrl()
    {
        return '/crm/details/paymentmethod';
    }
}