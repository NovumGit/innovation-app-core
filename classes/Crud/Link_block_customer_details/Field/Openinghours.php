<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Openinghours extends Link {

    protected $sFieldLabel = 'Openingstijden';

    function getLinkUrl()
    {
        return '/crm/details/openinghours';
    }


}