<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Addresses extends Link
{
    protected $sFieldLabel = 'Adressen';

    function getLinkUrl()
    {
        return '/crm/address/overview';
    }
}