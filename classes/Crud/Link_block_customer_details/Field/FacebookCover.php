<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class FacebookCover extends Link
{

    protected $sFieldLabel = 'Facebook profiel';

    function getLinkUrl()
    {
        return '/crm/details/facebookcover';
    }


}