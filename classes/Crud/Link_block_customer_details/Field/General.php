<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class General extends Link {

    protected $sFieldLabel = 'Algemeen';

    function getLinkUrl()
    {
        return '/crm/customer_edit';
    }


}