<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class WelcomeEmail extends Link {

    protected $sFieldLabel = 'Welkomst e-mail';

    function getLinkUrl()
    {
        return '/crm/email/welcome';
    }
}