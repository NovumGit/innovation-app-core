<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class GoogleplusCover extends Link
{

    protected $sFieldLabel = 'Googleplus profiel';

    function getLinkUrl()
    {
        return '/crm/details/googlepluscover';
    }
}