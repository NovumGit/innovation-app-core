<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Openinghours_special extends Link {

    protected $sFieldLabel = 'Speciale openingstijden';

    function getLinkUrl()
    {
        return '/crm/details/openinghours_special';
    }
}