<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class Keywords extends Link {

    protected $sFieldLabel = 'Tags / keywords';

    function getLinkUrl()
    {
        return '/crm/details/keywords';
    }



}