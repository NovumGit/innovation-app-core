<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class CompanyLogo extends Link {

    protected $sFieldLabel = ' Bedrijfs logo';

    function getLinkUrl()
    {
        return '/crm/details/logo';
    }
}