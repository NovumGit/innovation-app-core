<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class TransportAndParking extends Link {

    protected $sFieldLabel = ' Bereikbaarheid en parkeren';

    function getLinkUrl()
    {
        return '/crm/details/parking_public_transport';
    }

}