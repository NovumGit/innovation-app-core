<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class PasswordReset extends Link {

    protected $sFieldLabel = 'Wachtwoord reset';

    function getLinkUrl()
    {
        return '/crm/email/passreset';
    }

}