<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class ContactInfo extends Link {

    protected $sFieldLabel = 'Contact informatie';

    function getLinkUrl()
    {
        return '/crm/details/contactinfo';
    }


}