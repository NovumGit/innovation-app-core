<?php
namespace Crud\Link_block_customer_details\Field;

use Crud\Link;

class PushStatus extends Link {

    protected $sFieldLabel = 'Push status';

    function getLinkUrl()
    {
        return '/crm/api_connect/push_status';
    }



}