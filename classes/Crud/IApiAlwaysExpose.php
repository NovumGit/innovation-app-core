<?php
namespace Crud;

use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * Can be used to tag FormManagers that should always appear in the available andpoints of an API.
 *
 * Interface IApAlwaysExpose
 * @package Crud
 */
interface IApiAlwaysExpose
{
}
