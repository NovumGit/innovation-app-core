<?php
namespace Crud\SaleOrderItem\Field;

use Crud\SaleOrderItem\Field\Base\ProductNumber as BaseProductNumber;

/**
 * Skeleton subclass for representing product_number field from the sale_order_item table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ProductNumber extends BaseProductNumber
{
}
