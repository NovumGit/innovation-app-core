<?php
namespace Crud\SaleOrderItem\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderItem\ICollectionField as SaleOrderItemField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderItem\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderItemField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderItemField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderItemField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderItemField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderItemField
	{
		return current($this->aFields);
	}
}
