<?php
namespace Crud\SaleOrderItem\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderItem\FieldIterator;
use Crud\SaleOrderItem\Field\CreatedOn;
use Crud\SaleOrderItem\Field\Description;
use Crud\SaleOrderItem\Field\ManualEntry;
use Crud\SaleOrderItem\Field\Price;
use Crud\SaleOrderItem\Field\ProductNumber;
use Crud\SaleOrderItem\Field\Quantity;
use Crud\SaleOrderItem\Field\SaleOrderId;
use Crud\SaleOrderItem\Field\SingleItemWeight;
use Crud\SaleOrderItem\Field\VatPercentage;
use Exception\LogicException;
use Model\Sale\Map\SaleOrderItemTableMap;
use Model\Sale\SaleOrderItem;
use Model\Sale\SaleOrderItemQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderItem instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderItemManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderItemQuery::create();
	}


	public function getTableMap(): SaleOrderItemTableMap
	{
		return new SaleOrderItemTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderItem";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_item toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_item aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'ProductNumber', 'Description', 'Quantity', 'SingleItemWeight', 'Price', 'VatPercentage', 'ManualEntry'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'ProductNumber', 'Description', 'Quantity', 'SingleItemWeight', 'Price', 'VatPercentage', 'ManualEntry'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderItem
	 */
	public function getModel(array $aData = null): SaleOrderItem
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderItemQuery = SaleOrderItemQuery::create();
		     $oSaleOrderItem = $oSaleOrderItemQuery->findOneById($aData['id']);
		     if (!$oSaleOrderItem instanceof SaleOrderItem) {
		         throw new LogicException("SaleOrderItem should be an instance of SaleOrderItem but got something else." . __METHOD__);
		     }
		     $oSaleOrderItem = $this->fillVo($aData, $oSaleOrderItem);
		}
		else {
		     $oSaleOrderItem = new SaleOrderItem();
		     if (!empty($aData)) {
		         $oSaleOrderItem = $this->fillVo($aData, $oSaleOrderItem);
		     }
		}
		return $oSaleOrderItem;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderItem
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderItem
	{
		$oSaleOrderItem = $this->getModel($aData);


		 if(!empty($oSaleOrderItem))
		 {
		     $oSaleOrderItem = $this->fillVo($aData, $oSaleOrderItem);
		     $oSaleOrderItem->save();
		 }
		return $oSaleOrderItem;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderItem $oModel
	 * @return SaleOrderItem
	 */
	protected function fillVo(array $aData, SaleOrderItem $oModel): SaleOrderItem
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['sale_order_id'])) {
		     $oField = new SaleOrderId();
		     $mValue = $oField->sanitize($aData['sale_order_id']);
		     $oModel->setSaleOrderId($mValue);
		}
		if(isset($aData['product_number'])) {
		     $oField = new ProductNumber();
		     $mValue = $oField->sanitize($aData['product_number']);
		     $oModel->setProductNumber($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['quantity'])) {
		     $oField = new Quantity();
		     $mValue = $oField->sanitize($aData['quantity']);
		     $oModel->setQuantity($mValue);
		}
		if(isset($aData['single_item_weight'])) {
		     $oField = new SingleItemWeight();
		     $mValue = $oField->sanitize($aData['single_item_weight']);
		     $oModel->setSingleItemWeight($mValue);
		}
		if(isset($aData['price'])) {
		     $oField = new Price();
		     $mValue = $oField->sanitize($aData['price']);
		     $oModel->setPrice($mValue);
		}
		if(isset($aData['vat_percentage'])) {
		     $oField = new VatPercentage();
		     $mValue = $oField->sanitize($aData['vat_percentage']);
		     $oModel->setVatPercentage($mValue);
		}
		if(isset($aData['manual_entry'])) {
		     $oField = new ManualEntry();
		     $mValue = $oField->sanitize($aData['manual_entry']);
		     $oModel->setManualEntry($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
