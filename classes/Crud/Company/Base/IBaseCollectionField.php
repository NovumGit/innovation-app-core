<?php
namespace Crud\Company\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Company\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
