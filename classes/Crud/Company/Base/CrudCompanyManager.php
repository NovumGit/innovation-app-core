<?php
namespace Crud\Company\Base;

use Core\Utils;
use Crud;
use Crud\Company\FieldIterator;
use Crud\Company\Field\Bic;
use Crud\Company\Field\ChamberOfCommerce;
use Crud\Company\Field\CompanyLogoId;
use Crud\Company\Field\CompanyName;
use Crud\Company\Field\CreatedOn;
use Crud\Company\Field\DeliveryCity;
use Crud\Company\Field\DeliveryCompanyName;
use Crud\Company\Field\DeliveryCountryId;
use Crud\Company\Field\DeliveryNumber;
use Crud\Company\Field\DeliveryNumberAdd;
use Crud\Company\Field\DeliveryPostal;
use Crud\Company\Field\DeliveryStreet;
use Crud\Company\Field\Email;
use Crud\Company\Field\Fax;
use Crud\Company\Field\GeneralCity;
use Crud\Company\Field\GeneralCountryId;
use Crud\Company\Field\GeneralNumber;
use Crud\Company\Field\GeneralNumberAdd;
use Crud\Company\Field\GeneralPoBox;
use Crud\Company\Field\GeneralPostal;
use Crud\Company\Field\GeneralStreet;
use Crud\Company\Field\HasDeliveryAddress;
use Crud\Company\Field\HasInvoiceAddress;
use Crud\Company\Field\Iban;
use Crud\Company\Field\InvoiceCity;
use Crud\Company\Field\InvoiceCompanyName;
use Crud\Company\Field\InvoiceCountryId;
use Crud\Company\Field\InvoiceNumber;
use Crud\Company\Field\InvoiceNumberAdd;
use Crud\Company\Field\InvoicePostal;
use Crud\Company\Field\InvoiceStreet;
use Crud\Company\Field\IsDefaultSupplier;
use Crud\Company\Field\ItemDeleted;
use Crud\Company\Field\LegalFormId;
use Crud\Company\Field\MaxShippingCosts;
use Crud\Company\Field\Phone;
use Crud\Company\Field\Slogan;
use Crud\Company\Field\VatNumber;
use Crud\Company\Field\Website;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Company\Company;
use Model\Company\CompanyQuery;
use Model\Company\Map\CompanyTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Company instead if you need to override or add functionality.
 */
abstract class CrudCompanyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CompanyQuery::create();
	}


	public function getTableMap(): CompanyTableMap
	{
		return new CompanyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Company";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "own_company toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "own_company aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IsDefaultSupplier', 'CreatedOn', 'ItemDeleted', 'CompanyName', 'Slogan', 'ChamberOfCommerce', 'VatNumber', 'LegalFormId', 'Iban', 'Bic', 'Phone', 'Fax', 'Website', 'Email', 'GeneralStreet', 'GeneralNumber', 'GeneralNumberAdd', 'GeneralPostal', 'GeneralCity', 'GeneralPoBox', 'GeneralCountryId', 'HasInvoiceAddress', 'InvoiceCompanyName', 'InvoiceStreet', 'InvoiceNumber', 'InvoiceNumberAdd', 'InvoicePostal', 'InvoiceCity', 'InvoiceCountryId', 'HasDeliveryAddress', 'DeliveryCompanyName', 'DeliveryStreet', 'DeliveryNumber', 'DeliveryNumberAdd', 'DeliveryPostal', 'DeliveryCity', 'DeliveryCountryId', 'CompanyLogoId', 'MaxShippingCosts'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IsDefaultSupplier', 'CreatedOn', 'ItemDeleted', 'CompanyName', 'Slogan', 'ChamberOfCommerce', 'VatNumber', 'LegalFormId', 'Iban', 'Bic', 'Phone', 'Fax', 'Website', 'Email', 'GeneralStreet', 'GeneralNumber', 'GeneralNumberAdd', 'GeneralPostal', 'GeneralCity', 'GeneralPoBox', 'GeneralCountryId', 'HasInvoiceAddress', 'InvoiceCompanyName', 'InvoiceStreet', 'InvoiceNumber', 'InvoiceNumberAdd', 'InvoicePostal', 'InvoiceCity', 'InvoiceCountryId', 'HasDeliveryAddress', 'DeliveryCompanyName', 'DeliveryStreet', 'DeliveryNumber', 'DeliveryNumberAdd', 'DeliveryPostal', 'DeliveryCity', 'DeliveryCountryId', 'CompanyLogoId', 'MaxShippingCosts'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Company
	 */
	public function getModel(array $aData = null): Company
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCompanyQuery = CompanyQuery::create();
		     $oCompany = $oCompanyQuery->findOneById($aData['id']);
		     if (!$oCompany instanceof Company) {
		         throw new LogicException("Company should be an instance of Company but got something else." . __METHOD__);
		     }
		     $oCompany = $this->fillVo($aData, $oCompany);
		}
		else {
		     $oCompany = new Company();
		     if (!empty($aData)) {
		         $oCompany = $this->fillVo($aData, $oCompany);
		     }
		}
		return $oCompany;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Company
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Company
	{
		$oCompany = $this->getModel($aData);


		 if(!empty($oCompany))
		 {
		     $oCompany = $this->fillVo($aData, $oCompany);
		     $oCompany->save();
		 }
		return $oCompany;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Company $oModel
	 * @return Company
	 */
	protected function fillVo(array $aData, Company $oModel): Company
	{
		if(isset($aData['is_default_supplier'])) {
		     $oField = new IsDefaultSupplier();
		     $mValue = $oField->sanitize($aData['is_default_supplier']);
		     $oModel->setIsDefaultSupplier($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['is_deleted'])) {
		     $oField = new ItemDeleted();
		     $mValue = $oField->sanitize($aData['is_deleted']);
		     $oModel->setItemDeleted($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['slogan'])) {
		     $oField = new Slogan();
		     $mValue = $oField->sanitize($aData['slogan']);
		     $oModel->setSlogan($mValue);
		}
		if(isset($aData['chamber_of_commerce'])) {
		     $oField = new ChamberOfCommerce();
		     $mValue = $oField->sanitize($aData['chamber_of_commerce']);
		     $oModel->setChamberOfCommerce($mValue);
		}
		if(isset($aData['vat_number'])) {
		     $oField = new VatNumber();
		     $mValue = $oField->sanitize($aData['vat_number']);
		     $oModel->setVatNumber($mValue);
		}
		if(isset($aData['legal_form_id'])) {
		     $oField = new LegalFormId();
		     $mValue = $oField->sanitize($aData['legal_form_id']);
		     $oModel->setLegalFormId($mValue);
		}
		if(isset($aData['iban'])) {
		     $oField = new Iban();
		     $mValue = $oField->sanitize($aData['iban']);
		     $oModel->setIban($mValue);
		}
		if(isset($aData['bic'])) {
		     $oField = new Bic();
		     $mValue = $oField->sanitize($aData['bic']);
		     $oModel->setBic($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['fax'])) {
		     $oField = new Fax();
		     $mValue = $oField->sanitize($aData['fax']);
		     $oModel->setFax($mValue);
		}
		if(isset($aData['website'])) {
		     $oField = new Website();
		     $mValue = $oField->sanitize($aData['website']);
		     $oModel->setWebsite($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['general_street'])) {
		     $oField = new GeneralStreet();
		     $mValue = $oField->sanitize($aData['general_street']);
		     $oModel->setGeneralStreet($mValue);
		}
		if(isset($aData['general_number'])) {
		     $oField = new GeneralNumber();
		     $mValue = $oField->sanitize($aData['general_number']);
		     $oModel->setGeneralNumber($mValue);
		}
		if(isset($aData['general_number_add'])) {
		     $oField = new GeneralNumberAdd();
		     $mValue = $oField->sanitize($aData['general_number_add']);
		     $oModel->setGeneralNumberAdd($mValue);
		}
		if(isset($aData['general_postal'])) {
		     $oField = new GeneralPostal();
		     $mValue = $oField->sanitize($aData['general_postal']);
		     $oModel->setGeneralPostal($mValue);
		}
		if(isset($aData['general_city'])) {
		     $oField = new GeneralCity();
		     $mValue = $oField->sanitize($aData['general_city']);
		     $oModel->setGeneralCity($mValue);
		}
		if(isset($aData['general_po_box'])) {
		     $oField = new GeneralPoBox();
		     $mValue = $oField->sanitize($aData['general_po_box']);
		     $oModel->setGeneralPoBox($mValue);
		}
		if(isset($aData['general_country_id'])) {
		     $oField = new GeneralCountryId();
		     $mValue = $oField->sanitize($aData['general_country_id']);
		     $oModel->setGeneralCountryId($mValue);
		}
		if(isset($aData['has_invoice_address'])) {
		     $oField = new HasInvoiceAddress();
		     $mValue = $oField->sanitize($aData['has_invoice_address']);
		     $oModel->setHasInvoiceAddress($mValue);
		}
		if(isset($aData['invoice_company_name'])) {
		     $oField = new InvoiceCompanyName();
		     $mValue = $oField->sanitize($aData['invoice_company_name']);
		     $oModel->setInvoiceCompanyName($mValue);
		}
		if(isset($aData['invoice_street'])) {
		     $oField = new InvoiceStreet();
		     $mValue = $oField->sanitize($aData['invoice_street']);
		     $oModel->setInvoiceStreet($mValue);
		}
		if(isset($aData['invoice_number'])) {
		     $oField = new InvoiceNumber();
		     $mValue = $oField->sanitize($aData['invoice_number']);
		     $oModel->setInvoiceNumber($mValue);
		}
		if(isset($aData['invoice_number_add'])) {
		     $oField = new InvoiceNumberAdd();
		     $mValue = $oField->sanitize($aData['invoice_number_add']);
		     $oModel->setInvoiceNumberAdd($mValue);
		}
		if(isset($aData['invoice_postal'])) {
		     $oField = new InvoicePostal();
		     $mValue = $oField->sanitize($aData['invoice_postal']);
		     $oModel->setInvoicePostal($mValue);
		}
		if(isset($aData['invoice_city'])) {
		     $oField = new InvoiceCity();
		     $mValue = $oField->sanitize($aData['invoice_city']);
		     $oModel->setInvoiceCity($mValue);
		}
		if(isset($aData['invoice_country_id'])) {
		     $oField = new InvoiceCountryId();
		     $mValue = $oField->sanitize($aData['invoice_country_id']);
		     $oModel->setInvoiceCountryId($mValue);
		}
		if(isset($aData['has_delivery_address'])) {
		     $oField = new HasDeliveryAddress();
		     $mValue = $oField->sanitize($aData['has_delivery_address']);
		     $oModel->setHasDeliveryAddress($mValue);
		}
		if(isset($aData['delivery_company_name'])) {
		     $oField = new DeliveryCompanyName();
		     $mValue = $oField->sanitize($aData['delivery_company_name']);
		     $oModel->setDeliveryCompanyName($mValue);
		}
		if(isset($aData['delivery_street'])) {
		     $oField = new DeliveryStreet();
		     $mValue = $oField->sanitize($aData['delivery_street']);
		     $oModel->setDeliveryStreet($mValue);
		}
		if(isset($aData['delivery_number'])) {
		     $oField = new DeliveryNumber();
		     $mValue = $oField->sanitize($aData['delivery_number']);
		     $oModel->setDeliveryNumber($mValue);
		}
		if(isset($aData['delivery_number_add'])) {
		     $oField = new DeliveryNumberAdd();
		     $mValue = $oField->sanitize($aData['delivery_number_add']);
		     $oModel->setDeliveryNumberAdd($mValue);
		}
		if(isset($aData['delivery_postal'])) {
		     $oField = new DeliveryPostal();
		     $mValue = $oField->sanitize($aData['delivery_postal']);
		     $oModel->setDeliveryPostal($mValue);
		}
		if(isset($aData['delivery_city'])) {
		     $oField = new DeliveryCity();
		     $mValue = $oField->sanitize($aData['delivery_city']);
		     $oModel->setDeliveryCity($mValue);
		}
		if(isset($aData['delivery_country_id'])) {
		     $oField = new DeliveryCountryId();
		     $mValue = $oField->sanitize($aData['delivery_country_id']);
		     $oModel->setDeliveryCountryId($mValue);
		}
		if(isset($aData['company_logo_id'])) {
		     $oField = new CompanyLogoId();
		     $mValue = $oField->sanitize($aData['company_logo_id']);
		     $oModel->setCompanyLogoId($mValue);
		}
		if(isset($aData['max_shipping_costs'])) {
		     $oField = new MaxShippingCosts();
		     $mValue = $oField->sanitize($aData['max_shipping_costs']);
		     $oModel->setMaxShippingCosts($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
