<?php
namespace Crud\Company\Field\Base;

use Crud\Company\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'invoice_postal' crud field from the 'own_company' table.
 * This class is auto generated and should not be modified.
 */
abstract class InvoicePostal extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'invoice_postal';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getInvoicePostal';
	protected $sFqModelClassname = '\\\Model\Company\Company';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
