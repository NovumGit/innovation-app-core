<?php
namespace Crud\Company\Field\Base;

use Crud\Company\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'general_country_id' crud field from the 'own_company' table.
 * This class is auto generated and should not be modified.
 */
abstract class GeneralCountryId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'general_country_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getGeneralCountryId';
	protected $sFqModelClassname = '\\\Model\Company\Company';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
