<?php

namespace Crud\Company\Field;

use Crud\Company\Field\Base\ItemDeleted as BaseItemDeleted;

/**
 * Skeleton subclass for representing is_deleted field from the own_company table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ItemDeleted extends BaseItemDeleted
{
}
