<?php
namespace Crud;

use Hurah\Types\Type\Url;

/**
 * Is the crud field configurable when shown in a collection context?
 * Interface IConfigurableFieldCollection
 * @package Crud
 */
interface IConfigurableFieldCollection {


    /**
     * @param int $iCrudViewFieldId
     * @return Url
     */
    function getCollectionConfigUrl(int $iCrudViewFieldId): Url;
    function isConfigurableCollection(): bool;


}
