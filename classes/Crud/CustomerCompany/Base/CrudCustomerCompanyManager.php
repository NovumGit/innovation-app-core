<?php
namespace Crud\CustomerCompany\Base;

use Core\Utils;
use Crud;
use Crud\CustomerCompany\FieldIterator;
use Crud\CustomerCompany\Field\ChamberOfCommerce;
use Crud\CustomerCompany\Field\CompanyName;
use Crud\CustomerCompany\Field\CompanyTypeId;
use Crud\CustomerCompany\Field\CustomerId;
use Crud\CustomerCompany\Field\LegalFormId;
use Crud\CustomerCompany\Field\VatNumber;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerCompanyQuery;
use Model\Crm\Map\CustomerCompanyTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerCompany instead if you need to override or add functionality.
 */
abstract class CrudCustomerCompanyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerCompanyQuery::create();
	}


	public function getTableMap(): CustomerCompanyTableMap
	{
		return new CustomerCompanyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerCompany";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_company toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_company aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'LegalFormId', 'CompanyTypeId', 'CompanyName', 'ChamberOfCommerce', 'VatNumber'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'LegalFormId', 'CompanyTypeId', 'CompanyName', 'ChamberOfCommerce', 'VatNumber'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerCompany
	 */
	public function getModel(array $aData = null): CustomerCompany
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerCompanyQuery = CustomerCompanyQuery::create();
		     $oCustomerCompany = $oCustomerCompanyQuery->findOneById($aData['id']);
		     if (!$oCustomerCompany instanceof CustomerCompany) {
		         throw new LogicException("CustomerCompany should be an instance of CustomerCompany but got something else." . __METHOD__);
		     }
		     $oCustomerCompany = $this->fillVo($aData, $oCustomerCompany);
		}
		else {
		     $oCustomerCompany = new CustomerCompany();
		     if (!empty($aData)) {
		         $oCustomerCompany = $this->fillVo($aData, $oCustomerCompany);
		     }
		}
		return $oCustomerCompany;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerCompany
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerCompany
	{
		$oCustomerCompany = $this->getModel($aData);


		 if(!empty($oCustomerCompany))
		 {
		     $oCustomerCompany = $this->fillVo($aData, $oCustomerCompany);
		     $oCustomerCompany->save();
		 }
		return $oCustomerCompany;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerCompany $oModel
	 * @return CustomerCompany
	 */
	protected function fillVo(array $aData, CustomerCompany $oModel): CustomerCompany
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['legal_form_id'])) {
		     $oField = new LegalFormId();
		     $mValue = $oField->sanitize($aData['legal_form_id']);
		     $oModel->setLegalFormId($mValue);
		}
		if(isset($aData['company_type_id'])) {
		     $oField = new CompanyTypeId();
		     $mValue = $oField->sanitize($aData['company_type_id']);
		     $oModel->setCompanyTypeId($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['chamber_of_commerce'])) {
		     $oField = new ChamberOfCommerce();
		     $mValue = $oField->sanitize($aData['chamber_of_commerce']);
		     $oModel->setChamberOfCommerce($mValue);
		}
		if(isset($aData['vat_number'])) {
		     $oField = new VatNumber();
		     $mValue = $oField->sanitize($aData['vat_number']);
		     $oModel->setVatNumber($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
