<?php
namespace Crud\CustomerCompany\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerCompany\ICollectionField as CustomerCompanyField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerCompany\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerCompanyField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerCompanyField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerCompanyField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerCompanyField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerCompanyField
	{
		return current($this->aFields);
	}
}
