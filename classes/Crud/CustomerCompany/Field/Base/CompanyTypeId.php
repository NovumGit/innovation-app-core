<?php
namespace Crud\CustomerCompany\Field\Base;

use Crud\CustomerCompany\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'company_type_id' crud field from the 'customer_company' table.
 * This class is auto generated and should not be modified.
 */
abstract class CompanyTypeId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'company_type_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCompanyTypeId';
	protected $sFqModelClassname = '\\\Model\Crm\CustomerCompany';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
