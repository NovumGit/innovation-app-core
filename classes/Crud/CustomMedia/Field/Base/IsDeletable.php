<?php
namespace Crud\CustomMedia\Field\Base;

use Crud\CustomMedia\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_deletable' crud field from the 'custom_media' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsDeletable extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_deletable';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsDeletable';
	protected $sFqModelClassname = '\\\Model\Cms\CustomMedia';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
