<?php
namespace Crud\CustomMedia\Field;

use Crud\CustomMedia\Field\Base\ParentId as BaseParentId;

/**
 * Skeleton subclass for representing parent_id field from the custom_media table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class ParentId extends BaseParentId
{
}
