<?php
namespace Crud\CustomMedia;

/**
 * Skeleton subclass for representing a CustomMedia.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class CrudCustomMediaManager extends Base\CrudCustomMediaManager
{
}
