<?php
namespace Crud\CustomMedia\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomMedia\ICollectionField as CustomMediaField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomMedia\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomMediaField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomMediaField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomMediaField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomMediaField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomMediaField
	{
		return current($this->aFields);
	}
}
