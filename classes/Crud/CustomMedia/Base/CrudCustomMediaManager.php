<?php
namespace Crud\CustomMedia\Base;

use Core\Utils;
use Crud;
use Crud\CustomMedia\FieldIterator;
use Crud\CustomMedia\Field\CreatedDate;
use Crud\CustomMedia\Field\IsDeletable;
use Crud\CustomMedia\Field\Name;
use Crud\CustomMedia\Field\NodeTypeId;
use Crud\CustomMedia\Field\ParentId;
use Crud\CustomMedia\Field\SiteId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Cms\CustomMedia;
use Model\Cms\CustomMediaQuery;
use Model\Cms\Map\CustomMediaTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomMedia instead if you need to override or add functionality.
 */
abstract class CrudCustomMediaManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomMediaQuery::create();
	}


	public function getTableMap(): CustomMediaTableMap
	{
		return new CustomMediaTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomMedia";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "custom_media toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "custom_media aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ParentId', 'SiteId', 'NodeTypeId', 'Name', 'IsDeletable', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ParentId', 'SiteId', 'NodeTypeId', 'Name', 'IsDeletable', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomMedia
	 */
	public function getModel(array $aData = null): CustomMedia
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomMediaQuery = CustomMediaQuery::create();
		     $oCustomMedia = $oCustomMediaQuery->findOneById($aData['id']);
		     if (!$oCustomMedia instanceof CustomMedia) {
		         throw new LogicException("CustomMedia should be an instance of CustomMedia but got something else." . __METHOD__);
		     }
		     $oCustomMedia = $this->fillVo($aData, $oCustomMedia);
		}
		else {
		     $oCustomMedia = new CustomMedia();
		     if (!empty($aData)) {
		         $oCustomMedia = $this->fillVo($aData, $oCustomMedia);
		     }
		}
		return $oCustomMedia;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomMedia
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomMedia
	{
		$oCustomMedia = $this->getModel($aData);


		 if(!empty($oCustomMedia))
		 {
		     $oCustomMedia = $this->fillVo($aData, $oCustomMedia);
		     $oCustomMedia->save();
		 }
		return $oCustomMedia;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomMedia $oModel
	 * @return CustomMedia
	 */
	protected function fillVo(array $aData, CustomMedia $oModel): CustomMedia
	{
		if(isset($aData['parent_id'])) {
		     $oField = new ParentId();
		     $mValue = $oField->sanitize($aData['parent_id']);
		     $oModel->setParentId($mValue);
		}
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['node_type_id'])) {
		     $oField = new NodeTypeId();
		     $mValue = $oField->sanitize($aData['node_type_id']);
		     $oModel->setNodeTypeId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['is_deletable'])) {
		     $oField = new IsDeletable();
		     $mValue = $oField->sanitize($aData['is_deletable']);
		     $oModel->setIsDeletable($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
