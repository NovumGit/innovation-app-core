<?php
namespace Crud\CrudEditorBlock\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudEditorBlock\ICollectionField as CrudEditorBlockField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudEditorBlock\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudEditorBlockField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudEditorBlockField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudEditorBlockField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudEditorBlockField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudEditorBlockField
	{
		return current($this->aFields);
	}
}
