<?php
namespace Crud\CrudEditorBlock\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditorBlock\FieldIterator;
use Crud\CrudEditorBlock\Field\CrudEditorId;
use Crud\CrudEditorBlock\Field\Sorting;
use Crud\CrudEditorBlock\Field\Title;
use Crud\CrudEditorBlock\Field\Width;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditorBlock;
use Model\Setting\CrudManager\CrudEditorBlockQuery;
use Model\Setting\CrudManager\Map\CrudEditorBlockTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditorBlock instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorBlockManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorBlockQuery::create();
	}


	public function getTableMap(): CrudEditorBlockTableMap
	{
		return new CrudEditorBlockTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn formulier blokken opgeslagen";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditorBlock";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor_block toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor_block aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorId', 'Sorting', 'Width', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorId', 'Sorting', 'Width', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditorBlock
	 */
	public function getModel(array $aData = null): CrudEditorBlock
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorBlockQuery = CrudEditorBlockQuery::create();
		     $oCrudEditorBlock = $oCrudEditorBlockQuery->findOneById($aData['id']);
		     if (!$oCrudEditorBlock instanceof CrudEditorBlock) {
		         throw new LogicException("CrudEditorBlock should be an instance of CrudEditorBlock but got something else." . __METHOD__);
		     }
		     $oCrudEditorBlock = $this->fillVo($aData, $oCrudEditorBlock);
		}
		else {
		     $oCrudEditorBlock = new CrudEditorBlock();
		     if (!empty($aData)) {
		         $oCrudEditorBlock = $this->fillVo($aData, $oCrudEditorBlock);
		     }
		}
		return $oCrudEditorBlock;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditorBlock
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditorBlock
	{
		$oCrudEditorBlock = $this->getModel($aData);


		 if(!empty($oCrudEditorBlock))
		 {
		     $oCrudEditorBlock = $this->fillVo($aData, $oCrudEditorBlock);
		     $oCrudEditorBlock->save();
		 }
		return $oCrudEditorBlock;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditorBlock $oModel
	 * @return CrudEditorBlock
	 */
	protected function fillVo(array $aData, CrudEditorBlock $oModel): CrudEditorBlock
	{
		if(isset($aData['crud_editor_id'])) {
		     $oField = new CrudEditorId();
		     $mValue = $oField->sanitize($aData['crud_editor_id']);
		     $oModel->setCrudEditorId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['width'])) {
		     $oField = new Width();
		     $mValue = $oField->sanitize($aData['width']);
		     $oModel->setWidth($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
