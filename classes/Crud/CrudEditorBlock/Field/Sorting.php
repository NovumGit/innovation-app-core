<?php
namespace Crud\CrudEditorBlock\Field;

use Crud\CrudEditorBlock\Field\Base\Sorting as BaseSorting;

/**
 * Skeleton subclass for representing sorting field from the crud_editor_block table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Sorting extends BaseSorting
{
}
