<?php
namespace Crud\CrudEditorBlock\Field\Base;

use Crud\CrudEditorBlock\ICollectionField;
use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'sorting' crud field from the 'crud_editor_block' table.
 * This class is auto generated and should not be modified.
 */
abstract class Sorting extends GenericInteger implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sorting';
	protected $sFieldLabel = 'Volgorde';
	protected $sIcon = 'sort';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSorting';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorBlock';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
