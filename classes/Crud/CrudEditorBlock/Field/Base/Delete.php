<?php 
namespace Crud\CrudEditorBlock\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditorBlock;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorBlock)
		{
		     return "//system/crud_editor_block/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorBlock)
		{
		     return "//crud_editor_block?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
