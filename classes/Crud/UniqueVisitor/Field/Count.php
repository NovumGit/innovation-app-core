<?php
namespace Crud\UniqueVisitor\Field;

use Crud\UniqueVisitor\Field\Base\Count as BaseCount;

/**
 * Skeleton subclass for representing count field from the unique_visitor table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class Count extends BaseCount
{
}
