<?php
namespace Crud\UniqueVisitor\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UniqueVisitor\ICollectionField;

/**
 * Base class that represents the 'ip' crud field from the 'unique_visitor' table.
 * This class is auto generated and should not be modified.
 */
abstract class Ip extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'ip';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIp';
	protected $sFqModelClassname = '\\\Model\Stats\UniqueVisitor';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
