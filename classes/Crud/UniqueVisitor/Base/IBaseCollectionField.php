<?php
namespace Crud\UniqueVisitor\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\UniqueVisitor\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
