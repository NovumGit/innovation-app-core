<?php
namespace Crud\UniqueVisitor\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UniqueVisitor\ICollectionField as UniqueVisitorField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UniqueVisitor\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UniqueVisitorField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UniqueVisitorField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UniqueVisitorField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UniqueVisitorField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UniqueVisitorField
	{
		return current($this->aFields);
	}
}
