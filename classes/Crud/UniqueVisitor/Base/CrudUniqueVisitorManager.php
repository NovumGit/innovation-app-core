<?php
namespace Crud\UniqueVisitor\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UniqueVisitor\FieldIterator;
use Crud\UniqueVisitor\Field\Count;
use Crud\UniqueVisitor\Field\Ip;
use Crud\UniqueVisitor\Field\VisitDate;
use Exception\LogicException;
use Model\Stats\Map\UniqueVisitorTableMap;
use Model\Stats\UniqueVisitor;
use Model\Stats\UniqueVisitorQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UniqueVisitor instead if you need to override or add functionality.
 */
abstract class CrudUniqueVisitorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UniqueVisitorQuery::create();
	}


	public function getTableMap(): UniqueVisitorTableMap
	{
		return new UniqueVisitorTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UniqueVisitor";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "unique_visitor toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "unique_visitor aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Ip', 'VisitDate', 'Count'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Ip', 'VisitDate', 'Count'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UniqueVisitor
	 */
	public function getModel(array $aData = null): UniqueVisitor
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUniqueVisitorQuery = UniqueVisitorQuery::create();
		     $oUniqueVisitor = $oUniqueVisitorQuery->findOneById($aData['id']);
		     if (!$oUniqueVisitor instanceof UniqueVisitor) {
		         throw new LogicException("UniqueVisitor should be an instance of UniqueVisitor but got something else." . __METHOD__);
		     }
		     $oUniqueVisitor = $this->fillVo($aData, $oUniqueVisitor);
		}
		else {
		     $oUniqueVisitor = new UniqueVisitor();
		     if (!empty($aData)) {
		         $oUniqueVisitor = $this->fillVo($aData, $oUniqueVisitor);
		     }
		}
		return $oUniqueVisitor;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UniqueVisitor
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UniqueVisitor
	{
		$oUniqueVisitor = $this->getModel($aData);


		 if(!empty($oUniqueVisitor))
		 {
		     $oUniqueVisitor = $this->fillVo($aData, $oUniqueVisitor);
		     $oUniqueVisitor->save();
		 }
		return $oUniqueVisitor;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UniqueVisitor $oModel
	 * @return UniqueVisitor
	 */
	protected function fillVo(array $aData, UniqueVisitor $oModel): UniqueVisitor
	{
		if(isset($aData['ip'])) {
		     $oField = new Ip();
		     $mValue = $oField->sanitize($aData['ip']);
		     $oModel->setIp($mValue);
		}
		if(isset($aData['visit_date'])) {
		     $oField = new VisitDate();
		     $mValue = $oField->sanitize($aData['visit_date']);
		     $oModel->setVisitDate($mValue);
		}
		if(isset($aData['count'])) {
		     $oField = new Count();
		     $mValue = $oField->sanitize($aData['count']);
		     $oModel->setCount($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
