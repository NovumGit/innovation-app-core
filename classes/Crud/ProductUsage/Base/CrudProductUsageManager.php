<?php
namespace Crud\ProductUsage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductUsage\FieldIterator;
use Crud\ProductUsage\Field\ProductId;
use Crud\ProductUsage\Field\UsageId;
use Exception\LogicException;
use Model\Map\ProductUsageTableMap;
use Model\ProductUsage;
use Model\ProductUsageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductUsage instead if you need to override or add functionality.
 */
abstract class CrudProductUsageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductUsageQuery::create();
	}


	public function getTableMap(): ProductUsageTableMap
	{
		return new ProductUsageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductUsage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_usage toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_usage aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'UsageId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'UsageId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductUsage
	 */
	public function getModel(array $aData = null): ProductUsage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductUsageQuery = ProductUsageQuery::create();
		     $oProductUsage = $oProductUsageQuery->findOneById($aData['id']);
		     if (!$oProductUsage instanceof ProductUsage) {
		         throw new LogicException("ProductUsage should be an instance of ProductUsage but got something else." . __METHOD__);
		     }
		     $oProductUsage = $this->fillVo($aData, $oProductUsage);
		}
		else {
		     $oProductUsage = new ProductUsage();
		     if (!empty($aData)) {
		         $oProductUsage = $this->fillVo($aData, $oProductUsage);
		     }
		}
		return $oProductUsage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductUsage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductUsage
	{
		$oProductUsage = $this->getModel($aData);


		 if(!empty($oProductUsage))
		 {
		     $oProductUsage = $this->fillVo($aData, $oProductUsage);
		     $oProductUsage->save();
		 }
		return $oProductUsage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductUsage $oModel
	 * @return ProductUsage
	 */
	protected function fillVo(array $aData, ProductUsage $oModel): ProductUsage
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['usage_id'])) {
		     $oField = new UsageId();
		     $mValue = $oField->sanitize($aData['usage_id']);
		     $oModel->setUsageId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
