<?php
namespace Crud\ProductUsage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductUsage\ICollectionField as ProductUsageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductUsage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductUsageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductUsageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductUsageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductUsageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductUsageField
	{
		return current($this->aFields);
	}
}
