<?php

namespace Crud\AppType\Field;

use Crud\AppType\Field\Base\Name as BaseName;

/**
 * Skeleton subclass for representing name field from the app_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Name extends BaseName
{
}
