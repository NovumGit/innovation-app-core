<?php 
namespace Crud\AppType\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\AppType;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof AppType)
		{
		     return "//system/app_type/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof AppType)
		{
		     return "//app_type?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
