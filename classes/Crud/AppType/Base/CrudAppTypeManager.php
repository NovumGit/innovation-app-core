<?php
namespace Crud\AppType\Base;

use Core\Utils;
use Crud;
use Crud\AppType\FieldIterator;
use Crud\AppType\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\AppType;
use Model\System\AppTypeQuery;
use Model\System\Map\AppTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AppType instead if you need to override or add functionality.
 */
abstract class CrudAppTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return AppTypeQuery::create();
	}


	public function getTableMap(): AppTypeTableMap
	{
		return new AppTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Omschrijft soorten apps (web, mobile etc).";
	}


	public function getEntityTitle(): string
	{
		return "AppType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "app_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "app_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return AppType
	 */
	public function getModel(array $aData = null): AppType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oAppTypeQuery = AppTypeQuery::create();
		     $oAppType = $oAppTypeQuery->findOneById($aData['id']);
		     if (!$oAppType instanceof AppType) {
		         throw new LogicException("AppType should be an instance of AppType but got something else." . __METHOD__);
		     }
		     $oAppType = $this->fillVo($aData, $oAppType);
		}
		else {
		     $oAppType = new AppType();
		     if (!empty($aData)) {
		         $oAppType = $this->fillVo($aData, $oAppType);
		     }
		}
		return $oAppType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return AppType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): AppType
	{
		$oAppType = $this->getModel($aData);


		 if(!empty($oAppType))
		 {
		     $oAppType = $this->fillVo($aData, $oAppType);
		     $oAppType->save();
		 }
		return $oAppType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param AppType $oModel
	 * @return AppType
	 */
	protected function fillVo(array $aData, AppType $oModel): AppType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
