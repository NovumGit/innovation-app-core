<?php
namespace Crud;

interface IEventField {

    function getIcon();
    function isEventField();
}
