<?php
namespace Crud\SiteFAQ\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFAQ\FieldIterator;
use Crud\SiteFAQ\Field\Answer;
use Crud\SiteFAQ\Field\LanguageId;
use Crud\SiteFAQ\Field\Question;
use Crud\SiteFAQ\Field\SiteFaqCategoryId;
use Crud\SiteFAQ\Field\SiteId;
use Exception\LogicException;
use Model\Cms\Map\SiteFAQTableMap;
use Model\Cms\SiteFAQ;
use Model\Cms\SiteFAQQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFAQ instead if you need to override or add functionality.
 */
abstract class CrudSiteFAQManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFAQQuery::create();
	}


	public function getTableMap(): SiteFAQTableMap
	{
		return new SiteFAQTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFAQ";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_faq toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_faq aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'SiteFaqCategoryId', 'LanguageId', 'Question', 'Answer'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'SiteFaqCategoryId', 'LanguageId', 'Question', 'Answer'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFAQ
	 */
	public function getModel(array $aData = null): SiteFAQ
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFAQQuery = SiteFAQQuery::create();
		     $oSiteFAQ = $oSiteFAQQuery->findOneById($aData['id']);
		     if (!$oSiteFAQ instanceof SiteFAQ) {
		         throw new LogicException("SiteFAQ should be an instance of SiteFAQ but got something else." . __METHOD__);
		     }
		     $oSiteFAQ = $this->fillVo($aData, $oSiteFAQ);
		}
		else {
		     $oSiteFAQ = new SiteFAQ();
		     if (!empty($aData)) {
		         $oSiteFAQ = $this->fillVo($aData, $oSiteFAQ);
		     }
		}
		return $oSiteFAQ;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFAQ
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFAQ
	{
		$oSiteFAQ = $this->getModel($aData);


		 if(!empty($oSiteFAQ))
		 {
		     $oSiteFAQ = $this->fillVo($aData, $oSiteFAQ);
		     $oSiteFAQ->save();
		 }
		return $oSiteFAQ;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFAQ $oModel
	 * @return SiteFAQ
	 */
	protected function fillVo(array $aData, SiteFAQ $oModel): SiteFAQ
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['site_faq_category_id'])) {
		     $oField = new SiteFaqCategoryId();
		     $mValue = $oField->sanitize($aData['site_faq_category_id']);
		     $oModel->setSiteFaqCategoryId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['question'])) {
		     $oField = new Question();
		     $mValue = $oField->sanitize($aData['question']);
		     $oModel->setQuestion($mValue);
		}
		if(isset($aData['answer'])) {
		     $oField = new Answer();
		     $mValue = $oField->sanitize($aData['answer']);
		     $oModel->setAnswer($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
