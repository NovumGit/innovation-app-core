<?php
namespace Crud\SiteFAQ\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteFAQ\ICollectionField as SiteFAQField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteFAQ\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteFAQField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteFAQField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteFAQField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteFAQField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteFAQField
	{
		return current($this->aFields);
	}
}
