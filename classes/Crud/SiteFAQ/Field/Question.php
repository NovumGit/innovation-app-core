<?php
namespace Crud\SiteFAQ\Field;

use Crud\SiteFAQ\Field\Base\Question as BaseQuestion;

/**
 * Skeleton subclass for representing question field from the site_faq table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Question extends BaseQuestion
{
}
