<?php
namespace Crud\SiteFAQ\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteFAQ\ICollectionField;

/**
 * Base class that represents the 'site_faq_category_id' crud field from the 'site_faq' table.
 * This class is auto generated and should not be modified.
 */
abstract class SiteFaqCategoryId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'site_faq_category_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSiteFaqCategoryId';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFAQ';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
