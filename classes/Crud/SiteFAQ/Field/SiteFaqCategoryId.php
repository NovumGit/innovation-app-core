<?php
namespace Crud\SiteFAQ\Field;

use Crud\SiteFAQ\Field\Base\SiteFaqCategoryId as BaseSiteFaqCategoryId;

/**
 * Skeleton subclass for representing site_faq_category_id field from the site_faq table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class SiteFaqCategoryId extends BaseSiteFaqCategoryId
{
}
