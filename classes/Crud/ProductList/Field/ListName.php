<?php
namespace Crud\ProductList\Field;

use Crud\ProductList\Field\Base\ListName as BaseListName;

/**
 * Skeleton subclass for representing list_name field from the product_list table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ListName extends BaseListName
{
}
