<?php
namespace Crud\ProductList\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductList\FieldIterator;
use Crud\ProductList\Field\ListName;
use Crud\ProductList\Field\ProductId;
use Crud\ProductList\Field\Sorting;
use Exception\LogicException;
use Model\Map\ProductListTableMap;
use Model\ProductList;
use Model\ProductListQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductList instead if you need to override or add functionality.
 */
abstract class CrudProductListManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductListQuery::create();
	}


	public function getTableMap(): ProductListTableMap
	{
		return new ProductListTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductList";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_list toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_list aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ListName', 'ProductId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ListName', 'ProductId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductList
	 */
	public function getModel(array $aData = null): ProductList
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductListQuery = ProductListQuery::create();
		     $oProductList = $oProductListQuery->findOneById($aData['id']);
		     if (!$oProductList instanceof ProductList) {
		         throw new LogicException("ProductList should be an instance of ProductList but got something else." . __METHOD__);
		     }
		     $oProductList = $this->fillVo($aData, $oProductList);
		}
		else {
		     $oProductList = new ProductList();
		     if (!empty($aData)) {
		         $oProductList = $this->fillVo($aData, $oProductList);
		     }
		}
		return $oProductList;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductList
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductList
	{
		$oProductList = $this->getModel($aData);


		 if(!empty($oProductList))
		 {
		     $oProductList = $this->fillVo($aData, $oProductList);
		     $oProductList->save();
		 }
		return $oProductList;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductList $oModel
	 * @return ProductList
	 */
	protected function fillVo(array $aData, ProductList $oModel): ProductList
	{
		if(isset($aData['list_name'])) {
		     $oField = new ListName();
		     $mValue = $oField->sanitize($aData['list_name']);
		     $oModel->setListName($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
