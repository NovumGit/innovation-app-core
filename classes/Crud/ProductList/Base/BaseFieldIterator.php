<?php
namespace Crud\ProductList\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductList\ICollectionField as ProductListField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductList\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductListField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductListField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductListField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductListField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductListField
	{
		return current($this->aFields);
	}
}
