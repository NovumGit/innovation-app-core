<?php
namespace Crud\CrudViewButton\Field;

use Crud\CrudViewButton\Field\Base\ColorBeforeClick as BaseColorBeforeClick;

/**
 * Skeleton subclass for representing color_before_click field from the crud_view_button table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ColorBeforeClick extends BaseColorBeforeClick
{
}
