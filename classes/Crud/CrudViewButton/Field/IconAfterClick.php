<?php
namespace Crud\CrudViewButton\Field;

use Crud\CrudViewButton\Field\Base\IconAfterClick as BaseIconAfterClick;

/**
 * Skeleton subclass for representing icon_after_click field from the crud_view_button table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class IconAfterClick extends BaseIconAfterClick
{
}
