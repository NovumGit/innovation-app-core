<?php
namespace Crud\CrudViewButton\Field\Base;

use Crud\CrudViewButton\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'icon_before_click' crud field from the 'crud_view_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class IconBeforeClick extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'icon_before_click';
	protected $sFieldLabel = 'Icoon voor klik';
	protected $sIcon = 'picture-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIconBeforeClick';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewButton';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
