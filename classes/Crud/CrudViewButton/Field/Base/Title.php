<?php
namespace Crud\CrudViewButton\Field\Base;

use Crud\CrudViewButton\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'title' crud field from the 'crud_view_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class Title extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'title';
	protected $sFieldLabel = 'Titel';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTitle';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewButton';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
