<?php
namespace Crud\CrudViewButton\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudViewButton\ICollectionField as CrudViewButtonField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudViewButton\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudViewButtonField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudViewButtonField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudViewButtonField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudViewButtonField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudViewButtonField
	{
		return current($this->aFields);
	}
}
