<?php
namespace Crud\CrudViewButton\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\CrudViewButton\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
