<?php
namespace Crud\CrudViewButton\Base;

use Core\Utils;
use Crud;
use Crud\CrudViewButton\FieldIterator;
use Crud\CrudViewButton\Field\ColorAfterClick;
use Crud\CrudViewButton\Field\ColorBeforeClick;
use Crud\CrudViewButton\Field\CrudViewId;
use Crud\CrudViewButton\Field\IconAfterClick;
use Crud\CrudViewButton\Field\IconBeforeClick;
use Crud\CrudViewButton\Field\Sorting;
use Crud\CrudViewButton\Field\Title;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonQuery;
use Model\Setting\CrudManager\Map\CrudViewButtonTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudViewButton instead if you need to override or add functionality.
 */
abstract class CrudCrudViewButtonManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewButtonQuery::create();
	}


	public function getTableMap(): CrudViewButtonTableMap
	{
		return new CrudViewButtonTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CrudViewButton";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view_button toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view_button aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudViewButton
	 */
	public function getModel(array $aData = null): CrudViewButton
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewButtonQuery = CrudViewButtonQuery::create();
		     $oCrudViewButton = $oCrudViewButtonQuery->findOneById($aData['id']);
		     if (!$oCrudViewButton instanceof CrudViewButton) {
		         throw new LogicException("CrudViewButton should be an instance of CrudViewButton but got something else." . __METHOD__);
		     }
		     $oCrudViewButton = $this->fillVo($aData, $oCrudViewButton);
		}
		else {
		     $oCrudViewButton = new CrudViewButton();
		     if (!empty($aData)) {
		         $oCrudViewButton = $this->fillVo($aData, $oCrudViewButton);
		     }
		}
		return $oCrudViewButton;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudViewButton
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudViewButton
	{
		$oCrudViewButton = $this->getModel($aData);


		 if(!empty($oCrudViewButton))
		 {
		     $oCrudViewButton = $this->fillVo($aData, $oCrudViewButton);
		     $oCrudViewButton->save();
		 }
		return $oCrudViewButton;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudViewButton $oModel
	 * @return CrudViewButton
	 */
	protected function fillVo(array $aData, CrudViewButton $oModel): CrudViewButton
	{
		if(isset($aData['crud_view_id'])) {
		     $oField = new CrudViewId();
		     $mValue = $oField->sanitize($aData['crud_view_id']);
		     $oModel->setCrudViewId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['icon_before_click'])) {
		     $oField = new IconBeforeClick();
		     $mValue = $oField->sanitize($aData['icon_before_click']);
		     $oModel->setIconBeforeClick($mValue);
		}
		if(isset($aData['icon_after_click'])) {
		     $oField = new IconAfterClick();
		     $mValue = $oField->sanitize($aData['icon_after_click']);
		     $oModel->setIconAfterClick($mValue);
		}
		if(isset($aData['color_before_click'])) {
		     $oField = new ColorBeforeClick();
		     $mValue = $oField->sanitize($aData['color_before_click']);
		     $oModel->setColorBeforeClick($mValue);
		}
		if(isset($aData['color_after_click'])) {
		     $oField = new ColorAfterClick();
		     $mValue = $oField->sanitize($aData['color_after_click']);
		     $oModel->setColorAfterClick($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
