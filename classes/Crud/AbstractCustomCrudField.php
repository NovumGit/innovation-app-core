<?php
namespace Crud;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class AbstractCustomCrudField implements ICustomCrudField {
    abstract function getKeyField();

    abstract function getGetter();

    abstract function getSetter();

    abstract function getFindOneBy();

    abstract function getModel();

    public function store(ActiveRecordInterface $oObject, array $aData) {
        // TODO: Implement save() method.
    }
}
