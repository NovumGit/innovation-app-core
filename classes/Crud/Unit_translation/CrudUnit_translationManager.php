<?php
namespace Crud\Unit_translation;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\ProductUnitTranslation;
use Model\Setting\MasterTable\ProductUnitTranslationQuery;

class CrudUnit_translationManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/unit/edit';
    }
    function getEntityTitle():string
    {
        return 'eenheid';
    }
    function getNewFormTitle():string
    {
        return 'Eenheid toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Eenheid wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return ['Name', 'NamePlural', 'Delete', 'Edit'];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return ['Name', 'NamePlural'];
    }
    function getModel(array $aData = null)
    {

        $oProductUnitTranslation = null;
        if(isset($aData['language_id']) && isset($aData['unit_id']))
        {
            $oProductUnitTranslation = ProductUnitTranslationQuery::create()->filterByLanguageId($aData['language_id'])->filterByUnitId($aData['unit_id'])->findOne();
        }
        else if(isset($aData['id']) && $aData['id'])
        {
            $oProductUnitTranslation = ProductUnitTranslationQuery::create()->findOneById($aData['id']);
        }

        if($oProductUnitTranslation instanceof ProductUnitTranslation &&  isset($aData['language_id']) && isset($aData['unit_id']))
        {
            $oProductUnitTranslationQuery = ProductUnitTranslationQuery::create();
            $oProductUnitTranslationQuery->filterByLanguageId($aData['language_id']);
            $oProductUnitTranslationQuery->filterByUnitId($aData['unit_id']);
            $oProductUnitTranslation = $oProductUnitTranslationQuery->findOne();
        }

        if(!$oProductUnitTranslation instanceof ProductUnitTranslation)
        {
            $oProductUnitTranslation = new ProductUnitTranslation();
        }


        if(!empty($aData))
        {
            $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
        }

        return $oProductUnitTranslation;
    }
    function store(array $aData = null)
    {
        $oProductUnitTranslation = $this->getModel($aData);
        $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
        if(!empty($oProductUnitTranslation))
        {
            $oProductUnitTranslation->save();
        }
        return $oProductUnitTranslation;
    }

    private function fillVo($aData, ProductUnitTranslation $oProductUnitTranslation)
    {
        if(isset($aData['name'])){$oProductUnitTranslation->setName($aData['name']);}
        if(isset($aData['name_plural'])){$oProductUnitTranslation->setNamePlural($aData['name_plural']);}


        if(isset($aData['language_id'])){$oProductUnitTranslation->setLanguageId($aData['language_id']);}
        if(isset($aData['unit_id'])){$oProductUnitTranslation->setUnitId($aData['unit_id']);}

        return $oProductUnitTranslation;
    }

}
