<?php
namespace Crud\Unit_translation\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\ProductUnitTranslation as ModelObject;

class NamePlural extends Field{

    protected $sFieldLabel = 'Meervoud';
    protected $sFieldName = 'Meervoud';
    protected $sPlaceHolder = 'Meervoud';
    protected $sIcon = 'tag';

    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        /*
        $oValidator = Validator::alpha();
        if(empty($aPostedData['id']))
        {
            $oProductUnit = ProductUnitQuery::create()->findOneByName($aPostedData['name_plural']);

            if($oProductUnit instanceof ProductUnit)
            {
                $mResponse[] = 'De meervoudige versie van de naam komt al voor in de database.';
            }
        }
        $sNamePlural = trim($aPostedData['name_plural']);
        if(empty($sNamePlural))
        {
            $mResponse[] = 'De meervoudig versie van de naam mag niet leeg zijn.';
        }
        else if(!$oValidator->length(2, 120)->validate($aPostedData['name_plural']))
        {
            $mResponse[] = 'De meervoudige versie van de eenheid naam moet minimaal 2 en mag maximaal 120 tekens lang zijn.';
        }
        */
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Eenheid naam meervoud', 'name_plural');
    }
    function getOverviewValue($oProductUnit)
    {
        if(!$oProductUnit instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  model\Setting\MasterTable\ProductUnit in ".__METHOD__);
        }
        return '<td class="">'.$oProductUnit->getNamePlural().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }

}