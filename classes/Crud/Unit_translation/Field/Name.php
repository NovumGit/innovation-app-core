<?php
namespace Crud\Unit_translation\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\ProductUnitTranslation as ModelObject;


class Name extends Field{

    protected $sFieldLabel = 'Eenheid';
    protected $sFieldName = 'name';
    protected $sPlaceHolder = 'Eenheid naam';
    protected $sIcon = 'tag';

    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function hasValidations() { return true; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Eenheid naam', 'name');
    }
    function getOverviewValue($oProductUnitTranslation)
    {
        if(!$oProductUnitTranslation instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oProductUnitTranslation->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }

}