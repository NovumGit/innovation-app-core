<?php
namespace Crud;

use Core\Config;
use Hurah\Types\Type\Icon;
use Core\TemplateFactory;
use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\StringType;
use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use DateTime;
use Ui\RenderEditConfig;

abstract class Field extends CrudElement implements IField {

    protected $sFieldLabel;
	private $sCachedTranslatedTitle = null;
    private $sEditHtmlStyle = 'Style1';
    private $sFieldArrayName = 'data';
    private $oRenderEditConfig = null;
    protected $sFieldName;

    abstract function getFieldTitle();
    abstract function getEditHtml($mData, $bReadonly);
    abstract function getOverviewHeader();
    abstract function getOverviewValue($mData);
    abstract function validate($aPostedData);
    abstract function hasValidations();

    function getIcon()
    {
        if(property_exists($this, 'sIcon'))
        {
            return new Icon($this->sIcon);
        }

    }
    /**
     * This method is supposed to be overwritten by child classes
     * @param $value
     * @return mixed
     */
    function sanitize($value)
    {
        return $value;
    }
    function toArray():array
    {
        $aOut = [
            'name' => $this->sFieldName,
            'label' => $this->getFieldLabel(),
            'data_type' => method_exists($this, 'getDataType') ? $this->getDataType() : null,
            'primitive' => $this->getPrimitive()->getLabel(),
            'has_validations' => $this->hasValidations(),
            'is_filterable' => $this->isFilterable(),
            'is_editable' => $this->isEditorField(),
            'is_unique_key' => $this->isUniqueKey(),
            'interfaces' => class_implements($this),
            'field' => get_class($this)
        ];

        if($this instanceof GenericLookup)
        {
            $aOut['lookups'] = $this->getLookups();
        }
        return $aOut;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function isUniqueKey():bool
    {
        return false;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    function getFqn()
    {
        $oReflector = new \ReflectionClass($this);
        return $oReflector->getName();
    }
    /**
     * @return string []
     */
    function getPrimitive():InterfaceOasPrimitive
    {
        return new StringType();
    }

    /**
     * Should always be yes unless the field contents is immutable.
     * @return bool
     */
    function isMutable()
    {
        return true;
    }

    function isFilterable()
    {
        return ($this instanceof IFilterableField);
    }
    function setRenderEditConfig(RenderEditConfig $oRenderEditConfig)
    {
        $this->oRenderEditConfig = $oRenderEditConfig;
    }
    function getRenderEditConfig():RenderEditConfig
    {
        if($this->oRenderEditConfig instanceof RenderEditConfig)
        {
            return $this->oRenderEditConfig;
        }
        return new RenderEditConfig();
    }
    function setFieldArrayName($sName)
    {
        $this->sFieldArrayName = $sName;
    }
    private function getFieldArrayName()
    {
        return $this->sFieldArrayName;
    }
    function isOverviewField(): bool
    {
        return true;
    }
    function isFormField(): bool
    {
        if(!$this->isEditorField())
        {
            return false;
        }
        if(!$this->isEditorField())
        {
            return false;
        }
        if($this instanceof IEventField)
        {
            return false;
        }
        if(in_array($this->getFieldName(), ['Edit', 'Delete']))
        {
            return false;
        }
        return true;


    }

    function isEditorField(): bool
    {
        return true;
    }
    function setEditHtmlStyle($sEditHtmlStyle)
    {
        $this->sEditHtmlStyle = $sEditHtmlStyle;
    }
    function getEditHtmlStyle()
    {
        return $this->sEditHtmlStyle;
    }

    function getFieldLabel()
    {
    	if($this->sCachedTranslatedTitle == null)
	    {
		    $this->sCachedTranslatedTitle = $this->getTranslatedTitle();
	    }
        return $this->sCachedTranslatedTitle;
    }


    function getSortProps($sFieldDb){
        $sNewSort = 'asc';
        if(isset($_GET['sort']) && $_GET['sort'] == $sFieldDb){
            if(isset($_GET['dir']) && $_GET['dir'] =='asc'){
                $sNewSort = 'desc';
                $iconClass = 'footable-sorted';
            }else{
                $iconClass = 'footable-sorted-desc';
            }
        }
        else
        {
            $iconClass = 'footable-sortable';
        }

        return ['icon_class' => $iconClass, 'new_sort' =>$sNewSort];
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param array $aAddVars
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function sortableHeaderField($sFieldLabel, $sFieldDb, $aAddVars = []):string
    {
        if(isset($aAddVars['dir']))
        {
            unset($aAddVars['dir']);
        }
        if(isset($aAddVars['sort']))
        {
            unset($aAddVars['sort']);
        }
        if(isset($_GET['tab']))
        {
            $aAddVars['tab'] = $_GET['tab'];
        }

        $aProps = $this->getSortProps($sFieldDb);

        $aAddVars['sort'] = $sFieldDb;
        $aAddVars['dir'] = $aProps['new_sort'];

        $sSortUrl = Utils::getRequestUri(true, $aAddVars);

        $aData = [
            'sort_url' => $sSortUrl,
            'aprops_icon' => $aProps['icon_class'],
            'field_label' => $sFieldLabel
        ];

        if(isset($aAddVars['th_attributes']))
        {
            $aData['th_attributes'] = $aAddVars['th_attributes'];
        }

        return TemplateFactory::create()->render('/Field/sortableHeaderField.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sTdClass
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function nonSortableHeaderField($sFieldLabel, $sTdClass):string
    {
        $aData = [
            'field_label' => $sFieldLabel,
            'td_class' => $sTdClass
        ];

        return TemplateFactory::create()->render('/Field/nonSortableHeaderField.twig', $aData);
    }
    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param DateTime|string|null $mFieldValue
     * @param bool $bReadOnly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editDatePicker($sFieldLabel, $sFieldDb, $mFieldValue = null, $bReadOnly = false):string
    {
        $sVisibleDate = '';
        if($mFieldValue instanceof DateTime)
        {
            $sVisibleDate = $mFieldValue ? $mFieldValue->format(Config::getDateFormat()) : null;
        }
        else if (is_string($mFieldValue))
        {
            $sVisibleDate = $mFieldValue;
        }

        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'field_id' => 'fld_' . $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'is_unique' => $this->isUniqueKey(),
            'field_label' => $sFieldLabel,
            'visible_date' => $sVisibleDate,
            'readonly' => $bReadOnly,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editDatePicker.twig', $aData);
    }

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param DateTime|string|null $mFieldValue
     * @param bool $bReadOnly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editDateTimePicker($sFieldLabel, $sFieldDb, $mFieldValue = null, $bReadOnly = false):string
    {
        $sVisibleDate = '';
        if($mFieldValue instanceof DateTime)
        {
            $sVisibleDate = $mFieldValue ? $mFieldValue->format(Config::getDateTimeFormat()) : null;
        }
        else if (is_string($mFieldValue))
        {
            $sVisibleDate = $mFieldValue;
        }


        $aData = [
            'read_only' => $bReadOnly,
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'field_id' => 'fld_' . $sFieldDb,
            'field_label' => $sFieldLabel,
            'visible_date' => $sVisibleDate,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editDateTimePicker.twig', $aData);
    }

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param string $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @param array $aParameters
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editTextField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, $bReadonly = false, $aParameters = []):string
    {
        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'icon' => $sIcon,
            'placeholder' => $sPlaceholder,
            'field_value' => $sFieldValue,
            'read_only' => $bReadonly,
            'type' => 'text',
            'parameters' => $aParameters,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'tooltip' => $this->getRenderEditConfig()->getToolTip(),
        ];
        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editTextField.twig', $aData);
    }


    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param string $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @param array $aParameters
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editPasswordField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, $bReadonly = false, $aParameters = []):string
    {
        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'icon' => $sIcon,
            'placeholder' => $sPlaceholder,
            'field_value' => $sFieldValue,
            'read_only' => $bReadonly,
            'parameters' => $aParameters,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'type' => 'password',
            'tooltip' => $this->getRenderEditConfig()->getToolTip(),
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editTextField.twig', $aData);
    }

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param string $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editColorField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, $bReadonly = false):string
    {
        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'icon' => $sIcon,
            'placeholder' => $sPlaceholder,
            'field_value' => $sFieldValue,
            'read_only' => $bReadonly,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editColorField.twig', $aData);
    }


    /**
     * @param string $sFieldLabel
     * @param string $sFieldTxt
     * @param string $sFieldUrl
     * @param string $sLinkTitle
     * @param array $aExtraArguments
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editLinkField($sFieldLabel, $sFieldTxt, $sFieldUrl, $sLinkTitle, $aExtraArguments = null)
    {
        $aData = [
            'field_label' => $sFieldLabel,
            'field_txt' => $sFieldTxt,
            'field_url' => $sFieldUrl,
            'link_title' => $sLinkTitle,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
        ];
        $aData['target_attr'] = '';


        if(isset($aExtraArguments['icon']))
        {
            $aData['icon'] = $aExtraArguments['icon'];
        }

        if(isset($aExtraArguments['target']))
        {
            $aData['target_attr'] = 'target="'.$aExtraArguments['target'].'" ';
        }

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editLinkField.twig', $aData);
    }
    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param boolean $bReadonly
     * @param string $sPlaceholder
     * @param array $aArguments
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editTextArea($sFieldLabel, $sFieldDb, $sFieldValue, bool $bReadonly, array $aArguments = null, $sPlaceholder = null):string
    {
        $aData = [
        	'placeholder' => $sPlaceholder,
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'field_value' => $sFieldValue,
            'read_only' => $bReadonly
        ];

        if(isset($aArguments['height']))
        {
            $aData['height'] = $aArguments['height'];
        }
        else
        {
            $aData['height'] = 100;
        }
        if(isset($aArguments['width']))
        {
            $aData['width'] = 'width:'.$aArguments['width'].'px';
        }
        else
        {
            $aData['width'] = '';
        }

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editTextArea.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editRichTextField($sFieldLabel, $sFieldDb, $sFieldValue, $bReadonly):string
    {
        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'field_value' => $sFieldValue,
            'read_only' => $bReadonly
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editRichTextField.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editFileField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, bool $bReadonly = false)
    {

        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_label' => $sFieldLabel,
            'has_validations' => $this->hasValidations(),
            'field_db' => $sFieldDb,
            'field_value' => $sFieldValue,
            'placeholder' => $sPlaceholder,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'icon' => $sIcon,
            'read_only' => $bReadonly
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editFileField.twig', $aData);
    }

    /**
     * @param ConfigEditPreviewImage $oConfigEditPreviewImage
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editImagePreview(ConfigEditPreviewImage $oConfigEditPreviewImage)
    {
        $aData = [
            'title' => $oConfigEditPreviewImage->getTitle(),
            'image_url_or_path' => $oConfigEditPreviewImage->getImageUrlOrPath(),
            'width' => $oConfigEditPreviewImage->getWidth(),
            'height' => $oConfigEditPreviewImage->getHeight(),
            'alt' => $oConfigEditPreviewImage->getAlt(),
            'is_deletable' => $oConfigEditPreviewImage->getHasDeleteLink(),
            'delete_link' =>  $oConfigEditPreviewImage->getDeleteUrl()
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editImagePreview.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $bReadonly
     * @param null $sIcon
     * @param array $aParameters
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editBooleanField($sFieldLabel, $sFieldDb, $sFieldValue, $bReadonly, $sIcon = null, $aParameters = []):string
    {
        if(!isset($aParameters['label_w']))
        {
            $aParameters['label_w'] = $this->getRenderEditConfig()->getLabelWidth();
        }
        if(!isset($aParameters['field_w']))
        {
            $aParameters['field_w'] = $this->getRenderEditConfig()->getFieldWidth();
        }

        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_label' => $sFieldLabel,
            'has_validations' => $this->hasValidations(),
            'field_db' => $sFieldDb,
            'checked' => $sFieldValue ? 'checked="checked"' : '',
            'read_only' => $bReadonly,
            'icon' => $sIcon,
            'parameters' => $aParameters
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editBooleanField.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $aOptions
     * @param $bReadonly
     * @param null $sIcon
     * @param null $sPlaceHolder
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editLookupField($sFieldLabel, $sFieldDb, $sFieldValue, $aOptions, $bReadonly, $sIcon = null, $sPlaceHolder = null)
    {
        if(!empty($aOptions) && $sFieldValue)
        {
            foreach($aOptions as &$oOption)
            {
                if($oOption['id'] == $sFieldValue)
                {
                    $oOption['selected'] = 'selected="selected"';
                }
            }
        }
        $sValue = null;
        foreach ($aOptions as $aOption)
        {
            if(isset($aOption['selected']))
            {
                $sValue = $aOption['id'];
            }
        }

        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'field_value' => $sFieldValue,
            'options' => $aOptions,
            'value' => $sValue,
            'icon' => $sIcon,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'placeholder' => $sPlaceHolder,
            'single_suggest_enabled' => $this->getRenderEditConfig()->getSingleGoogleSuggestEnabled(),
            'read_only' => $bReadonly
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editLookupField.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $aOptions
     * @param $bReadonly
     * @param null $sIcon
     * @param null $sPlaceHolder
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editHiddenField($sFieldDb, $sFieldValue)
    {
        if(!empty($aOptions) && $sFieldValue)
        {
            foreach($aOptions as &$oOption)
            {
                if($oOption['id'] == $sFieldValue)
                {
                    $oOption['selected'] = 'selected="selected"';
                }
            }
        }

        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'field_value' => $sFieldValue
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editHiddenField.twig', $aData);
    }

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $aOptions
     * @param $bReadonly
     * @param null $sIcon
     * @param null $sPlaceHolder
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editCurrencyField($sFieldLabel, $sFieldDb, $sFieldValue, $aOptions, $bReadonly, $sIcon = null, $sPlaceHolder = null)
    {
        $aData = [
            'array_name' => $this->getFieldArrayName(),
            'field_db' => $sFieldDb,
            'has_validations' => $this->hasValidations(),
            'field_label' => $sFieldLabel,
            'field_value' => $sFieldValue,
            'options' => $aOptions,
            'icon' => $sIcon,
            'label_width' => $this->getRenderEditConfig()->getLabelWidth(),
            'field_width' => $this->getRenderEditConfig()->getFieldWidth(),
            'placeholder' => $sPlaceHolder,
            'read_only' => $bReadonly
        ];

        return TemplateFactory::create()->render('/Field/'.$this->getRenderEditConfig()->getLayout().'/editCurrencyField.twig', $aData);
    }

    public function defaultValue()
    {
        return null;
    }
}
