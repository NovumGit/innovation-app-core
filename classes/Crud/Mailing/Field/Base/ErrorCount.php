<?php
namespace Crud\Mailing\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Mailing\ICollectionField;

/**
 * Base class that represents the 'error_count' crud field from the 'mailing' table.
 * This class is auto generated and should not be modified.
 */
abstract class ErrorCount extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'error_count';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getErrorCount';
	protected $sFqModelClassname = '\\\Model\Marketing\Mailing';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
