<?php

namespace Crud\Mailing\Field;

use Crud\Mailing\Field\Base\MailingTemplateId as BaseMailingTemplateId;

/**
 * Skeleton subclass for representing mailing_template_id field from the mailing table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class MailingTemplateId extends BaseMailingTemplateId
{
}
