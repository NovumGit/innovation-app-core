<?php
namespace Crud\Mailing\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Mailing\FieldIterator;
use Crud\Mailing\Field\Contents;
use Crud\Mailing\Field\CreatedDate;
use Crud\Mailing\Field\ErrorCount;
use Crud\Mailing\Field\FromAddress;
use Crud\Mailing\Field\IsPlaying;
use Crud\Mailing\Field\MailingListId;
use Crud\Mailing\Field\MailingTemplateId;
use Crud\Mailing\Field\SendCount;
use Crud\Mailing\Field\Status;
use Crud\Mailing\Field\Subject;
use Crud\Mailing\Field\TotalCount;
use Exception\LogicException;
use Model\Marketing\Mailing;
use Model\Marketing\MailingQuery;
use Model\Marketing\Map\MailingTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Mailing instead if you need to override or add functionality.
 */
abstract class CrudMailingManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingQuery::create();
	}


	public function getTableMap(): MailingTableMap
	{
		return new MailingTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Mailing";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingListId', 'MailingTemplateId', 'FromAddress', 'Subject', 'Contents', 'IsPlaying', 'TotalCount', 'ErrorCount', 'SendCount', 'Status', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingListId', 'MailingTemplateId', 'FromAddress', 'Subject', 'Contents', 'IsPlaying', 'TotalCount', 'ErrorCount', 'SendCount', 'Status', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Mailing
	 */
	public function getModel(array $aData = null): Mailing
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingQuery = MailingQuery::create();
		     $oMailing = $oMailingQuery->findOneById($aData['id']);
		     if (!$oMailing instanceof Mailing) {
		         throw new LogicException("Mailing should be an instance of Mailing but got something else." . __METHOD__);
		     }
		     $oMailing = $this->fillVo($aData, $oMailing);
		}
		else {
		     $oMailing = new Mailing();
		     if (!empty($aData)) {
		         $oMailing = $this->fillVo($aData, $oMailing);
		     }
		}
		return $oMailing;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Mailing
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Mailing
	{
		$oMailing = $this->getModel($aData);


		 if(!empty($oMailing))
		 {
		     $oMailing = $this->fillVo($aData, $oMailing);
		     $oMailing->save();
		 }
		return $oMailing;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Mailing $oModel
	 * @return Mailing
	 */
	protected function fillVo(array $aData, Mailing $oModel): Mailing
	{
		if(isset($aData['mailing_list_id'])) {
		     $oField = new MailingListId();
		     $mValue = $oField->sanitize($aData['mailing_list_id']);
		     $oModel->setMailingListId($mValue);
		}
		if(isset($aData['mailing_template_id'])) {
		     $oField = new MailingTemplateId();
		     $mValue = $oField->sanitize($aData['mailing_template_id']);
		     $oModel->setMailingTemplateId($mValue);
		}
		if(isset($aData['from_address'])) {
		     $oField = new FromAddress();
		     $mValue = $oField->sanitize($aData['from_address']);
		     $oModel->setFromAddress($mValue);
		}
		if(isset($aData['subject'])) {
		     $oField = new Subject();
		     $mValue = $oField->sanitize($aData['subject']);
		     $oModel->setSubject($mValue);
		}
		if(isset($aData['contents'])) {
		     $oField = new Contents();
		     $mValue = $oField->sanitize($aData['contents']);
		     $oModel->setContents($mValue);
		}
		if(isset($aData['is_playing'])) {
		     $oField = new IsPlaying();
		     $mValue = $oField->sanitize($aData['is_playing']);
		     $oModel->setIsPlaying($mValue);
		}
		if(isset($aData['total_count'])) {
		     $oField = new TotalCount();
		     $mValue = $oField->sanitize($aData['total_count']);
		     $oModel->setTotalCount($mValue);
		}
		if(isset($aData['error_count'])) {
		     $oField = new ErrorCount();
		     $mValue = $oField->sanitize($aData['error_count']);
		     $oModel->setErrorCount($mValue);
		}
		if(isset($aData['send_count'])) {
		     $oField = new SendCount();
		     $mValue = $oField->sanitize($aData['send_count']);
		     $oModel->setSendCount($mValue);
		}
		if(isset($aData['status'])) {
		     $oField = new Status();
		     $mValue = $oField->sanitize($aData['status']);
		     $oModel->setStatus($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
