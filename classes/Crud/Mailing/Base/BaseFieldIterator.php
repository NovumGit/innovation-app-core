<?php
namespace Crud\Mailing\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Mailing\ICollectionField as MailingField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Mailing\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailingField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailingField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailingField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailingField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailingField
	{
		return current($this->aFields);
	}
}
