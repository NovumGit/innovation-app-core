<?php
namespace Crud\NotificationAction\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\NotificationAction\ICollectionField as NotificationActionField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\NotificationAction\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param NotificationActionField[] $aFields */
	private array $aFields = [];


	/**
	 * @param NotificationActionField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof NotificationActionField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(NotificationActionField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): NotificationActionField
	{
		return current($this->aFields);
	}
}
