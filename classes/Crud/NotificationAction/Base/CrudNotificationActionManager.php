<?php
namespace Crud\NotificationAction\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\NotificationAction\FieldIterator;
use Crud\NotificationAction\Field\Label;
use Crud\NotificationAction\Field\NotificationId;
use Crud\NotificationAction\Field\Url;
use Exception\LogicException;
use Model\System\Map\NotificationActionTableMap;
use Model\System\NotificationAction;
use Model\System\NotificationActionQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify NotificationAction instead if you need to override or add functionality.
 */
abstract class CrudNotificationActionManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return NotificationActionQuery::create();
	}


	public function getTableMap(): NotificationActionTableMap
	{
		return new NotificationActionTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "NotificationAction";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "notification_action toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "notification_action aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['NotificationId', 'Label', 'Url'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['NotificationId', 'Label', 'Url'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return NotificationAction
	 */
	public function getModel(array $aData = null): NotificationAction
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oNotificationActionQuery = NotificationActionQuery::create();
		     $oNotificationAction = $oNotificationActionQuery->findOneById($aData['id']);
		     if (!$oNotificationAction instanceof NotificationAction) {
		         throw new LogicException("NotificationAction should be an instance of NotificationAction but got something else." . __METHOD__);
		     }
		     $oNotificationAction = $this->fillVo($aData, $oNotificationAction);
		}
		else {
		     $oNotificationAction = new NotificationAction();
		     if (!empty($aData)) {
		         $oNotificationAction = $this->fillVo($aData, $oNotificationAction);
		     }
		}
		return $oNotificationAction;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return NotificationAction
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): NotificationAction
	{
		$oNotificationAction = $this->getModel($aData);


		 if(!empty($oNotificationAction))
		 {
		     $oNotificationAction = $this->fillVo($aData, $oNotificationAction);
		     $oNotificationAction->save();
		 }
		return $oNotificationAction;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param NotificationAction $oModel
	 * @return NotificationAction
	 */
	protected function fillVo(array $aData, NotificationAction $oModel): NotificationAction
	{
		if(isset($aData['notification_id'])) {
		     $oField = new NotificationId();
		     $mValue = $oField->sanitize($aData['notification_id']);
		     $oModel->setNotificationId($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
