<?php
namespace Crud\NotificationAction\Field;

use Crud\NotificationAction\Field\Base\NotificationId as BaseNotificationId;

/**
 * Skeleton subclass for representing notification_id field from the notification_action table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class NotificationId extends BaseNotificationId
{
}
