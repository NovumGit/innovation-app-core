<?php
namespace Crud\CrudEditorButtonEvent\Field;

use Crud\CrudEditorButtonEvent\Field\Base\EventClass as BaseEventClass;

/**
 * Skeleton subclass for representing event_class field from the crud_editor_button_event table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EventClass extends BaseEventClass
{
}
