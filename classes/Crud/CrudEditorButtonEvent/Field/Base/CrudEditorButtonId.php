<?php
namespace Crud\CrudEditorButtonEvent\Field\Base;

use Core\Utils;
use Crud\CrudEditorButtonEvent\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\Setting\CrudManager\CrudEditorButtonQuery;

/**
 * Base class that represents the 'crud_editor_button_id' crud field from the 'crud_editor_button_event' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudEditorButtonId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'crud_editor_button_id';
	protected $sFieldLabel = 'Knop';
	protected $sIcon = 'flash';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudEditorButtonId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonEvent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\CrudEditorButtonQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\CrudEditorButtonQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
