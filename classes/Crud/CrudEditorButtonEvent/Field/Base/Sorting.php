<?php
namespace Crud\CrudEditorButtonEvent\Field\Base;

use Crud\CrudEditorButtonEvent\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'sorting' crud field from the 'crud_editor_button_event' table.
 * This class is auto generated and should not be modified.
 */
abstract class Sorting extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sorting';
	protected $sFieldLabel = 'Volgorde';
	protected $sIcon = 'sort';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSorting';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonEvent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
