<?php
namespace Crud\CrudEditorButtonEvent\Field\Base;

use Crud\CrudEditorButtonEvent\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'event_class' crud field from the 'crud_editor_button_event' table.
 * This class is auto generated and should not be modified.
 */
abstract class EventClass extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'event_class';
	protected $sFieldLabel = 'Event klasse';
	protected $sIcon = 'code';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEventClass';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditorButtonEvent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
