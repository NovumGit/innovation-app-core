<?php 
namespace Crud\CrudEditorButtonEvent\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditorButtonEvent;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButtonEvent)
		{
		     return "//system/crud_editor_button_event/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditorButtonEvent)
		{
		     return "//crud_editor_button_event?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
