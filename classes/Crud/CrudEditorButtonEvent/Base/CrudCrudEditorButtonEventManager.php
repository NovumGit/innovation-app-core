<?php
namespace Crud\CrudEditorButtonEvent\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditorButtonEvent\FieldIterator;
use Crud\CrudEditorButtonEvent\Field\CrudEditorButtonId;
use Crud\CrudEditorButtonEvent\Field\EventClass;
use Crud\CrudEditorButtonEvent\Field\Sorting;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditorButtonEvent;
use Model\Setting\CrudManager\CrudEditorButtonEventQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonEventTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditorButtonEvent instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorButtonEventManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonEventQuery::create();
	}


	public function getTableMap(): CrudEditorButtonEventTableMap
	{
		return new CrudEditorButtonEventTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn acties opgeslagen die worden uitgevoerd als men op een knop drukt";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditorButtonEvent";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor_button_event toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor_button_event aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorButtonId', 'EventClass', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudEditorButtonId', 'EventClass', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditorButtonEvent
	 */
	public function getModel(array $aData = null): CrudEditorButtonEvent
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorButtonEventQuery = CrudEditorButtonEventQuery::create();
		     $oCrudEditorButtonEvent = $oCrudEditorButtonEventQuery->findOneById($aData['id']);
		     if (!$oCrudEditorButtonEvent instanceof CrudEditorButtonEvent) {
		         throw new LogicException("CrudEditorButtonEvent should be an instance of CrudEditorButtonEvent but got something else." . __METHOD__);
		     }
		     $oCrudEditorButtonEvent = $this->fillVo($aData, $oCrudEditorButtonEvent);
		}
		else {
		     $oCrudEditorButtonEvent = new CrudEditorButtonEvent();
		     if (!empty($aData)) {
		         $oCrudEditorButtonEvent = $this->fillVo($aData, $oCrudEditorButtonEvent);
		     }
		}
		return $oCrudEditorButtonEvent;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditorButtonEvent
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditorButtonEvent
	{
		$oCrudEditorButtonEvent = $this->getModel($aData);


		 if(!empty($oCrudEditorButtonEvent))
		 {
		     $oCrudEditorButtonEvent = $this->fillVo($aData, $oCrudEditorButtonEvent);
		     $oCrudEditorButtonEvent->save();
		 }
		return $oCrudEditorButtonEvent;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditorButtonEvent $oModel
	 * @return CrudEditorButtonEvent
	 */
	protected function fillVo(array $aData, CrudEditorButtonEvent $oModel): CrudEditorButtonEvent
	{
		if(isset($aData['crud_editor_button_id'])) {
		     $oField = new CrudEditorButtonId();
		     $mValue = $oField->sanitize($aData['crud_editor_button_id']);
		     $oModel->setCrudEditorButtonId($mValue);
		}
		if(isset($aData['event_class'])) {
		     $oField = new EventClass();
		     $mValue = $oField->sanitize($aData['event_class']);
		     $oModel->setEventClass($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
