<?php
namespace Crud\CrudEditorButtonEvent\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudEditorButtonEvent\ICollectionField as CrudEditorButtonEventField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudEditorButtonEvent\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudEditorButtonEventField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudEditorButtonEventField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudEditorButtonEventField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudEditorButtonEventField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudEditorButtonEventField
	{
		return current($this->aFields);
	}
}
