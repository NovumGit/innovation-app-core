<?php
namespace Crud\UsaState\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UsaState\ICollectionField;

/**
 * Base class that represents the 'abrev' crud field from the 'mt_usa_state' table.
 * This class is auto generated and should not be modified.
 */
abstract class Abrev extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'abrev';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAbrev';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\UsaState';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
