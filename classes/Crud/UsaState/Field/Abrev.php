<?php
namespace Crud\UsaState\Field;

use Crud\UsaState\Field\Base\Abrev as BaseAbrev;

/**
 * Skeleton subclass for representing abrev field from the mt_usa_state table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Abrev extends BaseAbrev
{
}
