<?php
namespace Crud\UsaState\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UsaState\FieldIterator;
use Crud\UsaState\Field\Abrev;
use Crud\UsaState\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\UsaStateTableMap;
use Model\Setting\MasterTable\UsaState;
use Model\Setting\MasterTable\UsaStateQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UsaState instead if you need to override or add functionality.
 */
abstract class CrudUsaStateManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UsaStateQuery::create();
	}


	public function getTableMap(): UsaStateTableMap
	{
		return new UsaStateTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UsaState";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_usa_state toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_usa_state aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Abrev'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Abrev'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UsaState
	 */
	public function getModel(array $aData = null): UsaState
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUsaStateQuery = UsaStateQuery::create();
		     $oUsaState = $oUsaStateQuery->findOneById($aData['id']);
		     if (!$oUsaState instanceof UsaState) {
		         throw new LogicException("UsaState should be an instance of UsaState but got something else." . __METHOD__);
		     }
		     $oUsaState = $this->fillVo($aData, $oUsaState);
		}
		else {
		     $oUsaState = new UsaState();
		     if (!empty($aData)) {
		         $oUsaState = $this->fillVo($aData, $oUsaState);
		     }
		}
		return $oUsaState;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UsaState
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UsaState
	{
		$oUsaState = $this->getModel($aData);


		 if(!empty($oUsaState))
		 {
		     $oUsaState = $this->fillVo($aData, $oUsaState);
		     $oUsaState->save();
		 }
		return $oUsaState;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UsaState $oModel
	 * @return UsaState
	 */
	protected function fillVo(array $aData, UsaState $oModel): UsaState
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['abrev'])) {
		     $oField = new Abrev();
		     $mValue = $oField->sanitize($aData['abrev']);
		     $oModel->setAbrev($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
