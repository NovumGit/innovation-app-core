<?php
namespace Crud\Legal_form\Field;

use Crud\Field;
use Crud\Generic\Field\GenericEdit;
use Crud\Generic\Field\GenericEditTextfield;
use InvalidArgumentException;

use Model\Setting\MasterTable\LegalForm as ModelObject;

class Name extends GenericEditTextfield {



	function __construct()
	{
		$this->sFieldName = '';
		$this->sFieldLabel = '';
		$this->sPlaceHolder = '';
		$this->sGetter = '';
		$this->sFqModelClassname = null;
	}



	function getFieldTitle()
    {
        return 'Rechtsvorm';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
