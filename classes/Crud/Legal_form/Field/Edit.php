<?php
namespace Crud\Legal_form\Field;

use Model\Setting\MasterTable\LegalForm;
use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;


class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof LegalForm)
        {
            throw new InvalidArgumentException('Expected an instance of LegalForm but got '.get_class($oObject));
        }

        return '/setting/mastertable/legal_form/edit?id='.$oObject->getId();
    }
}