<?php
namespace Crud\Legal_form\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\LegalForm;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof LegalForm)
        {
            throw new InvalidArgumentException('Expected an instance of BicCode but got '.get_class($oObject));
        }

        return '/setting/mastertable/legal_form/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}