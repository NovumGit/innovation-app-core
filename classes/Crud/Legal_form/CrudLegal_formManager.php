<?php
namespace Crud\Legal_form;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\LegalFormQuery;
use Model\Setting\MasterTable\LegalForm;
use Propel\Runtime\Exception\LogicException;

class CrudLegal_formManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/legal_form/edit';
    }
    function getEntityTitle():string
    {
        return 'rechtsvorm';
    }
    function getNewFormTitle():string{
        return 'Rechtsvorm toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Rechtsvorm wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Delete',
            'Edit'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name'
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oLegalForm = new LegalFormQuery();

            $oLegalForm = $oLegalForm->findOneById($aData['id']);

            if(!$oLegalForm instanceof LegalForm){
                throw new LogicException("LegalForm should be an instance of LegalForm but got ".get_class($oLegalForm)." in ".__METHOD__);
            }
        }
        else
        {
            $oLegalForm = new LegalForm();

            if(isset($aData['name']))
            {
                $oLegalForm->setName($aData['name']);
            }
        }

        return $oLegalForm;
    }

    function store(array $aData = null)
    {
        $oLegalForm = $this->getModel($aData);

        if(!empty($oLegalForm))
        {
            $oLegalForm->setName($aData['name']);
            $oLegalForm->save();
        }
        return $oLegalForm;
    }
}
