<?php
namespace Crud\Component_button_group_role\Base;

use Core\Utils;
use Crud;
use Crud\Component_button_group_role\FieldIterator;
use Crud\Component_button_group_role\Field\ItemKey;
use Crud\Component_button_group_role\Field\ItemLabel;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\ButtonGroup\Component_button_group_role;
use Model\System\LowCode\ButtonGroup\Component_button_group_roleQuery;
use Model\System\LowCode\ButtonGroup\Map\Component_button_group_roleTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_button_group_role instead if you need to override or add functionality.
 */
abstract class CrudComponent_button_group_roleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_button_group_roleQuery::create();
	}


	public function getTableMap(): Component_button_group_roleTableMap
	{
		return new Component_button_group_roleTableMap();
	}


	public function getShortDescription(): string
	{
		return "Used for validation and to populate dropdown values.";
	}


	public function getEntityTitle(): string
	{
		return "Component_button_group_role";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_button_group_role toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_button_group_role aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_button_group_role
	 */
	public function getModel(array $aData = null): Component_button_group_role
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_button_group_roleQuery = Component_button_group_roleQuery::create();
		     $oComponent_button_group_role = $oComponent_button_group_roleQuery->findOneById($aData['id']);
		     if (!$oComponent_button_group_role instanceof Component_button_group_role) {
		         throw new LogicException("Component_button_group_role should be an instance of Component_button_group_role but got something else." . __METHOD__);
		     }
		     $oComponent_button_group_role = $this->fillVo($aData, $oComponent_button_group_role);
		}
		else {
		     $oComponent_button_group_role = new Component_button_group_role();
		     if (!empty($aData)) {
		         $oComponent_button_group_role = $this->fillVo($aData, $oComponent_button_group_role);
		     }
		}
		return $oComponent_button_group_role;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_button_group_role
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_button_group_role
	{
		$oComponent_button_group_role = $this->getModel($aData);


		 if(!empty($oComponent_button_group_role))
		 {
		     $oComponent_button_group_role = $this->fillVo($aData, $oComponent_button_group_role);
		     $oComponent_button_group_role->save();
		 }
		return $oComponent_button_group_role;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_button_group_role $oModel
	 * @return Component_button_group_role
	 */
	protected function fillVo(array $aData, Component_button_group_role $oModel): Component_button_group_role
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_label'])) {
		     $oField = new ItemLabel();
		     $mValue = $oField->sanitize($aData['item_label']);
		     $oModel->setItemLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
