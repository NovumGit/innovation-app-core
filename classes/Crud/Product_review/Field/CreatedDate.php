<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product_review\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Core\Config;
use Model\ProductReview as ModelObject;


class CreatedDate extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'created_date';
    protected $sFieldLabel = 'Aanmaak datum';
    private $sIcon = 'calendar';
    private $sPlaceHolder = 'string';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oCreatedOn = $oModelObject->getCreatedDate();

        $sCreatedOn = '';
        if($oCreatedOn instanceof \DateTime)
        {
            $sCreatedOn = $oCreatedOn->format(Config::getDateTimeFormat());
        }
        return '<td class="">'.$sCreatedOn.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCreatedOn = $oModelObject->getCreatedDate();
        $sCreatedOn = '';
        if($oCreatedOn instanceof \DateTime)
        {
            $sCreatedOn = $oCreatedOn->format(Config::getDateFormat());
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sCreatedOn, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
