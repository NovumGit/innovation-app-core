<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product_review\Field;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Base\ProductReviewQuery;
use Model\ProductReview as ModelObject;

class Rating extends Field implements IFilterableLookupField{

    protected $sFieldName = 'rating';
    protected $sFieldLabel = 'Waardering';
    private $sIcon = 'star-o';
    private $sGetter = 'getRating';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getLookups($mSelectedItem = null)
    {
        $aLookups = range(0, 5);
        $aOptions = [];
        foreach($aLookups as $iLookup)
        {
            $sSelected = '';
            if($mSelectedItem == $iLookup)
            {
                $sSelected = 'selected';
            }
            $aOptions[] = ['id' => $iLookup, 'selected' => $sSelected ,'label' => $iLookup];
        }
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return ProductReviewQuery::create()->findOneById($iItemId)->getRating();
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aStars = [];
        foreach ($oModelObject->getStars() as $sStar)
        {
            if($sStar == 'full')
            {
                $aStars[] = '<i class="fa fa-star"></i>';
            }
            elseif($sStar == 'half')
            {
                $aStars[] = '<i class="fa fa-star-half"></i>';
            }
            elseif($sStar == 'empty')
            {
                $aStars[] = '<i class="fa fa-star-o"></i>';
            }
        }
        return '<td class="">'.join(' ', $aStars).'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return
        $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
