<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Product_review\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\ProductReview as ModelObject;


class Product extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'product';
    protected $sFieldLabel = 'Product';
    private $sIcon = 'gift';
    private $sPlaceHolder = 'string';
    private $sGetter = 'getProduct';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        if($oModelObject->getProduct() instanceof \Model\Product)
        {
            $sNumber = $oModelObject->getProduct()->getNumber();
        }
        else
        {
            $sNumber = 'Kies een product';
        }

        $sUrl = '/product/pickproduct';
        $sTitle = '';
        return $this->editLinkField(Translate::fromCode('Product'), $sNumber, $sUrl, $sTitle);
    }
}
