<?php
namespace Crud\Product_review\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Product;
use Model\ProductReview;

class ApproveButton extends Field implements IEventField{

    protected $sFieldLabel = 'Goedkeuren';

    function getIcon()
    {
        return 'check';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }
    function getOverviewValue($mData)
    {
        if(!$mData instanceof ProductReview)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $aOut = [];
        $aOut[] = '<td class="xx">';

        if($mData->getApproved())
        {
            $aOut[] = "    <a title=\"Review afkeuren\" href=\"/review/moderation?_do=Approve&review_id={$mData->getId()}\" class=\"btn btn-warning br2 btn-xs fs12 d\">";
            $aOut[] = '         <i class="fa fa-close"></i>';
            $aOut[] = '    </a>';
        }
        else
        {
            $aOut[] = "    <a title=\"Review goedkeuren\"  href=\"/review/moderation?_do=Approve&review_id={$mData->getId()}\" class=\"btn btn-success br2 btn-xs fs12 d\">";
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
        }


        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }
    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
