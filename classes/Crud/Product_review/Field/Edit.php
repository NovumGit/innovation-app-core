<?php
namespace Crud\Product_review\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\InvalidArgumentException;
use Model\ProductReview;

class Edit extends GenericEdit
{
    function getEditUrl($oProductReview){

        if(!$oProductReview instanceof ProductReview)
        {
            throw new InvalidArgumentException('Expected an instance of ProductReview but got '.get_class($oProductReview));
        }
        $addUrl = '';

        if(isset($_GET['tab']))
        {
            $addUrl = '&tab='.$_GET['tab'];
        }

        return '/review/edit?id='.$oProductReview->getId().$addUrl;
    }
}