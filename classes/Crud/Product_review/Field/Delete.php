<?php
namespace Crud\Product_review\Field;

use Crud\Generic\Field\GenericDelete;
use InvalidArgumentException;
use Model\ProductReview;

class Delete extends GenericDelete{

    protected $sFieldLabel = 'Verwijderen';

    function getDeleteUrl($oProductReview)
    {
        if(!$oProductReview instanceof ProductReview)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oProductReview));
        }
        $sAddUrl = '';
        if($_GET['tab'])
        {
            $sAddUrl = '&tab='.$_GET['tab'];
        }
        return '/review/moderation?_do=ConfirmDelete&id='.$oProductReview->getId().$sAddUrl;
    }

    function getUnDeleteUrl($oProduct)
    {
        return null;
    }
}