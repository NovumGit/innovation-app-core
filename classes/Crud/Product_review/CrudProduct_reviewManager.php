<?php
namespace Crud\Product_review;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\ProductReview;
use Model\ProductReviewQuery;


class CrudProduct_reviewManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'product_review';
    }
    function getOverviewUrl():string
    {
        return '/review/moderation';
    }
    function getCreateNewUrl():string
    {
        return '/review/edit';
    }
    function getNewFormTitle():string{
        return 'Review toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Review bewerken';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Customer',
            'Description',
            'Rating',
            'CreatedDate',
            'Approved'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'First_name',
            'From_city',
            'Rating',
            'Description'
        ];
    }
    function getModel(array $aData = null)
    {
        $oProductReview = null;
        if(isset($aData['id']) && $aData['id'])
        {
            $oProductReviewQuery = new ProductReviewQuery();
            $oProductReview = $oProductReviewQuery->findOneById($aData['id']);
        }
        if(!$oProductReview instanceof ProductReview)
        {
            $oProductReview = new ProductReview();
        }
        if(!empty($aData))
        {
            $oProductReview = $this->fillVo($aData, $oProductReview);
        }
        return $oProductReview;
    }
    function store(array $aData = null)
    {
        $oProductReview = $this->getModel($aData);

        if(!empty($oProductReview))
        {
            $oProductReview = $this->fillVo($aData, $oProductReview);

            $oProductReview->save();
        }
        return $oProductReview;
    }
    private function fillVo($aData, ProductReview $oProductReview)
    {
        if(isset($aData['product_id'])){$oProductReview->setProductId($aData['product_id']);}
        if(isset($aData['customer_id'])){$oProductReview->setCustomerId($aData['customer_id']);}
        if(isset($aData['old_id'])){$oProductReview->setOldId($aData['old_id']);}
        if(isset($aData['first_name'])){$oProductReview->setFirstName($aData['first_name']);}
        if(isset($aData['from_city'])){$oProductReview->setFromCity($aData['from_city']);}
        if(isset($aData['description'])){$oProductReview->setDescription($aData['description']);}
        if(isset($aData['rating'])){$oProductReview->setRating($aData['rating']);}
        if(isset($aData['created_date'])){$oProductReview->setCreatedDate($aData['created_date']);}
        if(isset($aData['approved'])){$oProductReview->setApproved($aData['approved']);}
        if(isset($aData['first_name'])){$oProductReview->setFirstName($aData['first_name']);}
        if(isset($aData['from_city'])){$oProductReview->setFromCity($aData['from_city']);}
        return $oProductReview;
    }
}
