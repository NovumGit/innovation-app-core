<?php
namespace Crud\Sale_order_status;

use Core\Utils;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use LogicException;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;

class CrudSale_order_statusManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Order status';
    }
    function getNewFormTitle():string{
        return 'StatusId toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'StatusId bewerken';
    }
    function getOverviewUrl():string
    {
        return '/admin/mastertable/sale_order_status/overview';
    }
    function getCreateNewUrl():string
    {
        return '/admin/mastertable/sale_order_status/edit';
    }
    function getDefaultOverviewFields(bool $bAddNs = false):?array
    {
        $aOverviewColumns =  [
            'Name',
            'Edit',
            'Delete'
        ];
        if($bAddNs){
            array_walk($aOverviewColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }
    function getDefaultEditFields(bool $bAddNs = false):?array{
        $aOverviewColumns =  [
            'Name'
        ];
        if($bAddNs){
            array_walk($aOverviewColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }
    /**
     * @param $aData
     * @return SaleOrderStatus
     */
    function getModel(array $aData = null)
    {
        var_dump($aData);
        if($aData['id'])
        {
            $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneById($aData['id']);

            if(!$oSaleOrderStatus instanceof SaleOrderStatus)
            {
                var_dump($oSaleOrderStatus);
                throw new LogicException("SaleOrderStatus should be an instance of SaleOrderStatus but got ".get_class($oSaleOrderStatus)." in ".__METHOD__);
            }
        }
        else
        {
            $oSaleOrderStatus = new SaleOrderStatus();
            $oSaleOrderStatus = $this->fillVo($oSaleOrderStatus, $aData);
        }
        return $oSaleOrderStatus;
    }
    function store(array $aData = null)
    {
        $oSaleOrderStatus = $this->getModel($aData);

        if(!empty($oSaleOrderStatus))
        {

            $oSaleOrderStatus = $this->fillVo($oSaleOrderStatus, $aData);
            $oSaleOrderStatus->save();
        }
        return $oSaleOrderStatus;
    }
    function fillVo(SaleOrderStatus $oSaleOrderStatus, $aData)
    {

        if(isset($aData['name'])){
            $oSaleOrderStatus->setName($aData['name']);
        }
        if(isset($aData['code'])){
            $oSaleOrderStatus->setCode($aData['code']);
        }
        if(isset($aData['is_deletable'])){
            $oSaleOrderStatus->setIsDeletable($aData['is_deletable']);
        }
        return $oSaleOrderStatus;
    }
}
