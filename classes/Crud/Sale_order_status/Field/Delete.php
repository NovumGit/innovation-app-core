<?php
namespace Crud\Sale_order_status\Field;

use Crud\Generic\Field\GenericDelete;
use model\Setting\MasterTable\SaleOrderStatus as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oModelObject){


        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/setting/mastertable/sale_order_status/overview?_do=Delete&id='.$oModelObject->getId();
    }
    function getOverviewValue($oObject)
    {
        $aOut = [];
        $aOut[] = '<td class="">';

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        if($oObject->getIsDeletable())
        {
            $aOut[] = '<a href="'.$this->getDeleteUrl($oObject).'" class="btn btn-danger br2 btn-xs fs12"> <i class="fa fa-trash-o"></i> </a>';
        }
        else
        {
            $aOut[] = '<a href="#" class="btn btn-default br2 btn-xs fs12 d"><i class="fa fa-trash-o"></i> </a>';
        }


        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }

    function getUnDeleteUrl($oModelObject){
        return;
    }
}