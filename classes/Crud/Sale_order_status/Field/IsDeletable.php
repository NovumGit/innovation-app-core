<?php
namespace Crud\Sale_order_status\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\SaleOrderStatus;
use Crud\IFilterableField;
use Crud\IEditableField;

class IsDeletable extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'is_deletable';
    protected $sFieldLabel = 'Verwijderbaar';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations()
    {
        return true;
    }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getDataType():string
    {
        return 'boolean';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Kan verwijderd worden', 'is_deletable');
    }
    function getOverviewValue($oSaleOrderStatus)
    {
        if(!$oSaleOrderStatus instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\SaleOrderStatus\\SaleOrderStatus in ".__METHOD__);
        }
        $sVisible = $oSaleOrderStatus->getIsDeletable() ? 'Ja' : 'Nee';
        return '<td class="">'.$sVisible.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\SaleOrderStatus\\SaleOrderStatus in ".__METHOD__);
        }
        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $mData->getIsDeletable(), $bReadonly);
    }
}

