<?php
namespace Crud\Sale_order_status\Field;

use Crud\Field;
use InvalidArgumentException;
use model\Setting\MasterTable\SaleOrderStatus;

class Code extends Field{

    protected $sFieldName = 'code';
    protected $sFieldLabel = 'Code';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'Code';

    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oSaleOrderStatus)
    {
        if(!$oSaleOrderStatus instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($oSaleOrderStatus)."  in ".__METHOD__);
        }
        return '<td class="">'.$oSaleOrderStatus->getCode().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($mData)." in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getCode(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}