<?php
namespace Crud\Sale_order_status\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\SaleOrderStatus;

class Name extends Field{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Naam';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'Name';

    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oSaleOrderStatus)
    {
        if(!$oSaleOrderStatus instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($oSaleOrderStatus)."  in ".__METHOD__);
        }
        return '<td class="">'.$oSaleOrderStatus->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrderStatus)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($mData)." in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}