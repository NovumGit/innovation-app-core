<?php
namespace Crud\Sale_order_status\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\SaleOrderStatus as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }
        return '/admin/mastertable/sale_order_status/edit?id='.$oModelObject->getId();
    }
}

