<?php
namespace Crud\Rule\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Rule\FieldIterator;
use Crud\Rule\Field\Name;
use Crud\Rule\Field\Title;
use Exception\LogicException;
use Model\Rule\Map\RuleTableMap;
use Model\Rule\Rule;
use Model\Rule\RuleQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Rule instead if you need to override or add functionality.
 */
abstract class CrudRuleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return RuleQuery::create();
	}


	public function getTableMap(): RuleTableMap
	{
		return new RuleTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Rule";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "rule toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "rule aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Rule
	 */
	public function getModel(array $aData = null): Rule
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oRuleQuery = RuleQuery::create();
		     $oRule = $oRuleQuery->findOneById($aData['id']);
		     if (!$oRule instanceof Rule) {
		         throw new LogicException("Rule should be an instance of Rule but got something else." . __METHOD__);
		     }
		     $oRule = $this->fillVo($aData, $oRule);
		}
		else {
		     $oRule = new Rule();
		     if (!empty($aData)) {
		         $oRule = $this->fillVo($aData, $oRule);
		     }
		}
		return $oRule;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Rule
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Rule
	{
		$oRule = $this->getModel($aData);


		 if(!empty($oRule))
		 {
		     $oRule = $this->fillVo($aData, $oRule);
		     $oRule->save();
		 }
		return $oRule;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Rule $oModel
	 * @return Rule
	 */
	protected function fillVo(array $aData, Rule $oModel): Rule
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
