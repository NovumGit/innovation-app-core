<?php
namespace Crud\Rule\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Rule\ICollectionField as RuleField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Rule\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param RuleField[] $aFields */
	private array $aFields = [];


	/**
	 * @param RuleField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof RuleField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(RuleField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): RuleField
	{
		return current($this->aFields);
	}
}
