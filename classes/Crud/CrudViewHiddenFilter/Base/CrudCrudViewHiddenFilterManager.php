<?php
namespace Crud\CrudViewHiddenFilter\Base;

use Core\Utils;
use Crud;
use Crud\CrudViewHiddenFilter\FieldIterator;
use Crud\CrudViewHiddenFilter\Field\CrudViewId;
use Crud\CrudViewHiddenFilter\Field\FilterName;
use Crud\CrudViewHiddenFilter\Field\FilterOperatorId;
use Crud\CrudViewHiddenFilter\Field\FilterValue;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewHiddenFilter;
use Model\Setting\CrudManager\CrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\Map\CrudViewHiddenFilterTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudViewHiddenFilter instead if you need to override or add functionality.
 */
abstract class CrudCrudViewHiddenFilterManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewHiddenFilterQuery::create();
	}


	public function getTableMap(): CrudViewHiddenFilterTableMap
	{
		return new CrudViewHiddenFilterTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint is opgeslagen welke filters er op lijsten zijn toegepast.";
	}


	public function getEntityTitle(): string
	{
		return "CrudViewHiddenFilter";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view_hidden_filter toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view_hidden_filter aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudViewHiddenFilter
	 */
	public function getModel(array $aData = null): CrudViewHiddenFilter
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewHiddenFilterQuery = CrudViewHiddenFilterQuery::create();
		     $oCrudViewHiddenFilter = $oCrudViewHiddenFilterQuery->findOneById($aData['id']);
		     if (!$oCrudViewHiddenFilter instanceof CrudViewHiddenFilter) {
		         throw new LogicException("CrudViewHiddenFilter should be an instance of CrudViewHiddenFilter but got something else." . __METHOD__);
		     }
		     $oCrudViewHiddenFilter = $this->fillVo($aData, $oCrudViewHiddenFilter);
		}
		else {
		     $oCrudViewHiddenFilter = new CrudViewHiddenFilter();
		     if (!empty($aData)) {
		         $oCrudViewHiddenFilter = $this->fillVo($aData, $oCrudViewHiddenFilter);
		     }
		}
		return $oCrudViewHiddenFilter;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudViewHiddenFilter
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudViewHiddenFilter
	{
		$oCrudViewHiddenFilter = $this->getModel($aData);


		 if(!empty($oCrudViewHiddenFilter))
		 {
		     $oCrudViewHiddenFilter = $this->fillVo($aData, $oCrudViewHiddenFilter);
		     $oCrudViewHiddenFilter->save();
		 }
		return $oCrudViewHiddenFilter;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudViewHiddenFilter $oModel
	 * @return CrudViewHiddenFilter
	 */
	protected function fillVo(array $aData, CrudViewHiddenFilter $oModel): CrudViewHiddenFilter
	{
		if(isset($aData['crud_view_id'])) {
		     $oField = new CrudViewId();
		     $mValue = $oField->sanitize($aData['crud_view_id']);
		     $oModel->setCrudViewId($mValue);
		}
		if(isset($aData['filter_operator_id'])) {
		     $oField = new FilterOperatorId();
		     $mValue = $oField->sanitize($aData['filter_operator_id']);
		     $oModel->setFilterOperatorId($mValue);
		}
		if(isset($aData['filter_name'])) {
		     $oField = new FilterName();
		     $mValue = $oField->sanitize($aData['filter_name']);
		     $oModel->setFilterName($mValue);
		}
		if(isset($aData['filter_value'])) {
		     $oField = new FilterValue();
		     $mValue = $oField->sanitize($aData['filter_value']);
		     $oModel->setFilterValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
