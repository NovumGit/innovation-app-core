<?php
namespace Crud\CrudViewHiddenFilter\Field;

use Crud\CrudViewHiddenFilter\Field\Base\FilterOperatorId as BaseFilterOperatorId;

/**
 * Skeleton subclass for representing filter_operator_id field from the crud_view_hidden_filter table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FilterOperatorId extends BaseFilterOperatorId
{
}
