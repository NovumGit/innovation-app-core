<?php 
namespace Crud\CrudViewHiddenFilter\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudViewHiddenFilter;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewHiddenFilter)
		{
		     return "//system/crud_view_hidden_filter/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewHiddenFilter)
		{
		     return "//crud_view_hidden_filter?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
