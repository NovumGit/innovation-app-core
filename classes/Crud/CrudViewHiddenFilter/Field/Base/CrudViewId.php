<?php
namespace Crud\CrudViewHiddenFilter\Field\Base;

use Core\Utils;
use Crud\CrudViewHiddenFilter\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\Setting\CrudManager\CrudViewQuery;

/**
 * Base class that represents the 'crud_view_id' crud field from the 'crud_view_hidden_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudViewId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'crud_view_id';
	protected $sFieldLabel = 'View';
	protected $sIcon = 'table';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudViewId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewHiddenFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\CrudViewQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\CrudViewQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['crud_view_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "View" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
