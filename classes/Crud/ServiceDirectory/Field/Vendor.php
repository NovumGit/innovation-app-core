<?php/*** @unfixed**/
namespace Crud\ServiceDirectory\Field;

use Crud\ServiceDirectory\Field\Base\Vendor as BaseVendor;

/**
 * Skeleton subclass for representing vendor field from the service_directory table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Vendor extends BaseVendor
{
}
