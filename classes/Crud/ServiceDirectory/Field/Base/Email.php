<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericEmail;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'email' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class Email extends GenericEmail implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'email';
	protected $sFieldLabel = 'Email';
	protected $sIcon = 'envelope';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEmail';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
