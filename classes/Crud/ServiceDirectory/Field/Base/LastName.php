<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'last_name' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class LastName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'last_name';
	protected $sFieldLabel = 'Last name';
	protected $sIcon = 'user';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLastName';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
