<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'service_name' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class ServiceName extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'service_name';
	protected $sFieldLabel = 'Service name';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getServiceName';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['service_name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Service name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
