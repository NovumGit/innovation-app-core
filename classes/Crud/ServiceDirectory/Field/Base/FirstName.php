<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'first_name' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class FirstName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'first_name';
	protected $sFieldLabel = 'First name';
	protected $sIcon = 'user';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFirstName';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
