<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'homepage' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class Homepage extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'homepage';
	protected $sFieldLabel = 'Homepage';
	protected $sIcon = 'globe';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHomepage';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
