<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'license' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class License extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'license';
	protected $sFieldLabel = 'License';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLicense';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
