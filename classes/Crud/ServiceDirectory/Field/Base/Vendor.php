<?php
namespace Crud\ServiceDirectory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ServiceDirectory\ICollectionField;

/**
 * Base class that represents the 'vendor' crud field from the 'service_directory' table.
 * This class is auto generated and should not be modified.
 */
abstract class Vendor extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'vendor';
	protected $sFieldLabel = 'Vendor';
	protected $sIcon = 'lock';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getVendor';
	protected $sFqModelClassname = '\\\\\ServiceDirectory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['vendor']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Vendor" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
