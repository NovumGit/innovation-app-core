<?php
namespace Crud\ServiceDirectory;

use Crud\ServiceDirectory\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of ServiceDirectory crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
