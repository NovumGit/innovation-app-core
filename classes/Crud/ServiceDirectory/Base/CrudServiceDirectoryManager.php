<?php
namespace Crud\ServiceDirectory\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ServiceDirectory\FieldIterator;
use Crud\ServiceDirectory\Field\Description;
use Crud\ServiceDirectory\Field\Domain;
use Crud\ServiceDirectory\Field\Email;
use Crud\ServiceDirectory\Field\FirstName;
use Crud\ServiceDirectory\Field\Homepage;
use Crud\ServiceDirectory\Field\Keywords;
use Crud\ServiceDirectory\Field\LastName;
use Crud\ServiceDirectory\Field\License;
use Crud\ServiceDirectory\Field\ServiceName;
use Crud\ServiceDirectory\Field\Vendor;
use Exception\LogicException;
use Map\ServiceDirectoryTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;
use ServiceDirectory;
use ServiceDirectoryQuery;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ServiceDirectory instead if you need to override or add functionality.
 */
abstract class CrudServiceDirectoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ServiceDirectoryQuery::create();
	}


	public function getTableMap(): ServiceDirectoryTableMap
	{
		return new ServiceDirectoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "Contains known API\'s";
	}


	public function getEntityTitle(): string
	{
		return "ServiceDirectory";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "service_directory toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "service_directory aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Vendor', 'ServiceName', 'Homepage', 'Description', 'Keywords', 'License', 'FirstName', 'LastName', 'Email', 'Domain'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Vendor', 'ServiceName', 'Homepage', 'Description', 'Keywords', 'License', 'FirstName', 'LastName', 'Email', 'Domain'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ServiceDirectory
	 */
	public function getModel(array $aData = null): ServiceDirectory
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oServiceDirectoryQuery = ServiceDirectoryQuery::create();
		     $oServiceDirectory = $oServiceDirectoryQuery->findOneById($aData['id']);
		     if (!$oServiceDirectory instanceof ServiceDirectory) {
		         throw new LogicException("ServiceDirectory should be an instance of ServiceDirectory but got something else." . __METHOD__);
		     }
		     $oServiceDirectory = $this->fillVo($aData, $oServiceDirectory);
		}
		else {
		     $oServiceDirectory = new ServiceDirectory();
		     if (!empty($aData)) {
		         $oServiceDirectory = $this->fillVo($aData, $oServiceDirectory);
		     }
		}
		return $oServiceDirectory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ServiceDirectory
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ServiceDirectory
	{
		$oServiceDirectory = $this->getModel($aData);


		 if(!empty($oServiceDirectory))
		 {
		     $oServiceDirectory = $this->fillVo($aData, $oServiceDirectory);
		     $oServiceDirectory->save();
		 }
		return $oServiceDirectory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ServiceDirectory $oModel
	 * @return ServiceDirectory
	 */
	protected function fillVo(array $aData, ServiceDirectory $oModel): ServiceDirectory
	{
		if(isset($aData['vendor'])) {
		     $oField = new Vendor();
		     $mValue = $oField->sanitize($aData['vendor']);
		     $oModel->setVendor($mValue);
		}
		if(isset($aData['service_name'])) {
		     $oField = new ServiceName();
		     $mValue = $oField->sanitize($aData['service_name']);
		     $oModel->setServiceName($mValue);
		}
		if(isset($aData['homepage'])) {
		     $oField = new Homepage();
		     $mValue = $oField->sanitize($aData['homepage']);
		     $oModel->setHomepage($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['keywords'])) {
		     $oField = new Keywords();
		     $mValue = $oField->sanitize($aData['keywords']);
		     $oModel->setKeywords($mValue);
		}
		if(isset($aData['license'])) {
		     $oField = new License();
		     $mValue = $oField->sanitize($aData['license']);
		     $oModel->setLicense($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['last_name'])) {
		     $oField = new LastName();
		     $mValue = $oField->sanitize($aData['last_name']);
		     $oModel->setLastName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['domain'])) {
		     $oField = new Domain();
		     $mValue = $oField->sanitize($aData['domain']);
		     $oModel->setDomain($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
