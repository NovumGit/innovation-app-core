<?php
namespace Crud\ServiceDirectory\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\ServiceDirectory\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
