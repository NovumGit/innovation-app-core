<?php
namespace Crud\ServiceDirectory\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ServiceDirectory\ICollectionField as ServiceDirectoryField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ServiceDirectory\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ServiceDirectoryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ServiceDirectoryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ServiceDirectoryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ServiceDirectoryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ServiceDirectoryField
	{
		return current($this->aFields);
	}
}
