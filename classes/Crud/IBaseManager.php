<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28-11-19
 * Time: 15:34
 */

namespace Crud;

interface IBaseManager
{
    function getDefaultOverviewFields(bool $bAddNs = false): ?array;

    function getDefaultEditFields(bool $bAddNs = false): ?array;

    function getCrudLayoutKey();

    function getAllAvailableActions();

    function getAllAvailableFields();

    function setArgument($sKey, $sValue);

    function getArgument($sKey);

    function getAllArguments();

    public function loadCrudActions(array $aActionNames);

    /**
     * @param $aFieldNames
     * @return Field[]
     */
    public function loadCrudFields(array $aFieldNames);

    function getConfiguredEditFields($sFormLayoutKey);
}
