<?php
namespace Crud\Component_modal_button_location\Field;

use Crud\Component_modal_button_location\Field\Base\ItemKey as BaseItemKey;

/**
 * Skeleton subclass for representing item_key field from the component_modal_button_location table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ItemKey extends BaseItemKey
{
}
