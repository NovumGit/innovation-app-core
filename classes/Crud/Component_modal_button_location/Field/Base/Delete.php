<?php 
namespace Crud\Component_modal_button_location\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Modal\Component_modal_button_location;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_modal_button_location)
		{
		     return "//system/component_modal_button_location/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_modal_button_location)
		{
		     return "//component_modal_button_location?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
