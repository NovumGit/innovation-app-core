<?php
namespace Crud\Component_modal_button_location\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_modal_button_location\ICollectionField as Component_modal_button_locationField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_modal_button_location\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_modal_button_locationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_modal_button_locationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_modal_button_locationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_modal_button_locationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_modal_button_locationField
	{
		return current($this->aFields);
	}
}
