<?php
namespace Crud\Site_usp\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Cms\SiteUsp as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        $sSite = $this->getArgument('site');
        return "/cms/usp/overview?site=$sSite&_do=Delete&id=".$oModelObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}