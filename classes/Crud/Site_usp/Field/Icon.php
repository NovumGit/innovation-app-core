<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_usp\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteUsp as ModelObject;

class Icon extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'icon';
    protected $sFieldLabel = 'Icon';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getIcon';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aArguments = ['site' => $this->getArgument('site')];
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName, $aArguments);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
