<?php
namespace Crud\Site_usp\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Model\Cms\SiteUsp;

class Edit extends GenericEdit{


    function getEditUrl($oSiteUsp){

        if(!$oSiteUsp instanceof SiteUsp)
        {
            throw new LogicException('Expected an instance of \\model\\\Product\\\Product but got '.get_class($oSiteUsp));
        }

        $sSite = $oSiteUsp->getSite()->getDomain();
        return "/cms/usp/edit?site=$sSite&id=".$oSiteUsp->getId();
    }
}