<?php
namespace Crud\Site_usp;;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\Cms\SiteUsp;
use Model\Cms\SiteUspQuery;

class CrudSite_uspManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'site_usp';
    }
    function getNewFormTitle():string
    {
        return 'Unique sellingpoints';
    }
    function getEditFormTitle():string
    {
        return 'USP bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/usp/overview?site='.$_GET['site'];
        }
        return '/cms/usp/overview?site='.$this->getArgument('site');
    }
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/usp/edit?site='.$_GET['site'];
        }
        return '/cms/usp/edit?site='.$this->getArgument('site');
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'AboutTxt',
            'Slogan',
            'Edit'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'AboutTxt',
            'LongTxt',
            'ShortTxt',
            'Slogan',
        ];
    }


    /**
     * @param $aData
     * @return SiteUsp
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SiteUspQuery();

            $oSiteUsp = $oQuery->findOneById($aData['id']);

            if(!$oSiteUsp instanceof SiteUsp)
            {
                throw new LogicException("SiteUsp should be an instance of Site but got ".get_class($oSiteUsp)." in ".__METHOD__);
            }
        }
        else
        {
            $oSiteUsp = new SiteUsp();
            $oSiteUsp = $this->fillVo($oSiteUsp, $aData);
        }
        return $oSiteUsp;
    }

    function store(array $aData = null)
    {
        $oSiteUsp = $this->getModel($aData);

        if(!empty($oSiteUsp))
        {
            $oSiteUsp = $this->fillVo($oSiteUsp, $aData);
            $oSiteUsp->save();
        }
        return $oSiteUsp;
    }
    function fillVo(SiteUsp $oSiteUsp, $aData)
    {

        $sSite = $this->getArgument('site');

        $oSiteQuery = SiteQuery::create();
        $oSite = $oSiteQuery->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("You are trying to add a USP to a site but no site with the domain $sSite was found int the system.");
        }

        $oSiteUsp->setSiteId($oSite->getId());

        if(isset($aData['about_txt'])){
            $oSiteUsp->setAboutTxt($aData['about_txt']);
        }
        if(isset($aData['icon'])){
            $oSiteUsp->setIcon($aData['icon']);
        }
        if(isset($aData['language_id'])){
            $oSiteUsp->setLanguageId($aData['language_id']);
        }
        if(isset($aData['site_hook'])){
            $oSiteUsp->setSiteHook($aData['site_hook']);
        }
        if(isset($aData['short_txt'])){
            $oSiteUsp->setShortTxt($aData['short_txt']);
        }
        if(isset($aData['long_txt'])){
            $oSiteUsp->setLongTxt($aData['long_txt']);
        }
        if(isset($aData['slogan'])){
            $oSiteUsp->setSlogan($aData['slogan']);
        }
        return $oSiteUsp;
    }
}
