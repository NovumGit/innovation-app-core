<?php
namespace Crud\ProductColor\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductColor\FieldIterator;
use Crud\ProductColor\Field\ColorId;
use Crud\ProductColor\Field\ProductId;
use Exception\LogicException;
use Model\Map\ProductColorTableMap;
use Model\ProductColor;
use Model\ProductColorQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductColor instead if you need to override or add functionality.
 */
abstract class CrudProductColorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductColorQuery::create();
	}


	public function getTableMap(): ProductColorTableMap
	{
		return new ProductColorTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductColor";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_color toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_color aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'ColorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'ColorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductColor
	 */
	public function getModel(array $aData = null): ProductColor
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductColorQuery = ProductColorQuery::create();
		     $oProductColor = $oProductColorQuery->findOneById($aData['id']);
		     if (!$oProductColor instanceof ProductColor) {
		         throw new LogicException("ProductColor should be an instance of ProductColor but got something else." . __METHOD__);
		     }
		     $oProductColor = $this->fillVo($aData, $oProductColor);
		}
		else {
		     $oProductColor = new ProductColor();
		     if (!empty($aData)) {
		         $oProductColor = $this->fillVo($aData, $oProductColor);
		     }
		}
		return $oProductColor;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductColor
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductColor
	{
		$oProductColor = $this->getModel($aData);


		 if(!empty($oProductColor))
		 {
		     $oProductColor = $this->fillVo($aData, $oProductColor);
		     $oProductColor->save();
		 }
		return $oProductColor;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductColor $oModel
	 * @return ProductColor
	 */
	protected function fillVo(array $aData, ProductColor $oModel): ProductColor
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['color_id'])) {
		     $oField = new ColorId();
		     $mValue = $oField->sanitize($aData['color_id']);
		     $oModel->setColorId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
