<?php
namespace Crud\ProductColor\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductColor\ICollectionField as ProductColorField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductColor\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductColorField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductColorField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductColorField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductColorField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductColorField
	{
		return current($this->aFields);
	}
}
