<?php
namespace Crud\BankTransactionCategoryFilter\Field\Base;

use Crud\BankTransactionCategoryFilter\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'bank_transaction_category_id' crud field from the 'bank_transaction_category_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class BankTransactionCategoryId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'bank_transaction_category_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getBankTransactionCategoryId';
	protected $sFqModelClassname = '\\\Model\Finance\BankTransactionCategoryFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
