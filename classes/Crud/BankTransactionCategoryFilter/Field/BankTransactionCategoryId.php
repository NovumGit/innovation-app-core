<?php
namespace Crud\BankTransactionCategoryFilter\Field;

use Crud\BankTransactionCategoryFilter\Field\Base\BankTransactionCategoryId as BaseBankTransactionCategoryId;

/**
 * Skeleton subclass for representing bank_transaction_category_id field from the bank_transaction_category_filter table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class BankTransactionCategoryId extends BaseBankTransactionCategoryId
{
}
