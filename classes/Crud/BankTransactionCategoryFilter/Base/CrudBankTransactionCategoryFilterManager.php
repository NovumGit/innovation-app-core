<?php
namespace Crud\BankTransactionCategoryFilter\Base;

use Core\Utils;
use Crud;
use Crud\BankTransactionCategoryFilter\FieldIterator;
use Crud\BankTransactionCategoryFilter\Field\BankTransactionCategoryId;
use Crud\BankTransactionCategoryFilter\Field\FilterValue;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankTransactionCategoryFilter;
use Model\Finance\BankTransactionCategoryFilterQuery;
use Model\Finance\Map\BankTransactionCategoryFilterTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankTransactionCategoryFilter instead if you need to override or add functionality.
 */
abstract class CrudBankTransactionCategoryFilterManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankTransactionCategoryFilterQuery::create();
	}


	public function getTableMap(): BankTransactionCategoryFilterTableMap
	{
		return new BankTransactionCategoryFilterTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankTransactionCategoryFilter";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_transaction_category_filter toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_transaction_category_filter aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['BankTransactionCategoryId', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['BankTransactionCategoryId', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankTransactionCategoryFilter
	 */
	public function getModel(array $aData = null): BankTransactionCategoryFilter
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankTransactionCategoryFilterQuery = BankTransactionCategoryFilterQuery::create();
		     $oBankTransactionCategoryFilter = $oBankTransactionCategoryFilterQuery->findOneById($aData['id']);
		     if (!$oBankTransactionCategoryFilter instanceof BankTransactionCategoryFilter) {
		         throw new LogicException("BankTransactionCategoryFilter should be an instance of BankTransactionCategoryFilter but got something else." . __METHOD__);
		     }
		     $oBankTransactionCategoryFilter = $this->fillVo($aData, $oBankTransactionCategoryFilter);
		}
		else {
		     $oBankTransactionCategoryFilter = new BankTransactionCategoryFilter();
		     if (!empty($aData)) {
		         $oBankTransactionCategoryFilter = $this->fillVo($aData, $oBankTransactionCategoryFilter);
		     }
		}
		return $oBankTransactionCategoryFilter;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankTransactionCategoryFilter
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankTransactionCategoryFilter
	{
		$oBankTransactionCategoryFilter = $this->getModel($aData);


		 if(!empty($oBankTransactionCategoryFilter))
		 {
		     $oBankTransactionCategoryFilter = $this->fillVo($aData, $oBankTransactionCategoryFilter);
		     $oBankTransactionCategoryFilter->save();
		 }
		return $oBankTransactionCategoryFilter;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankTransactionCategoryFilter $oModel
	 * @return BankTransactionCategoryFilter
	 */
	protected function fillVo(array $aData, BankTransactionCategoryFilter $oModel): BankTransactionCategoryFilter
	{
		if(isset($aData['bank_transaction_category_id'])) {
		     $oField = new BankTransactionCategoryId();
		     $mValue = $oField->sanitize($aData['bank_transaction_category_id']);
		     $oModel->setBankTransactionCategoryId($mValue);
		}
		if(isset($aData['filter_value'])) {
		     $oField = new FilterValue();
		     $mValue = $oField->sanitize($aData['filter_value']);
		     $oModel->setFilterValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
