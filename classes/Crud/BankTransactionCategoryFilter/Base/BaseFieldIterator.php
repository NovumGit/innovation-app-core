<?php
namespace Crud\BankTransactionCategoryFilter\Base;

use Crud\BankTransactionCategoryFilter\ICollectionField as BankTransactionCategoryFilterField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankTransactionCategoryFilter\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankTransactionCategoryFilterField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankTransactionCategoryFilterField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankTransactionCategoryFilterField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankTransactionCategoryFilterField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankTransactionCategoryFilterField
	{
		return current($this->aFields);
	}
}
