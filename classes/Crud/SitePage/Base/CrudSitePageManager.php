<?php
namespace Crud\SitePage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SitePage\FieldIterator;
use Crud\SitePage\Field\About;
use Crud\SitePage\Field\Content;
use Crud\SitePage\Field\CoverImageExt;
use Crud\SitePage\Field\HasChangeableUrl;
use Crud\SitePage\Field\HasContent;
use Crud\SitePage\Field\HasCoverImage;
use Crud\SitePage\Field\HasMetaDescription;
use Crud\SitePage\Field\HasMetaKeywords;
use Crud\SitePage\Field\HasNavigation;
use Crud\SitePage\Field\HasTitle;
use Crud\SitePage\Field\HasTitleBackgroundcolorEditor;
use Crud\SitePage\Field\HasTitleColorEditor;
use Crud\SitePage\Field\HasTitlePositioningFields;
use Crud\SitePage\Field\HasUrl;
use Crud\SitePage\Field\IsDeletable;
use Crud\SitePage\Field\IsEditable;
use Crud\SitePage\Field\IsVisible;
use Crud\SitePage\Field\LanguageId;
use Crud\SitePage\Field\MaxPictures;
use Crud\SitePage\Field\MetaDescription;
use Crud\SitePage\Field\MetaKeywords;
use Crud\SitePage\Field\MinPictures;
use Crud\SitePage\Field\NavigationId;
use Crud\SitePage\Field\OldId;
use Crud\SitePage\Field\SiteId;
use Crud\SitePage\Field\Tag;
use Crud\SitePage\Field\Title;
use Crud\SitePage\Field\TitleBackgroundColor;
use Crud\SitePage\Field\TitleColor;
use Crud\SitePage\Field\TitlePosX;
use Crud\SitePage\Field\TitlePosY;
use Crud\SitePage\Field\Url;
use Exception\LogicException;
use Model\Cms\Map\SitePageTableMap;
use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SitePage instead if you need to override or add functionality.
 */
abstract class CrudSitePageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SitePageQuery::create();
	}


	public function getTableMap(): SitePageTableMap
	{
		return new SitePageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SitePage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_page toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_page aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'LanguageId', 'NavigationId', 'SiteId', 'Tag', 'Url', 'HasChangeableUrl', 'Title', 'TitleColor', 'TitleBackgroundColor', 'MetaDescription', 'MetaKeywords', 'Content', 'About', 'MinPictures', 'MaxPictures', 'TitlePosX', 'TitlePosY', 'HasTitleColorEditor', 'HasTitleBackgroundcolorEditor', 'HasNavigation', 'HasTitle', 'HasTitlePositioningFields', 'HasContent', 'HasUrl', 'HasMetaDescription', 'HasMetaKeywords', 'IsDeletable', 'IsEditable', 'IsVisible', 'HasCoverImage', 'CoverImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'LanguageId', 'NavigationId', 'SiteId', 'Tag', 'Url', 'HasChangeableUrl', 'Title', 'TitleColor', 'TitleBackgroundColor', 'MetaDescription', 'MetaKeywords', 'Content', 'About', 'MinPictures', 'MaxPictures', 'TitlePosX', 'TitlePosY', 'HasTitleColorEditor', 'HasTitleBackgroundcolorEditor', 'HasNavigation', 'HasTitle', 'HasTitlePositioningFields', 'HasContent', 'HasUrl', 'HasMetaDescription', 'HasMetaKeywords', 'IsDeletable', 'IsEditable', 'IsVisible', 'HasCoverImage', 'CoverImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SitePage
	 */
	public function getModel(array $aData = null): SitePage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSitePageQuery = SitePageQuery::create();
		     $oSitePage = $oSitePageQuery->findOneById($aData['id']);
		     if (!$oSitePage instanceof SitePage) {
		         throw new LogicException("SitePage should be an instance of SitePage but got something else." . __METHOD__);
		     }
		     $oSitePage = $this->fillVo($aData, $oSitePage);
		}
		else {
		     $oSitePage = new SitePage();
		     if (!empty($aData)) {
		         $oSitePage = $this->fillVo($aData, $oSitePage);
		     }
		}
		return $oSitePage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SitePage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SitePage
	{
		$oSitePage = $this->getModel($aData);


		 if(!empty($oSitePage))
		 {
		     $oSitePage = $this->fillVo($aData, $oSitePage);
		     $oSitePage->save();
		 }
		return $oSitePage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SitePage $oModel
	 * @return SitePage
	 */
	protected function fillVo(array $aData, SitePage $oModel): SitePage
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['navigation_id'])) {
		     $oField = new NavigationId();
		     $mValue = $oField->sanitize($aData['navigation_id']);
		     $oModel->setNavigationId($mValue);
		}
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		if(isset($aData['has_changeable_url'])) {
		     $oField = new HasChangeableUrl();
		     $mValue = $oField->sanitize($aData['has_changeable_url']);
		     $oModel->setHasChangeableUrl($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['title_color'])) {
		     $oField = new TitleColor();
		     $mValue = $oField->sanitize($aData['title_color']);
		     $oModel->setTitleColor($mValue);
		}
		if(isset($aData['title_background_color'])) {
		     $oField = new TitleBackgroundColor();
		     $mValue = $oField->sanitize($aData['title_background_color']);
		     $oModel->setTitleBackgroundColor($mValue);
		}
		if(isset($aData['meta_description'])) {
		     $oField = new MetaDescription();
		     $mValue = $oField->sanitize($aData['meta_description']);
		     $oModel->setMetaDescription($mValue);
		}
		if(isset($aData['meta_keywords'])) {
		     $oField = new MetaKeywords();
		     $mValue = $oField->sanitize($aData['meta_keywords']);
		     $oModel->setMetaKeywords($mValue);
		}
		if(isset($aData['content'])) {
		     $oField = new Content();
		     $mValue = $oField->sanitize($aData['content']);
		     $oModel->setContent($mValue);
		}
		if(isset($aData['about'])) {
		     $oField = new About();
		     $mValue = $oField->sanitize($aData['about']);
		     $oModel->setAbout($mValue);
		}
		if(isset($aData['min_pictures'])) {
		     $oField = new MinPictures();
		     $mValue = $oField->sanitize($aData['min_pictures']);
		     $oModel->setMinPictures($mValue);
		}
		if(isset($aData['max_pictures'])) {
		     $oField = new MaxPictures();
		     $mValue = $oField->sanitize($aData['max_pictures']);
		     $oModel->setMaxPictures($mValue);
		}
		if(isset($aData['title_pos_x'])) {
		     $oField = new TitlePosX();
		     $mValue = $oField->sanitize($aData['title_pos_x']);
		     $oModel->setTitlePosX($mValue);
		}
		if(isset($aData['title_pos_y'])) {
		     $oField = new TitlePosY();
		     $mValue = $oField->sanitize($aData['title_pos_y']);
		     $oModel->setTitlePosY($mValue);
		}
		if(isset($aData['has_title_color_editor'])) {
		     $oField = new HasTitleColorEditor();
		     $mValue = $oField->sanitize($aData['has_title_color_editor']);
		     $oModel->setHasTitleColorEditor($mValue);
		}
		if(isset($aData['has_title_backgroundcolor_editor'])) {
		     $oField = new HasTitleBackgroundcolorEditor();
		     $mValue = $oField->sanitize($aData['has_title_backgroundcolor_editor']);
		     $oModel->setHasTitleBackgroundcolorEditor($mValue);
		}
		if(isset($aData['has_navigation'])) {
		     $oField = new HasNavigation();
		     $mValue = $oField->sanitize($aData['has_navigation']);
		     $oModel->setHasNavigation($mValue);
		}
		if(isset($aData['has_title'])) {
		     $oField = new HasTitle();
		     $mValue = $oField->sanitize($aData['has_title']);
		     $oModel->setHasTitle($mValue);
		}
		if(isset($aData['has_title_positioning_fields'])) {
		     $oField = new HasTitlePositioningFields();
		     $mValue = $oField->sanitize($aData['has_title_positioning_fields']);
		     $oModel->setHasTitlePositioningFields($mValue);
		}
		if(isset($aData['has_content'])) {
		     $oField = new HasContent();
		     $mValue = $oField->sanitize($aData['has_content']);
		     $oModel->setHasContent($mValue);
		}
		if(isset($aData['has_url'])) {
		     $oField = new HasUrl();
		     $mValue = $oField->sanitize($aData['has_url']);
		     $oModel->setHasUrl($mValue);
		}
		if(isset($aData['has_meta_description'])) {
		     $oField = new HasMetaDescription();
		     $mValue = $oField->sanitize($aData['has_meta_description']);
		     $oModel->setHasMetaDescription($mValue);
		}
		if(isset($aData['has_meta_keywords'])) {
		     $oField = new HasMetaKeywords();
		     $mValue = $oField->sanitize($aData['has_meta_keywords']);
		     $oModel->setHasMetaKeywords($mValue);
		}
		if(isset($aData['is_deletable'])) {
		     $oField = new IsDeletable();
		     $mValue = $oField->sanitize($aData['is_deletable']);
		     $oModel->setIsDeletable($mValue);
		}
		if(isset($aData['is_editable'])) {
		     $oField = new IsEditable();
		     $mValue = $oField->sanitize($aData['is_editable']);
		     $oModel->setIsEditable($mValue);
		}
		if(isset($aData['is_visible'])) {
		     $oField = new IsVisible();
		     $mValue = $oField->sanitize($aData['is_visible']);
		     $oModel->setIsVisible($mValue);
		}
		if(isset($aData['has_cover_image'])) {
		     $oField = new HasCoverImage();
		     $mValue = $oField->sanitize($aData['has_cover_image']);
		     $oModel->setHasCoverImage($mValue);
		}
		if(isset($aData['cover_image_ext'])) {
		     $oField = new CoverImageExt();
		     $mValue = $oField->sanitize($aData['cover_image_ext']);
		     $oModel->setCoverImageExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
