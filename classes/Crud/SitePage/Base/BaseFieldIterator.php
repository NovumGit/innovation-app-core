<?php
namespace Crud\SitePage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SitePage\ICollectionField as SitePageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SitePage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SitePageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SitePageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SitePageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SitePageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SitePageField
	{
		return current($this->aFields);
	}
}
