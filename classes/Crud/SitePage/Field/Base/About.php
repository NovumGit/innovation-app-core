<?php
namespace Crud\SitePage\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SitePage\ICollectionField;

/**
 * Base class that represents the 'about' crud field from the 'site_page' table.
 * This class is auto generated and should not be modified.
 */
abstract class About extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'about';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAbout';
	protected $sFqModelClassname = '\\\Model\Cms\SitePage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
