<?php
namespace Crud\SitePage\Field;

use Crud\SitePage\Field\Base\About as BaseAbout;

/**
 * Skeleton subclass for representing about field from the site_page table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class About extends BaseAbout
{
}
