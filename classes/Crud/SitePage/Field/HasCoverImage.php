<?php
namespace Crud\SitePage\Field;

use Crud\SitePage\Field\Base\HasCoverImage as BaseHasCoverImage;

/**
 * Skeleton subclass for representing has_cover_image field from the site_page table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class HasCoverImage extends BaseHasCoverImage
{
}
