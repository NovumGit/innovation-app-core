<?php
namespace Crud\SitePage\Field;

use Crud\SitePage\Field\Base\HasTitleBackgroundcolorEditor as BaseHasTitleBackgroundcolorEditor;

/**
 * Skeleton subclass for representing has_title_backgroundcolor_editor field from the site_page table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class HasTitleBackgroundcolorEditor extends BaseHasTitleBackgroundcolorEditor
{
}
