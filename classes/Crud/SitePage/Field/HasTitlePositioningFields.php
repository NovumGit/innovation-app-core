<?php
namespace Crud\SitePage\Field;

use Crud\SitePage\Field\Base\HasTitlePositioningFields as BaseHasTitlePositioningFields;

/**
 * Skeleton subclass for representing has_title_positioning_fields field from the site_page table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class HasTitlePositioningFields extends BaseHasTitlePositioningFields
{
}
