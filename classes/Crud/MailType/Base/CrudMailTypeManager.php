<?php
namespace Crud\MailType\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailType\FieldIterator;
use Crud\MailType\Field\Name;
use Exception\LogicException;
use Model\Cms\Mail\MailType;
use Model\Cms\Mail\MailTypeQuery;
use Model\Cms\Mail\Map\MailTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailType instead if you need to override or add functionality.
 */
abstract class CrudMailTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailTypeQuery::create();
	}


	public function getTableMap(): MailTypeTableMap
	{
		return new MailTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mail_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mail_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailType
	 */
	public function getModel(array $aData = null): MailType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailTypeQuery = MailTypeQuery::create();
		     $oMailType = $oMailTypeQuery->findOneById($aData['id']);
		     if (!$oMailType instanceof MailType) {
		         throw new LogicException("MailType should be an instance of MailType but got something else." . __METHOD__);
		     }
		     $oMailType = $this->fillVo($aData, $oMailType);
		}
		else {
		     $oMailType = new MailType();
		     if (!empty($aData)) {
		         $oMailType = $this->fillVo($aData, $oMailType);
		     }
		}
		return $oMailType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailType
	{
		$oMailType = $this->getModel($aData);


		 if(!empty($oMailType))
		 {
		     $oMailType = $this->fillVo($aData, $oMailType);
		     $oMailType->save();
		 }
		return $oMailType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailType $oModel
	 * @return MailType
	 */
	protected function fillVo(array $aData, MailType $oModel): MailType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
