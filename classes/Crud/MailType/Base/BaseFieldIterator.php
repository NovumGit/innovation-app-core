<?php
namespace Crud\MailType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailType\ICollectionField as MailTypeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailTypeField
	{
		return current($this->aFields);
	}
}
