<?php
namespace Crud\Timezone\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\Timezone\ICollectionField;

/**
 * Base class that represents the 'continent' crud field from the 'timezone' table.
 * This class is auto generated and should not be modified.
 */
abstract class Continent extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'continent';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getContinent';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\Timezone';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['continent']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
