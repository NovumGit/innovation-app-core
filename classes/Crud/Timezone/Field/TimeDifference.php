<?php
namespace Crud\Timezone\Field;

use Crud\Timezone\Field\Base\TimeDifference as BaseTimeDifference;

/**
 * Skeleton subclass for representing time_difference field from the timezone table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class TimeDifference extends BaseTimeDifference
{
}
