<?php
namespace Crud\Timezone\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Timezone\FieldIterator;
use Crud\Timezone\Field\Continent;
use Crud\Timezone\Field\TimeDifference;
use Crud\Timezone\Field\ZoneName;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\TimezoneTableMap;
use Model\Setting\MasterTable\Timezone;
use Model\Setting\MasterTable\TimezoneQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Timezone instead if you need to override or add functionality.
 */
abstract class CrudTimezoneManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return TimezoneQuery::create();
	}


	public function getTableMap(): TimezoneTableMap
	{
		return new TimezoneTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Timezone";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "timezone toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "timezone aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Continent', 'ZoneName', 'TimeDifference'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Continent', 'ZoneName', 'TimeDifference'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Timezone
	 */
	public function getModel(array $aData = null): Timezone
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oTimezoneQuery = TimezoneQuery::create();
		     $oTimezone = $oTimezoneQuery->findOneById($aData['id']);
		     if (!$oTimezone instanceof Timezone) {
		         throw new LogicException("Timezone should be an instance of Timezone but got something else." . __METHOD__);
		     }
		     $oTimezone = $this->fillVo($aData, $oTimezone);
		}
		else {
		     $oTimezone = new Timezone();
		     if (!empty($aData)) {
		         $oTimezone = $this->fillVo($aData, $oTimezone);
		     }
		}
		return $oTimezone;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Timezone
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Timezone
	{
		$oTimezone = $this->getModel($aData);


		 if(!empty($oTimezone))
		 {
		     $oTimezone = $this->fillVo($aData, $oTimezone);
		     $oTimezone->save();
		 }
		return $oTimezone;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Timezone $oModel
	 * @return Timezone
	 */
	protected function fillVo(array $aData, Timezone $oModel): Timezone
	{
		if(isset($aData['continent'])) {
		     $oField = new Continent();
		     $mValue = $oField->sanitize($aData['continent']);
		     $oModel->setContinent($mValue);
		}
		if(isset($aData['zone_name'])) {
		     $oField = new ZoneName();
		     $mValue = $oField->sanitize($aData['zone_name']);
		     $oModel->setZoneName($mValue);
		}
		if(isset($aData['time_difference'])) {
		     $oField = new TimeDifference();
		     $mValue = $oField->sanitize($aData['time_difference']);
		     $oModel->setTimeDifference($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
