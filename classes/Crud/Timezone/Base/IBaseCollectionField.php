<?php
namespace Crud\Timezone\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Timezone\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
