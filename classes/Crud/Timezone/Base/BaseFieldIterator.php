<?php
namespace Crud\Timezone\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Timezone\ICollectionField as TimezoneField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Timezone\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param TimezoneField[] $aFields */
	private array $aFields = [];


	/**
	 * @param TimezoneField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof TimezoneField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(TimezoneField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): TimezoneField
	{
		return current($this->aFields);
	}
}
