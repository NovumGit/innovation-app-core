<?php
namespace Crud\CustomerType\Field\Base;

use Crud\CustomerType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'description' crud field from the 'mt_customer_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class Description extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'description';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDescription';
	protected $sFqModelClassname = '\\\Model\Crm\CustomerType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
