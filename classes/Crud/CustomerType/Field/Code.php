<?php
namespace Crud\CustomerType\Field;

use Crud\CustomerType\Field\Base\Code as BaseCode;

/**
 * Skeleton subclass for representing code field from the mt_customer_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Code extends BaseCode
{
}
