<?php
namespace Crud\CustomerType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerType\ICollectionField as CustomerTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerTypeField
	{
		return current($this->aFields);
	}
}
