<?php
namespace Crud\CustomerType\Base;

use Core\Utils;
use Crud;
use Crud\CustomerType\FieldIterator;
use Crud\CustomerType\Field\Code;
use Crud\CustomerType\Field\Description;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerType;
use Model\Crm\CustomerTypeQuery;
use Model\Crm\Map\CustomerTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerType instead if you need to override or add functionality.
 */
abstract class CrudCustomerTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerTypeQuery::create();
	}


	public function getTableMap(): CustomerTypeTableMap
	{
		return new CustomerTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_customer_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_customer_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerType
	 */
	public function getModel(array $aData = null): CustomerType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerTypeQuery = CustomerTypeQuery::create();
		     $oCustomerType = $oCustomerTypeQuery->findOneById($aData['id']);
		     if (!$oCustomerType instanceof CustomerType) {
		         throw new LogicException("CustomerType should be an instance of CustomerType but got something else." . __METHOD__);
		     }
		     $oCustomerType = $this->fillVo($aData, $oCustomerType);
		}
		else {
		     $oCustomerType = new CustomerType();
		     if (!empty($aData)) {
		         $oCustomerType = $this->fillVo($aData, $oCustomerType);
		     }
		}
		return $oCustomerType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerType
	{
		$oCustomerType = $this->getModel($aData);


		 if(!empty($oCustomerType))
		 {
		     $oCustomerType = $this->fillVo($aData, $oCustomerType);
		     $oCustomerType->save();
		 }
		return $oCustomerType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerType $oModel
	 * @return CustomerType
	 */
	protected function fillVo(array $aData, CustomerType $oModel): CustomerType
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
