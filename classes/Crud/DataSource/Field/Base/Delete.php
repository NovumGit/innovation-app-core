<?php 
namespace Crud\DataSource\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataSource;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSource)
		{
		     return "//system/datasource/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSource)
		{
		     return "//datasource?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
