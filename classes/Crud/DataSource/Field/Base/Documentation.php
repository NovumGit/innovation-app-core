<?php
namespace Crud\DataSource\Field\Base;

use Crud\DataSource\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'documentation' crud field from the 'datasource' table.
 * This class is auto generated and should not be modified.
 */
abstract class Documentation extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'documentation';
	protected $sFieldLabel = 'Documentatie url';
	protected $sIcon = 'globe';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDocumentation';
	protected $sFqModelClassname = '\\\Model\System\DataSource';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
