<?php
namespace Crud\DataSource\Field;

use Crud\DataSource\Field\Base\Titel as BaseTitel;

/**
 * Skeleton subclass for representing titel field from the datasource table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Titel extends BaseTitel
{
}
