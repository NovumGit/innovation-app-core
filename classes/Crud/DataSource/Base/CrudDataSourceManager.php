<?php
namespace Crud\DataSource\Base;

use Core\Utils;
use Crud;
use Crud\DataSource\FieldIterator;
use Crud\DataSource\Field\Code;
use Crud\DataSource\Field\Documentation;
use Crud\DataSource\Field\Titel;
use Crud\DataSource\Field\Url;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataSource;
use Model\System\DataSourceQuery;
use Model\System\Map\DataSourceTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataSource instead if you need to override or add functionality.
 */
abstract class CrudDataSourceManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataSourceQuery::create();
	}


	public function getTableMap(): DataSourceTableMap
	{
		return new DataSourceTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle API\'s die bekend zijn binnen dit systeem";
	}


	public function getEntityTitle(): string
	{
		return "DataSource";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "datasource toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "datasource aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code', 'Url', 'Documentation'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code', 'Url', 'Documentation'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataSource
	 */
	public function getModel(array $aData = null): DataSource
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataSourceQuery = DataSourceQuery::create();
		     $oDataSource = $oDataSourceQuery->findOneById($aData['id']);
		     if (!$oDataSource instanceof DataSource) {
		         throw new LogicException("DataSource should be an instance of DataSource but got something else." . __METHOD__);
		     }
		     $oDataSource = $this->fillVo($aData, $oDataSource);
		}
		else {
		     $oDataSource = new DataSource();
		     if (!empty($aData)) {
		         $oDataSource = $this->fillVo($aData, $oDataSource);
		     }
		}
		return $oDataSource;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataSource
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataSource
	{
		$oDataSource = $this->getModel($aData);


		 if(!empty($oDataSource))
		 {
		     $oDataSource = $this->fillVo($aData, $oDataSource);
		     $oDataSource->save();
		 }
		return $oDataSource;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataSource $oModel
	 * @return DataSource
	 */
	protected function fillVo(array $aData, DataSource $oModel): DataSource
	{
		if(isset($aData['titel'])) {
		     $oField = new Titel();
		     $mValue = $oField->sanitize($aData['titel']);
		     $oModel->setTitel($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		if(isset($aData['documentation'])) {
		     $oField = new Documentation();
		     $mValue = $oField->sanitize($aData['documentation']);
		     $oModel->setDocumentation($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
