<?php
namespace Crud\DataSource\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataSource\ICollectionField as DataSourceField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataSource\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataSourceField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataSourceField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataSourceField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataSourceField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataSourceField
	{
		return current($this->aFields);
	}
}
