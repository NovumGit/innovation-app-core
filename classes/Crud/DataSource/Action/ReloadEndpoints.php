<?php

namespace Crud\DataSource\Action;

use Core\InlineTemplate;
use Core\Mailer;
use Core\MailMessage;
use Core\Mime\VoidMime;
use Core\OpenApi\Parser;
use Core\Utils;
use Crud\AbstractActionDefinition;
use Crud\Field;
use Crud\FormManager;
use Crud\IActionDefinition;
use Crud\Sale_order\CrudSale_orderManager;
use Crud\Sale_order_item\CrudSale_order_itemManager;
use Exception\LogicException;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderEmail;
use Model\Sale\SaleOrderQuery;
use Model\System\DataSourceEndpointMethodQuery;
use Model\System\DataSourceEndpointQuery;
use Model\System\DataSourceQuery;
use Model\System\SystemRegistryQuery;

final class ReloadEndpoints extends AbstractActionDefinition implements IActionDefinition
{
    function hasOutput()
    {
        return true;
    }
    function saveConfiguration($sEnvironment)
    {
        // TODO: Implement saveConfiguration() method.
    }

    function isConfigurable(): bool
    {
        return false;
    }

    function getExampleAnswer(): string
    {
        return "{}";
    }

    function getExampleRequest(): string
    {
        return "{}";
    }

    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }


    function getDescription(): string
    {
        return "Haalt alle beschikbare endpoints op bij de API.";
    }

    /**
     * @param string $mArguments
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function trigger($mArguments): void
    {
        $oDataSource = DataSourceQuery::create()->findOneById($this->get('item_id'));

        $oOpenApiParser = new Parser();
        $oOpenApiParser->fromUrl($oDataSource->getUrl());

        $aPaths = $oOpenApiParser->getPaths();

        foreach($aPaths as $sPath => $aMethods)
        {
            foreach($aMethods as $sMethod => $aProperties)
            {
                $oDataSourceEndpointMethodQuery = DataSourceEndpointMethodQuery::create();
                $oDataSourceEndpointMethodQuery->filterByCode($sMethod);
                $oDataSourceEndpointMethod = $oDataSourceEndpointMethodQuery->findOneOrCreate();
                $oDataSourceEndpointMethod->save();

                $oDataSourceEndpointQuery = DataSourceEndpointQuery::create();
                $oDataSourceEndpointQuery->filterByDatasourceId($oDataSource->getId());
                $oDataSourceEndpointQuery->filterByUrl($sPath);
                $oDataSourceEndpointQuery->filterByDatasourceEndpointMethodId($oDataSourceEndpointMethod->getId());
                $oDataSourceEndpoint = $oDataSourceEndpointQuery->findOneOrCreate();
                $oDataSourceEndpoint->setSummary($aProperties['summary']);
                $oDataSourceEndpoint->setCode($aProperties['operationId']);
                $oDataSourceEndpoint->save();

                echo 'Adding ' . $sPath . ' -> ' . $sMethod . "<br>";
            }

        }


        exit();
    }
}

