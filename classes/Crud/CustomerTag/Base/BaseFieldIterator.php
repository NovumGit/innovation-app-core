<?php
namespace Crud\CustomerTag\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerTag\ICollectionField as CustomerTagField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerTag\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerTagField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerTagField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerTagField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerTagField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerTagField
	{
		return current($this->aFields);
	}
}
