<?php
namespace Crud\CustomerTag\Base;

use Core\Utils;
use Crud;
use Crud\CustomerTag\FieldIterator;
use Crud\CustomerTag\Field\CustomerId;
use Crud\CustomerTag\Field\TagId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerTag;
use Model\Crm\CustomerTagQuery;
use Model\Crm\Map\CustomerTagTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerTag instead if you need to override or add functionality.
 */
abstract class CrudCustomerTagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerTagQuery::create();
	}


	public function getTableMap(): CustomerTagTableMap
	{
		return new CustomerTagTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerTag";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_tag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_tag aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'TagId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'TagId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerTag
	 */
	public function getModel(array $aData = null): CustomerTag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerTagQuery = CustomerTagQuery::create();
		     $oCustomerTag = $oCustomerTagQuery->findOneById($aData['id']);
		     if (!$oCustomerTag instanceof CustomerTag) {
		         throw new LogicException("CustomerTag should be an instance of CustomerTag but got something else." . __METHOD__);
		     }
		     $oCustomerTag = $this->fillVo($aData, $oCustomerTag);
		}
		else {
		     $oCustomerTag = new CustomerTag();
		     if (!empty($aData)) {
		         $oCustomerTag = $this->fillVo($aData, $oCustomerTag);
		     }
		}
		return $oCustomerTag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerTag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerTag
	{
		$oCustomerTag = $this->getModel($aData);


		 if(!empty($oCustomerTag))
		 {
		     $oCustomerTag = $this->fillVo($aData, $oCustomerTag);
		     $oCustomerTag->save();
		 }
		return $oCustomerTag;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerTag $oModel
	 * @return CustomerTag
	 */
	protected function fillVo(array $aData, CustomerTag $oModel): CustomerTag
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['tag_id'])) {
		     $oField = new TagId();
		     $mValue = $oField->sanitize($aData['tag_id']);
		     $oModel->setTagId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
