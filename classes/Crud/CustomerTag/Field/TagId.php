<?php
namespace Crud\CustomerTag\Field;

use Crud\CustomerTag\Field\Base\TagId as BaseTagId;

/**
 * Skeleton subclass for representing tag_id field from the customer_tag table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class TagId extends BaseTagId
{
}
