<?php
namespace Crud\SiteFAQCategory\Field;

use Crud\SiteFAQCategory\Field\Base\ImageExt as BaseImageExt;

/**
 * Skeleton subclass for representing image_ext field from the site_faq_category table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class ImageExt extends BaseImageExt
{
}
