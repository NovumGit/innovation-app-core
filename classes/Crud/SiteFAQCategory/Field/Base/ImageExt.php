<?php
namespace Crud\SiteFAQCategory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteFAQCategory\ICollectionField;

/**
 * Base class that represents the 'image_ext' crud field from the 'site_faq_category' table.
 * This class is auto generated and should not be modified.
 */
abstract class ImageExt extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'image_ext';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getImageExt';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFAQCategory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
