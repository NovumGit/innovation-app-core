<?php
namespace Crud\SiteFAQCategory\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFAQCategory\FieldIterator;
use Crud\SiteFAQCategory\Field\HasImage;
use Crud\SiteFAQCategory\Field\ImageExt;
use Crud\SiteFAQCategory\Field\Name;
use Crud\SiteFAQCategory\Field\SiteId;
use Exception\LogicException;
use Model\Cms\Map\SiteFAQCategoryTableMap;
use Model\Cms\SiteFAQCategory;
use Model\Cms\SiteFAQCategoryQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFAQCategory instead if you need to override or add functionality.
 */
abstract class CrudSiteFAQCategoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFAQCategoryQuery::create();
	}


	public function getTableMap(): SiteFAQCategoryTableMap
	{
		return new SiteFAQCategoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFAQCategory";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_faq_category toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_faq_category aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'SiteId', 'HasImage', 'ImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'SiteId', 'HasImage', 'ImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFAQCategory
	 */
	public function getModel(array $aData = null): SiteFAQCategory
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFAQCategoryQuery = SiteFAQCategoryQuery::create();
		     $oSiteFAQCategory = $oSiteFAQCategoryQuery->findOneById($aData['id']);
		     if (!$oSiteFAQCategory instanceof SiteFAQCategory) {
		         throw new LogicException("SiteFAQCategory should be an instance of SiteFAQCategory but got something else." . __METHOD__);
		     }
		     $oSiteFAQCategory = $this->fillVo($aData, $oSiteFAQCategory);
		}
		else {
		     $oSiteFAQCategory = new SiteFAQCategory();
		     if (!empty($aData)) {
		         $oSiteFAQCategory = $this->fillVo($aData, $oSiteFAQCategory);
		     }
		}
		return $oSiteFAQCategory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFAQCategory
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFAQCategory
	{
		$oSiteFAQCategory = $this->getModel($aData);


		 if(!empty($oSiteFAQCategory))
		 {
		     $oSiteFAQCategory = $this->fillVo($aData, $oSiteFAQCategory);
		     $oSiteFAQCategory->save();
		 }
		return $oSiteFAQCategory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFAQCategory $oModel
	 * @return SiteFAQCategory
	 */
	protected function fillVo(array $aData, SiteFAQCategory $oModel): SiteFAQCategory
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['has_image'])) {
		     $oField = new HasImage();
		     $mValue = $oField->sanitize($aData['has_image']);
		     $oModel->setHasImage($mValue);
		}
		if(isset($aData['image_ext'])) {
		     $oField = new ImageExt();
		     $mValue = $oField->sanitize($aData['image_ext']);
		     $oModel->setImageExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
