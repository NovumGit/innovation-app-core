<?php
namespace Crud\SiteFAQCategory\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteFAQCategory\ICollectionField as SiteFAQCategoryField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteFAQCategory\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteFAQCategoryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteFAQCategoryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteFAQCategoryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteFAQCategoryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteFAQCategoryField
	{
		return current($this->aFields);
	}
}
