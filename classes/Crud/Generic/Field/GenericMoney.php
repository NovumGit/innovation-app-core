<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\Number;
use Core\Utils;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IFilterableField;
use Exception\InvalidArgumentException;
use Model\System\DataModel\DataType;

abstract class GenericMoney extends GenericFloat implements InterfaceGenericFieldType, IFilterableField
{
    public static function getApiDescription(): string
    {
        return 'Accepts floating point values with a dot as the decimal separator.';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::DECIMAL);
    }

    public static function getGenericLabel(): string
    {
        return "Money amount";
    }

    public function getDataType(): string
    {
        return 'money';
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new Number();
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__);
        }
        $sValue = Utils::priceDisplay($mData->{$this->sGetter}());

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $sValue, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
    // implement me later
}
