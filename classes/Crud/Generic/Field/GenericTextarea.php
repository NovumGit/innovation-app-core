<?php

namespace Crud\Generic\Field;

use Cassandra\Exception\InvalidArgumentException;
use Crud\Generic\InterfaceGenericFieldType;
use Model\System\DataModel\DataType;

abstract class GenericTextarea extends GenericEditTextfield implements InterfaceGenericFieldType
{
    static function getApiDescription(): string
    {
        return 'Accepts any string or longer pieces of text.';
    }

    static function getGenericLabel(): string
    {
        return "A multiline text (textarea)";
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $aArguments = [];

        return $this->editTextArea($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly, $aArguments, $this->sPlaceHolder);
    }

    public function getDataType(): string
    {
        return 'varchar';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }
}
