<?php

namespace Crud\Generic\Field;

use Core\Config;
use Crud\Generic\InterfaceGenericFieldType;
use DateTime;
use InvalidArgumentException;
use Model\System\DataModel\DataType;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

abstract class GenericDateTime extends GenericEditTextfield implements InterfaceGenericFieldType
{
    public function getDataType(): string
    {
        return 'datetime';
    }

    public static function getGenericLabel(): string
    {
        return "Date + time";
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::TIMESTAMP);
    }

    public static function getApiDescription(): string
    {
        return 'The date as a unix timetamp for input or date + time (e.g. YYYY-MM-DD HH:MM:SS). output is in the format of ' . Config::getDateTimeFormat();
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $oValue = $oModelObject->{$this->sGetter}();

        $sValue = '';
        if ($oValue instanceof DateTime) {
            $sValue = $oValue->format(Config::getDateTimeFormat());
        }

        return '<td class="">' . $sValue . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__ . ' ' . get_class($this));
        }
        return $this->editDateTimePicker($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
    }
}
