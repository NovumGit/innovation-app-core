<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\StringType;
use Exception;
use Model\System\DataModel\DataType;

abstract class GenericColor extends GenericEditTextfield
{
    // implement me later

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public static function getGenericLabel(): string
    {
        return "Color picker";
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    public function getDataType(): string
    {
        return 'string';
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new StringType();
    }

    public static function getApiDescription(): string
    {
        return 'Any textual values, by default no validations are applied';
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new Exception("Expected an instance of $this->sFqModelClassname in " . __METHOD__ . ' ' . get_class($this));
        }
        return $this->editColorField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
