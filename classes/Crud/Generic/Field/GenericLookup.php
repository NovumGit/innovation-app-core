<?php

namespace Crud\Generic\Field;

use AdminModules\Generic\Crud\Field\Item\Configurators\Lookup\Config;
use Hurah\Types\Type\Url;
use Core\Reflector;
use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\Integer;
use Core\Utils;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IConfigurableFieldItem;
use InvalidArgumentException;
use LogicException;
use Model\Setting\CrudManager\CrudEditorBlockField;

abstract class GenericLookup extends GenericEditTextfield implements InterfaceGenericFieldType, IConfigurableFieldItem
{
    protected $sFieldName = null;
    protected $sFieldLabel = null;
    protected $sIcon = 'tag';
    protected $sGetter = null;

    abstract public function getLookups($iSelectedItem = null);

    abstract public function getVisibleValue($iItemId);

    public function getFieldTitle()
    {

        $oConfig = $this->getItemConfig();
        if ($oConfig && $sTitle = $oConfig->getTitle()) {
            return $sTitle;
        }
        return $this->sFieldLabel;
    }

    protected function filterLookups(array $aOptions): array
    {

        $oConfig = $this->getItemConfig();
        if ($oConfig && $aAllowedOptions = $oConfig->getAllowedOptions()) {
            $aFilteredOptions = [];
            foreach ($aOptions as $aOption) {
                if (in_array($aOption['id'], $aAllowedOptions)) {
                    $aFilteredOptions[] = $aOption;
                }
            }
            $aOptions = $aFilteredOptions;
        }
        return $aOptions;
    }

    public static function getGenericLabel(): string
    {
        return "Dropdown menu";
    }

    public static function getApiDescription(): string
    {
        return 'This field can only contain a fixed set of variables and the system validates against them.';
    }

    public function getDataType(): string
    {
        return 'lookup';
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new Integer();
    }

    public function getItemConfig(): ?Config
    {
        if ($oCrudEditorBlockField = $this->getArgument('crud_editor_block_field')) {
            return new Config($oCrudEditorBlockField);
        }
        return null;
    }

    private function getVisibleValueOrDefault($oModelObject): string
    {
        $iValue = $this->getCurrentOrDefaultValue($oModelObject);
        return $this->getVisibleValue($iValue);
    }

    function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject, got  " . (new Reflector($oModelObject))->getName() . ' in ' . (new Reflector($this))->getName() . '. ' . __METHOD__);
        }

        return '<td class="">' . $this->getVisibleValueOrDefault($oModelObject) . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $bIsHidden = false;
        if ($oConfig = $this->getItemConfig()) {
            if ($mData->isNew()) {
                // We willen readonly alleen aanpassen als readonly=true. We willen hem niet op false zetten als hij
                // als parameter op true stond.
                if ($oConfig->getReadOnlyOnNew() === true) {
                    $bReadonly = true;
                }
                if ($oConfig->getHideFieldOnNew()) {
                    $bIsHidden = true;
                }
            } else {
                // We willen readonly alleen aanpassen als readonly=true. We willen hem niet op false zetten als hij
                // als parameter op true stond.
                if ($oConfig->getReadOnlyOnEdit() === true) {
                    $bReadonly = $oConfig->getReadOnlyOnEdit();
                }
                if ($oConfig->getHideFieldOnEdit()) {
                    $bIsHidden = true;
                }
            }

            if ($oConfig->getWriteOnceReadMany() && ($mData->{$this->sGetter}() || $mData->isNew())) {
                $bReadonly = true;
            }
        }

        if (empty($this->sGetter)) {
            throw new LogicException("sGetter is not set in " . get_class($this));
        }

        $mCurrentValue = $this->getCurrentOrDefaultValue($mData);
        if ($bIsHidden) {
            return $this->editHiddenField($this->sFieldName, $mCurrentValue);
        } else {
            $aOptions = $this->getLookups($mCurrentValue ?? null);
            return $this->editLookupField($this->getFieldTitle(), $this->sFieldName, $aOptions, $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
        }
    }
}
