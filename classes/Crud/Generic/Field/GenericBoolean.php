<?php

namespace Crud\Generic\Field;

use Core\Translate;
use Crud\Generic\InterfaceGenericFieldType;
use Exception\InvalidArgumentException;
use Model\System\Base\DataTypeQuery;
use Model\System\DataModel\DataType;
use Propel\Generator\Model\PropelTypes;

abstract class GenericBoolean extends GenericEditTextfield implements InterfaceGenericFieldType
{

    static function getGenericLabel(): string
    {
        return "Boolean (yes or no)";
    }

    function getDataType(): string
    {
        return 'boolean';
    }

    static function getPropelType(): DataType
    {
        return DataType::get(DataType::BOOLEAN);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}() ? Translate::fromCode('Ja') : Translate::fromCode('Nee');
        return '<td class="">' . $sValue . '</td>';
    }

    public function getEditHtml($oModelObject, $bReadonly)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of  $oModelObject {$this->sGetter} in " . __METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
