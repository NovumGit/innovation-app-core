<?php

namespace Crud\Generic\Field;

use Core\Translate;
use Crud\Field;
use Crud\IEventField;
use LogicException;

abstract class GenericEdit extends Field implements IEventField {
    public abstract function getEditUrl($oObject);

    protected $sFieldLabel = 'Bewerken knop';

    public function getFieldTitle() {
        return $this->sFieldLabel;
    }

    public function getIcon() {
        return 'edit';
    }

    public static function getGenericLabel(): string {
        return "Edit button";
    }

    public static function getApiDescription() {
        return Translate::fromCode('Bewerken knop');
    }

    public function hasValidations() {
        return false;
    }

    public function validate($aPostedData) {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader() {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a class="btn btn-default br2 btn-xs">';
        $aOut[] = '        <i class="fa fa-pencil"></i>';
        $aOut[] = '    <a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($oObject) {
        $aOut[] = '<td>';
        $aOut[] = '    <a href="' . $this->getEditUrl($oObject) . '" class="btn btn-dark br2 btn-xs fs12"> <i class="fa fa-pencil"></i> </a>';
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly) {
        throw new LogicException("Edit field should not be there in edit view.");
    }
}
