<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\StringType;
use Crud\Generic\InterfaceGenericFieldType;
use LogicException;
use Crud\Field;
use InvalidArgumentException;
use Model\System\DataModel\DataType;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

abstract class GenericImage extends Field implements InterfaceGenericFieldType
{

    protected $sFieldName = null;
    protected $sFieldLabel = null;
    protected $sIcon = null;
    protected $sPlaceHolder = null;
    protected $sGetter = null;
    protected $sFqModelClassname = null;

    static function getGenericLabel(): string
    {
        return "Image";
    }

    function getDataType(): string
    {
        return 'image';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    function getPrimitive(): InterfaceOasPrimitive
    {
        return new StringType();
    }

    static function getApiDescription(): string
    {
        return 'Generic image uploader field';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function modelTypeIsOk($modelObject): bool
    {
        if ($modelObject instanceof $this->sFqModelClassname) {
            return true;
        }
        return false;
    }

    function isValidModel($oModel)
    {
        if (empty($this->sFqModelClassname)) {
            throw new LogicException("Variable sFqModelClassname must be set in " . get_class($this));
        }
        return $oModel instanceof $this->sFqModelClassname;
    }

    function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    /**
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__ . ' ' . get_class($this));
        }
        return $this->editFileField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
