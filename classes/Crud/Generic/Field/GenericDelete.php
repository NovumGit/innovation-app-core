<?php

namespace Crud\Generic\Field;

use Core\DeferredAction;
use Core\Translate;
use Crud\Field;
use Crud\IEventField;
use LogicException;

abstract class GenericDelete extends Field implements IEventField
{

    abstract function getDeleteUrl($oObject);

    abstract function getUnDeleteUrl($oObject);

    protected $sFieldLabel = 'Verwijderen knop';

    function hasValidations()
    {
        return false;
    }

    function getIcon()
    {
        return 'trash';
    }

    static function getGenericLabel(): string
    {
        return "Delete button";
    }

    static function getApiDescription()
    {
        return Translate::fromCode('Verwijderen knop');
    }

    function getTitle()
    {
        return Translate::fromCode('Verwijderen');
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aProps = $this->getSortProps('is_deleted');
        $sTitle = 'title="' . htmlentities($this->getTitle()) . '"';

        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a ' . $sTitle . ' href="?sort=is_deleted&dir=' . $aProps['new_sort'] . '" class="btn btn-default br2 btn-xs">';
        $aOut[] = '        <i class="fa fa-trash-o"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($oObject)
    {
        DeferredAction::register('after_delete', $_SERVER['REQUEST_URI']);

        $aOut = [];
        $aOut[] = '<td class="">';

        if (method_exists($oObject, 'getIsDeletable') && !$oObject->getIsDeletable()) {
            $aOut[] = '<a href="" title="Dit item kan niet verwijderd worden want dan zou het systeem niet meer goed functioneren." class="btn btn-default br2 btn-xs fs12"> <i class="fa fa-trash-o"></i> </a>';
        } else {
            if (method_exists($oObject, 'getItemDeleted') && $oObject->getItemDeleted()) {
                $aOut[] = '<a href="' . $this->getUnDeleteUrl($oObject) . '" title="' . Translate::fromCode('Terugzetten') . '" class="btn btn-default br2 btn-xs fs12"> <i class="fa fa-undo"></i> </a>';
            } else {
                $aOut[] = '<a href="' . $this->getDeleteUrl($oObject) . '" class="btn btn-danger br2 btn-xs fs12 d"><i class="fa fa-trash-o"></i> </a>';
            }
        }
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
