<?php

namespace Crud\Generic\Field;

use Core\Reflector;
use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\Integer;
use Crud\Generic\InterfaceGenericFieldType;
use InvalidArgumentException;
use LogicException;

abstract class GenericForeignKey extends GenericLookup implements InterfaceGenericFieldType
{
    public static function getGenericLabel(): string
    {
        return "Relation to another model";
    }

    public static function getApiDescription(): string
    {
        return 'Contains lookups that map to another model.';
    }
}
