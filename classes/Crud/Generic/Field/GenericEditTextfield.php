<?php

namespace Crud\Generic\Field;

use AdminModules\Generic\Crud\Field\Item\Configurators\Text\Config;
use AdminModules\Generic\Crud\Field\Item\IItemConfigContract;
use Hurah\Types\Type\Url;
use Core\Reflector;
use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\StringType;
use Core\Utils;
use Crud\Field;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IConfigurableFieldItem;
use InvalidArgumentException;
use LogicException;
use Model\System\DataModel\DataType;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

abstract class GenericEditTextfield extends Field implements InterfaceGenericFieldType, IConfigurableFieldItem
{

    protected $sFieldName = null;
    protected $sFieldLabel = null;
    protected $sIcon = null;
    protected $sPlaceHolder = null;
    protected $sGetter = null;
    protected $sFqModelClassname = null;

    function getDataType(): string
    {
        return 'text';
    }

    function isConfigurableItem(): bool
    {
        return true;
    }

    function getItemConfig(): ?IItemConfigContract
    {
        if ($oCrudEditorBlockField = $this->getArgument('crud_editor_block_field')) {
            return new Config($oCrudEditorBlockField);
        }
        return null;
    }

    /**
     * This class fills both a generic type role as an actual implementation. getItemConfig must return something that
     * is the same as it's derrived classes so getItemConfig cannot return Config which is nice to have.
     * @return Config|null
     */
    function getTypedItemConfig(): ?Config
    {
        $oConfig = $this->getItemConfig();
        if ($oConfig instanceof Config) {
            return $oConfig;
        }
        return null;
    }

    function getItemConfigUrl(int $iCrudEditorBlockFieldId): Url
    {
        return new Url(Utils::makeUrl('/generic/crud/field/item/configure', ['crud_editor_block_field_id' => $iCrudEditorBlockFieldId]));
    }

    static function getGenericLabel(): string
    {
        return "Text field";
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    function getPrimitive(): InterfaceOasPrimitive
    {
        return new StringType();
    }

    static function getApiDescription(): string
    {
        return 'Accepts any string, suitable for blobs and longer texts';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        $oConfig = $this->getItemConfig();
        if ($oConfig) {
            if ($sTitle = $oConfig->getTitle()) {
                return $sTitle;
            }
        }
        return $this->sFieldLabel;
    }

    function modelTypeIsOk($modelObject): bool
    {
        if ($modelObject instanceof $this->sFqModelClassname) {
            return true;
        }
        return false;
    }

    function isValidModel($oModel)
    {
        if (empty($this->sFqModelClassname)) {
            throw new LogicException("Variable sFqModelClassname must be set in " . get_class($this));
        }
        return $oModel instanceof $this->sFqModelClassname;
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    /**
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            $sClassName = (new Reflector($this))->getName();

            $aBackTrace = debug_backtrace();
            foreach ($aBackTrace as $aItem) {
                echo $aItem['file'] . ' -  ' . $aItem['line'] . '<br>' . PHP_EOL;
            }

            throw new InvalidArgumentException("Expected an instance of ModelObject: " . $sClassName . " got " . (new Reflector($oModelObject))->getName() . ' in ' . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    protected function getCurrentOrDefaultValue($oModelObject)
    {
        $mCurrentValue = $oModelObject->{$this->sGetter}();
        if ($mCurrentValue) {
            return $mCurrentValue;
        }
        $mCurrentValue = null;

        if ($oModelObject->isNew() && $oConfig = $this->getItemConfig()) {
            $mCurrentValue = $oConfig->getDefaultValue();
        }
        return $mCurrentValue;
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getEditHtml($mData, $bReadonly)
    {

        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__ . ' ' . get_class($this));
        }

        $mValue = $this->getCurrentOrDefaultValue($mData);
        $aParameters = [];

        $oItemConfig = $this->getTypedItemConfig();

        if ($sText = $oItemConfig->getInfoHelpText()) {
            $aParameters = ['info_help_text' => $sText];
        }
        return $this->editTextField($this->getFieldTitle(), $this->sFieldName, $mValue, $this->sPlaceHolder, $this->sIcon, $bReadonly, $aParameters);
    }
}
