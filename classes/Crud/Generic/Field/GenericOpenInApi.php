<?php

namespace Crud\Generic\Field;

use Crud\Field;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IEventField;
use Crud\IFieldHasApi;
use Exception\LogicException;

abstract class GenericOpenInApi extends Field implements IEventField, IFieldHasApi, InterfaceGenericFieldType
{

    protected $sFieldLabel = 'Openen in APi / als json';

    // Generated in a CrudApiTrait class
    abstract public function getDocumentationUrl(): string;

    abstract public function getApiUrl(): string;

    abstract public function getApiNamespace(): string;

    abstract public function getApiVersion(): string;

    // Implemented by extending class
    abstract public function getModule(): string;

    abstract public function getModuleDir(): string;

    static function getApiDescription(): string
    {
        return 'An automatically generated link to this record in the API';
    }

    static function getGenericLabel(): string
    {
        return "Open in API";
    }

    function getIcon()
    {
        return 'code';
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() . '"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($mData)
    {
        $sId = null;

        if (method_exists($mData, 'getId')) {
            $sId = $mData->getId();
        }

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = '    <a title="Bekijk in API" target="_blank" target="_blank" href="' . $this->getApiUrl() . '/v' . $this->getApiVersion() . '/rest/' . strtolower($this->getModuleDir()) . '/' . $sId . '" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '         <i class="fa fa-code"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
