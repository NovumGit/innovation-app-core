<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\Integer;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IFilterableField;
use Model\System\DataModel\DataType;

abstract class GenericInteger extends GenericEditTextfield implements InterfaceGenericFieldType, IFilterableField
{

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::INTEGER);
    }

    public static function getGenericLabel(): string
    {
        return "Round number";
    }

    public function getDataType(): string
    {
        return 'number';
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new Integer();
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public static function getApiDescription(): string
    {
        return 'Accepts whole numbers.';
    }
}
