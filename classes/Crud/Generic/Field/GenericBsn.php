<?php

namespace Crud\Generic\Field;

use Crud\Generic\InterfaceGenericFieldType;
use Model\System\DataModel\DataType;

abstract class GenericBsn extends GenericEditTextfield implements InterfaceGenericFieldType
{
    public static function getGenericLabel(): string
    {
        return "Dutch BSN";
    }

    public function getDataType(): string
    {
        return 'bsn';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    public static function getApiDescription(): string
    {
        return 'This validates a dutch burger service number.';
    }
}
