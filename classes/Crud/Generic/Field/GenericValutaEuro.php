<?php

namespace Crud\Generic\Field;

use Core\Utils;
use Crud\Generic\InterfaceGenericFieldType;
use Exception\InvalidArgumentException;
use Model\System\DataModel\DataType;

abstract class GenericValutaEuro extends GenericMoney implements InterfaceGenericFieldType
{
    function getDataType(): string
    {
        return 'money';
    }

    static function getApiDescription(): string
    {
        return 'Accepts money values in US format, outputs in EU format.';
    }

    static function getGenericLabel(): string
    {
        return "Money value in euro's";
    }

    function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}();

        return '<td class="numeric">&euro; ' . Utils::priceDisplay($sValue) . '</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__);
        }

        $sValue = $mData->{$this->sGetter}();
        $sValue = Utils::priceDisplay($sValue);

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $sValue, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::DECIMAL);
    }
}
