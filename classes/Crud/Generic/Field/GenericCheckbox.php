<?php

namespace Crud\Generic\Field;

use Crud\Field;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IEventField;
use Crud\IField;
use Exception\NullPointerException;
use Model\System\DataModel\DataType;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

class GenericCheckbox extends Field implements IEventField, IField, InterfaceGenericFieldType
{

    protected $sFieldName = 'Multi purpose checkbox';
    protected $sFieldLabel = 'Checkbox';

    public static function getGenericLabel(): string
    {
        return "Checkbox to select items";
    }

    public function getDataType(): string
    {
        return 'boolean';
    }

    public function getIcon()
    {
        return 'checkbox';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::BOOLEAN);
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    /**
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getOverviewHeader()
    {
        return $this->nonSortableHeaderField('<a class="btn btn-default br2 btn-xs"><i class="toggle_check fa fa-edit"></i></a>', 'iconcol');
    }

    public function getOverviewValue($oModelObject)
    {
        if (!method_exists($oModelObject, 'getId')) {
            throw new NullPointerException("Object has no getId method, no way to know item identity");
        }
        return '<td class="">
                    <input class="checkbox" name="items[' . $oModelObject->getId() . ']" value="1" type="checkbox" />
                </td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($mData, $bReadonly)
    {
        return "Checkbox is intended only for overview.";
    }

    public static function getApiDescription(): string
    {
        return 'A multifunctional checkbox that is used in collections of items to select unique items';
    }
}
