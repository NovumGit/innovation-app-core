<?php

namespace Crud\Generic\Field;

use Core\Config;
use Crud\Generic\InterfaceGenericFieldType;
use DateTime;
use InvalidArgumentException;
use Model\System\DataModel\DataType;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

abstract class GenericDate extends GenericEditTextfield implements InterfaceGenericFieldType
{

    public function getDataType(): string
    {
        return 'date';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::DATE);
    }

    public static function getGenericLabel(): string
    {
        return "Date";
    }

    public static function getApiDescription(): string
    {
        return 'The date as a unix timetamp for input, output is in the format of ' . Config::getDateFormat();
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $oValue = $oModelObject->{$this->sGetter}();
        $sValue = null;
        if ($oValue instanceof DateTime) {
            $sValue = $oValue->format(Config::getDateFormat());
        }

        return '<td class="">' . $sValue . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of $this->sFqModelClassname in " . __METHOD__ . ' ' . get_class($this));
        }

        $sValue = null;
        if ($mData instanceof DateTime) {
            $sValue = $mData->format(Config::getDateFormat());
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $sValue, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
