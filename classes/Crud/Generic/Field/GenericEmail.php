<?php

namespace Crud\Generic\Field;

use Core\Translate;
use Crud\Generic\InterfaceGenericFieldType;
use Model\System\DataModel\DataType;

abstract class GenericEmail extends GenericEditTextfield implements InterfaceGenericFieldType
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        if (!filter_var($aPostedData[$this->sFieldName], FILTER_VALIDATE_EMAIL)) {
            $mResponse = [Translate::fromCode('Ongeldig e-mail adres')];
        }
        return $mResponse;
    }

    public static function getGenericLabel(): string
    {
        return "Email address";
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    public static function getApiDescription(): string
    {
        return 'This validates e-mail addresses against the syntax in RFC 822.';
    }
}
