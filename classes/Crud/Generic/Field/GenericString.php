<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\StringType;
use Model\System\DataModel\DataType;

abstract class GenericString extends GenericEditTextfield
{
    // implement me later

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function getDataType(): string
    {
        return 'string';
    }

    public static function getGenericLabel(): string
    {
        return "String / text field";
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new StringType();
    }

    public static function getApiDescription(): string
    {
        return 'Any textual values, by default no validations are applied';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }
}
