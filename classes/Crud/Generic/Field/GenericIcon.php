<?php

namespace Crud\Generic\Field;

use Core\Type\InterfaceOasPrimitive;
use Core\Type\Primitive\Integer;
use Core\Utils;
use Crud\Generic\InterfaceGenericFieldType;
use InvalidArgumentException;
use LogicException;
use Model\System\DataModel\DataType;
use Model\System\UI\Icon;
use Model\System\UI\IconQuery;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

abstract class GenericIcon extends GenericEditTextfield implements InterfaceGenericFieldType
{
    protected $sFieldName = null;
    protected $sFieldLabel = null;
    protected $sIcon = 'tag';
    protected $sGetter = null;

    public static function getGenericLabel(): string
    {
        return "Icon picker";
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }

    public function getLookups($iSelectedItem = null)
    {
        $aIcons = IconQuery::create()->find();
        return Utils::makeSelectOptions($aIcons, 'getName', $iSelectedItem);
    }

    public function getVisibleValue($iItemId)
    {
        $oIcon = IconQuery::create()->findOneById($iItemId);
        if ($oIcon instanceof Icon) {
            return $oIcon->getName();
        }
        return null;
    }

    public static function getApiDescription(): string
    {
        return 'Contains a list of font awesome icons.';
    }

    public function getDataType(): string
    {
        return 'lookup';
    }

    public function getPrimitive(): InterfaceOasPrimitive
    {
        return new Integer();
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$this->isValidModel($oModelObject)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $this->getVisibleValue($oModelObject->{$this->sGetter}()) . '</td>';
    }

    /**
     * @param $mData
     * @param $bReadonly
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function getEditHtml($mData, $bReadonly)
    {
        if (!$this->isValidModel($mData)) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        $aOptions = $this->getLookups();

        if (empty($this->sGetter)) {
            throw new LogicException("sGetter is not set in " . get_class($this));
        }

        return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
