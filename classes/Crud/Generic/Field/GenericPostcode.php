<?php

namespace Crud\Generic\Field;

use Crud\Generic\InterfaceGenericFieldType;
use Model\System\DataModel\DataType;

abstract class GenericPostcode extends GenericEditTextfield implements InterfaceGenericFieldType
{
    static function getApiDescription(): string
    {
        return 'This validates a dutch postcode format (4 numbers + 2 chars).';
    }

    static function getGenericLabel(): string
    {
        return "Dutch postal code";
    }

    public function getDataType(): string
    {
        return 'postcode';
    }

    public static function getPropelType(): DataType
    {
        return DataType::get(DataType::VARCHAR);
    }
}
