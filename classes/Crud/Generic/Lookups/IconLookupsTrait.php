<?php

namespace Crud\Generic\Lookups;

use Core\Utils;
use Model\System\UI\IconQuery;

trait IconLookupsTrait
{
    public function getLookups($iSelectedItem = null)
    {
        $aIcons = IconQuery::create();
        return Utils::makeSelectOptions($aIcons, 'getName', $iSelectedItem);
    }

    public function getVisibleValue($iItemId)
    {
        $oIconQuery = IconQuery::create();
        $oIcon = $oIconQuery->findOneById($iItemId);
        return $oIcon->getName();
    }
}
