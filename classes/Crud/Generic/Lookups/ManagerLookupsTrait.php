<?php

namespace Crud\Generic\Lookups;

use Core\Reflector;
use Core\Utils;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\IApiExposable;
use Model\System\UI\IconQuery;

trait ManagerLookupsTrait
{
    public function getLookups($iSelectedItem = null)
    {
        $aAllCruds = CrudFactory::getAll();

        $aOut = [];
        foreach ($aAllCruds as $aManager) {
            $oManager = null;
            if (isset($aManager['crud_manager'])) {
                $oManager = $aManager['crud_manager'];
            }
            if ($oManager instanceof IApiExposable) {
                $aOut[] = [
                    'id'       => (new Reflector($oManager))->getName(),
                    'selected' => ($iSelectedItem === (new Reflector($oManager))->getName()) ? 'selected' : '',
                    'label'    => $oManager->getEntityTitle(),
                ];
            }
        }

        return $aOut;
    }

    public function getVisibleValue($iItemId)
    {
        $oIconQuery = IconQuery::create();
        $oIcon = $oIconQuery->findOneById($iItemId);
        return $oIcon->getName();
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        if (!isset($aPostedData['manager']) || empty($aPostedData['manager'])) {
            $mResponse = [];
            $mResponse[] = 'Het veld "Manager" is een verplicht veld';
        }
        if (!empty($mParentResponse)) {
            $mResponse = array_merge($mResponse, $mParentResponse);
        }
        return $mResponse;
    }
}
