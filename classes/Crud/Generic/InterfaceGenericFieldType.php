<?php

namespace Crud\Generic;

use Core\Type\InterfaceOasPrimitive;
use Model\System\DataModel\DataType;

interface InterfaceGenericFieldType
{
    /**
     * Should contain an easy to understand label for this field type to be shown in for instance te data modeller.
     * @return string
     */
    public static function getGenericLabel(): string;

    public static function getApiDescription(): string;

    /**
     * Should return one of the valid open api datatypes.
     * https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md
     * @return InterfaceOasPrimitive
     */
    public function getPrimitive(): InterfaceOasPrimitive;

    public static function getPropelType(): DataType;
}
