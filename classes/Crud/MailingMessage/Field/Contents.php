<?php
namespace Crud\MailingMessage\Field;

use Crud\MailingMessage\Field\Base\Contents as BaseContents;

/**
 * Skeleton subclass for representing contents field from the mailing_message table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class Contents extends BaseContents
{
}
