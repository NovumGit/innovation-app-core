<?php
namespace Crud\MailingMessage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailingMessage\ICollectionField as MailingMessageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailingMessage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailingMessageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailingMessageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailingMessageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailingMessageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailingMessageField
	{
		return current($this->aFields);
	}
}
