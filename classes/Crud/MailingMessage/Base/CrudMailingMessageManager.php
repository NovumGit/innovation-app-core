<?php
namespace Crud\MailingMessage\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailingMessage\FieldIterator;
use Crud\MailingMessage\Field\Contents;
use Crud\MailingMessage\Field\CreatedDate;
use Crud\MailingMessage\Field\MailingId;
use Crud\MailingMessage\Field\Subject;
use Crud\MailingMessage\Field\ToAddress;
use Crud\MailingMessage\Field\ToFullName;
use Exception\LogicException;
use Model\Marketing\MailingMessage;
use Model\Marketing\MailingMessageQuery;
use Model\Marketing\Map\MailingMessageTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailingMessage instead if you need to override or add functionality.
 */
abstract class CrudMailingMessageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingMessageQuery::create();
	}


	public function getTableMap(): MailingMessageTableMap
	{
		return new MailingMessageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailingMessage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing_message toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing_message aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingId', 'ToAddress', 'ToFullName', 'Subject', 'Contents', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingId', 'ToAddress', 'ToFullName', 'Subject', 'Contents', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailingMessage
	 */
	public function getModel(array $aData = null): MailingMessage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingMessageQuery = MailingMessageQuery::create();
		     $oMailingMessage = $oMailingMessageQuery->findOneById($aData['id']);
		     if (!$oMailingMessage instanceof MailingMessage) {
		         throw new LogicException("MailingMessage should be an instance of MailingMessage but got something else." . __METHOD__);
		     }
		     $oMailingMessage = $this->fillVo($aData, $oMailingMessage);
		}
		else {
		     $oMailingMessage = new MailingMessage();
		     if (!empty($aData)) {
		         $oMailingMessage = $this->fillVo($aData, $oMailingMessage);
		     }
		}
		return $oMailingMessage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailingMessage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailingMessage
	{
		$oMailingMessage = $this->getModel($aData);


		 if(!empty($oMailingMessage))
		 {
		     $oMailingMessage = $this->fillVo($aData, $oMailingMessage);
		     $oMailingMessage->save();
		 }
		return $oMailingMessage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailingMessage $oModel
	 * @return MailingMessage
	 */
	protected function fillVo(array $aData, MailingMessage $oModel): MailingMessage
	{
		if(isset($aData['mailing_id'])) {
		     $oField = new MailingId();
		     $mValue = $oField->sanitize($aData['mailing_id']);
		     $oModel->setMailingId($mValue);
		}
		if(isset($aData['to_address'])) {
		     $oField = new ToAddress();
		     $mValue = $oField->sanitize($aData['to_address']);
		     $oModel->setToAddress($mValue);
		}
		if(isset($aData['to_full_name'])) {
		     $oField = new ToFullName();
		     $mValue = $oField->sanitize($aData['to_full_name']);
		     $oModel->setToFullName($mValue);
		}
		if(isset($aData['subject'])) {
		     $oField = new Subject();
		     $mValue = $oField->sanitize($aData['subject']);
		     $oModel->setSubject($mValue);
		}
		if(isset($aData['contents'])) {
		     $oField = new Contents();
		     $mValue = $oField->sanitize($aData['contents']);
		     $oModel->setContents($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
