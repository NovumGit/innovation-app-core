<?php
namespace Crud\Supplier\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Supplier\FieldIterator;
use Crud\Supplier\Field\AccountManagerId;
use Crud\Supplier\Field\City;
use Crud\Supplier\Field\Email;
use Crud\Supplier\Field\Latitude;
use Crud\Supplier\Field\Longitude;
use Crud\Supplier\Field\Name;
use Crud\Supplier\Field\Number;
use Crud\Supplier\Field\OldId;
use Crud\Supplier\Field\Phone;
use Exception\LogicException;
use Model\Supplier\Map\SupplierTableMap;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Supplier instead if you need to override or add functionality.
 */
abstract class CrudSupplierManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SupplierQuery::create();
	}


	public function getTableMap(): SupplierTableMap
	{
		return new SupplierTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Supplier";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "supplier toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "supplier aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Number', 'Name', 'Email', 'Phone', 'City', 'Latitude', 'Longitude', 'AccountManagerId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Number', 'Name', 'Email', 'Phone', 'City', 'Latitude', 'Longitude', 'AccountManagerId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Supplier
	 */
	public function getModel(array $aData = null): Supplier
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSupplierQuery = SupplierQuery::create();
		     $oSupplier = $oSupplierQuery->findOneById($aData['id']);
		     if (!$oSupplier instanceof Supplier) {
		         throw new LogicException("Supplier should be an instance of Supplier but got something else." . __METHOD__);
		     }
		     $oSupplier = $this->fillVo($aData, $oSupplier);
		}
		else {
		     $oSupplier = new Supplier();
		     if (!empty($aData)) {
		         $oSupplier = $this->fillVo($aData, $oSupplier);
		     }
		}
		return $oSupplier;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Supplier
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Supplier
	{
		$oSupplier = $this->getModel($aData);


		 if(!empty($oSupplier))
		 {
		     $oSupplier = $this->fillVo($aData, $oSupplier);
		     $oSupplier->save();
		 }
		return $oSupplier;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Supplier $oModel
	 * @return Supplier
	 */
	protected function fillVo(array $aData, Supplier $oModel): Supplier
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['number'])) {
		     $oField = new Number();
		     $mValue = $oField->sanitize($aData['number']);
		     $oModel->setNumber($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['city'])) {
		     $oField = new City();
		     $mValue = $oField->sanitize($aData['city']);
		     $oModel->setCity($mValue);
		}
		if(isset($aData['latitude'])) {
		     $oField = new Latitude();
		     $mValue = $oField->sanitize($aData['latitude']);
		     $oModel->setLatitude($mValue);
		}
		if(isset($aData['longitude'])) {
		     $oField = new Longitude();
		     $mValue = $oField->sanitize($aData['longitude']);
		     $oModel->setLongitude($mValue);
		}
		if(isset($aData['account_manager_id'])) {
		     $oField = new AccountManagerId();
		     $mValue = $oField->sanitize($aData['account_manager_id']);
		     $oModel->setAccountManagerId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
