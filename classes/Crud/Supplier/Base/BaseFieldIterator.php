<?php
namespace Crud\Supplier\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Supplier\ICollectionField as SupplierField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Supplier\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SupplierField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SupplierField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SupplierField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SupplierField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SupplierField
	{
		return current($this->aFields);
	}
}
