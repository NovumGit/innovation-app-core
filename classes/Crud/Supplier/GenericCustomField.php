<?php
namespace Crud\Supplier;

use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierProperty;
use Model\Supplier\SupplierPropertyQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class GenericCustomField extends Field implements ICustomCrudField
{
    protected $sFieldName = null;
    protected $sFieldLabel = null;
    protected $sIcon = null;
    protected $sPlaceHolder = null;

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }
    function store(ActiveRecordInterface $oObject, array $aData)
    {
        if(!isset($aData[$this->getFieldName()]))
        {
            return null;
        }
        if(!$oObject instanceof Supplier)
        {
            throw new LogicException("Expected an instance of Supplier");
        }

        $oSupplierPropertyQuery = SupplierPropertyQuery::create();
        $oSupplierPropertyQuery->filterByPropertyKey($this->getFieldName());
        $oSupplierPropertyQuery->filterBySupplierId($oObject->getId());
        $oSupplierProperty = $oSupplierPropertyQuery->findOne();

        if(!$oSupplierProperty instanceof SupplierProperty)
        {
            $oSupplierProperty = new SupplierProperty();
            $oSupplierProperty->setPropertyKey($this->getFieldName());
            $oSupplierProperty->setSupplierId($oObject->getId());
        }
        $oSupplierProperty->setPropertyValue($aData[$this->getFieldName()]);
        $oSupplierProperty->save();

    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getFieldTitle(), $this->getFieldName(), $_GET);
    }

    function getOverviewValue($oSupplier)
    {
        if(!$oSupplier instanceof Supplier)
        {
            throw new InvalidArgumentException("Expected an instance of  Supplier in ".__METHOD__);
        }

        $oSupplierPropertyQuery = SupplierPropertyQuery::create();
        $oSupplierPropertyQuery->filterByPropertyKey($this->getFieldName());
        $oSupplierPropertyQuery->filterBySupplierId($oSupplier->getId());
        $oSupplierProperty = $oSupplierPropertyQuery->findOne();

        $sProperty = '';
        if($oSupplierProperty instanceof SupplierProperty)
        {
            $sProperty = $oSupplierProperty->getPropertyValue();
        }

        return '<td class="">'.$sProperty.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Supplier)
        {
            throw new InvalidArgumentException("Expected an instance of Supplier in ".__METHOD__);
        }


        $oSupplierPropertyQuery = SupplierPropertyQuery::create();
        $oSupplierPropertyQuery->filterByPropertyKey($this->getFieldName());
        $oSupplierPropertyQuery->filterBySupplierId($mData->getId());
        $oSupplierProperty = $oSupplierPropertyQuery->findOne();

        $sProperty = '';
        if($oSupplierProperty instanceof SupplierProperty)
        {
            $sProperty = $oSupplierProperty->getPropertyValue();
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $sProperty, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
