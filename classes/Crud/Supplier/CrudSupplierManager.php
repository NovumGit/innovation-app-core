<?php
namespace Crud\Supplier;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Exception\LogicException;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;

class CrudSupplierManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getCreateNewUrl():string
    {
        return '/supplier/edit';
    }
    function getOverviewUrl():string
    {
        return '/supplier/overview';
    }

    function getEntityTitle():string
    {
        return 'leverancier';
    }
    function getNewFormTitle():string
    {
        return 'Leverancier toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Leverancier wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Number',
            'Name',
            'Delete',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Number',
            'Name',
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oSupplier = SupplierQuery::create()->findOneById($aData['id']);
            if(!$oSupplier instanceof Supplier)
            {
                throw new LogicException("Supplier should be an instance of Supplier but got ".get_class($oSupplier)." in ".__METHOD__);
            }
        }
        else
        {
            $oSupplier = new Supplier();
            if(isset($aData['old_id']))
            {
                $oSupplier->setOldId($aData['old_id']);
            }
            if(isset($aData['name']))
            {
                $oSupplier->setName($aData['name']);
            }
            if(isset($aData['number']))
            {
                $oSupplier->setNumber($aData['number']);
            }
        }
        return $oSupplier;
    }

    function store(array $aData = null)
    {
        $oSupplier = $this->getModel($aData);
        if(!empty($oSupplier))
        {
            if(isset($aData['old_id']))
            {
                $oSupplier->setOldId($aData['old_id']);
            }
            if(isset($aData['email']))
            {
                $oSupplier->setEmail($aData['email']);
            }
            if(isset($aData['phone']))
            {
                $oSupplier->setPhone($aData['phone']);
            }
            if(isset($aData['name']))
            {
                $oSupplier->setName($aData['name']);
            }
            if(isset($aData['city']))
            {
                $oSupplier->setCity($aData['city']);
            }
            if(isset($aData['number']))
            {
                $oSupplier->setNumber($aData['number']);
            }
            if(isset($aData['account_manager_id']))
            {
                $oSupplier->setAccountManagerId($aData['account_manager_id']);
            }
            $oSupplier->save();
        }

        // maex_id, hexon_id, height, width,... mogelijk meer
        $this->saveCustomFields($oSupplier, $aData);
        return $oSupplier;
    }
}
