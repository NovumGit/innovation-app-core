<?php
namespace Crud\Supplier\Field;

use Crud\Supplier\Field\Base\Longitude as BaseLongitude;

/**
 * Skeleton subclass for representing longitude field from the supplier table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Longitude extends BaseLongitude
{
}
