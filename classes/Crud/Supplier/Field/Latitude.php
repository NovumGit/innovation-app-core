<?php
namespace Crud\Supplier\Field;

use Crud\Supplier\Field\Base\Latitude as BaseLatitude;

/**
 * Skeleton subclass for representing latitude field from the supplier table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Latitude extends BaseLatitude
{
}
