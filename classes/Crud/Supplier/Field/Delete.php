<?php
namespace Crud\Supplier\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Supplier\Supplier as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }


        return "/supplier/overview?_do=Delete&supplier_id=".$oModelObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}