<?php
namespace Crud\Supplier\Field;

use Core\Translate;
use Core\Utils;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Account\User;
use Model\Account\UserQuery;
use InvalidArgumentException;
use Crud\Field;
use Model\Setting\MasterTable\RoleQuery;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;

class AccountManagerId extends Field implements IFilterableField, IEditableField, IFilterableLookupField
{

    protected $sFieldLabel = 'Accountmanager';
    protected $sFieldName = 'account_manager_id';

    private $sPlaceHolder;
    private $sIcon = 'bolt';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {

        $oSupplier = SupplierQuery::create()->findOneById($iItemId);
        $oUser = $oSupplier->getUser();

        if($oUser instanceof User)
        {
            return $oUser->getFullName();
        }
        return '';
    }

    function hasValidations()
    {
        return true;
    }
	function validate($aPostedData)
	{
		$mResponse = false;

		if(empty($aPostedData['account_manager_id']))
		{
			$mResponse = [];
			$mResponse[] = Translate::fromCode("Accountmanager is een verplicht veld!");
		}

		return $mResponse;
	}

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getFieldLabel(), $this->getFieldName(), $_GET);
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof Supplier)
        {
            throw new InvalidArgumentException("Expected an instance of SiteUsp in " . __METHOD__);
        }

        $sFullName = '';
        if($mData->getUser() instanceof User)
        {
            $sFullName = $mData->getUser()->getFullName();
        }

        return '<td class="">' . $sFullName . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {
        $oRole = RoleQuery::create()->findOneByName('Accountmanager');
        $aUsers = UserQuery::create()->filterByRole($oRole);

        $aDropdownOptions = Utils::makeSelectOptions($aUsers, 'getFullName', $mSelectedItem);
        array_unshift($aDropdownOptions, ['id' => null, 'label' => Translate::fromCode('Maak een keuze')]);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof Supplier)
        {
            throw new InvalidArgumentException("Expected an instance of SiteUsp " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getAccountManagerId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getAccountManagerId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
