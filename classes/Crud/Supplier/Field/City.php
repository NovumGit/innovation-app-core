<?php
namespace Crud\Supplier\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Model\Supplier\Supplier;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

class City extends Field implements IFilterableField, IEditableField, IDisplayableField
{
    private $sFieldName = 'city';
    protected $sFieldLabel = 'Stad / woonplaats';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getCity';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getFieldLabel(), $this->getFieldName(), $_GET);
    }

    function getOverviewValue($oSupplier)
    {
        if(!$oSupplier instanceof Supplier)
            throw new InvalidArgumentException("Expected an instance of  Supplier in ".__METHOD__);

        return '<td class="">'.$oSupplier->{$this->sGetter}().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof Supplier)
        {
            throw new InvalidArgumentException("Expected an instance of Supplier in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}


