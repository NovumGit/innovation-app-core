<?php
namespace Crud\Supplier\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Model\Supplier\Supplier;

class Edit extends GenericEdit{


    function getEditUrl($oSupplier){

        if(!$oSupplier instanceof Supplier)
        {
            throw new LogicException('Expected an instance of Supplier but got '.get_class($oSupplier));
        }

        return "/supplier/edit?id=".$oSupplier->getId();
    }
}