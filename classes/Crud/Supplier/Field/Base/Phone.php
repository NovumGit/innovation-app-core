<?php
namespace Crud\Supplier\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Supplier\ICollectionField;

/**
 * Base class that represents the 'phone' crud field from the 'supplier' table.
 * This class is auto generated and should not be modified.
 */
abstract class Phone extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'phone';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhone';
	protected $sFqModelClassname = '\\\Model\Supplier\Supplier';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
