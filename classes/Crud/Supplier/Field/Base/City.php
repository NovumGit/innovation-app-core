<?php
namespace Crud\Supplier\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Supplier\ICollectionField;

/**
 * Base class that represents the 'city' crud field from the 'supplier' table.
 * This class is auto generated and should not be modified.
 */
abstract class City extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'city';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCity';
	protected $sFqModelClassname = '\\\Model\Supplier\Supplier';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
