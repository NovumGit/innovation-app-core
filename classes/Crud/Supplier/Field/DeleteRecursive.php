<?php
namespace Crud\Supplier\Field;

use Core\DeferredAction;
use Core\Utils;
use Crud\Generic\Field\GenericDelete;
use Model\Supplier\Supplier as ModelObject;
use InvalidArgumentException;

class DeleteRecursive extends GenericDelete{

    protected $sFieldLabel = 'Recursief verwijderen';

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }


        DeferredAction::register('before_delete_recursive_url', Utils::getRequestUri(true));
        return "/supplier/overview?_do=ConfirmDeleteRecursive&supplier_id=".$oModelObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}