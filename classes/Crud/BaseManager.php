<?php

namespace Crud;

use Core\AbstractEvent;
use Core\Cfg;
use Exception\LogicException;
use Model\Logging\Except_log;
use Model\Setting\CrudManager\Base\CrudEditorBlockQuery;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\CrudEditorQuery;
use ReflectionClass;
use ReflectionObject;

abstract class BaseManager implements IBaseManager {
    abstract function getDefaultOverviewFields(bool $addNs = false): ?array;

    protected $sCrudLayoutKey;
    protected $aArguments = [];

    function getDefaultEditFields(bool $bAddNs = false): ?array {
        return null;
    }

    function getCrudLayoutKey() {
        return $this->sCrudLayoutKey ?? self::class;
    }

    /**
     * @param bool $bAddNamespace
     * @return string
     */
    function getManagerClassName($bAddNamespace = false): string {
        try
        {
            if ($bAddNamespace)
            {
                return (new ReflectionClass($this))->getName();
            }
            return (new ReflectionClass($this))->getShortName();
        } catch (\Exception $e)
        {
            Except_log::register($e, true);
        }
    }

    function getAllAvailableActions() {
        $sClassFileName = (new ReflectionObject($this))->getFileName();
        $sClassFileDir = dirname($sClassFileName);
        $aFieldDirs = [];
        $aFieldDirs[] = $sClassFileDir . '/Action/';
        $aFieldDirs[] = Cfg::get('ABSOLUTE_ROOT') . '/classes/Crud/Generic/Action';

        $aFieldDirs[] = $sClassFileDir . '/Action/' . Cfg::get('CUSTOM_NAMESPACE');

        if (Cfg::get('CUSTOM_NAMESPACE') && is_dir($sClassFileDir . '/Action/' . Cfg::get('CUSTOM_NAMESPACE')))
        {
            $aFieldDirs[] = $sClassFileDir . '/Action/' . Cfg::get('CUSTOM_NAMESPACE');
        }

        $aFields = [];
        foreach ($aFieldDirs as $sFieldDir)
        {
            if (!is_dir($sFieldDir))
            {
                continue;
            }

            $rFieldDirHandle = opendir($sFieldDir);

            $sPattern = '/\.php$/';

            while (false !== ($sEntry = readdir($rFieldDirHandle)))
            {

                if (preg_match($sPattern, $sEntry) && !preg_match('/^Abstract/', $sEntry))
                {
                    $aFields[] = preg_replace($sPattern, '', $sEntry);
                }
            }
        }
        return $aFields;
    }

    /**
     * @return array
     */
    function getAllAvailableFields() {
        $sClassFileName = (new ReflectionObject($this))->getFileName();
        $sClassFileDir = dirname($sClassFileName);
        $aFieldDirs = [];
        $aFieldDirs[] = $sClassFileDir . '/Field/';

        if (Cfg::get('CUSTOM_NAMESPACE') && is_dir($sClassFileDir . '/Field/' . Cfg::get('CUSTOM_NAMESPACE')))
        {
            $aFieldDirs[] = $sClassFileDir . '/Field/' . Cfg::get('CUSTOM_NAMESPACE');
        }

        $aFields = [];
        foreach ($aFieldDirs as $sFieldDir)
        {
            if (!$rFieldDirHandle = opendir($sFieldDir))
            {
                throw new LogicException("Field dir niet gevonden voor Crud object");
            }

            $sPattern = '/\.php$/';

            while (false !== ($sEntry = readdir($rFieldDirHandle)))
            {

                if (preg_match($sPattern, $sEntry) && !preg_match('/^Abstract/', $sEntry))
                {
                    $aFields[] = preg_replace($sPattern, '', $sEntry);
                }
            }
        }
        return $aFields;
    }

    function setArgument($sKey, $sValue) {
        $this->aArguments[$sKey] = $sValue;
    }

    function getArgument($sKey) {
        return $this->aArguments[$sKey];
    }

    function getAllArguments() {
        return $this->aArguments;
    }

    public function loadCrudActions(array $aActionNames) {
        $aActions = [];

        $sMyNamespace = (new ReflectionObject($this))->getNamespaceName();

        foreach ($aActionNames as $sFieldName)
        {

            if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($sMyNamespace . "\\Action\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName"))
            {

                $sFullyQualifiedObjectName = $sMyNamespace . "\\Action\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
            } else
            {
                if (class_exists($sFqFieldName = '\\Crud\\Generic\\Action\\' . $sFieldName))
                {
                    $sFullyQualifiedObjectName = $sFqFieldName;
                } else
                {
                    $sFullyQualifiedObjectName = $sMyNamespace . "\\Action\\$sFieldName";
                }
            }

            if (class_implements($sFullyQualifiedObjectName, AbstractEvent::class))
            {

                $aGet = [];
                $aPost = [];

                $aActions[] = [
                    'base_name' => $sFieldName,
                    'fqn'       => $sFullyQualifiedObjectName,
                    'object'    => new $sFullyQualifiedObjectName($aGet, $aPost),
                ];
            }
        }

        return $aActions;
    }


    public function getFieldIterator()
    {

    }

    /**
     * @param $aFieldNames
     * @return CrudElement[]
     */
    public function loadCrudFields(array $aFieldNames) {
        $aFields = [];

        $sMyNamespace = (new ReflectionObject($this))->getNamespaceName();

        foreach ($aFieldNames as $sFieldName)
        {
            if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($sMyNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName"))
            {

                $sFullyQualifiedObjectName = $sMyNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
            } else
            {
                $sFullyQualifiedObjectName = $sMyNamespace . "\\Field\\$sFieldName";
            }

            $oFieldObject = new $sFullyQualifiedObjectName;

            if ($oFieldObject instanceof CrudElement)
            {
                $oFieldObject->setArguments($this->getAllArguments());
                $aFields[] = $oFieldObject;
            } else
            {
                throw new LogicException("Crudfield $sFullyQualifiedObjectName should be an instance of CrudItem in " . __METHOD__ . " on line " . __LINE__);
            }
        }
        if (empty($aFields))
        {
            throw new LogicException("Geen velden gevonden in crud $sMyNamespace");
        }
        return $aFields;
    }

    /**
     * @param $sFormLayoutKey
     * @return array|null
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    function getConfiguredEditFields($sFormLayoutKey) {
        $oCrudConfig = CrudConfigQuery::create()->findOneByManagerName(get_class($this));

        if (!$oCrudConfig instanceof CrudConfig)
        {
            return null;
        }

        $oCrudEditor = CrudEditorQuery::create()->filterByCrudConfigId($oCrudConfig->getId())->filterByName($sFormLayoutKey)->findOne();

        if (empty($oCrudEditor))
        {
            return null;
        }

        $aCrudEditorBlocks = CrudEditorBlockQuery::create()->findByCrudEditorId($oCrudEditor->getId());
        $aCrudEditorFields = CrudEditorBlockFieldQuery::create()->filterByCrudEditorBlock($aCrudEditorBlocks)->find();

        $aEditFields = [];
        $sCustomNamespace = Cfg::get('CUSTOM_NAMESPACE');
        if (!$aCrudEditorFields->isEmpty())
        {
            foreach ($aCrudEditorFields as $oField)
            {

                $sFqCustomName = str_replace('Field', 'Field\\' . $sCustomNamespace, $oField->getField());

                if (class_exists($sFqCustomName))
                {
                    $aEditFields[] = (new ReflectionClass($sFqCustomName))->getShortName();
                } else
                {
                    $aEditFields[] = (new ReflectionClass($oField->getField()))->getShortName();
                }
            }
        }
        return $aEditFields;
    }
}
