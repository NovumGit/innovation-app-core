<?php
namespace Crud\CrudView\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudView\ICollectionField as CrudViewField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudView\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudViewField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudViewField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudViewField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudViewField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudViewField
	{
		return current($this->aFields);
	}
}
