<?php
namespace Crud\CrudView\Base;

use Core\Utils;
use Crud;
use Crud\CrudView\FieldIterator;
use Crud\CrudView\Field\Code;
use Crud\CrudView\Field\CrudConfigId;
use Crud\CrudView\Field\DefaultOrderBy;
use Crud\CrudView\Field\DefaultOrderDir;
use Crud\CrudView\Field\IsHidden;
use Crud\CrudView\Field\Name;
use Crud\CrudView\Field\ShowQuantity;
use Crud\CrudView\Field\Sorting;
use Crud\CrudView\Field\TabColorLogic;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use Model\Setting\CrudManager\Map\CrudViewTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudView instead if you need to override or add functionality.
 */
abstract class CrudCrudViewManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewQuery::create();
	}


	public function getTableMap(): CrudViewTableMap
	{
		return new CrudViewTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint formulierdefinities opgeslagen";
	}


	public function getEntityTitle(): string
	{
		return "CrudView";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudConfigId', 'Name', 'Code', 'Sorting', 'IsHidden', 'ShowQuantity', 'DefaultOrderBy', 'DefaultOrderDir', 'TabColorLogic'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudConfigId', 'Name', 'Code', 'Sorting', 'IsHidden', 'ShowQuantity', 'DefaultOrderBy', 'DefaultOrderDir', 'TabColorLogic'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudView
	 */
	public function getModel(array $aData = null): CrudView
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewQuery = CrudViewQuery::create();
		     $oCrudView = $oCrudViewQuery->findOneById($aData['id']);
		     if (!$oCrudView instanceof CrudView) {
		         throw new LogicException("CrudView should be an instance of CrudView but got something else." . __METHOD__);
		     }
		     $oCrudView = $this->fillVo($aData, $oCrudView);
		}
		else {
		     $oCrudView = new CrudView();
		     if (!empty($aData)) {
		         $oCrudView = $this->fillVo($aData, $oCrudView);
		     }
		}
		return $oCrudView;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudView
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudView
	{
		$oCrudView = $this->getModel($aData);


		 if(!empty($oCrudView))
		 {
		     $oCrudView = $this->fillVo($aData, $oCrudView);
		     $oCrudView->save();
		 }
		return $oCrudView;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudView $oModel
	 * @return CrudView
	 */
	protected function fillVo(array $aData, CrudView $oModel): CrudView
	{
		if(isset($aData['crud_config_id'])) {
		     $oField = new CrudConfigId();
		     $mValue = $oField->sanitize($aData['crud_config_id']);
		     $oModel->setCrudConfigId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['is_hidden'])) {
		     $oField = new IsHidden();
		     $mValue = $oField->sanitize($aData['is_hidden']);
		     $oModel->setIsHidden($mValue);
		}
		if(isset($aData['show_quantity'])) {
		     $oField = new ShowQuantity();
		     $mValue = $oField->sanitize($aData['show_quantity']);
		     $oModel->setShowQuantity($mValue);
		}
		if(isset($aData['default_order_by'])) {
		     $oField = new DefaultOrderBy();
		     $mValue = $oField->sanitize($aData['default_order_by']);
		     $oModel->setDefaultOrderBy($mValue);
		}
		if(isset($aData['default_order_dir'])) {
		     $oField = new DefaultOrderDir();
		     $mValue = $oField->sanitize($aData['default_order_dir']);
		     $oModel->setDefaultOrderDir($mValue);
		}
		if(isset($aData['tab_color_logic'])) {
		     $oField = new TabColorLogic();
		     $mValue = $oField->sanitize($aData['tab_color_logic']);
		     $oModel->setTabColorLogic($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
