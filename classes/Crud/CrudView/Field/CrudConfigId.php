<?php
namespace Crud\CrudView\Field;

use Crud\CrudView\Field\Base\CrudConfigId as BaseCrudConfigId;

/**
 * Skeleton subclass for representing crud_config_id field from the crud_view table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudConfigId extends BaseCrudConfigId
{
}
