<?php
namespace Crud\CrudView\Field\Base;

use Crud\CrudView\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'name' crud field from the 'crud_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class Name extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'name';
	protected $sFieldLabel = 'Name';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getName';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudView';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
