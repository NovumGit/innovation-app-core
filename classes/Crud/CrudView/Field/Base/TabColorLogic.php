<?php
namespace Crud\CrudView\Field\Base;

use Crud\CrudView\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'tab_color_logic' crud field from the 'crud_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class TabColorLogic extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'tab_color_logic';
	protected $sFieldLabel = 'Kleur logica';
	protected $sIcon = 'picture-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTabColorLogic';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudView';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
