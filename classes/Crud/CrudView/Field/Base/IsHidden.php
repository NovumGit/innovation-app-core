<?php
namespace Crud\CrudView\Field\Base;

use Crud\CrudView\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'is_hidden' crud field from the 'crud_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsHidden extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'is_hidden';
	protected $sFieldLabel = 'Is verborgen';
	protected $sIcon = 'sort-numeric-asc';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsHidden';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudView';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['is_hidden']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Is verborgen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
