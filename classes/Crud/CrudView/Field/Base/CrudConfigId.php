<?php
namespace Crud\CrudView\Field\Base;

use Core\Utils;
use Crud\CrudView\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\Setting\CrudManager\CrudConfigQuery;

/**
 * Base class that represents the 'crud_config_id' crud field from the 'crud_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudConfigId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'crud_config_id';
	protected $sFieldLabel = 'Manager';
	protected $sIcon = 'cogs';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudConfigId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudView';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\CrudConfigQuery::create()->orderByManagerName()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getManagerName", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\CrudConfigQuery::create()->findOneById($iItemId)->getManagerName();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['crud_config_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Manager" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
