<?php
namespace Crud\CrudView\Field\Base;

use Crud\CrudView\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'show_quantity' crud field from the 'crud_view' table.
 * This class is auto generated and should not be modified.
 */
abstract class ShowQuantity extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'show_quantity';
	protected $sFieldLabel = 'Aantallen tonen';
	protected $sIcon = 'sort-numeric-asc';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getShowQuantity';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudView';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['show_quantity']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Aantallen tonen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
