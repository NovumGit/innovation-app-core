<?php

namespace Crud;

use Core\Translate;
use Exception\LogicException;
use Model\Logging\Except_log;

/**
 * Represents a unit or field or item that can be configured as a crud. Not every item that is configurable is a Field.
 * Class CrudElement
 * @package Crud
 */
abstract class CrudElement implements ICrudElement
{

    private $sTranslatedTitle = null;
    protected $sFieldLabel;
    protected $aArguments = [];

    function setArguments($aArguments)
    {
        if (!is_array($aArguments))
        {
            throw new LogicException("Arguments must be an array in " . __METHOD__);
        }
        $this->aArguments = array_merge($this->aArguments, $aArguments);
    }

    function getArgument($sKey)
    {
        return $this->aArguments[$sKey] ?? null;
    }

    /**
     * Returns the name of the crud module
     * @return string
     */
    final function getModuleName(): string
    {
        try
        {
            $sFileName = (new \ReflectionClass($this))->getFileName();
            $sFileDir = dirname($sFileName);
            $aParts = array_reverse(explode(DIRECTORY_SEPARATOR, $sFileDir));
            return $aParts[1];
        } catch (\Exception $e)
        {
            Except_log::register($e, true);
        }
    }

    final function needsFullEditorBlock(): bool
    {
        return $this instanceof INeedsFullEditorBlock;
    }

    final function isWeb(): bool
    {
        return $this instanceof IWebOnlyField;
    }

    final function isApp(): bool
    {
        return $this instanceof IAppOnlyField;
    }

    // By default zijn ze allemaa editable
    function isEditorField(): bool
    {
        return true;
    }

    final function isEventField(): bool
    {
        return $this instanceof IEventField;
    }

    function getUntranslatedFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function getTranslatedTitle()
    {
        if ($this->sTranslatedTitle == null)
        {
            $this->sTranslatedTitle = Translate::crudField($this->sFieldLabel, $this);
        }
        return $this->sTranslatedTitle;
    }
}
