<?php
namespace Crud\MailingTemplate\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailingTemplate\ICollectionField as MailingTemplateField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailingTemplate\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailingTemplateField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailingTemplateField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailingTemplateField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailingTemplateField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailingTemplateField
	{
		return current($this->aFields);
	}
}
