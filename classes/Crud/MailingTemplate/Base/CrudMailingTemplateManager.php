<?php
namespace Crud\MailingTemplate\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailingTemplate\FieldIterator;
use Crud\MailingTemplate\Field\Contents;
use Crud\MailingTemplate\Field\CreatedDate;
use Crud\MailingTemplate\Field\Name;
use Exception\LogicException;
use Model\Marketing\MailingTemplate;
use Model\Marketing\MailingTemplateQuery;
use Model\Marketing\Map\MailingTemplateTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailingTemplate instead if you need to override or add functionality.
 */
abstract class CrudMailingTemplateManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingTemplateQuery::create();
	}


	public function getTableMap(): MailingTemplateTableMap
	{
		return new MailingTemplateTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailingTemplate";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing_template toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing_template aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Contents', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Contents', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailingTemplate
	 */
	public function getModel(array $aData = null): MailingTemplate
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingTemplateQuery = MailingTemplateQuery::create();
		     $oMailingTemplate = $oMailingTemplateQuery->findOneById($aData['id']);
		     if (!$oMailingTemplate instanceof MailingTemplate) {
		         throw new LogicException("MailingTemplate should be an instance of MailingTemplate but got something else." . __METHOD__);
		     }
		     $oMailingTemplate = $this->fillVo($aData, $oMailingTemplate);
		}
		else {
		     $oMailingTemplate = new MailingTemplate();
		     if (!empty($aData)) {
		         $oMailingTemplate = $this->fillVo($aData, $oMailingTemplate);
		     }
		}
		return $oMailingTemplate;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailingTemplate
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailingTemplate
	{
		$oMailingTemplate = $this->getModel($aData);


		 if(!empty($oMailingTemplate))
		 {
		     $oMailingTemplate = $this->fillVo($aData, $oMailingTemplate);
		     $oMailingTemplate->save();
		 }
		return $oMailingTemplate;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailingTemplate $oModel
	 * @return MailingTemplate
	 */
	protected function fillVo(array $aData, MailingTemplate $oModel): MailingTemplate
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['contents'])) {
		     $oField = new Contents();
		     $mValue = $oField->sanitize($aData['contents']);
		     $oModel->setContents($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
