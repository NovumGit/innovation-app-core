<?php
namespace Crud;

class ConfigEditPreviewImage
{
    private $imageUrlOrPath;
    private $title = 'Voorbeeld';
    private $hasDeleteLink = false;
    private $width = 300;
    private $deleteUrl;
    private $height;
    private $alt;


    /**
     * @return mixed
     */
    public function getImageUrlOrPath()
    {
        return $this->imageUrlOrPath;
    }

    /**
     * @param mixed $imageUrlOrPath
     */
    public function setImageUrlOrPath($imageUrlOrPath)
    {
        $this->imageUrlOrPath = $imageUrlOrPath;
    }

    /**
     * @return mixed
     */
    public function getHasDeleteLink()
    {
        return $this->hasDeleteLink;
    }

    /**
     * @param mixed $hasDeleteLink
     */
    public function setDeleteUrl($sDeleteUrl)
    {
        if($sDeleteUrl)
        {
            $this->hasDeleteLink = true;
        }
        $this->deleteUrl = $sDeleteUrl;
    }

    /**
     * @param mixed $hasDeleteLink
     */
    public function getDeleteUrl()
    {
        return $this->deleteUrl;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param mixed $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}