<?php
namespace Crud\Component_modal_render_style;

/**
 * Skeleton subclass for representing a Component_modal_render_style.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudComponent_modal_render_styleManager extends Base\CrudComponent_modal_render_styleManager
{
}
