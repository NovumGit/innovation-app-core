<?php
namespace Crud\Component_modal_render_style;

use Crud\Component_modal_render_style\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of Component_modal_render_style crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
