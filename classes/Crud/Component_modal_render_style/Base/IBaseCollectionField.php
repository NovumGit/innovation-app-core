<?php
namespace Crud\Component_modal_render_style\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_modal_render_style\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
