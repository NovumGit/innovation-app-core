<?php
namespace Crud\Component_modal_render_style\Base;

use Core\Utils;
use Crud;
use Crud\Component_modal_render_style\FieldIterator;
use Crud\Component_modal_render_style\Field\ItemKey;
use Crud\Component_modal_render_style\Field\ItemLabel;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Modal\Component_modal_render_style;
use Model\System\LowCode\Modal\Component_modal_render_styleQuery;
use Model\System\LowCode\Modal\Map\Component_modal_render_styleTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_modal_render_style instead if you need to override or add functionality.
 */
abstract class CrudComponent_modal_render_styleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_modal_render_styleQuery::create();
	}


	public function getTableMap(): Component_modal_render_styleTableMap
	{
		return new Component_modal_render_styleTableMap();
	}


	public function getShortDescription(): string
	{
		return "Used for validation and to populate dropdown values.";
	}


	public function getEntityTitle(): string
	{
		return "Component_modal_render_style";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_modal_render_style toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_modal_render_style aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_modal_render_style
	 */
	public function getModel(array $aData = null): Component_modal_render_style
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_modal_render_styleQuery = Component_modal_render_styleQuery::create();
		     $oComponent_modal_render_style = $oComponent_modal_render_styleQuery->findOneById($aData['id']);
		     if (!$oComponent_modal_render_style instanceof Component_modal_render_style) {
		         throw new LogicException("Component_modal_render_style should be an instance of Component_modal_render_style but got something else." . __METHOD__);
		     }
		     $oComponent_modal_render_style = $this->fillVo($aData, $oComponent_modal_render_style);
		}
		else {
		     $oComponent_modal_render_style = new Component_modal_render_style();
		     if (!empty($aData)) {
		         $oComponent_modal_render_style = $this->fillVo($aData, $oComponent_modal_render_style);
		     }
		}
		return $oComponent_modal_render_style;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_modal_render_style
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_modal_render_style
	{
		$oComponent_modal_render_style = $this->getModel($aData);


		 if(!empty($oComponent_modal_render_style))
		 {
		     $oComponent_modal_render_style = $this->fillVo($aData, $oComponent_modal_render_style);
		     $oComponent_modal_render_style->save();
		 }
		return $oComponent_modal_render_style;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_modal_render_style $oModel
	 * @return Component_modal_render_style
	 */
	protected function fillVo(array $aData, Component_modal_render_style $oModel): Component_modal_render_style
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_label'])) {
		     $oField = new ItemLabel();
		     $mValue = $oField->sanitize($aData['item_label']);
		     $oModel->setItemLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
