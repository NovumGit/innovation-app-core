<?php
namespace Crud\SiteFAQCategoryTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteFAQCategoryTranslation\ICollectionField;

/**
 * Base class that represents the 'language_id' crud field from the 'site_faq_category_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class LanguageId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'language_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLanguageId';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFAQCategoryTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
