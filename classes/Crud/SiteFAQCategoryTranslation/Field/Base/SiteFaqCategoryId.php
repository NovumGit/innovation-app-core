<?php
namespace Crud\SiteFAQCategoryTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\SiteFAQCategoryTranslation\ICollectionField;

/**
 * Base class that represents the 'site_faq_category_id' crud field from the 'site_faq_category_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class SiteFaqCategoryId extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'site_faq_category_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSiteFaqCategoryId';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFAQCategoryTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['site_faq_category_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
