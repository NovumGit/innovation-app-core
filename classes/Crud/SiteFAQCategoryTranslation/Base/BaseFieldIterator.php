<?php
namespace Crud\SiteFAQCategoryTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteFAQCategoryTranslation\ICollectionField as SiteFAQCategoryTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteFAQCategoryTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteFAQCategoryTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteFAQCategoryTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteFAQCategoryTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteFAQCategoryTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteFAQCategoryTranslationField
	{
		return current($this->aFields);
	}
}
