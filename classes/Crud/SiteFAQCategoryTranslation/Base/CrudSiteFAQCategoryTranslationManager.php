<?php
namespace Crud\SiteFAQCategoryTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFAQCategoryTranslation\FieldIterator;
use Crud\SiteFAQCategoryTranslation\Field\LanguageId;
use Crud\SiteFAQCategoryTranslation\Field\Name;
use Crud\SiteFAQCategoryTranslation\Field\SiteFaqCategoryId;
use Exception\LogicException;
use Model\Cms\Map\SiteFAQCategoryTranslationTableMap;
use Model\Cms\SiteFAQCategoryTranslation;
use Model\Cms\SiteFAQCategoryTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFAQCategoryTranslation instead if you need to override or add functionality.
 */
abstract class CrudSiteFAQCategoryTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFAQCategoryTranslationQuery::create();
	}


	public function getTableMap(): SiteFAQCategoryTranslationTableMap
	{
		return new SiteFAQCategoryTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFAQCategoryTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_faq_category_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_faq_category_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'SiteFaqCategoryId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'SiteFaqCategoryId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFAQCategoryTranslation
	 */
	public function getModel(array $aData = null): SiteFAQCategoryTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFAQCategoryTranslationQuery = SiteFAQCategoryTranslationQuery::create();
		     $oSiteFAQCategoryTranslation = $oSiteFAQCategoryTranslationQuery->findOneById($aData['id']);
		     if (!$oSiteFAQCategoryTranslation instanceof SiteFAQCategoryTranslation) {
		         throw new LogicException("SiteFAQCategoryTranslation should be an instance of SiteFAQCategoryTranslation but got something else." . __METHOD__);
		     }
		     $oSiteFAQCategoryTranslation = $this->fillVo($aData, $oSiteFAQCategoryTranslation);
		}
		else {
		     $oSiteFAQCategoryTranslation = new SiteFAQCategoryTranslation();
		     if (!empty($aData)) {
		         $oSiteFAQCategoryTranslation = $this->fillVo($aData, $oSiteFAQCategoryTranslation);
		     }
		}
		return $oSiteFAQCategoryTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFAQCategoryTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFAQCategoryTranslation
	{
		$oSiteFAQCategoryTranslation = $this->getModel($aData);


		 if(!empty($oSiteFAQCategoryTranslation))
		 {
		     $oSiteFAQCategoryTranslation = $this->fillVo($aData, $oSiteFAQCategoryTranslation);
		     $oSiteFAQCategoryTranslation->save();
		 }
		return $oSiteFAQCategoryTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFAQCategoryTranslation $oModel
	 * @return SiteFAQCategoryTranslation
	 */
	protected function fillVo(array $aData, SiteFAQCategoryTranslation $oModel): SiteFAQCategoryTranslation
	{
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['site_faq_category_id'])) {
		     $oField = new SiteFaqCategoryId();
		     $mValue = $oField->sanitize($aData['site_faq_category_id']);
		     $oModel->setSiteFaqCategoryId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
