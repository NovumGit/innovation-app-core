<?php
namespace Crud\PurchaseInvoice\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\PurchaseInvoice\ICollectionField as PurchaseInvoiceField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\PurchaseInvoice\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PurchaseInvoiceField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PurchaseInvoiceField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PurchaseInvoiceField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PurchaseInvoiceField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PurchaseInvoiceField
	{
		return current($this->aFields);
	}
}
