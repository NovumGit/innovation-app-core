<?php
namespace Crud\PurchaseInvoice\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PurchaseInvoice\FieldIterator;
use Crud\PurchaseInvoice\Field\DueDate;
use Crud\PurchaseInvoice\Field\FilePath;
use Crud\PurchaseInvoice\Field\HasDocument;
use Crud\PurchaseInvoice\Field\InvoiceDate;
use Crud\PurchaseInvoice\Field\IsBooked;
use Crud\PurchaseInvoice\Field\IsPaid;
use Crud\PurchaseInvoice\Field\NeedsManualBooking;
use Crud\PurchaseInvoice\Field\PayDate;
use Crud\PurchaseInvoice\Field\RemoteInvoiceId;
use Crud\PurchaseInvoice\Field\SupplierId;
use Crud\PurchaseInvoice\Field\TotalPayableAmount;
use Exception\LogicException;
use Model\Finance\Map\PurchaseInvoiceTableMap;
use Model\Finance\PurchaseInvoice;
use Model\Finance\PurchaseInvoiceQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PurchaseInvoice instead if you need to override or add functionality.
 */
abstract class CrudPurchaseInvoiceManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PurchaseInvoiceQuery::create();
	}


	public function getTableMap(): PurchaseInvoiceTableMap
	{
		return new PurchaseInvoiceTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PurchaseInvoice";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "purchase_invoice toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "purchase_invoice aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RemoteInvoiceId', 'SupplierId', 'InvoiceDate', 'DueDate', 'PayDate', 'IsPaid', 'TotalPayableAmount', 'HasDocument', 'IsBooked', 'NeedsManualBooking', 'FilePath'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RemoteInvoiceId', 'SupplierId', 'InvoiceDate', 'DueDate', 'PayDate', 'IsPaid', 'TotalPayableAmount', 'HasDocument', 'IsBooked', 'NeedsManualBooking', 'FilePath'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PurchaseInvoice
	 */
	public function getModel(array $aData = null): PurchaseInvoice
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPurchaseInvoiceQuery = PurchaseInvoiceQuery::create();
		     $oPurchaseInvoice = $oPurchaseInvoiceQuery->findOneById($aData['id']);
		     if (!$oPurchaseInvoice instanceof PurchaseInvoice) {
		         throw new LogicException("PurchaseInvoice should be an instance of PurchaseInvoice but got something else." . __METHOD__);
		     }
		     $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
		}
		else {
		     $oPurchaseInvoice = new PurchaseInvoice();
		     if (!empty($aData)) {
		         $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
		     }
		}
		return $oPurchaseInvoice;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PurchaseInvoice
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PurchaseInvoice
	{
		$oPurchaseInvoice = $this->getModel($aData);


		 if(!empty($oPurchaseInvoice))
		 {
		     $oPurchaseInvoice = $this->fillVo($aData, $oPurchaseInvoice);
		     $oPurchaseInvoice->save();
		 }
		return $oPurchaseInvoice;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PurchaseInvoice $oModel
	 * @return PurchaseInvoice
	 */
	protected function fillVo(array $aData, PurchaseInvoice $oModel): PurchaseInvoice
	{
		if(isset($aData['remote_invoice_id'])) {
		     $oField = new RemoteInvoiceId();
		     $mValue = $oField->sanitize($aData['remote_invoice_id']);
		     $oModel->setRemoteInvoiceId($mValue);
		}
		if(isset($aData['supplier_id'])) {
		     $oField = new SupplierId();
		     $mValue = $oField->sanitize($aData['supplier_id']);
		     $oModel->setSupplierId($mValue);
		}
		if(isset($aData['invoice_date'])) {
		     $oField = new InvoiceDate();
		     $mValue = $oField->sanitize($aData['invoice_date']);
		     $oModel->setInvoiceDate($mValue);
		}
		if(isset($aData['due_date'])) {
		     $oField = new DueDate();
		     $mValue = $oField->sanitize($aData['due_date']);
		     $oModel->setDueDate($mValue);
		}
		if(isset($aData['pay_date'])) {
		     $oField = new PayDate();
		     $mValue = $oField->sanitize($aData['pay_date']);
		     $oModel->setPayDate($mValue);
		}
		if(isset($aData['is_paid'])) {
		     $oField = new IsPaid();
		     $mValue = $oField->sanitize($aData['is_paid']);
		     $oModel->setIsPaid($mValue);
		}
		if(isset($aData['total_payable_amount'])) {
		     $oField = new TotalPayableAmount();
		     $mValue = $oField->sanitize($aData['total_payable_amount']);
		     $oModel->setTotalPayableAmount($mValue);
		}
		if(isset($aData['has_document'])) {
		     $oField = new HasDocument();
		     $mValue = $oField->sanitize($aData['has_document']);
		     $oModel->setHasDocument($mValue);
		}
		if(isset($aData['is_booked'])) {
		     $oField = new IsBooked();
		     $mValue = $oField->sanitize($aData['is_booked']);
		     $oModel->setIsBooked($mValue);
		}
		if(isset($aData['needs_manual_booking'])) {
		     $oField = new NeedsManualBooking();
		     $mValue = $oField->sanitize($aData['needs_manual_booking']);
		     $oModel->setNeedsManualBooking($mValue);
		}
		if(isset($aData['file_path'])) {
		     $oField = new FilePath();
		     $mValue = $oField->sanitize($aData['file_path']);
		     $oModel->setFilePath($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
