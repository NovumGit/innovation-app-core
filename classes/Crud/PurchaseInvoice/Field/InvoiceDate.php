<?php
namespace Crud\PurchaseInvoice\Field;

use Crud\PurchaseInvoice\Field\Base\InvoiceDate as BaseInvoiceDate;

/**
 * Skeleton subclass for representing invoice_date field from the purchase_invoice table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class InvoiceDate extends BaseInvoiceDate
{
}
