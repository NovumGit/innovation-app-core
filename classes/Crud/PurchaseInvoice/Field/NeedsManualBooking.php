<?php
namespace Crud\PurchaseInvoice\Field;

use Crud\PurchaseInvoice\Field\Base\NeedsManualBooking as BaseNeedsManualBooking;

/**
 * Skeleton subclass for representing needs_manual_booking field from the purchase_invoice table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class NeedsManualBooking extends BaseNeedsManualBooking
{
}
