<?php
namespace Crud\PurchaseInvoice\Field;

use Crud\PurchaseInvoice\Field\Base\IsPaid as BaseIsPaid;

/**
 * Skeleton subclass for representing is_paid field from the purchase_invoice table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class IsPaid extends BaseIsPaid
{
}
