<?php
namespace Crud\PurchaseInvoice\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\PurchaseInvoice\ICollectionField;

/**
 * Base class that represents the 'is_booked' crud field from the 'purchase_invoice' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsBooked extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_booked';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsBooked';
	protected $sFqModelClassname = '\\\Model\Finance\PurchaseInvoice';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
