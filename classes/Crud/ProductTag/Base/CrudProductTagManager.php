<?php
namespace Crud\ProductTag\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductTag\FieldIterator;
use Crud\ProductTag\Field\ProductId;
use Crud\ProductTag\Field\TagId;
use Exception\LogicException;
use Model\Product\Map\ProductTagTableMap;
use Model\Product\ProductTag;
use Model\Product\ProductTagQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductTag instead if you need to override or add functionality.
 */
abstract class CrudProductTagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductTagQuery::create();
	}


	public function getTableMap(): ProductTagTableMap
	{
		return new ProductTagTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductTag";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_tag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_tag aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['TagId', 'ProductId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['TagId', 'ProductId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductTag
	 */
	public function getModel(array $aData = null): ProductTag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductTagQuery = ProductTagQuery::create();
		     $oProductTag = $oProductTagQuery->findOneById($aData['id']);
		     if (!$oProductTag instanceof ProductTag) {
		         throw new LogicException("ProductTag should be an instance of ProductTag but got something else." . __METHOD__);
		     }
		     $oProductTag = $this->fillVo($aData, $oProductTag);
		}
		else {
		     $oProductTag = new ProductTag();
		     if (!empty($aData)) {
		         $oProductTag = $this->fillVo($aData, $oProductTag);
		     }
		}
		return $oProductTag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductTag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductTag
	{
		$oProductTag = $this->getModel($aData);


		 if(!empty($oProductTag))
		 {
		     $oProductTag = $this->fillVo($aData, $oProductTag);
		     $oProductTag->save();
		 }
		return $oProductTag;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductTag $oModel
	 * @return ProductTag
	 */
	protected function fillVo(array $aData, ProductTag $oModel): ProductTag
	{
		if(isset($aData['tag_id'])) {
		     $oField = new TagId();
		     $mValue = $oField->sanitize($aData['tag_id']);
		     $oModel->setTagId($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
