<?php
namespace Crud\ProductTag\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductTag\ICollectionField as ProductTagField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductTag\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductTagField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductTagField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductTagField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductTagField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductTagField
	{
		return current($this->aFields);
	}
}
