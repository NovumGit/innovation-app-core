<?php
namespace Crud\ProductTag\Field;

use Crud\ProductTag\Field\Base\TagId as BaseTagId;

/**
 * Skeleton subclass for representing tag_id field from the product_tag table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class TagId extends BaseTagId
{
}
