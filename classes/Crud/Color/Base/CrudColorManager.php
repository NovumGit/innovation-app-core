<?php
namespace Crud\Color\Base;

use Core\Utils;
use Crud;
use Crud\Color\FieldIterator;
use Crud\Color\Field\Name;
use Crud\Color\Field\OldId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\Color;
use Model\Setting\MasterTable\ColorQuery;
use Model\Setting\MasterTable\Map\ColorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Color instead if you need to override or add functionality.
 */
abstract class CrudColorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ColorQuery::create();
	}


	public function getTableMap(): ColorTableMap
	{
		return new ColorTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Color";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_color toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_color aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Color
	 */
	public function getModel(array $aData = null): Color
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oColorQuery = ColorQuery::create();
		     $oColor = $oColorQuery->findOneById($aData['id']);
		     if (!$oColor instanceof Color) {
		         throw new LogicException("Color should be an instance of Color but got something else." . __METHOD__);
		     }
		     $oColor = $this->fillVo($aData, $oColor);
		}
		else {
		     $oColor = new Color();
		     if (!empty($aData)) {
		         $oColor = $this->fillVo($aData, $oColor);
		     }
		}
		return $oColor;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Color
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Color
	{
		$oColor = $this->getModel($aData);


		 if(!empty($oColor))
		 {
		     $oColor = $this->fillVo($aData, $oColor);
		     $oColor->save();
		 }
		return $oColor;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Color $oModel
	 * @return Color
	 */
	protected function fillVo(array $aData, Color $oModel): Color
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
