<?php

namespace Crud\Color;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\Color;
use Model\Setting\MasterTable\ColorQuery;
use Exception\LogicException;

class CrudColorManager extends FormManager implements IConfigurableCrud
{

    function getEntityTitle(): string
    {
        return 'kleur';
    }

    function getOverviewUrl(): string
    {
        return '/setting/mastertable/color/overview';
    }

    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/color/edit';
    }

    public function getNewFormTitle(): string
    {
        return 'Kleur toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Kleur wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Edit',
            'Delete',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
        ];
    }

    public function getModel(array $aData = null)
    {
        if (isset($aData['id']) && $aData['id']) {
            $oColorQuery = ColorQuery::create();
            $oColor = $oColorQuery->findOneById($aData['id']);

            if (!$oColor instanceof Color) {
                throw new LogicException("Color should be an instance of Color but got " . get_class($oColor) . " in " . __METHOD__);
            }
        } else {
            $oColor = new Color();

            if (!empty($aData)) {
                $oColor = $this->fillVo($aData, $oColor);
            }
        }

        return $oColor;
    }

    public function store(array $aData = null)
    {
        $oColor = $this->getModel($aData);

        if (!empty($oColor)) {
            $oColor = $this->fillVo($aData, $oColor);

            $oColor->save();
        }
        return $oColor;
    }

    private function fillVo($aData, Color $oColor)
    {
        if (isset($aData['name'])) {
            $oColor->setName($aData['name']);
        }

        return $oColor;
    }
}
