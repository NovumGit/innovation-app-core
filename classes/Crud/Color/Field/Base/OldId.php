<?php
namespace Crud\Color\Field\Base;

use Crud\Color\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'old_id' crud field from the 'mt_color' table.
 * This class is auto generated and should not be modified.
 */
abstract class OldId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'old_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getOldId';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\Color';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
