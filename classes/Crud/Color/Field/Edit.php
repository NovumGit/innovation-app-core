<?php

namespace Crud\Color\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Setting\MasterTable\Color;

class Edit extends GenericEdit
{

    public function getEditUrl($oColor)
    {

        if (!$oColor instanceof Color) {
            throw new InvalidArgumentException('Expected an instance of Color but got ' . get_class($oColor));
        }

        return '/setting/mastertable/color/edit?id=' . $oColor->getId();
    }
}
