<?php

namespace Crud\Color\Field;

use Crud\Color\Field\Base\OldId as BaseOldId;

/**
 * Skeleton subclass for representing old_id field from the mt_color table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class OldId extends BaseOldId
{
}
