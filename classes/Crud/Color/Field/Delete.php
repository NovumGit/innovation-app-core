<?php

namespace Crud\Color\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Color;
use Exception\InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oColor)
    {

        if (!$oColor instanceof Color) {
            throw new InvalidArgumentException('Expected an instance of Color but got ' . get_class($oColor));
        }
        return '/setting/mastertable/color/edit?_do=Delete&id=' . $oColor->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
