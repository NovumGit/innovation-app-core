<?php
namespace Crud\NotificationType\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\NotificationType\FieldIterator;
use Crud\NotificationType\Field\Name;
use Exception\LogicException;
use Model\System\Map\NotificationTypeTableMap;
use Model\System\NotificationType;
use Model\System\NotificationTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify NotificationType instead if you need to override or add functionality.
 */
abstract class CrudNotificationTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return NotificationTypeQuery::create();
	}


	public function getTableMap(): NotificationTypeTableMap
	{
		return new NotificationTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "NotificationType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "notification_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "notification_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return NotificationType
	 */
	public function getModel(array $aData = null): NotificationType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oNotificationTypeQuery = NotificationTypeQuery::create();
		     $oNotificationType = $oNotificationTypeQuery->findOneById($aData['id']);
		     if (!$oNotificationType instanceof NotificationType) {
		         throw new LogicException("NotificationType should be an instance of NotificationType but got something else." . __METHOD__);
		     }
		     $oNotificationType = $this->fillVo($aData, $oNotificationType);
		}
		else {
		     $oNotificationType = new NotificationType();
		     if (!empty($aData)) {
		         $oNotificationType = $this->fillVo($aData, $oNotificationType);
		     }
		}
		return $oNotificationType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return NotificationType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): NotificationType
	{
		$oNotificationType = $this->getModel($aData);


		 if(!empty($oNotificationType))
		 {
		     $oNotificationType = $this->fillVo($aData, $oNotificationType);
		     $oNotificationType->save();
		 }
		return $oNotificationType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param NotificationType $oModel
	 * @return NotificationType
	 */
	protected function fillVo(array $aData, NotificationType $oModel): NotificationType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
