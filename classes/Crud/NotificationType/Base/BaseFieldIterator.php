<?php
namespace Crud\NotificationType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\NotificationType\ICollectionField as NotificationTypeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\NotificationType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param NotificationTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param NotificationTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof NotificationTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(NotificationTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): NotificationTypeField
	{
		return current($this->aFields);
	}
}
