<?php
namespace Crud\ModuleRole\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Crud\ModuleRole\ICollectionField;
use Model\ModuleQuery;

/**
 * Base class that represents the 'module_id' crud field from the 'module_role' table.
 * This class is auto generated and should not be modified.
 */
abstract class ModuleId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'module_id';
	protected $sFieldLabel = 'Module';
	protected $sIcon = 'book';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getModuleId';
	protected $sFqModelClassname = '\\\Model\Module\ModuleRole';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\ModuleQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\ModuleQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['module_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Module" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
