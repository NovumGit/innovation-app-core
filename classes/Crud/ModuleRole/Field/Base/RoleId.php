<?php
namespace Crud\ModuleRole\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Crud\ModuleRole\ICollectionField;
use Model\Setting\MasterTableQuery;

/**
 * Base class that represents the 'role_id' crud field from the 'module_role' table.
 * This class is auto generated and should not be modified.
 */
abstract class RoleId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'role_id';
	protected $sFieldLabel = 'Rol';
	protected $sIcon = 'group';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRoleId';
	protected $sFqModelClassname = '\\\Model\Module\ModuleRole';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\MasterTableQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\MasterTableQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['role_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Rol" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
