<?php
namespace Crud\ModuleRole\Field\Base;

use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ModuleRole\ICollectionField;

/**
 * Base class that represents the 'sorting' crud field from the 'module_role' table.
 * This class is auto generated and should not be modified.
 */
abstract class Sorting extends GenericInteger implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'sorting';
	protected $sFieldLabel = 'Volgorde';
	protected $sIcon = 'sort';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSorting';
	protected $sFqModelClassname = '\\\Model\Module\ModuleRole';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['sorting']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Volgorde" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
