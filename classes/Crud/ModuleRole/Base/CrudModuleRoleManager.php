<?php
namespace Crud\ModuleRole\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ModuleRole\FieldIterator;
use Crud\ModuleRole\Field\ModuleId;
use Crud\ModuleRole\Field\RoleId;
use Crud\ModuleRole\Field\Sorting;
use Exception\LogicException;
use Model\Module\Map\ModuleRoleTableMap;
use Model\Module\ModuleRole;
use Model\Module\ModuleRoleQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ModuleRole instead if you need to override or add functionality.
 */
abstract class CrudModuleRoleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ModuleRoleQuery::create();
	}


	public function getTableMap(): ModuleRoleTableMap
	{
		return new ModuleRoleTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint staat de relatie tussen modules en gebruikersrollen omschreven";
	}


	public function getEntityTitle(): string
	{
		return "ModuleRole";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "module_role toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "module_role aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ModuleId', 'RoleId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ModuleId', 'RoleId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ModuleRole
	 */
	public function getModel(array $aData = null): ModuleRole
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oModuleRoleQuery = ModuleRoleQuery::create();
		     $oModuleRole = $oModuleRoleQuery->findOneById($aData['id']);
		     if (!$oModuleRole instanceof ModuleRole) {
		         throw new LogicException("ModuleRole should be an instance of ModuleRole but got something else." . __METHOD__);
		     }
		     $oModuleRole = $this->fillVo($aData, $oModuleRole);
		}
		else {
		     $oModuleRole = new ModuleRole();
		     if (!empty($aData)) {
		         $oModuleRole = $this->fillVo($aData, $oModuleRole);
		     }
		}
		return $oModuleRole;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ModuleRole
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ModuleRole
	{
		$oModuleRole = $this->getModel($aData);


		 if(!empty($oModuleRole))
		 {
		     $oModuleRole = $this->fillVo($aData, $oModuleRole);
		     $oModuleRole->save();
		 }
		return $oModuleRole;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ModuleRole $oModel
	 * @return ModuleRole
	 */
	protected function fillVo(array $aData, ModuleRole $oModel): ModuleRole
	{
		if(isset($aData['module_id'])) {
		     $oField = new ModuleId();
		     $mValue = $oField->sanitize($aData['module_id']);
		     $oModel->setModuleId($mValue);
		}
		if(isset($aData['role_id'])) {
		     $oField = new RoleId();
		     $mValue = $oField->sanitize($aData['role_id']);
		     $oModel->setRoleId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
