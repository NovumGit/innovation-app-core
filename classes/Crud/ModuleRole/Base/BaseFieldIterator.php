<?php
namespace Crud\ModuleRole\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ModuleRole\ICollectionField as ModuleRoleField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ModuleRole\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ModuleRoleField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ModuleRoleField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ModuleRoleField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ModuleRoleField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ModuleRoleField
	{
		return current($this->aFields);
	}
}
