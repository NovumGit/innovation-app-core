<?php
namespace Crud\Test\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Test\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
