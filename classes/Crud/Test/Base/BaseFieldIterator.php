<?php
namespace Crud\Test\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Test\ICollectionField as TestField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Test\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param TestField[] $aFields */
	private array $aFields = [];


	/**
	 * @param TestField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof TestField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(TestField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): TestField
	{
		return current($this->aFields);
	}
}
