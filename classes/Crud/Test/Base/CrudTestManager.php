<?php
namespace Crud\Test\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Test\FieldIterator;
use Crud\Test\Field\Test;
use Exception\LogicException;
use Map\TestTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;
use Test as Test1;
use TestQuery;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Test instead if you need to override or add functionality.
 */
abstract class CrudTestManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return TestQuery::create();
	}


	public function getTableMap(): TestTableMap
	{
		return new TestTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Test";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "test toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "test aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Test'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Test'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Test
	 */
	public function getModel(array $aData = null): Test1
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oTestQuery = TestQuery::create();
		     $oTest = $oTestQuery->findOneById($aData['id']);
		     if (!$oTest instanceof Test) {
		         throw new LogicException("Test should be an instance of Test but got something else." . __METHOD__);
		     }
		     $oTest = $this->fillVo($aData, $oTest);
		}
		else {
		     $oTest = new Test();
		     if (!empty($aData)) {
		         $oTest = $this->fillVo($aData, $oTest);
		     }
		}
		return $oTest;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Test
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Test1
	{
		$oTest = $this->getModel($aData);


		 if(!empty($oTest))
		 {
		     $oTest = $this->fillVo($aData, $oTest);
		     $oTest->save();
		 }
		return $oTest;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Test $oModel
	 * @return Test
	 */
	protected function fillVo(array $aData, Test1 $oModel): Test1
	{
		if(isset($aData['test'])) {
		     $oField = new Test();
		     $mValue = $oField->sanitize($aData['test']);
		     $oModel->setTest($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
