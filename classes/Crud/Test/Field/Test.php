<?php/*** @unfixed**/
namespace Crud\Test\Field;

use Crud\Test\Field\Base\Test as BaseTest;

/**
 * Skeleton subclass for representing test field from the test table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Test extends BaseTest
{
}
