<?php
namespace Crud\Test\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\Test\ICollectionField;

/**
 * Base class that represents the 'test' crud field from the 'test' table.
 * This class is auto generated and should not be modified.
 */
abstract class Test extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'test';
	protected $sFieldLabel = 'Command';
	protected $sIcon = 'lock';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTest';
	protected $sFqModelClassname = '\\\\\Test';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['test']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Command" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
