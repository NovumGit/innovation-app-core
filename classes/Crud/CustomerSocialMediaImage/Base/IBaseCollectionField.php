<?php
namespace Crud\CustomerSocialMediaImage\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\CustomerSocialMediaImage\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
