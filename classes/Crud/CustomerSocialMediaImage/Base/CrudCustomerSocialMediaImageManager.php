<?php
namespace Crud\CustomerSocialMediaImage\Base;

use Core\Utils;
use Crud;
use Crud\CustomerSocialMediaImage\FieldIterator;
use Crud\CustomerSocialMediaImage\Field\CustomerId;
use Crud\CustomerSocialMediaImage\Field\FileExt;
use Crud\CustomerSocialMediaImage\Field\ImageLocation;
use Crud\CustomerSocialMediaImage\Field\Mime;
use Crud\CustomerSocialMediaImage\Field\OriginalName;
use Crud\CustomerSocialMediaImage\Field\SocialNetwork;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerSocialMediaImage;
use Model\Crm\CustomerSocialMediaImageQuery;
use Model\Crm\Map\CustomerSocialMediaImageTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerSocialMediaImage instead if you need to override or add functionality.
 */
abstract class CrudCustomerSocialMediaImageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerSocialMediaImageQuery::create();
	}


	public function getTableMap(): CustomerSocialMediaImageTableMap
	{
		return new CustomerSocialMediaImageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerSocialMediaImage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_social_media_image toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_social_media_image aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'SocialNetwork', 'ImageLocation', 'OriginalName', 'FileExt', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'SocialNetwork', 'ImageLocation', 'OriginalName', 'FileExt', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerSocialMediaImage
	 */
	public function getModel(array $aData = null): CustomerSocialMediaImage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerSocialMediaImageQuery = CustomerSocialMediaImageQuery::create();
		     $oCustomerSocialMediaImage = $oCustomerSocialMediaImageQuery->findOneById($aData['id']);
		     if (!$oCustomerSocialMediaImage instanceof CustomerSocialMediaImage) {
		         throw new LogicException("CustomerSocialMediaImage should be an instance of CustomerSocialMediaImage but got something else." . __METHOD__);
		     }
		     $oCustomerSocialMediaImage = $this->fillVo($aData, $oCustomerSocialMediaImage);
		}
		else {
		     $oCustomerSocialMediaImage = new CustomerSocialMediaImage();
		     if (!empty($aData)) {
		         $oCustomerSocialMediaImage = $this->fillVo($aData, $oCustomerSocialMediaImage);
		     }
		}
		return $oCustomerSocialMediaImage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerSocialMediaImage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerSocialMediaImage
	{
		$oCustomerSocialMediaImage = $this->getModel($aData);


		 if(!empty($oCustomerSocialMediaImage))
		 {
		     $oCustomerSocialMediaImage = $this->fillVo($aData, $oCustomerSocialMediaImage);
		     $oCustomerSocialMediaImage->save();
		 }
		return $oCustomerSocialMediaImage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerSocialMediaImage $oModel
	 * @return CustomerSocialMediaImage
	 */
	protected function fillVo(array $aData, CustomerSocialMediaImage $oModel): CustomerSocialMediaImage
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['social_network'])) {
		     $oField = new SocialNetwork();
		     $mValue = $oField->sanitize($aData['social_network']);
		     $oModel->setSocialNetwork($mValue);
		}
		if(isset($aData['image_location'])) {
		     $oField = new ImageLocation();
		     $mValue = $oField->sanitize($aData['image_location']);
		     $oModel->setImageLocation($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		if(isset($aData['mime'])) {
		     $oField = new Mime();
		     $mValue = $oField->sanitize($aData['mime']);
		     $oModel->setMime($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
