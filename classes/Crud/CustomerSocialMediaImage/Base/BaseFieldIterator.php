<?php
namespace Crud\CustomerSocialMediaImage\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerSocialMediaImage\ICollectionField as CustomerSocialMediaImageField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerSocialMediaImage\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerSocialMediaImageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerSocialMediaImageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerSocialMediaImageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerSocialMediaImageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerSocialMediaImageField
	{
		return current($this->aFields);
	}
}
