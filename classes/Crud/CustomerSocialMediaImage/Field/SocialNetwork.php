<?php
namespace Crud\CustomerSocialMediaImage\Field;

use Crud\CustomerSocialMediaImage\Field\Base\SocialNetwork as BaseSocialNetwork;

/**
 * Skeleton subclass for representing social_network field from the customer_social_media_image table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class SocialNetwork extends BaseSocialNetwork
{
}
