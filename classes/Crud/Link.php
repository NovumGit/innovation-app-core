<?php
namespace Crud;

abstract class Link extends CrudElement{

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }
    abstract function getLinkUrl();
}
