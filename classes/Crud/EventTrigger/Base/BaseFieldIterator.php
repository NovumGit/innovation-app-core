<?php
namespace Crud\EventTrigger\Base;

use Crud\BaseCrudFieldIterator;
use Crud\EventTrigger\ICollectionField as EventTriggerField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\EventTrigger\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param EventTriggerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param EventTriggerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof EventTriggerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(EventTriggerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): EventTriggerField
	{
		return current($this->aFields);
	}
}
