<?php
namespace Crud\EventTrigger\Base;

use Core\Utils;
use Crud;
use Crud\EventTrigger\FieldIterator;
use Crud\EventTrigger\Field\Code;
use Crud\EventTrigger\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\Event\EventTrigger;
use Model\System\Event\EventTriggerQuery;
use Model\System\Event\Map\EventTriggerTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify EventTrigger instead if you need to override or add functionality.
 */
abstract class CrudEventTriggerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return EventTriggerQuery::create();
	}


	public function getTableMap(): EventTriggerTableMap
	{
		return new EventTriggerTableMap();
	}


	public function getShortDescription(): string
	{
		return "Een lijst met mogelijke event triggers";
	}


	public function getEntityTitle(): string
	{
		return "EventTrigger";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "event_trigger toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "event_trigger aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return EventTrigger
	 */
	public function getModel(array $aData = null): EventTrigger
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oEventTriggerQuery = EventTriggerQuery::create();
		     $oEventTrigger = $oEventTriggerQuery->findOneById($aData['id']);
		     if (!$oEventTrigger instanceof EventTrigger) {
		         throw new LogicException("EventTrigger should be an instance of EventTrigger but got something else." . __METHOD__);
		     }
		     $oEventTrigger = $this->fillVo($aData, $oEventTrigger);
		}
		else {
		     $oEventTrigger = new EventTrigger();
		     if (!empty($aData)) {
		         $oEventTrigger = $this->fillVo($aData, $oEventTrigger);
		     }
		}
		return $oEventTrigger;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return EventTrigger
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): EventTrigger
	{
		$oEventTrigger = $this->getModel($aData);


		 if(!empty($oEventTrigger))
		 {
		     $oEventTrigger = $this->fillVo($aData, $oEventTrigger);
		     $oEventTrigger->save();
		 }
		return $oEventTrigger;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param EventTrigger $oModel
	 * @return EventTrigger
	 */
	protected function fillVo(array $aData, EventTrigger $oModel): EventTrigger
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
