<?php
namespace Crud\EventTrigger\Field;

use Crud\EventTrigger\Field\Base\EventTypeId as BaseEventTypeId;

/**
 * Skeleton subclass for representing event_type_id field from the event_trigger table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EventTypeId extends BaseEventTypeId
{
}
