<?php
namespace Crud\EventTrigger\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\Event\EventTypeQuery;

/**
 * Base class that represents the 'event_type_id' crud field from the 'event_trigger' table.
 * This class is auto generated and should not be modified.
 */
abstract class EventTypeId extends GenericLookup implements IFilterableField, IEditableField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'event_type_id';

	protected $sFieldLabel = 'Event type';

	protected $sIcon = 'edit';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getEventTypeId';

	protected $sFqModelClassname = '\Model\System\Event\EventTrigger';


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\Event\EventTypeQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\Event\EventTypeQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['event_type_id']) || empty($aPostedData['event_type_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Event type" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
