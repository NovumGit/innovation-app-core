<?php
namespace Crud\EventTrigger\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\EventQuery;

/**
 * Base class that represents the 'event_id' crud field from the 'event_trigger' table.
 * This class is auto generated and should not be modified.
 */
abstract class EventId extends GenericLookup implements IFilterableField, IEditableField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'event_id';

	protected $sFieldLabel = 'Event';

	protected $sIcon = 'flash';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getEventId';

	protected $sFqModelClassname = '\Model\System\Event\EventTrigger';


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\EventQuery::create()->orderByexecutable()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getexecutable", $mSelectedItem, "getId");
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\EventQuery::create()->findOneById($iItemId)->getexecutable();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['event_id']) || empty($aPostedData['event_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Event" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
