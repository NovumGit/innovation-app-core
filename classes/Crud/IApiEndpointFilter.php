<?php
namespace Crud;

interface IApiEndpointFilter
{

    /**
     * @param A crudmanager
     * @return true when the manager is in, false when it is out.
     */
    function filter(FormManager $oManager):bool ;
}
