<?php
namespace Crud\CustomerWishlist\Field;

use Crud\CustomerWishlist\Field\Base\CustomerId as BaseCustomerId;

/**
 * Skeleton subclass for representing customer_id field from the customer_wishlist table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class CustomerId extends BaseCustomerId
{
}
