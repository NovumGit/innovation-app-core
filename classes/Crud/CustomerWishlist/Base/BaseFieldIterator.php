<?php
namespace Crud\CustomerWishlist\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerWishlist\ICollectionField as CustomerWishlistField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerWishlist\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerWishlistField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerWishlistField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerWishlistField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerWishlistField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerWishlistField
	{
		return current($this->aFields);
	}
}
