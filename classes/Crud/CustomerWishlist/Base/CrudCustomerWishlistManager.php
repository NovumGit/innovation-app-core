<?php
namespace Crud\CustomerWishlist\Base;

use Core\Utils;
use Crud;
use Crud\CustomerWishlist\FieldIterator;
use Crud\CustomerWishlist\Field\CustomerId;
use Crud\CustomerWishlist\Field\ProductId;
use Crud\CustomerWishlist\Field\Sort;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Category\CustomerWishlist;
use Model\Category\CustomerWishlistQuery;
use Model\Category\Map\CustomerWishlistTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerWishlist instead if you need to override or add functionality.
 */
abstract class CrudCustomerWishlistManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerWishlistQuery::create();
	}


	public function getTableMap(): CustomerWishlistTableMap
	{
		return new CustomerWishlistTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerWishlist";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_wishlist toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_wishlist aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'ProductId', 'Sort'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'ProductId', 'Sort'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerWishlist
	 */
	public function getModel(array $aData = null): CustomerWishlist
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerWishlistQuery = CustomerWishlistQuery::create();
		     $oCustomerWishlist = $oCustomerWishlistQuery->findOneById($aData['id']);
		     if (!$oCustomerWishlist instanceof CustomerWishlist) {
		         throw new LogicException("CustomerWishlist should be an instance of CustomerWishlist but got something else." . __METHOD__);
		     }
		     $oCustomerWishlist = $this->fillVo($aData, $oCustomerWishlist);
		}
		else {
		     $oCustomerWishlist = new CustomerWishlist();
		     if (!empty($aData)) {
		         $oCustomerWishlist = $this->fillVo($aData, $oCustomerWishlist);
		     }
		}
		return $oCustomerWishlist;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerWishlist
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerWishlist
	{
		$oCustomerWishlist = $this->getModel($aData);


		 if(!empty($oCustomerWishlist))
		 {
		     $oCustomerWishlist = $this->fillVo($aData, $oCustomerWishlist);
		     $oCustomerWishlist->save();
		 }
		return $oCustomerWishlist;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerWishlist $oModel
	 * @return CustomerWishlist
	 */
	protected function fillVo(array $aData, CustomerWishlist $oModel): CustomerWishlist
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['sort'])) {
		     $oField = new Sort();
		     $mValue = $oField->sanitize($aData['sort']);
		     $oModel->setSort($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
