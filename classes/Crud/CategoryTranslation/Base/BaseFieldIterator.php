<?php
namespace Crud\CategoryTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CategoryTranslation\ICollectionField as CategoryTranslationField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CategoryTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CategoryTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CategoryTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CategoryTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CategoryTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CategoryTranslationField
	{
		return current($this->aFields);
	}
}
