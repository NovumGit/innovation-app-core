<?php
namespace Crud\CategoryTranslation\Base;

use Core\Utils;
use Crud;
use Crud\CategoryTranslation\FieldIterator;
use Crud\CategoryTranslation\Field\Alt;
use Crud\CategoryTranslation\Field\CategoryId;
use Crud\CategoryTranslation\Field\Description;
use Crud\CategoryTranslation\Field\LanguageId;
use Crud\CategoryTranslation\Field\MetaDescription;
use Crud\CategoryTranslation\Field\Name;
use Crud\CategoryTranslation\Field\Url;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Category\CategoryTranslation;
use Model\Category\CategoryTranslationQuery;
use Model\Category\Map\CategoryTranslationTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CategoryTranslation instead if you need to override or add functionality.
 */
abstract class CrudCategoryTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CategoryTranslationQuery::create();
	}


	public function getTableMap(): CategoryTranslationTableMap
	{
		return new CategoryTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CategoryTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "category_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "category_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CategoryId', 'LanguageId', 'Name', 'Alt', 'Url', 'Description', 'MetaDescription'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CategoryId', 'LanguageId', 'Name', 'Alt', 'Url', 'Description', 'MetaDescription'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CategoryTranslation
	 */
	public function getModel(array $aData = null): CategoryTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCategoryTranslationQuery = CategoryTranslationQuery::create();
		     $oCategoryTranslation = $oCategoryTranslationQuery->findOneById($aData['id']);
		     if (!$oCategoryTranslation instanceof CategoryTranslation) {
		         throw new LogicException("CategoryTranslation should be an instance of CategoryTranslation but got something else." . __METHOD__);
		     }
		     $oCategoryTranslation = $this->fillVo($aData, $oCategoryTranslation);
		}
		else {
		     $oCategoryTranslation = new CategoryTranslation();
		     if (!empty($aData)) {
		         $oCategoryTranslation = $this->fillVo($aData, $oCategoryTranslation);
		     }
		}
		return $oCategoryTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CategoryTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CategoryTranslation
	{
		$oCategoryTranslation = $this->getModel($aData);


		 if(!empty($oCategoryTranslation))
		 {
		     $oCategoryTranslation = $this->fillVo($aData, $oCategoryTranslation);
		     $oCategoryTranslation->save();
		 }
		return $oCategoryTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CategoryTranslation $oModel
	 * @return CategoryTranslation
	 */
	protected function fillVo(array $aData, CategoryTranslation $oModel): CategoryTranslation
	{
		if(isset($aData['category_id'])) {
		     $oField = new CategoryId();
		     $mValue = $oField->sanitize($aData['category_id']);
		     $oModel->setCategoryId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['alt'])) {
		     $oField = new Alt();
		     $mValue = $oField->sanitize($aData['alt']);
		     $oModel->setAlt($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['meta_description'])) {
		     $oField = new MetaDescription();
		     $mValue = $oField->sanitize($aData['meta_description']);
		     $oModel->setMetaDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
