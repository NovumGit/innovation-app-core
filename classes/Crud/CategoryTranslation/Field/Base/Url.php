<?php
namespace Crud\CategoryTranslation\Field\Base;

use Crud\CategoryTranslation\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'url' crud field from the 'category_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class Url extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'url';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUrl';
	protected $sFqModelClassname = '\\\Model\Category\CategoryTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
