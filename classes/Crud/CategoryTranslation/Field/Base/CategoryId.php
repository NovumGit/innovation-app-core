<?php
namespace Crud\CategoryTranslation\Field\Base;

use Crud\CategoryTranslation\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'category_id' crud field from the 'category_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class CategoryId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'category_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCategoryId';
	protected $sFqModelClassname = '\\\Model\Category\CategoryTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
