<?php
namespace Crud\Legal_form_translation;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\LegalFormTranslationQuery;
use Model\Setting\MasterTable\LegalFormTranslation;

class CrudLegal_form_translationManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/legal_form/edit';
    }
    function getEntityTitle():string
    {
        return 'rechtsvorm_vertaling';
    }
    function getNewFormTitle():string{
        return 'Rechtsvorm vertaling toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Rechtsvorm vertaling wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name'
        ];
    }
    function getModel(array $aData = null)
    {
        $oLegalFormTranslation = null;
        if(isset($aData['id']) && $aData['id'])
        {
            $oLegalFormTranslation = LegalFormTranslationQuery::create()->findOneById($aData['id']);
        }
        if(!$oLegalFormTranslation instanceof LegalFormTranslation && isset($aData['language_id']) && isset($aData['legal_form_id'])){

            $oLegalFormTranslation = LegalFormTranslationQuery::create()
                    ->filterByLanguageId($aData['language_id'])
                    ->filterByLegalFormId($aData['legal_form_id'])
                    ->findOne();
        }
        if(!$oLegalFormTranslation instanceof LegalFormTranslation)
        {
            $oLegalFormTranslation = new LegalFormTranslation();
        }
        if(!empty($aData))
        {
            $oLegalFormTranslation = $this->fillVo($aData, $oLegalFormTranslation);
        }
        return $oLegalFormTranslation;
    }
    private function fillVo($aData, LegalFormTranslation $oLegalFormTranslation)
    {
        if(isset($aData['legal_form_id'])) { $oLegalFormTranslation->setLegalFormId($aData['legal_form_id']); }
        if(isset($aData['language_id'])) { $oLegalFormTranslation->setLanguageId($aData['language_id']); }
        if(isset($aData['name'])) { $oLegalFormTranslation->setName($aData['name']); }

        return $oLegalFormTranslation;
    }
    function store(array $aData = null)
    {
        $oLegalForm = $this->getModel($aData);

        if(!empty($oLegalForm))
        {
            $oLegalForm = $this->fillVo($aData, $oLegalForm);
            $oLegalForm->save();
        }
        return $oLegalForm;
    }
}
