<?php
namespace Crud\Legal_form_translation\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\LegalFormTranslation as ModelObject;

class Name extends Field{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Rechtsvorm';
    protected $sIcon = 'legal';
    protected $sPlaceHolder = '';

    function getFieldTitle()
    {
        return 'Rechtsvorm';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}