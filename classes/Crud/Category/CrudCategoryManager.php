<?php
namespace Crud\Category;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Category\Category;
use Model\Category\CategoryQuery;
use Propel\Runtime\Exception\LogicException;

class CrudCategoryManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'category';
    }
    function getOverviewUrl():string
    {
        return '/setting/category/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/category/edit';
    }
    function getNewFormTitle():string{
        return 'Categorie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Categorie wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Description',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'Icon',
            'ImageUploader',
            'Description',
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oCategoryQuery = new CategoryQuery();

            $oCategory = $oCategoryQuery->findOneById($aData['id']);

            if(!$oCategory instanceof Category){
                throw new LogicException("Country should be an instance of Category but got ".get_class($oCategory)." in ".__METHOD__);
            }
        }
        else
        {
            $oCategory = new Category();

            if(!empty($aData))
            {
                $oCategory = $this->fillVo($aData, $oCategory);
            }
        }
        return $oCategory;
    }

    function store(array $aData = null)
    {
        $oCategory = $this->getModel($aData);

        if(!empty($oCategory))
        {
            $oCategory = $this->fillVo($aData, $oCategory);
            $oCategory->save();
        }
        return $oCategory;
    }
    private function fillVo($aData, Category $oCategory)
    {
        if(isset($_FILES['data']) && $_FILES['data']['error']['image_file'] === 0 && $oCategory->getId())
        {
            $sPrevImgFile = '../data/img/category/' . $oCategory->getId() . '.' . $oCategory->getImageExt();
            if (file_exists($sPrevImgFile) && !is_dir($sPrevImgFile))
            {
                unlink($sPrevImgFile);
            }
        }
        if(isset($aData['parent_id'])){$oCategory->setParentId($aData['parent_id']);}
        if(isset($aData['in_bestsellers'])){$oCategory->setInBestsellers($aData['in_bestsellers']);}
        if(isset($aData['name'])){$oCategory->setName($aData['name']);}
        if(isset($aData['description'])){$oCategory->setDescription($aData['description']);}
        if(isset($aData['has_image'])){$oCategory->setHasImage($aData['has_image']);}
        if(isset($aData['icon'])){$oCategory->setIcon($aData['icon']);}

        if(isset($_FILES['data']) && $_FILES['data']['error']['image_file'] === 0)
        {
            $sFileDir = '../data/img/category/';
            if(!is_dir($sFileDir))
            {
                mkdir($sFileDir);
            }
            if(!is_writable($sFileDir))
            {
                throw new \Exception\LogicException($sFileDir.' is not writable.');
            }

            $sExt = pathinfo($_FILES['data']['name']['image_file'], PATHINFO_EXTENSION);
            move_uploaded_file($_FILES['data']['tmp_name']['image_file'], $sFileDir.$oCategory->getId().'.'.$sExt);

            $oCategory->setImageExt($sExt);
            $oCategory->setHasImage(1);
        }
        return $oCategory;
    }
}
