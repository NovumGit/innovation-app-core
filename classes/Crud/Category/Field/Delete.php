<?php
namespace Crud\Category\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Category\Category;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof Category)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/setting/category/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}