<?php
namespace Crud\Category\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Category\Category;


class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof Category)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/setting/category/edit?id='.$oObject->getId();
    }
}