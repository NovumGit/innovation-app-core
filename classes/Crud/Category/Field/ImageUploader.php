<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Category\Field;

use Crud\Field;
use Exception\InvalidArgumentException;

use Model\Category\Category as ModelObject;


class ImageUploader extends Field{

    protected $sFieldName = '';
    protected $sFieldLabel = 'Afbeelding uploaden';
    private $sIcon = 'photo';
    private $sPlaceHolder = '';
    private $sGetter = '';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function isOverviewField(): bool
    {
        return false;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editFileField($this->getTranslatedTitle().' 1920px x 300px', 'image_file', '', $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
