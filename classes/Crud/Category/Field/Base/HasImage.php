<?php
namespace Crud\Category\Field\Base;

use Crud\Category\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'has_image' crud field from the 'category' table.
 * This class is auto generated and should not be modified.
 */
abstract class HasImage extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'has_image';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHasImage';
	protected $sFqModelClassname = '\\\Model\Category\Category';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
