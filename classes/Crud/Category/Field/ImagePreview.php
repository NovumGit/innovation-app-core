<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Category\Field;

use Core\Translate;
use Crud\ConfigEditPreviewImage;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Exception\LogicException;
use Model\Category\Category as ModelObject;


class ImagePreview extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'image_preview';
    protected $sFieldLabel = 'Afbeelding voorbeeld';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function isOverviewField(): bool
    {
        return false;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        throw new LogicException("This field should never arrive in any overview.");
    }
    function getOverviewValue($oModelObject)
    {
        throw new LogicException("This field should never arrive in any overview.");
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oEditPreviewImageConfig = new ConfigEditPreviewImage();
        if($oModelObject->getId() && $oModelObject->getHasImage())
        {
            $oEditPreviewImageConfig->setDeleteUrl('/setting/category/edit?id='.$oModelObject->getId().'&_do=DeleteImage');
            // $oEditPreviewImageConfig->setDeleteTooltipText(Translate::fromCode("Afbeelding"));
        }
        if($oModelObject->getHasImage())
        {


            $oEditPreviewImageConfig->setImageUrlOrPath($oModelObject->getImagePath());
        }
        else
        {
            $oEditPreviewImageConfig->setImageUrlOrPath('/img/no-image-available.svg');
        }

        $oEditPreviewImageConfig->setWidth(200);
        $oEditPreviewImageConfig->setTitle(Translate::fromCode('Voorbeeld'));
        $oEditPreviewImageConfig->setAlt(Translate::fromCode('Voorbeeld'));

        return $this->editImagePreview($oEditPreviewImageConfig);
    }
}
