<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Category\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Exception\LogicException;
use Model\Category\Category as ModelObject;


class HasImage extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'has_image';
    protected $sFieldLabel = 'Heeft afbeelding';
    private $sGetter = 'getHasImage';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function isEditorField(): bool
    {
        return false;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        throw new LogicException('This field is not directly editable');
    }
}
