<?php
namespace Crud\Category\Field;

use Crud\Category\Field\Base\OldId as BaseOldId;

/**
 * Skeleton subclass for representing old_id field from the category table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class OldId extends BaseOldId
{
}
