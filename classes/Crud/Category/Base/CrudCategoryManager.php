<?php
namespace Crud\Category\Base;

use Core\Utils;
use Crud;
use Crud\Category\FieldIterator;
use Crud\Category\Field\Description;
use Crud\Category\Field\HasImage;
use Crud\Category\Field\Icon;
use Crud\Category\Field\ImageExt;
use Crud\Category\Field\InBestsellers;
use Crud\Category\Field\Name;
use Crud\Category\Field\NamePlural;
use Crud\Category\Field\NameSingular;
use Crud\Category\Field\OldId;
use Crud\Category\Field\ParentId;
use Crud\Category\Field\Sorting;
use Crud\Category\Field\Tag;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Category\Category;
use Model\Category\CategoryQuery;
use Model\Category\Map\CategoryTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Category instead if you need to override or add functionality.
 */
abstract class CrudCategoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CategoryQuery::create();
	}


	public function getTableMap(): CategoryTableMap
	{
		return new CategoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Category";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "category toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "category aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Sorting', 'ParentId', 'Tag', 'Name', 'NameSingular', 'NamePlural', 'Description', 'HasImage', 'InBestsellers', 'ImageExt', 'Icon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'Sorting', 'ParentId', 'Tag', 'Name', 'NameSingular', 'NamePlural', 'Description', 'HasImage', 'InBestsellers', 'ImageExt', 'Icon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Category
	 */
	public function getModel(array $aData = null): Category
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCategoryQuery = CategoryQuery::create();
		     $oCategory = $oCategoryQuery->findOneById($aData['id']);
		     if (!$oCategory instanceof Category) {
		         throw new LogicException("Category should be an instance of Category but got something else." . __METHOD__);
		     }
		     $oCategory = $this->fillVo($aData, $oCategory);
		}
		else {
		     $oCategory = new Category();
		     if (!empty($aData)) {
		         $oCategory = $this->fillVo($aData, $oCategory);
		     }
		}
		return $oCategory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Category
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Category
	{
		$oCategory = $this->getModel($aData);


		 if(!empty($oCategory))
		 {
		     $oCategory = $this->fillVo($aData, $oCategory);
		     $oCategory->save();
		 }
		return $oCategory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Category $oModel
	 * @return Category
	 */
	protected function fillVo(array $aData, Category $oModel): Category
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['parent_id'])) {
		     $oField = new ParentId();
		     $mValue = $oField->sanitize($aData['parent_id']);
		     $oModel->setParentId($mValue);
		}
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['name_singular'])) {
		     $oField = new NameSingular();
		     $mValue = $oField->sanitize($aData['name_singular']);
		     $oModel->setNameSingular($mValue);
		}
		if(isset($aData['name_plural'])) {
		     $oField = new NamePlural();
		     $mValue = $oField->sanitize($aData['name_plural']);
		     $oModel->setNamePlural($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['has_image'])) {
		     $oField = new HasImage();
		     $mValue = $oField->sanitize($aData['has_image']);
		     $oModel->setHasImage($mValue);
		}
		if(isset($aData['in_bestsellers'])) {
		     $oField = new InBestsellers();
		     $mValue = $oField->sanitize($aData['in_bestsellers']);
		     $oModel->setInBestsellers($mValue);
		}
		if(isset($aData['image_ext'])) {
		     $oField = new ImageExt();
		     $mValue = $oField->sanitize($aData['image_ext']);
		     $oModel->setImageExt($mValue);
		}
		if(isset($aData['icon'])) {
		     $oField = new Icon();
		     $mValue = $oField->sanitize($aData['icon']);
		     $oModel->setIcon($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
