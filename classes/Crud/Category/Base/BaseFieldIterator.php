<?php
namespace Crud\Category\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Category\ICollectionField as CategoryField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Category\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CategoryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CategoryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CategoryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CategoryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CategoryField
	{
		return current($this->aFields);
	}
}
