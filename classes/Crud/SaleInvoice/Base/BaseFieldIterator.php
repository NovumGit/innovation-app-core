<?php
namespace Crud\SaleInvoice\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleInvoice\ICollectionField as SaleInvoiceField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleInvoice\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleInvoiceField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleInvoiceField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleInvoiceField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleInvoiceField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleInvoiceField
	{
		return current($this->aFields);
	}
}
