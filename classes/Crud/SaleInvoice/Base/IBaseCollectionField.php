<?php
namespace Crud\SaleInvoice\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\SaleInvoice\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
