<?php
namespace Crud\SaleInvoice\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleInvoice\FieldIterator;
use Crud\SaleInvoice\Field\CustomerId;
use Crud\SaleInvoice\Field\DocumentExt;
use Crud\SaleInvoice\Field\DueDate;
use Crud\SaleInvoice\Field\ExternalId;
use Crud\SaleInvoice\Field\ExternalSource;
use Crud\SaleInvoice\Field\HasDocument;
use Crud\SaleInvoice\Field\InvoiceDate;
use Crud\SaleInvoice\Field\InvoiceNumber;
use Crud\SaleInvoice\Field\IsPaid;
use Crud\SaleInvoice\Field\OutstandingAmount;
use Crud\SaleInvoice\Field\RevenueDate;
use Crud\SaleInvoice\Field\TotalPayableAmount;
use Crud\SaleInvoice\Field\VatDate;
use Exception\LogicException;
use Model\Finance\Map\SaleInvoiceTableMap;
use Model\Finance\SaleInvoice;
use Model\Finance\SaleInvoiceQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleInvoice instead if you need to override or add functionality.
 */
abstract class CrudSaleInvoiceManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleInvoiceQuery::create();
	}


	public function getTableMap(): SaleInvoiceTableMap
	{
		return new SaleInvoiceTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleInvoice";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_invoice toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_invoice aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['InvoiceNumber', 'ExternalId', 'ExternalSource', 'CustomerId', 'InvoiceDate', 'RevenueDate', 'VatDate', 'DueDate', 'IsPaid', 'HasDocument', 'DocumentExt', 'TotalPayableAmount', 'OutstandingAmount'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['InvoiceNumber', 'ExternalId', 'ExternalSource', 'CustomerId', 'InvoiceDate', 'RevenueDate', 'VatDate', 'DueDate', 'IsPaid', 'HasDocument', 'DocumentExt', 'TotalPayableAmount', 'OutstandingAmount'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleInvoice
	 */
	public function getModel(array $aData = null): SaleInvoice
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleInvoiceQuery = SaleInvoiceQuery::create();
		     $oSaleInvoice = $oSaleInvoiceQuery->findOneById($aData['id']);
		     if (!$oSaleInvoice instanceof SaleInvoice) {
		         throw new LogicException("SaleInvoice should be an instance of SaleInvoice but got something else." . __METHOD__);
		     }
		     $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
		}
		else {
		     $oSaleInvoice = new SaleInvoice();
		     if (!empty($aData)) {
		         $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
		     }
		}
		return $oSaleInvoice;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleInvoice
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleInvoice
	{
		$oSaleInvoice = $this->getModel($aData);


		 if(!empty($oSaleInvoice))
		 {
		     $oSaleInvoice = $this->fillVo($aData, $oSaleInvoice);
		     $oSaleInvoice->save();
		 }
		return $oSaleInvoice;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleInvoice $oModel
	 * @return SaleInvoice
	 */
	protected function fillVo(array $aData, SaleInvoice $oModel): SaleInvoice
	{
		if(isset($aData['invoice_number'])) {
		     $oField = new InvoiceNumber();
		     $mValue = $oField->sanitize($aData['invoice_number']);
		     $oModel->setInvoiceNumber($mValue);
		}
		if(isset($aData['external_id'])) {
		     $oField = new ExternalId();
		     $mValue = $oField->sanitize($aData['external_id']);
		     $oModel->setExternalId($mValue);
		}
		if(isset($aData['external_source'])) {
		     $oField = new ExternalSource();
		     $mValue = $oField->sanitize($aData['external_source']);
		     $oModel->setExternalSource($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['invoice_date'])) {
		     $oField = new InvoiceDate();
		     $mValue = $oField->sanitize($aData['invoice_date']);
		     $oModel->setInvoiceDate($mValue);
		}
		if(isset($aData['revenue_date'])) {
		     $oField = new RevenueDate();
		     $mValue = $oField->sanitize($aData['revenue_date']);
		     $oModel->setRevenueDate($mValue);
		}
		if(isset($aData['vat_date'])) {
		     $oField = new VatDate();
		     $mValue = $oField->sanitize($aData['vat_date']);
		     $oModel->setVatDate($mValue);
		}
		if(isset($aData['due_date'])) {
		     $oField = new DueDate();
		     $mValue = $oField->sanitize($aData['due_date']);
		     $oModel->setDueDate($mValue);
		}
		if(isset($aData['is_paid'])) {
		     $oField = new IsPaid();
		     $mValue = $oField->sanitize($aData['is_paid']);
		     $oModel->setIsPaid($mValue);
		}
		if(isset($aData['has_document'])) {
		     $oField = new HasDocument();
		     $mValue = $oField->sanitize($aData['has_document']);
		     $oModel->setHasDocument($mValue);
		}
		if(isset($aData['document_ext'])) {
		     $oField = new DocumentExt();
		     $mValue = $oField->sanitize($aData['document_ext']);
		     $oModel->setDocumentExt($mValue);
		}
		if(isset($aData['total_payable_amount'])) {
		     $oField = new TotalPayableAmount();
		     $mValue = $oField->sanitize($aData['total_payable_amount']);
		     $oModel->setTotalPayableAmount($mValue);
		}
		if(isset($aData['outstanding_amount'])) {
		     $oField = new OutstandingAmount();
		     $mValue = $oField->sanitize($aData['outstanding_amount']);
		     $oModel->setOutstandingAmount($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
