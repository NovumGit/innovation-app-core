<?php
namespace Crud\SaleInvoice\Field;

use Crud\SaleInvoice\Field\Base\OutstandingAmount as BaseOutstandingAmount;

/**
 * Skeleton subclass for representing outstanding_amount field from the sale_invoice table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class OutstandingAmount extends BaseOutstandingAmount
{
}
