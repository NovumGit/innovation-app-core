<?php
namespace Crud\SaleInvoice\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleInvoice\ICollectionField;

/**
 * Base class that represents the 'due_date' crud field from the 'sale_invoice' table.
 * This class is auto generated and should not be modified.
 */
abstract class DueDate extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'due_date';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDueDate';
	protected $sFqModelClassname = '\\\Model\Finance\SaleInvoice';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
