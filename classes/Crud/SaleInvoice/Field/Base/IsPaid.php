<?php
namespace Crud\SaleInvoice\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\SaleInvoice\ICollectionField;

/**
 * Base class that represents the 'is_paid' crud field from the 'sale_invoice' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsPaid extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'is_paid';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsPaid';
	protected $sFqModelClassname = '\\\Model\Finance\SaleInvoice';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['is_paid']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
