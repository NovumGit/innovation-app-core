<?php
namespace Crud\CountryShippingMethod\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CountryShippingMethod\ICollectionField as CountryShippingMethodField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CountryShippingMethod\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CountryShippingMethodField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CountryShippingMethodField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CountryShippingMethodField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CountryShippingMethodField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CountryShippingMethodField
	{
		return current($this->aFields);
	}
}
