<?php
namespace Crud\CountryShippingMethod\Base;

use Core\Utils;
use Crud;
use Crud\CountryShippingMethod\FieldIterator;
use Crud\CountryShippingMethod\Field\CountryId;
use Crud\CountryShippingMethod\Field\IsEnabledWebsite;
use Crud\CountryShippingMethod\Field\Price;
use Crud\CountryShippingMethod\Field\ShippingMethodId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\CountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Model\Setting\MasterTable\Map\CountryShippingMethodTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CountryShippingMethod instead if you need to override or add functionality.
 */
abstract class CrudCountryShippingMethodManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CountryShippingMethodQuery::create();
	}


	public function getTableMap(): CountryShippingMethodTableMap
	{
		return new CountryShippingMethodTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CountryShippingMethod";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "country_shipping_method toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "country_shipping_method aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CountryId', 'ShippingMethodId', 'IsEnabledWebsite', 'Price'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CountryId', 'ShippingMethodId', 'IsEnabledWebsite', 'Price'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CountryShippingMethod
	 */
	public function getModel(array $aData = null): CountryShippingMethod
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCountryShippingMethodQuery = CountryShippingMethodQuery::create();
		     $oCountryShippingMethod = $oCountryShippingMethodQuery->findOneById($aData['id']);
		     if (!$oCountryShippingMethod instanceof CountryShippingMethod) {
		         throw new LogicException("CountryShippingMethod should be an instance of CountryShippingMethod but got something else." . __METHOD__);
		     }
		     $oCountryShippingMethod = $this->fillVo($aData, $oCountryShippingMethod);
		}
		else {
		     $oCountryShippingMethod = new CountryShippingMethod();
		     if (!empty($aData)) {
		         $oCountryShippingMethod = $this->fillVo($aData, $oCountryShippingMethod);
		     }
		}
		return $oCountryShippingMethod;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CountryShippingMethod
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CountryShippingMethod
	{
		$oCountryShippingMethod = $this->getModel($aData);


		 if(!empty($oCountryShippingMethod))
		 {
		     $oCountryShippingMethod = $this->fillVo($aData, $oCountryShippingMethod);
		     $oCountryShippingMethod->save();
		 }
		return $oCountryShippingMethod;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CountryShippingMethod $oModel
	 * @return CountryShippingMethod
	 */
	protected function fillVo(array $aData, CountryShippingMethod $oModel): CountryShippingMethod
	{
		if(isset($aData['country_id'])) {
		     $oField = new CountryId();
		     $mValue = $oField->sanitize($aData['country_id']);
		     $oModel->setCountryId($mValue);
		}
		if(isset($aData['shipping_method_id'])) {
		     $oField = new ShippingMethodId();
		     $mValue = $oField->sanitize($aData['shipping_method_id']);
		     $oModel->setShippingMethodId($mValue);
		}
		if(isset($aData['is_enabled_website'])) {
		     $oField = new IsEnabledWebsite();
		     $mValue = $oField->sanitize($aData['is_enabled_website']);
		     $oModel->setIsEnabledWebsite($mValue);
		}
		if(isset($aData['price'])) {
		     $oField = new Price();
		     $mValue = $oField->sanitize($aData['price']);
		     $oModel->setPrice($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
