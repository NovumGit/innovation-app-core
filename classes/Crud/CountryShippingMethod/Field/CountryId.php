<?php
namespace Crud\CountryShippingMethod\Field;

use Crud\CountryShippingMethod\Field\Base\CountryId as BaseCountryId;

/**
 * Skeleton subclass for representing country_id field from the country_shipping_method table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class CountryId extends BaseCountryId
{
}
