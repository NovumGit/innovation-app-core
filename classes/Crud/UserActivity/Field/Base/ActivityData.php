<?php
namespace Crud\UserActivity\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UserActivity\ICollectionField;

/**
 * Base class that represents the 'activity_data' crud field from the 'user_activity' table.
 * This class is auto generated and should not be modified.
 */
abstract class ActivityData extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'activity_data';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getActivityData';
	protected $sFqModelClassname = '\\\Model\User\UserActivity';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
