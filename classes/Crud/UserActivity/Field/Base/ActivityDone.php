<?php
namespace Crud\UserActivity\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UserActivity\ICollectionField;

/**
 * Base class that represents the 'activity_done' crud field from the 'user_activity' table.
 * This class is auto generated and should not be modified.
 */
abstract class ActivityDone extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'activity_done';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getActivityDone';
	protected $sFqModelClassname = '\\\Model\User\UserActivity';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
