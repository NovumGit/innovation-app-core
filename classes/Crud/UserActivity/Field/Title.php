<?php
namespace Crud\UserActivity\Field;

use Crud\UserActivity\Field\Base\Title as BaseTitle;

/**
 * Skeleton subclass for representing title field from the user_activity table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class Title extends BaseTitle
{
}
