<?php
namespace Crud\UserActivity\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UserActivity\ICollectionField as UserActivityField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UserActivity\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UserActivityField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UserActivityField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UserActivityField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UserActivityField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UserActivityField
	{
		return current($this->aFields);
	}
}
