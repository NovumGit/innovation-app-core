<?php
namespace Crud\UserActivity\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UserActivity\FieldIterator;
use Crud\UserActivity\Field\ActivityData;
use Crud\UserActivity\Field\ActivityDone;
use Crud\UserActivity\Field\ActivityTypeId;
use Crud\UserActivity\Field\Title;
use Crud\UserActivity\Field\UserId;
use Exception\LogicException;
use Model\User\Map\UserActivityTableMap;
use Model\User\UserActivity;
use Model\User\UserActivityQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UserActivity instead if you need to override or add functionality.
 */
abstract class CrudUserActivityManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UserActivityQuery::create();
	}


	public function getTableMap(): UserActivityTableMap
	{
		return new UserActivityTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UserActivity";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "user_activity toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "user_activity aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'Title', 'ActivityTypeId', 'ActivityDone', 'ActivityData'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'Title', 'ActivityTypeId', 'ActivityDone', 'ActivityData'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UserActivity
	 */
	public function getModel(array $aData = null): UserActivity
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUserActivityQuery = UserActivityQuery::create();
		     $oUserActivity = $oUserActivityQuery->findOneById($aData['id']);
		     if (!$oUserActivity instanceof UserActivity) {
		         throw new LogicException("UserActivity should be an instance of UserActivity but got something else." . __METHOD__);
		     }
		     $oUserActivity = $this->fillVo($aData, $oUserActivity);
		}
		else {
		     $oUserActivity = new UserActivity();
		     if (!empty($aData)) {
		         $oUserActivity = $this->fillVo($aData, $oUserActivity);
		     }
		}
		return $oUserActivity;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UserActivity
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UserActivity
	{
		$oUserActivity = $this->getModel($aData);


		 if(!empty($oUserActivity))
		 {
		     $oUserActivity = $this->fillVo($aData, $oUserActivity);
		     $oUserActivity->save();
		 }
		return $oUserActivity;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UserActivity $oModel
	 * @return UserActivity
	 */
	protected function fillVo(array $aData, UserActivity $oModel): UserActivity
	{
		if(isset($aData['user_id'])) {
		     $oField = new UserId();
		     $mValue = $oField->sanitize($aData['user_id']);
		     $oModel->setUserId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['activity_type_id'])) {
		     $oField = new ActivityTypeId();
		     $mValue = $oField->sanitize($aData['activity_type_id']);
		     $oModel->setActivityTypeId($mValue);
		}
		if(isset($aData['activity_done'])) {
		     $oField = new ActivityDone();
		     $mValue = $oField->sanitize($aData['activity_done']);
		     $oModel->setActivityDone($mValue);
		}
		if(isset($aData['activity_data'])) {
		     $oField = new ActivityData();
		     $mValue = $oField->sanitize($aData['activity_data']);
		     $oModel->setActivityData($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
