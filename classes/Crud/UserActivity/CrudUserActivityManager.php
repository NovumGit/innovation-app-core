<?php
namespace Crud\UserActivity;

/**
 * Skeleton subclass for representing a UserActivity.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class CrudUserActivityManager extends Base\CrudUserActivityManager
{
}
