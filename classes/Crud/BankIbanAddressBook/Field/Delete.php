<?php
namespace Crud\BankIbanAddressBook\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Crm\Customer;
use InvalidArgumentException;
use Model\Finance\BankIbanAddressBook;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof BankIbanAddressBook)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }
        $sTab = '';
        if(isset($_GET['tab']))
        {
            $sTab = '&tab='.$_GET['tab'];
        }
        return '/finance/bank/iban/alias?_do=Delete&id=' . $oObject->getId() . $sTab;
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}