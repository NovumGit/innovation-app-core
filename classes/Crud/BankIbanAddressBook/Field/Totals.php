<?php
namespace Crud\BankIbanAddressBook\Field;

use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\BankIbanAddressBook as ModelObject;

class Totals extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'totals';
    protected $sFieldLabel = 'Totaal bij + af';
    private $sGetter = 'getTotals';
	private $sIccon = 'money';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $fPrice = $oModelObject->{$this->sGetter}();
        $color = $fPrice > 0 ? 'green' : 'red';


        return '<td style="text-align: right; color:'.$color.';">'
                    . Utils::priceDisplay($fPrice).
                '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sTotals = $oModelObject->getTotals();

	    $sColor = 'black';
        if($sTotals > 0)
        {
	        $sColor = 'green';
        }
        else if($sTotals < 0)
        {
	        $sColor = 'red';
        }


	    $fTotals = Utils::priceDisplay($sTotals);
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, '<span style="color:' . $sColor .'">€ ' . $fTotals . '</span>', 'Totaal van alle transacties', $this->sIccon, true);
    }
}
