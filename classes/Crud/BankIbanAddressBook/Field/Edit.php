<?php
namespace Crud\BankIbanAddressBook\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Crm\Customer;
use Model\Finance\BankIbanAddressBook;

class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof BankIbanAddressBook)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got ' . get_class($oObject));
        }

        return '/finance/bank/iban/alias?id=' . $oObject->getId();
    }
}