<?php
namespace Crud\BankIbanAddressBook;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Finance\BankIbanAddressBook;
use Model\Finance\BankIbanAddressBookQuery;
use Exception\LogicException;

class CrudBankIbanAddressBookManager extends FormManager implements IConfigurableCrud {

    function getEntityTitle():string
    {
        return 'bank_iban_address_book';
    }
    function getOverviewUrl():string
    {
	    return '/finance/bank/iban/addresses';
    }
    function getCreateNewUrl():string
    {
        return '/finance/bank/iban/edit';
    }
    function getNewFormTitle():string{
        return 'IBAN Alias aanmaken';
    }
    function getEditFormTitle():string
    {
        return 'IBAN Alias configureren';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
	    return [
		    'Description',
		    'Iban'
	    ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
	        'Description',
	        'Iban'
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
        	$oBankIbanAddressBookQuery = BankIbanAddressBookQuery::create();
	        $oBankIbanAddressBook = $oBankIbanAddressBookQuery->findOneById($aData['id']);

            if(!$oBankIbanAddressBook instanceof BankIbanAddressBook)
            {
                throw new LogicException("{ should be an instance of BankIbanAddressBook but got ".get_class($oBankIbanAddressBook)." in ".__METHOD__);
            }
        }
        else
        {
	        $oBankIbanAddressBook = new BankIbanAddressBook();
            if(!empty($aData))
            {
	            $oBankIbanAddressBook = $this->fillVo($aData, $oBankIbanAddressBook);
            }
        }
        return $oBankIbanAddressBook;
    }

    function store(array $aData = null)
    {
	    $oBankIbanAddressBook = $this->getModel($aData);

        if(!empty($oBankIbanAddressBook))
        {
	        $oBankIbanAddressBook = $this->fillVo($aData, $oBankIbanAddressBook);
	        $oBankIbanAddressBook->save();
        }
        return $oBankIbanAddressBook;
    }
    private function fillVo($aData, BankIbanAddressBook $oBankIbanAddressBook)
    {
	    if(isset($aData['description'])){$oBankIbanAddressBook->setDescription($aData['description']);}
	    if(isset($aData['iban'])){$oBankIbanAddressBook->setIban($aData['iban']);}

        return $oBankIbanAddressBook;
    }
}
