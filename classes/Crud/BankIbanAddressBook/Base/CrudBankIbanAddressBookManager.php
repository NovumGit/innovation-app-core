<?php
namespace Crud\BankIbanAddressBook\Base;

use Core\Utils;
use Crud;
use Crud\BankIbanAddressBook\FieldIterator;
use Crud\BankIbanAddressBook\Field\Description;
use Crud\BankIbanAddressBook\Field\Iban;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankIbanAddressBook;
use Model\Finance\BankIbanAddressBookQuery;
use Model\Finance\Map\BankIbanAddressBookTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankIbanAddressBook instead if you need to override or add functionality.
 */
abstract class CrudBankIbanAddressBookManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankIbanAddressBookQuery::create();
	}


	public function getTableMap(): BankIbanAddressBookTableMap
	{
		return new BankIbanAddressBookTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankIbanAddressBook";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_iban_address_book toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_iban_address_book aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Iban'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Iban'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankIbanAddressBook
	 */
	public function getModel(array $aData = null): BankIbanAddressBook
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankIbanAddressBookQuery = BankIbanAddressBookQuery::create();
		     $oBankIbanAddressBook = $oBankIbanAddressBookQuery->findOneById($aData['id']);
		     if (!$oBankIbanAddressBook instanceof BankIbanAddressBook) {
		         throw new LogicException("BankIbanAddressBook should be an instance of BankIbanAddressBook but got something else." . __METHOD__);
		     }
		     $oBankIbanAddressBook = $this->fillVo($aData, $oBankIbanAddressBook);
		}
		else {
		     $oBankIbanAddressBook = new BankIbanAddressBook();
		     if (!empty($aData)) {
		         $oBankIbanAddressBook = $this->fillVo($aData, $oBankIbanAddressBook);
		     }
		}
		return $oBankIbanAddressBook;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankIbanAddressBook
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankIbanAddressBook
	{
		$oBankIbanAddressBook = $this->getModel($aData);


		 if(!empty($oBankIbanAddressBook))
		 {
		     $oBankIbanAddressBook = $this->fillVo($aData, $oBankIbanAddressBook);
		     $oBankIbanAddressBook->save();
		 }
		return $oBankIbanAddressBook;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankIbanAddressBook $oModel
	 * @return BankIbanAddressBook
	 */
	protected function fillVo(array $aData, BankIbanAddressBook $oModel): BankIbanAddressBook
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['iban'])) {
		     $oField = new Iban();
		     $mValue = $oField->sanitize($aData['iban']);
		     $oModel->setIban($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
