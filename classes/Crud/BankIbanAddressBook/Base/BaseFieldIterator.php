<?php
namespace Crud\BankIbanAddressBook\Base;

use Crud\BankIbanAddressBook\ICollectionField as BankIbanAddressBookField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankIbanAddressBook\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankIbanAddressBookField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankIbanAddressBookField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankIbanAddressBookField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankIbanAddressBookField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankIbanAddressBookField
	{
		return current($this->aFields);
	}
}
