<?php
namespace Crud\Site_navigation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Site_navigation\FieldIterator;
use Crud\Site_navigation\Field\Description;
use Crud\Site_navigation\Field\Name;
use Crud\Site_navigation\Field\ParentId;
use Crud\Site_navigation\Field\SiteId;
use Crud\Site_navigation\Field\Sorting;
use Crud\Site_navigation\Field\Tag;
use Exception\LogicException;
use Model\Cms\Map\Site_navigationTableMap;
use Model\Cms\Site_navigation;
use Model\Cms\Site_navigationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Site_navigation instead if you need to override or add functionality.
 */
abstract class CrudSite_navigationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Site_navigationQuery::create();
	}


	public function getTableMap(): Site_navigationTableMap
	{
		return new Site_navigationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Site_navigation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_navigation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_navigation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'ParentId', 'Tag', 'Name', 'Description', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'ParentId', 'Tag', 'Name', 'Description', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Site_navigation
	 */
	public function getModel(array $aData = null): Site_navigation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSite_navigationQuery = Site_navigationQuery::create();
		     $oSite_navigation = $oSite_navigationQuery->findOneById($aData['id']);
		     if (!$oSite_navigation instanceof Site_navigation) {
		         throw new LogicException("Site_navigation should be an instance of Site_navigation but got something else." . __METHOD__);
		     }
		     $oSite_navigation = $this->fillVo($aData, $oSite_navigation);
		}
		else {
		     $oSite_navigation = new Site_navigation();
		     if (!empty($aData)) {
		         $oSite_navigation = $this->fillVo($aData, $oSite_navigation);
		     }
		}
		return $oSite_navigation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Site_navigation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Site_navigation
	{
		$oSite_navigation = $this->getModel($aData);


		 if(!empty($oSite_navigation))
		 {
		     $oSite_navigation = $this->fillVo($aData, $oSite_navigation);
		     $oSite_navigation->save();
		 }
		return $oSite_navigation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Site_navigation $oModel
	 * @return Site_navigation
	 */
	protected function fillVo(array $aData, Site_navigation $oModel): Site_navigation
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['parent_id'])) {
		     $oField = new ParentId();
		     $mValue = $oField->sanitize($aData['parent_id']);
		     $oModel->setParentId($mValue);
		}
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
