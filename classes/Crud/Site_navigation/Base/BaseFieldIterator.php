<?php
namespace Crud\Site_navigation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Site_navigation\ICollectionField as Site_navigationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Site_navigation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Site_navigationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Site_navigationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Site_navigationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Site_navigationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Site_navigationField
	{
		return current($this->aFields);
	}
}
