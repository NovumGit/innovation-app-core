<?php
namespace Crud\Site_navigation\Field;

use Crud\Site_navigation\Field\Base\Tag as BaseTag;

/**
 * Skeleton subclass for representing tag field from the site_navigation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class Tag extends BaseTag
{
}
