<?php
namespace Crud\Site_navigation\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Cms\Site_navigation;

class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof Site_navigation)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/setting/category/edit?id='.$oObject->getId();
    }
}