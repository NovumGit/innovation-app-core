<?php
namespace Crud\Site_navigation\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Cms\Site_navigation;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof Site_navigation)
        {
            throw new InvalidArgumentException('Expected an instance of Site_navigation but got '.get_class($oObject));
        }

        return '/setting/category/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}