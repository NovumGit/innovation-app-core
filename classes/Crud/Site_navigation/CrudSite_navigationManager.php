<?php
namespace Crud\Site_navigation;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Cms\Site_navigation;
use Model\Cms\Site_navigationQuery;
use Propel\Runtime\Exception\LogicException;

class CrudSite_navigationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'category';
    }
    function getOverviewUrl():string
    {
        $sAddUrl = '';
        if(isset($_GET['site']))
        {
            $sAddUrl = '?site='.$_GET['site'];
        }
        return '/cms/site_navigation/overview'.$sAddUrl;
    }
    function getCreateNewUrl():string
    {
        $sAddUrl = '';
        if(isset($_GET['site']))
        {
            $sAddUrl = '?site='.$_GET['site'];
        }
        return '/setting/category/edit'.$sAddUrl;
    }
    function getNewFormTitle():string{
        return 'Navigatie item toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Navigatie item wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Description',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
            'Description',
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oSite_navigationQuery = new Site_navigationQuery();

            $oSite_navigation = $oSite_navigationQuery->findOneById($aData['id']);

            if(!$oSite_navigation instanceof Site_navigation)
            {
                throw new LogicException("\$oSite_navigation should be an instance of Site_navigation but got ".get_class($oSite_navigation)." in ".__METHOD__);
            }
        }
        else
        {
            $oSite_navigation = new Site_navigation();

            if(!empty($aData))
            {
                $oSite_navigation = $this->fillVo($aData, $oSite_navigation);
            }
        }
        return $oSite_navigation;
    }

    function store(array $aData = null)
    {
        $oSite_navigation = $this->getModel($aData);

        if(!empty($oSite_navigation))
        {
            $oSite_navigation = $this->fillVo($aData, $oSite_navigation);
            $oSite_navigation->save();
        }
        return $oSite_navigation;
    }
    private function fillVo($aData, Site_navigation $oSite_navigation)
    {
        if(isset($aData['parent_id'])){ $oSite_navigation->setParentId($aData['parent_id']); }
        if(isset($aData['name'])){ $oSite_navigation->setName($aData['name']); }
        if(isset($aData['description'])){ $oSite_navigation->setDescription($aData['description']); }

        return $oSite_navigation;
    }
}
