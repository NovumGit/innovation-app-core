<?php
namespace Crud\Site_navigation;

use Crud\Site_navigation\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of Site_navigation crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
