<?php
namespace Crud\EmailVerification;

use Crud\FormManager;
use Crud\IConfigurableCrud;

/**
 * Class CrudEmailVerificationManager
 * @package Crud\Email_verification
 * Deze Crud set wordt gebruikt bij het verzenden van status e-mail berichten, eigenlijk alleen om de verificatie e-mail link / tag toe te kunnen voegen.
 */

class CrudEmailVerificationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string{ return 'E-mail verificatie'; }
    function getOverviewUrl():string{ return ''; }
    function getCreateNewUrl():string{ return ''; }
    function getNewFormTitle():string{return '';}
    function getEditFormTitle():string{ return 'E-mail verficatie';}
    function getDefaultOverviewFields(bool $bAddNs = false):?array{ return []; }
    function getDefaultEditFields(bool $bAddNs = false):?array{ return []; }
    function getModel(array $aData = null){}
    function store(array $aData = null){}
}
