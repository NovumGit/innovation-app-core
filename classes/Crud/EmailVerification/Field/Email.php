<?php
namespace Crud\EmailVerification\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;

class Email extends Field implements IDisplayableField {

    protected $sFieldName = 'email';
    protected $sFieldLabel = 'E-mail';

    function getGetter()
    {
        return 'getEmail';
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        return '';
    }

    function getEditHtml($mData, $bReadonly)
    {
        return '';
    }
}
