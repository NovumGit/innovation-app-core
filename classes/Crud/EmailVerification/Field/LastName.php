<?php
namespace Crud\EmailVerification\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;

class LastName extends Field implements IDisplayableField {

    protected $sFieldName = 'last_name';
    protected $sFieldLabel = 'Achternaam';

    function getGetter()
    {
        return 'getLastName';
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        return '';
        /*
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getAddressL1().'</td>';
        */
    }

    function getEditHtml($mData, $bReadonly)
    {
        return '';
        /*
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getAddressL1(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
        */
    }
}
