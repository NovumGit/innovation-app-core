<?php
namespace Crud\CustomerAddress\Base;

use Core\Utils;
use Crud;
use Crud\CustomerAddress\FieldIterator;
use Crud\CustomerAddress\Field\AddressL1;
use Crud\CustomerAddress\Field\AddressL2;
use Crud\CustomerAddress\Field\AttnName;
use Crud\CustomerAddress\Field\City;
use Crud\CustomerAddress\Field\CompanyName;
use Crud\CustomerAddress\Field\CountryId;
use Crud\CustomerAddress\Field\CustomerAddressTypeId;
use Crud\CustomerAddress\Field\CustomerId;
use Crud\CustomerAddress\Field\IsDefault;
use Crud\CustomerAddress\Field\Number;
use Crud\CustomerAddress\Field\NumberAdd;
use Crud\CustomerAddress\Field\Postal;
use Crud\CustomerAddress\Field\Region;
use Crud\CustomerAddress\Field\Street;
use Crud\CustomerAddress\Field\UsaStateId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\Map\CustomerAddressTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerAddress instead if you need to override or add functionality.
 */
abstract class CrudCustomerAddressManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerAddressQuery::create();
	}


	public function getTableMap(): CustomerAddressTableMap
	{
		return new CustomerAddressTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerAddress";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_address toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_address aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'CustomerAddressTypeId', 'IsDefault', 'CompanyName', 'AttnName', 'Street', 'Number', 'NumberAdd', 'Postal', 'City', 'Region', 'CountryId', 'UsaStateId', 'AddressL1', 'AddressL2'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'CustomerAddressTypeId', 'IsDefault', 'CompanyName', 'AttnName', 'Street', 'Number', 'NumberAdd', 'Postal', 'City', 'Region', 'CountryId', 'UsaStateId', 'AddressL1', 'AddressL2'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerAddress
	 */
	public function getModel(array $aData = null): CustomerAddress
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerAddressQuery = CustomerAddressQuery::create();
		     $oCustomerAddress = $oCustomerAddressQuery->findOneById($aData['id']);
		     if (!$oCustomerAddress instanceof CustomerAddress) {
		         throw new LogicException("CustomerAddress should be an instance of CustomerAddress but got something else." . __METHOD__);
		     }
		     $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);
		}
		else {
		     $oCustomerAddress = new CustomerAddress();
		     if (!empty($aData)) {
		         $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);
		     }
		}
		return $oCustomerAddress;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerAddress
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerAddress
	{
		$oCustomerAddress = $this->getModel($aData);


		 if(!empty($oCustomerAddress))
		 {
		     $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);
		     $oCustomerAddress->save();
		 }
		return $oCustomerAddress;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerAddress $oModel
	 * @return CustomerAddress
	 */
	protected function fillVo(array $aData, CustomerAddress $oModel): CustomerAddress
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['customer_address_type_id'])) {
		     $oField = new CustomerAddressTypeId();
		     $mValue = $oField->sanitize($aData['customer_address_type_id']);
		     $oModel->setCustomerAddressTypeId($mValue);
		}
		if(isset($aData['is_default'])) {
		     $oField = new IsDefault();
		     $mValue = $oField->sanitize($aData['is_default']);
		     $oModel->setIsDefault($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['attn_name'])) {
		     $oField = new AttnName();
		     $mValue = $oField->sanitize($aData['attn_name']);
		     $oModel->setAttnName($mValue);
		}
		if(isset($aData['street'])) {
		     $oField = new Street();
		     $mValue = $oField->sanitize($aData['street']);
		     $oModel->setStreet($mValue);
		}
		if(isset($aData['number'])) {
		     $oField = new Number();
		     $mValue = $oField->sanitize($aData['number']);
		     $oModel->setNumber($mValue);
		}
		if(isset($aData['number_add'])) {
		     $oField = new NumberAdd();
		     $mValue = $oField->sanitize($aData['number_add']);
		     $oModel->setNumberAdd($mValue);
		}
		if(isset($aData['postal'])) {
		     $oField = new Postal();
		     $mValue = $oField->sanitize($aData['postal']);
		     $oModel->setPostal($mValue);
		}
		if(isset($aData['city'])) {
		     $oField = new City();
		     $mValue = $oField->sanitize($aData['city']);
		     $oModel->setCity($mValue);
		}
		if(isset($aData['region'])) {
		     $oField = new Region();
		     $mValue = $oField->sanitize($aData['region']);
		     $oModel->setRegion($mValue);
		}
		if(isset($aData['country_id'])) {
		     $oField = new CountryId();
		     $mValue = $oField->sanitize($aData['country_id']);
		     $oModel->setCountryId($mValue);
		}
		if(isset($aData['usa_state_id'])) {
		     $oField = new UsaStateId();
		     $mValue = $oField->sanitize($aData['usa_state_id']);
		     $oModel->setUsaStateId($mValue);
		}
		if(isset($aData['address_l1'])) {
		     $oField = new AddressL1();
		     $mValue = $oField->sanitize($aData['address_l1']);
		     $oModel->setAddressL1($mValue);
		}
		if(isset($aData['address_l2'])) {
		     $oField = new AddressL2();
		     $mValue = $oField->sanitize($aData['address_l2']);
		     $oModel->setAddressL2($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
