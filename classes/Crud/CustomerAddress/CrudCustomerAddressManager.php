<?php
namespace Crud\CustomerAddress;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Exception\LogicException;
use Model\Setting\MasterTable\Base\CountryQuery;
use Model\Setting\MasterTable\Country;

class CrudCustomerAddressManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'klant adres';
    }
    function getOverviewUrl():string
    {
        return '/crm/address/overview';
    }
    function getCreateNewUrl():string
    {
        return '/crm/address/edit';
    }
    function getNewFormTitle():string{
        return 'Adres toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Adres wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CompanyName',
            'Attn',
            'Street',
            'Number',
            'NumberAdd',
            'Postal',
            'City',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'CompanyName',
            'Attn',
            'Street',
            'Number',
            'NumberAdd',
            'Postal',
            'City'
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']))
        {
            $oCustomerAddressQuery = new CustomerAddressQuery();

            $oCustomerAddress = $oCustomerAddressQuery->findOneById($aData['id']);

            if(!$oCustomerAddress instanceof CustomerAddress){
                throw new LogicException("Customer should be an instance of Customer but got ".get_class($oCustomerAddressQuery)." in ".__METHOD__);
            }
            $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);
        }
        else
        {
            $oCustomerAddress = new CustomerAddress();

            if(!empty($aData))
            {
                $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);
            }
        }

        return $oCustomerAddress;
    }

    function store(array $aData = null)
    {

        $oCustomerAddress = $this->getModel($aData);

        if($oCustomerAddress instanceof CustomerAddress)
        {
            $oCustomerAddress = $this->fillVo($aData, $oCustomerAddress);

            $oCustomerAddress->save();
        }
        return $oCustomerAddress;
    }

    private function fillVo($aData, CustomerAddress $oCustomerAddress)
    {

        if(isset($aData['customer_id'])){$oCustomerAddress->setCustomerId($aData['customer_id']);}
        if(isset($aData['address_l1'])){$oCustomerAddress->setAddressL1($aData['address_l1']);}
        if(isset($aData['address_l2'])){ $oCustomerAddress->setAddressL2($aData['address_l2']);}
        if(isset($aData['attn'])){$oCustomerAddress->setAttnName($aData['attn']);}
        if(isset($aData['city'])){$oCustomerAddress->setCity($aData['city']);}
        if(isset($aData['is_default'])){$oCustomerAddress->setIsDefault($aData['is_default']);}
        if(isset($aData['company_name'])){$oCustomerAddress->setCompanyName($aData['company_name']);}
        if(isset($aData['region'])){$oCustomerAddress->setRegion($aData['region']);}

        if(isset($aData['country_id']))
        {
            $oCustomerAddress->setCountryId($aData['country_id']);

            $oCountry = CountryQuery::create()->findOneById($aData['country_id']);


            if($oCountry instanceof Country && $oCountry->getName() == 'United States of America' && isset($aData['usa_state_id']))
            {
                $oCustomerAddress->setUsaStateId($aData['usa_state_id']);
            }
            else
            {
                $oCustomerAddress->setUsaStateId(null);
            }
        }
        if(isset($aData['number'])){$oCustomerAddress->setNumber($aData['number']);}
        if(isset($aData['number_add'])){$oCustomerAddress->setNumberAdd($aData['number_add']);}
        if(isset($aData['postal'])){$oCustomerAddress->setPostal($aData['postal']);}
        if(isset($aData['street'])){$oCustomerAddress->setStreet($aData['street']);}
        if(isset($aData['customer_address_type_id'])){$oCustomerAddress->setCustomerAddressTypeId($aData['customer_address_type_id']);}
        return $oCustomerAddress;
    }
}
