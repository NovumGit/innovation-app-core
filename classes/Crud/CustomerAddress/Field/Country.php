<?php
namespace Crud\CustomerAddress\Field;

use Core\Utils;
use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\CustomerAddress as ModelObject;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\Country as CountryModel;

class Country extends Field implements IFilterableField, IEditableField, IFilterableLookupField{

    protected $sFieldName = 'country_id';
    protected $sFieldLabel = 'Land';
    private $sOverviewLabel = 'Land';
    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getVisibleValue($iItemId)
    {
        $oQuery = CountryQuery::create();
        $oCountry = $oQuery->findOneById($iItemId);

        if($oCountry instanceof CountryModel)
        {
            return $oCountry->getName();
        }
        return '';
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(!isset($aPostedData[$this->sFieldName]) || empty($aPostedData[$this->sFieldName]))
        {
            $mResponse[] = 'Land is een verplicht veld';
        }

        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {

        $aCountries = CountryQuery::create()->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aCountries, 'getName', $mSelectedItem);

        return $aDropdownOptions;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sOverviewLabel, $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$this->getVisibleValue($oModelObject->getCountryId()).'</td>';
    }


    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getCustomerAddressTypeId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCountryId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
