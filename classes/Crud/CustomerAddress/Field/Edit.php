<?php
namespace Crud\CustomerAddress\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Crm\CustomerAddress as ModelObject;


class Edit extends GenericEdit{


    function getEditUrl($oObject){

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/crm/address/edit?id='.$oObject->getId();
    }
}