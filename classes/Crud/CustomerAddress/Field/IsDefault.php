<?php
namespace Crud\CustomerAddress\Field;

use Crud\Field;
use Model\Crm\CustomerAddress as ModelObject;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

class IsDefault extends Field implements IFilterableField, IEditableField{

    protected $sFieldLabel = 'Standaard';
    protected $sFieldName = 'is_default';
    protected $sGetter = 'getIsDefault';

    function getDataType():string
    {
        return 'boolean';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oObject)
    {
        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject, got ".get_class($oObject)." in ".__METHOD__);
        }
        $sVisibleValue = $oObject->{$this->sGetter}() ? 'Ja' : 'Nee';
        return '<td class="">'.$sVisibleValue.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  model\\Product\\Product in ".__METHOD__);
        }
        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $mData->getIsDefault(), $bReadonly);
    }
}

