<?php
namespace Crud\CustomerAddress\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Crm\CustomerAddress as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oObject){

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of Customer but got '.get_class($oObject));
        }

        return '/crm/address/edit?_do=Delete&id='.$oObject->getId();
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}