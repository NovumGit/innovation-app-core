<?php
namespace Crud\CustomerAddress\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\CustomerAddress as ModelObject;
use Model\Crm\CustomerAddressType;
use Model\Crm\CustomerAddressTypeQuery;
use Core\Utils;

class CustomerAddressTypeId extends Field implements IFilterableField, IEditableField, IFilterableLookupField{

    protected $sFieldName = 'customer_address_type_id';
    protected $sFieldLabel = 'Soort adres';
    private $sIcon = 'book';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getVisibleValue($iItemId)
    {

        $oQuery = CustomerAddressTypeQuery::create();
        $oAddressType = $oQuery->findOneById($iItemId);

        if($oAddressType instanceof CustomerAddressType)
        {
            return $oAddressType->getDescription();
        }
        return '';
    }
    function getLookups($mSelectedItem = null)
    {
        $oAddressType = CustomerAddressTypeQuery::create()->orderByDescription()->find();
        $aDropdownOptions = Utils::makeSelectOptions($oAddressType, 'getDescription', $mSelectedItem);

        return $aDropdownOptions;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(empty($aPostedData['customer_address_type_id']))
        {
            $mResponse = [];
            $mResponse[] = 'Soort adres is een verplicht veld.';
        }

        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$this->getVisibleValue($oModelObject->getCustomerAddressTypeId()).'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getCustomerAddressTypeId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCustomerAddressTypeId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
