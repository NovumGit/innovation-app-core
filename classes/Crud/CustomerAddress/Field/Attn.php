<?php
namespace Crud\CustomerAddress\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\CustomerAddress as ModelObject;


class Attn extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'attn';
    protected $sFieldLabel = 'Ter attentie van';
    private $sOverviewLabel = 't.a.v.';

    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sOverviewLabel, $this->sFieldName);
    }
    function getDataType():string
    {
        return 'number';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getAttnName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getAttnName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
