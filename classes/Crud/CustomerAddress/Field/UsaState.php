<?php
namespace Crud\CustomerAddress\Field;

use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Crm\CustomerAddress as ModelObject;
use Model\Setting\MasterTable\UsaStateQuery;
use Core\Utils;

class UsaState extends Field implements IFilterableField, IEditableField, IFilterableLookupField{

    protected $sFieldName = 'usa_state_id';
    protected $sFieldLabel = 'Staat';
    private $sOverviewLabel = 'Staat';
    private $sIcon = 'globe';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getVisibleValue($iItemId)
    {
        $oQuery = UsaStateQuery::create();
        $oUsaState = $oQuery->findOneById($iItemId);

        if($oUsaState instanceof UsaState)
        {
            return $oUsaState->getName();
        }
        return '';
    }
    function getLookups($mSelectedItem = null)
    {
        $aUsaStates = UsaStateQuery::create()->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aUsaStates, 'getName', $mSelectedItem);

        return $aDropdownOptions;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sOverviewLabel, $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getCountry().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getUsaStateId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getUsaStateId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
