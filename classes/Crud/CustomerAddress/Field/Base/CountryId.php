<?php
namespace Crud\CustomerAddress\Field\Base;

use Crud\CustomerAddress\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'country_id' crud field from the 'customer_address' table.
 * This class is auto generated and should not be modified.
 */
abstract class CountryId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'country_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCountryId';
	protected $sFqModelClassname = '\\\Model\Crm\CustomerAddress';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
