<?php
namespace Crud\CustomerAddress\Field;

use Crud\CustomerAddress\Field\Base\UsaStateId as BaseUsaStateId;

/**
 * Skeleton subclass for representing usa_state_id field from the customer_address table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class UsaStateId extends BaseUsaStateId
{
}
