<?php
namespace Crud\ProductDeliveryTime\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductDeliveryTime\FieldIterator;
use Crud\ProductDeliveryTime\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ProductDeliveryTimeTableMap;
use Model\Setting\MasterTable\ProductDeliveryTime;
use Model\Setting\MasterTable\ProductDeliveryTimeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductDeliveryTime instead if you need to override or add functionality.
 */
abstract class CrudProductDeliveryTimeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductDeliveryTimeQuery::create();
	}


	public function getTableMap(): ProductDeliveryTimeTableMap
	{
		return new ProductDeliveryTimeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductDeliveryTime";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_delivery_time toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_delivery_time aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductDeliveryTime
	 */
	public function getModel(array $aData = null): ProductDeliveryTime
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductDeliveryTimeQuery = ProductDeliveryTimeQuery::create();
		     $oProductDeliveryTime = $oProductDeliveryTimeQuery->findOneById($aData['id']);
		     if (!$oProductDeliveryTime instanceof ProductDeliveryTime) {
		         throw new LogicException("ProductDeliveryTime should be an instance of ProductDeliveryTime but got something else." . __METHOD__);
		     }
		     $oProductDeliveryTime = $this->fillVo($aData, $oProductDeliveryTime);
		}
		else {
		     $oProductDeliveryTime = new ProductDeliveryTime();
		     if (!empty($aData)) {
		         $oProductDeliveryTime = $this->fillVo($aData, $oProductDeliveryTime);
		     }
		}
		return $oProductDeliveryTime;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductDeliveryTime
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductDeliveryTime
	{
		$oProductDeliveryTime = $this->getModel($aData);


		 if(!empty($oProductDeliveryTime))
		 {
		     $oProductDeliveryTime = $this->fillVo($aData, $oProductDeliveryTime);
		     $oProductDeliveryTime->save();
		 }
		return $oProductDeliveryTime;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductDeliveryTime $oModel
	 * @return ProductDeliveryTime
	 */
	protected function fillVo(array $aData, ProductDeliveryTime $oModel): ProductDeliveryTime
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
