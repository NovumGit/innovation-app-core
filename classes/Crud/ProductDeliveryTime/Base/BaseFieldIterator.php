<?php
namespace Crud\ProductDeliveryTime\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductDeliveryTime\ICollectionField as ProductDeliveryTimeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductDeliveryTime\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductDeliveryTimeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductDeliveryTimeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductDeliveryTimeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductDeliveryTimeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductDeliveryTimeField
	{
		return current($this->aFields);
	}
}
