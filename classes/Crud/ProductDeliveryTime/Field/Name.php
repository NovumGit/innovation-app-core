<?php
namespace Crud\ProductDeliveryTime\Field;

use Crud\ProductDeliveryTime\Field\Base\Name as BaseName;

/**
 * Skeleton subclass for representing name field from the mt_delivery_time table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Name extends BaseName
{
}
