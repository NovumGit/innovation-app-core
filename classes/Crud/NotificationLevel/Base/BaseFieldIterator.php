<?php
namespace Crud\NotificationLevel\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\NotificationLevel\ICollectionField as NotificationLevelField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\NotificationLevel\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param NotificationLevelField[] $aFields */
	private array $aFields = [];


	/**
	 * @param NotificationLevelField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof NotificationLevelField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(NotificationLevelField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): NotificationLevelField
	{
		return current($this->aFields);
	}
}
