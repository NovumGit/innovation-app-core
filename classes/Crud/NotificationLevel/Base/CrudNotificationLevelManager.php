<?php
namespace Crud\NotificationLevel\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\NotificationLevel\FieldIterator;
use Crud\NotificationLevel\Field\Name;
use Exception\LogicException;
use Model\System\Map\NotificationLevelTableMap;
use Model\System\NotificationLevel;
use Model\System\NotificationLevelQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify NotificationLevel instead if you need to override or add functionality.
 */
abstract class CrudNotificationLevelManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return NotificationLevelQuery::create();
	}


	public function getTableMap(): NotificationLevelTableMap
	{
		return new NotificationLevelTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "NotificationLevel";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "notification_level toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "notification_level aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return NotificationLevel
	 */
	public function getModel(array $aData = null): NotificationLevel
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oNotificationLevelQuery = NotificationLevelQuery::create();
		     $oNotificationLevel = $oNotificationLevelQuery->findOneById($aData['id']);
		     if (!$oNotificationLevel instanceof NotificationLevel) {
		         throw new LogicException("NotificationLevel should be an instance of NotificationLevel but got something else." . __METHOD__);
		     }
		     $oNotificationLevel = $this->fillVo($aData, $oNotificationLevel);
		}
		else {
		     $oNotificationLevel = new NotificationLevel();
		     if (!empty($aData)) {
		         $oNotificationLevel = $this->fillVo($aData, $oNotificationLevel);
		     }
		}
		return $oNotificationLevel;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return NotificationLevel
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): NotificationLevel
	{
		$oNotificationLevel = $this->getModel($aData);


		 if(!empty($oNotificationLevel))
		 {
		     $oNotificationLevel = $this->fillVo($aData, $oNotificationLevel);
		     $oNotificationLevel->save();
		 }
		return $oNotificationLevel;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param NotificationLevel $oModel
	 * @return NotificationLevel
	 */
	protected function fillVo(array $aData, NotificationLevel $oModel): NotificationLevel
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
