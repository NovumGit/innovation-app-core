<?php
namespace Crud\MtCompanyCategory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MtCompanyCategory\ICollectionField;

/**
 * Base class that represents the 'description' crud field from the 'mt_company_category' table.
 * This class is auto generated and should not be modified.
 */
abstract class Description extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'description';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDescription';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\MtCompanyCategory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
