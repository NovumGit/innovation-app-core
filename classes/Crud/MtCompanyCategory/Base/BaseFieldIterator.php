<?php
namespace Crud\MtCompanyCategory\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MtCompanyCategory\ICollectionField as MtCompanyCategoryField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MtCompanyCategory\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MtCompanyCategoryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MtCompanyCategoryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MtCompanyCategoryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MtCompanyCategoryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MtCompanyCategoryField
	{
		return current($this->aFields);
	}
}
