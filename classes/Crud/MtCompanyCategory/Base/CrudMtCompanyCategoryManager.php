<?php
namespace Crud\MtCompanyCategory\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MtCompanyCategory\FieldIterator;
use Crud\MtCompanyCategory\Field\Description;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\MtCompanyCategoryTableMap;
use Model\Setting\MasterTable\MtCompanyCategory;
use Model\Setting\MasterTable\MtCompanyCategoryQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MtCompanyCategory instead if you need to override or add functionality.
 */
abstract class CrudMtCompanyCategoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MtCompanyCategoryQuery::create();
	}


	public function getTableMap(): MtCompanyCategoryTableMap
	{
		return new MtCompanyCategoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MtCompanyCategory";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_company_category toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_company_category aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MtCompanyCategory
	 */
	public function getModel(array $aData = null): MtCompanyCategory
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMtCompanyCategoryQuery = MtCompanyCategoryQuery::create();
		     $oMtCompanyCategory = $oMtCompanyCategoryQuery->findOneById($aData['id']);
		     if (!$oMtCompanyCategory instanceof MtCompanyCategory) {
		         throw new LogicException("MtCompanyCategory should be an instance of MtCompanyCategory but got something else." . __METHOD__);
		     }
		     $oMtCompanyCategory = $this->fillVo($aData, $oMtCompanyCategory);
		}
		else {
		     $oMtCompanyCategory = new MtCompanyCategory();
		     if (!empty($aData)) {
		         $oMtCompanyCategory = $this->fillVo($aData, $oMtCompanyCategory);
		     }
		}
		return $oMtCompanyCategory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MtCompanyCategory
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MtCompanyCategory
	{
		$oMtCompanyCategory = $this->getModel($aData);


		 if(!empty($oMtCompanyCategory))
		 {
		     $oMtCompanyCategory = $this->fillVo($aData, $oMtCompanyCategory);
		     $oMtCompanyCategory->save();
		 }
		return $oMtCompanyCategory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MtCompanyCategory $oModel
	 * @return MtCompanyCategory
	 */
	protected function fillVo(array $aData, MtCompanyCategory $oModel): MtCompanyCategory
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
