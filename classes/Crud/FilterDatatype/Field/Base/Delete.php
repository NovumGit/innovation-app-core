<?php 
namespace Crud\FilterDatatype\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\FilterDatatype;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterDatatype)
		{
		     return "//system/filter_datatype/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterDatatype)
		{
		     return "//filter_datatype?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
