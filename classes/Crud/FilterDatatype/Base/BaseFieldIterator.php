<?php
namespace Crud\FilterDatatype\Base;

use Crud\BaseCrudFieldIterator;
use Crud\FilterDatatype\ICollectionField as FilterDatatypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\FilterDatatype\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FilterDatatypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FilterDatatypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FilterDatatypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FilterDatatypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FilterDatatypeField
	{
		return current($this->aFields);
	}
}
