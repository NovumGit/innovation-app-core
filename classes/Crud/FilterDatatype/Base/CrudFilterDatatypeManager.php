<?php
namespace Crud\FilterDatatype\Base;

use Core\Utils;
use Crud;
use Crud\FilterDatatype\FieldIterator;
use Crud\FilterDatatype\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\FilterDatatype;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\Map\FilterDatatypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify FilterDatatype instead if you need to override or add functionality.
 */
abstract class CrudFilterDatatypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FilterDatatypeQuery::create();
	}


	public function getTableMap(): FilterDatatypeTableMap
	{
		return new FilterDatatypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "In is opgeslagen welke met soorten data het systeem om kan gaan";
	}


	public function getEntityTitle(): string
	{
		return "FilterDatatype";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "filter_datatype toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "filter_datatype aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return FilterDatatype
	 */
	public function getModel(array $aData = null): FilterDatatype
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFilterDatatypeQuery = FilterDatatypeQuery::create();
		     $oFilterDatatype = $oFilterDatatypeQuery->findOneById($aData['id']);
		     if (!$oFilterDatatype instanceof FilterDatatype) {
		         throw new LogicException("FilterDatatype should be an instance of FilterDatatype but got something else." . __METHOD__);
		     }
		     $oFilterDatatype = $this->fillVo($aData, $oFilterDatatype);
		}
		else {
		     $oFilterDatatype = new FilterDatatype();
		     if (!empty($aData)) {
		         $oFilterDatatype = $this->fillVo($aData, $oFilterDatatype);
		     }
		}
		return $oFilterDatatype;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return FilterDatatype
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): FilterDatatype
	{
		$oFilterDatatype = $this->getModel($aData);


		 if(!empty($oFilterDatatype))
		 {
		     $oFilterDatatype = $this->fillVo($aData, $oFilterDatatype);
		     $oFilterDatatype->save();
		 }
		return $oFilterDatatype;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param FilterDatatype $oModel
	 * @return FilterDatatype
	 */
	protected function fillVo(array $aData, FilterDatatype $oModel): FilterDatatype
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
