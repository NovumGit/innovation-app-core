<?php
namespace Crud\SaleOrderItemStockClaimed\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderItemStockClaimed\ICollectionField as SaleOrderItemStockClaimedField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderItemStockClaimed\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderItemStockClaimedField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderItemStockClaimedField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderItemStockClaimedField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderItemStockClaimedField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderItemStockClaimedField
	{
		return current($this->aFields);
	}
}
