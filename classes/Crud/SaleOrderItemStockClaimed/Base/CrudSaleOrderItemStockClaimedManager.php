<?php
namespace Crud\SaleOrderItemStockClaimed\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderItemStockClaimed\FieldIterator;
use Crud\SaleOrderItemStockClaimed\Field\CreatedOn;
use Crud\SaleOrderItemStockClaimed\Field\ProductId;
use Crud\SaleOrderItemStockClaimed\Field\Quantity;
use Crud\SaleOrderItemStockClaimed\Field\SaleOrderItemId;
use Exception\LogicException;
use Model\Sale\Map\SaleOrderItemStockClaimedTableMap;
use Model\Sale\SaleOrderItemStockClaimed;
use Model\Sale\SaleOrderItemStockClaimedQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderItemStockClaimed instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderItemStockClaimedManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderItemStockClaimedQuery::create();
	}


	public function getTableMap(): SaleOrderItemStockClaimedTableMap
	{
		return new SaleOrderItemStockClaimedTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderItemStockClaimed";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_item_stock_claimed toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_item_stock_claimed aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderItemId', 'ProductId', 'CreatedOn', 'Quantity'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderItemId', 'ProductId', 'CreatedOn', 'Quantity'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderItemStockClaimed
	 */
	public function getModel(array $aData = null): SaleOrderItemStockClaimed
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderItemStockClaimedQuery = SaleOrderItemStockClaimedQuery::create();
		     $oSaleOrderItemStockClaimed = $oSaleOrderItemStockClaimedQuery->findOneById($aData['id']);
		     if (!$oSaleOrderItemStockClaimed instanceof SaleOrderItemStockClaimed) {
		         throw new LogicException("SaleOrderItemStockClaimed should be an instance of SaleOrderItemStockClaimed but got something else." . __METHOD__);
		     }
		     $oSaleOrderItemStockClaimed = $this->fillVo($aData, $oSaleOrderItemStockClaimed);
		}
		else {
		     $oSaleOrderItemStockClaimed = new SaleOrderItemStockClaimed();
		     if (!empty($aData)) {
		         $oSaleOrderItemStockClaimed = $this->fillVo($aData, $oSaleOrderItemStockClaimed);
		     }
		}
		return $oSaleOrderItemStockClaimed;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderItemStockClaimed
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderItemStockClaimed
	{
		$oSaleOrderItemStockClaimed = $this->getModel($aData);


		 if(!empty($oSaleOrderItemStockClaimed))
		 {
		     $oSaleOrderItemStockClaimed = $this->fillVo($aData, $oSaleOrderItemStockClaimed);
		     $oSaleOrderItemStockClaimed->save();
		 }
		return $oSaleOrderItemStockClaimed;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderItemStockClaimed $oModel
	 * @return SaleOrderItemStockClaimed
	 */
	protected function fillVo(array $aData, SaleOrderItemStockClaimed $oModel): SaleOrderItemStockClaimed
	{
		if(isset($aData['sale_order_item_id'])) {
		     $oField = new SaleOrderItemId();
		     $mValue = $oField->sanitize($aData['sale_order_item_id']);
		     $oModel->setSaleOrderItemId($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['quantity'])) {
		     $oField = new Quantity();
		     $mValue = $oField->sanitize($aData['quantity']);
		     $oModel->setQuantity($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
