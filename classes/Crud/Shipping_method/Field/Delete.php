<?php
namespace Crud\Shipping_method\Field;

use Crud\Generic\Field\GenericDelete;
use InvalidArgumentException;
use Model\Setting\MasterTable\ShippingMethod as ModelObject;

class Delete extends GenericDelete{


    function getDeleteUrl($oModelObject){


        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/admin/mastertable/shipping_method/edit?_do=Delete&id='.$oModelObject->getId();
    }
    function getOverviewValue($oObject)
    {
        $aOut = [];
        $aOut[] = '<td class="">';

        if(!$oObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oObject));
        }

        $aOut[] = '<a href="'.$this->getDeleteUrl($oObject).'" class="btn btn-danger br2 btn-xs fs12"> <i class="fa fa-trash-o"></i> </a>';
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }

    function getUnDeleteUrl($oModelObject){
        return;
    }
}