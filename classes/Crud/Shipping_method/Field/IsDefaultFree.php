<?php
namespace Crud\Shipping_method\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\ShippingMethod as ModelObject;

class IsDefaultFree extends Field{

    protected $sFieldName = 'is_default_free';
    protected $sFieldLabel = 'Is standaard gratis';
    private $sIcon = 'tag';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oShippingMethod)
    {
        if(!$oShippingMethod instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject got ".get_class($oShippingMethod)."  in ".__METHOD__);
        }
        $sLabel = $oShippingMethod->getIsDefaultFree() ? 'Ja' : 'Nee';
        return '<td class="">'.$sLabel.'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $mData->getIsDefaultFree(), $bReadonly, $this->sIcon);
    }
}