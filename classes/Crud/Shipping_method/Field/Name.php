<?php
namespace Crud\Shipping_method\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\ShippingMethod;

class Name extends Field{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Naam';
    private $sIcon = 'tag';
    private $sPlaceHolder = 'Name';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oShippingMethod)
    {
        if(!$oShippingMethod instanceof ShippingMethod)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($oShippingMethod)."  in ".__METHOD__);
        }
        return '<td class="">'.$oShippingMethod->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ShippingMethod)
        {
            throw new InvalidArgumentException("Expected an instance of model\\Setting\\MasterTable\\SaleOrderStatus got ".get_class($mData)." in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}