<?php
namespace Crud\Shipping_method\Field;

use Crud\Generic\Field\GenericEdit;

use InvalidArgumentException;
use Model\Setting\MasterTable\ShippingMethod as ModelObject;


class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }
        return '/admin/mastertable/shipping_method/edit?id='.$oModelObject->getId();
    }
}

