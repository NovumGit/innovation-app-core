<?php
namespace Crud\Shipping_method;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use LogicException;

use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery;

class CrudShipping_methodManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Verzend methode';
    }
    function getNewFormTitle():string{
        return 'Verzendmethod toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Verzendmethode bewerken';
    }
    function getOverviewUrl():string
    {
        return '/admin/mastertable/shipping_method/overview';
    }
    function getCreateNewUrl():string
    {
        return '/admin/mastertable/shipping_method/edit';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
        ];
    }
    /**
     * @param $aData
     * @return ShippingMethod
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = ShippingMethodQuery::create();
            $oShippingMethod = $oQuery->findOneById($aData['id']);
            if(!$oShippingMethod instanceof ShippingMethod)
            {
                throw new LogicException("ShippingMethod should be an instance of ShippingMethod but got ".get_class($oShippingMethod)." in ".__METHOD__);
            }
        }
        else
        {
            $oShippingMethod = new ShippingMethod();
            $oShippingMethod = $this->fillVo($oShippingMethod, $aData);
        }
        return $oShippingMethod;
    }
    function store(array $aData = null)
    {
        $oShippingMethod = $this->getModel($aData);

        if(!empty($oShippingMethod))
        {
            $oShippingMethod = $this->fillVo($oShippingMethod, $aData);


            $oShippingMethod->save();
        }
        return $oShippingMethod;
    }
    function fillVo(ShippingMethod $oShippingMethod, $aData)
    {
        if(isset($aData['name']))
        {
            $oShippingMethod->setName($aData['name']);
        }

        if(isset($aData['is_track_trace']))
        {
            $oShippingMethod->setIsTrackTrace($aData['is_track_trace'] ? true : false);
        }
        if(isset($aData['is_default_free'])){
            if($aData['is_default_free'] == '1')
            {
                ShippingMethodQuery::create()->update(['IsDefaultFree' => false]);
                $oShippingMethod->setIsDefaultFree(true);
            }
        }

        return $oShippingMethod;
    }
}
