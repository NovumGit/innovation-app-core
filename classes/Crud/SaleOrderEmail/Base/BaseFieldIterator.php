<?php
namespace Crud\SaleOrderEmail\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderEmail\ICollectionField as SaleOrderEmailField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderEmail\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderEmailField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderEmailField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderEmailField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderEmailField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderEmailField
	{
		return current($this->aFields);
	}
}
