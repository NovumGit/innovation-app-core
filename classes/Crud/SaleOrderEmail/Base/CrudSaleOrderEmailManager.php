<?php
namespace Crud\SaleOrderEmail\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderEmail\FieldIterator;
use Crud\SaleOrderEmail\Field\AttachedFiles;
use Crud\SaleOrderEmail\Field\Content;
use Crud\SaleOrderEmail\Field\CreatedOn;
use Crud\SaleOrderEmail\Field\SaleOrderId;
use Crud\SaleOrderEmail\Field\SendFrom;
use Crud\SaleOrderEmail\Field\SendTo;
use Crud\SaleOrderEmail\Field\Subject;
use Exception\LogicException;
use Model\Sale\Map\SaleOrderEmailTableMap;
use Model\Sale\SaleOrderEmail;
use Model\Sale\SaleOrderEmailQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderEmail instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderEmailManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderEmailQuery::create();
	}


	public function getTableMap(): SaleOrderEmailTableMap
	{
		return new SaleOrderEmailTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderEmail";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_email toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_email aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'SendFrom', 'SendTo', 'Subject', 'Content', 'AttachedFiles'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'SendFrom', 'SendTo', 'Subject', 'Content', 'AttachedFiles'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderEmail
	 */
	public function getModel(array $aData = null): SaleOrderEmail
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderEmailQuery = SaleOrderEmailQuery::create();
		     $oSaleOrderEmail = $oSaleOrderEmailQuery->findOneById($aData['id']);
		     if (!$oSaleOrderEmail instanceof SaleOrderEmail) {
		         throw new LogicException("SaleOrderEmail should be an instance of SaleOrderEmail but got something else." . __METHOD__);
		     }
		     $oSaleOrderEmail = $this->fillVo($aData, $oSaleOrderEmail);
		}
		else {
		     $oSaleOrderEmail = new SaleOrderEmail();
		     if (!empty($aData)) {
		         $oSaleOrderEmail = $this->fillVo($aData, $oSaleOrderEmail);
		     }
		}
		return $oSaleOrderEmail;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderEmail
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderEmail
	{
		$oSaleOrderEmail = $this->getModel($aData);


		 if(!empty($oSaleOrderEmail))
		 {
		     $oSaleOrderEmail = $this->fillVo($aData, $oSaleOrderEmail);
		     $oSaleOrderEmail->save();
		 }
		return $oSaleOrderEmail;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderEmail $oModel
	 * @return SaleOrderEmail
	 */
	protected function fillVo(array $aData, SaleOrderEmail $oModel): SaleOrderEmail
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['sale_order_id'])) {
		     $oField = new SaleOrderId();
		     $mValue = $oField->sanitize($aData['sale_order_id']);
		     $oModel->setSaleOrderId($mValue);
		}
		if(isset($aData['send_from'])) {
		     $oField = new SendFrom();
		     $mValue = $oField->sanitize($aData['send_from']);
		     $oModel->setSendFrom($mValue);
		}
		if(isset($aData['send_to'])) {
		     $oField = new SendTo();
		     $mValue = $oField->sanitize($aData['send_to']);
		     $oModel->setSendTo($mValue);
		}
		if(isset($aData['subject'])) {
		     $oField = new Subject();
		     $mValue = $oField->sanitize($aData['subject']);
		     $oModel->setSubject($mValue);
		}
		if(isset($aData['content'])) {
		     $oField = new Content();
		     $mValue = $oField->sanitize($aData['content']);
		     $oModel->setContent($mValue);
		}
		if(isset($aData['attached_files'])) {
		     $oField = new AttachedFiles();
		     $mValue = $oField->sanitize($aData['attached_files']);
		     $oModel->setAttachedFiles($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
