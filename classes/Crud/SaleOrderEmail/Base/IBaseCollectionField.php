<?php
namespace Crud\SaleOrderEmail\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderEmail\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
