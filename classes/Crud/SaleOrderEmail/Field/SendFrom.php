<?php
namespace Crud\SaleOrderEmail\Field;

use Crud\SaleOrderEmail\Field\Base\SendFrom as BaseSendFrom;

/**
 * Skeleton subclass for representing send_from field from the sale_order_email table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class SendFrom extends BaseSendFrom
{
}
