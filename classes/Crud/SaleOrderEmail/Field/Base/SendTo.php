<?php
namespace Crud\SaleOrderEmail\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderEmail\ICollectionField;

/**
 * Base class that represents the 'send_to' crud field from the 'sale_order_email' table.
 * This class is auto generated and should not be modified.
 */
abstract class SendTo extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'send_to';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSendTo';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrderEmail';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
