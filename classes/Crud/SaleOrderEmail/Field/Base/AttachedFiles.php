<?php
namespace Crud\SaleOrderEmail\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderEmail\ICollectionField;

/**
 * Base class that represents the 'attached_files' crud field from the 'sale_order_email' table.
 * This class is auto generated and should not be modified.
 */
abstract class AttachedFiles extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'attached_files';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAttachedFiles';
	protected $sFqModelClassname = '\\\Model\Sale\SaleOrderEmail';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
