<?php
namespace Crud;
use Core\Paginate;
use Hurah\Types\Type\Url;
use Model\Setting\CrudManager\CrudView;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Map\TableMap;
use Ui\RenderEditConfig;
use Hurah\Types\Type\Json;

interface IFormManager extends IBaseManager
{
    function getOverviewEditUrl(CrudView $oCrudView):Url;
    function getFormEditUrl(string $sLayoutKey, string $sTitle = null):Url;
    function getRenderedFormAsArray($sRendering = null):array;
    function getRenderedFormAsJson($sRendering = null):Json;

    function getEditFormTitle():string;
    function getEntityTitle():string;
    function getNewFormTitle():string;
    function getModel(array $aData = null);
    function getModuleName():string;
    function getOverviewUrl():string;
    function getCreateNewUrl():string;
    function getOverviewData():\IteratorAggregate;
    function getPaginate():?Paginate;
    function save(array $aData = null);
    function store(array $aData = null);
    function storeCustomFields(ActiveRecordInterface $oObject, array $aData);
    function getValidationErrors($aPostedData, $sFormLayoutKey = null):?array;
    function isValid($aPostedData, $sFormLayoutKey = null);
    function registerValidationErrorStatusMessages($aPostedData, $sFormLayoutKey = null);
    function setOverviewData(\IteratorAggregate $mData, int $iCurrentPage = null, int $iItemsPP = 50):void;
    function getOverviewHtml(array $aFields = null, array $aArguments = null);
    function getAllFieldsAsArray(callable $callableFilter = null): ?array;
    function getFieldIterator(callable $filter = null):ICrudFieldIterator;
    function getRenderedEditHtml($mData, $sRendering = null, RenderEditConfig $oRenderEditConfig = null);
    function getEditHtml($mData, RenderEditConfig $oRenderEditConfig = null);
}
