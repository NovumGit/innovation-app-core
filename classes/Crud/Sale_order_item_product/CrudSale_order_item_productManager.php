<?php
namespace Crud\Sale_order_item_product;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\Sale\OrderItemProduct;
use Model\Sale\OrderItemProductQuery;

use LogicException;
use Model\Sale\SaleOrderItemQuery;

class CrudSale_order_item_productManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'sale_order_item_product';
    }
    function getNewFormTitle():string{
        return '';
    }
    function getEditFormTitle():string
    {
        return 'Order item wijzigen';
    }
    function getOverviewUrl():string
    {
        return '';
    }
    function getCreateNewUrl():string
    {
        return '';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return null;
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'SalePrice',
            'Width',
            'Height',
            'Thickness',
            'Weight',
            'SaleOrderItemQuantity',
            'TypeName',
            'UnitName',
            'MaterialName'
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']))
        {
            $oQuery = new OrderItemProductQuery();

            $oOrderItemProduct = $oQuery->findOneById($aData['id']);

            if($oOrderItemProduct == null)
            {
                throw new LogicException("OrderItemProduct is NULL");

            }

            if(!$oOrderItemProduct instanceof OrderItemProduct)
            {
                throw new LogicException("OrderItemProduct should be an instance of OrderItemProduct but got ".get_class($oOrderItemProduct)." in ".__METHOD__);
            }
        }
        else
        {
            $oOrderItemProduct = new OrderItemProduct();
            $oOrderItemProduct = $this->fillVo($oOrderItemProduct, $aData);
        }
        return $oOrderItemProduct;
    }

    function store(array $aData = null)
    {
        $oOrderItemProduct = $this->getModel($aData);

        if(!empty($oOrderItemProduct))
        {
            if($aData['order_item_description'])
            {
                $oOrderItem = $oOrderItemProduct->getOrderItem();
                $oOrderItem->setDescription($aData['order_item_description']);
                $oOrderItem->save();
            }
            $oOrderItemProduct = $this->fillVo($oOrderItemProduct, $aData);

            if(isset($aData['sale_order_item_quantity']))
            {
                $SaleOrderItem = SaleOrderItemQuery::create()->findOneById($oOrderItemProduct->getSaleOrderItemId());
                $SaleOrderItem->setQuantity($aData['sale_order_item_quantity']);
                $SaleOrderItem->save();
            }
            if(isset($aData['sale_price']))
            {
                $SaleOrderItem = SaleOrderItemQuery::create()->findOneById($oOrderItemProduct->getSaleOrderItemId());
                $aData['sale_price'] = str_replace(',', '.', $aData['sale_price']);
                $SaleOrderItem->setPrice($aData['sale_price']);
                $SaleOrderItem->save();
            }

            $oOrderItemProduct->save();
        }
        return $oOrderItemProduct;
    }

    function fillVo(OrderItemProduct $oOrderItem, $aData)
    {
        if(isset($aData['id'])) $oOrderItem->setId($aData['id']);
        if(isset($aData['product_id'])) $oOrderItem->setProductId($aData['product_id']);
        if(isset($aData['sale_price'])) $oOrderItem->setSalePrice($aData['sale_price']);
        if(isset($aData['width'])) $oOrderItem->setWidth($aData['width']);
        if(isset($aData['height'])) $oOrderItem->setHeight($aData['height']);
        if(isset($aData['thickness'])) $oOrderItem->setThickness($aData['thickness']);
        if(isset($aData['material_name'])) $oOrderItem->setMaterialName($aData['material_name']);

        return $oOrderItem;
    }
}
