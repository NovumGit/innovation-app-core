<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\OrderItemProduct as ModelObject;
use Model\Sale\SaleOrderItem;
use Model\Sale\SaleOrderItemQuery;


class SaleOrderItemQuantity extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'sale_order_item_quantity';
    protected $sFieldLabel = 'Aantal';
    private $sIcon = 'euro';
    private $sPlaceHolder = '';

    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($oModelObject->getSaleOrderItemId());
        $iQuantity = '';
        if($oSaleOrderItem instanceof SaleOrderItem)
        {
            $iQuantity = $oSaleOrderItem->getQuantity();
        }
        return '<td class="">'.$iQuantity.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $iQuantity = '';
        $oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($mData->getSaleOrderItemId());
        if($oSaleOrderItem instanceof SaleOrderItem)
        {
            $iQuantity = $oSaleOrderItem->getQuantity();
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $iQuantity, $this->sPlaceHolder, $this->sIcon, false);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
}
