<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\OrderItemProduct as ModelObject;

class Product extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'height';
    protected $sFieldLabel = 'Hoogte';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getHeight';

    function getDataType():string
    {
        return '';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getProduct()->getDescription().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $mData->getProduct()->getDescription().' <a href="/order/add_product?id=10&amp;_do=RemoveProduct&amp;order_item_id=19" class="btn btn-info br2 btn-xs fs12 d">
                                                            <i class="fa fa-edit"></i>
                                                        </a>';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
}
