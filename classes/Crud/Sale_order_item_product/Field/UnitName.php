<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\OrderItemProduct as ModelObject;

class UnitName extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'unit_name';
    protected $sFieldLabel = 'Eenheid';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getUnitName';

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, false);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
}
