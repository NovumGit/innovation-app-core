<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Helper\SaleOrderItemPropertyHelper;
use Model\Sale\OrderItemProduct as ModelObject;

class OptPrintingTechnique extends Field{

    protected $sFieldName = 'opt_printing_technique';
    protected $sFieldLabel = 'Druktechniek';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sOverviewValue = SaleOrderItemPropertyHelper::getProperty('parameter', $oModelObject->getSaleOrderItemId(), 'Druktechniek');
        return '<td class="">'.$sOverviewValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sEditValue = SaleOrderItemPropertyHelper::getProperty('parameter', $mData->getSaleOrderItemId(), 'Druktechniek');
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sEditValue, $this->sPlaceHolder, $this->sIcon, false);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
}