<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\OrderItemProduct as ModelObject;

class OrderItemDescription extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'order_item_description';
    protected $sFieldLabel = 'Omschrijving';
    private $sPlaceHolder = 'Omschrijving';
    private $sIcon = 'edit';

    function getDataType():string
    {
        return '';
    }

    function hasValidations() {
        return false;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return '<td class="">'.$oModelObject->getOrderItem()->getDescription().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sDescription = '';

        if($mData->getOrderItem())
        {
            $sDescription = $mData->getOrderItem()->getDescription();
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $sDescription, $this->sPlaceHolder, $this->sIcon, false);
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
}
