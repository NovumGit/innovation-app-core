<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\OrderItemProduct as ModelObject;
use Model\Sale\SaleOrderItemQuery;

class SalePrice extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'sale_price';
    protected $sFieldLabel = 'Prijs';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
//    private $sGetter = 'getSalePrice';

    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($oModelObject->getSaleOrderItemId());
        $fPrice = $oSaleOrderItem->getPrice();
        $fPrice = str_replace('.', ',', $fPrice);
        return '<td class="">'.$fPrice.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($mData->getSaleOrderItemId());

        $fPrice = $oSaleOrderItem->getPrice();
        $fPrice = str_replace('.', ',', $fPrice);

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $fPrice, $this->sPlaceHolder, $this->sIcon, false);
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
}
