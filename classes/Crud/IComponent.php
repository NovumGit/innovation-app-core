<?php
namespace Crud;

use Ui\RenderEditConfig;

interface IComponent
{
    function setArguments($aArguments);

    function getArgument($sKey);

    function getTranslatedTitle();

    function getFieldTitle();

    function getEditHtml($mData, $bReadonly);

    function getFieldName();

    /**
     * Should always be yes unless the field contents is immutable.
     * @return bool
     */
    function isMutable();

    function setRenderEditConfig(RenderEditConfig $oRenderEditConfig);

    function getRenderEditConfig(): RenderEditConfig;

    function isOverviewField(): bool;

    function isEditorField():bool;

    function getFieldLabel();

    function toArray():array;

}
