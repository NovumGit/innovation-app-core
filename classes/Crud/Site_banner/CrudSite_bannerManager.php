<?php
namespace Crud\Site_banner;

use Core\StatusMessage;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\Cms\SiteBanner;
use Model\Cms\SiteBannerQuery;

class CrudSite_bannerManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'site_banner';
    }
    function getNewFormTitle():string
    {
        return 'Banner toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Banner bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/banner/overview?site='.$_GET['site'];
        }
        return '/cms/banner/overview?site='.$this->getArgument('site');
    }
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/banner/edit?site='.$_GET['site'];
        }
        return '/cms/banner/edit?site='.$this->getArgument('site');
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'ViewImage',
            'AboutTxt',
            'Slogan',
            'HasInfoButton',
            'HasBuyNowLink',
            'Edit',
            'Delete'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'AboutTxt',
            'Slogan',
            'HasInfoButton',
            'InfoLink',
            'HasBuyNowLink',
            'BuyNowLink',
            'Image'
        ];
    }

    /**
     * @param $aData
     * @return SiteBanner
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SiteBannerQuery();

            $oSiteBanner = $oQuery->findOneById($aData['id']);

            if(!$oSiteBanner instanceof SiteBanner)
            {
                throw new LogicException("SiteBanner should be an instance of Site but got ".get_class($oSiteBanner)." in ".__METHOD__);
            }
        }
        else
        {
            $oSiteBanner = new SiteBanner();
            $oSiteBanner = $this->fillVo($oSiteBanner, $aData);
        }
        return $oSiteBanner;
    }

    function store(array $aData = null)
    {
        $oSiteBanner = $this->getModel($aData);
        if(!empty($oSiteBanner))
        {
            $oSiteBanner = $this->fillVo($oSiteBanner, $aData);
            $oSiteBanner->save();

            if(isseT($_FILES['data']) && !empty($_FILES['data']) && $_FILES['data']['error']['image'] == 0)
            {
                $sBannerImgDir = '../data/img/banner/';
                if(!is_dir($sBannerImgDir))
                {
                    mkdir($sBannerImgDir, 0777, true);
                }

                $sPossiblePreviousFile = $sBannerImgDir.$oSiteBanner->getId().'.'.$oSiteBanner->getFileExt();
                if(file_exists($sPossiblePreviousFile))
                {
                    unlink($sPossiblePreviousFile);
                }

                $sExt = pathinfo($_FILES['data']['name']['image'], PATHINFO_EXTENSION);
                $oSiteBanner->setFileExt($sExt);

                move_uploaded_file($_FILES['data']['tmp_name']['image'], $sBannerImgDir.$oSiteBanner->getId().'.'.$sExt);

                $oSiteBanner->save();
            }
        }
        return $oSiteBanner;
    }
    function delete($iBannerId)
    {
        $oBannerModel = SiteBannerQuery::create()->findOneById($iBannerId);
        $sDomain = $oBannerModel->getSite()->getDomain();

        $sBannerImgDir = './sites/'.$sDomain.'/img/banner/';
        $sPossiblePreviousFile = $sBannerImgDir.$oBannerModel->getId().'.'.$oBannerModel->getFileExt();

        if(file_exists($sPossiblePreviousFile))
        {
            unlink($sPossiblePreviousFile);
        }
        $oBannerModel->delete();
        StatusMessage::success("Banner verwijderd.");

    }
    function fillVo(SiteBanner $oSiteBanner, $aData)
    {
        $sSite = $this->getArgument('site');

        $oSiteQuery = SiteQuery::create();
        $oSite = $oSiteQuery->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("You are trying to add a USP to a site but no site with the domain $sSite was found int the system.");
        }

        $oSiteBanner->setSiteId($oSite->getId());

        if(isset($aData['about_txt'])){
            $oSiteBanner->setAboutTxt($aData['about_txt']);
        }
        if(isset($aData['sub_text'])){
            $oSiteBanner->setSubText($aData['sub_text']);
        }
        if(isset($aData['alignment'])){
            if(!$aData['alignment'])
            {
                $aData['alignment'] = null;
            }
            $oSiteBanner->setAlignment($aData['alignment']);
        }
        if(isset($aData['slogan'])){
            $oSiteBanner->setSlogan($aData['slogan']);
        }
        if(isset($aData['has_info_button'])){
            $oSiteBanner->setHasInfoButton($aData['has_info_button']==1 ? true : false);
        }else{
            $oSiteBanner->setHasInfoButton(false);
        }
        if(isset($aData['info_link'])){
            $oSiteBanner->setInfoLink($aData['info_link']);
        }
        if(isset($aData['has_buy_now'])){
            $oSiteBanner->setHasBuyNow($aData['has_buy_now']==1 ? true : false);
        }else{
            $oSiteBanner->setHasBuyNow(false);
        }
        if(isset($aData['buy_now_link'])){
            $oSiteBanner->setBuyNowLink($aData['buy_now_link']);
        }
        return $oSiteBanner;
    }
}
