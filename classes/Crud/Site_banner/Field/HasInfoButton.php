<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_banner\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteBanner as ModelObject;

class HasInfoButton extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'has_info_button';
    protected $sFieldLabel = 'Heeft info knop';
    private $sGetter = 'getHasInfoButton';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
    }
}
