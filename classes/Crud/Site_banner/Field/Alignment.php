<?php
namespace Crud\Site_banner\Field;

use Crud\Field;
use Crud\IFilterableLookupField;
use InvalidArgumentException;
use Model\Cms\SiteBanner as ModelObject;

class Alignment extends Field implements IFilterableLookupField{

    private $sFieldName = 'alignment';
    protected $sFieldLabel = 'Uitlijning L/R';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getLookups($iSelectedItem = null)
    {
        $aDropdownOptions = [];
        $aDropdownOptions[] = ['id' => 'left', 'selected' => $iSelectedItem == 'left' , 'label' => 'Links'];
        $aDropdownOptions[] = ['id' => 'right', 'selected' => $iSelectedItem == 'right' , 'label' => 'Rechts'];

        return $aDropdownOptions;
    }
    function getVisibleValue($iItemId)
    {
        return $iItemId == 'left' ? 'Links' : 'Rechts';
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->getAlignment().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();

        $sSmartIcon = $mData->getAlignment() == 'left' ? 'align-left' : 'align-right';

        return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $mData->getAlignment(), $aOptions, $bReadonly, $sSmartIcon, 'Maak een keuze');
    }
}
