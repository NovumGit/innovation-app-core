<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_banner\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IEventField;
use Exception\LogicException;
use Model\Cms\SiteBanner as ModelObject;

class ViewImage extends Field implements IEventField{

    protected $sFieldLabel = 'Afbeelding';
    private $sFieldName;


    function getIcon()
    {
        return 'image';
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOut = [];
        $aOut[] = '<td class="xx">';
        if($oModelObject->getFileExt())
        {
            $iId = $oModelObject->getId();
            $sExt = $oModelObject->getFileExt();

            $aOut[] = '     <a target="_blank" title="Afbeelding bekijen" href="/img/banner/'.$iId.'.'.$sExt.'" class="btn btn-success br2 btn-xs fs12 d">';
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
        }
        else
        {
            $aOut[] = '     <a title="Geen afbeelding" href="" class="btn btn-default br2 btn-xs fs12 d">';
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
        }
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Field should not be there in edit view.");
    }
}
