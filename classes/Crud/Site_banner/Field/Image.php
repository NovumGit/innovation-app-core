<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_banner\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteBanner as ModelObject;

class Image extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'image';
    protected $sFieldLabel = 'Afbeelding';
    private $sIcon = 'picture-o';
    private $sPlaceHolder = 'http://infolink.xx';
    private $sGetter = 'getInfoLink';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editFileField($this->sFieldLabel, $this->sFieldName, null, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
