<?php
namespace Crud\Site_banner\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Model\Cms\SiteBanner;

class Edit extends GenericEdit{

    function getEditUrl($oSiteBanner){

        if(!$oSiteBanner instanceof SiteBanner)
        {
            throw new LogicException('Expected an instance of SiteBanner but got '.get_class($oSiteBanner));
        }

        $sSite = $oSiteBanner->getSite()->getDomain();
        return "/cms/banner/edit?site=$sSite&id=".$oSiteBanner->getId();
    }
}