<?php
namespace Crud\MessageTranslationField\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MessageTranslationField\ICollectionField as MessageTranslationFieldField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MessageTranslationField\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MessageTranslationFieldField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MessageTranslationFieldField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MessageTranslationFieldField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MessageTranslationFieldField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MessageTranslationFieldField
	{
		return current($this->aFields);
	}
}
