<?php
namespace Crud\MessageTranslationField\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MessageTranslationField\FieldIterator;
use Crud\MessageTranslationField\Field\FromField;
use Crud\MessageTranslationField\Field\FromString;
use Crud\MessageTranslationField\Field\MessageTranslationId;
use Crud\MessageTranslationField\Field\ToField;
use Exception\LogicException;
use Model\Custom\NovumOverheid\Map\MessageTranslationFieldTableMap;
use Model\Custom\NovumOverheid\MessageTranslationField;
use Model\Custom\NovumOverheid\MessageTranslationFieldQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MessageTranslationField instead if you need to override or add functionality.
 */
abstract class CrudMessageTranslationFieldManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MessageTranslationFieldQuery::create();
	}


	public function getTableMap(): MessageTranslationFieldTableMap
	{
		return new MessageTranslationFieldTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bericht vertaal mapping.";
	}


	public function getEntityTitle(): string
	{
		return "MessageTranslationField";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "message_translation_field toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "message_translation_field aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MessageTranslationId', 'FromField', 'FromString', 'ToField'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MessageTranslationId', 'FromField', 'FromString', 'ToField'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MessageTranslationField
	 */
	public function getModel(array $aData = null): MessageTranslationField
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMessageTranslationFieldQuery = MessageTranslationFieldQuery::create();
		     $oMessageTranslationField = $oMessageTranslationFieldQuery->findOneById($aData['id']);
		     if (!$oMessageTranslationField instanceof MessageTranslationField) {
		         throw new LogicException("MessageTranslationField should be an instance of MessageTranslationField but got something else." . __METHOD__);
		     }
		     $oMessageTranslationField = $this->fillVo($aData, $oMessageTranslationField);
		}
		else {
		     $oMessageTranslationField = new MessageTranslationField();
		     if (!empty($aData)) {
		         $oMessageTranslationField = $this->fillVo($aData, $oMessageTranslationField);
		     }
		}
		return $oMessageTranslationField;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MessageTranslationField
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MessageTranslationField
	{
		$oMessageTranslationField = $this->getModel($aData);


		 if(!empty($oMessageTranslationField))
		 {
		     $oMessageTranslationField = $this->fillVo($aData, $oMessageTranslationField);
		     $oMessageTranslationField->save();
		 }
		return $oMessageTranslationField;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MessageTranslationField $oModel
	 * @return MessageTranslationField
	 */
	protected function fillVo(array $aData, MessageTranslationField $oModel): MessageTranslationField
	{
		if(isset($aData['message_translation_id'])) {
		     $oField = new MessageTranslationId();
		     $mValue = $oField->sanitize($aData['message_translation_id']);
		     $oModel->setMessageTranslationId($mValue);
		}
		if(isset($aData['from_field'])) {
		     $oField = new FromField();
		     $mValue = $oField->sanitize($aData['from_field']);
		     $oModel->setFromField($mValue);
		}
		if(isset($aData['from_string'])) {
		     $oField = new FromString();
		     $mValue = $oField->sanitize($aData['from_string']);
		     $oModel->setFromString($mValue);
		}
		if(isset($aData['to_field'])) {
		     $oField = new ToField();
		     $mValue = $oField->sanitize($aData['to_field']);
		     $oModel->setToField($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
