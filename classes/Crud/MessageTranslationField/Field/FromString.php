<?php
namespace Crud\MessageTranslationField\Field;

use Crud\MessageTranslationField\Field\Base\FromString as BaseFromString;

/**
 * Skeleton subclass for representing from_string field from the message_translation_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:06:03
 */
final class FromString extends BaseFromString
{
}
