<?php
namespace Crud\MessageTranslationField\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\MessageTranslationField\ICollectionField;
use Model\Custom\NovumOverheid\MessageTranslationQuery;

/**
 * Base class that represents the 'message_translation_id' crud field from the 'message_translation_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class MessageTranslationId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'message_translation_id';
	protected $sFieldLabel = 'Bericht vertaling';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMessageTranslationId';
	protected $sFqModelClassname = '\\\Model\Custom\NovumOverheid\MessageTranslationField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Custom\NovumOverheid\MessageTranslationQuery::create()->orderBytitle()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "gettitle", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Custom\NovumOverheid\MessageTranslationQuery::create()->findOneById($iItemId)->gettitle();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
