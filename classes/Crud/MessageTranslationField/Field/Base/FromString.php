<?php
namespace Crud\MessageTranslationField\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MessageTranslationField\ICollectionField;

/**
 * Base class that represents the 'from_string' crud field from the 'message_translation_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class FromString extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'from_string';
	protected $sFieldLabel = 'Tekst meesturen';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFromString';
	protected $sFqModelClassname = '\\\Model\Custom\NovumOverheid\MessageTranslationField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
