<?php
namespace Crud\MessageTranslationField\Field\Base;

use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MessageTranslationField\ICollectionField;

/**
 * Base class that represents the 'from_field' crud field from the 'message_translation_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class FromField extends GenericLookup implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'from_field';
	protected $sFieldLabel = 'Translation definition';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFromField';
	protected $sFqModelClassname = '\\\Model\Custom\NovumOverheid\MessageTranslationField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
