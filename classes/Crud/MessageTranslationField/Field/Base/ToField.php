<?php
namespace Crud\MessageTranslationField\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MessageTranslationField\ICollectionField;

/**
 * Base class that represents the 'to_field' crud field from the 'message_translation_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class ToField extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'to_field';
	protected $sFieldLabel = 'Bestemming veld';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getToField';
	protected $sFqModelClassname = '\\\Model\Custom\NovumOverheid\MessageTranslationField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
