<?php 
namespace Crud\MessageTranslationField\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumOverheid\MessageTranslationField;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof MessageTranslationField)
		{
		     return "//services/message_translation_field/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof MessageTranslationField)
		{
		     return "//message_translation_field?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
