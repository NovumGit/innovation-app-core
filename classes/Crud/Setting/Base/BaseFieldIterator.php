<?php
namespace Crud\Setting\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Setting\ICollectionField as SettingField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Setting\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SettingField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SettingField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SettingField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SettingField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SettingField
	{
		return current($this->aFields);
	}
}
