<?php
namespace Crud\Setting\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Setting\FieldIterator;
use Crud\Setting\Field\City;
use Crud\Setting\Field\CountryId;
use Crud\Setting\Field\TimezoneId;
use Crud\Setting\Field\UnitOfLength;
use Crud\Setting\Field\UnitOfLiquid;
use Exception\LogicException;
use Model\Setting\Map\SettingTableMap;
use Model\Setting\Setting;
use Model\Setting\SettingQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Setting instead if you need to override or add functionality.
 */
abstract class CrudSettingManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SettingQuery::create();
	}


	public function getTableMap(): SettingTableMap
	{
		return new SettingTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Setting";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "setting toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "setting aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['TimezoneId', 'CountryId', 'City', 'UnitOfLength', 'UnitOfLiquid'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['TimezoneId', 'CountryId', 'City', 'UnitOfLength', 'UnitOfLiquid'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Setting
	 */
	public function getModel(array $aData = null): Setting
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSettingQuery = SettingQuery::create();
		     $oSetting = $oSettingQuery->findOneById($aData['id']);
		     if (!$oSetting instanceof Setting) {
		         throw new LogicException("Setting should be an instance of Setting but got something else." . __METHOD__);
		     }
		     $oSetting = $this->fillVo($aData, $oSetting);
		}
		else {
		     $oSetting = new Setting();
		     if (!empty($aData)) {
		         $oSetting = $this->fillVo($aData, $oSetting);
		     }
		}
		return $oSetting;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Setting
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Setting
	{
		$oSetting = $this->getModel($aData);


		 if(!empty($oSetting))
		 {
		     $oSetting = $this->fillVo($aData, $oSetting);
		     $oSetting->save();
		 }
		return $oSetting;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Setting $oModel
	 * @return Setting
	 */
	protected function fillVo(array $aData, Setting $oModel): Setting
	{
		if(isset($aData['timezone_id'])) {
		     $oField = new TimezoneId();
		     $mValue = $oField->sanitize($aData['timezone_id']);
		     $oModel->setTimezoneId($mValue);
		}
		if(isset($aData['country_id'])) {
		     $oField = new CountryId();
		     $mValue = $oField->sanitize($aData['country_id']);
		     $oModel->setCountryId($mValue);
		}
		if(isset($aData['city'])) {
		     $oField = new City();
		     $mValue = $oField->sanitize($aData['city']);
		     $oModel->setCity($mValue);
		}
		if(isset($aData['unit_of_length'])) {
		     $oField = new UnitOfLength();
		     $mValue = $oField->sanitize($aData['unit_of_length']);
		     $oModel->setUnitOfLength($mValue);
		}
		if(isset($aData['unit_of_liquid'])) {
		     $oField = new UnitOfLiquid();
		     $mValue = $oField->sanitize($aData['unit_of_liquid']);
		     $oModel->setUnitOfLiquid($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
