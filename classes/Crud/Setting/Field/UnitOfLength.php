<?php
/**
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Crud\Setting\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;

use Model\Setting\Setting as ModelObject;


class UnitOfLength extends Field implements IFilterableLookupField{

    protected $sFieldName = 'unit_of_length';
    protected $sFieldLabel = 'Unit of length';
    private $sIcon = '';
    private $sPlaceHolder = '';
    private $sGetter = 'getUnitOfLength';

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getLookups($mSelectedItem = null)
    {
        $aPriceLevels = " . QueryObject::class . "::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aPriceLevels, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return PriceLevelQuery::create()->findOneById($iItemId)->getName();;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

}
