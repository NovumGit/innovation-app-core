<?php
/**
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Crud\Setting\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Setting\Setting as ModelObject;


class City extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'city';
    protected $sFieldLabel = 'City';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getCity';

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getLookups($mSelectedItem = null)
    {
        $aOptions = [];
        /*
        $aPriceLevels = " . QueryObject::class . "::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aPriceLevels, 'getName', $mSelectedItem);
        */
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return PriceLevelQuery::create()->findOneById($iItemId)->getName();
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

}
