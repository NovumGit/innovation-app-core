<?php
namespace Crud\Setting\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Setting\ICollectionField;

/**
 * Base class that represents the 'unit_of_length' crud field from the 'setting' table.
 * This class is auto generated and should not be modified.
 */
abstract class UnitOfLength extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'unit_of_length';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUnitOfLength';
	protected $sFqModelClassname = '\\\Model\Setting\Setting';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
