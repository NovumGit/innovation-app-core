<?php
namespace Crud\Setting\Field;

use Crud\Setting\Field\Base\CountryId as BaseCountryId;

/**
 * Skeleton subclass for representing country_id field from the setting table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class CountryId extends BaseCountryId
{
}
