<?php
namespace Crud\RelatedProduct\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\RelatedProduct\ICollectionField as RelatedProductField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\RelatedProduct\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param RelatedProductField[] $aFields */
	private array $aFields = [];


	/**
	 * @param RelatedProductField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof RelatedProductField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(RelatedProductField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): RelatedProductField
	{
		return current($this->aFields);
	}
}
