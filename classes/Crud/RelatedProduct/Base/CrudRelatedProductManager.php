<?php
namespace Crud\RelatedProduct\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\RelatedProduct\FieldIterator;
use Crud\RelatedProduct\Field\OtherProductId;
use Crud\RelatedProduct\Field\ProductId;
use Crud\RelatedProduct\Field\Sorting;
use Exception\LogicException;
use Model\Product\Map\RelatedProductTableMap;
use Model\Product\RelatedProduct;
use Model\Product\RelatedProductQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify RelatedProduct instead if you need to override or add functionality.
 */
abstract class CrudRelatedProductManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return RelatedProductQuery::create();
	}


	public function getTableMap(): RelatedProductTableMap
	{
		return new RelatedProductTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "RelatedProduct";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "related_product toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "related_product aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'OtherProductId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'OtherProductId', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return RelatedProduct
	 */
	public function getModel(array $aData = null): RelatedProduct
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oRelatedProductQuery = RelatedProductQuery::create();
		     $oRelatedProduct = $oRelatedProductQuery->findOneById($aData['id']);
		     if (!$oRelatedProduct instanceof RelatedProduct) {
		         throw new LogicException("RelatedProduct should be an instance of RelatedProduct but got something else." . __METHOD__);
		     }
		     $oRelatedProduct = $this->fillVo($aData, $oRelatedProduct);
		}
		else {
		     $oRelatedProduct = new RelatedProduct();
		     if (!empty($aData)) {
		         $oRelatedProduct = $this->fillVo($aData, $oRelatedProduct);
		     }
		}
		return $oRelatedProduct;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return RelatedProduct
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): RelatedProduct
	{
		$oRelatedProduct = $this->getModel($aData);


		 if(!empty($oRelatedProduct))
		 {
		     $oRelatedProduct = $this->fillVo($aData, $oRelatedProduct);
		     $oRelatedProduct->save();
		 }
		return $oRelatedProduct;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param RelatedProduct $oModel
	 * @return RelatedProduct
	 */
	protected function fillVo(array $aData, RelatedProduct $oModel): RelatedProduct
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['other_product_id'])) {
		     $oField = new OtherProductId();
		     $mValue = $oField->sanitize($aData['other_product_id']);
		     $oModel->setOtherProductId($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
