<?php
namespace Crud\Promo_product_translation;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\PromoProductTranslation;
use Model\Setting\MasterTable\ProductUnitTranslation;
use Model\Setting\MasterTable\ProductUnitTranslationQuery;

class CrudPromo_product_translationManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        $sSite = $_GET['site'];
        return "/cms/promoproducts/edit?site=$sSite";
    }

    function getEntityTitle():string
    {
        return 'eenheid';
    }
    function getNewFormTitle():string
    {
        return 'Promo product vertaling toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Promo product vertaling bewerken';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {

        return [
            'Title',
            'LanguageId',
            'Delete',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name',
            'NamePlural'
        ];
    }
    function getModel(array $aData = null)
    {
        exit('implement'.__METHOD__.':'.__LINE__);
        $oPromoProductTranslation = null;
        if(isset($aData['id']) && $aData['id'])
        {
            $oPromoProductTranslation = PromoProductTranslation::create()->findOneById($aData['id']);
        }

        if($oPromoProductTranslation instanceof PromoProductTranslation &&  isset($aData['language_id']))
        {
            $oProductUnitTranslationQuery = ProductUnitTranslationQuery::create();
            $oProductUnitTranslationQuery->filterByLanguageId($aData['language_id']);
            $oProductUnitTranslationQuery->filterByUnitId($aData['unit_id']);
            $oProductUnitTranslation = $oProductUnitTranslationQuery->findOne();
        }

        if(!$oProductUnitTranslation instanceof ProductUnitTranslation)
        {
            $oProductUnitTranslation = new ProductUnitTranslation();
        }


        if(!empty($aData))
        {
            $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
        }

        return $oProductUnitTranslation;
    }


    private function fillVo($aData, ProductUnitTranslation $oProductUnitTranslation)
    {
        exit('implement'.__METHOD__.':'.__LINE__);
        if(isset($aData['name'])){$oProductUnitTranslation->setName($aData['name']);}
        if(isset($aData['name_plural'])){$oProductUnitTranslation->setNamePlural($aData['name_plural']);}
        if(isset($aData['alt_url'])){$oProductUnitTranslation->setNamePlural($aData['alt_url']);}


        if(isset($aData['language_id'])){$oProductUnitTranslation->setLanguageId($aData['language_id']);}
        if(isset($aData['unit_id'])){$oProductUnitTranslation->setUnitId($aData['unit_id']);}

        return $oProductUnitTranslation;
    }
    function store(array $aData = null)
    {
        exit('implement'.__METHOD__.':'.__LINE__);
        $oProductUnitTranslation = $this->getModel($aData);
        $oProductUnitTranslation = $this->fillVo($aData, $oProductUnitTranslation);
        if(!empty($oProductUnitTranslation))
        {
            $oProductUnitTranslation->save();
        }
        return $oProductUnitTranslation;
    }
}
