<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Promo_product_translation\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;

use Model\PromoProductTranslation as ModelObject;


class LanguageId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'language_id';
    protected $sFieldLabel = 'Taal';
    private $sIcon = 'globe';
    private $sPlaceHolder = 'lookup';
    private $sGetter = 'getLanguageId';


                function getLookups($mSelectedItem = null)
                {
                    $aPriceLevels = Model\PromoProductTranslationQuery::create()->orderByName()->find();
                    $aOptions = Utils::makeSelectOptions($aPriceLevels, 'getName', $mSelectedItem);
                    return $aOptions;
                }
                function getVisibleValue($iItemId)
                {
                    return PriceLevelQuery::create()->findOneById($iItemId)->getName();;
                }


    function getDataType():string
    {
        return 'lookup';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $this->sIcon, 'Maak een keuze');
    }
}
