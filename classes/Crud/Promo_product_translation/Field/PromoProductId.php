<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Promo_product_translation\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\PromoProductTranslation as ModelObject;


class PromoProductId extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'promo_product_id';
    protected $sFieldLabel = 'To which promotion does this belong';
    private $sIcon = 'info';
    private $sPlaceHolder = '';
    private $sGetter = 'getPromoProductId';



    function getDataType():string
    {
        return '';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
