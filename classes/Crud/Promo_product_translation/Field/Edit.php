<?php
namespace Crud\Promo_product_translation\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Product;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oProduct){

        if(!$oProduct instanceof Product)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\Product but got '.get_class($oProduct));
        }

        return '/product/edit?id='.$oProduct->getId();
    }
}