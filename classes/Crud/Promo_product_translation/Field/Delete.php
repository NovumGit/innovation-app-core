<?php
namespace Crud\Promo_product_translation\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\Base\PromoProduct;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oPromoProduct){

        if(!$oPromoProduct instanceof PromoProduct)
        {
            throw new InvalidArgumentException('Expected an instance of PromoProduct but got '.get_class($oPromoProduct));
        }

        throw new LogicException("Not implemented yet");
        return '/product/edit?_do=Delete&id='.$oPromoProduct->get();
    }

    function getUnDeleteUrl($oPromoProduct){

        if(!$oPromoProduct instanceof PromoProduct)
        {
            throw new InvalidArgumentException('Expected an instance of PromoProduct but got '.get_class($oPromoProduct));
        }

        throw new LogicException("Not implemented yet");
        return '/product/edit?_do=Undelete&id='.$oProduct->getId();
    }
}