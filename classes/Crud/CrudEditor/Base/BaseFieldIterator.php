<?php
namespace Crud\CrudEditor\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudEditor\ICollectionField as CrudEditorField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudEditor\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudEditorField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudEditorField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudEditorField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudEditorField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudEditorField
	{
		return current($this->aFields);
	}
}
