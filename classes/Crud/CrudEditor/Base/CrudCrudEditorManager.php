<?php
namespace Crud\CrudEditor\Base;

use Core\Utils;
use Crud;
use Crud\CrudEditor\FieldIterator;
use Crud\CrudEditor\Field\CrudConfigId;
use Crud\CrudEditor\Field\Name;
use Crud\CrudEditor\Field\PrivateDescription;
use Crud\CrudEditor\Field\PublicDescription;
use Crud\CrudEditor\Field\Title;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorQuery;
use Model\Setting\CrudManager\Map\CrudEditorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudEditor instead if you need to override or add functionality.
 */
abstract class CrudCrudEditorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorQuery::create();
	}


	public function getTableMap(): CrudEditorTableMap
	{
		return new CrudEditorTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint formulierdefinities opgeslagen";
	}


	public function getEntityTitle(): string
	{
		return "CrudEditor";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_editor toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_editor aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudConfigId', 'Name', 'Title', 'PublicDescription', 'PrivateDescription'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudConfigId', 'Name', 'Title', 'PublicDescription', 'PrivateDescription'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudEditor
	 */
	public function getModel(array $aData = null): CrudEditor
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudEditorQuery = CrudEditorQuery::create();
		     $oCrudEditor = $oCrudEditorQuery->findOneById($aData['id']);
		     if (!$oCrudEditor instanceof CrudEditor) {
		         throw new LogicException("CrudEditor should be an instance of CrudEditor but got something else." . __METHOD__);
		     }
		     $oCrudEditor = $this->fillVo($aData, $oCrudEditor);
		}
		else {
		     $oCrudEditor = new CrudEditor();
		     if (!empty($aData)) {
		         $oCrudEditor = $this->fillVo($aData, $oCrudEditor);
		     }
		}
		return $oCrudEditor;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudEditor
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudEditor
	{
		$oCrudEditor = $this->getModel($aData);


		 if(!empty($oCrudEditor))
		 {
		     $oCrudEditor = $this->fillVo($aData, $oCrudEditor);
		     $oCrudEditor->save();
		 }
		return $oCrudEditor;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudEditor $oModel
	 * @return CrudEditor
	 */
	protected function fillVo(array $aData, CrudEditor $oModel): CrudEditor
	{
		if(isset($aData['crud_config_id'])) {
		     $oField = new CrudConfigId();
		     $mValue = $oField->sanitize($aData['crud_config_id']);
		     $oModel->setCrudConfigId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['public_description'])) {
		     $oField = new PublicDescription();
		     $mValue = $oField->sanitize($aData['public_description']);
		     $oModel->setPublicDescription($mValue);
		}
		if(isset($aData['private_description'])) {
		     $oField = new PrivateDescription();
		     $mValue = $oField->sanitize($aData['private_description']);
		     $oModel->setPrivateDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
