<?php
namespace Crud\CrudEditor\Field;

use Crud\CrudEditor\Field\Base\PublicDescription as BasePublicDescription;

/**
 * Skeleton subclass for representing public_description field from the crud_editor table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class PublicDescription extends BasePublicDescription
{
}
