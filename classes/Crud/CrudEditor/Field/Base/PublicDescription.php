<?php
namespace Crud\CrudEditor\Field\Base;

use Crud\CrudEditor\ICollectionField;
use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'public_description' crud field from the 'crud_editor' table.
 * This class is auto generated and should not be modified.
 */
abstract class PublicDescription extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'public_description';
	protected $sFieldLabel = 'Omschrijving (publiek)';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPublicDescription';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudEditor';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
