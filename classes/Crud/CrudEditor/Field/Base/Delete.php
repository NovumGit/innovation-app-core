<?php 
namespace Crud\CrudEditor\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudEditor;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditor)
		{
		     return "//system/crud_editor/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudEditor)
		{
		     return "//crud_editor?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
