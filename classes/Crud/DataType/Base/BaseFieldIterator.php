<?php
namespace Crud\DataType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataType\ICollectionField as DataTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataTypeField
	{
		return current($this->aFields);
	}
}
