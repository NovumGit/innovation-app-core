<?php
namespace Crud\DataType\Base;

use Core\Utils;
use Crud;
use Crud\DataType\FieldIterator;
use Crud\DataType\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataModel\DataType;
use Model\System\DataModel\DataTypeQuery;
use Model\System\DataModel\Map\DataTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataType instead if you need to override or add functionality.
 */
abstract class CrudDataTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataTypeQuery::create();
	}


	public function getTableMap(): DataTypeTableMap
	{
		return new DataTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint beschijft hoe gegevens worden opgeslagen .";
	}


	public function getEntityTitle(): string
	{
		return "DataType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "data_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "data_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataType
	 */
	public function getModel(array $aData = null): DataType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataTypeQuery = DataTypeQuery::create();
		     $oDataType = $oDataTypeQuery->findOneById($aData['id']);
		     if (!$oDataType instanceof DataType) {
		         throw new LogicException("DataType should be an instance of DataType but got something else." . __METHOD__);
		     }
		     $oDataType = $this->fillVo($aData, $oDataType);
		}
		else {
		     $oDataType = new DataType();
		     if (!empty($aData)) {
		         $oDataType = $this->fillVo($aData, $oDataType);
		     }
		}
		return $oDataType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataType
	{
		$oDataType = $this->getModel($aData);


		 if(!empty($oDataType))
		 {
		     $oDataType = $this->fillVo($aData, $oDataType);
		     $oDataType->save();
		 }
		return $oDataType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataType $oModel
	 * @return DataType
	 */
	protected function fillVo(array $aData, DataType $oModel): DataType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
