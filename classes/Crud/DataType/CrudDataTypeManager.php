<?php
namespace Crud\DataType;

/**
 * Skeleton subclass for representing a DataType.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudDataTypeManager extends Base\CrudDataTypeManager
{
}
