<?php
namespace Crud\MailingListsCriteria\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailingListsCriteria\FieldIterator;
use Crud\MailingListsCriteria\Field\FilterName;
use Crud\MailingListsCriteria\Field\FilterOperatorId;
use Crud\MailingListsCriteria\Field\FilterValue;
use Crud\MailingListsCriteria\Field\MailingListId;
use Exception\LogicException;
use Model\Marketing\MailingListsCriteria;
use Model\Marketing\MailingListsCriteriaQuery;
use Model\Marketing\Map\MailingListsCriteriaTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailingListsCriteria instead if you need to override or add functionality.
 */
abstract class CrudMailingListsCriteriaManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingListsCriteriaQuery::create();
	}


	public function getTableMap(): MailingListsCriteriaTableMap
	{
		return new MailingListsCriteriaTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailingListsCriteria";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing_list_criteria toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing_list_criteria aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingListId', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailingListId', 'FilterOperatorId', 'FilterName', 'FilterValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailingListsCriteria
	 */
	public function getModel(array $aData = null): MailingListsCriteria
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingListsCriteriaQuery = MailingListsCriteriaQuery::create();
		     $oMailingListsCriteria = $oMailingListsCriteriaQuery->findOneById($aData['id']);
		     if (!$oMailingListsCriteria instanceof MailingListsCriteria) {
		         throw new LogicException("MailingListsCriteria should be an instance of MailingListsCriteria but got something else." . __METHOD__);
		     }
		     $oMailingListsCriteria = $this->fillVo($aData, $oMailingListsCriteria);
		}
		else {
		     $oMailingListsCriteria = new MailingListsCriteria();
		     if (!empty($aData)) {
		         $oMailingListsCriteria = $this->fillVo($aData, $oMailingListsCriteria);
		     }
		}
		return $oMailingListsCriteria;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailingListsCriteria
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailingListsCriteria
	{
		$oMailingListsCriteria = $this->getModel($aData);


		 if(!empty($oMailingListsCriteria))
		 {
		     $oMailingListsCriteria = $this->fillVo($aData, $oMailingListsCriteria);
		     $oMailingListsCriteria->save();
		 }
		return $oMailingListsCriteria;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailingListsCriteria $oModel
	 * @return MailingListsCriteria
	 */
	protected function fillVo(array $aData, MailingListsCriteria $oModel): MailingListsCriteria
	{
		if(isset($aData['mailing_list_id'])) {
		     $oField = new MailingListId();
		     $mValue = $oField->sanitize($aData['mailing_list_id']);
		     $oModel->setMailingListId($mValue);
		}
		if(isset($aData['filter_operator_id'])) {
		     $oField = new FilterOperatorId();
		     $mValue = $oField->sanitize($aData['filter_operator_id']);
		     $oModel->setFilterOperatorId($mValue);
		}
		if(isset($aData['filter_name'])) {
		     $oField = new FilterName();
		     $mValue = $oField->sanitize($aData['filter_name']);
		     $oModel->setFilterName($mValue);
		}
		if(isset($aData['filter_value'])) {
		     $oField = new FilterValue();
		     $mValue = $oField->sanitize($aData['filter_value']);
		     $oModel->setFilterValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
