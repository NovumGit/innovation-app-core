<?php
namespace Crud\MailingListsCriteria\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailingListsCriteria\ICollectionField as MailingListsCriteriaField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailingListsCriteria\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailingListsCriteriaField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailingListsCriteriaField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailingListsCriteriaField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailingListsCriteriaField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailingListsCriteriaField
	{
		return current($this->aFields);
	}
}
