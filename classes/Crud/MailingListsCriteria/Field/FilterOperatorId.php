<?php
namespace Crud\MailingListsCriteria\Field;

use Crud\MailingListsCriteria\Field\Base\FilterOperatorId as BaseFilterOperatorId;

/**
 * Skeleton subclass for representing filter_operator_id field from the mailing_list_criteria table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class FilterOperatorId extends BaseFilterOperatorId
{
}
