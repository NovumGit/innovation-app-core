<?php
namespace Crud\Coupon_type\Field;

use Crud\Generic\Field\GenericDelete;
use InvalidArgumentException;
use Model\Marketing\CouponType;

class Delete extends GenericDelete
{
    function getDeleteUrl($oObject)
    {
        if(!$oObject instanceof CouponType)
        {
            throw new InvalidArgumentException('Expected an instance of CouponType but got '.get_class($oObject));
        }
        return '/marketing/coupon/edit?_do=Delete&id='.$oObject->getId();
    }
    function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}