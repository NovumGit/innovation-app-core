<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Coupon_type\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Marketing\CouponType as ModelObject;

class Coupon_type extends Field implements IFilterableLookupField{

    protected $sFieldName = 'coupon_type';
    protected $sFieldLabel = 'Soort coupon';
    private $sIcon = 'money';
    private $sGetter = 'getCouponType';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getLookups($mSelectedItem = null)
    {
        $aOptions = [];
        $aOptions[] = ['id' => 'percentage', 'selected' => false ,'label' => 'percentage'];
        $aOptions[] = ['id' => 'amount', 'selected' => false ,'label' => 'amount'];

        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        return $iItemId;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(empty($aPostedData['coupon_type']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode('Geef alstublieft het soort korting op.');
        }
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
