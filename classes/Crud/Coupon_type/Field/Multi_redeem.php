<?php
namespace Crud\Coupon_type\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Marketing\CouponType as ModelObject;

class Multi_redeem extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'multi_redeem';
    protected $sFieldLabel = 'Herbruikbaar';
    private $sGetter = 'getMultiRedeem';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'boolean';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {

        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sVisibleValue = $oModelObject->getMultiRedeem() ? 'Ja' : 'Nee';
        return '<td class="">'.$sVisibleValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
