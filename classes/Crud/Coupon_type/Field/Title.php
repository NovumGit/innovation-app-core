<?php
namespace Crud\Coupon_type\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Marketing\CouponType as ModelObject;

class Title extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'title';
    protected $sFieldLabel = 'Titel';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getTitle';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['title']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("Geef alstublieft een titel op");
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon);
    }
}
