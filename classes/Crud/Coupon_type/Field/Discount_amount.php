<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Coupon_type\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Marketing\CouponType as ModelObject;

class Discount_amount extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'discount_amount';
    protected $sFieldLabel = 'Kortings bedrag';
    private $sIcon = 'money';
    private $sPlaceHolder = '';
    private $sGetter = 'getDiscountAmount';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sVisibleValue = '€ '.number_format($oModelObject->getDiscountAmount(), 2, ',', '.');
        return '<td class="">'.$sVisibleValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon);
    }
}
