<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Coupon_type\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Core\Config;
use DateTime;
use Model\Marketing\CouponType as ModelObject;


class Valid_after extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'valid_after';
    protected $sFieldLabel = 'Bruikbaar na';
    private $sGetter = 'getValidAfter';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['valid_after']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("Geef alstublieft een datum op waarna deze coupon geldig is.");

        }
        else if(!preg_match('/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $aPostedData['valid_after']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("De bruikbaar na datum moet in het format dd-mm-yyyy worden opgegeven.");
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oValid_afterDate = $oModelObject->{$this->sGetter}();

        $sValid_afterDate = '<span style="color:red">'.Translate::fromCode('Niet ingevuld').'</span>';
        if($oValid_afterDate instanceof DateTime)
        {
            $sValid_afterDate = $oValid_afterDate->format(Config::getDateFormat());
        }
        return '<td class="">'.$sValid_afterDate.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editDatePicker($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}());
    }
}
