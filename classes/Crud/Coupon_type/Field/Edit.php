<?php
namespace Crud\Coupon_type\Field;

use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;
use Model\Marketing\CouponType;

class Edit extends GenericEdit
{
    function getEditUrl($oObject)
    {
        if(!$oObject instanceof CouponType)
        {
            throw new InvalidArgumentException('Expected an instance of CouponType but got '.get_class($oObject));
        }
        return '/marketing/coupon/edit?id='.$oObject->getId();
    }
}