<?php
namespace Crud\Coupon_type;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Marketing\CouponTypeQuery;
use Model\Marketing\CouponType;
use Exception\LogicException;

class CrudCoupon_typeManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'coupon';
    }
    function getOverviewUrl():string
    {
        return '/marketing/coupon/overview';
    }
    function getCreateNewUrl():string
    {
        return '/marketing/coupon/edit';
    }
    function getNewFormTitle():string{
        return 'Coupon toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Coupon wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Title',
            'Coupon_type',
            'Multi_redeem',
            'Discount_amount',
            'Discount_percentage',
            'Valid_after',
            'Valid_until',
            'Delete',
            'Edit'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Title',
            'Coupon_type',
            'Multi_redeem',
            'Discount_amount',
            'Discount_percentage',
            'Valid_after',
            'Valid_until'
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oCouponType = CouponTypeQuery::create()->findOneById($aData['id']);
            if(!$oCouponType instanceof CouponType)
            {
                throw new LogicException("\$CouponType should be an instance of CouponType but got ".get_class($oCouponType)." in ".__METHOD__);
            }
            $oCouponType = $this->fillVo($aData, $oCouponType);
        }
        else
        {
            $oCouponType = new CouponType();
            if(!empty($aData))
            {
                $oCouponType = $this->fillVo($aData, $oCouponType);
            }
        }
        return $oCouponType;
    }
    function store(array $aData = null)
    {
        $oCouponType = $this->getModel($aData);
        if(!empty($oCouponType))
        {
            $oCouponType = $this->fillVo($aData, $oCouponType);
            $oCouponType->save();
        }
        return $oCouponType;
    }
    public function fillVo($aData, CouponType $oCouponType)
    {
        if(isset($aData['title'])){$oCouponType->setTitle($aData['title']);}
        if(isset($aData['coupon_type'])){$oCouponType->setCouponType($aData['coupon_type']);}
        if(isset($aData['multi_redeem'])){$oCouponType->setMultiRedeem($aData['multi_redeem']);}
        if(isset($aData['discount_amount'])){$oCouponType->setDiscountAmount($aData['discount_amount']);}
        if(isset($aData['discount_percentage'])){$oCouponType->setDiscountPercentage($aData['discount_percentage']);}
        if(isset($aData['valid_after'])){$oCouponType->setValidAfter($aData['valid_after']);}
        if(isset($aData['valid_until'])){$oCouponType->setValidUntil($aData['valid_until']);}
        return $oCouponType;
    }
}
