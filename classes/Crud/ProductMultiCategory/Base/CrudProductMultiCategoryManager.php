<?php
namespace Crud\ProductMultiCategory\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductMultiCategory\FieldIterator;
use Crud\ProductMultiCategory\Field\CategoryId;
use Crud\ProductMultiCategory\Field\ProductId;
use Crud\ProductMultiCategory\Field\SortingOrder;
use Exception\LogicException;
use Model\Category\Map\ProductMultiCategoryTableMap;
use Model\Category\ProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductMultiCategory instead if you need to override or add functionality.
 */
abstract class CrudProductMultiCategoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductMultiCategoryQuery::create();
	}


	public function getTableMap(): ProductMultiCategoryTableMap
	{
		return new ProductMultiCategoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductMultiCategory";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_multi_category toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_multi_category aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'CategoryId', 'SortingOrder'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'CategoryId', 'SortingOrder'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductMultiCategory
	 */
	public function getModel(array $aData = null): ProductMultiCategory
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductMultiCategoryQuery = ProductMultiCategoryQuery::create();
		     $oProductMultiCategory = $oProductMultiCategoryQuery->findOneById($aData['id']);
		     if (!$oProductMultiCategory instanceof ProductMultiCategory) {
		         throw new LogicException("ProductMultiCategory should be an instance of ProductMultiCategory but got something else." . __METHOD__);
		     }
		     $oProductMultiCategory = $this->fillVo($aData, $oProductMultiCategory);
		}
		else {
		     $oProductMultiCategory = new ProductMultiCategory();
		     if (!empty($aData)) {
		         $oProductMultiCategory = $this->fillVo($aData, $oProductMultiCategory);
		     }
		}
		return $oProductMultiCategory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductMultiCategory
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductMultiCategory
	{
		$oProductMultiCategory = $this->getModel($aData);


		 if(!empty($oProductMultiCategory))
		 {
		     $oProductMultiCategory = $this->fillVo($aData, $oProductMultiCategory);
		     $oProductMultiCategory->save();
		 }
		return $oProductMultiCategory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductMultiCategory $oModel
	 * @return ProductMultiCategory
	 */
	protected function fillVo(array $aData, ProductMultiCategory $oModel): ProductMultiCategory
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['category_id'])) {
		     $oField = new CategoryId();
		     $mValue = $oField->sanitize($aData['category_id']);
		     $oModel->setCategoryId($mValue);
		}
		if(isset($aData['sorting_order'])) {
		     $oField = new SortingOrder();
		     $mValue = $oField->sanitize($aData['sorting_order']);
		     $oModel->setSortingOrder($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
