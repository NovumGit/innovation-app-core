<?php
namespace Crud\ProductMultiCategory\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductMultiCategory\ICollectionField;

/**
 * Base class that represents the 'sorting_order' crud field from the 'product_multi_category' table.
 * This class is auto generated and should not be modified.
 */
abstract class SortingOrder extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sorting_order';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSortingOrder';
	protected $sFqModelClassname = '\\\Model\Category\ProductMultiCategory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
