<?php
namespace Crud\ProductMultiCategory\Field;

use Crud\ProductMultiCategory\Field\Base\CategoryId as BaseCategoryId;

/**
 * Skeleton subclass for representing category_id field from the product_multi_category table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class CategoryId extends BaseCategoryId
{
}
