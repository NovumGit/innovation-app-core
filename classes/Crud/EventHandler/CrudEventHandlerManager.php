<?php
namespace Crud\EventHandler;

/**
 * Skeleton subclass for representing a EventHandler.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudEventHandlerManager extends Base\CrudEventHandlerManager
{
}
