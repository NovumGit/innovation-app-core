<?php
namespace Crud\EventHandler\Base;

use Core\Utils;
use Crud;
use Crud\EventHandler\FieldIterator;
use Crud\EventHandler\Field\CrudConfigId;
use Crud\EventHandler\Field\EventId;
use Crud\EventHandler\Field\EventTypeId;
use Crud\EventHandler\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\Event\EventHandler;
use Model\System\Event\EventHandlerQuery;
use Model\System\Event\Map\EventHandlerTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify EventHandler instead if you need to override or add functionality.
 */
abstract class CrudEventHandlerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return EventHandlerQuery::create();
	}


	public function getTableMap(): EventHandlerTableMap
	{
		return new EventHandlerTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat de defintitie van de uitvoering van een event";
	}


	public function getEntityTitle(): string
	{
		return "EventHandler";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "event_handler toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "event_handler aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'EventId', 'EventTypeId', 'CrudConfigId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'EventId', 'EventTypeId', 'CrudConfigId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return EventHandler
	 */
	public function getModel(array $aData = null): EventHandler
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oEventHandlerQuery = EventHandlerQuery::create();
		     $oEventHandler = $oEventHandlerQuery->findOneById($aData['id']);
		     if (!$oEventHandler instanceof EventHandler) {
		         throw new LogicException("EventHandler should be an instance of EventHandler but got something else." . __METHOD__);
		     }
		     $oEventHandler = $this->fillVo($aData, $oEventHandler);
		}
		else {
		     $oEventHandler = new EventHandler();
		     if (!empty($aData)) {
		         $oEventHandler = $this->fillVo($aData, $oEventHandler);
		     }
		}
		return $oEventHandler;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return EventHandler
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): EventHandler
	{
		$oEventHandler = $this->getModel($aData);


		 if(!empty($oEventHandler))
		 {
		     $oEventHandler = $this->fillVo($aData, $oEventHandler);
		     $oEventHandler->save();
		 }
		return $oEventHandler;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param EventHandler $oModel
	 * @return EventHandler
	 */
	protected function fillVo(array $aData, EventHandler $oModel): EventHandler
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['event_id'])) {
		     $oField = new EventId();
		     $mValue = $oField->sanitize($aData['event_id']);
		     $oModel->setEventId($mValue);
		}
		if(isset($aData['event_type_id'])) {
		     $oField = new EventTypeId();
		     $mValue = $oField->sanitize($aData['event_type_id']);
		     $oModel->setEventTypeId($mValue);
		}
		if(isset($aData['crud_config_id'])) {
		     $oField = new CrudConfigId();
		     $mValue = $oField->sanitize($aData['crud_config_id']);
		     $oModel->setCrudConfigId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
