<?php
namespace Crud\EventHandler\Base;

use Crud\BaseCrudFieldIterator;
use Crud\EventHandler\ICollectionField as EventHandlerField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\EventHandler\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param EventHandlerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param EventHandlerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof EventHandlerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(EventHandlerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): EventHandlerField
	{
		return current($this->aFields);
	}
}
