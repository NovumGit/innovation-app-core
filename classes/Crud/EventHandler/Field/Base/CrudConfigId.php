<?php
namespace Crud\EventHandler\Field\Base;

use Crud\EventHandler\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'crud_config_id' crud field from the 'event_handler' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudConfigId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'crud_config_id';
	protected $sFieldLabel = 'Input';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudConfigId';
	protected $sFqModelClassname = '\\\Model\System\Event\EventHandler';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['crud_config_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Input" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
