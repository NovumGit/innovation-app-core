<?php
namespace Crud\EventHandler\Field;

use Crud\EventHandler\Field\Base\CrudConfigId as BaseCrudConfigId;
use Crud\Generic\Lookups\ManagerLookupsTrait;

/**
 * Skeleton subclass for representing crud_config_id field from the event_handler table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudConfigId extends BaseCrudConfigId
{
    use ManagerLookupsTrait;
}
