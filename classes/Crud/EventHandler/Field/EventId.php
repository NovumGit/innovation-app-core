<?php
namespace Crud\EventHandler\Field;

use Crud\EventHandler\Field\Base\EventId as BaseEventId;

/**
 * Skeleton subclass for representing event_id field from the event_handler table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EventId extends BaseEventId
{
}
