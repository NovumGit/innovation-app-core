<?php

namespace Crud\Payterm\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Payterm as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oObject)
    {

        if (!$oObject instanceof ModelObject) {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got ' . get_class($oObject));
        }

        return '/setting/mastertable/payterm/edit?_do=Delete&id=' . $oObject->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
