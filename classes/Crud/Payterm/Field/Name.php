<?php

namespace Crud\Payterm\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PayTerm as ModelObject;

class Name extends Field
{

    protected $sFieldName = 'name';
    protected $sFieldLabel = 'Betaaltermijn omschrijving';
    private $sIcon = 'calendar-o ';
    private $sPlaceHolder = '';

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: " . get_class($oModelObject) . __METHOD__);
        }
        return '<td class="">' . $oModelObject->getName() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
