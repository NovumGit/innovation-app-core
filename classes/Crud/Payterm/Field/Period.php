<?php

namespace Crud\Payterm\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PayTerm as ModelObject;

class Period extends Field
{

    protected $sFieldName = 'period';
    protected $sFieldLabel = 'Periode';
    private $sIcon = 'edit';
    private $sGetter = 'getPeriod';

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: " . get_class($oModelObject) . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        $aIntervals = [
            [
                'id'       => 0,
                'selected' => $mData->getPeriod() == 'DAY' ? 'selected="selected"' : '',
                'label'    => 'Dag',
            ],
            [
                'id'       => 1,
                'selected' => $mData->getPeriod() == 'WEEK' ? 'selected="selected"' : '',
                'label'    => 'Week',
            ],
            [
                'id'       => 2,
                'selected' => $mData->getPeriod() == 'MONTH' ? 'selected="selected"' : '',
                'label'    => 'Maand',
            ],
            [
                'id'       => 3,
                'selected' => $mData->getPeriod() == 'YEAR' ? 'selected="selected"' : '',
                'label'    => 'Jaar',
            ],
        ];

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $aIntervals, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
