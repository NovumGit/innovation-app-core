<?php

namespace Crud\Payterm\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PayTerm as ModelObject;

class TotalDays extends Field
{

    protected $sFieldName = 'total_days';
    protected $sFieldLabel = 'Totale dagen';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getTotalDays';

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: " . get_class($oModelObject) . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
