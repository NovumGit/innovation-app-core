<?php

namespace Crud\Payterm\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\PayTerm as ModelObject;

class Code extends Field
{

    protected $sFieldName = 'code';
    protected $sFieldLabel = 'Code';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getCode';

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in got: " . get_class($oModelObject) . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
