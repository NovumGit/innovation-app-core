<?php

namespace Crud\Payterm\Field;

use Model\Setting\MasterTable\Payterm as ModelObject;
use Crud\Generic\Field\GenericEdit;
use InvalidArgumentException;

class Edit extends GenericEdit
{

    public function getEditUrl($oObject)
    {

        if (!$oObject instanceof ModelObject) {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got ' . get_class($oObject));
        }

        return '/setting/mastertable/payterm/edit?id=' . $oObject->getId();
    }
}
