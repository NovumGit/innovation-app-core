<?php

namespace Crud\Payterm;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\PayTermQuery;
use Model\Setting\MasterTable\PayTerm;
use Propel\Runtime\Exception\LogicException;

class CrudPaytermManager extends FormManager implements IEditableCrud
{

    public function getCreateNewUrl(): string
    {
        return '/setting/mastertable/payterm/edit';
    }

    public function getEntityTitle(): string
    {
        return 'betaaltermijn';
    }

    public function getNewFormTitle(): string
    {
        return 'Betaaltermijn toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Betaaltermijn wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false): ?array
    {
        return [
            'Name',
            'Code',
            'TotalDays',
            'Period',
            'Delete',
            'Edit',
        ];
    }

    public function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false): ?array
    {

        return [
            'Name',
            'TotalDays',
            'Period',
            'Code',
        ];
    }

    public function getModel(array $aData = null)
    {
        if ($aData['id']) {
            $oPayTermQuery = new PayTermQuery();

            $oPayTerm = $oPayTermQuery->findOneById($aData['id']);

            if (!$oPayTerm instanceof PayTerm) {
                throw new LogicException("PayTerm should be an instance of PayTerm but got " . get_class($oPayTerm) . " in " . __METHOD__);
            }
        } else {
            $oPayTerm = new PayTerm();

            if (isset($aData['name'])) {
                $oPayTerm->setName($aData['name']);
            }
        }

        return $oPayTerm;
    }

    public function store(array $aData = null)
    {
        $oPayTerm = $this->getModel($aData);

        if (!empty($oPayTerm)) {
            $oPayTerm = $this->fillVo($aData, $oPayTerm);
            $oPayTerm->save();
        }
        return $oPayTerm;
    }

    private function fillVo($aData, PayTerm $oPayterm)
    {
        static $aIntervalValues = [
            'DAY',
            'WEEK',
            'MONTH',
            'YEAR',
        ];

        if (isset($aData['name'])) {
            $oPayterm->setName($aData['name']);
        }
        if (isset($aData['code'])) {
            $oPayterm->setCode($aData['code']);
        }
        if (isset($aData['total_days'])) {
            $oPayterm->setTotalDays($aData['total_days']);
        }
        if (isset($aData['period'])) {
            $oPayterm->setPeriod($aIntervalValues[$aData['period']]);
        }

        return $oPayterm;
    }
}
