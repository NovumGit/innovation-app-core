<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28-11-19
 * Time: 15:52
 */

namespace Crud;

interface ICrudElement
{
    function setArguments($aArguments);

    function getArgument($sKey);

    /**
     * @return mixed
     */
    function isEditorField(): bool;

    function getUntranslatedFieldTitle();

    function getTranslatedTitle();

    function getModuleName():string;
    function isWeb():bool;
    function isApp():bool;
}
