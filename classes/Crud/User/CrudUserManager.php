<?php

namespace Crud\User;

use Crud\IApiAlwaysExpose;
use Model\Account\User;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

/**
 * Skeleton subclass for representing a User.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudUserManager extends Base\CrudUserManager implements IApiAlwaysExpose
{

    public function save(array $aData = null): ActiveRecordInterface
    {
        if (isset($aData['password'])) {
            $sSalt = \Core\User::makeSalt();
            $sPassword = \Core\User::makeEncryptedPass($aData['password'], $sSalt);

            $aData['password'] = $sPassword;
            $aData['salt'] = $sSalt;
        }

        return parent::save($aData);
    }

    protected function fillVo(array $aData, User $oModel): User
    {

        if (!isset($aData['prefered_language_id']) || empty($aData['prefered_language_id'])) {
            $oLanguage = LanguageQuery::create()->findOneByIsDefaultCms(true);
            if (!$oLanguage instanceof Language) {
                $oLanguage = LanguageQuery::create()->findOneByLocaleCode('nl_NL');
            }

            $aData['prefered_language_id'] = $oLanguage->getId();
        }
        return parent::fillVo($aData, $oModel);
    }
}
