<?php
namespace Crud\User\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\User\ICollectionField as UserField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\User\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UserField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UserField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UserField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UserField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UserField
	{
		return current($this->aFields);
	}
}
