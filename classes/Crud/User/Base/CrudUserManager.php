<?php
namespace Crud\User\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\User\FieldIterator;
use Crud\User\Field\AboutMe;
use Crud\User\Field\CanChangeRoles;
use Crud\User\Field\Created;
use Crud\User\Field\Email;
use Crud\User\Field\FirstName;
use Crud\User\Field\HasProfilePic;
use Crud\User\Field\ItemDeleted;
use Crud\User\Field\LastName;
use Crud\User\Field\Modified;
use Crud\User\Field\MustChangePassword;
use Crud\User\Field\Password;
use Crud\User\Field\Phone;
use Crud\User\Field\PhoneMobile;
use Crud\User\Field\PicExt;
use Crud\User\Field\PreferedLanguageId;
use Crud\User\Field\PrivateEmail;
use Crud\User\Field\RoleId;
use Crud\User\Field\Salt;
use Crud\User\Field\SourceNetwork;
use Crud\User\Field\Twitter;
use Crud\User\Field\Website;
use Exception\LogicException;
use Model\Account\Map\UserTableMap;
use Model\Account\User;
use Model\Account\UserQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify User instead if you need to override or add functionality.
 */
abstract class CrudUserManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UserQuery::create();
	}


	public function getTableMap(): UserTableMap
	{
		return new UserTableMap();
	}


	public function getShortDescription(): string
	{
		return "This endpoint has all the system users";
	}


	public function getEntityTitle(): string
	{
		return "User";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "user toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "user aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemDeleted', 'CanChangeRoles', 'RoleId', 'PreferedLanguageId', 'FirstName', 'LastName', 'Email', 'Phone', 'PhoneMobile', 'Website', 'Twitter', 'AboutMe', 'PrivateEmail', 'Password', 'Salt', 'SourceNetwork', 'Created', 'Modified', 'MustChangePassword', 'HasProfilePic', 'PicExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemDeleted', 'CanChangeRoles', 'RoleId', 'PreferedLanguageId', 'FirstName', 'LastName', 'Email', 'Phone', 'PhoneMobile', 'Website', 'Twitter', 'AboutMe', 'PrivateEmail', 'Password', 'Salt', 'SourceNetwork', 'Created', 'Modified', 'MustChangePassword', 'HasProfilePic', 'PicExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return User
	 */
	public function getModel(array $aData = null): User
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUserQuery = UserQuery::create();
		     $oUser = $oUserQuery->findOneById($aData['id']);
		     if (!$oUser instanceof User) {
		         throw new LogicException("User should be an instance of User but got something else." . __METHOD__);
		     }
		     $oUser = $this->fillVo($aData, $oUser);
		}
		else {
		     $oUser = new User();
		     if (!empty($aData)) {
		         $oUser = $this->fillVo($aData, $oUser);
		     }
		}
		return $oUser;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return User
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): User
	{
		$oUser = $this->getModel($aData);


		 if(!empty($oUser))
		 {
		     $oUser = $this->fillVo($aData, $oUser);
		     $oUser->save();
		 }
		return $oUser;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param User $oModel
	 * @return User
	 */
	protected function fillVo(array $aData, User $oModel): User
	{
		if(isset($aData['is_deleted'])) {
		     $oField = new ItemDeleted();
		     $mValue = $oField->sanitize($aData['is_deleted']);
		     $oModel->setItemDeleted($mValue);
		}
		if(isset($aData['can_change_roles'])) {
		     $oField = new CanChangeRoles();
		     $mValue = $oField->sanitize($aData['can_change_roles']);
		     $oModel->setCanChangeRoles($mValue);
		}
		if(isset($aData['role_id'])) {
		     $oField = new RoleId();
		     $mValue = $oField->sanitize($aData['role_id']);
		     $oModel->setRoleId($mValue);
		}
		if(isset($aData['prefered_language_id'])) {
		     $oField = new PreferedLanguageId();
		     $mValue = $oField->sanitize($aData['prefered_language_id']);
		     $oModel->setPreferedLanguageId($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['last_name'])) {
		     $oField = new LastName();
		     $mValue = $oField->sanitize($aData['last_name']);
		     $oModel->setLastName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['phone_mobile'])) {
		     $oField = new PhoneMobile();
		     $mValue = $oField->sanitize($aData['phone_mobile']);
		     $oModel->setPhoneMobile($mValue);
		}
		if(isset($aData['website'])) {
		     $oField = new Website();
		     $mValue = $oField->sanitize($aData['website']);
		     $oModel->setWebsite($mValue);
		}
		if(isset($aData['twitter'])) {
		     $oField = new Twitter();
		     $mValue = $oField->sanitize($aData['twitter']);
		     $oModel->setTwitter($mValue);
		}
		if(isset($aData['about_me'])) {
		     $oField = new AboutMe();
		     $mValue = $oField->sanitize($aData['about_me']);
		     $oModel->setAboutMe($mValue);
		}
		if(isset($aData['private_email'])) {
		     $oField = new PrivateEmail();
		     $mValue = $oField->sanitize($aData['private_email']);
		     $oModel->setPrivateEmail($mValue);
		}
		if(isset($aData['password'])) {
		     $oField = new Password();
		     $mValue = $oField->sanitize($aData['password']);
		     $oModel->setPassword($mValue);
		}
		if(isset($aData['salt'])) {
		     $oField = new Salt();
		     $mValue = $oField->sanitize($aData['salt']);
		     $oModel->setSalt($mValue);
		}
		if(isset($aData['source_network'])) {
		     $oField = new SourceNetwork();
		     $mValue = $oField->sanitize($aData['source_network']);
		     $oModel->setSourceNetwork($mValue);
		}
		if(isset($aData['created'])) {
		     $oField = new Created();
		     $mValue = $oField->sanitize($aData['created']);
		     $oModel->setCreated($mValue);
		}
		if(isset($aData['modified'])) {
		     $oField = new Modified();
		     $mValue = $oField->sanitize($aData['modified']);
		     $oModel->setModified($mValue);
		}
		if(isset($aData['must_change_password'])) {
		     $oField = new MustChangePassword();
		     $mValue = $oField->sanitize($aData['must_change_password']);
		     $oModel->setMustChangePassword($mValue);
		}
		if(isset($aData['has_profile_pic'])) {
		     $oField = new HasProfilePic();
		     $mValue = $oField->sanitize($aData['has_profile_pic']);
		     $oModel->setHasProfilePic($mValue);
		}
		if(isset($aData['pic_ext'])) {
		     $oField = new PicExt();
		     $mValue = $oField->sanitize($aData['pic_ext']);
		     $oModel->setPicExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
