<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\Password as BasePassword;

/**
 * Skeleton subclass for representing password field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Password extends BasePassword
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        if (empty($aPostedData['id'])) {
            $sPassword = $aPostedData['password'];

            if (empty($sPassword)) {
                $mResponse[] = 'U moet verplicht een wachtwoord opgeven.';
            }
            // password_2 is niet beschikbaar wanneer de commandline interface wordt gebruikt
            if (isset($aPostedData['password_2']) && $aPostedData['password'] != $aPostedData['password_2']) {
                $mResponse[] = 'De opgegeven wachtwoorden zijn ongelijk.';
            } else {
                if (strlen($aPostedData['password']) < 8) {
                    $mResponse[] = 'Het opgegeven wachtwoord moet minimaal 8 tekens lang zijn, het opgegeven wachtwoord was ' . strlen($aPostedData['password']) . ' tekens lang.';
                }
            }
        }
        return $mResponse;
    }

    public function getEditHtml($mData, $bReadonly)
    {
        $aOut = [];
        $aOut[] = $this->editPasswordField('Wachtwoord', 'password', '', 'Wachtwoord', 'key', false);
        $aOut[] = $this->editPasswordField('Wachtwoord (herhaal)', 'password_2', '', 'Wachtwoord nogmaals', 'key', false);

        return join(PHP_EOL, $aOut);
    }
}
