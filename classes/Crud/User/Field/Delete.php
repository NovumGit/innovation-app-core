<?php

namespace Crud\User\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Account\User;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oUser)
    {

        if (!$oUser instanceof User) {
            throw new InvalidArgumentException('Expected an instance of \\model\\\User\\\User but got ' . get_class($oUser));
        }
        $sAddUrl = '';

        if (isset($_GET['tab'])) {
            $sAddUrl = '&tab=' . $_GET['tab'];
        }
        return '/user/edit?_do=Delete&id=' . $oUser->getId() . $sAddUrl;
    }

    public function getUnDeleteUrl($oUser)
    {

        if (!$oUser instanceof User) {
            throw new InvalidArgumentException('Expected an instance of \\model\\\User\\\User but got ' . get_class($oUser));
        }
        $sAddUrl = '';

        if (isset($_GET['tab'])) {
            $sAddUrl = '&tab=' . $_GET['tab'];
        }

        return '/user/edit?_do=Undelete&id=' . $oUser->getId() . $sAddUrl;
    }
}
