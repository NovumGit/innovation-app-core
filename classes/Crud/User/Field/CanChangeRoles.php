<?php

namespace Crud\User\Field;

use Core\Translate;
use Model\Account\User;
use Crud\User\Field\Base\CanChangeRoles as BaseCanChangeRoles;
use Exception\InvalidArgumentException;
use Propel\Runtime\Exception\PropelException;
use Ui\RenderEditConfig;

/**
 * Skeleton subclass for representing can_change_roles field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CanChangeRoles extends BaseCanChangeRoles
{
    /**
     * @param $oUser
     * @return string
     * @throws PropelException
     */
    public function getOverviewValue($oUser)
    {
        if (!$oUser instanceof User) {
            throw new InvalidArgumentException("Expected an instance of User in " . __METHOD__);
        }
        if ($oUser->getRole()->getName() == 'Admin') {
            $sAnswer = Translate::fromCode('Admin altijd');
        } else {
            if ($oUser->getCanChangeRoles()) {
                $sAnswer = Translate::fromCode('Ja');
            } else {
                $sAnswer = Translate::fromCode('Nee');
            }
        }

        return '<td class="">' . $sAnswer . '</td>';
    }

    public function getEditHtml($oUser, $bReadonly)
    {
        if (\Core\User::getRole()->getName() != 'Admin') {
            $bReadonly = true;
        }
        if (!$oUser instanceof User) {
            throw new InvalidArgumentException("Expected an instance of User in " . __METHOD__);
        }
        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(3, 8);

        $this->setRenderEditConfig($oRenderEditConfig);

        return $this->editBooleanField($this->getFieldLabel(), $this->sFieldName, $oUser->getCanChangeRoles(), $bReadonly, 'user');
    }
}
