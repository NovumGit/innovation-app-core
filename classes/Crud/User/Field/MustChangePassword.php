<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\MustChangePassword as BaseMustChangePassword;

/**
 * Skeleton subclass for representing must_change_password field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class MustChangePassword extends BaseMustChangePassword
{
}
