<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\Created as BaseCreated;

/**
 * Skeleton subclass for representing created field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Created extends BaseCreated
{
}
