<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'can_change_roles' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class CanChangeRoles extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'can_change_roles';
	protected $sFieldLabel = 'Kan van rol veranderen';
	protected $sIcon = 'group';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCanChangeRoles';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['can_change_roles']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Kan van rol veranderen" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
