<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'about_me' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class AboutMe extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'about_me';
	protected $sFieldLabel = 'Over mij';
	protected $sIcon = 'book';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAboutMe';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
