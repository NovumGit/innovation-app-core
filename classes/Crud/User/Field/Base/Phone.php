<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'phone' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class Phone extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'phone';
	protected $sFieldLabel = 'Telefoon';
	protected $sIcon = 'phone';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhone';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
