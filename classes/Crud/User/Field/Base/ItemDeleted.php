<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'is_deleted' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemDeleted extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_deleted';
	protected $sFieldLabel = 'Is verwijderd';
	protected $sIcon = 'trash-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemDeleted';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
