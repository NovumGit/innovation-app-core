<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericPassword;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'password' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class Password extends GenericPassword implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'password';
	protected $sFieldLabel = 'Wachtwoord';
	protected $sIcon = 'lock';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPassword';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
