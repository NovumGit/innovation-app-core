<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'must_change_password' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class MustChangePassword extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'must_change_password';
	protected $sFieldLabel = 'Moet wachtwoord veranderen';
	protected $sIcon = 'lock';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMustChangePassword';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
