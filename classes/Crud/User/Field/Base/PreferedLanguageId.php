<?php
namespace Crud\User\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\User\ICollectionField;
use Model\Setting\MasterTable\LanguageQuery;

/**
 * Base class that represents the 'prefered_language_id' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class PreferedLanguageId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'prefered_language_id';
	protected $sFieldLabel = 'Taal';
	protected $sIcon = 'flag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPreferedLanguageId';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\MasterTable\LanguageQuery::create()->orderBydescription()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getdescription", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\MasterTable\LanguageQuery::create()->findOneById($iItemId)->getdescription();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
