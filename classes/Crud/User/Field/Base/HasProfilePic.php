<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'has_profile_pic' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class HasProfilePic extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'has_profile_pic';
	protected $sFieldLabel = 'Heeft profiel foto';
	protected $sIcon = 'picture-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHasProfilePic';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
