<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'source_network' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class SourceNetwork extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'source_network';
	protected $sFieldLabel = 'Bron netwerk';
	protected $sIcon = 'globe';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSourceNetwork';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
