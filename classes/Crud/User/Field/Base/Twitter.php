<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'twitter' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class Twitter extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'twitter';
	protected $sFieldLabel = 'Twitter';
	protected $sIcon = 'twitter';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTwitter';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
