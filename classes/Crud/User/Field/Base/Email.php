<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericEmail;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'email' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class Email extends GenericEmail implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'email';
	protected $sFieldLabel = 'Mailadres';
	protected $sIcon = 'envelope';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEmail';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['email']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Mailadres" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
