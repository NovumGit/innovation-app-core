<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericDateTime;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'modified' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class Modified extends GenericDateTime implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'modified';
	protected $sFieldLabel = 'Gewijzigd op';
	protected $sIcon = 'calendar';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getModified';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
