<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'phone_mobile' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class PhoneMobile extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'phone_mobile';
	protected $sFieldLabel = 'Mobiel';
	protected $sIcon = 'mobile-phone';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhoneMobile';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
