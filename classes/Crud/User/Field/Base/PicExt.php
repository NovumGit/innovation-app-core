<?php
namespace Crud\User\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\User\ICollectionField;

/**
 * Base class that represents the 'pic_ext' crud field from the 'user' table.
 * This class is auto generated and should not be modified.
 */
abstract class PicExt extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'pic_ext';
	protected $sFieldLabel = 'Bestands extensie';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPicExt';
	protected $sFqModelClassname = '\\\Model\Account\User';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
