<?php

namespace Crud\User\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Account\User;
use InvalidArgumentException;

class Edit extends GenericEdit
{

    public function getEditUrl($oUser)
    {

        if (!$oUser instanceof User) {
            throw new InvalidArgumentException('Expected an instance of \\model\\\User\\\User but got ' . get_class($oUser));
        }

        return '/user/edit?id=' . $oUser->getId();
    }
}
