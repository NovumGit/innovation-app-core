<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\AboutMe as BaseAboutMe;

/**
 * Skeleton subclass for representing about_me field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class AboutMe extends BaseAboutMe
{
}
