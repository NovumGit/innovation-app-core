<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\RoleId as BaseRoleId;
use Respect\Validation\Validator;

/**
 * Skeleton subclass for representing role_id field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class RoleId extends BaseRoleId
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {

        $mResponse = false;
        $oValidator = Validator::numeric();

        if (empty($aPostedData['role_id'])) {
            $mResponse[] = 'U moet de gebruiker verplicht een rol geven.';
        } else {
            if (!$oValidator->intVal()->validate($aPostedData['role_id'])) {
                $mResponse[] = 'De opgegeven rol is ongeldig.';
            }
        }

        return $mResponse;
    }
}
