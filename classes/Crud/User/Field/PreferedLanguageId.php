<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\PreferedLanguageId as BasePreferedLanguageId;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

/**
 * Skeleton subclass for representing prefered_language_id field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class PreferedLanguageId extends BasePreferedLanguageId
{
    public function defaultValue()
    {
        $oLanguage = LanguageQuery::create()->findOneByIsDefaultCms(true);

        if ($oLanguage instanceof Language) {
            return $oLanguage->getId();
        }
        return null;
    }
}
