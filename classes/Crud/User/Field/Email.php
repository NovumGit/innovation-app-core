<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\Email as BaseEmail;
use Model\Account\User;
use Model\Account\UserQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for representing email field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Email extends BaseEmail
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        $sEmail = trim($aPostedData['email']);

        $oEmail = new \Hurah\Types\Type\Email($sEmail);

        if (empty($sEmail)) {
            $mResponse[] = 'Het veld e-mail mag niet leeg zijn.';
        }

        if (!$oEmail->isValid()) {
            $mResponse[] = 'Ongeldig e-mail adres.';
        }
        if (!isset($aPostedData['id'])) {
            $oUser = UserQuery::create()->findOneByEmail($aPostedData['email']);
            if ($oUser instanceof User) {
                $mResponse[] = "E-mailadres moet uniek zijn, {$aPostedData['email']} komt al voor in de database";
            }
        } else {
            $oUser = UserQuery::create()->filterByEmail($aPostedData['email'])->filterById($aPostedData['id'], Criteria::NOT_EQUAL)->findOne();
            if ($oUser instanceof User) {
                $mResponse[] = "E-mailadres moet uniek zijn, {$aPostedData['email']} komt al voor in de database";
            }
        }

        return $mResponse;
    }
}
