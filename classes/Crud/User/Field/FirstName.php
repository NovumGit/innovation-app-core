<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\FirstName as BaseFirstName;
use Respect\Validation\Validator;

/**
 * Skeleton subclass for representing first_name field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FirstName extends BaseFirstName
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        $oValidator = Validator::alpha();

        $sFirstName = trim($aPostedData['first_name']);
        if (empty($sFirstName)) {
            $mResponse[] = 'Het veld voornaam mag niet leeg zijn.';
        } else {
            if (!$oValidator->length(2, 20)->validate($aPostedData['first_name'])) {
                $mResponse[] = 'Het veld voornaam moet minimaal 2 en mag maximaal 20 tekens lang zijn.';
            }
        }

        return $mResponse;
    }
}
