<?php

namespace Crud\User\Field;

use Crud\User\Field\Base\LastName as BaseLastName;
use Model\Account\User;
use Model\Account\UserQuery;

/**
 * Skeleton subclass for representing last_name field from the user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class LastName extends BaseLastName
{
    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;

        $sLastName = trim($aPostedData['last_name']);
        if (empty($sLastName)) {
            $mResponse[] = 'Het veld achternaam mag niet leeg zijn.';
        } else {
            if (strlen($aPostedData['last_name']) < 2) {
                $mResponse[] = 'Het veld achternaam moet minimaal 2 tekens lang zijn.';
            } else {
                if (strlen($aPostedData['last_name']) > 20) {
                    $mResponse[] = 'Het veld achternaam moet maximaal 20 tekens lang zijn.';
                }
            }
        }

        return $mResponse;
    }
}
