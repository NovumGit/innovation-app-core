<?php
namespace Crud\SiteUsp\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteUsp\ICollectionField as SiteUspField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteUsp\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteUspField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteUspField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteUspField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteUspField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteUspField
	{
		return current($this->aFields);
	}
}
