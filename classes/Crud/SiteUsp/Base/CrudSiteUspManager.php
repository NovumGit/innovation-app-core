<?php
namespace Crud\SiteUsp\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteUsp\FieldIterator;
use Crud\SiteUsp\Field\AboutTxt;
use Crud\SiteUsp\Field\Icon;
use Crud\SiteUsp\Field\LanguageId;
use Crud\SiteUsp\Field\LongTxt;
use Crud\SiteUsp\Field\ShortTxt;
use Crud\SiteUsp\Field\SiteHook;
use Crud\SiteUsp\Field\SiteId;
use Crud\SiteUsp\Field\Slogan;
use Exception\LogicException;
use Model\Cms\Map\SiteUspTableMap;
use Model\Cms\SiteUsp;
use Model\Cms\SiteUspQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteUsp instead if you need to override or add functionality.
 */
abstract class CrudSiteUspManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteUspQuery::create();
	}


	public function getTableMap(): SiteUspTableMap
	{
		return new SiteUspTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteUsp";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_usp toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_usp aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'SiteHook', 'AboutTxt', 'ShortTxt', 'LongTxt', 'Slogan', 'Icon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'LanguageId', 'SiteHook', 'AboutTxt', 'ShortTxt', 'LongTxt', 'Slogan', 'Icon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteUsp
	 */
	public function getModel(array $aData = null): SiteUsp
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteUspQuery = SiteUspQuery::create();
		     $oSiteUsp = $oSiteUspQuery->findOneById($aData['id']);
		     if (!$oSiteUsp instanceof SiteUsp) {
		         throw new LogicException("SiteUsp should be an instance of SiteUsp but got something else." . __METHOD__);
		     }
		     $oSiteUsp = $this->fillVo($aData, $oSiteUsp);
		}
		else {
		     $oSiteUsp = new SiteUsp();
		     if (!empty($aData)) {
		         $oSiteUsp = $this->fillVo($aData, $oSiteUsp);
		     }
		}
		return $oSiteUsp;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteUsp
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteUsp
	{
		$oSiteUsp = $this->getModel($aData);


		 if(!empty($oSiteUsp))
		 {
		     $oSiteUsp = $this->fillVo($aData, $oSiteUsp);
		     $oSiteUsp->save();
		 }
		return $oSiteUsp;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteUsp $oModel
	 * @return SiteUsp
	 */
	protected function fillVo(array $aData, SiteUsp $oModel): SiteUsp
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['site_hook'])) {
		     $oField = new SiteHook();
		     $mValue = $oField->sanitize($aData['site_hook']);
		     $oModel->setSiteHook($mValue);
		}
		if(isset($aData['about_txt'])) {
		     $oField = new AboutTxt();
		     $mValue = $oField->sanitize($aData['about_txt']);
		     $oModel->setAboutTxt($mValue);
		}
		if(isset($aData['short_txt'])) {
		     $oField = new ShortTxt();
		     $mValue = $oField->sanitize($aData['short_txt']);
		     $oModel->setShortTxt($mValue);
		}
		if(isset($aData['long_txt'])) {
		     $oField = new LongTxt();
		     $mValue = $oField->sanitize($aData['long_txt']);
		     $oModel->setLongTxt($mValue);
		}
		if(isset($aData['slogan'])) {
		     $oField = new Slogan();
		     $mValue = $oField->sanitize($aData['slogan']);
		     $oModel->setSlogan($mValue);
		}
		if(isset($aData['icon'])) {
		     $oField = new Icon();
		     $mValue = $oField->sanitize($aData['icon']);
		     $oModel->setIcon($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
