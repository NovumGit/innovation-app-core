<?php
namespace Crud\SiteUsp\Field;

use Crud\SiteUsp\Field\Base\Icon as BaseIcon;

/**
 * Skeleton subclass for representing icon field from the site_usp table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Icon extends BaseIcon
{
}
