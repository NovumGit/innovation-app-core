<?php
namespace Crud\SiteUsp\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteUsp\ICollectionField;

/**
 * Base class that represents the 'site_hook' crud field from the 'site_usp' table.
 * This class is auto generated and should not be modified.
 */
abstract class SiteHook extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'site_hook';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSiteHook';
	protected $sFqModelClassname = '\\\Model\Cms\SiteUsp';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
