<?php
namespace Crud;

interface IListableField extends IField
{
    function getListTitle():string;
    function getList(): array ;
    function storeListOrder(array $Ids);
    function addToList(int $iItemId);
    function removeFromList(int $iItemId);
    function getOverviewHeader();
    function getOverviewValue($mData);
}
