<?php
namespace Crud\Component_table_form_row\Base;

use Core\Utils;
use Crud;
use Crud\Component_table_form_row\FieldIterator;
use Crud\Component_table_form_row\Field\LayoutKey;
use Crud\Component_table_form_row\Field\RenderStyle;
use Crud\Component_table_form_row\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\TableFormRow\Component_table_form_row;
use Model\System\LowCode\TableFormRow\Component_table_form_rowQuery;
use Model\System\LowCode\TableFormRow\Map\Component_table_form_rowTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_table_form_row instead if you need to override or add functionality.
 */
abstract class CrudComponent_table_form_rowManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_table_form_rowQuery::create();
	}


	public function getTableMap(): Component_table_form_rowTableMap
	{
		return new Component_table_form_rowTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Table form row component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_table_form_row";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_table_form_row toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_table_form_row aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LayoutKey', 'RenderStyle', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LayoutKey', 'RenderStyle', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_table_form_row
	 */
	public function getModel(array $aData = null): Component_table_form_row
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_table_form_rowQuery = Component_table_form_rowQuery::create();
		     $oComponent_table_form_row = $oComponent_table_form_rowQuery->findOneById($aData['id']);
		     if (!$oComponent_table_form_row instanceof Component_table_form_row) {
		         throw new LogicException("Component_table_form_row should be an instance of Component_table_form_row but got something else." . __METHOD__);
		     }
		     $oComponent_table_form_row = $this->fillVo($aData, $oComponent_table_form_row);
		}
		else {
		     $oComponent_table_form_row = new Component_table_form_row();
		     if (!empty($aData)) {
		         $oComponent_table_form_row = $this->fillVo($aData, $oComponent_table_form_row);
		     }
		}
		return $oComponent_table_form_row;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_table_form_row
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_table_form_row
	{
		$oComponent_table_form_row = $this->getModel($aData);


		 if(!empty($oComponent_table_form_row))
		 {
		     $oComponent_table_form_row = $this->fillVo($aData, $oComponent_table_form_row);
		     $oComponent_table_form_row->save();
		 }
		return $oComponent_table_form_row;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_table_form_row $oModel
	 * @return Component_table_form_row
	 */
	protected function fillVo(array $aData, Component_table_form_row $oModel): Component_table_form_row
	{
		if(isset($aData['layout_key'])) {
		     $oField = new LayoutKey();
		     $mValue = $oField->sanitize($aData['layout_key']);
		     $oModel->setLayoutKey($mValue);
		}
		if(isset($aData['render_style'])) {
		     $oField = new RenderStyle();
		     $mValue = $oField->sanitize($aData['render_style']);
		     $oModel->setRenderStyle($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
