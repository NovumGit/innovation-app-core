<?php
namespace Crud\Component_table_form_row\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_table_form_row\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
