<?php
namespace Crud\Component_table_form_row\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_table_form_row\ICollectionField as Component_table_form_rowField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_table_form_row\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_table_form_rowField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_table_form_rowField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_table_form_rowField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_table_form_rowField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_table_form_rowField
	{
		return current($this->aFields);
	}
}
