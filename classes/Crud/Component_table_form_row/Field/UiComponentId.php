<?php
namespace Crud\Component_table_form_row\Field;

use Crud\Component_table_form_row\Field\Base\UiComponentId as BaseUiComponentId;

/**
 * Skeleton subclass for representing ui_component_id field from the component_table_form_row table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class UiComponentId extends BaseUiComponentId
{
}
