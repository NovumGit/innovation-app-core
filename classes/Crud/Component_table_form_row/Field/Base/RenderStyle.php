<?php
namespace Crud\Component_table_form_row\Field\Base;

use Crud\Component_table_form_row\ICollectionField;
use Crud\Generic\Field\GenericBoolean;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'render_style' crud field from the 'component_table_form_row' table.
 * This class is auto generated and should not be modified.
 */
abstract class RenderStyle extends GenericBoolean implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'render_style';
	protected $sFieldLabel = 'Auto save';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRenderStyle';
	protected $sFqModelClassname = '\Model\System\LowCode\TableFormRow\Component_table_form_row';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
