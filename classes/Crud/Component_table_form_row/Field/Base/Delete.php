<?php 
namespace Crud\Component_table_form_row\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\TableFormRow\Component_table_form_row;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_table_form_row)
		{
		     return "//system/component_table_form_row/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_table_form_row)
		{
		     return "//component_table_form_row?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
