<?php
namespace Crud\Component_table_form_row;

/**
 * Skeleton subclass for representing a Component_table_form_row.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudComponent_table_form_rowManager extends Base\CrudComponent_table_form_rowManager
{
}
