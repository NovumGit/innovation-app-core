<?php
namespace Crud\CustomerPhoto\Field;

use Crud\CustomerPhoto\Field\Base\CreatedOn as BaseCreatedOn;

/**
 * Skeleton subclass for representing created_on field from the customer_photo table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class CreatedOn extends BaseCreatedOn
{
}
