<?php
namespace Crud\CustomerPhoto\Base;

use Core\Utils;
use Crud;
use Crud\CustomerPhoto\FieldIterator;
use Crud\CustomerPhoto\Field\CreatedOn;
use Crud\CustomerPhoto\Field\CustomerId;
use Crud\CustomerPhoto\Field\Description;
use Crud\CustomerPhoto\Field\FileExt;
use Crud\CustomerPhoto\Field\Mime;
use Crud\CustomerPhoto\Field\OriginalName;
use Crud\CustomerPhoto\Field\Sort;
use Crud\CustomerPhoto\Field\Tags;
use Crud\CustomerPhoto\Field\Title;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerPhoto;
use Model\Crm\CustomerPhotoQuery;
use Model\Crm\Map\CustomerPhotoTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerPhoto instead if you need to override or add functionality.
 */
abstract class CrudCustomerPhotoManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerPhotoQuery::create();
	}


	public function getTableMap(): CustomerPhotoTableMap
	{
		return new CustomerPhotoTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerPhoto";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_photo toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_photo aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'CustomerId', 'Title', 'Description', 'Tags', 'OriginalName', 'FileExt', 'Mime', 'Sort'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'CustomerId', 'Title', 'Description', 'Tags', 'OriginalName', 'FileExt', 'Mime', 'Sort'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerPhoto
	 */
	public function getModel(array $aData = null): CustomerPhoto
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerPhotoQuery = CustomerPhotoQuery::create();
		     $oCustomerPhoto = $oCustomerPhotoQuery->findOneById($aData['id']);
		     if (!$oCustomerPhoto instanceof CustomerPhoto) {
		         throw new LogicException("CustomerPhoto should be an instance of CustomerPhoto but got something else." . __METHOD__);
		     }
		     $oCustomerPhoto = $this->fillVo($aData, $oCustomerPhoto);
		}
		else {
		     $oCustomerPhoto = new CustomerPhoto();
		     if (!empty($aData)) {
		         $oCustomerPhoto = $this->fillVo($aData, $oCustomerPhoto);
		     }
		}
		return $oCustomerPhoto;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerPhoto
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerPhoto
	{
		$oCustomerPhoto = $this->getModel($aData);


		 if(!empty($oCustomerPhoto))
		 {
		     $oCustomerPhoto = $this->fillVo($aData, $oCustomerPhoto);
		     $oCustomerPhoto->save();
		 }
		return $oCustomerPhoto;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerPhoto $oModel
	 * @return CustomerPhoto
	 */
	protected function fillVo(array $aData, CustomerPhoto $oModel): CustomerPhoto
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['tags'])) {
		     $oField = new Tags();
		     $mValue = $oField->sanitize($aData['tags']);
		     $oModel->setTags($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		if(isset($aData['mime'])) {
		     $oField = new Mime();
		     $mValue = $oField->sanitize($aData['mime']);
		     $oModel->setMime($mValue);
		}
		if(isset($aData['sort'])) {
		     $oField = new Sort();
		     $mValue = $oField->sanitize($aData['sort']);
		     $oModel->setSort($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
