<?php
namespace Crud\FilterOperatorDatatype\Field\Base;

use Core\Utils;
use Crud\FilterOperatorDatatype\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\Setting\CrudManager\FilterDatatypeQuery;

/**
 * Base class that represents the 'filter_datatype_id' crud field from the 'filter_operator_datatype' table.
 * This class is auto generated and should not be modified.
 */
abstract class FilterDatatypeId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'filter_datatype_id';
	protected $sFieldLabel = 'Datatype';
	protected $sIcon = 'table';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFilterDatatypeId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\FilterOperatorDatatype';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\FilterDatatypeQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\FilterDatatypeQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['filter_datatype_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Datatype" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
