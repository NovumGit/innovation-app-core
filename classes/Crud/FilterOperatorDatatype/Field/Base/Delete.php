<?php 
namespace Crud\FilterOperatorDatatype\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\FilterOperatorDatatype;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterOperatorDatatype)
		{
		     return "//system/filter_operator_datatype/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterOperatorDatatype)
		{
		     return "//filter_operator_datatype?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
