<?php
namespace Crud\FilterOperatorDatatype\Base;

use Core\Utils;
use Crud;
use Crud\FilterOperatorDatatype\FieldIterator;
use Crud\FilterOperatorDatatype\Field\FilterDatatypeId;
use Crud\FilterOperatorDatatype\Field\FilterOperatorId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\FilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\Map\FilterOperatorDatatypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify FilterOperatorDatatype instead if you need to override or add functionality.
 */
abstract class CrudFilterOperatorDatatypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FilterOperatorDatatypeQuery::create();
	}


	public function getTableMap(): FilterOperatorDatatypeTableMap
	{
		return new FilterOperatorDatatypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint is opgeslagen op welke datatype een filter operator van toepassing is. .";
	}


	public function getEntityTitle(): string
	{
		return "FilterOperatorDatatype";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "filter_operator_datatype toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "filter_operator_datatype aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['FilterDatatypeId', 'FilterOperatorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['FilterDatatypeId', 'FilterOperatorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return FilterOperatorDatatype
	 */
	public function getModel(array $aData = null): FilterOperatorDatatype
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFilterOperatorDatatypeQuery = FilterOperatorDatatypeQuery::create();
		     $oFilterOperatorDatatype = $oFilterOperatorDatatypeQuery->findOneById($aData['id']);
		     if (!$oFilterOperatorDatatype instanceof FilterOperatorDatatype) {
		         throw new LogicException("FilterOperatorDatatype should be an instance of FilterOperatorDatatype but got something else." . __METHOD__);
		     }
		     $oFilterOperatorDatatype = $this->fillVo($aData, $oFilterOperatorDatatype);
		}
		else {
		     $oFilterOperatorDatatype = new FilterOperatorDatatype();
		     if (!empty($aData)) {
		         $oFilterOperatorDatatype = $this->fillVo($aData, $oFilterOperatorDatatype);
		     }
		}
		return $oFilterOperatorDatatype;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return FilterOperatorDatatype
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): FilterOperatorDatatype
	{
		$oFilterOperatorDatatype = $this->getModel($aData);


		 if(!empty($oFilterOperatorDatatype))
		 {
		     $oFilterOperatorDatatype = $this->fillVo($aData, $oFilterOperatorDatatype);
		     $oFilterOperatorDatatype->save();
		 }
		return $oFilterOperatorDatatype;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param FilterOperatorDatatype $oModel
	 * @return FilterOperatorDatatype
	 */
	protected function fillVo(array $aData, FilterOperatorDatatype $oModel): FilterOperatorDatatype
	{
		if(isset($aData['filter_datatype_id'])) {
		     $oField = new FilterDatatypeId();
		     $mValue = $oField->sanitize($aData['filter_datatype_id']);
		     $oModel->setFilterDatatypeId($mValue);
		}
		if(isset($aData['filter_operator_id'])) {
		     $oField = new FilterOperatorId();
		     $mValue = $oField->sanitize($aData['filter_operator_id']);
		     $oModel->setFilterOperatorId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
