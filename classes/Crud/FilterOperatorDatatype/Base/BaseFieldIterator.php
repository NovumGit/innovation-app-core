<?php
namespace Crud\FilterOperatorDatatype\Base;

use Crud\BaseCrudFieldIterator;
use Crud\FilterOperatorDatatype\ICollectionField as FilterOperatorDatatypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\FilterOperatorDatatype\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FilterOperatorDatatypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FilterOperatorDatatypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FilterOperatorDatatypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FilterOperatorDatatypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FilterOperatorDatatypeField
	{
		return current($this->aFields);
	}
}
