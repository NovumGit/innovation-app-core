<?php
namespace Crud\ColorTranslation\Field\Base;

use Crud\ColorTranslation\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'color_id' crud field from the 'mt_color_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class ColorId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'color_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getColorId';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\ColorTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
