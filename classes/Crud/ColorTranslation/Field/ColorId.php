<?php
namespace Crud\ColorTranslation\Field;

use Crud\ColorTranslation\Field\Base\ColorId as BaseColorId;

/**
 * Skeleton subclass for representing color_id field from the mt_color_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ColorId extends BaseColorId
{
}
