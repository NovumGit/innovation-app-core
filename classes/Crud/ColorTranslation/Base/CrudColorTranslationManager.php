<?php
namespace Crud\ColorTranslation\Base;

use Core\Utils;
use Crud;
use Crud\ColorTranslation\FieldIterator;
use Crud\ColorTranslation\Field\ColorId;
use Crud\ColorTranslation\Field\LanguageId;
use Crud\ColorTranslation\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\ColorTranslation;
use Model\Setting\MasterTable\ColorTranslationQuery;
use Model\Setting\MasterTable\Map\ColorTranslationTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ColorTranslation instead if you need to override or add functionality.
 */
abstract class CrudColorTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ColorTranslationQuery::create();
	}


	public function getTableMap(): ColorTranslationTableMap
	{
		return new ColorTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ColorTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_color_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_color_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ColorId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ColorId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ColorTranslation
	 */
	public function getModel(array $aData = null): ColorTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oColorTranslationQuery = ColorTranslationQuery::create();
		     $oColorTranslation = $oColorTranslationQuery->findOneById($aData['id']);
		     if (!$oColorTranslation instanceof ColorTranslation) {
		         throw new LogicException("ColorTranslation should be an instance of ColorTranslation but got something else." . __METHOD__);
		     }
		     $oColorTranslation = $this->fillVo($aData, $oColorTranslation);
		}
		else {
		     $oColorTranslation = new ColorTranslation();
		     if (!empty($aData)) {
		         $oColorTranslation = $this->fillVo($aData, $oColorTranslation);
		     }
		}
		return $oColorTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ColorTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ColorTranslation
	{
		$oColorTranslation = $this->getModel($aData);


		 if(!empty($oColorTranslation))
		 {
		     $oColorTranslation = $this->fillVo($aData, $oColorTranslation);
		     $oColorTranslation->save();
		 }
		return $oColorTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ColorTranslation $oModel
	 * @return ColorTranslation
	 */
	protected function fillVo(array $aData, ColorTranslation $oModel): ColorTranslation
	{
		if(isset($aData['color_id'])) {
		     $oField = new ColorId();
		     $mValue = $oField->sanitize($aData['color_id']);
		     $oModel->setColorId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
