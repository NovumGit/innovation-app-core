<?php
namespace Crud\ColorTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ColorTranslation\ICollectionField as ColorTranslationField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ColorTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ColorTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ColorTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ColorTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ColorTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ColorTranslationField
	{
		return current($this->aFields);
	}
}
