<?php
namespace Crud\CookieRegister\Field;

use Crud\CookieRegister\Field\Base\IpAddress as BaseIpAddress;

/**
 * Skeleton subclass for representing ip_address field from the cookie_register table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class IpAddress extends BaseIpAddress
{
}
