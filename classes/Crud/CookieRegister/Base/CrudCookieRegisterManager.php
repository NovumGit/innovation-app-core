<?php
namespace Crud\CookieRegister\Base;

use Core\Utils;
use Crud;
use Crud\CookieRegister\FieldIterator;
use Crud\CookieRegister\Field\CreatedDate;
use Crud\CookieRegister\Field\IpAddress;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CookieRegister;
use Model\Crm\CookieRegisterQuery;
use Model\Crm\Map\CookieRegisterTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CookieRegister instead if you need to override or add functionality.
 */
abstract class CrudCookieRegisterManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CookieRegisterQuery::create();
	}


	public function getTableMap(): CookieRegisterTableMap
	{
		return new CookieRegisterTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CookieRegister";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "cookie_register toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "cookie_register aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IpAddress', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IpAddress', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CookieRegister
	 */
	public function getModel(array $aData = null): CookieRegister
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCookieRegisterQuery = CookieRegisterQuery::create();
		     $oCookieRegister = $oCookieRegisterQuery->findOneById($aData['id']);
		     if (!$oCookieRegister instanceof CookieRegister) {
		         throw new LogicException("CookieRegister should be an instance of CookieRegister but got something else." . __METHOD__);
		     }
		     $oCookieRegister = $this->fillVo($aData, $oCookieRegister);
		}
		else {
		     $oCookieRegister = new CookieRegister();
		     if (!empty($aData)) {
		         $oCookieRegister = $this->fillVo($aData, $oCookieRegister);
		     }
		}
		return $oCookieRegister;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CookieRegister
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CookieRegister
	{
		$oCookieRegister = $this->getModel($aData);


		 if(!empty($oCookieRegister))
		 {
		     $oCookieRegister = $this->fillVo($aData, $oCookieRegister);
		     $oCookieRegister->save();
		 }
		return $oCookieRegister;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CookieRegister $oModel
	 * @return CookieRegister
	 */
	protected function fillVo(array $aData, CookieRegister $oModel): CookieRegister
	{
		if(isset($aData['ip_address'])) {
		     $oField = new IpAddress();
		     $mValue = $oField->sanitize($aData['ip_address']);
		     $oModel->setIpAddress($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
