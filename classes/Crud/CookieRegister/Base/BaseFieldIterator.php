<?php
namespace Crud\CookieRegister\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CookieRegister\ICollectionField as CookieRegisterField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CookieRegister\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CookieRegisterField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CookieRegisterField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CookieRegisterField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CookieRegisterField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CookieRegisterField
	{
		return current($this->aFields);
	}
}
