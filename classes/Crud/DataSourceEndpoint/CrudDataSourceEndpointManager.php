<?php
namespace Crud\DataSourceEndpoint;

use Crud\IApiAlwaysExpose;

/**
 * Skeleton subclass for representing a DataSourceEndpoint.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudDataSourceEndpointManager extends Base\CrudDataSourceEndpointManager implements IApiAlwaysExpose
{
}
