<?php
namespace Crud\DataSourceEndpoint\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataSourceEndpoint\ICollectionField as DataSourceEndpointField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataSourceEndpoint\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataSourceEndpointField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataSourceEndpointField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataSourceEndpointField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataSourceEndpointField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataSourceEndpointField
	{
		return current($this->aFields);
	}
}
