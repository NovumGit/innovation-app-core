<?php
namespace Crud\DataSourceEndpoint\Base;

use Core\Utils;
use Crud;
use Crud\DataSourceEndpoint\FieldIterator;
use Crud\DataSourceEndpoint\Field\Code;
use Crud\DataSourceEndpoint\Field\DatasourceEndpointMethodId;
use Crud\DataSourceEndpoint\Field\DatasourceId;
use Crud\DataSourceEndpoint\Field\Summary;
use Crud\DataSourceEndpoint\Field\Url;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataSourceEndpoint;
use Model\System\DataSourceEndpointQuery;
use Model\System\Map\DataSourceEndpointTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataSourceEndpoint instead if you need to override or add functionality.
 */
abstract class CrudDataSourceEndpointManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataSourceEndpointQuery::create();
	}


	public function getTableMap(): DataSourceEndpointTableMap
	{
		return new DataSourceEndpointTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle bekende endpoints van een API";
	}


	public function getEntityTitle(): string
	{
		return "DataSourceEndpoint";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "datasource_endpoint toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "datasource_endpoint aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DatasourceId', 'DatasourceEndpointMethodId', 'Code', 'Summary', 'Url'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DatasourceId', 'DatasourceEndpointMethodId', 'Code', 'Summary', 'Url'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataSourceEndpoint
	 */
	public function getModel(array $aData = null): DataSourceEndpoint
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataSourceEndpointQuery = DataSourceEndpointQuery::create();
		     $oDataSourceEndpoint = $oDataSourceEndpointQuery->findOneById($aData['id']);
		     if (!$oDataSourceEndpoint instanceof DataSourceEndpoint) {
		         throw new LogicException("DataSourceEndpoint should be an instance of DataSourceEndpoint but got something else." . __METHOD__);
		     }
		     $oDataSourceEndpoint = $this->fillVo($aData, $oDataSourceEndpoint);
		}
		else {
		     $oDataSourceEndpoint = new DataSourceEndpoint();
		     if (!empty($aData)) {
		         $oDataSourceEndpoint = $this->fillVo($aData, $oDataSourceEndpoint);
		     }
		}
		return $oDataSourceEndpoint;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataSourceEndpoint
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataSourceEndpoint
	{
		$oDataSourceEndpoint = $this->getModel($aData);


		 if(!empty($oDataSourceEndpoint))
		 {
		     $oDataSourceEndpoint = $this->fillVo($aData, $oDataSourceEndpoint);
		     $oDataSourceEndpoint->save();
		 }
		return $oDataSourceEndpoint;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataSourceEndpoint $oModel
	 * @return DataSourceEndpoint
	 */
	protected function fillVo(array $aData, DataSourceEndpoint $oModel): DataSourceEndpoint
	{
		if(isset($aData['datasource_id'])) {
		     $oField = new DatasourceId();
		     $mValue = $oField->sanitize($aData['datasource_id']);
		     $oModel->setDatasourceId($mValue);
		}
		if(isset($aData['datasource_endpoint_method_id'])) {
		     $oField = new DatasourceEndpointMethodId();
		     $mValue = $oField->sanitize($aData['datasource_endpoint_method_id']);
		     $oModel->setDatasourceEndpointMethodId($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['summary'])) {
		     $oField = new Summary();
		     $mValue = $oField->sanitize($aData['summary']);
		     $oModel->setSummary($mValue);
		}
		if(isset($aData['url'])) {
		     $oField = new Url();
		     $mValue = $oField->sanitize($aData['url']);
		     $oModel->setUrl($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
