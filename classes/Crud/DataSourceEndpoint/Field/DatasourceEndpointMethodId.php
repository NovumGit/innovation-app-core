<?php
namespace Crud\DataSourceEndpoint\Field;

use Crud\DataSourceEndpoint\Field\Base\DatasourceEndpointMethodId as BaseDatasourceEndpointMethodId;

/**
 * Skeleton subclass for representing datasource_endpoint_method_id field from the datasource_endpoint table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class DatasourceEndpointMethodId extends BaseDatasourceEndpointMethodId
{
}
