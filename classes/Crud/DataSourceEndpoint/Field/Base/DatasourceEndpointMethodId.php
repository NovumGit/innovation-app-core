<?php
namespace Crud\DataSourceEndpoint\Field\Base;

use Core\Utils;
use Crud\DataSourceEndpoint\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\DataSourceEndpointMethodQuery;

/**
 * Base class that represents the 'datasource_endpoint_method_id' crud field from the 'datasource_endpoint' table.
 * This class is auto generated and should not be modified.
 */
abstract class DatasourceEndpointMethodId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'datasource_endpoint_method_id';
	protected $sFieldLabel = 'Http method';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDatasourceEndpointMethodId';
	protected $sFqModelClassname = '\\\Model\System\DataSourceEndpoint';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\DataSourceEndpointMethodQuery::create()->orderBycode()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getcode", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\DataSourceEndpointMethodQuery::create()->findOneById($iItemId)->getcode();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['datasource_endpoint_method_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Http method" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
