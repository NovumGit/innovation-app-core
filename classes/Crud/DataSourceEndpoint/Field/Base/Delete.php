<?php 
namespace Crud\DataSourceEndpoint\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataSourceEndpoint;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSourceEndpoint)
		{
		     return "//system/datasource_endpoint/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSourceEndpoint)
		{
		     return "//datasource_endpoint?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
