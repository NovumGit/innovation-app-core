<?php
namespace Crud\DataSourceEndpoint\Field\Base;

use Crud\DataSourceEndpoint\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'summary' crud field from the 'datasource_endpoint' table.
 * This class is auto generated and should not be modified.
 */
abstract class Summary extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'summary';
	protected $sFieldLabel = 'Summary';
	protected $sIcon = 'globe';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSummary';
	protected $sFqModelClassname = '\\\Model\System\DataSourceEndpoint';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['summary']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Summary" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
