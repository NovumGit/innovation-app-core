<?php

namespace Crud\Unit;

use Core\Utils;
use Crud\IApiExposable;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\Map\ProductUnitTableMap;
use Model\Setting\MasterTable\ProductUnitQuery;
use Model\Setting\MasterTable\ProductUnit;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Map\TableMap;

class CrudUnitManager extends FormManager implements IEditableCrud, IApiExposable
{
    function getTableMap(): TableMap
    {
        return new ProductUnitTableMap();
    }

    function getShortDescription(): string
    {
        return 'Unit values such as meter, piece, inc etc';
    }

    function getQueryObject(): ModelCriteria
    {
        return ProductUnitQuery::create();
    }

    function getCreateNewUrl(): string
    {
        return '/setting/mastertable/unit/edit';
    }

    public function getEntityTitle(): string
    {
        return 'eenheid';
    }

    public function getNewFormTitle(): string
    {
        return 'Eenheid toevoegen';
    }

    public function getEditFormTitle(): string
    {
        return 'Eenheid wijzigen';
    }

    public function getDefaultOverviewFields(bool $bAddNs = false): ?array
    {
        $aColumns = [
            'Name',
            'NamePlural',
            'Delete',
            'Edit',
        ];

        if ($bAddNs) {
            array_walk($aColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }

    public function getDefaultEditFields(bool $bAddNs = false): ?array
    {

        $aColumns = [
            'Name',
            'NamePlural',
        ];

        if ($bAddNs) {
            array_walk($aColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }

    public function getModel(array $aData = null)
    {

        if ($aData['id']) {
            $oProductUnitQuery = new ProductUnitQuery();

            $oProductUnit = $oProductUnitQuery->findOneById($aData['id']);

            if (!$oProductUnit instanceof ProductUnit) {
                throw new LogicException("ProductUnit should be an instance of ProductUnit but got " . get_class($oProductUnit) . " in " . __METHOD__);
            }
        } else {
            $oProductUnit = new ProductUnit();
            if (isset($aData['name'])) {
                $oProductUnit->setName($aData['name']);
            }
            if (isset($aData['name_plural'])) {
                $oProductUnit->setNamePlural($aData['name_plural']);
            }
        }

        return $oProductUnit;
    }

    public function store(array $aData = null)
    {
        $oProductUnit = $this->getModel($aData);

        if (!empty($oProductUnit)) {
            if (isset($aData['name'])) {
                $oProductUnit->setName($aData['name']);
            }
            if (isset($aData['name_plural'])) {
                $oProductUnit->setNamePlural($aData['name_plural']);
            }
            $oProductUnit->save();
        }
        return $oProductUnit;
    }
}
