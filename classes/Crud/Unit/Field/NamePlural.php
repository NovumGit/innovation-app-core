<?php

namespace Crud\Unit\Field;

use Crud\Field;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Setting\MasterTable\ProductUnit as ModelObject;

class NamePlural extends Field implements IEditableField
{

    protected $sFieldLabel = 'Meervoud';
    protected $sFieldName = 'name_plural';
    protected $sPlaceHolder = 'Eenheid naam meervoud';
    protected $sIcon = 'tag';

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getDataType(): string
    {
        return 'string';
    }

    public function hasValidations()
    {
        return true;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        /*
        $oValidator = Validator::alpha();
        if(empty($aPostedData['id']))
        {
            $oProductUnit = ProductUnitQuery::create()->findOneByName($aPostedData['name_plural']);

            if($oProductUnit instanceof ProductUnit)
            {
                $mResponse[] = 'De meervoudige versie van de naam komt al voor in de database.';
            }
        }
        $sNamePlural = trim($aPostedData['name_plural']);
        if(empty($sNamePlural))
        {
            $mResponse[] = 'De meervoudig versie van de naam mag niet leeg zijn.';
        }
        else if(!$oValidator->length(2, 120)->validate($aPostedData['name_plural']))
        {
            $mResponse[] = 'De meervoudige versie van de eenheid naam moet minimaal 2 en mag maximaal 120 tekens lang zijn.';
        }
        */
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField('Eenheid naam meervoud', 'name_plural');
    }

    public function getOverviewValue($oProductUnitTranslation)
    {
        if (!$oProductUnitTranslation instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of  model\Setting\MasterTable\ProductUnit in " . __METHOD__);
        }
        return '<td class="">' . $oProductUnitTranslation->getNamePlural() . '</td>';
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
