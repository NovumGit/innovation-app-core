<?php

namespace Crud\Unit\Field;

use Crud\Field;
use Crud\IEditableField;
use InvalidArgumentException;
use Model\Setting\MasterTable\ProductUnit as ModelObject;
use Model\Setting\MasterTable\ProductUnitQuery;

class Name extends Field implements IEditableField
{

    protected $sFieldLabel = 'Eenheid';
    protected $sFieldName = 'name';
    protected $sIcon = 'tag';
    protected $sPlaceHolder = 'Eenhoud';

    function getDataType(): string
    {
        return 'string';
    }

    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function hasValidations()
    {
        return true;
    }

    function validate($aPostedData)
    {
        $mResponse = false;

        if (empty($aPostedData['id'])) {
            $oProductUnit = ProductUnitQuery::create()->findOneByName($aPostedData['name']);

            if ($oProductUnit instanceof ModelObject) {
                $mResponse[] = 'Er is al een eenheid met deze naam.';
            }
        }
        $sName = trim($aPostedData['name']);

        if (empty($sName)) {
            $mResponse[] = 'Het eenheid naam mag niet leeg zijn.';
        } else {
            if (strlen($aPostedData['name']) < 2) {
                $mResponse[] = 'Het veld eenheid naam moet minimaal 2 tekens lang zijn.';
            } else {
                if (strlen($aPostedData['name']) > 120) {
                    $mResponse[] = 'Het veld eenheid naam mag minimaal 120 tekens lang zijn.';
                }
            }
        }

        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    public function getOverviewValue($oProductUnit)
    {
        if (!$oProductUnit instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of  model\Setting\MasterTable\ProductUnit in " . __METHOD__);
        }
        return '<td class="">' . $oProductUnit->getName() . '</td>';
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getName(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
