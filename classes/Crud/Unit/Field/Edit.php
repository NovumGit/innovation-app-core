<?php

namespace Crud\Unit\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\ProductUnit;
use InvalidArgumentException;

class Edit extends GenericEdit
{

    public function getEditUrl($oProductUnit)
    {

        if (!$oProductUnit instanceof ProductUnit) {
            throw new InvalidArgumentException('Expected an instance of ProductUnit but got ' . get_class($oProductUnit));
        }

        return '/setting/mastertable/unit/edit?id=' . $oProductUnit->getId();
    }
}
