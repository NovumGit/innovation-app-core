<?php

namespace Crud\Unit\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\ProductUnit;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oProductMaterial)
    {

        if (!$oProductMaterial instanceof ProductUnit) {
            throw new InvalidArgumentException('Expected an instance of ProductMaterial but got ' . get_class($oProductMaterial));
        }

        return '/setting/mastertable/unit/edit?_do=Delete&id=' . $oProductMaterial->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
