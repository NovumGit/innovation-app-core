<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;

class CustomerVatNumber extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'customer_vat_number';
    protected $sFieldLabel = 'BTW klant';
    private $sIcon = 'tag';
    private $sPlaceHolder = '';
    private $sGetter = 'getCustomerVatNumber';

    function getDataType():string
    {
        return 'string';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function hasValidations() { return false; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
