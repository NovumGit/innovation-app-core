<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Model\Sale\SaleOrder as ModelObject;

class SaleOrderLineCount extends Field{

    protected $sFieldName = 'order_line_count';
    protected $sFieldLabel = 'Aantal producten';
    private $sIcon = 'bar-chart-o';
    private $sPlaceHolder = '';

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject but got ".get_class($oModelObject)." in ".__METHOD__);
        }
        $iCount = 0;
        if($oModelObject->getSaleOrderItems()){
            $iCount = $oModelObject->getSaleOrderItems()->count();
        }

        return '<td class="">'.$iCount.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $iCount = 0;
        if($mData->getSaleOrderItems()){
            $iCount = $mData->getSaleOrderItems()->count();
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $iCount, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}