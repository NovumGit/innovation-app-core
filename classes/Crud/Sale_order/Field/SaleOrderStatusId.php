<?php
namespace Crud\Sale_order\Field;

use Crud\Field;
use Core\Utils;
use Model\Sale\SaleOrder;
use InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\SaleOrderStatus as SaleOrderStatusModel;

class SaleOrderStatusId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'sale_order_status_id';
    protected $sFieldLabel = 'Order status';
    private $sPlaceHolder = 'Kies een order status';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['sale_order_status_id']))
        {
            $mResponse[] = 'Het veld orderstatus mag niet leeg zijn.';
        }

        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {
        $oSalesOrderStatusQuery = SaleOrderStatusQuery::create();
        $aSalesOrderStatuses = $oSalesOrderStatusQuery->find();
        return Utils::makeSelectOptions($aSalesOrderStatuses, 'getName', $mSelectedItem);
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oSalesOrderStatusQuery = SaleOrderStatusQuery::create();
        $oSalesOrderStatus = $oSalesOrderStatusQuery->findOneById($iItemId);

        if($oSalesOrderStatus instanceof SaleOrderStatus)
        {
            return $oSalesOrderStatus->getName();
        }
        return '';
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oSaleOrder)
    {
        if(!$oSaleOrder instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder in ".__METHOD__);
        }
        $iSaleOrderStatusId = $oSaleOrder->getSaleOrderStatusId();
        $sSaleOrderStatus = '';
        $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneById($iSaleOrderStatusId);


        if($oSaleOrderStatus instanceof SaleOrderStatusModel)
        {
            $sSaleOrderStatus = $oSaleOrderStatus->getName();
        }
        return '<td class="">'.$sSaleOrderStatus.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder ".__METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData->getSaleOrderStatusId());
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getSaleOrderStatusId(), $aDropdownOptions, $bReadonly, 'check', $this->sPlaceHolder);
    }
}
