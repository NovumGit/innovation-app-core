<?php
namespace Crud\Sale_Order\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Sale\SaleOrder as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }
        $sAddUrl = '';
        if(isset($_GET['tab']))
        {
            $sAddUrl = '&tab='.$_GET['tab'];
        }
        return '/order/edit/edit?_do=Delete&order_id='.$oModelObject->getId().$sAddUrl;
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}