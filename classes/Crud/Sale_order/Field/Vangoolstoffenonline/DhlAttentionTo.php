<?php
namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Model\Sale\DhlShipment;
use Model\Sale\DhlShipmentQuery;
use model\Sale\SaleOrder as ModelObject;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class DhlAttentionTo extends Field implements ICustomCrudField {

	protected $sFieldLabel = 'Ter attentie van (dhl)';
	private $sGetter = '';

	function store(ActiveRecordInterface $oObject, array $aData)
	{
	}
	function getGetter()
	{
		return $this->sGetter;
	}
	function getDataType():string
	{
		return 'string';
	}
	function getFieldName()
	{
		return null;
	}
	function hasValidations() { return false; }
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}
	function getOverviewHeader()
	{
		$aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
		return $this->sortableHeaderField($this->getTranslatedTitle(), null, $aAddVars);
	}
	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
	}
	function getFieldTitle(){
		return $this->getTranslatedTitle();
	}
	function getEditHtml($mData, $bReadonly)
	{
		if(!$mData instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		$oDhlShipment = DhlShipmentQuery::create()->findOneBySaleOrderId($mData->getId());

		if(!$oDhlShipment instanceof DhlShipment)
		{
			$oDhlShipment = new DhlShipment();
		}

		$sFirstName = $oDhlShipment->getAttnToFirstname();
		$sLastName = $oDhlShipment->getAttnToLastname();
		$oSaleOrder = $oDhlShipment->getSaleOrder();

		if(empty($sFirstName))
		{
			$sFirstName = explode(' ', $oSaleOrder->getCustomerDeliveryAttnName())[0];
		}
		if(empty($sLastName))
		{
			$sLastName = array_reverse(explode(' ', $oSaleOrder->getCustomerDeliveryAttnName()))[0];
		}

		$aOut = [];
		$aOut[] = '<div class="form-group">';
		$aOut[] = '		<label for="fld_attention_to" class="col-lg-4 control-label">';
		$aOut[] = '         Ter attentie van';
		$aOut[] = '		</label>';
		$aOut[] = '		<div class="col-lg-3">';
        $aOut[] = '			<div class="input-group">';
        $aOut[] = '				<span class="input-group-addon">';
		$aOut[] = '					<i class="fa fa-user"></i>';
        $aOut[] = '				</span>';
		$aOut[] = '		        <input name="data[attn_to_firstname]" id="fld_attn_to_firstname" class="form-control" data-toggle="tooltip" data-placement="top"  placeholder="Ter attentie van (voornaam)" value="' . $sFirstName. '" type="text">';
		$aOut[] = '			</div>';
		$aOut[] = '		</div>';
		$aOut[] = '		<div class="col-lg-4">';
		$aOut[] = '			<div class="input-group">';
		$aOut[] = '				<span class="input-group-addon">';
		$aOut[] = '					<i class="fa fa-user"></i>';
		$aOut[] = '				</span>';
		$aOut[] = '		        <input name="data[attn_to_lastname]" id="fld_attn_to_lastname" class="form-control" data-toggle="tooltip" data-placement="top"  placeholder="Ter attentie van (achternaam)" value="' . $sLastName. '" type="text">';
		$aOut[] = '			</div>';
		$aOut[] = '		</div>';
		$aOut[] = '</div>';

		return join(PHP_EOL, $aOut);
	}
}
