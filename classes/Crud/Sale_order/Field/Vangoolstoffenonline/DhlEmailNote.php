<?php
namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Sale\DhlShipment;
use Model\Sale\DhlShipmentQuery;
use model\Sale\SaleOrder as ModelObject;
use Model\Sale\SaleOrder;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;


class DhlEmailNote extends Field implements ICustomCrudField {

	protected $sFieldName = 'email_note';
	protected $sFieldLabel = 'Persoonlijk bericht (dhl)';
	private $sIcon = 'building';
	private $sPlaceHolder = '';
	private $sGetter = '';

	function store(ActiveRecordInterface $oObject, array $aData)
	{
	}
	function getGetter()
	{
		return $this->sGetter;
	}
	function getDataType():string
	{
		return 'string';
	}
	function getFieldName()
	{
		return $this->sFieldName;
	}
	function hasValidations() { return false; }
	function validate($aPostedData)
	{
		$mResponse = false;
		return $mResponse;
	}
	function getOverviewHeader()
	{
		$aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
		return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
	}
	function getOverviewValue($oModelObject)
	{
		if(!$oModelObject instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
	}
	function getFieldTitle(){
		return $this->getTranslatedTitle();
	}
	function getEditHtml($mData, $bReadonly)
	{
		if(!$mData instanceof ModelObject)
		{
			throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
		}
		$oDhlShipment = DhlShipmentQuery::create()->findOneBySaleOrderId($mData->getId());

		if(!$oDhlShipment instanceof DhlShipment)
		{
			$oDhlShipment = new DhlShipment();
		}
		return $this->editTextArea($this->getTranslatedTitle(), $this->sFieldName, $oDhlShipment->getEmailNote(), $this->sPlaceHolder, null, false);
	}
}
