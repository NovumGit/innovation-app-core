<?php
namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Api\Push\Shipping\Citymail\GetShippingmethodsVo;
use Api\Push\Shipping\Citymail\TransferVo;
use Crud\Field;
use Crud\ICustomCrudField;
use Exception\LogicException;
use Model\Sale\CitymailShipment;
use Model\Sale\CitymailShipmentQuery;
use Model\Sale\SaleOrder;
use InvalidArgumentException;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\SaleOrderStatus as SaleOrderStatusModel;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class CitymailShippingMethod extends Field implements ICustomCrudField {

    protected $sFieldName = 'shipment_option';
    protected $sFieldLabel = 'Verzendmethode (citymail)';
    private $sPlaceHolder = 'Kies een verzendmethode';

    function store(ActiveRecordInterface $oObject, array $aData)
    {
        ini_set('display_errors', 0);
    	if(!isset($aData['shipment_option']))
	    {
	    	return;
	    }
        if(!$oObject instanceof SaleOrder)
        {
            throw new LogicException('Expected an instance of SaleOrder');
        }
        $oCitymailShipmentQuery = CitymailShipmentQuery::create();
        $oCitymailShipmentQuery->filterBySaleOrderId($oObject->getId());
        $oCitymailShipment = $oCitymailShipmentQuery->findOne();

        if(!$oCitymailShipment instanceof CitymailShipment)
        {
            $oCitymailShipment = new CitymailShipment();
            $oCitymailShipment->setSaleOrderId($oObject->getId());
        }
        $oCitymailShipment->setShipmentOption($aData['shipment_option']);
        $oCitymailShipment->save();

    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {
        if(!$mSelectedItem instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of SaleOrder, got ".get_class($mSelectedItem));
        }
        $oCitymailShipment = CitymailShipmentQuery::create()->findOneBySaleOrderId($mSelectedItem->getId());
        if(!$oCitymailShipment instanceof CitymailShipment)
        {
            $oCitymailShipment = new CitymailShipment();
        }
        if(!$oCitymailShipment->getWeight())
        {
            $oCitymailShipment->setWeight(1000);
        }
        $sCountry = $mSelectedItem->getCustomerDeliveryCountry();
        if(strtolower($sCountry) == 'the netherlands')
        {
            $sCountry = 'Nederland';
        }
        $oCountry = CountryQuery::create()->findOneByName($sCountry);

        if(!$oCountry instanceof Country)
        {
            $oCountry = CountryQuery::create()->findOneByName('the');
        }

        $oGetShippingmethodsVo = new GetShippingmethodsVo(
                                        $mSelectedItem->getCustomerDeliveryPostal(),
                                        $mSelectedItem->getCustomerDeliveryNumber(),
                                        $mSelectedItem->getTotalPrice(),
                                        $oCitymailShipment->getWeight(),
                                        $oCountry->getIso2(),
                                        1);
        $aShippingMethods = TransferVo::call($oGetShippingmethodsVo);
        $aDropdownOptions = [];
        if($aShippingMethods)
        {
            foreach($aShippingMethods as $aShippingMethod)
            {
                $bSelected = ($oCitymailShipment->getShipmentOption() == $aShippingMethod['option_key']) ? 'selected' : '';
                $aDropdownOptions[] = ['id' => $aShippingMethod['option_key'], 'selected' => $bSelected ,'label' => $aShippingMethod['option_name']];
            }
        }
        return $aDropdownOptions;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oSalesOrderStatusQuery = SaleOrderStatusQuery::create();
        $oSalesOrderStatus = $oSalesOrderStatusQuery->findOneById($iItemId);
        if($oSalesOrderStatus instanceof SaleOrderStatus)
        {
            return $oSalesOrderStatus->getName();
        }
        return '';
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oSaleOrder)
    {
        if(!$oSaleOrder instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder in ".__METHOD__);
        }
        $iSaleOrderStatusId = $oSaleOrder->getSaleOrderStatusId();
        $sSaleOrderStatus = '';
        $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneById($iSaleOrderStatusId);
        if($oSaleOrderStatus instanceof SaleOrderStatusModel)
        {
            $sSaleOrderStatus = $oSaleOrderStatus->getName();
        }
        return '<td class="">'.$sSaleOrderStatus.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder ".__METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData);
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getSaleOrderStatusId(), $aDropdownOptions, $bReadonly, 'check', $this->sPlaceHolder);
    }
}
