<?php
namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Crud\Field;
use Crud\ICustomCrudField;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Sale\CitymailShipment;
use Model\Sale\CitymailShipmentQuery;
use model\Sale\SaleOrder as ModelObject;
use Model\Sale\SaleOrder;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;


class CitymailWeight extends Field implements ICustomCrudField {

    protected $sFieldName = 'citymail_weight';
    protected $sFieldLabel = 'Gewicht (citymail)';
    private $sIcon = 'building';
    private $sPlaceHolder = '';
    private $sGetter = '';

    function store(ActiveRecordInterface $oObject, array $aData)
    {
    	if(!isset($aData['citymail_weight']))
	    {
	    	return;
	    }
        if(!$oObject instanceof SaleOrder)
        {
            throw new LogicException('Expected an instance of SaleOrder');
        }
        $oCitymailShipmentQuery = CitymailShipmentQuery::create();
        $oCitymailShipmentQuery->filterBySaleOrderId($oObject->getId());
        $oCitymailShipment = $oCitymailShipmentQuery->findOne();

        if(!$oCitymailShipment instanceof CitymailShipment)
        {
            $oCitymailShipment = new CitymailShipment();
            $oCitymailShipment->setSaleOrderId($oObject->getId());
        }
        $oCitymailShipment->setWeight($aData['citymail_weight']);
        $oCitymailShipment->save();
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCitymailShipment = CitymailShipmentQuery::create()->findOneBySaleOrderId($mData->getId());

        if(!$oCitymailShipment instanceof CitymailShipment)
        {
            $oCitymailShipment = new CitymailShipment();
        }
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oCitymailShipment->getWeight(), $this->sPlaceHolder, $this->sIcon, false);
    }
}
