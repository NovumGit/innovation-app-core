<?php
namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Sale\SaleOrder;

class CitymailPickButton extends Field implements IEventField{

    protected $sFieldLabel = 'CityMail order picken';
    function getIcon()
    {
        return 'dropbox';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oSaleOrder = $mData;

        // Nadat de gebruiker op opslaan klikt gaan we nergens heen.
        $sItemUrl = '/order/edit/edit?order_id='.$oSaleOrder->getId().'&r=current_url_'.$mData->getId().'&view=citymail_pick';
        DeferredAction::register('current_url_'.$mData->getId(), $sItemUrl);

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Orderpicken" href="'.$sItemUrl.'" class="btn btn-dark br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        return 'overview_only_field';
    }
}
