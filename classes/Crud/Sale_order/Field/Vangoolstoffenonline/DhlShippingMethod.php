<?php

namespace Crud\Sale_Order\Field\Vangoolstoffenonline;

use Api\Push\Shipping\Dhl\LabelGenerator;
use Core\Cfg;
use Core\StatusMessage;
use Crud\Field;
use Crud\ICustomCrudField;
use Dhl\ApiClient;
use Exception\LogicException;
use Model\Sale\DhlShipment;
use Model\Sale\DhlShipmentQuery;
use Model\Sale\SaleOrder;
use InvalidArgumentException;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\ShippingMethod;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class DhlShippingMethod extends Field implements ICustomCrudField
{

    protected $sFieldName = 'shipment_option';
    protected $sFieldLabel = 'Verzendmethode (dhl)';

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getLookups($mSelectedItem = null)
    {
        if (!$mSelectedItem instanceof SaleOrder) {
            throw new LogicException("Expected an instance of SaleOrder, got " . get_class($mSelectedItem));
        }

        $oDhlShipment = DhlShipmentQuery::create()->findOneBySaleOrderId($mSelectedItem->getId());
        if (!$oDhlShipment instanceof DhlShipment) {
            $oDhlShipment = new DhlShipment();
        }

        $sCountry = $mSelectedItem->getCustomerDeliveryCountry();
        if (strtolower($sCountry) == 'the netherlands') {
            $sCountry = 'Nederland';
        }
        $oCountry = CountryQuery::create()->findOneByName($sCountry);

        if (!$oCountry instanceof Country) {
            throw new LogicException("No country found when selecting DHL shipment options.");
        }

        ini_set('display_errors', 0);
        $oClient = new ApiClient([
            'apiUser' => Cfg::get('DHL_API_USER_ID'),
            'apiKey' => Cfg::get('DHL_API_KEY'),
            'accountId' => Cfg::get('DHL_API_ACCOUNT_ID'),
            'organisationId' => Cfg::get('DHL_API_ORGANIZATION_ID'),
        ]);


        // consumer
        // parcelShop
        // business

        try
        {
            $oSaleOrder = $oDhlShipment->getSaleOrder();
        }
        catch (\Exception $e)
        {
            StatusMessage::warning($e->getMessage());
        }

        $vat = null;
        if ($oSaleOrder == null) {
            StatusMessage::warning("Kon geen DHL verzendgegevens vinden, waarschijnlijk is deze order van voor de DHL integratie. Verzenden op de oude manier.");
        }

        try {
            ini_set('display_errors', 0);
            error_reporting(E_ALL);
            $parameters = [
                'fromCountry' => CountryQuery::create()->findOneByName($mSelectedItem->getOurGeneralCountry())->getIso2(),
                'toCountry' => $oCountry->getIso2(),
                'senderType' => 'business',
                'fromPostalCode' => $mSelectedItem->getOurGeneralPostal(),
                'toPostalCode' => $mSelectedItem->getCustomerDeliveryPostal(),
            ];

//          $parameters['toBusiness'] = true;
            $aResult = $oClient->capabilities($parameters);

        } catch (\Exception $e) {
            StatusMessage::danger($e->getMessage());
        }

        $aPickup = null;
        if ($oDhlShipment instanceof DhlShipment && $oDhlShipment->getPickupPoint()) {
            try {

                $oPickup = $oClient->findParcelShopLocationsById(['countryCode' => $oCountry->getIso2(), 'id' => $oDhlShipment->getPickupPoint()]);

                if ($oPickup instanceof \GuzzleHttp\Command\Result) {
                    $aPickup = $oPickup->toArray();

                    StatusMessage::info($aPickup['name'] . ' ' . $aPickup['address']['street'] . ' ' . $aPickup['address']['number'] . ', ' . $aPickup['address']['city'] . ' - Zakelijk: ' . ($aPickup['address']['isBusiness'] ? 'Ja' : 'Nee'), 'Afhaalpunt');
                }
            } catch (\Exception $e) {

                StatusMessage::danger($e->getMessage());
            }
        }

        $aOut = [];
        $aOrderType = ['DFY', 'EUROPLUS'];
        $aOrderSize = ['SMALL', 'MEDIUM', 'LARGE'];

        $bHasDfy = false;
        foreach ($aResult as $aItem) {
            if ($aItem['product']['label'] == 'DFY') {
                $bHasDfy = true;
            }
        }

        foreach ($aOrderType as $sType) {
            foreach ($aOrderSize as $sSize) {
                foreach ($aResult as $aItem) {
                    if ($bHasDfy && $sType == 'EUROPLUS') {
                        continue;
                    }
                    if ($aItem['product']['label'] == $sType) {
                        if ($aItem['parcelType']['key'] == $sSize) {
                            $aOut[] = $aItem;
                        }
                    }
                }
            }
        }
        return $aOut;

    }

    function getDataType():string
    {
        return 'lookup';
    }

    function getVisibleValue($iItemId)
    {
        $oSalesOrderStatusQuery = SaleOrderStatusQuery::create();
        $oSalesOrderStatus = $oSalesOrderStatusQuery->findOneById($iItemId);
        if ($oSalesOrderStatus instanceof SaleOrderStatus) {
            return $oSalesOrderStatus->getName();
        }
        return '';
    }

    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }

    function getOverviewValue($oSaleOrder)
    {
        if (!$oSaleOrder instanceof SaleOrder) {
            throw new InvalidArgumentException("Expected an instance of SaleOrder in " . __METHOD__);
        }
        $sName = '';
        if ($oSaleOrder->getShippingMethod() instanceof ShippingMethod) {
            $sName = $oSaleOrder->getShippingMethod()->getName();
        }

        return '<td class="">' . $sName . '</td>';
    }

    function store(ActiveRecordInterface $oObject, array $aData)
    {
        // Er
        ini_set('display_errors', 1);
        if (!isset($_POST['dhl_method'])) {
            return;
        }
        $sDhlMethod = $_POST['dhl_method'];

        $aOptions = $_POST[$sDhlMethod];
        $sDeliveryMethodDhlName = $aOptions['delivery_method_dhl_name'];

        if (!$oObject instanceof SaleOrder) {
            throw new LogicException('Expected an instance of SaleOrder');
        }

        $oDhlShipmentQuery = DhlShipmentQuery::create();
        $oDhlShipmentQuery->filterBySaleOrderId($oObject->getId());
        $oDhlShipment = $oDhlShipmentQuery->findOne();

        if (!$oDhlShipment instanceof DhlShipment) {
            $oDhlShipment = new DhlShipment();
            $oDhlShipment->setSaleOrderId($oObject->getId());
        }

        //$oDhlShipment->setDeliveryMethod($aOptions['delivery_method_dhl_name']);
        $oDhlShipment->setShipmentOption($sDhlMethod);
        $oDhlShipment->setEmailNote($aData['email_note']);
        $oDhlShipment->setReference($aData['reference']);
        $oDhlShipment->setWeight($aData['dhl_weight']);
        $oDhlShipment->setDeliveryMethodDhlName($sDeliveryMethodDhlName);
        $oDhlShipment->setAttnToLastname($aData['attn_to_lastname']);
        $oDhlShipment->setAttnToFirstname($aData['attn_to_firstname']);

        unset($aOptions['delivery_method_dhl_name']);
        if (!empty($aOptions)) {
            $oDhlShipment->setAdditionalFeatures(json_encode($aOptions));
        } else {
            $oDhlShipment->setAdditionalFeatures(null);
        }
        $oDhlShipment->save();

        LabelGenerator::generate($oObject->getId());
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof SaleOrder) {
            throw new InvalidArgumentException("Expected an instance of SaleOrder " . __METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData);

        $oDhlShipment = DhlShipmentQuery::create()->findOneBySaleOrderId($mData->getId());

        if (!$oDhlShipment instanceof DhlShipment) {
            $oDhlShipment = new DhlShipment();
        }

        if (!empty($aDropdownOptions)) {
            $i = 0;
            $aOut = [];
            foreach ($aDropdownOptions as $aOption) {
                if ($aOption['parcelType']['key'] == 'PALLET') {
                    continue;
                }

                $i++;
                if ($i == 0 || ($i % 3) == 0) {
                    $aOut[] = '<div class="row">';
                }
                $aOut[] = '	<div class="col-md-4">';

                $checked = '';
                if ($aOption['parcelType']['key'] == 'SMALL' && $aOption['product']['label'] == 'DFY') {
                    $checked = 'checked';
                }

                $sProductKey = $aOption['product']['key'] . ',' . $aOption['parcelType']['key'];

                $aOut[] = " 	<input id=\"fld_radio_{$i}\" type=\"radio\" $checked name=\"dhl_method\" value='" . $sProductKey . "' /> ";
                $aOut[] = '	<label style="cursor:pointer" for="fld_radio_' . $i . '">' . $aOption['product']['label'] . ", ";
                $aOut[] = $aOption['parcelType']['key'];

                if (isset($aOption['parcelType']['price'])) {
                    $aOut[] = '&euro; ' . number_format($aOption['parcelType']['price']['withTax'], 2, ',', '.') . ", ";
                }
                $aOut[] = '</label><br>';
                $aOut[] = '<ul style="list-style-type: none; margin-left:0;padding-left:5px;">';

                $bAnyShippingRadioChecked = false;
                foreach ($aOption['options'] as $aAvailableOptionForShippingMethod) {
                    /*
                    if(in_array($aAvailableOptionForShippingMethod['key'], ['H', 'EXP', 'SSN', 'BOUW', 'COD_CASH', 'EXW', 'PERS_NOTE', 'REFERENCE', 'ADD_RETURN_LABEL', 'REFERENCE2', 'INS']))
                    {
                        continue;
                    }
                    */
                    if ($aAvailableOptionForShippingMethod['key'] == 'PS' && empty($oDhlShipment->getPickupPoint())) {
                        continue;
                    }

                    $sLocalDeliveryMethodName = $oDhlShipment->getDeliveryMethod();

                    $aLocaLToDhlShippingMethods = [
                        'picked_up' => 'PS', // Parcel shop
                        'door_delivery' => 'DOOR',
                        'letter' => 'BP' //Briefpost
                    ];

                    if ($oDhlShipment->getDeliveryMethodDhlName()) {
                        $sDhlDeliveryMethodName = $oDhlShipment->getDeliveryMethodDhlName();
                    } else {
                        $sDhlDeliveryMethodName = $aLocaLToDhlShippingMethods[$sLocalDeliveryMethodName];
                    }
                    $sChecked = '';
                    $sFieldType = null;

                    if ($aAvailableOptionForShippingMethod['key'] == $sDhlDeliveryMethodName) {
                        $sChecked = 'checked';
                    }

                    $aOut[] = '<li>';

                    if (in_array($aAvailableOptionForShippingMethod['key'], array_values($aLocaLToDhlShippingMethods))) {

                        if (!$bAnyShippingRadioChecked) {
                            $bOneInLoopIsMatch = false;
                            foreach ($aOption['options'] as $aTmpSecondLoop) {

                                if ($aTmpSecondLoop['key'] == $sDhlDeliveryMethodName) {
                                    $bOneInLoopIsMatch = true;
                                }
                            }
                            if (!$bOneInLoopIsMatch) {
                                $sChecked = 'checked';

                                $bAnyShippingRadioChecked = true;
                            }
                        }

                        // This is actually the shipping method
                        $aOut[] = "	<input id=\"fld_checkbox_{$i}_{$aAvailableOptionForShippingMethod['key']}\" name=\"" . $sProductKey . "[delivery_method_dhl_name]\" value=\"{$aAvailableOptionForShippingMethod['key']}\" type=\"radio\" $sChecked /> ";
                    } else {
                        // Other options / the checkboxes
                        $aOut[] = "	<input id=\"fld_checkbox_{$i}_{$aAvailableOptionForShippingMethod['key']}\" name=\"" . $sProductKey . "[{$aAvailableOptionForShippingMethod['key']}]\" value=\"1\" type=\"checkbox\" $sChecked /> ";
                    }
                    $sDescription = str_replace(' or DHL Parcelstation', '', $aAvailableOptionForShippingMethod['description']);
                    $sDescription = str_replace('the specified DHL ', '', $sDescription);
                    $aOut[] = '   <label for="fld_checkbox_' . $i . '_' . $aAvailableOptionForShippingMethod['key'] . '" style="font-weight:normal;cursor:pointer;">' . $sDescription . '</label>';
                    $aOut[] = '</li>';
                }
                $aOut[] = '</ul>';
                $aOut[] = '</div>';

                if ($i == 0 || ($i % 3) == 0) {
                    $aOut[] = '</div>';
                }
            }
        }

        $aOut[] = '<div class="cl"></div>';
        $aOut[] = '<hr>';
        $aOut[] = '<div style="margin-bottom: 20px;"></div>';
        return join('', $aOut);
    }
}
