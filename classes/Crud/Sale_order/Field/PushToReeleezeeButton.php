<?php
namespace Crud\Sale_Order\Field;

use Core\DeferredAction;
use Core\Utils;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Sale\SaleOrder;

class PushToReeleezeeButton extends Field implements IEventField{

    protected $sFieldLabel = 'Push to reeleezee';
    function getIcon()
    {
        return 'rocket';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oSaleOrder = $mData;


        $sCurrentURL  = Utils::getRequestUri();
        DeferredAction::register('after_push', $sCurrentURL);

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Push to Reeleezee" href="/order/trigger/event?r=after_push&event=Push_to_reeleezee&event_argument[order_id]='.$oSaleOrder->getId().'&immediately=true" class="btn btn-dark br2 btn-xs fs12 d" style="background-color:#f48221!important;">';
        $aOut[] = '  <i class="fa fa-rocket"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        return 'overview_only_field';
    }
}
