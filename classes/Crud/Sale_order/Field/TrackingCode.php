<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Core\Currency;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;

class TrackingCode extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'tracking_code';
    protected $sFieldLabel = 'Tracking code';
    private $sIcon = 'globe';
    private $sPlaceHolder = '';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getGetter()
    {
        return 'getTrackingCode';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        // Make the column as small as possible without going to the next line.
        $aAddVars['th_attributes'] = 'style="width:1%;white-space: nowrap;"';
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return '<td class="text-right">'.$oModelObject->getTrackingCode().'</td>';
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $mData->getTrackingCode(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
