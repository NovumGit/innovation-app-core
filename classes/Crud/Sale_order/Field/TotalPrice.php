<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Core\Currency;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;

class TotalPrice extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'invoice_number';
    protected $sFieldLabel = 'Totaal bedrag';
    private $sIcon = 'money';
    private $sPlaceHolder = '';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getGetter()
    {
        return 'getTotalPrice';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        // Make the column as small as possible without going to the next line.
        $aAddVars['th_attributes'] = 'style="width:1%;white-space: nowrap;"';
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $fTotalPrice = Currency::priceFormat($oModelObject->getTotalPrice());
        return '<td class="text-right">'.$fTotalPrice.'</td>';
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        $bReadonly = true;
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $fTotalPrice = Currency::priceFormat($mData->getTotalPrice());
        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $fTotalPrice, $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
