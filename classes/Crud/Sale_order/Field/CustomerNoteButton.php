<?php
namespace Crud\Sale_Order\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Sale\SaleOrder;

class CustomerNoteButton extends Field implements IEventField{

    protected $sFieldLabel = 'Heeft notitie indicator';
    function getIcon()
    {
        return 'warning';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oSaleOrder = $mData;



        DeferredAction::register('after_show_note', $_SERVER['REQUEST_URI']);

        $sIconColor = 'default';
        $sNote = $oSaleOrder->getCustomerOrderNote();
        if($sNote)
        {
            $sIconColor = 'warning';
        }
        else
        {
            $sNote = '';
        }



        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="'.htmlspecialchars($sNote).'" href="/order/summary?_do=ShowNote&order_id='.$oSaleOrder->getId().'" class="btn btn-'.$sIconColor.' br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        return 'overview_only_field';
    }
}
