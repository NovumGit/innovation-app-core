<?php
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Sale\SaleOrder;

class TrackingButton extends Field implements IEventField{

    protected $sFieldLabel = 'Track & trace';
    function getIcon()
    {
        return 'globe';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oSaleOrder = $mData;


        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Track & trace invoeren" href="/order/edit/edit?order_id='.$oSaleOrder->getId().'&r=order_overview_back_url&view=track_trace" class="btn btn-dark br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        return 'overview_only_field';
    }
}
