<?php
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Sale\SaleOrder;

class SummaryButton extends Field implements IEventField{

    protected $sFieldLabel = 'Samenvatting bekijken';
    function getIcon()
    {
        return 'list-ol';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of Customer, got ".get_class($mData));
        }
        $oSaleOrder = $mData;


        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Samenvatting bekijken" href="/order/summary?order_id='.$oSaleOrder->getId().'" class="btn btn-dark br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
