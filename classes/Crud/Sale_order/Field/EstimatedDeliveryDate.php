<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Core\Config;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;


class EstimatedDeliveryDate extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'estimated_delivery_date';
    protected $sFieldLabel = 'Leverdatum';
    private $sGetter = 'getEstimatedDeliveryDate';

    function getDataType():string
    {
        return 'string';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function hasValidations() { return false; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject but got ".get_class($oModelObject)." in ".__METHOD__);
        }

        $oEstimatedDeliveryDate = $oModelObject->{$this->sGetter}();

        $sEstimatedDeliveryDate = '';
        if($oEstimatedDeliveryDate instanceof \DateTime)
        {
            $sEstimatedDeliveryDate = $oEstimatedDeliveryDate->format(Config::getDateFormat());
        }

        return '<td class="">'.$sEstimatedDeliveryDate.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oEstimatedDeliveryDate = $mData->{$this->sGetter}();

        $sEstimatedDeliveryDate = '';
        if($oEstimatedDeliveryDate instanceof \DateTime)
        {
            $sEstimatedDeliveryDate = $oEstimatedDeliveryDate->format(Config::getDateFormat());
        }

        return $this->editDatePicker($this->getTranslatedTitle(), $this->sFieldName, $sEstimatedDeliveryDate);
    }
}
