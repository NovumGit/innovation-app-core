<?php
namespace Crud\Sale_order\Field;

use Crud\Field;
use Core\Utils;
use Model\Sale\SaleOrder;
use InvalidArgumentException;
use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery;
use Crud\IFilterableLookupField;

class ShippingMethodId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'shipping_method_id';
    protected $sFieldLabel = 'Verzend methode';
    private $sPlaceHolder = 'Kies een verzendmethode';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(empty($aPostedData['shipping_method_id']))
        {

            $mResponse[] = 'U moet verplicht een verzendmethode opgeven.';
        }

        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {
        $aShippingMethods = ShippingMethodQuery::create()->orderByName()->find();
        return Utils::makeSelectOptions($aShippingMethods, 'getName', $mSelectedItem);
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getVisibleValue($iItemId)
    {
        $oShippingMethodQuery = ShippingMethodQuery::create();
        $oShippingMethod = $oShippingMethodQuery->findOneById($iItemId);


        if($oShippingMethod instanceof ShippingMethod)
        {
            return $oShippingMethod->getName();
        }
        return '';
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oSaleOrder)
    {
        $sShippingMethod = '';
        if(!$oSaleOrder instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder in ".__METHOD__);
        }
        $oShippingMethod = $oSaleOrder->getShippingMethod();

        if($oShippingMethod instanceof ShippingMethod)
        {
            $sShippingMethod = $oShippingMethod->getName();
        }
        return '<td class="">'.$sShippingMethod.'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder ".__METHOD__);
        }
        $aDropdownOptions = $this->getLookups($mData->getShippingMethodId());
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getShippingMethodId(), $aDropdownOptions, $bReadonly, 'check', $this->sPlaceHolder);
    }
}
