<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Core\Config;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;

class CreatedOn extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'created_on';
    protected $sFieldLabel = 'Aangemaakt op';
    private $sGetter = 'getCreatedOn';
    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oCreatedOn = $oModelObject->getCreatedOn();


        $sCreatedOn = '';
        if($oCreatedOn instanceof \DateTime)
        {
            $sCreatedOn = $oModelObject->getCreatedOn()->format(Config::getDateTimeFormat());
        }

        return '<td class="">'.$sCreatedOn.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editDateTimePicker($this->getTranslatedTitle(), $this->sFieldName, $mData->getCreatedOn());
    }
}
