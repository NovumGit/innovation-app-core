<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\Sale\SaleOrder as ModelObject;
use Model\Setting\MasterTable\Base\CountryQuery;

class CustomerInvoiceCountry extends Field implements IFilterableLookupField, IDisplayableField {

    protected $sFieldName = 'customer_invoice_country';
    protected $sFieldLabel = 'fact. Land';
    private $sIcon = 'map-marker';
    private $sPlaceHolder = '';
    private $sGetter = 'getCustomerInvoiceCountry';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getLookups($mSelectedItem = null)
    {

        $aCountries = CountryQuery::create()->orderByName()->find();
        $aOut = [];
        foreach($aCountries as $oCountry)
        {
            $sSelected = ($mSelectedItem == $oCountry->getName()) ? 'selected' : '';
            $aOut[] = ['label' => $oCountry->getName(), 'id' => $oCountry->getName(), 'selected' => $sSelected];
        }
        return $aOut;
    }
    function getVisibleValue($iItemId)
    {
        // Landen namen worden in de sale_order tabel als tekst opslagen.
        return $iItemId;
    }

    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}();

        if($sValue == 'United States of America')
        {
            $sValue = $oModelObject->getCustomerDeliveryUsaState().', USA';
        }

        return '<td class="">' . $sValue .'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getCustomerInvoiceCountry());

        array_unshift($aDropdownOptions, ['id' => null, 'selected' => '', 'label' => 'Maak een keuze']);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCustomerInvoiceCountry(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
