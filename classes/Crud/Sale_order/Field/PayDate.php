<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Sale\SaleOrder as ModelObject;
use Core\Config;

class PayDate extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'pay_date';
    protected $sFieldLabel = 'Betaal datum';
    private $sGetter = 'getPayDate';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oPayDate =  $oModelObject->getPayDate();

        $sPayDate = '';
        if($oPayDate instanceof \DateTime)
        {
            $sPayDate = $oPayDate->format(Config::getDateFormat());
        }

        return '<td class="">'.$sPayDate.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editDatePicker($this->getTranslatedTitle(), $this->sFieldName, $mData->getPayDate());


    }
}
