<?php
namespace Crud\Sale_Order\Field;

use Core\DeferredAction;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Field;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IInlineEditor;
use Crud\Sale_order_item\CrudSale_order_itemManager;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderItemQuery;
use Model\Setting\CrudManager\CrudView;

class OrderItemList extends Field implements IInlineEditor{

    protected $sFieldName = 'new_order_item_list';
    protected $sFieldLabel = 'Order item list';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getDataType():string
    {
        return 'other';
    }
    function getOverviewHeader()
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getOverviewValue($oProduct)
    {
        throw new LogicException('This field was only intended to be used in an editor view');
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof SaleOrder)
        {
            throw new InvalidArgumentException("Expected an instance of SaleOrder in ".__METHOD__);
        }
        $oSaleOrder = $mData;


        if($oSaleOrder->getId())
        {
            $oSaleOrderItemQuery = SaleOrderItemQuery::create();
            $oSaleOrderItemQuery->filterBySaleOrderId($oSaleOrder->getId());
        }

        if(!isset($oSaleOrderItemQuery) || !$oSaleOrderItemQuery instanceof SaleOrderItemQuery)
        {
            return Translate::fromCode('Deze bestelling is nog niet opgeslagen.');
        }
        else
        {
            $oCrudSaleOrderItemManager = new CrudSale_order_itemManager();
            $aCrudViews = CrudViewManager::getViews($oCrudSaleOrderItemManager);
            $oCrudview = current($aCrudViews);
            $sTabId = '';
            if($oCrudview instanceof CrudView)
            {
                $sTabId = '&tab='.$oCrudview->getId();
            }

            // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
            if($oSaleOrderItemQuery->count() === 0)
            {
                $sHtmlTable = '<p>'.Translate::fromCode('Deze order heeft nog geen producten.').'</p><!--aftercrud-->';
            }
            else
            {
                $aFields = CrudViewManager::getFields($oCrudview);
                $oCrudSaleOrderItemManager->setOverviewData($oSaleOrderItemQuery);

                $sHtmlTable = $oCrudSaleOrderItemManager->getOverviewHtml($aFields);
            }

            DeferredAction::register('return_order_edit', '/order/edit/edit?id=' . $mData->getId());
            /*
            $aEditLink[] = '    <div class="pull-left">';
            $aEditLink[] = '        <a href="/product/add_derrived?from_product_id='.$mData->getId().'">';
            $aEditLink[] = '            <i class="fa fa-plus"></i> Variatie toevoegen';
            $aEditLink[] = '        </a>';
            $aEditLink[] = '    </div>';
            */
            $aEditLink[] = '    <div class="pull-right">';
            $aEditLink[] = '        <a href="/generic/crud/create?module=Sale_order_item&manager=Sale_order_itemManager&r=return_product_edit'.$sTabId.'">';
            $aEditLink[] = '            configureer deze tabel <i class="fa fa-gear"></i>';
            $aEditLink[] = '        </a>';
            $aEditLink[] = '    </div>';
            $sEditLink = join(PHP_EOL, $aEditLink);

            $sHtmlTable = str_replace('<!--aftercrud-->', $sEditLink, $sHtmlTable);

            return $sHtmlTable;

        }

    }
}
