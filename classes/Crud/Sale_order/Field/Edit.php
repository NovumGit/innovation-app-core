<?php
namespace Crud\Sale_Order\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Sale\SaleOrder as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/order/summary?order_id='.$oModelObject->getId();
    }
}