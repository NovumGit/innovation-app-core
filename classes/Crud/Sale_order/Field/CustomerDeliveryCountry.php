<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_Order\Field;

use Crud\Field;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\Sale\SaleOrder as ModelObject;
use Model\Setting\MasterTable\Base\CountryQuery;

class CustomerDeliveryCountry extends Field implements IFilterableLookupField {

    protected $sFieldName = 'customer_delivery_country';
    protected $sFieldLabel = 'afl. Land';
    private $sIcon = 'map-marker';
    private $sPlaceHolder = '';
    private $sGetter = 'getCustomerDeliveryCountry';

    function getLookups($mSelectedItem = null)
    {
        $aCountries = CountryQuery::create()->orderByName()->find();
        $aOut = [];
        foreach($aCountries as $oCountry)
        {
            $aOut[] = ['label' => $oCountry->getName(), 'id' => $oCountry->getName(), 'selected' => ($mSelectedItem == $oCountry->getName()) ? 'selected' : ''];
        }
        return $aOut;
    }
    function getVisibleValue($iItemId)
    {
        // Landen namen worden in de sale_order tabel als tekst opslagen.
        return $iItemId;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sValue = $oModelObject->{$this->sGetter}();

        if($sValue == 'United States of America')
        {
            $sValue = $oModelObject->getCustomerDeliveryUsaState().', USA';
        }

        return '<td class="">'.$sValue.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getCustomerDeliveryCountry());

        array_unshift($aDropdownOptions, ['id' => null, 'selected' => '', 'label' => 'Maak een keuze']);

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getCustomerDeliveryCountry(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
