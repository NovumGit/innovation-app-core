<?php
namespace Crud\Sale_Order\Field;

use Core\Utils;
use Document\PackingSlip\PackingSlip;
use InvalidArgumentException;
use Crud\Field;
use Model\Sale\SaleOrder as ModelObject;
use Model\Setting\MasterTable\MasterPackingSlipQuery;

class PackingSlipType extends Field
{

    protected $sFieldLabel = 'Pakbon soort';
    protected $sFieldName = 'packing_slip_type';
    private $sPlaceHolder = 'Selecteer de pakbon soort';
    private $sIcon = 'tag';

    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }

    function hasValidations()
    {
        return false;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    function getOverviewHeader()
    {
        $aAddVars['tab'] = isset($_GET['tab']) ? $_GET['tab'] : null;
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName, $aAddVars);
    }

    function getOverviewValue($mData)
    {
        if (!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $mData->getPackingSlipType() . '</td>';
    }

    function getLookups($mSelectedItem = null)
    {

        $oMethodQuery = MasterPackingSlipQuery::create();
        $aAllMethods = $oMethodQuery->orderByName()->find();

        $oMasterPackingSlip = MasterPackingSlipQuery::create()->findOneByName($mSelectedItem);

        $iSlipId = null;

        if($oMasterPackingSlip)
        {
            $iSlipId = $oMasterPackingSlip->getId();
        }

        $aDropdownOptions = Utils::makeSelectOptions($aAllMethods, 'getName', $iSlipId);

        return $aDropdownOptions;
    }

    function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in " . __METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getPackingSlipType());

        $oMasterPackingSlipQuery = MasterPackingSlipQuery::create();
        $oPackingSlipType = $oMasterPackingSlipQuery->findOneByName($mData->getPackingSlipType());

        $iPackingSlipId = null;
        if($oPackingSlipType instanceof PackingSlip)
        {
            $iPackingSlipId = $oPackingSlipType->getId();
        }

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $iPackingSlipId, $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}