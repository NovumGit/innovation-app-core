<?php
namespace Crud\Sale_order;

use Core\Utils;
use LogicException;
use Core\User;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\MasterPackingSlip;
use Model\Setting\MasterTable\MasterPackingSlipQuery;

use Model\Account\User as UserModel;

class CrudSale_orderManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'product';
    }
    function getNewFormTitle():string{
        return 'Order toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Order bewerken';
    }
    function getOverviewUrl():string
    {
        return '/order/overview';
    }
    function getCreateNewUrl():string
    {
        return '/order/new';
    }

    function getDefaultOverviewFields(bool $addNs = false): ?array
    {
        return $this->getDefaultEditFields($addNs);
    }

    function getDefaultEditFields(bool $bAddNs = false):?array{

        $aOverviewColumns = [
            'CreatedOn',
            'CustomerId',
            'InvoiceNumber',
            'OrderNumber',
            'IsDeleted',
            'CompanyName',
            'Debitor',
            'OurPhone',
            'OurFax',
            'OurWebsite',
            'OurEmail',
            'OurGeneralCompanyName',
            'OurGeneralPostal',
            'OurGeneralCity',
            'CustomerOrderReference',
            'CustomerOrderPlacedBy',
            'CustomerInvoiceCompanyName',
            'CustomerInvoicePostal',
            'CustomerInvoiceCity',
            'CustomerDeliveryCompanyName',
            'CustomerDeliveryPostal',
            'CustomerDeliveryCity',
            'CustomerChamberOfCommerce',
            'CustomerVatNumber'
        ];
        if($bAddNs){
            array_walk($aOverviewColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }


    /**
     * @param $aData
     * @return SaleOrder
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SaleOrderQuery();

            $oSaleOrder = $oQuery->findOneById($aData['id']);

            if(!$oSaleOrder instanceof SaleOrder)
            {
                throw new LogicException("SaleOrder should be an instance of SaleOrder but got ".get_class($oSaleOrder)." in ".__METHOD__);
            }
        }
        else
        {
            $oSaleOrder = new SaleOrder();
            $oSaleOrder = $this->fillVo($oSaleOrder, $aData);
        }
        return $oSaleOrder;
    }

    function store(array $aData = null)
    {
        $oSaleOrder = $this->getModel($aData);

        if(!empty($oSaleOrder))
        {
            $oSaleOrder = $this->fillVo($oSaleOrder, $aData);
            $oSaleOrder->save();
        }

        $this->saveCustomFields($oSaleOrder, $aData);
        return $oSaleOrder;
    }
    function fillVo(SaleOrder $oSaleOrder, $aData)
    {

        if (isset($aData['created_on'])) { $oSaleOrder->setCreatedOn($aData['created_on']); }

        $oCurrentUser = User::getMember();
        if ($oCurrentUser instanceof UserModel)
        {
            $oSaleOrder->setCreatedByUserId($oCurrentUser->getId());
        }

        if (isset($aData['is_pushed_to_accounting'])) { $oSaleOrder->setIsPushedToAccounting($aData['is_pushed_to_accounting']);}
        if (isset($aData['is_closed'])) { $oSaleOrder->setIsClosed($aData['is_closed']);}
        if (isset($aData['tracking_url'])) { $oSaleOrder->setTrackingUrl($aData['tracking_url']);}
        if (isset($aData['tracking_code'])) { $oSaleOrder->setTrackingCode($aData['tracking_code']);}


        if (isset($aData['is_ready_for_shipping'])) { $oSaleOrder->setIsReadyForShipping($aData['is_ready_for_shipping']);}
        if (isset($aData['customer_delivery_address_l1'])) { $oSaleOrder->setCustomerDeliveryAddressL1($aData['customer_delivery_address_l1']);}
        if (isset($aData['customer_delivery_address_l2'])) { $oSaleOrder->setCustomerDeliveryAddressL2($aData['customer_delivery_address_l2']);}
        if (isset($aData['customer_invoice_address_l1'])) { $oSaleOrder->setCustomerInvoiceAddressL1($aData['customer_invoice_address_l1']);}
        if (isset($aData['customer_invoice_address_l2'])) { $oSaleOrder->setCustomerInvoiceAddressL2($aData['customer_invoice_address_l2']);}

        if (isset($aData['customer_id'])) { $oSaleOrder->setCustomerId($aData['customer_id']);}
        if (isset($aData['invoice_number'])) { $oSaleOrder->setInvoiceNumber($aData['invoice_number']);}
        if (isset($aData['order_number'])) { $oSaleOrder->setOrderNumber($aData['order_number']); }
        if (isset($aData['is_deleted'])) { $oSaleOrder->setItemDeleted($aData['is_deleted']); }
        if (isset($aData['is_fully_paid'])) { $oSaleOrder->setIsFullyPaid($aData['is_fully_paid']); }
        if (isset($aData['is_fully_shipped'])) { $oSaleOrder->setIsFullyShipped($aData['is_fully_shipped']); }
        if (isset($aData['company_name'])) { $oSaleOrder->setCompanyName($aData['company_name']);}

        if (isset($aData['debitor'])) { $oSaleOrder->setDebitor($aData['debitor']);}
        if (isset($aData['is_completed'])) {$oSaleOrder->setIsCompleted($aData['is_completed']);}
        if (isset($aData['shipping_method_id'])) {$oSaleOrder->setShippingMethodId($aData['shipping_method_id']);}

        if (isset($aData['customer_fax'])) { $oSaleOrder->setCustomerFax($aData['customer_fax']); }

        if (isset($aData['customer_email'])) { $oSaleOrder->setCustomerEmail($aData['customer_email']);}
        if (isset($aData['our_phone'])) { $oSaleOrder->setOurPhone($aData['our_phone']);
        }
        if (isset($aData['our_fax'])) { $oSaleOrder->setOurFax($aData['our_fax']); }
        if (isset($aData['our_website'])) {$oSaleOrder->setOurWebsite($aData['our_website']);}
        if (isset($aData['our_email'])) {$oSaleOrder->setOurEmail($aData['our_email']);}
        if (isset($aData['our_general_street'])) {$oSaleOrder->setOurGeneralStreet($aData['our_general_street']);}
        if (isset($aData['our_general_number'])) {$oSaleOrder->setOurGeneralNumber($aData['our_general_number']);}
        if (isset($aData['our_general_number_add'])) {$oSaleOrder->setOurGeneralNumberAdd($aData['our_general_number_add']);}
        if (isset($aData['our_general_postal'])) {
            $oSaleOrder->setOurGeneralPostal($aData['our_general_postal']);
        }
        if (isset($aData['our_general_city'])) {
            $oSaleOrder->setOurGeneralCity($aData['our_general_city']);
        }
        if (isset($aData['customer_order_reference'])) {
            $oSaleOrder->setCustomerOrderReference($aData['customer_order_reference']);
        }
        if (isset($aData['customer_order_placed_by'])) {
            $oSaleOrder->setCustomerOrderPlacedBy($aData['customer_order_placed_by']);
        }
        if (isset($aData['customer_invoice_company_name'])) {
            $oSaleOrder->setCustomerInvoiceCompanyName($aData['customer_invoice_company_name']);
        }
        if (isset($aData['customer_invoice_street'])) {
            $oSaleOrder->setCustomerInvoiceStreet($aData['customer_invoice_street']);
        }
        if (isset($aData['customer_invoice_number'])) {
            $oSaleOrder->setCustomerInvoiceNumber($aData['customer_invoice_number']);
        }
        if (isset($aData['customer_invoice_number_add'])) {
            $oSaleOrder->setCustomerInvoiceNumberAdd($aData['customer_invoice_number_add']);
        }
        if (isset($aData['customer_invoice_postal'])) {
            $oSaleOrder->setCustomerInvoicePostal($aData['customer_invoice_postal']);
        }
        if (isset($aData['customer_invoice_city'])) {
            $oSaleOrder->setCustomerInvoiceCity($aData['customer_invoice_city']);
        }
        if (isset($aData['customer_invoice_country'])) {
            $oSaleOrder->setCustomerInvoiceCountry($aData['customer_invoice_country']);
        }
        if (isset($aData['customer_delivery_company_name'])) {
            $oSaleOrder->setCustomerDeliveryCompanyName($aData['customer_delivery_company_name']);
        }
        if (isset($aData['customer_delivery_street'])) {
            $oSaleOrder->setCustomerDeliveryStreet($aData['customer_delivery_street']);
        }
        if (isset($aData['customer_delivery_number'])) {
            $oSaleOrder->setCustomerDeliveryNumber($aData['customer_delivery_number']);
        }
        if (isset($aData['customer_delivery_number_add'])) {
            $oSaleOrder->setCustomerDeliveryNumberAdd($aData['customer_delivery_number_add']);
        }
        if (isset($aData['customer_delivery_postal'])) {
            $oSaleOrder->setCustomerDeliveryPostal($aData['customer_delivery_postal']);
        }
        if (isset($aData['customer_delivery_city'])) {
            $oSaleOrder->setCustomerDeliveryCity($aData['customer_delivery_city']);
        }
        if (isset($aData['customer_delivery_country'])) {
            $oSaleOrder->setCustomerDeliveryCountry($aData['customer_delivery_country']);
        }
        if (isset($aData['customer_chamber_of_commerce'])) {
            $oSaleOrder->setCustomerChamberOfCommerce($aData['customer_chamber_of_commerce']);
        }
        if (isset($aData['customer_vat_number'])) {
            $oSaleOrder->setCustomerVatNumber($aData['customer_vat_number']);
        }
        if (isset($aData['customer_phone'])) {
            $oSaleOrder->setCustomerPhone($aData['customer_phone']);
        }
        if (isset($aData['cp_first_name'])) {
            $oSaleOrder->setCpFirstName($aData['cp_first_name']);
        }
        if (isset($aData['cp_last_name'])) {
            $oSaleOrder->setCpLastName($aData['cp_last_name']);
        }
        if (isset($aData['cp_sallutation'])) {
            $oSaleOrder->setCpSallutation($aData['cp_sallutation']);
        }
        if (isset($aData['email'])) {
            $oSaleOrder->setCustomerEmail($aData['email']);
        }
        if (isset($aData['reference'])) {
            $oSaleOrder->setReference($aData['reference']);
        }
        if (isset($aData['customer_order_note'])) {
            $oSaleOrder->setCustomerOrderNote($aData['customer_order_note']);
        }
        if (isset($aData['shipping_order_note'])) {
            $oSaleOrder->setShippingOrderNote($aData['shipping_order_note']);
        }
        if (isset($aData['shipping_carrier'])) {
            $oSaleOrder->setShippingCarrier($aData['shipping_carrier']);
        }
        if (isset($aData['shipping_price'])) {
            $fShippingPrice = str_replace(',', '.', $aData['shipping_price']);
            $oSaleOrder->setShippingPrice($fShippingPrice);
        }
        if (isset($aData['sale_order_status_id'])) {
            $oSaleOrder->setSaleOrderStatusId($aData['sale_order_status_id']);
        }
        if (isset($aData['packing_slip_type']))
        {
            $oMasterPackingSlip = MasterPackingSlipQuery::create()->findOneById($aData['packing_slip_type']);
            if ($oMasterPackingSlip instanceof MasterPackingSlip) {
                $oSaleOrder->setPackingSlipType($oMasterPackingSlip->getName());
            }
        }
        if(isset($aData['estimated_delivery_date']))
        {
            $iTime = strtotime($aData['estimated_delivery_date']);
            $oSaleOrder->setEstimatedDeliveryDate($iTime);
        }
        if (isset($aData['package_amount'])) {
            $oSaleOrder->setPackageAmount($aData['package_amount']);
        }

        return $oSaleOrder;
    }
}
