<?php
namespace Crud\Sale_order;

class CrudSale_order_summaryManager extends CrudSale_orderManager
{

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'SaleOrderStatusId',
            'ShippingCarrier',
            'ShippingPrice',
            'PackingSlipType'
        ];
    }

}
