<?php

namespace Crud;

/**
 * Class CrudFieldFilter
 * @package Crud
 */
class CrudFieldFilter
{
    private $aEditableFields = [];
    private $aEventFields = [];
    private $aFilterableLookupFields = [];
    private $aFilterableFields = [];
    private $aValidatedFields = [];
    private $aRequiredFields = [];

    function __construct(IFormManager $oCrudManager)
    {
        $aAllFields = $oCrudManager->getFieldIterator();

        foreach ($aAllFields as $oField)
        {
            if ($oField instanceof IEditableField)
            {
                $this->aEditableFields[] = $oField;
            }

            if ($oField->hasValidations())
            {
                $this->aValidatedFields[] = $oField;
            }

            if ($oField instanceof IEventField)
            {
                $this->aEventFields[] = $oField;
            }

            if ($oField instanceof IFilterableLookupField)
            {
                $this->aFilterableLookupFields[] = $oField;
            }

            if ($oField instanceof IRequiredField)
            {
                $this->aRequiredFields[] = $oField;
            }

            if ($oField instanceof IFilterableField)
            {
                $this->aFilterableFields[] = $oField;
            }
        }
    }

    /**
     * @return Field[]
     */
    public function getValidatedFields()
    {
        return $this->aValidatedFields;
    }

    /**
     * @return IRequiredField[]
     */
    public function getRequiredFields()
    {
        return $this->aRequiredFields;
    }

    /**
     * @return IEditableField[]
     */
    public function getEditableFields()
    {
        return $this->aEditableFields;
    }

    /**
     * @return IEventField[]
     */
    public function getEventFields(): array
    {
        return $this->aEventFields;
    }

    /**
     * @return IFilterableLookupField[]
     */
    public function getFilterableLookupFields(): array
    {
        return $this->aFilterableLookupFields;
    }

    /**
     * @return IFilterableField[]
     */
    public function getFilterableFields(): array
    {
        return $this->aFilterableFields;
    }

}
