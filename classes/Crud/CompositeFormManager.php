<?php
namespace Crud;
use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\Style1\Style1Helper;
use Core\DeferredAction;
use Core\Paginate;
use Core\Translate;
use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Helper\FilterHelper;
use Model\Setting\CrudManager\Base\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditorBlock;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\CrudEditorBlockQuery;
use Core\StatusMessage;
use Model\Setting\CrudManager\CrudView;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use \ReflectionClass;
use Exception\LogicException;

abstract class CompositeFormManager implements IFormManager
{
   // @todo needs implementation
}
