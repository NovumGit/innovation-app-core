<?php
namespace Crud\Contact_message_type;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessageType;
use Model\ContactMessageTypeQuery;

class CrudContact_message_typeManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'bericht type';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/contact_message_type/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/contact_message_type/edit';
    }
    function getNewFormTitle():string{
        return 'Contact bericht type toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Contact bericht type wijzigen';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name',
        ];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oContactMessageTypeQuery = ContactMessageTypeQuery::create();
            $oContactMessageType = $oContactMessageTypeQuery->findOneById($aData['id']);

            if(!$oContactMessageType instanceof ContactMessageType)
            {
                throw new LogicException("ContactMessageType should be an instance of ContactMessageType but got ".get_class($oContactMessageType)." in ".__METHOD__);
            }
        }
        else
        {
            $oContactMessageType = new ContactMessageType();
            if(!empty($aData))
            {
                $oContactMessageType = $this->fillVo($aData, $oContactMessageType);
            }
        }
        return $oContactMessageType;
    }
    function store(array $aData = null)
    {
        $oContactMessageType = $this->getModel($aData);
        if(!empty($oContactMessageType))
        {
            $oContactMessageType = $this->fillVo($aData, $oContactMessageType);
            $oContactMessageType->save();
        }
        return $oContactMessageType;
    }
    private function fillVo($aData, ContactMessageType $oContactMessageType)
    {
        if(isset($aData['name']))
        {
            $oContactMessageType->setName($aData['name']);
        }
        return $oContactMessageType;
    }
}
