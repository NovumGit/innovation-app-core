<?php
namespace Crud\Contact_message_type\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\ContactMessageType;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oContactMessageType){

        if(!$oContactMessageType instanceof ContactMessageType)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessageType but got '.get_class($oContactMessageType));
        }

        return '/setting/mastertable/contact_message_type/edit?_do=Delete&id='.$oContactMessageType->getId();
    }

    function getUnDeleteUrl($oContactMessageType)
    {
        throw new LogicException("Product delivery time cannot be undeleted");
    }
}