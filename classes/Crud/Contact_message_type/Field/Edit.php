<?php
namespace Crud\Contact_message_type\Field;

use Crud\Generic\Field\GenericEdit;
use Model\ContactMessageType;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit
{
    function getEditUrl($oContactMessageType)
    {
        if(!$oContactMessageType instanceof ContactMessageType)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessageType but got '.get_class($oContactMessageType));
        }
        return '/setting/mastertable/contact_message_type/edit?id='.$oContactMessageType->getId();
    }
}