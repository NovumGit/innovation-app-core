<?php
namespace Crud;

abstract class Link_blockManager extends BaseManager
{

    function getLinkArray($sLayoutKey)
    {
        $aFieldsNames = $this->getConfiguredEditFields($sLayoutKey);

        if(empty($aFieldsNames))
        {
            $aFieldsNames = $this->getDefaultEditFields( $bAddNamespaceUnimplemented = false);
        }


        if(empty($aFieldsNames))
        {
            return null;
        }
        $aFields = $this->loadCrudFields($aFieldsNames);
        return $aFields;
    }
}
