<?php
namespace Crud\SystemRegistry\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SystemRegistry\FieldIterator;
use Crud\SystemRegistry\Field\ItemKey;
use Crud\SystemRegistry\Field\ItemValue;
use Exception\LogicException;
use Model\System\Map\SystemRegistryTableMap;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SystemRegistry instead if you need to override or add functionality.
 */
abstract class CrudSystemRegistryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SystemRegistryQuery::create();
	}


	public function getTableMap(): SystemRegistryTableMap
	{
		return new SystemRegistryTableMap();
	}


	public function getShortDescription(): string
	{
		return "A key / value registry that is used for system wide storing settings and preferences.";
	}


	public function getEntityTitle(): string
	{
		return "SystemRegistry";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "system_registry toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "system_registry aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SystemRegistry
	 */
	public function getModel(array $aData = null): SystemRegistry
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSystemRegistryQuery = SystemRegistryQuery::create();
		     $oSystemRegistry = $oSystemRegistryQuery->findOneById($aData['id']);
		     if (!$oSystemRegistry instanceof SystemRegistry) {
		         throw new LogicException("SystemRegistry should be an instance of SystemRegistry but got something else." . __METHOD__);
		     }
		     $oSystemRegistry = $this->fillVo($aData, $oSystemRegistry);
		}
		else {
		     $oSystemRegistry = new SystemRegistry();
		     if (!empty($aData)) {
		         $oSystemRegistry = $this->fillVo($aData, $oSystemRegistry);
		     }
		}
		return $oSystemRegistry;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SystemRegistry
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SystemRegistry
	{
		$oSystemRegistry = $this->getModel($aData);


		 if(!empty($oSystemRegistry))
		 {
		     $oSystemRegistry = $this->fillVo($aData, $oSystemRegistry);
		     $oSystemRegistry->save();
		 }
		return $oSystemRegistry;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SystemRegistry $oModel
	 * @return SystemRegistry
	 */
	protected function fillVo(array $aData, SystemRegistry $oModel): SystemRegistry
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_value'])) {
		     $oField = new ItemValue();
		     $mValue = $oField->sanitize($aData['item_value']);
		     $oModel->setItemValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
