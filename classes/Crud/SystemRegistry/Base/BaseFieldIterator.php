<?php
namespace Crud\SystemRegistry\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SystemRegistry\ICollectionField as SystemRegistryField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SystemRegistry\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SystemRegistryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SystemRegistryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SystemRegistryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SystemRegistryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SystemRegistryField
	{
		return current($this->aFields);
	}
}
