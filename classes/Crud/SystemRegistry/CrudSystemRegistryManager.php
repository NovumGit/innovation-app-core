<?php
namespace Crud\SystemRegistry;

use Core\Setting;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;

/**
 * Skeleton subclass for representing a SystemRegistry.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudSystemRegistryManager extends Base\CrudSystemRegistryManager
{
    function store(array $aData = null): SystemRegistry
    {

        Setting::store($aData['item_key'], $aData['item_value']);
        return SystemRegistryQuery::create()->findOneByItemKey($aData['item_key']);

    }
}
