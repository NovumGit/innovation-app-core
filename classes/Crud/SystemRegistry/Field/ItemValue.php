<?php
namespace Crud\SystemRegistry\Field;

use Crud\SystemRegistry\Field\Base\ItemValue as BaseItemValue;

/**
 * Skeleton subclass for representing item_value field from the system_registry table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ItemValue extends BaseItemValue
{
}
