<?php
namespace Crud\SystemRegistry\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SystemRegistry\ICollectionField;

/**
 * Base class that represents the 'item_value' crud field from the 'system_registry' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemValue extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_value';
	protected $sFieldLabel = 'Attribute value';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemValue';
	protected $sFqModelClassname = '\\\Model\System\SystemRegistry';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
