<?php 
namespace Crud\SystemRegistry\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\SystemRegistry;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof SystemRegistry)
		{
		     return "//system/system_registry/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof SystemRegistry)
		{
		     return "//system_registry?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
