<?php

namespace Crud\PayTerm\Field;

use Crud\PayTerm\Field\Base\TotalDays as BaseTotalDays;

/**
 * Skeleton subclass for representing total_days field from the mt_payterm table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class TotalDays extends BaseTotalDays
{
}
