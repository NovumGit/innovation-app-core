<?php
namespace Crud\PayTerm\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\PayTerm\ICollectionField;

/**
 * Base class that represents the 'code' crud field from the 'mt_payterm' table.
 * This class is auto generated and should not be modified.
 */
abstract class Code extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'code';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCode';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\PayTerm';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
