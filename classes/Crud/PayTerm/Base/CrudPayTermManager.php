<?php
namespace Crud\PayTerm\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PayTerm\FieldIterator;
use Crud\PayTerm\Field\Code;
use Crud\PayTerm\Field\IsDefault;
use Crud\PayTerm\Field\Name;
use Crud\PayTerm\Field\Period;
use Crud\PayTerm\Field\TotalDays;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\PayTermTableMap;
use Model\Setting\MasterTable\PayTerm;
use Model\Setting\MasterTable\PayTermQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PayTerm instead if you need to override or add functionality.
 */
abstract class CrudPayTermManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PayTermQuery::create();
	}


	public function getTableMap(): PayTermTableMap
	{
		return new PayTermTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PayTerm";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_payterm toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_payterm aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'TotalDays', 'Period', 'IsDefault'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'TotalDays', 'Period', 'IsDefault'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PayTerm
	 */
	public function getModel(array $aData = null): PayTerm
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPayTermQuery = PayTermQuery::create();
		     $oPayTerm = $oPayTermQuery->findOneById($aData['id']);
		     if (!$oPayTerm instanceof PayTerm) {
		         throw new LogicException("PayTerm should be an instance of PayTerm but got something else." . __METHOD__);
		     }
		     $oPayTerm = $this->fillVo($aData, $oPayTerm);
		}
		else {
		     $oPayTerm = new PayTerm();
		     if (!empty($aData)) {
		         $oPayTerm = $this->fillVo($aData, $oPayTerm);
		     }
		}
		return $oPayTerm;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PayTerm
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PayTerm
	{
		$oPayTerm = $this->getModel($aData);


		 if(!empty($oPayTerm))
		 {
		     $oPayTerm = $this->fillVo($aData, $oPayTerm);
		     $oPayTerm->save();
		 }
		return $oPayTerm;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PayTerm $oModel
	 * @return PayTerm
	 */
	protected function fillVo(array $aData, PayTerm $oModel): PayTerm
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['total_days'])) {
		     $oField = new TotalDays();
		     $mValue = $oField->sanitize($aData['total_days']);
		     $oModel->setTotalDays($mValue);
		}
		if(isset($aData['period'])) {
		     $oField = new Period();
		     $mValue = $oField->sanitize($aData['period']);
		     $oModel->setPeriod($mValue);
		}
		if(isset($aData['is_default'])) {
		     $oField = new IsDefault();
		     $mValue = $oField->sanitize($aData['is_default']);
		     $oModel->setIsDefault($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
