<?php
namespace Crud\Vat_translation\Field;

use Crud\Field;
use InvalidArgumentException;
use Model\Setting\MasterTable\VatTranslation;
use Model\Setting\MasterTable\VatTranslationQuery;

class Name extends Field
{
    protected $sFieldLabel = 'BTW groep titel';
    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($aPostedData['id']))
        {
            $oVat = VatQuery::create()->findOneByName($aPostedData['name']);
            if($oVat instanceof Vat)
            {
                $mResponse[] = 'Er is al een btw groep in het systeem aanwezig met deze naam.';
            }

        }


        if(strlen($aPostedData['name']) < 2 || strlen($aPostedData['name']) > 20)
        {
            $mResponse[] = 'De naam van de btw groep moet minimaal 2 en mag maximaal 20 tekens lang zijn, is nu '.strlen($aPostedData['name']).' tekens lang.';
        }

        return $mResponse;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('BTW groep titel', 'name');
    }
    function getOverviewValue($oVatTranslation)
    {
        if(!$oVatTranslation instanceof VatTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of VatTranslation in ".__METHOD__);
        }

        return '<td class="">'.$oVat->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof VatTranslation)
        {
            throw new InvalidArgumentException("Expected an instance of VatTranslation in ".__METHOD__);
        }

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="user_email" class="col-lg-3 control-label">BTW groep titel</label>';
        $aOut[] = '    <div class="col-lg-8">';
        $aOut[] = '            <div class="input-group">';
        $aOut[] = '            <span class="input-group-addon">';
        $aOut[] = '                <i class="fa fa-tags"></i>';
        $aOut[] = '            </span>';
        $aOut[] = '            <input name="data[name]" id="name" class="form-control" value="'.$mData->getName().'" placeholder="Voorbeeld: Hoog 21%" type="text">';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        return join(PHP_EOL, $aOut);
    }
}