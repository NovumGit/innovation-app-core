<?php
namespace Crud\Vat_translation;

use Core\Utils;
use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\VatTranslation;
use Model\Setting\MasterTable\VatTranslationQuery;

class CrudVat_translationManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/vat/edit';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/vat/overview';
    }

    function getEntityTitle():string
    {
        return 'btw';
    }
    function getNewFormTitle():string{
        return 'Btw tarief toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'BTW tarief wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNs  = false):?array
    {
        $aColumns = [
            'Name',
        ];
        if($bAddNs){
            array_walk($aColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }
    function getDefaultEditFields(bool $bAddNs  = false):?array{

        $aColumns = [
            'Name'
        ];

        if($bAddNs){
            array_walk($aColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }

    function getModel(array $aData = null)
    {
        $oVatTranslation = null;

        if(isset($aData['id']) && $aData['id'])
        {
            $oVatTranslation = VatTranslationQuery::create()->findOneById($aData['id']);
        }
        if(!$oVatTranslation instanceof VatTranslation && isset($aData['language_id']) && isset($aData['vat_id'])){

            $oVatTranslationQuery = VatTranslationQuery::create();
            $oVatTranslationQuery->filterByLanguageId($aData['language_id']);
            $oVatTranslationQuery->filterByVatId($aData['vat_id']);
            $oVatTranslation = $oVatTranslationQuery->findOne();
        }

        if(!$oVatTranslation instanceof VatTranslation)
        {
            $oVatTranslation = new VatTranslation();
        }

        if(!empty($aData))
        {
            $oVatTranslation = $this->fillVo($oVatTranslation, $aData);
        }
        return $oVatTranslation;
    }

    private function fillVo(VatTranslation $oVatTranslation, $aData)
    {
        if(isset($aData['name'])){
            $oVatTranslation->setName($aData['name']);
        }
        if(isset($aData['language_id'])){
            $oVatTranslation->setLanguageId($aData['language_id']);
        }
        if(isset($aData['vat_id'])){
            $oVatTranslation->setVatId($aData['vat_id']);
        }

        return $oVatTranslation;
    }
    function store(array $aData = null)
    {
        $oVat = $this->getModel($aData);
        if(!empty($oVat))
        {
            $oVat = $this->fillVo(new VatTranslation(), $aData);
            $oVat->save();
        }
        return $oVat;
    }
}
