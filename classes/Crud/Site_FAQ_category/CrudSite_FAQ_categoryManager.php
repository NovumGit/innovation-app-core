<?php
namespace Crud\Site_FAQ_category;

use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Core\Config;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteFAQCategory;
use Model\Cms\SiteFAQCategoryQuery;
use Model\Cms\SiteQuery;

class CrudSite_FAQ_categoryManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'site_faq_category';
    }
    function getNewFormTitle():string
    {
        return 'FAQ categorie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'FAQ categorie bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/setting/faq_category/overview?site='.$_GET['site'];
        }
        throw new LogicException("The FAQ pages require a domainname / GET[site] var.");
    }
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/setting/faq_category/edit?site='.$_GET['site'];
        }
        throw new LogicException("The FAQ pages require a domainname / GET[site] var.");
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name'
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SiteFAQCategoryQuery();
            $oSiteFAQCategory = $oQuery->findOneById($aData['id']);
            if(!$oSiteFAQCategory instanceof SiteFAQCategory)
            {
                throw new LogicException("SiteFAQCategory should be an instance of Site FAQ category but got ".get_class($oSiteFAQCategory)." in ".__METHOD__);
            }
        }
        else
        {
            $oSiteFAQCategory = new SiteFAQCategory();
            $oSiteFAQCategory = $this->fillVo($oSiteFAQCategory, $aData);
        }
        return $oSiteFAQCategory;
    }
    function store(array $aData = null)
    {
        $oSiteFAQCategory = $this->getModel($aData);
        if(!empty($oSiteFAQCategory))
        {
            $oSiteFAQCategory = $this->fillVo($oSiteFAQCategory, $aData);
            $oSiteFAQCategory->save();
            if( isset($_FILES['data']['name']) && ($_FILES['data']['size']['image_file'] > 0) && ($_FILES['data']['error']['image_file'] === 0)) {

                $aMimeTypes = Utils::getSupportedImageMimeTypes();
                if(!in_array($_FILES['data']['type']['image_file'], $aMimeTypes))
                {
                    StatusMessage::warning(Translate::fromCode("Het geuploadde bestand wordt niet ondersteund."));
                }
                else
                {
                    $sFileExt = pathinfo($_FILES['data']['name']['image_file'], PATHINFO_EXTENSION);
                    $oSiteFAQCategory->setImageExt($sFileExt);
                    $oSiteFAQCategory->setHasImage(true);
                    $oSiteFAQCategory->save();
                    $sNewLocation = $oSiteFAQCategory->getImageFileLocation();
                    $sDir = dirname($oSiteFAQCategory->getImageFileLocation());

                    if(!is_dir($sDir))
                    {
                        mkdir($sDir, 0777, true);
                    }
                    if(!is_writable($sDir))
                    {
                        throw new LogicException("The folder $sDir is not writable.");
                    }
                    move_uploaded_file($_FILES['data']['tmp_name']['image_file'], $sNewLocation);
                }
            }
        }
        return $oSiteFAQCategory;
    }
    function delete($iSiteFAQCategoryId)
    {
        $oSiteFAQCategory = SiteFAQCategoryQuery::create()->findOneById($iSiteFAQCategoryId);
        $oSiteFAQCategory->delete();
        StatusMessage::success("FAQ categorie verwijderd.");
    }
    function fillVo(SiteFAQCategory $oSiteFAQCategory, $aData)
    {
        if(!isset($_GET['site']))
        {
            throw new LogicException("The system needs a site domain as GET variable in order to function.");
        }
        $sSite = $_GET['site'];
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("Could not find the site associated with the domainname $sSite.");
        }
        $oSiteFAQCategory->setSiteId($oSite->getId());

        if(isset($aData['name']))
        {
            $oSiteFAQCategory->setName($aData['name']);
        }
        if(isset($aData['has_image']))
        {
            $oSiteFAQCategory->setHasImage($aData['has_image']);
        }
        if(isset($aData['image_ext']))
        {
            $oSiteFAQCategory->setImageExt($aData['image_ext']);
        }
        return $oSiteFAQCategory;
    }
}
