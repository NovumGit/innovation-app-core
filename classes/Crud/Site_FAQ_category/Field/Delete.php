<?php
namespace Crud\Site_FAQ_category\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Cms\SiteFAQCategory as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }
        return '/setting/faq_category/edit?site='.$this->getArgument('site').'&id='.$oModelObject->getId().'&_do=Delete';
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}