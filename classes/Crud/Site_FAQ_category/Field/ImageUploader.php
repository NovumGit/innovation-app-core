<?php
namespace Crud\Site_FAQ_category\Field;
use Crud\Field;
use Exception\InvalidArgumentException;
use Model\Cms\SiteFAQCategory as ModelObject;


class ImageUploader extends Field{

    protected $sFieldName = '';
    protected $sFieldLabel = 'Afbeelding uploaden';
    private $sIcon = 'photo';
    private $sPlaceHolder = '';
    private $sGetter = '';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function isOverviewField(): bool
    {
        return false;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editFileField($this->getTranslatedTitle(), 'image_file', '', $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
