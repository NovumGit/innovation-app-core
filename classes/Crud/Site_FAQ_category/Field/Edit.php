<?php
namespace Crud\Site_FAQ_category\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Cms\SiteFAQCategory as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/setting/faq_category/edit?site='.$this->getArgument('site').'&id='.$oModelObject->getId();
    }
}