<?php
namespace Crud\Site_FAQ_category\Field;

use Core\Translate;
use Crud\ConfigEditPreviewImage;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Exception\LogicException;
use Model\Cms\SiteFAQCategory as ModelObject;

class ImagePreview extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'image_preview';
    protected $sFieldLabel = 'Afbeelding voorbeeld';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function isOverviewField(): bool
    {
        return false;
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        throw new LogicException("This field should never arrive in any overview.");
    }
    function getOverviewValue($oModelObject)
    {
        throw new LogicException("This field should never arrive in any overview.");
    }
    function getFieldTitle()
    {
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!isset($_GET['site']))
        {
            throw new LogicException("This feature depends on a site variable in the url, not having this might indicate a bug. This issue is logged");
        }
        $sSite = $_GET['site'];
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $oEditPreviewImageConfig = new ConfigEditPreviewImage();
        if($oModelObject->getId() && $oModelObject->getHasImage())
        {
            $sDeleteUrl = "/setting/faq_category/edit?site=$sSite&id=".$oModelObject->getId()."&_do=DeleteImage";
            $oEditPreviewImageConfig->setDeleteUrl($sDeleteUrl);
        }
        if($oModelObject->getHasImage())
        {
            $oEditPreviewImageConfig->setImageUrlOrPath($oModelObject->getImagePath());
        }
        else
        {
            $oEditPreviewImageConfig->setImageUrlOrPath('/img/no-image-available.svg');
        }

        $oEditPreviewImageConfig->setWidth(200);
        $oEditPreviewImageConfig->setTitle(Translate::fromCode('Voorbeeld'));
        $oEditPreviewImageConfig->setAlt(Translate::fromCode('Voorbeeld'));

        return $this->editImagePreview($oEditPreviewImageConfig);
    }
}
