<?php
namespace Crud\SitePagePicture\Field;

use Crud\SitePagePicture\Field\Base\Sorting as BaseSorting;

/**
 * Skeleton subclass for representing sorting field from the site_page_picture table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Sorting extends BaseSorting
{
}
