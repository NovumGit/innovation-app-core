<?php
namespace Crud\SitePagePicture\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SitePagePicture\ICollectionField;

/**
 * Base class that represents the 'original_name' crud field from the 'site_page_picture' table.
 * This class is auto generated and should not be modified.
 */
abstract class OriginalName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'original_name';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getOriginalName';
	protected $sFqModelClassname = '\\\Model\Cms\SitePagePicture';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
