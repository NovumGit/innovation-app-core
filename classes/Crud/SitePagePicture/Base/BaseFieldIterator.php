<?php
namespace Crud\SitePagePicture\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SitePagePicture\ICollectionField as SitePagePictureField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SitePagePicture\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SitePagePictureField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SitePagePictureField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SitePagePictureField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SitePagePictureField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SitePagePictureField
	{
		return current($this->aFields);
	}
}
