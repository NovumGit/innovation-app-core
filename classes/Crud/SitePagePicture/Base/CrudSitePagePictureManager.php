<?php
namespace Crud\SitePagePicture\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SitePagePicture\FieldIterator;
use Crud\SitePagePicture\Field\FileExt;
use Crud\SitePagePicture\Field\FileMime;
use Crud\SitePagePicture\Field\FileSize;
use Crud\SitePagePicture\Field\OldId;
use Crud\SitePagePicture\Field\OriginalName;
use Crud\SitePagePicture\Field\SitePageId;
use Crud\SitePagePicture\Field\Sorting;
use Exception\LogicException;
use Model\Cms\Map\SitePagePictureTableMap;
use Model\Cms\SitePagePicture;
use Model\Cms\SitePagePictureQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SitePagePicture instead if you need to override or add functionality.
 */
abstract class CrudSitePagePictureManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SitePagePictureQuery::create();
	}


	public function getTableMap(): SitePagePictureTableMap
	{
		return new SitePagePictureTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SitePagePicture";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_page_picture toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_page_picture aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'SitePageId', 'OriginalName', 'FileExt', 'FileSize', 'FileMime', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OldId', 'SitePageId', 'OriginalName', 'FileExt', 'FileSize', 'FileMime', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SitePagePicture
	 */
	public function getModel(array $aData = null): SitePagePicture
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSitePagePictureQuery = SitePagePictureQuery::create();
		     $oSitePagePicture = $oSitePagePictureQuery->findOneById($aData['id']);
		     if (!$oSitePagePicture instanceof SitePagePicture) {
		         throw new LogicException("SitePagePicture should be an instance of SitePagePicture but got something else." . __METHOD__);
		     }
		     $oSitePagePicture = $this->fillVo($aData, $oSitePagePicture);
		}
		else {
		     $oSitePagePicture = new SitePagePicture();
		     if (!empty($aData)) {
		         $oSitePagePicture = $this->fillVo($aData, $oSitePagePicture);
		     }
		}
		return $oSitePagePicture;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SitePagePicture
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SitePagePicture
	{
		$oSitePagePicture = $this->getModel($aData);


		 if(!empty($oSitePagePicture))
		 {
		     $oSitePagePicture = $this->fillVo($aData, $oSitePagePicture);
		     $oSitePagePicture->save();
		 }
		return $oSitePagePicture;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SitePagePicture $oModel
	 * @return SitePagePicture
	 */
	protected function fillVo(array $aData, SitePagePicture $oModel): SitePagePicture
	{
		if(isset($aData['old_id'])) {
		     $oField = new OldId();
		     $mValue = $oField->sanitize($aData['old_id']);
		     $oModel->setOldId($mValue);
		}
		if(isset($aData['site_page_id'])) {
		     $oField = new SitePageId();
		     $mValue = $oField->sanitize($aData['site_page_id']);
		     $oModel->setSitePageId($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		if(isset($aData['file_size'])) {
		     $oField = new FileSize();
		     $mValue = $oField->sanitize($aData['file_size']);
		     $oModel->setFileSize($mValue);
		}
		if(isset($aData['file_mime'])) {
		     $oField = new FileMime();
		     $mValue = $oField->sanitize($aData['file_mime']);
		     $oModel->setFileMime($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
