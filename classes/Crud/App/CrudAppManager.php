<?php

namespace Crud\App;

use Core\Utils;

/**
 * Skeleton subclass for representing a App.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudAppManager extends Base\CrudAppManager
{

    public function getDefaultOverviewFields(bool $bAddNs = false): array
    {
        $aOverviewColumns = ['Name', 'AppTypeId', 'AppDescription', 'AppAudience', 'AppIcon', 'Design', 'Edit', 'Delete'];
        if ($bAddNs) {
            array_walk($aOverviewColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }
}
