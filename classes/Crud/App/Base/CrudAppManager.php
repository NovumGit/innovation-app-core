<?php
namespace Crud\App\Base;

use Core\Utils;
use Crud;
use Crud\App\FieldIterator;
use Crud\App\Field\AppAudience;
use Crud\App\Field\AppDescription;
use Crud\App\Field\AppIcon;
use Crud\App\Field\AppTypeId;
use Crud\App\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\App;
use Model\System\AppQuery;
use Model\System\Map\AppTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify App instead if you need to override or add functionality.
 */
abstract class CrudAppManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return AppQuery::create();
	}


	public function getTableMap(): AppTableMap
	{
		return new AppTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat alle apps / formulieren.";
	}


	public function getEntityTitle(): string
	{
		return "App";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "app toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "app aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'AppTypeId', 'AppDescription', 'AppAudience', 'AppIcon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'AppTypeId', 'AppDescription', 'AppAudience', 'AppIcon'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return App
	 */
	public function getModel(array $aData = null): App
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oAppQuery = AppQuery::create();
		     $oApp = $oAppQuery->findOneById($aData['id']);
		     if (!$oApp instanceof App) {
		         throw new LogicException("App should be an instance of App but got something else." . __METHOD__);
		     }
		     $oApp = $this->fillVo($aData, $oApp);
		}
		else {
		     $oApp = new App();
		     if (!empty($aData)) {
		         $oApp = $this->fillVo($aData, $oApp);
		     }
		}
		return $oApp;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return App
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): App
	{
		$oApp = $this->getModel($aData);


		 if(!empty($oApp))
		 {
		     $oApp = $this->fillVo($aData, $oApp);
		     $oApp->save();
		 }
		return $oApp;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param App $oModel
	 * @return App
	 */
	protected function fillVo(array $aData, App $oModel): App
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['app_type_id'])) {
		     $oField = new AppTypeId();
		     $mValue = $oField->sanitize($aData['app_type_id']);
		     $oModel->setAppTypeId($mValue);
		}
		if(isset($aData['app_description'])) {
		     $oField = new AppDescription();
		     $mValue = $oField->sanitize($aData['app_description']);
		     $oModel->setAppDescription($mValue);
		}
		if(isset($aData['app_audience'])) {
		     $oField = new AppAudience();
		     $mValue = $oField->sanitize($aData['app_audience']);
		     $oModel->setAppAudience($mValue);
		}
		if(isset($aData['app_icon'])) {
		     $oField = new AppIcon();
		     $mValue = $oField->sanitize($aData['app_icon']);
		     $oModel->setAppIcon($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
