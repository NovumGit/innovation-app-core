<?php
namespace Crud\App\Base;

use Crud\App\ICollectionField as AppField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\App\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param AppField[] $aFields */
	private array $aFields = [];


	/**
	 * @param AppField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof AppField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(AppField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): AppField
	{
		return current($this->aFields);
	}
}
