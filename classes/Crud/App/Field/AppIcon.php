<?php

namespace Crud\App\Field;

use Crud\App\Field\Base\AppIcon as BaseAppIcon;

/**
 * Skeleton subclass for representing app_icon field from the app table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class AppIcon extends BaseAppIcon
{
}
