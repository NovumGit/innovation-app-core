<?php
namespace Crud\App\Field\Base;

use Crud\App\ICollectionField;
use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'app_audience' crud field from the 'app' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppAudience extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'app_audience';
	protected $sFieldLabel = 'Voor wie is de app bedoeld';
	protected $sIcon = 'comment-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppAudience';
	protected $sFqModelClassname = '\\\Model\System\App';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
