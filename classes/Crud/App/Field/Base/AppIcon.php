<?php
namespace Crud\App\Field\Base;

use Crud\App\ICollectionField;
use Crud\Generic\Field\GenericFile;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'app_icon' crud field from the 'app' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppIcon extends GenericFile implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'app_icon';
	protected $sFieldLabel = 'Upoad een Icoon';
	protected $sIcon = 'file-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppIcon';
	protected $sFqModelClassname = '\\\Model\System\App';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
