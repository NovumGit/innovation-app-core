<?php
namespace Crud\App\Field\Base;

use Core\Utils;
use Crud\App\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\AppTypeQuery;

/**
 * Base class that represents the 'app_type_id' crud field from the 'app' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppTypeId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'app_type_id';
	protected $sFieldLabel = 'Soort app';
	protected $sIcon = 'mobile';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppTypeId';
	protected $sFqModelClassname = '\\\Model\System\App';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\AppTypeQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\AppTypeQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['app_type_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Soort app" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
