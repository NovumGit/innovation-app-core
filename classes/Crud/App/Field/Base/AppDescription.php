<?php
namespace Crud\App\Field\Base;

use Crud\App\ICollectionField;
use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'app_description' crud field from the 'app' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppDescription extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'app_description';
	protected $sFieldLabel = 'Omschrijf wat de app doet';
	protected $sIcon = 'comment-o';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppDescription';
	protected $sFqModelClassname = '\\\Model\System\App';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
