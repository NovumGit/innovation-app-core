<?php

namespace Crud\App\Field;

use Core\Utils;
use Crud\App\Field\Base\AppTypeId as BaseAppTypeId;
use Model\System\AppTypeQuery;

/**
 * Skeleton subclass for representing app_type_id field from the app table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class AppTypeId extends BaseAppTypeId
{
    public function getVisibleValue($iItemId)
    {
        return AppTypeQuery::create()->findOneById($iItemId)->getName();
    }

    public function getLookups($iSelectedItem = null)
    {
        return Utils::makeSelectOptions(AppTypeQuery::create(), 'getName', $iSelectedItem);
    }
}
