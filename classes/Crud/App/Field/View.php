<?php

namespace Crud\App\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\System\App as ModelObject;

class View extends Field implements IEventField
{

    protected $sFieldLabel = 'App testen';

    function getIcon()
    {
        return 'mobile-phone';
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-globe"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($mData)
    {
        if (!$mData instanceof ModelObject) {
            throw new LogicException("Expected an instance of ModelObject, got " . get_class($mData));
        }

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = '    <a href="/apps/app/test/test?app_id=' . $mData->getId() . '" class="btn btn-success br2 btn-xs fs12 d">';
        $sIcon = $mData->getAppType()->getCode() == 'web_app' ? 'globe' : 'mobile-phone';
        $aOut[] = '         <i class="fa fa-globe"></i>';
        $aOut[] = '    </a>';

        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
