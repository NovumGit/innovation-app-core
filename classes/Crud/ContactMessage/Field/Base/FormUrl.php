<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericUrl;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'form_url' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class FormUrl extends GenericUrl implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'form_url';
	protected $sFieldLabel = 'Formulier url';
	protected $sIcon = 'building';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFormUrl';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['form_url']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Formulier url" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
