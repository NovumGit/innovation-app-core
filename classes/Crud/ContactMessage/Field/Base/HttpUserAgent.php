<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'http_user_agent' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class HttpUserAgent extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'http_user_agent';
	protected $sFieldLabel = 'HTTP user agent';
	protected $sIcon = 'globe';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHttpUserAgent';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['http_user_agent']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "HTTP user agent" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
