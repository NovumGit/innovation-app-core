<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_new' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsNew extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_new';
	protected $sFieldLabel = 'Is nieuw';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsNew';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
