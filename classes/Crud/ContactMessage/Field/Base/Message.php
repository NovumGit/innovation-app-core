<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'message' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class Message extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'message';
	protected $sFieldLabel = 'Bericht';
	protected $sIcon = 'envelope';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMessage';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
