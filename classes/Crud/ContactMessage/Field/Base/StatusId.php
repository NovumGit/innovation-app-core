<?php
namespace Crud\ContactMessage\Field\Base;

use Core\Utils;
use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\ContactMessageStatusQuery;

/**
 * Base class that represents the 'status_id' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class StatusId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'status_id';
	protected $sFieldLabel = 'Status';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getStatusId';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \\Model\ContactMessageStatusQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \\Model\ContactMessageStatusQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
