<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'subject' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class Subject extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'subject';
	protected $sFieldLabel = 'Onderwerp';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSubject';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['subject']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Onderwerp" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
