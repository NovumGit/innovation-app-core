<?php
namespace Crud\ContactMessage\Field\Base;

use Core\Utils;
use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\Setting\MasterTable\LanguageQuery;

/**
 * Base class that represents the 'language_id' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class LanguageId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'language_id';
	protected $sFieldLabel = 'Klant';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLanguageId';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\MasterTable\LanguageQuery::create()->orderBydescription()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getdescription", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\MasterTable\LanguageQuery::create()->findOneById($iItemId)->getdescription();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
