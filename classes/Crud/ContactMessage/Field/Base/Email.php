<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericEmail;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'email' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class Email extends GenericEmail implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'email';
	protected $sFieldLabel = 'E-mail';
	protected $sIcon = 'envelope';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEmail';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
