<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericEmail;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'phone' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class Phone extends GenericEmail implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'phone';
	protected $sFieldLabel = 'Telefoonnummer';
	protected $sIcon = 'envelope';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhone';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
