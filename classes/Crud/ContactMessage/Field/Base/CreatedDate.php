<?php
namespace Crud\ContactMessage\Field\Base;

use Crud\ContactMessage\ICollectionField;
use Crud\Generic\Field\GenericDateTime;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'created_date' crud field from the 'contact_message' table.
 * This class is auto generated and should not be modified.
 */
abstract class CreatedDate extends GenericDateTime implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'created_date';
	protected $sFieldLabel = 'Aanmaak datum';
	protected $sIcon = 'calendar';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCreatedDate';
	protected $sFqModelClassname = '\\\Model\ContactMessage';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
