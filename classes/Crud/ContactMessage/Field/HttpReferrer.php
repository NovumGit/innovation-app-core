<?php
namespace Crud\ContactMessage\Field;

use Crud\ContactMessage\Field\Base\HttpReferrer as BaseHttpReferrer;

/**
 * Skeleton subclass for representing http_referrer field from the contact_message table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:28:10
 */
final class HttpReferrer extends BaseHttpReferrer
{
}
