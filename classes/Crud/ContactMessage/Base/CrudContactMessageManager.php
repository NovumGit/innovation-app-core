<?php
namespace Crud\ContactMessage\Base;

use Core\Utils;
use Crud;
use Crud\ContactMessage\FieldIterator;
use Crud\ContactMessage\Field\CompanyName;
use Crud\ContactMessage\Field\CreatedDate;
use Crud\ContactMessage\Field\CustomerId;
use Crud\ContactMessage\Field\Email;
use Crud\ContactMessage\Field\FormUrl;
use Crud\ContactMessage\Field\HttpReferrer;
use Crud\ContactMessage\Field\HttpUserAgent;
use Crud\ContactMessage\Field\IsNew;
use Crud\ContactMessage\Field\LanguageId;
use Crud\ContactMessage\Field\Message;
use Crud\ContactMessage\Field\Name;
use Crud\ContactMessage\Field\Note;
use Crud\ContactMessage\Field\Phone;
use Crud\ContactMessage\Field\ReferrerSite;
use Crud\ContactMessage\Field\RemoteAddr;
use Crud\ContactMessage\Field\StatusId;
use Crud\ContactMessage\Field\Subject;
use Crud\ContactMessage\Field\TypeId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessage;
use Model\ContactMessageQuery;
use Model\Map\ContactMessageTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ContactMessage instead if you need to override or add functionality.
 */
abstract class CrudContactMessageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ContactMessageQuery::create();
	}


	public function getTableMap(): ContactMessageTableMap
	{
		return new ContactMessageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ContactMessage";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "contact_message toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "contact_message aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IsNew', 'LanguageId', 'CustomerId', 'TypeId', 'StatusId', 'CompanyName', 'Name', 'Email', 'Phone', 'Subject', 'Message', 'FormUrl', 'ReferrerSite', 'HttpReferrer', 'RemoteAddr', 'HttpUserAgent', 'Note', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['IsNew', 'LanguageId', 'CustomerId', 'TypeId', 'StatusId', 'CompanyName', 'Name', 'Email', 'Phone', 'Subject', 'Message', 'FormUrl', 'ReferrerSite', 'HttpReferrer', 'RemoteAddr', 'HttpUserAgent', 'Note', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ContactMessage
	 */
	public function getModel(array $aData = null): ContactMessage
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oContactMessageQuery = ContactMessageQuery::create();
		     $oContactMessage = $oContactMessageQuery->findOneById($aData['id']);
		     if (!$oContactMessage instanceof ContactMessage) {
		         throw new LogicException("ContactMessage should be an instance of ContactMessage but got something else." . __METHOD__);
		     }
		     $oContactMessage = $this->fillVo($aData, $oContactMessage);
		}
		else {
		     $oContactMessage = new ContactMessage();
		     if (!empty($aData)) {
		         $oContactMessage = $this->fillVo($aData, $oContactMessage);
		     }
		}
		return $oContactMessage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ContactMessage
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ContactMessage
	{
		$oContactMessage = $this->getModel($aData);


		 if(!empty($oContactMessage))
		 {
		     $oContactMessage = $this->fillVo($aData, $oContactMessage);
		     $oContactMessage->save();
		 }
		return $oContactMessage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ContactMessage $oModel
	 * @return ContactMessage
	 */
	protected function fillVo(array $aData, ContactMessage $oModel): ContactMessage
	{
		if(isset($aData['is_new'])) {
		     $oField = new IsNew();
		     $mValue = $oField->sanitize($aData['is_new']);
		     $oModel->setIsNew($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['type_id'])) {
		     $oField = new TypeId();
		     $mValue = $oField->sanitize($aData['type_id']);
		     $oModel->setTypeId($mValue);
		}
		if(isset($aData['status_id'])) {
		     $oField = new StatusId();
		     $mValue = $oField->sanitize($aData['status_id']);
		     $oModel->setStatusId($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['phone'])) {
		     $oField = new Phone();
		     $mValue = $oField->sanitize($aData['phone']);
		     $oModel->setPhone($mValue);
		}
		if(isset($aData['subject'])) {
		     $oField = new Subject();
		     $mValue = $oField->sanitize($aData['subject']);
		     $oModel->setSubject($mValue);
		}
		if(isset($aData['message'])) {
		     $oField = new Message();
		     $mValue = $oField->sanitize($aData['message']);
		     $oModel->setMessage($mValue);
		}
		if(isset($aData['form_url'])) {
		     $oField = new FormUrl();
		     $mValue = $oField->sanitize($aData['form_url']);
		     $oModel->setFormUrl($mValue);
		}
		if(isset($aData['referrer_site'])) {
		     $oField = new ReferrerSite();
		     $mValue = $oField->sanitize($aData['referrer_site']);
		     $oModel->setReferrerSite($mValue);
		}
		if(isset($aData['http_referrer'])) {
		     $oField = new HttpReferrer();
		     $mValue = $oField->sanitize($aData['http_referrer']);
		     $oModel->setHttpReferrer($mValue);
		}
		if(isset($aData['remote_addr'])) {
		     $oField = new RemoteAddr();
		     $mValue = $oField->sanitize($aData['remote_addr']);
		     $oModel->setRemoteAddr($mValue);
		}
		if(isset($aData['http_user_agent'])) {
		     $oField = new HttpUserAgent();
		     $mValue = $oField->sanitize($aData['http_user_agent']);
		     $oModel->setHttpUserAgent($mValue);
		}
		if(isset($aData['note'])) {
		     $oField = new Note();
		     $mValue = $oField->sanitize($aData['note']);
		     $oModel->setNote($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
