<?php
namespace Crud;


use Model\Setting\CrudManager\CrudEditorBlockField;

final class FieldRow{

    private $aTitleParts = [];
    private $aFields = [];
    private $iRowWidth = 0;

    function addField(CrudEditorBlockField $oCrudEditorBlockField, int $iWidth)
    {
        $this->iRowWidth = $this->iRowWidth + $iWidth;
        $this->aTitleParts[] = $oCrudEditorBlockField->getFieldObject()->getTranslatedTitle();
        $this->aFields[] = new FieldRowItem($oCrudEditorBlockField, $iWidth);
    }
    function getFullRowWidth():int
    {
        return $this->iRowWidth;
    }

    function getLabel():string
    {
        return join(', ', $this->aTitleParts);
    }

    /**
     * @return FieldRowItem[]
     */
    function getFields():array
    {
        return $this->aFields;
    }
    function countFields():int
    {
        return count($this->aFields);
    }
}
