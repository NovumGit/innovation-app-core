<?php
namespace Crud\SaleOrderStatus\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderStatus\ICollectionField;

/**
 * Base class that represents the 'is_deletable' crud field from the 'mt_sale_order_status' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsDeletable extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_deletable';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsDeletable';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\SaleOrderStatus';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
