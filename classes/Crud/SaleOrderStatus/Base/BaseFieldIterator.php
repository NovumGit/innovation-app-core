<?php
namespace Crud\SaleOrderStatus\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderStatus\ICollectionField as SaleOrderStatusField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderStatus\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderStatusField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderStatusField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderStatusField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderStatusField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderStatusField
	{
		return current($this->aFields);
	}
}
