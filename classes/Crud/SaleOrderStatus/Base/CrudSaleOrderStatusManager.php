<?php
namespace Crud\SaleOrderStatus\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderStatus\FieldIterator;
use Crud\SaleOrderStatus\Field\Code;
use Crud\SaleOrderStatus\Field\IsDeletable;
use Crud\SaleOrderStatus\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\SaleOrderStatusTableMap;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderStatus instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderStatusManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderStatusQuery::create();
	}


	public function getTableMap(): SaleOrderStatusTableMap
	{
		return new SaleOrderStatusTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderStatus";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_sale_order_status toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_sale_order_status aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'IsDeletable'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'IsDeletable'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderStatus
	 */
	public function getModel(array $aData = null): SaleOrderStatus
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderStatusQuery = SaleOrderStatusQuery::create();
		     $oSaleOrderStatus = $oSaleOrderStatusQuery->findOneById($aData['id']);
		     if (!$oSaleOrderStatus instanceof SaleOrderStatus) {
		         throw new LogicException("SaleOrderStatus should be an instance of SaleOrderStatus but got something else." . __METHOD__);
		     }
		     $oSaleOrderStatus = $this->fillVo($aData, $oSaleOrderStatus);
		}
		else {
		     $oSaleOrderStatus = new SaleOrderStatus();
		     if (!empty($aData)) {
		         $oSaleOrderStatus = $this->fillVo($aData, $oSaleOrderStatus);
		     }
		}
		return $oSaleOrderStatus;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderStatus
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderStatus
	{
		$oSaleOrderStatus = $this->getModel($aData);


		 if(!empty($oSaleOrderStatus))
		 {
		     $oSaleOrderStatus = $this->fillVo($aData, $oSaleOrderStatus);
		     $oSaleOrderStatus->save();
		 }
		return $oSaleOrderStatus;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderStatus $oModel
	 * @return SaleOrderStatus
	 */
	protected function fillVo(array $aData, SaleOrderStatus $oModel): SaleOrderStatus
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['is_deletable'])) {
		     $oField = new IsDeletable();
		     $mValue = $oField->sanitize($aData['is_deletable']);
		     $oModel->setIsDeletable($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
