<?php
namespace Crud\DataSourceEndpointMethod\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataSourceEndpointMethod\ICollectionField as DataSourceEndpointMethodField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataSourceEndpointMethod\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataSourceEndpointMethodField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataSourceEndpointMethodField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataSourceEndpointMethodField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataSourceEndpointMethodField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataSourceEndpointMethodField
	{
		return current($this->aFields);
	}
}
