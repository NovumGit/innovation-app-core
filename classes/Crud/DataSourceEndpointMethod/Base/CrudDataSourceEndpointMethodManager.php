<?php
namespace Crud\DataSourceEndpointMethod\Base;

use Core\Utils;
use Crud;
use Crud\DataSourceEndpointMethod\FieldIterator;
use Crud\DataSourceEndpointMethod\Field\Code;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataSourceEndpointMethod;
use Model\System\DataSourceEndpointMethodQuery;
use Model\System\Map\DataSourceEndpointMethodTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataSourceEndpointMethod instead if you need to override or add functionality.
 */
abstract class CrudDataSourceEndpointMethodManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataSourceEndpointMethodQuery::create();
	}


	public function getTableMap(): DataSourceEndpointMethodTableMap
	{
		return new DataSourceEndpointMethodTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat ondersteunde acties op een endpoint";
	}


	public function getEntityTitle(): string
	{
		return "DataSourceEndpointMethod";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "datasource_endpoint_method toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "datasource_endpoint_method aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataSourceEndpointMethod
	 */
	public function getModel(array $aData = null): DataSourceEndpointMethod
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataSourceEndpointMethodQuery = DataSourceEndpointMethodQuery::create();
		     $oDataSourceEndpointMethod = $oDataSourceEndpointMethodQuery->findOneById($aData['id']);
		     if (!$oDataSourceEndpointMethod instanceof DataSourceEndpointMethod) {
		         throw new LogicException("DataSourceEndpointMethod should be an instance of DataSourceEndpointMethod but got something else." . __METHOD__);
		     }
		     $oDataSourceEndpointMethod = $this->fillVo($aData, $oDataSourceEndpointMethod);
		}
		else {
		     $oDataSourceEndpointMethod = new DataSourceEndpointMethod();
		     if (!empty($aData)) {
		         $oDataSourceEndpointMethod = $this->fillVo($aData, $oDataSourceEndpointMethod);
		     }
		}
		return $oDataSourceEndpointMethod;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataSourceEndpointMethod
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataSourceEndpointMethod
	{
		$oDataSourceEndpointMethod = $this->getModel($aData);


		 if(!empty($oDataSourceEndpointMethod))
		 {
		     $oDataSourceEndpointMethod = $this->fillVo($aData, $oDataSourceEndpointMethod);
		     $oDataSourceEndpointMethod->save();
		 }
		return $oDataSourceEndpointMethod;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataSourceEndpointMethod $oModel
	 * @return DataSourceEndpointMethod
	 */
	protected function fillVo(array $aData, DataSourceEndpointMethod $oModel): DataSourceEndpointMethod
	{
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
