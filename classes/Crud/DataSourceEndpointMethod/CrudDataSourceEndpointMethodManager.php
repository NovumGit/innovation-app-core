<?php
namespace Crud\DataSourceEndpointMethod;

use Crud\IApiAlwaysExpose;

/**
 * Skeleton subclass for representing a DataSourceEndpointMethod.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudDataSourceEndpointMethodManager extends Base\CrudDataSourceEndpointMethodManager implements IApiAlwaysExpose
{
}
