<?php
namespace Crud\DataSourceEndpointMethod\Field\Base;

use Crud\DataSourceEndpointMethod\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'code' crud field from the 'datasource_endpoint_method' table.
 * This class is auto generated and should not be modified.
 */
abstract class Code extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'code';
	protected $sFieldLabel = 'Code';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCode';
	protected $sFqModelClassname = '\\\Model\System\DataSourceEndpointMethod';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['code']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Code" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
