<?php 
namespace Crud\DataSourceEndpointMethod\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataSourceEndpointMethod;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSourceEndpointMethod)
		{
		     return "//system/datasource_endpoint_method/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataSourceEndpointMethod)
		{
		     return "//datasource_endpoint_method?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
