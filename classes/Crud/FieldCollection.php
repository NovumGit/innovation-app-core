<?php

namespace Crud;

class FieldCollection implements ICrudFieldIterator
{

    /**
     * @var IComponent[]
     */
    private array $aFields;

    /**
     * @param IComponent[] $aFields
     */
    public function __construct(array $aFields)
    {
        $this->aFields = $aFields;
    }
    public function add(IComponent $oField)
    {
        $this->aFields[] = $oField;
    }

    function next(): void
    {
        next($this->aFields);
    }

    function valid(): bool
    {
        $key = key($this->aFields);
        return ($key !== null && $key !== false);
    }

    function current(): IComponent
    {
        return current($this->aFields);
    }

    function rewind()
    {
        reset($this->aFields);
    }

    function key(): int
    {
        return key($this->aFields);
    }
}
