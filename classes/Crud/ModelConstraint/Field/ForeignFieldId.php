<?php
namespace Crud\ModelConstraint\Field;

use Crud\Generic\Lookups\DataFieldLookupsTrait;
use Crud\ModelConstraint\Field\Base\ForeignFieldId as BaseForeignFieldId;

/**
 * Skeleton subclass for representing foreign_field_id field from the model_constraint table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ForeignFieldId extends BaseForeignFieldId
{
    use DataFieldLookupsTrait;
}
