<?php
namespace Crud\ModelConstraint\Field\Base;

use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ModelConstraint\ICollectionField;

/**
 * Base class that represents the 'local_field_id' crud field from the 'model_constraint' table.
 * This class is auto generated and should not be modified.
 */
abstract class LocalFieldId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'local_field_id';
	protected $sFieldLabel = 'Local column';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLocalFieldId';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\ModelConstraint';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['local_field_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Local column" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
