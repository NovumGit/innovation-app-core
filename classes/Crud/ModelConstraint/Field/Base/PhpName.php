<?php
namespace Crud\ModelConstraint\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ModelConstraint\ICollectionField;

/**
 * Base class that represents the 'php_name' crud field from the 'model_constraint' table.
 * This class is auto generated and should not be modified.
 */
abstract class PhpName extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'php_name';
	protected $sFieldLabel = 'Php name';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhpName';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\ModelConstraint';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['php_name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Php name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
