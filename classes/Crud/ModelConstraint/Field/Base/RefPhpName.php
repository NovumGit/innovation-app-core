<?php
namespace Crud\ModelConstraint\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\ModelConstraint\ICollectionField;

/**
 * Base class that represents the 'ref_php_name' crud field from the 'model_constraint' table.
 * This class is auto generated and should not be modified.
 */
abstract class RefPhpName extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'ref_php_name';
	protected $sFieldLabel = 'Ref php name';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRefPhpName';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\ModelConstraint';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['ref_php_name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Ref php name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
