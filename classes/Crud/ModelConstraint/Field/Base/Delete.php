<?php 
namespace Crud\ModelConstraint\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataModel\Model\ModelConstraint;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof ModelConstraint)
		{
		     return "//system/model_constraint/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof ModelConstraint)
		{
		     return "//model_constraint?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
