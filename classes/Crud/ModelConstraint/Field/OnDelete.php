<?php
namespace Crud\ModelConstraint\Field;

use Core\Utils;
use Crud\ModelConstraint\Field\Base\OnDelete as BaseOnDelete;

/**
 * Skeleton subclass for representing on_delete field from the model_constraint table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OnDelete extends BaseOnDelete
{
    private function getOptions():array
    {
        return  [
            ['id' => 'set null', 'label' => 'set null'],
            ['id' => 'cascade', 'label' => 'cascade'],
            ['id' => 'restruct', 'label' => 'set null'],
            ['id' => 'none', 'label' => 'set null'],
        ];
    }
    function getVisibleValue($iItemId)
    {
        $aOptions = $this->getOptions();
        foreach($aOptions as $aOption)
        {
            if($aOption['id'] == $iItemId)
            {
                return $aOption['label'];
            }
        }
        return null;
    }
    function getLookups($iSelectedItem = null)
    {

        return Utils::makeSelectOptions($this->getOptions(), 'label', $iSelectedItem);
    }
}
