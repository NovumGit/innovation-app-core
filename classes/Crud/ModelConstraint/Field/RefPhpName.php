<?php
namespace Crud\ModelConstraint\Field;

use Crud\ModelConstraint\Field\Base\RefPhpName as BaseRefPhpName;

/**
 * Skeleton subclass for representing ref_php_name field from the model_constraint table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Mon 12:56:38
 */
final class RefPhpName extends BaseRefPhpName
{
}
