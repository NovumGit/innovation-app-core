<?php
namespace Crud\ModelConstraint\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ModelConstraint\ICollectionField as ModelConstraintField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ModelConstraint\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ModelConstraintField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ModelConstraintField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ModelConstraintField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ModelConstraintField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ModelConstraintField
	{
		return current($this->aFields);
	}
}
