<?php
namespace Crud\ModelConstraint\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ModelConstraint\FieldIterator;
use Crud\ModelConstraint\Field\ForeignFieldId;
use Crud\ModelConstraint\Field\LocalFieldId;
use Crud\ModelConstraint\Field\OnDelete;
use Crud\ModelConstraint\Field\OnUpdate;
use Crud\ModelConstraint\Field\PhpName;
use Crud\ModelConstraint\Field\RefPhpName;
use Exception\LogicException;
use Model\System\DataModel\Model\Map\ModelConstraintTableMap;
use Model\System\DataModel\Model\ModelConstraint;
use Model\System\DataModel\Model\ModelConstraintQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ModelConstraint instead if you need to override or add functionality.
 */
abstract class CrudModelConstraintManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ModelConstraintQuery::create();
	}


	public function getTableMap(): ModelConstraintTableMap
	{
		return new ModelConstraintTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat relaties tussen modellen.";
	}


	public function getEntityTitle(): string
	{
		return "ModelConstraint";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "model_constraint toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "model_constraint aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PhpName', 'RefPhpName', 'ForeignFieldId', 'LocalFieldId', 'OnDelete', 'OnUpdate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PhpName', 'RefPhpName', 'ForeignFieldId', 'LocalFieldId', 'OnDelete', 'OnUpdate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ModelConstraint
	 */
	public function getModel(array $aData = null): ModelConstraint
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oModelConstraintQuery = ModelConstraintQuery::create();
		     $oModelConstraint = $oModelConstraintQuery->findOneById($aData['id']);
		     if (!$oModelConstraint instanceof ModelConstraint) {
		         throw new LogicException("ModelConstraint should be an instance of ModelConstraint but got something else." . __METHOD__);
		     }
		     $oModelConstraint = $this->fillVo($aData, $oModelConstraint);
		}
		else {
		     $oModelConstraint = new ModelConstraint();
		     if (!empty($aData)) {
		         $oModelConstraint = $this->fillVo($aData, $oModelConstraint);
		     }
		}
		return $oModelConstraint;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ModelConstraint
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ModelConstraint
	{
		$oModelConstraint = $this->getModel($aData);


		 if(!empty($oModelConstraint))
		 {
		     $oModelConstraint = $this->fillVo($aData, $oModelConstraint);
		     $oModelConstraint->save();
		 }
		return $oModelConstraint;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ModelConstraint $oModel
	 * @return ModelConstraint
	 */
	protected function fillVo(array $aData, ModelConstraint $oModel): ModelConstraint
	{
		if(isset($aData['php_name'])) {
		     $oField = new PhpName();
		     $mValue = $oField->sanitize($aData['php_name']);
		     $oModel->setPhpName($mValue);
		}
		if(isset($aData['ref_php_name'])) {
		     $oField = new RefPhpName();
		     $mValue = $oField->sanitize($aData['ref_php_name']);
		     $oModel->setRefPhpName($mValue);
		}
		if(isset($aData['foreign_field_id'])) {
		     $oField = new ForeignFieldId();
		     $mValue = $oField->sanitize($aData['foreign_field_id']);
		     $oModel->setForeignFieldId($mValue);
		}
		if(isset($aData['local_field_id'])) {
		     $oField = new LocalFieldId();
		     $mValue = $oField->sanitize($aData['local_field_id']);
		     $oModel->setLocalFieldId($mValue);
		}
		if(isset($aData['on_delete'])) {
		     $oField = new OnDelete();
		     $mValue = $oField->sanitize($aData['on_delete']);
		     $oModel->setOnDelete($mValue);
		}
		if(isset($aData['on_update'])) {
		     $oField = new OnUpdate();
		     $mValue = $oField->sanitize($aData['on_update']);
		     $oModel->setOnUpdate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
