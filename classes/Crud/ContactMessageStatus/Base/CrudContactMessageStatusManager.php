<?php
namespace Crud\ContactMessageStatus\Base;

use Core\Utils;
use Crud;
use Crud\ContactMessageStatus\FieldIterator;
use Crud\ContactMessageStatus\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessageStatus;
use Model\ContactMessageStatusQuery;
use Model\Map\ContactMessageStatusTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ContactMessageStatus instead if you need to override or add functionality.
 */
abstract class CrudContactMessageStatusManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ContactMessageStatusQuery::create();
	}


	public function getTableMap(): ContactMessageStatusTableMap
	{
		return new ContactMessageStatusTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ContactMessageStatus";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "contact_message_status toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "contact_message_status aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ContactMessageStatus
	 */
	public function getModel(array $aData = null): ContactMessageStatus
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oContactMessageStatusQuery = ContactMessageStatusQuery::create();
		     $oContactMessageStatus = $oContactMessageStatusQuery->findOneById($aData['id']);
		     if (!$oContactMessageStatus instanceof ContactMessageStatus) {
		         throw new LogicException("ContactMessageStatus should be an instance of ContactMessageStatus but got something else." . __METHOD__);
		     }
		     $oContactMessageStatus = $this->fillVo($aData, $oContactMessageStatus);
		}
		else {
		     $oContactMessageStatus = new ContactMessageStatus();
		     if (!empty($aData)) {
		         $oContactMessageStatus = $this->fillVo($aData, $oContactMessageStatus);
		     }
		}
		return $oContactMessageStatus;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ContactMessageStatus
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ContactMessageStatus
	{
		$oContactMessageStatus = $this->getModel($aData);


		 if(!empty($oContactMessageStatus))
		 {
		     $oContactMessageStatus = $this->fillVo($aData, $oContactMessageStatus);
		     $oContactMessageStatus->save();
		 }
		return $oContactMessageStatus;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ContactMessageStatus $oModel
	 * @return ContactMessageStatus
	 */
	protected function fillVo(array $aData, ContactMessageStatus $oModel): ContactMessageStatus
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
