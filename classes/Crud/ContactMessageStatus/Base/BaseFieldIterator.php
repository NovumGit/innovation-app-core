<?php
namespace Crud\ContactMessageStatus\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ContactMessageStatus\ICollectionField as ContactMessageStatusField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ContactMessageStatus\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ContactMessageStatusField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ContactMessageStatusField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ContactMessageStatusField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ContactMessageStatusField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ContactMessageStatusField
	{
		return current($this->aFields);
	}
}
