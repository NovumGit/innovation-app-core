<?php
namespace Crud\CustomerNote\Base;

use Core\Utils;
use Crud;
use Crud\CustomerNote\FieldIterator;
use Crud\CustomerNote\Field\CreatedByUserId;
use Crud\CustomerNote\Field\CreatedOn;
use Crud\CustomerNote\Field\CustomerId;
use Crud\CustomerNote\Field\Description;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerNote;
use Model\Crm\CustomerNoteQuery;
use Model\Crm\Map\CustomerNoteTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerNote instead if you need to override or add functionality.
 */
abstract class CrudCustomerNoteManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerNoteQuery::create();
	}


	public function getTableMap(): CustomerNoteTableMap
	{
		return new CustomerNoteTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerNote";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_note toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_note aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'CustomerId', 'CreatedByUserId', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'CustomerId', 'CreatedByUserId', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerNote
	 */
	public function getModel(array $aData = null): CustomerNote
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerNoteQuery = CustomerNoteQuery::create();
		     $oCustomerNote = $oCustomerNoteQuery->findOneById($aData['id']);
		     if (!$oCustomerNote instanceof CustomerNote) {
		         throw new LogicException("CustomerNote should be an instance of CustomerNote but got something else." . __METHOD__);
		     }
		     $oCustomerNote = $this->fillVo($aData, $oCustomerNote);
		}
		else {
		     $oCustomerNote = new CustomerNote();
		     if (!empty($aData)) {
		         $oCustomerNote = $this->fillVo($aData, $oCustomerNote);
		     }
		}
		return $oCustomerNote;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerNote
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerNote
	{
		$oCustomerNote = $this->getModel($aData);


		 if(!empty($oCustomerNote))
		 {
		     $oCustomerNote = $this->fillVo($aData, $oCustomerNote);
		     $oCustomerNote->save();
		 }
		return $oCustomerNote;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerNote $oModel
	 * @return CustomerNote
	 */
	protected function fillVo(array $aData, CustomerNote $oModel): CustomerNote
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['created_by_user_id'])) {
		     $oField = new CreatedByUserId();
		     $mValue = $oField->sanitize($aData['created_by_user_id']);
		     $oModel->setCreatedByUserId($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
