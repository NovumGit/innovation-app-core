<?php
namespace Crud\CustomerNote\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerNote\ICollectionField as CustomerNoteField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerNote\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerNoteField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerNoteField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerNoteField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerNoteField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerNoteField
	{
		return current($this->aFields);
	}
}
