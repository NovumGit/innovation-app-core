<?php
namespace Crud\CustomerNote\Field\Base;

use Crud\CustomerNote\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'created_by_user_id' crud field from the 'customer_note' table.
 * This class is auto generated and should not be modified.
 */
abstract class CreatedByUserId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'created_by_user_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCreatedByUserId';
	protected $sFqModelClassname = '\\\Model\Crm\CustomerNote';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
