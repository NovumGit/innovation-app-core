<?php
namespace Crud\Component_form\Field\Base;

use Core\Utils;
use Crud\Component_form\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\LowCode\Form\Component_form_render_styleQuery;

/**
 * Base class that represents the 'fk_component_form_render_style' crud field from the 'component_form' table.
 * This class is auto generated and should not be modified.
 */
abstract class FkComponentFormRenderStyle extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'fk_component_form_render_style';
	protected $sFieldLabel = 'Form rendering style';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFkComponentFormRenderStyle';
	protected $sFqModelClassname = '\Model\System\LowCode\Form\Component_form';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\LowCode\Form\Component_form_render_styleQuery::create()->orderByItemLabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getItemLabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\LowCode\Form\Component_form_render_styleQuery::create()->findOneById($iItemId)->getItemLabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
