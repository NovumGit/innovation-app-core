<?php
namespace Crud\Component_form\Field\Base;

use Crud\Component_form\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'layout_key' crud field from the 'component_form' table.
 * This class is auto generated and should not be modified.
 */
abstract class LayoutKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'layout_key';
	protected $sFieldLabel = 'Layout key';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLayoutKey';
	protected $sFqModelClassname = '\Model\System\LowCode\Form\Component_form';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
