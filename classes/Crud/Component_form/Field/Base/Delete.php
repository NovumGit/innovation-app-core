<?php 
namespace Crud\Component_form\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Form\Component_form;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_form)
		{
		     return "//system/component_form/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_form)
		{
		     return "//component_form?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
