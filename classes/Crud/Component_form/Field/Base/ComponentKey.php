<?php
namespace Crud\Component_form\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'component_key' crud field from the 'component_form' table.
 * This class is auto generated and should not be modified.
 */
abstract class ComponentKey extends GenericString implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'component_key';

	protected $sFieldLabel = 'Layout key';

	protected $sIcon = '';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getComponentKey';

	protected $sFqModelClassname = '\Model\System\LowCode\Form\Component_form';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['component_key']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Layout key" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
