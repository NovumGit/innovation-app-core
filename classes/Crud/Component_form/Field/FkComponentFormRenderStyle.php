<?php
namespace Crud\Component_form\Field;

use Crud\Component_form\Field\Base\FkComponentFormRenderStyle as BaseFkComponentFormRenderStyle;

/**
 * Skeleton subclass for representing fk_component_form_render_style field from the component_form table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentFormRenderStyle extends BaseFkComponentFormRenderStyle
{
}
