<?php
namespace Crud\Component_form\Field;

use Crud\Component_form\Field\Base\FkComponentFormButtonLocation as BaseFkComponentFormButtonLocation;

/**
 * Skeleton subclass for representing fk_component_form_button_location field from the component_form table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentFormButtonLocation extends BaseFkComponentFormButtonLocation
{
}
