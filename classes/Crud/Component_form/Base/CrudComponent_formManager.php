<?php
namespace Crud\Component_form\Base;

use Core\Utils;
use Crud;
use Crud\Component_form\FieldIterator;
use Crud\Component_form\Field\FkComponentFormButtonLocation;
use Crud\Component_form\Field\FkComponentFormRenderStyle;
use Crud\Component_form\Field\LayoutKey;
use Crud\Component_form\Field\Manager;
use Crud\Component_form\Field\Title;
use Crud\Component_form\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Form\Component_form;
use Model\System\LowCode\Form\Component_formQuery;
use Model\System\LowCode\Form\Map\Component_formTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_form instead if you need to override or add functionality.
 */
abstract class CrudComponent_formManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_formQuery::create();
	}


	public function getTableMap(): Component_formTableMap
	{
		return new Component_formTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van IForm component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_form";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_form toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_form aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'LayoutKey', 'Title', 'FkComponentFormRenderStyle', 'FkComponentFormButtonLocation', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'LayoutKey', 'Title', 'FkComponentFormRenderStyle', 'FkComponentFormButtonLocation', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_form
	 */
	public function getModel(array $aData = null): Component_form
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_formQuery = Component_formQuery::create();
		     $oComponent_form = $oComponent_formQuery->findOneById($aData['id']);
		     if (!$oComponent_form instanceof Component_form) {
		         throw new LogicException("Component_form should be an instance of Component_form but got something else." . __METHOD__);
		     }
		     $oComponent_form = $this->fillVo($aData, $oComponent_form);
		}
		else {
		     $oComponent_form = new Component_form();
		     if (!empty($aData)) {
		         $oComponent_form = $this->fillVo($aData, $oComponent_form);
		     }
		}
		return $oComponent_form;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_form
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_form
	{
		$oComponent_form = $this->getModel($aData);


		 if(!empty($oComponent_form))
		 {
		     $oComponent_form = $this->fillVo($aData, $oComponent_form);
		     $oComponent_form->save();
		 }
		return $oComponent_form;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_form $oModel
	 * @return Component_form
	 */
	protected function fillVo(array $aData, Component_form $oModel): Component_form
	{
		if(isset($aData['manager'])) {
		     $oField = new Manager();
		     $mValue = $oField->sanitize($aData['manager']);
		     $oModel->setManager($mValue);
		}
		if(isset($aData['layout_key'])) {
		     $oField = new LayoutKey();
		     $mValue = $oField->sanitize($aData['layout_key']);
		     $oModel->setLayoutKey($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['fk_component_form_render_style'])) {
		     $oField = new FkComponentFormRenderStyle();
		     $mValue = $oField->sanitize($aData['fk_component_form_render_style']);
		     $oModel->setFkComponentFormRenderStyle($mValue);
		}
		if(isset($aData['fk_component_form_button_location'])) {
		     $oField = new FkComponentFormButtonLocation();
		     $mValue = $oField->sanitize($aData['fk_component_form_button_location']);
		     $oModel->setFkComponentFormButtonLocation($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
