<?php
namespace Crud\Component_form\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_form\ICollectionField as Component_formField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_form\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_formField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_formField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_formField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_formField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_formField
	{
		return current($this->aFields);
	}
}
