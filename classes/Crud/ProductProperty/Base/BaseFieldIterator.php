<?php
namespace Crud\ProductProperty\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductProperty\ICollectionField as ProductPropertyField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductProperty\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductPropertyField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductPropertyField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductPropertyField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductPropertyField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductPropertyField
	{
		return current($this->aFields);
	}
}
