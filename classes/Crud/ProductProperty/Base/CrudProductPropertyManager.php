<?php
namespace Crud\ProductProperty\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductProperty\FieldIterator;
use Crud\ProductProperty\Field\Field;
use Crud\ProductProperty\Field\ProductId;
use Crud\ProductProperty\Field\Value;
use Exception\LogicException;
use Model\Map\ProductPropertyTableMap;
use Model\ProductProperty;
use Model\ProductPropertyQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductProperty instead if you need to override or add functionality.
 */
abstract class CrudProductPropertyManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductPropertyQuery::create();
	}


	public function getTableMap(): ProductPropertyTableMap
	{
		return new ProductPropertyTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductProperty";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "product_property toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "product_property aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'Field', 'Value'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ProductId', 'Field', 'Value'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductProperty
	 */
	public function getModel(array $aData = null): ProductProperty
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductPropertyQuery = ProductPropertyQuery::create();
		     $oProductProperty = $oProductPropertyQuery->findOneById($aData['id']);
		     if (!$oProductProperty instanceof ProductProperty) {
		         throw new LogicException("ProductProperty should be an instance of ProductProperty but got something else." . __METHOD__);
		     }
		     $oProductProperty = $this->fillVo($aData, $oProductProperty);
		}
		else {
		     $oProductProperty = new ProductProperty();
		     if (!empty($aData)) {
		         $oProductProperty = $this->fillVo($aData, $oProductProperty);
		     }
		}
		return $oProductProperty;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductProperty
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductProperty
	{
		$oProductProperty = $this->getModel($aData);


		 if(!empty($oProductProperty))
		 {
		     $oProductProperty = $this->fillVo($aData, $oProductProperty);
		     $oProductProperty->save();
		 }
		return $oProductProperty;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductProperty $oModel
	 * @return ProductProperty
	 */
	protected function fillVo(array $aData, ProductProperty $oModel): ProductProperty
	{
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['field'])) {
		     $oField = new Field();
		     $mValue = $oField->sanitize($aData['field']);
		     $oModel->setField($mValue);
		}
		if(isset($aData['value'])) {
		     $oField = new Value();
		     $mValue = $oField->sanitize($aData['value']);
		     $oModel->setValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
