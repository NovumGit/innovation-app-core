<?php
namespace Crud\ProductProperty\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductProperty\ICollectionField;

/**
 * Base class that represents the 'value' crud field from the 'product_property' table.
 * This class is auto generated and should not be modified.
 */
abstract class Value extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'value';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getValue';
	protected $sFqModelClassname = '\\\Model\ProductProperty';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
