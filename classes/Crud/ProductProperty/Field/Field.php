<?php
namespace Crud\ProductProperty\Field;

use Crud\ProductProperty\Field\Base\Field as BaseField;

/**
 * Skeleton subclass for representing field field from the product_property table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Field extends BaseField
{
}
