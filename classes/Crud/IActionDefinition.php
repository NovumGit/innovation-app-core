<?php
namespace Crud;

use Core\IAbstractEvent;

/**
 * The class AbstractEvent holds all the logic for storing events so you should extend that or implement it your self.
 *
 * Interface IActionDefinition
 * @package Crud
 */
interface IActionDefinition extends IAbstractEvent {

    function outputType(): \Core\Mime\Mime;
}
