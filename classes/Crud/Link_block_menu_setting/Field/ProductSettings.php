<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class ProductSettings extends Link
{
    protected $sFieldLabel = 'Product instellingen';

    function getLinkUrl()
    {
        return '/setting/product/config';
    }
}