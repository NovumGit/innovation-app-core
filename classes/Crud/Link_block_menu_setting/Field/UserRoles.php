<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class UserRoles extends Link
{
    protected $sFieldLabel = 'Gebruikers rollen';

    function getLinkUrl()
    {
        return '/setting/user_role/overview';
    }
}