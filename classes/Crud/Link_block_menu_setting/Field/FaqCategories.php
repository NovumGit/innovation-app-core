<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class FaqCategories extends Link
{
    protected $sFieldLabel = 'FAQ categorieën';

    function getLinkUrl()
    {
        return '/setting/faq_category/choose_site';
    }
}