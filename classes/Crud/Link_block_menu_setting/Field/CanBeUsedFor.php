<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class CanBeUsedFor extends Link
{
    protected $sFieldLabel = 'Gebruiken / bruibaar voor';

    function getLinkUrl()
    {
        return '/setting/mastertable/usage/overview';
    }
}