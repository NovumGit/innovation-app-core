<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class CompanyLegalForms extends Link
{
    protected $sFieldLabel = 'Bedrijf rechtsvormen';

    function getLinkUrl()
    {
        return '/setting/mastertable/legal_form/overview';
    }
}