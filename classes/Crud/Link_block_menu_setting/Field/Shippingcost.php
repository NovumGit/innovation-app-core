<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Shippingcost extends Link
{
    protected $sFieldLabel = 'Verzend instellingen';

    function getLinkUrl()
    {
        return '/setting/shipping/shippingcost';
    }
}