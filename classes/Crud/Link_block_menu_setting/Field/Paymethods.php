<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Paymethods extends Link
{
    protected $sFieldLabel = 'Betaalmethoden';

    function getLinkUrl()
    {
        return '/setting/mastertable/payment_method/overview';
    }
}