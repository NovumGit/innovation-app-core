<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class StatusEmailMessages extends Link
{
    protected $sFieldLabel = 'Status e-mailberichten';

    function getLinkUrl()
    {
        return '/setting/sale_order/status_email/overview';
    }
}