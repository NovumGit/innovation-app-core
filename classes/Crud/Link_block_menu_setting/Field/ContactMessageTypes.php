<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Contact_message_type\CrudContact_message_typeManager;
use Crud\Link;

class ContactMessageTypes extends Link
{
    protected $sFieldLabel = 'Contact bericht typen';

    function getLinkUrl()
    {
        return (new CrudContact_message_typeManager())->getOverviewUrl();
    }
}