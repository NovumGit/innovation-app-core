<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class OrderSettings extends Link
{
    protected $sFieldLabel = 'Order instellingen';

    function getLinkUrl()
    {
        return '/setting/sale_order/config';
    }
}