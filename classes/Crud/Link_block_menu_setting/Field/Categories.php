<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Categories extends Link
{
    protected $sFieldLabel = 'Categorieën';

    function getLinkUrl()
    {
        return '/setting/category/overview';
    }
}