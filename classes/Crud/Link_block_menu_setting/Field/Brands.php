<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Brands extends Link
{
    protected $sFieldLabel = 'Merken';

    function getLinkUrl()
    {
        return '/setting/mastertable/brand/overview';
    }
}