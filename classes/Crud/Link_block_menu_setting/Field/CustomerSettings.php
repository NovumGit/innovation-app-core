<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class CustomerSettings extends Link
{
    protected $sFieldLabel = 'Klant instellingen';

    function getLinkUrl()
    {
        return '/setting/customer/config';
    }
}