<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Countries extends Link
{
    protected $sFieldLabel = 'Landen';

    function getLinkUrl()
    {
        return '/setting/mastertable/country/overview';
    }
}