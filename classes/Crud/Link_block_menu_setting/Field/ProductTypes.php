<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class ProductTypes extends Link
{
    protected $sFieldLabel = 'Product typen';

    function getLinkUrl()
    {
        return '/setting/mastertable/product_type/overview';
    }
}