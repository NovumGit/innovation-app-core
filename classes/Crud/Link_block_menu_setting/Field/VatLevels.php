<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class VatLevels extends Link
{
    protected $sFieldLabel = 'BTW Tarieven';

    function getLinkUrl()
    {
        return '/setting/mastertable/vat/overview';
    }
}