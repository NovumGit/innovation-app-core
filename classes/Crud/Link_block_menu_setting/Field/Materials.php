<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Materials extends Link
{
    protected $sFieldLabel = 'Materialen';

    function getLinkUrl()
    {
        return '/setting/mastertable/material/overview';
    }
}