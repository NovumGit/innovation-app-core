<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Contact_message_status\CrudContact_message_statusManager;
use Crud\Link;

class ContactStatusses extends Link
{
    protected $sFieldLabel = 'Contact statussen';

    function getLinkUrl()
    {
        return (new CrudContact_message_statusManager())->getOverviewUrl();
    }
}