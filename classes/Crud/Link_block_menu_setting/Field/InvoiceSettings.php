<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class InvoiceSettings extends Link
{
    protected $sFieldLabel = 'Factuur instellingen';

    function getLinkUrl()
    {
        return '/setting/invoice/config';
    }
}