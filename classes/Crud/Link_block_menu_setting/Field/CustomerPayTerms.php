<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class CustomerPayTerms extends Link
{
    protected $sFieldLabel = 'Klant betaaltermijnen';

    function getLinkUrl()
    {
        return '/setting/mastertable/payterm/overview';
    }
}