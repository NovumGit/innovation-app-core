<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class ProductDeliveryTimes extends Link
{
    protected $sFieldLabel = 'Product levertijden';

    function getLinkUrl()
    {
        return '/setting/mastertable/product_delivery_time/overview';
    }
}