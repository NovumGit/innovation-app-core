<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Units extends Link
{
    protected $sFieldLabel = 'Eenheden';

    function getLinkUrl()
    {
        return '/setting/mastertable/unit/overview';
    }
}