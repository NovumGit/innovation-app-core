<?php
namespace Crud\Link_block_menu_setting\Field\LitamsIot;

use Crud\Link;

class Tagreader extends Link
{
    protected $sFieldLabel = 'RFID scanners';

    function getLinkUrl()
    {
        return '/custom/litamsiot/settings/tagreader_overview';
    }
}