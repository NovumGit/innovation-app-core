<?php
namespace Crud\Link_block_menu_setting\Field\LitamsIot;

use Crud\Link;

class Scale extends Link
{
    protected $sFieldLabel = 'Weegschaal typen';

    function getLinkUrl()
    {
        return '/custom/litamsiot/settings/scale_overview';
    }
}