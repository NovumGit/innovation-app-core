<?php
namespace Crud\Link_block_menu_setting\Field;

use Crud\Link;

class Colors extends Link
{
    protected $sFieldLabel = 'Kleuren';

    function getLinkUrl()
    {
        return '/setting/mastertable/color/overview';
    }
}