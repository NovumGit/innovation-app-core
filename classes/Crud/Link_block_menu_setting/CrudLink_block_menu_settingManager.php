<?php
namespace Crud\Link_block_menu_setting;

use Crud\Link_blockManager;

class CrudLink_block_menu_settingManager extends Link_blockManager  {


    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return $this->getDefaultOverviewFields($bAddNamespaceUnimplemented);
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CompanyLegalForms',
            'VatLevels',
            'Categories',
            'Units',
            'UserRoles',
            'InvoiceSettings',
            'FaqCategories',
            'CanBeUsedFor',
            'CustomerPayTerms',
            'CustomerSettings',
            'Colors',
            'Countries',
            'Materials',
            'OrderSettings',
            'ProductSettings',
            'ProductTypes',
            'Paymethods',
            'ProductDeliveryTimes',
            'StatusEmailMessages'
        ];
    }
}
