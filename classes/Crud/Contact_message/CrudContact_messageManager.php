<?php
namespace Crud\Contact_message;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessage;
use Model\ContactMessageQuery;

class CrudContact_messageManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'bericht';
    }
    function getOverviewUrl():string
    {
        return '/contact/overview';
    }
    function getCreateNewUrl():string
    {
        return '/contact/edit';
    }
    function getNewFormTitle():string
    {
        return 'Contact bericht toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Contact bericht wijzigen';
    }

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Email',
            'Subject',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
        return [
            'Name',
            'Email',
            'Subject',
            'Message'
        ];
    }

    function getModel(array $aData = null)
    {

        if(isset($aData['id']) && $aData['id'])
        {
            $oContactMessageQuery = ContactMessageQuery::create();
            $oContactMessage = $oContactMessageQuery->findOneById($aData['id']);

            if(!$oContactMessage instanceof ContactMessage)
            {
                throw new LogicException("ContactMessage should be an instance of ContactMessage but got ".get_class($oContactMessage)." in ".__METHOD__);
            }
        }
        else
        {
            $oContactMessage = new ContactMessage();
            if(!empty($aData))
            {
                $oContactMessage = $this->fillVo($aData, $oContactMessage);
            }
        }
        return $oContactMessage;
    }

    function store(array $aData = null)
    {
        $oContactMessage = $this->getModel($aData);
        if(!empty($oContactMessage))
        {
            $oContactMessage = $this->fillVo($aData, $oContactMessage);
            $oContactMessage->save();
        }
        $this->saveCustomFields($oContactMessage, $aData);
        return $oContactMessage;
    }

    private function fillVo($aData, ContactMessage $oContactMessage)
    {

        if(isset($aData['name']))
        {
            $oContactMessage->setName($aData['name']);
        }

        if(isset($aData['contact_message_type_id']))
        {
            $oContactMessage->setTypeId($aData['contact_message_type_id']);
        }
        if(isset($aData['contact_message_status_id']))
        {
            $oContactMessage->setStatusId($aData['contact_message_status_id']);
        }
        if(isset($aData['customer_id']))
        {
            $oContactMessage->setCustomerId($aData['customer_id']);
        }
        if(isset($aData['email']))
        {
            $oContactMessage->setEmail($aData['email']);
        }
        if(isset($aData['phone']))
        {
            $oContactMessage->setPhone($aData['phone']);
        }
        if(isset($aData['subject']))
        {
            $oContactMessage->setSubject($aData['subject']);
        }
        if(isset($aData['referrer_site']))
        {
            $oContactMessage->setReferrerSite($aData['referrer_site']);
        }
        if(isset($aData['http_referrer']))
        {
            $oContactMessage->setHttpReferrer($aData['http_referrer']);
        }
        if(isset($aData['remote_addr']))
        {
            $oContactMessage->setRemoteAddr($aData['remote_addr']);
        }
        if(isset($aData['http_user_agent']))
        {
            $oContactMessage->setHttpUserAgent($aData['http_user_agent']);
        }
        if(isset($aData['note']))
        {
            $oContactMessage->setNote($aData['note']);
        }
        if(isset($aData['created_date']))
        {
            $oContactMessage->setCreatedDate($aData['created_date']);
        }
        return $oContactMessage;
    }

}
