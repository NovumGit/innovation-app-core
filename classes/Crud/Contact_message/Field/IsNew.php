<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Contact_message\Field;

use Crud\Field;
use Crud\Generic\Field\GenericBoolean;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\ContactMessage as ModelObject;


class IsNew extends GenericBoolean{

    protected $sFieldName = 'is_new';
    protected $sFieldLabel = 'Ongelezen';
    protected $sIcon = '';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getisNew';

    function getGetter()
    {
        return $this->sGetter;
    }

    /**
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Nieuw', $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sColor = $oModelObject->getIsNew() ? 'green' : 'white';
        if($oModelObject->getIsNew())
        {
            return '<td class="is_new" style="color:green"><i class="fa fa-flag"></td>';
        }
        else
        {
            return '<td class="" style="color:' . $sColor . '"></td>';
        }
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
