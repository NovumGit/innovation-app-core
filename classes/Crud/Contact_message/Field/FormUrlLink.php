<?php
namespace Crud\Contact_message\Field;

use Core\DeferredAction;
use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\ContactMessage;
use Model\DtcLease\Request\DtcLeaseRequest;
use Model\DtcLease\Request\DtcLeaseRequestQuery;
use Model\Product;

class FormUrlLink extends Field implements IEventField{

    protected $sFieldLabel = 'Open herkomst pagina';

    function getIcon()
    {
        return 'globe';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" title="" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-' . $this->getIcon() .'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }
    function getOverviewValue($mData)
    {
        if(!$mData instanceof ContactMessage)
        {
            throw new LogicException("Expected an instance of Product, got ".get_class($mData));
        }
        $oContactMessage = $mData;
        $aOut = [];
        if($oContactMessage instanceof ContactMessage && $oContactMessage->getFormUrl())
        {
            $aOut[] = '<td class="xx">';
            $aOut[] = "    <a title=\"Bekijk pagina\" href=\"".$oContactMessage->getFormUrl()."\" class=\"btn btn-success br2 btn-xs fs12 d\">";
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
            $aOut[] = '</td>';
        }
        else
        {
            $aOut[] = '<td class="xx">';
            $aOut[] = "    <a href=\"#\" class=\"btn btn-default br2 btn-xs fs12 d\">";
            $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
            $aOut[] = '    </a>';
            $aOut[] = '</td>';
        }
        return join(PHP_EOL, $aOut);
    }
    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Add field should not be there in edit view.");
    }
}
