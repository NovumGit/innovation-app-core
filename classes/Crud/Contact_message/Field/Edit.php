<?php
namespace Crud\Contact_message\Field;

use Crud\Generic\Field\GenericEdit;
use Model\ContactMessage;
use Exception\InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oContactMessage){

        if(!$oContactMessage instanceof ContactMessage)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessage but got '.get_class($oContactMessage));
        }

        return '/contact/edit?id='.$oContactMessage->getId();
    }
}