<?php
namespace Crud\Contact_message\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\ContactMessage as ModelObject;
use Model\ContactMessageStatus;
use Model\ContactMessageStatusQuery;

class StatusId extends Field implements IFilterableLookupField, IDisplayableField {

    protected $sFieldName = 'contact_message_status_id';
    protected $sFieldLabel = 'Status';
    private $sIcon = 'info';
    private $sPlaceHolder = '';


    function getGetter()
    {
        return 'getStatusId';
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getLookups($iSelectedItem = null)
    {
        $aContactMessageStatusses = ContactMessageStatusQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aContactMessageStatusses, 'getName', $iSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        if($iItemId)
        {
            $oContactMessageStatusQuery = ContactMessageStatusQuery::create();
            $oContactMessageStatus = $oContactMessageStatusQuery->findOneById($iItemId);

            if($oContactMessageStatus instanceof ContactMessageStatus)
            {
                return $oContactMessageStatus->getName();
            }
        }
        return null;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oMessageStatus =  $oModelObject->getContactMessageStatus();



        return '<td class="">'.$oMessageStatus->getName().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOptions = $this->getLookups($oModelObject->getStatusId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->getStatusId(), $aOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
