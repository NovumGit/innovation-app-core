<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Contact_message\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\ContactMessageQuery;
use Model\ContactMessage as ModelObject;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;


class CustomerId extends Field implements IFilterableLookupField, IDisplayableField{

    protected $sFieldName = 'customer_id';
    protected $sFieldLabel = 'Klant';
    private $sIcon = 'user';
    private $sGetter = 'getCustomerId';

    function getGetter()
    {
        return $this->sGetter;
    }


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getLookups($mSelectedItem = null)
    {
        $aPriceLevels = ContactMessageQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aPriceLevels, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        $oCustomerQuery = CustomerQuery::create();

        if($iItemId)
        {
            $oCustomer = $oCustomerQuery->findOneById($iItemId);
        }
        if($oCustomer instanceof Customer)
        {
            return $oCustomer->getFullName();
        }
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $this->sIcon, 'Maak een keuze');
    }
}
