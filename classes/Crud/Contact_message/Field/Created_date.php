<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Contact_message\Field;

use Core\Config;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\ContactMessage as ModelObject;


class Created_date extends Field implements IFilterableField, IEditableField, IDisplayableField{

    protected $sFieldName = 'created_date';
    protected $sFieldLabel = 'Aangemaakt op';
    private $sGetter = 'getCreatedDate';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'date';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oCreatedDate  = $oModelObject->getCreatedDate();
        $sCreatedDate = '';
        if($oCreatedDate instanceof \DateTime)
        {
            $sCreatedDate  = $oCreatedDate->format(Config::getDateFormat());
        }

        return '<td class="">'.$sCreatedDate.'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sCreatedDate = null;
        $oCreatedDate  = $oModelObject->getCreatedDate();
        if($oCreatedDate instanceof \DateTime)
        {
            $sCreatedDate  = $oCreatedDate->format(Config::getDateFormat());
        }

        return $this->editDatePicker($this->getTranslatedTitle(), $this->sFieldName, $sCreatedDate );
    }
}
