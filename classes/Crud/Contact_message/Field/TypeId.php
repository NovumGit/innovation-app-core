<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Contact_message\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableLookupField;
use Exception\InvalidArgumentException;
use Model\ContactMessage as ModelObject;
use Model\ContactMessageType;
use Model\ContactMessageTypeQuery;

class TypeId extends Field implements IFilterableLookupField, IDisplayableField {

    protected $sFieldName = 'contact_message_type_id';
    protected $sFieldLabel = 'Bericht type';
    private $sIcon = 'info';
    private $sPlaceHolder = '';
    private $sGetter = 'getTypeId';


    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getLookups($iSelectedItem = null)
    {
        $aContactMessageTypes = ContactMessageTypeQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aContactMessageTypes, 'getName', $iSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        if($iItemId)
        {
            $oContactMessageTypeQuery = ContactMessageTypeQuery::create();
            $oContactMessageType = $oContactMessageTypeQuery->findOneById($iItemId);

            if($oContactMessageType instanceof ContactMessageType)
            {
                return $oContactMessageType->getName();
            }
        }
        return null;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $oMessageType = $oModelObject->getContactMessageType();

        $sMessageType = '';
        if($oMessageType instanceof ContactMessageType)
        {
            $sMessageType = $oMessageType->getName();
        }

        return '<td class="">' . $sMessageType . '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOptions = $this->getLookups($oModelObject->getTypeId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
