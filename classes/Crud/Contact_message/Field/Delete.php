<?php
namespace Crud\Contact_message\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\ContactMessage;
use InvalidArgumentException;

class Delete extends GenericDelete{


    function getDeleteUrl($oContactMessage){

        if(!$oContactMessage instanceof ContactMessage)
        {
            throw new InvalidArgumentException('Expected an instance of ContactMessage but got '.get_class($oContactMessage));
        }

        return '/contact/edit?_do=Delete&id='.$oContactMessage->getId();
    }

    function getUnDeleteUrl($oContactMessage)
    {
        throw new LogicException("ContactMessage delivery time cannot be undeleted");
    }
}