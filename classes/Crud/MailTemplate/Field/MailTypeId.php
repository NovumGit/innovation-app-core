<?php
namespace Crud\MailTemplate\Field;

use Crud\MailTemplate\Field\Base\MailTypeId as BaseMailTypeId;

/**
 * Skeleton subclass for representing mail_type_id field from the mail_template table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class MailTypeId extends BaseMailTypeId
{
}
