<?php
namespace Crud\MailTemplate\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\MailTemplate\ICollectionField;

/**
 * Base class that represents the 'email_bcc' crud field from the 'mail_template' table.
 * This class is auto generated and should not be modified.
 */
abstract class EmailBcc extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'email_bcc';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getEmailBcc';
	protected $sFqModelClassname = '\\\Model\Cms\Mail\MailTemplate';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
