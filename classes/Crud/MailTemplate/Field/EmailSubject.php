<?php
namespace Crud\MailTemplate\Field;

use Crud\MailTemplate\Field\Base\EmailSubject as BaseEmailSubject;

/**
 * Skeleton subclass for representing email_subject field from the mail_template table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class EmailSubject extends BaseEmailSubject
{
}
