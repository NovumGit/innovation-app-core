<?php
namespace Crud\MailTemplate\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\MailTemplate\ICollectionField as MailTemplateField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\MailTemplate\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param MailTemplateField[] $aFields */
	private array $aFields = [];


	/**
	 * @param MailTemplateField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof MailTemplateField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(MailTemplateField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): MailTemplateField
	{
		return current($this->aFields);
	}
}
