<?php
namespace Crud\MailTemplate\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailTemplate\FieldIterator;
use Crud\MailTemplate\Field\Content;
use Crud\MailTemplate\Field\EmailBcc;
use Crud\MailTemplate\Field\EmailCc;
use Crud\MailTemplate\Field\EmailFrom;
use Crud\MailTemplate\Field\EmailSubject;
use Crud\MailTemplate\Field\MailTypeId;
use Crud\MailTemplate\Field\SiteId;
use Exception\LogicException;
use Model\Cms\Mail\MailTemplate;
use Model\Cms\Mail\MailTemplateQuery;
use Model\Cms\Mail\Map\MailTemplateTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailTemplate instead if you need to override or add functionality.
 */
abstract class CrudMailTemplateManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailTemplateQuery::create();
	}


	public function getTableMap(): MailTemplateTableMap
	{
		return new MailTemplateTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailTemplate";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mail_template toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mail_template aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['EmailFrom', 'EmailCc', 'EmailBcc', 'EmailSubject', 'Content', 'SiteId', 'MailTypeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['EmailFrom', 'EmailCc', 'EmailBcc', 'EmailSubject', 'Content', 'SiteId', 'MailTypeId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailTemplate
	 */
	public function getModel(array $aData = null): MailTemplate
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailTemplateQuery = MailTemplateQuery::create();
		     $oMailTemplate = $oMailTemplateQuery->findOneById($aData['id']);
		     if (!$oMailTemplate instanceof MailTemplate) {
		         throw new LogicException("MailTemplate should be an instance of MailTemplate but got something else." . __METHOD__);
		     }
		     $oMailTemplate = $this->fillVo($aData, $oMailTemplate);
		}
		else {
		     $oMailTemplate = new MailTemplate();
		     if (!empty($aData)) {
		         $oMailTemplate = $this->fillVo($aData, $oMailTemplate);
		     }
		}
		return $oMailTemplate;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailTemplate
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailTemplate
	{
		$oMailTemplate = $this->getModel($aData);


		 if(!empty($oMailTemplate))
		 {
		     $oMailTemplate = $this->fillVo($aData, $oMailTemplate);
		     $oMailTemplate->save();
		 }
		return $oMailTemplate;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailTemplate $oModel
	 * @return MailTemplate
	 */
	protected function fillVo(array $aData, MailTemplate $oModel): MailTemplate
	{
		if(isset($aData['email_from'])) {
		     $oField = new EmailFrom();
		     $mValue = $oField->sanitize($aData['email_from']);
		     $oModel->setEmailFrom($mValue);
		}
		if(isset($aData['email_cc'])) {
		     $oField = new EmailCc();
		     $mValue = $oField->sanitize($aData['email_cc']);
		     $oModel->setEmailCc($mValue);
		}
		if(isset($aData['email_bcc'])) {
		     $oField = new EmailBcc();
		     $mValue = $oField->sanitize($aData['email_bcc']);
		     $oModel->setEmailBcc($mValue);
		}
		if(isset($aData['email_subject'])) {
		     $oField = new EmailSubject();
		     $mValue = $oField->sanitize($aData['email_subject']);
		     $oModel->setEmailSubject($mValue);
		}
		if(isset($aData['content'])) {
		     $oField = new Content();
		     $mValue = $oField->sanitize($aData['content']);
		     $oModel->setContent($mValue);
		}
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['mail_type_id'])) {
		     $oField = new MailTypeId();
		     $mValue = $oField->sanitize($aData['mail_type_id']);
		     $oModel->setMailTypeId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
