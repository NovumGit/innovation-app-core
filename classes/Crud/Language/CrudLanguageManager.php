<?php
namespace Crud\Language;

use Core\QueryMapper;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Exception\LogicException;

class CrudLanguageManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'taal';
    }
    function getOverviewUrl():string
    {
        return '/admin/language_overview';
    }
    function getCreateNewUrl():string
    {
        return '/admin/language_edit';
    }
    function getNewFormTitle():string{
        return 'Taal toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Taal wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'IsDefaultCms',
            'FlagIcon',
            'Description',
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Description',
            'LocaleCode',
            'IsDefaultCms',
            'IsDefaultWebshop',
            'IsEnabledCms',
            'IsEnabledWebshop',
            'FlagIcon',
        ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oLanguageQuery = new LanguageQuery();

            $oLanguage = $oLanguageQuery->findOneById($aData['id']);

            if(!$oLanguage instanceof Language){
                throw new LogicException("Language should be an instance of Language but got ".get_class($oLanguage)." in ".__METHOD__);
            }
        }
        else
        {
            $oLanguage = new Language();

            if(!empty($aData))
            {
                $oLanguage = $this->fillVo($aData, $oLanguage);
            }
        }

        return $oLanguage;
    }

    function store(array $aData = null)
    {
        $oLanguage = $this->getModel($aData);

        if(!empty($oLanguage))
        {
            $oLanguage = $this->fillVo($aData, $oLanguage);

            $oLanguage->save();
        }
        return $oLanguage;
    }

    private function fillVo($aData, Language $oLanguage)
    {
        if(isset($aData['description'])){$oLanguage->setDescription($aData['description']);}
        if(isset($aData['locale_code'])){$oLanguage->setLocaleCode($aData['locale_code']);}
        if(isset($aData['flag_icon'])){$oLanguage->setFlagIcon($aData['flag_icon']);}
        if(isset($aData['is_enabled_cms'])){$oLanguage->setIsEnabledCms($aData['is_enabled_cms']);}
        if(isset($aData['is_enabled_webshop'])){$oLanguage->setIsEnabledWebshop($aData['is_enabled_webshop']);}

        // Als hij nog niet bestond en hij wordt op 1 gezet, de rest op 0 zetten.
        if(isset($aData['is_default_webshop']) && $aData['is_default_webshop'] == 1)
        {
            $sAddWhere = $oLanguage->getId() ? ' WHERE id != '.$oLanguage->getId() : '';
            QueryMapper::query("UPDATE mt_language SET is_default_webshop = 0".$sAddWhere);
            $oLanguage->setIsDefaultWebshop(true);
        }
        else
        {
            $oLanguage->setIsDefaultWebshop(false);
        }
        if(isset($aData['is_default_cms']) && $aData['is_default_cms'] == 1)
        {
            $sAddWhere = $oLanguage->getId() ? ' WHERE id != '.$oLanguage->getId() : '';
            QueryMapper::query("UPDATE mt_language SET is_default_cms = 0".$sAddWhere);
            $oLanguage->setIsDefaultCms(true);
        }
        else
        {
            $oLanguage->setIsDefaultCms(false);
        }
        return $oLanguage;
    }
}
