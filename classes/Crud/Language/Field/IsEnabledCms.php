<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Language\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Setting\MasterTable\Language as ModelObject;


class IsEnabledCms extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'is_enabled_cms';
    protected $sFieldLabel = 'Beschikbaar (in cms)';
    private $sGetter = 'getIsEnabledCms';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'boolean';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $sChecked = $oModelObject->{$this->sGetter}() ? 'checked' : '';
        $aResponse = [];
        $aResponse[] = '<td class="text-danger">';
        $aResponse[] = '    <label class="option block mn">';
        $aResponse[] = '        <input class="is_enabled_cms" type="checkbox" '.$sChecked.' name="enabled_cms['.$oModelObject->getId().']" value="'.$oModelObject->getId().'">';
        $aResponse[] = '        <span class="checkbox mn"></span>';
        $aResponse[] = '    </label>';
        $aResponse[] = '</td>';

        return join(PHP_EOL, $aResponse);
    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadonly);
    }
}
