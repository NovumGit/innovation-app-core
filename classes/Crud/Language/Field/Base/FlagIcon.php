<?php
namespace Crud\Language\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Language\ICollectionField;

/**
 * Base class that represents the 'flag_icon' crud field from the 'mt_language' table.
 * This class is auto generated and should not be modified.
 */
abstract class FlagIcon extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'flag_icon';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFlagIcon';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\Language';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
