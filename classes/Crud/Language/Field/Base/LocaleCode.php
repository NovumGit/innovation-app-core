<?php
namespace Crud\Language\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Language\ICollectionField;

/**
 * Base class that represents the 'locale_code' crud field from the 'mt_language' table.
 * This class is auto generated and should not be modified.
 */
abstract class LocaleCode extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'locale_code';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLocaleCode';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\Language';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
