<?php
namespace Crud\Language\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\Language;
use InvalidArgumentException;
use Crud\Language\CrudLanguageManager as ManagerObject;

class Delete extends GenericDelete{

    function getDeleteUrl($oLanguage){
        if(!$oLanguage instanceof Language)
        {
            throw new InvalidArgumentException('Expected an instance of Language but got '.get_class($oLanguage));
        }
        $oManager = new ManagerObject();
        return $oManager->getCreateNewUrl().'?_do=Delete&language_id='.$oLanguage->getId();
    }

    function getUnDeleteUrl($oLanguage){
        if(!$oLanguage instanceof Language)
        {
            throw new InvalidArgumentException('Expected an instance of Language but got '.get_class($oLanguage));
        }
        $oManager = new ManagerObject();
        return $oManager->getCreateNewUrl().'?_do=Undelete&language_id='.$oLanguage->getId();
    }
}