<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Language\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Setting\MasterTable\Language as ModelObject;

class FlagIcon extends Field implements IFilterableLookupField{

    protected $sFieldName = 'flag_icon';
    protected $sFieldLabel = 'Vlag icoon';
    private $sIcon = 'flag';
    private $sGetter = 'getFlagIcon';

    function getFieldName()
    {
        return $this->getTranslatedTitle();
    }

    function getLookups($mSelectedItem = null)
    {
        $sFlagPath = '../admin_public_html/img/flags/flags-iso/flat/24/';
        $aFlagFiles = glob("$sFlagPath*");

        $aDropdownOptions = [];

        foreach($aFlagFiles as $sFlagFile)
        {
            $sFile = basename($sFlagFile);
            $sFile = str_replace('.png', '', $sFile);
            $aDropdownOptions[] = [
                'id' => $sFile,
                'selected' => true ,
                'label' => $sFile
            ];
        }

        return $aDropdownOptions;
    }
    function getVisibleValue($iItemId)
    {
        return $iItemId;
    }

    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aResponse = [];
        $aResponse[] = '<td class="w120">';
        $aResponse[] = '    <img class="img-responsive mw40 ib mr10" title="user" src="'.$oModelObject->getFlagIconPath(48).'">';
        $aResponse[] = '</td>';

        return join(PHP_EOL, $aResponse);

    }

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups($oModelObject->{$this->sGetter}());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
