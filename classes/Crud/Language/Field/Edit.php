<?php
namespace Crud\Language\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\Language;
use Exception\InvalidArgumentException;
use Crud\Language\CrudLanguageManager as ManagerObject;

class Edit extends GenericEdit{

    function getEditUrl($oLanguage){

        if(!$oLanguage instanceof Language)
        {
            throw new InvalidArgumentException('Expected an instance of Language but got '.get_class($oLanguage));
        }
        $oManager = new ManagerObject();
        return $oManager->getCreateNewUrl().'?language_id='.$oLanguage->getId();
    }
}