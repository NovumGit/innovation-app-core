<?php
namespace Crud\Language\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Language\FieldIterator;
use Crud\Language\Field\Description;
use Crud\Language\Field\FlagIcon;
use Crud\Language\Field\IsDefaultCms;
use Crud\Language\Field\IsDefaultWebshop;
use Crud\Language\Field\IsEnabledCms;
use Crud\Language\Field\IsEnabledWebshop;
use Crud\Language\Field\ItemDeleted;
use Crud\Language\Field\LocaleCode;
use Crud\Language\Field\ShopUrlPrefix;
use Exception\LogicException;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Map\LanguageTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Language instead if you need to override or add functionality.
 */
abstract class CrudLanguageManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return LanguageQuery::create();
	}


	public function getTableMap(): LanguageTableMap
	{
		return new LanguageTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Language";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_language toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_language aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemDeleted', 'Description', 'LocaleCode', 'IsEnabledCms', 'IsEnabledWebshop', 'IsDefaultCms', 'IsDefaultWebshop', 'FlagIcon', 'ShopUrlPrefix'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemDeleted', 'Description', 'LocaleCode', 'IsEnabledCms', 'IsEnabledWebshop', 'IsDefaultCms', 'IsDefaultWebshop', 'FlagIcon', 'ShopUrlPrefix'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Language
	 */
	public function getModel(array $aData = null): Language
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oLanguageQuery = LanguageQuery::create();
		     $oLanguage = $oLanguageQuery->findOneById($aData['id']);
		     if (!$oLanguage instanceof Language) {
		         throw new LogicException("Language should be an instance of Language but got something else." . __METHOD__);
		     }
		     $oLanguage = $this->fillVo($aData, $oLanguage);
		}
		else {
		     $oLanguage = new Language();
		     if (!empty($aData)) {
		         $oLanguage = $this->fillVo($aData, $oLanguage);
		     }
		}
		return $oLanguage;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Language
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Language
	{
		$oLanguage = $this->getModel($aData);


		 if(!empty($oLanguage))
		 {
		     $oLanguage = $this->fillVo($aData, $oLanguage);
		     $oLanguage->save();
		 }
		return $oLanguage;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Language $oModel
	 * @return Language
	 */
	protected function fillVo(array $aData, Language $oModel): Language
	{
		if(isset($aData['is_deleted'])) {
		     $oField = new ItemDeleted();
		     $mValue = $oField->sanitize($aData['is_deleted']);
		     $oModel->setItemDeleted($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['locale_code'])) {
		     $oField = new LocaleCode();
		     $mValue = $oField->sanitize($aData['locale_code']);
		     $oModel->setLocaleCode($mValue);
		}
		if(isset($aData['is_enabled_cms'])) {
		     $oField = new IsEnabledCms();
		     $mValue = $oField->sanitize($aData['is_enabled_cms']);
		     $oModel->setIsEnabledCms($mValue);
		}
		if(isset($aData['is_enabled_webshop'])) {
		     $oField = new IsEnabledWebshop();
		     $mValue = $oField->sanitize($aData['is_enabled_webshop']);
		     $oModel->setIsEnabledWebshop($mValue);
		}
		if(isset($aData['is_default_cms'])) {
		     $oField = new IsDefaultCms();
		     $mValue = $oField->sanitize($aData['is_default_cms']);
		     $oModel->setIsDefaultCms($mValue);
		}
		if(isset($aData['is_default_webshop'])) {
		     $oField = new IsDefaultWebshop();
		     $mValue = $oField->sanitize($aData['is_default_webshop']);
		     $oModel->setIsDefaultWebshop($mValue);
		}
		if(isset($aData['flag_icon'])) {
		     $oField = new FlagIcon();
		     $mValue = $oField->sanitize($aData['flag_icon']);
		     $oModel->setFlagIcon($mValue);
		}
		if(isset($aData['shop_url_prefix'])) {
		     $oField = new ShopUrlPrefix();
		     $mValue = $oField->sanitize($aData['shop_url_prefix']);
		     $oModel->setShopUrlPrefix($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
