<?php
namespace Crud\Language\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Language\ICollectionField as LanguageField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Language\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param LanguageField[] $aFields */
	private array $aFields = [];


	/**
	 * @param LanguageField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof LanguageField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(LanguageField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): LanguageField
	{
		return current($this->aFields);
	}
}
