<?php
namespace Crud\Link_block_cms_module;

use Crud\Link_blockManager;

class CrudLink_block_cms_moduleManager extends Link_blockManager  {

    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'General'
        ];
    }
}
