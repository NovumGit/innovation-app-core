<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class HolidayMessage extends Link {

    protected $sFieldLabel = 'Holiday message';

    function getLinkUrl()
    {
        return '/cms/holiday_message/edit';
    }
}