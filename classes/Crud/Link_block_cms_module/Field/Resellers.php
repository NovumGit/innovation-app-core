<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Resellers extends Link {

    protected $sFieldLabel = 'Reseller list';

    function getLinkUrl()
    {
        return '/cms/reseller/overview';
    }
}