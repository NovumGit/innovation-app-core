<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Usp extends Link {

    protected $sFieldLabel = 'Unique selling points';

    function getLinkUrl()
    {
        return '/cms/usp/overview';
    }
}