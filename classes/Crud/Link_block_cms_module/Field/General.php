<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class General extends Link {

    protected $sFieldLabel = 'Algemeen';

    function getLinkUrl()
    {
        return '/cms/general';
    }
}