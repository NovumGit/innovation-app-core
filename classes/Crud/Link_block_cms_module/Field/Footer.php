<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Footer extends Link{

    protected $sFieldLabel = 'Footer';

    function getLinkUrl()
    {
        return '/cms/footer/edit';
    }
}