<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Blog extends Link {

    protected $sFieldLabel = 'Blog';

    function getLinkUrl()
    {
        return '/cms/blog/overview';
    }
}