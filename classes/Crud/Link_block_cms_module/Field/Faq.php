<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Faq extends Link {

    protected $sFieldLabel = 'Faq';

    function getLinkUrl()
    {
        return '/cms/faq/overview';
    }
}