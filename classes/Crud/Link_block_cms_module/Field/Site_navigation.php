<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Site_navigation extends Link {

    protected $sFieldLabel = 'Website navigatie';

    function getLinkUrl()
    {
        return '/cms/site_navigation/overview';
    }
}   