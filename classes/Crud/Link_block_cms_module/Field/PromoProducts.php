<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class PromoProducts extends Link {

    protected $sFieldLabel = 'Promotions';

    function getLinkUrl()
    {
        return '/cms/promoproducts/overview';
    }
}