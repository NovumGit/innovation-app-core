<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Banner extends Link {

    protected $sFieldLabel = 'Banner';

    function getLinkUrl()
    {
        return '/cms/banner/overview';
    }
}