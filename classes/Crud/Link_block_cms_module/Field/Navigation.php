<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Navigation extends Link {

    protected $sFieldLabel = 'Navigatie';

    function getLinkUrl()
    {
        return '/cms/navigation/overview';
    }
}