<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class OpeningHours extends Link {

    protected $sFieldLabel = 'Openingstijden';

    function getLinkUrl()
    {
        return '/cms/opening_hours/edit';
    }
}