<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class MediaUploader extends Link {

    protected $sFieldLabel = 'Media uploader';

    function getLinkUrl()
    {
        return '/cms/media/structure';
    }
}