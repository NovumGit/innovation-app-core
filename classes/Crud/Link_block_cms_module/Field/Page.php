<?php
namespace Crud\Link_block_cms_module\Field;

use Crud\Link;

class Page extends Link {

    protected $sFieldLabel = 'CMS';

    function getLinkUrl()
    {
        return '/cms/page/overview';
    }
}