<?php
namespace Crud\Sale_order_item\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use model\Sale\SaleOrderItem as ModelObject;
use model\Setting\MasterTable\VatQuery;

class VatPercentage extends Field implements IFilterableLookupField, IDisplayableField {

    private $sFieldName = 'vat_percentage';
    protected $sFieldLabel = 'BTW percentage';
    private $sIcon = 'frown-o';
    private $sGetter = 'getVatPercentage';

    function getFieldName()
    {
        return 'vat_percentage';
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getLookups($mSelectedItem = null)
    {
        $aVatPercentages = VatQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aVatPercentages, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        $oVatQuery = VatQuery::create()->findOneById($iItemId);
        if($oVatQuery)
        {
            return $oVatQuery->getName();
        }

        return null;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() {
        return false;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($oModelObject, $bReadOnly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadOnly, $this->sIcon, 'Maak een keuze');
    }
}
