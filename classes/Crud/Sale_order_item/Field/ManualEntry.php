<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use model\Sale\SaleOrderItem as ModelObject;


class ManualEntry extends Field implements IFilterableField, IEditableField, IDisplayableField {

    private $sFieldName = 'manual_entry';
    protected $sFieldLabel = 'Handmatige invoer';
    private $sGetter = 'getManualEntry';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'boolean';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($oModelObject, $bReadOnly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $bReadOnly);
    }
}
