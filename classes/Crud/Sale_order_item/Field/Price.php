<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use model\Sale\SaleOrderItem as ModelObject;

class Price extends Field implements IFilterableField, IEditableField, IDisplayableField {

    private $sFieldName = 'price';
    protected $sFieldLabel = 'Prijs';
    private $sIcon = 'money';
    private $sPlaceHolder = '';
    private $sGetter = 'getPrice';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'number';
    }

    function hasValidations() {
        return false;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $fPrice = $oModelObject->{$this->sGetter}();
        $fPrice = str_replace('.', ',', $fPrice);
        return '<td class="">'.$fPrice.'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($oModelObject, $bReadOnly = false)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $fPrice = $oModelObject->{$this->sGetter}();
        $fPrice = str_replace('.', ',', $fPrice);
        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $fPrice, $this->sPlaceHolder, $this->sIcon, $bReadOnly);
    }
}
