<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\IFilterableField;
use Crud\IEditableField;
use Exception\InvalidArgumentException;
use model\Sale\SaleOrderItem as ModelObject;
use model\Sale\SaleOrderQuery;


class SaleOrderId extends Field implements IFilterableField, IEditableField, IDisplayableField {

    private $sFieldName = 'sale_order_id';
    protected $sFieldLabel = 'Order id';
    private $sIcon = 'bolt';
    private $sGetter = 'getSaleOrderId';

    function getGetter()
    {
        return $this->sGetter;
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function getLookups($mSelectedItem = null)
    {
        $aAllSaleOrder = SaleOrderQuery::create()->orderById()->find();
        $aOptions = Utils::makeSelectOptions($aAllSaleOrder, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function hasValidations() {
        return false;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($oModelObject, $bReadOnly = false)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadOnly, $this->sIcon, 'Maak een keuze');
    }
}
