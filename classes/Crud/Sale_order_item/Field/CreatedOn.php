<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Sale_order_item\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use model\Sale\SaleOrderItem as ModelObject;


class CreatedOn extends Field implements IFilterableField, IEditableField, IDisplayableField {

    private $sFieldName = 'created_on';
    protected $sFieldLabel = 'Aangemaakt op';
    private $sIcon = 'calendar';
    private $sPlaceHolder = '';
    private $sGetter = 'getCreatedOn';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle()
    {
        return $this->sFieldLabel;
    }
    function getEditHtml($oModelObject, $bReadOnly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadOnly);
    }
}
