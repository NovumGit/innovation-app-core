<?php
$aCrudFields = glob('./Field/*');

$sNamespace = 'Site';
$sModelObject = 'model\Site\Site';
$sQueryObject = 'model\Site\SiteQuery';

$sCrudTemplate = file_get_contents('../crudfield.template.php');
$sCrudTemplate = str_replace('{{ namespace }}', $sNamespace, $sCrudTemplate);
$sCrudTemplate = str_replace('{{ modelobject }}', $sModelObject, $sCrudTemplate);


function fromCamelCase($input) {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
        $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
}

$handle = fopen ("php://stdin","r");

foreach($aCrudFields as $sCrudField)
{
    $sCrudFieldTemplate = $sCrudTemplate;
    $sBaseName = basename($sCrudField);
    $sClassName = str_replace('.php', '', $sBaseName);

    $sFieldName = fromCamelCase($sClassName);

    if($sBaseName == 'Delete.php' || $sBaseName == 'Edit.php' || $sBaseName == 'Id.php' )
    {
        echo "Skipped $sBaseName".PHP_EOL;
        continue;
    }
    $sCurrentFileContents = file_get_contents($sCrudField);

    if(strpos($sCurrentFileContents, 'class '.str_replace('.php', '', $sBaseName)))
    {
        echo "Er zit al een klassedefinitie in bestand $sBaseName, we slaan hem over.".PHP_EOL;
        continue;
    }

    $sCrudFieldTemplate = str_replace('{{ classname }}', $sClassName, $sCrudFieldTemplate);

    echo "Configreren van $sBaseName ".PHP_EOL;

    echo "Geef de database veldnaam: $sFieldName";
    $sResponse = trim(fgets($handle));

    if(empty($sResponse))
    {
        $sResponse = $sFieldName;
    }
    $sCrudFieldTemplate = str_replace('{{ fieldname }}', $sResponse, $sCrudFieldTemplate);



    $sGetter = str_replace('.php', '', 'get'.$sClassName);
    echo "Geef veld getter, enter voor: $sGetter";
    $sResponse = trim(fgets($handle));

    if(!empty(trim($sResponse)))
    {
        $sGetter = $sResponse;
    }

    $sCrudFieldTemplate = str_replace('{{ getter }}', $sGetter, $sCrudFieldTemplate);

    echo "Geef veld label: ";
    $sResponse = trim(fgets($handle));
    $sCrudFieldTemplate = str_replace('{{ fieldlabel }}', $sResponse, $sCrudFieldTemplate);
    $sDataType = null;
    while(!in_array($sResponse , ['boolean', 'number', 'string', 'lookup']))
    {
        echo "Geef veld datatype (boolean, number, string, lookup): ";
        $sResponse = trim(fgets($handle));
        $sCrudFieldTemplate = str_replace('{{ fieldplaceholder }}', $sResponse, $sCrudFieldTemplate);
        $sDataType = $sResponse;
        if($sResponse == 'lookup')
        {
            $sCrudFieldTemplate = str_replace('{{ crudinterface }}' , 'InterfaceFilterableLookupField', $sCrudFieldTemplate);

            $sLookupMethods = "
                function getLookups(\$mSelectedItem = null)
                {
                    \$aPriceLevels = ".$sQueryObject."::create()->orderByName()->find();
                    \$aOptions = Utils::makeSelectOptions(\$aPriceLevels, 'getName', \$mSelectedItem);
                    return \$aOptions;
                }
                function getVisibleValue(\$iItemId)
                {
                    return PriceLevelQuery::create()->findOneById(\$iItemId)->getName();;
                }
            ";

        }
        else
        {
            $sLookupMethods = "";
            $sCrudFieldTemplate = str_replace('{{ crudinterface }}' , 'IFilterableField', $sCrudFieldTemplate);
        }
        $sCrudFieldTemplate = str_replace('{{ lookup_methods }}', $sLookupMethods, $sCrudFieldTemplate);
        $sCrudFieldTemplate = str_replace('{{ datatype }}', $sDataType, $sCrudFieldTemplate);

    }

    if($sDataType == 'boolean')
    {
        $sEditHtmlMethod = "return \$this->editBooleanField(\$this->sFieldLabel, \$this->sFieldName, \$oModelObject->{\$this->sGetter}());";

    }
    else if($sDataType == 'lookup')
    {
        $sEditHtmlMethod = "\$aOptions = \$this->getLookups();";
        $sEditHtmlMethod .= "return \$this->editLookupField(\$this->sFieldLabel, \$this->sFieldName, \$oModelObject->{\$this->sGetter}(), \$aOptions, \$this->sIcon, 'Maak een keuze');";
    }
    else
    {
        $sEditHtmlMethod = "return \$this->editTextField(\$this->sFieldLabel, \$this->sFieldName, \$oModelObject->{\$this->sGetter}(), \$this->sPlaceHolder, \$this->sIcon);";
    }
    $sCrudFieldTemplate = str_replace('{{ edit_html_method }}', $sEditHtmlMethod, $sCrudFieldTemplate);

    if($sDataType != 'boolean')
    {
        echo "Geef veld icon: ";
        $sResponse = trim(fgets($handle));
        $sCrudFieldTemplate = str_replace('{{ fieldicon }}', $sResponse, $sCrudFieldTemplate);

        echo "Geef veld placeholder: ";
        $sResponse = trim(fgets($handle));
        $sCrudFieldTemplate = str_replace('{{ fieldplaceholder }}', $sResponse, $sCrudFieldTemplate);
    }

    file_put_contents($sCrudField, $sCrudFieldTemplate);

    echo  "Crudveld weg geschreven naar $sCrudField".PHP_EOL;

    echo basename($sCrudField).PHP_EOL;
    echo $sCrudField.PHP_EOL;

}


