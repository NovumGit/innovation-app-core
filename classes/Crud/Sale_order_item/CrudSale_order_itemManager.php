<?php
namespace Crud\Sale_order_item;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use model\Sale\SaleOrderItem as OrderItem;
use model\Sale\SaleOrderItemQuery;
use LogicException;

class CrudSale_order_itemManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'sale_order_item';
    }
    function getNewFormTitle():string{
        return 'Orderregel toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Orderregel bewerken';
    }
    function getOverviewUrl():string
    {
        return '/order/item/overview';
    }
    function getCreateNewUrl():string
    {
        return '/order/item/edit';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'SaleOrderId',
            'Description',
            'Quantity',
            'Price',
            'VatPercentage'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'SaleOrderId',
            'Description',
            'Quantity',
            'Price',
            'VatPercentage'
        ];
    }

    /**
     * @param $aData
     * @return OrderItem
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SaleOrderItemQuery();

            $oOrderItem = $oQuery->findOneById($aData['id']);

            if(!$oOrderItem instanceof OrderItem)
            {
                throw new LogicException("OrderItem should be an instance of Product but got ".get_class($oOrderItem)." in ".__METHOD__);
            }
        }
        else
        {
            $oOrderItem = new OrderItem();
            $oOrderItem = $this->fillVo($oOrderItem, $aData);
        }
        return $oOrderItem;
    }

    function store(array $aData = null)
    {
        $oOrderItem = $this->getModel($aData);

        if(!empty($oOrderItem))
        {

            $oOrderItem = $this->fillVo($oOrderItem, $aData);
            $oOrderItem->save();
        }
        return $oOrderItem;
    }
    function fillVo(OrderItem $oOrderItem, $aData)
    {
        if(isset($aData['id'])){
            $oOrderItem->setId($aData['id']);
        }
        if(isset($aData['created_on'])){
            $oOrderItem->setCreatedOn($aData['created_on']);
        }
        if(isset($aData['sale_order_id'])){
            $oOrderItem->setSaleOrderId($aData['sale_order_id']);
        }
        if(isset($aData['description'])){
            $oOrderItem->setDescription($aData['description']);
        }
        if(isset($aData['quantity'])){
            $oOrderItem->setQuantity($aData['quantity']);
        }
        if(isset($aData['price'])){
            $aData['price'] = str_replace('.', ',', $aData['price']);
            $oOrderItem->setPrice($aData['price']);
        }
        if(isset($aData['vat_percentage'])){
            $oOrderItem->setVatPercentage($aData['vat_percentage']);
        }

        return $oOrderItem;
    }
}
