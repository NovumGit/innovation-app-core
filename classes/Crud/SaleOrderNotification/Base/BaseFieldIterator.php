<?php
namespace Crud\SaleOrderNotification\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SaleOrderNotification\ICollectionField as SaleOrderNotificationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SaleOrderNotification\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SaleOrderNotificationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SaleOrderNotificationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SaleOrderNotificationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SaleOrderNotificationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SaleOrderNotificationField
	{
		return current($this->aFields);
	}
}
