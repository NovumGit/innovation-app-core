<?php
namespace Crud\SaleOrderNotification\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SaleOrderNotification\FieldIterator;
use Crud\SaleOrderNotification\Field\Content;
use Crud\SaleOrderNotification\Field\ContentCompressed;
use Crud\SaleOrderNotification\Field\DateSent;
use Crud\SaleOrderNotification\Field\FromEmail;
use Crud\SaleOrderNotification\Field\SaleOrderId;
use Crud\SaleOrderNotification\Field\SaleOrderNotificationTypeId;
use Crud\SaleOrderNotification\Field\SenderIp;
use Crud\SaleOrderNotification\Field\Title;
use Crud\SaleOrderNotification\Field\ToEmail;
use Exception\LogicException;
use Model\SaleOrderNotification\Map\SaleOrderNotificationTableMap;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SaleOrderNotification instead if you need to override or add functionality.
 */
abstract class CrudSaleOrderNotificationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SaleOrderNotificationQuery::create();
	}


	public function getTableMap(): SaleOrderNotificationTableMap
	{
		return new SaleOrderNotificationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SaleOrderNotification";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_notification toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_notification aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderId', 'SaleOrderNotificationTypeId', 'FromEmail', 'ToEmail', 'Content', 'ContentCompressed', 'Title', 'DateSent', 'SenderIp'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderId', 'SaleOrderNotificationTypeId', 'FromEmail', 'ToEmail', 'Content', 'ContentCompressed', 'Title', 'DateSent', 'SenderIp'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SaleOrderNotification
	 */
	public function getModel(array $aData = null): SaleOrderNotification
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSaleOrderNotificationQuery = SaleOrderNotificationQuery::create();
		     $oSaleOrderNotification = $oSaleOrderNotificationQuery->findOneById($aData['id']);
		     if (!$oSaleOrderNotification instanceof SaleOrderNotification) {
		         throw new LogicException("SaleOrderNotification should be an instance of SaleOrderNotification but got something else." . __METHOD__);
		     }
		     $oSaleOrderNotification = $this->fillVo($aData, $oSaleOrderNotification);
		}
		else {
		     $oSaleOrderNotification = new SaleOrderNotification();
		     if (!empty($aData)) {
		         $oSaleOrderNotification = $this->fillVo($aData, $oSaleOrderNotification);
		     }
		}
		return $oSaleOrderNotification;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SaleOrderNotification
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SaleOrderNotification
	{
		$oSaleOrderNotification = $this->getModel($aData);


		 if(!empty($oSaleOrderNotification))
		 {
		     $oSaleOrderNotification = $this->fillVo($aData, $oSaleOrderNotification);
		     $oSaleOrderNotification->save();
		 }
		return $oSaleOrderNotification;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SaleOrderNotification $oModel
	 * @return SaleOrderNotification
	 */
	protected function fillVo(array $aData, SaleOrderNotification $oModel): SaleOrderNotification
	{
		if(isset($aData['sale_order_id'])) {
		     $oField = new SaleOrderId();
		     $mValue = $oField->sanitize($aData['sale_order_id']);
		     $oModel->setSaleOrderId($mValue);
		}
		if(isset($aData['sale_order_notification_type_id'])) {
		     $oField = new SaleOrderNotificationTypeId();
		     $mValue = $oField->sanitize($aData['sale_order_notification_type_id']);
		     $oModel->setSaleOrderNotificationTypeId($mValue);
		}
		if(isset($aData['from_email'])) {
		     $oField = new FromEmail();
		     $mValue = $oField->sanitize($aData['from_email']);
		     $oModel->setFromEmail($mValue);
		}
		if(isset($aData['to_email'])) {
		     $oField = new ToEmail();
		     $mValue = $oField->sanitize($aData['to_email']);
		     $oModel->setToEmail($mValue);
		}
		if(isset($aData['content'])) {
		     $oField = new Content();
		     $mValue = $oField->sanitize($aData['content']);
		     $oModel->setContent($mValue);
		}
		if(isset($aData['content_compressed'])) {
		     $oField = new ContentCompressed();
		     $mValue = $oField->sanitize($aData['content_compressed']);
		     $oModel->setContentCompressed($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['date_sent'])) {
		     $oField = new DateSent();
		     $mValue = $oField->sanitize($aData['date_sent']);
		     $oModel->setDateSent($mValue);
		}
		if(isset($aData['sender_ip'])) {
		     $oField = new SenderIp();
		     $mValue = $oField->sanitize($aData['sender_ip']);
		     $oModel->setSenderIp($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
