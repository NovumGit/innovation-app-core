<?php
namespace Crud\SaleOrderNotification\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderNotification\ICollectionField;

/**
 * Base class that represents the 'sale_order_notification_type_id' crud field from the 'sale_order_notification' table.
 * This class is auto generated and should not be modified.
 */
abstract class SaleOrderNotificationTypeId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sale_order_notification_type_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSaleOrderNotificationTypeId';
	protected $sFqModelClassname = '\\\Model\SaleOrderNotification\SaleOrderNotification';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
