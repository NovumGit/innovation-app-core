<?php
namespace Crud\SaleOrderNotification\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderNotification\ICollectionField;

/**
 * Base class that represents the 'content_compressed' crud field from the 'sale_order_notification' table.
 * This class is auto generated and should not be modified.
 */
abstract class ContentCompressed extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'content_compressed';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getContentCompressed';
	protected $sFqModelClassname = '\\\Model\SaleOrderNotification\SaleOrderNotification';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
