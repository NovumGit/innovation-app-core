<?php
namespace Crud\SaleOrderNotification\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SaleOrderNotification\ICollectionField;

/**
 * Base class that represents the 'to_email' crud field from the 'sale_order_notification' table.
 * This class is auto generated and should not be modified.
 */
abstract class ToEmail extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'to_email';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getToEmail';
	protected $sFqModelClassname = '\\\Model\SaleOrderNotification\SaleOrderNotification';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
