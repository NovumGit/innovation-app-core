<?php
namespace Crud\SaleOrderNotification\Field;

use Crud\SaleOrderNotification\Field\Base\Title as BaseTitle;

/**
 * Skeleton subclass for representing title field from the sale_order_notification table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class Title extends BaseTitle
{
}
