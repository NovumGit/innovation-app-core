<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\BankLedger\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Finance\BankLedger as ModelObject;

class Description extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'description';
    protected $sFieldLabel = 'Omschrijving';
    private $sIcon = 'pen';
    private $sPlaceHolder = 'Geef een omschrijving';
    private $sGetter = 'getDescription';

    function getFieldName()
    {
    	return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
