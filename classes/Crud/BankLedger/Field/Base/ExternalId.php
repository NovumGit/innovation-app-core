<?php
namespace Crud\BankLedger\Field\Base;

use Crud\BankLedger\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'external_id' crud field from the 'bank_ledger' table.
 * This class is auto generated and should not be modified.
 */
abstract class ExternalId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'external_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getExternalId';
	protected $sFqModelClassname = '\\\Model\Finance\BankLedger';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
