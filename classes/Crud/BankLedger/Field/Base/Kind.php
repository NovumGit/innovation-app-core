<?php
namespace Crud\BankLedger\Field\Base;

use Crud\BankLedger\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'kind' crud field from the 'bank_ledger' table.
 * This class is auto generated and should not be modified.
 */
abstract class Kind extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'kind';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getKind';
	protected $sFqModelClassname = '\\\Model\Finance\BankLedger';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
