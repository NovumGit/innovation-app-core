<?php
namespace Crud\BankLedger\Field;

use Crud\BankLedger\Field\Base\LedgerOrigin as BaseLedgerOrigin;

/**
 * Skeleton subclass for representing ledger_origin field from the bank_ledger table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class LedgerOrigin extends BaseLedgerOrigin
{
}
