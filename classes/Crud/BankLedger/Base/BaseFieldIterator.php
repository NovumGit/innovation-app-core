<?php
namespace Crud\BankLedger\Base;

use Crud\BankLedger\ICollectionField as BankLedgerField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankLedger\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankLedgerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankLedgerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankLedgerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankLedgerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankLedgerField
	{
		return current($this->aFields);
	}
}
