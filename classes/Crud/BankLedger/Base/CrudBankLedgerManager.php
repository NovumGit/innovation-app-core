<?php
namespace Crud\BankLedger\Base;

use Core\Utils;
use Crud;
use Crud\BankLedger\FieldIterator;
use Crud\BankLedger\Field\AssetLiability;
use Crud\BankLedger\Field\Description;
use Crud\BankLedger\Field\ExternalId;
use Crud\BankLedger\Field\Kind;
use Crud\BankLedger\Field\LedgerOrigin;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankLedger;
use Model\Finance\BankLedgerQuery;
use Model\Finance\Map\BankLedgerTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankLedger instead if you need to override or add functionality.
 */
abstract class CrudBankLedgerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankLedgerQuery::create();
	}


	public function getTableMap(): BankLedgerTableMap
	{
		return new BankLedgerTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankLedger";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_ledger toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_ledger aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ExternalId', 'LedgerOrigin', 'Description', 'AssetLiability', 'Kind'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ExternalId', 'LedgerOrigin', 'Description', 'AssetLiability', 'Kind'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankLedger
	 */
	public function getModel(array $aData = null): BankLedger
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankLedgerQuery = BankLedgerQuery::create();
		     $oBankLedger = $oBankLedgerQuery->findOneById($aData['id']);
		     if (!$oBankLedger instanceof BankLedger) {
		         throw new LogicException("BankLedger should be an instance of BankLedger but got something else." . __METHOD__);
		     }
		     $oBankLedger = $this->fillVo($aData, $oBankLedger);
		}
		else {
		     $oBankLedger = new BankLedger();
		     if (!empty($aData)) {
		         $oBankLedger = $this->fillVo($aData, $oBankLedger);
		     }
		}
		return $oBankLedger;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankLedger
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankLedger
	{
		$oBankLedger = $this->getModel($aData);


		 if(!empty($oBankLedger))
		 {
		     $oBankLedger = $this->fillVo($aData, $oBankLedger);
		     $oBankLedger->save();
		 }
		return $oBankLedger;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankLedger $oModel
	 * @return BankLedger
	 */
	protected function fillVo(array $aData, BankLedger $oModel): BankLedger
	{
		if(isset($aData['external_id'])) {
		     $oField = new ExternalId();
		     $mValue = $oField->sanitize($aData['external_id']);
		     $oModel->setExternalId($mValue);
		}
		if(isset($aData['ledger_origin'])) {
		     $oField = new LedgerOrigin();
		     $mValue = $oField->sanitize($aData['ledger_origin']);
		     $oModel->setLedgerOrigin($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['asset_liability'])) {
		     $oField = new AssetLiability();
		     $mValue = $oField->sanitize($aData['asset_liability']);
		     $oModel->setAssetLiability($mValue);
		}
		if(isset($aData['kind'])) {
		     $oField = new Kind();
		     $mValue = $oField->sanitize($aData['kind']);
		     $oModel->setKind($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
