<?php
namespace Crud\BankLedger;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankLedger;
use Model\Finance\BankLedgerQuery;

class CrudBankLedgerManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'Grootboekrekening';
    }
    function getOverviewUrl():string
    {
        return '/finance/bank/ledger/overview';
    }
    function getCreateNewUrl():string
    {
        return '/custom/cockpit/project/edit';
    }
    function getNewFormTitle():string{
        return 'Nieuwe grootboekrekening';
    }
    function getEditFormTitle():string
    {
        return 'Grootboekrekening bewerken';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
'CategoryId',
'Description'
];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
'CategoryId',
'Description'
];
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oBankLedgerQuery = BankLedgerQuery::create();
            $oBankLedger = $oBankLedgerQuery->findOneById($aData['id']);

            if(!$oBankLedger instanceof BankLedger)
            {
                throw new LogicException("Project should be an instance of BankLedger but got something else." . __METHOD__);
            }
            $oBankLedger = $this->fillVo($aData, $oBankLedger);
        }
        else
        {
            $oBankLedger = new BankLedger();
            if(!empty($aData))
            {
                $oBankLedger = $this->fillVo($aData, $oBankLedger);
            }
        }
        return $oBankLedger;
    }

    /**
     * @param $aData
     * @return BankLedger
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null):BankLedger
    {
        $oBankLedger = $this->getModel($aData);
        if($oBankLedger instanceof BankLedger)
        {
            $oBankLedger = $this->fillVo($aData, $oBankLedger);
            $oBankLedger->save();
        }
        return $oBankLedger;
    }
    private function fillVo($aData, BankLedger $oModel)
    {
        if(isset($aData['category_id'])){ $oModel->setCategoryId( $aData['category_id'] ); }
        if(isset($aData['description'])){ $oModel->setDescription( $aData['description'] ); }
        return $oModel;
    }
}
