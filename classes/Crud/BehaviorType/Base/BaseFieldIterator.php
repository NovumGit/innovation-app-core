<?php
namespace Crud\BehaviorType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\BehaviorType\ICollectionField as BehaviorTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BehaviorType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BehaviorTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BehaviorTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BehaviorTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BehaviorTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BehaviorTypeField
	{
		return current($this->aFields);
	}
}
