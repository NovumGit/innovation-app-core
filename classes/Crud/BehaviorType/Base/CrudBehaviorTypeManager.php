<?php
namespace Crud\BehaviorType\Base;

use Core\Utils;
use Crud;
use Crud\BehaviorType\FieldIterator;
use Crud\BehaviorType\Field\Description;
use Crud\BehaviorType\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataModel\Behavior\BehaviorType;
use Model\System\DataModel\Behavior\BehaviorTypeQuery;
use Model\System\DataModel\Behavior\Map\BehaviorTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BehaviorType instead if you need to override or add functionality.
 */
abstract class CrudBehaviorTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BehaviorTypeQuery::create();
	}


	public function getTableMap(): BehaviorTypeTableMap
	{
		return new BehaviorTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Ondersteunde behavior typen http://propelorm.org/documentation/cookbook/writing-behavior.html.";
	}


	public function getEntityTitle(): string
	{
		return "BehaviorType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "behavior_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "behavior_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BehaviorType
	 */
	public function getModel(array $aData = null): BehaviorType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBehaviorTypeQuery = BehaviorTypeQuery::create();
		     $oBehaviorType = $oBehaviorTypeQuery->findOneById($aData['id']);
		     if (!$oBehaviorType instanceof BehaviorType) {
		         throw new LogicException("BehaviorType should be an instance of BehaviorType but got something else." . __METHOD__);
		     }
		     $oBehaviorType = $this->fillVo($aData, $oBehaviorType);
		}
		else {
		     $oBehaviorType = new BehaviorType();
		     if (!empty($aData)) {
		         $oBehaviorType = $this->fillVo($aData, $oBehaviorType);
		     }
		}
		return $oBehaviorType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BehaviorType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BehaviorType
	{
		$oBehaviorType = $this->getModel($aData);


		 if(!empty($oBehaviorType))
		 {
		     $oBehaviorType = $this->fillVo($aData, $oBehaviorType);
		     $oBehaviorType->save();
		 }
		return $oBehaviorType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BehaviorType $oModel
	 * @return BehaviorType
	 */
	protected function fillVo(array $aData, BehaviorType $oModel): BehaviorType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
