<?php
namespace Crud\BehaviorType;

/**
 * Skeleton subclass for representing a BehaviorType.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Mon 12:56:38
 */
final class CrudBehaviorTypeManager extends Base\CrudBehaviorTypeManager
{
}
