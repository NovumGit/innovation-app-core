<?php
namespace Crud\BehaviorType\Field\Base;

use Crud\BehaviorType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'description' crud field from the 'behavior_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class Description extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'description';
	protected $sFieldLabel = 'Omschrijving';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDescription';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Behavior\BehaviorType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['description']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Omschrijving" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
