<?php 
namespace Crud\BehaviorType\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataModel\Behavior\BehaviorType;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof BehaviorType)
		{
		     return "//system/behavior_type/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof BehaviorType)
		{
		     return "//behavior_type?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
