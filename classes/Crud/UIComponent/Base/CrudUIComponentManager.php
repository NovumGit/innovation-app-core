<?php
namespace Crud\UIComponent\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UIComponent\FieldIterator;
use Crud\UIComponent\Field\AppId;
use Crud\UIComponent\Field\Label;
use Crud\UIComponent\Field\ParentId;
use Crud\UIComponent\Field\ParentLocation;
use Crud\UIComponent\Field\UiComponentTypeId;
use Exception\LogicException;
use Model\System\UI\Map\UIComponentTableMap;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UIComponent instead if you need to override or add functionality.
 */
abstract class CrudUIComponentManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UIComponentQuery::create();
	}


	public function getTableMap(): UIComponentTableMap
	{
		return new UIComponentTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint omschrijft de verschillende UI typen.";
	}


	public function getEntityTitle(): string
	{
		return "UIComponent";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "ui_component toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "ui_component aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['AppId', 'ParentId', 'ParentLocation', 'UiComponentTypeId', 'Label'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['AppId', 'ParentId', 'ParentLocation', 'UiComponentTypeId', 'Label'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UIComponent
	 */
	public function getModel(array $aData = null): UIComponent
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUIComponentQuery = UIComponentQuery::create();
		     $oUIComponent = $oUIComponentQuery->findOneById($aData['id']);
		     if (!$oUIComponent instanceof UIComponent) {
		         throw new LogicException("UIComponent should be an instance of UIComponent but got something else." . __METHOD__);
		     }
		     $oUIComponent = $this->fillVo($aData, $oUIComponent);
		}
		else {
		     $oUIComponent = new UIComponent();
		     if (!empty($aData)) {
		         $oUIComponent = $this->fillVo($aData, $oUIComponent);
		     }
		}
		return $oUIComponent;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UIComponent
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UIComponent
	{
		$oUIComponent = $this->getModel($aData);


		 if(!empty($oUIComponent))
		 {
		     $oUIComponent = $this->fillVo($aData, $oUIComponent);
		     $oUIComponent->save();
		 }
		return $oUIComponent;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UIComponent $oModel
	 * @return UIComponent
	 */
	protected function fillVo(array $aData, UIComponent $oModel): UIComponent
	{
		if(isset($aData['app_id'])) {
		     $oField = new AppId();
		     $mValue = $oField->sanitize($aData['app_id']);
		     $oModel->setAppId($mValue);
		}
		if(isset($aData['parent_id'])) {
		     $oField = new ParentId();
		     $mValue = $oField->sanitize($aData['parent_id']);
		     $oModel->setParentId($mValue);
		}
		if(isset($aData['parent_location'])) {
		     $oField = new ParentLocation();
		     $mValue = $oField->sanitize($aData['parent_location']);
		     $oModel->setParentLocation($mValue);
		}
		if(isset($aData['ui_component_type_id'])) {
		     $oField = new UiComponentTypeId();
		     $mValue = $oField->sanitize($aData['ui_component_type_id']);
		     $oModel->setUiComponentTypeId($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
