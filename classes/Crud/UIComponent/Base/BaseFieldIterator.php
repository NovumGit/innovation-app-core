<?php
namespace Crud\UIComponent\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UIComponent\ICollectionField as UIComponentField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UIComponent\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UIComponentField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UIComponentField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UIComponentField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UIComponentField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UIComponentField
	{
		return current($this->aFields);
	}
}
