<?php
namespace Crud\UIComponent\Field;

use Crud\UIComponent\Field\Base\Label as BaseLabel;

/**
 * Skeleton subclass for representing label field from the ui_component table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Label extends BaseLabel
{
}
