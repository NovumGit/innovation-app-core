<?php
namespace Crud\UIComponent\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\UIComponent\ICollectionField;
use Model\System\AppQuery;

/**
 * Base class that represents the 'app_id' crud field from the 'ui_component' table.
 * This class is auto generated and should not be modified.
 */
abstract class AppId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'app_id';
	protected $sFieldLabel = 'App';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getAppId';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\AppQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\AppQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
