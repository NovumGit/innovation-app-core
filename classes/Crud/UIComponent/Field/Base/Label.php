<?php
namespace Crud\UIComponent\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\UIComponent\ICollectionField;

/**
 * Base class that represents the 'label' crud field from the 'ui_component' table.
 * This class is auto generated and should not be modified.
 */
abstract class Label extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'label';
	protected $sFieldLabel = 'Label';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLabel';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['label']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Label" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
