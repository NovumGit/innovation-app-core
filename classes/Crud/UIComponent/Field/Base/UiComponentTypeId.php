<?php
namespace Crud\UIComponent\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\UIComponent\ICollectionField;
use Model\System\UI\UiComponentTypeQuery;

/**
 * Base class that represents the 'ui_component_type_id' crud field from the 'ui_component' table.
 * This class is auto generated and should not be modified.
 */
abstract class UiComponentTypeId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'ui_component_type_id';
	protected $sFieldLabel = 'Component type';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUiComponentTypeId';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\UiComponentTypeQuery::create()->orderBytitle()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "gettitle", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\UiComponentTypeQuery::create()->findOneById($iItemId)->gettitle();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
