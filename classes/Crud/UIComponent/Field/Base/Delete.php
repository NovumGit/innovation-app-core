<?php 
namespace Crud\UIComponent\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\UI\UIComponent;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponent)
		{
		     return "//system/ui_component/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponent)
		{
		     return "//ui_component?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
