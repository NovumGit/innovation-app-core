<?php
namespace Crud\UIComponent\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\UIComponent\ICollectionField;
use Model\System\UI\UIComponentQuery;

/**
 * Base class that represents the 'parent_id' crud field from the 'ui_component' table.
 * This class is auto generated and should not be modified.
 */
abstract class ParentId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'parent_id';
	protected $sFieldLabel = 'Parent id';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getParentId';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\UIComponentQuery::create()->orderBylabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getlabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\UIComponentQuery::create()->findOneById($iItemId)->getlabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
