<?php
namespace Crud\UIComponent\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UIComponent\ICollectionField;

/**
 * Base class that represents the 'parent_location' crud field from the 'ui_component' table.
 * This class is auto generated and should not be modified.
 */
abstract class ParentLocation extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'parent_location';
	protected $sFieldLabel = 'Locatie';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getParentLocation';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
