<?php
namespace Crud\UIComponent\Field;

use Crud\UIComponent\Field\Base\ParentId as BaseParentId;

/**
 * Skeleton subclass for representing parent_id field from the ui_component table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ParentId extends BaseParentId
{
}
