<?php
namespace Crud\Own_company\Field;

use Core\Utils;
use Crud\Field;
use InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Model\Company\Company as ModelObject;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\Country as CountryModel;

class DeliveryCountry extends Field implements IFilterableField, IEditableField, IFilterableLookupField{

    protected $sFieldName = 'delivery_country_id';
    protected $sFieldLabel = 'Afl. Land';
    private $sOverviewLabel = 'Land';
    private $sIcon = 'building';
    private $sPlaceHolder = '';

    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getVisibleValue($iItemId)
    {
        $oQuery = CountryQuery::create();
        $oCountry = $oQuery->findOneById($iItemId);

        if($oCountry instanceof CountryModel)
        {
            return $oCountry->getName();
        }
        return '';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getLookups($mSelectedItem = null)
    {
        $aCountries = CountryQuery::create()->orderByName()->find();
        $aDropdownOptions = Utils::makeSelectOptions($aCountries, 'getName', $mSelectedItem);
        return $aDropdownOptions;
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sOverviewLabel, $this->sFieldName);
    }
    function getDataType():string
    {
        return 'string';
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$this->getVisibleValue($oModelObject->getCountryId()).'</td>';
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aDropdownOptions = $this->getLookups($mData->getDeliveryCountryId());

        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $mData->getDeliveryCountryId(), $aDropdownOptions, $bReadonly, $this->sIcon, $this->sPlaceHolder);
    }
}
