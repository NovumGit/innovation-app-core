<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Own_company\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use model\Company\Company as ModelObject;


class HasDeliveryAddress extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'has_delivery_address';
    protected $sFieldLabel = 'Heeft afwijkend afleveradres';
    protected $sGetter = 'getHasDeliveryAddress';

    function getDataType():string
    {
        return 'boolean';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }

    function getEditHtml($mData, $bReadonly)
    {
        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly);
    }
}
