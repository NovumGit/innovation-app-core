<?php
namespace Crud\Own_company\Field;

use Crud\Field;
use InvalidArgumentException;
use model\Company\Company as ModelObject;

class LogoUploader extends Field{

    protected $sTitle = 'Logo';
    protected $sField = 'company_logo_id';
    protected $sIcon = 'picture-o';


    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }

    function getFieldTitle()
    {
        return $this->sTitle;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sTitle, $this->sField);
    }
    function getOverviewValue($oProductType)
    {
        if(!$oProductType instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in ".__METHOD__);
        }
        return '<td class="">Dit hoort niet in de overview te staan</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        return $this->editFileField($this->sTitle, null, null, '', $this->sIcon, $bReadonly);
    }

}