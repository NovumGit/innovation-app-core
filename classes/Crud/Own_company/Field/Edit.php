<?php
namespace Crud\Own_company\Field;

use Crud\Generic\Field\GenericEdit;
use model\Company\Company as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/company/edit?id='.$oModelObject->getId();
    }
}