<?php
namespace Crud\Own_company\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use model\Company\Company;

class MakeDefault extends Field implements IEventField{


    function getIcon()
    {
        return 'check-circle';
    }

    function hasValidations() { return false; }

    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return 'Standaard vanuit deze leveren';
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof Company)
        {
            throw new LogicException("Expected an instance of Company, got ".get_class($mData));
        }
        $oCompany = $mData;

        $sButtonColor = $oCompany->isDefaultSupplier() ? 'btn-success' : 'btn-default';
        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="'.$this->getFieldTitle().'" href="/company/overview?_do=SetDefaultSupplyingCompany&company_id='.$oCompany->getId().'" class="btn '.$sButtonColor.' br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("MakeDefault field should not be there in edit view.");
    }
}
