<?php
namespace Crud\Own_company\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use model\Company\Company;

class EditSettings extends Field implements IEventField{


    function getIcon()
    {
        return 'gear';
    }

    function hasValidations() { return false; }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getFieldTitle(){
        return 'Instellingen';
    }

    function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '         <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    function getOverviewValue($mData)
    {

        if(!$mData instanceof Company)
        {
            throw new LogicException("Expected an instance of Company, got ".get_class($mData));
        }
        $oCompany = $mData;

        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="'.$this->getFieldTitle().'" href="/company/editsettings?company_id='.$oCompany->getId().'" class="btn btn-dark br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-'.$this->getIcon().'"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';

        return join(PHP_EOL, $aOut);
    }

    function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
