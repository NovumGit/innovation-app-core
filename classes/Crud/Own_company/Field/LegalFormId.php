<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Own_company\Field;

use Core\Utils;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Company\Company as ModelObject;
use Model\Setting\MasterTable\LegalFormQuery;

class LegalFormId extends Field implements IFilterableLookupField, IDisplayableField   {

    protected $sFieldName = 'legal_form_id';
    protected $sFieldLabel = 'Rechtsvorm';
    protected $sIcon = 'legal';
    protected $sGetter = 'getLegalFormId';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getLookups($iSelectedItem = null)
    {
        $aLegalForms = LegalFormQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aLegalForms, 'getName', $iSelectedItem);
        return $aOptions;
    }

    function getVisibleValue($iItemId)
    {
        return LegalFormQuery::create()->findOneById($iItemId)->getName();
    }


    function getDataType():string
    {
        return 'lookup';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
