<?php
namespace Crud\Own_company\Field;

use Crud\Field;
use InvalidArgumentException;
use model\Company\Company as ModelObject;

class SelectedImage extends Field{

    protected $sTitle = 'Geselecteerde logo';
    protected $sField = '';
    protected $sIcon = 'picture-o';


    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        return false;
    }

    function getFieldTitle()
    {
        return $this->sTitle;
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sTitle, $this->sField);
    }
    function getOverviewValue($oProductType)
    {
        if(!$oProductType instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of  ModelObject in ".__METHOD__);
        }
        return '<td class="">Dit hoort niet in de overview te staan</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected value of ModelObject but instead got " . $mData);
        }
        $oCompanyLogo = $mData->getCompanyLogoRelatedByCompanyLogoId();

        $aHtml = [];
        $aHtml[] = '<div class="form-group">';
        $aHtml[] = '        <label for="product_type" class="col-lg-3 control-label">Afbeelding thumbnail</label>';
        $aHtml[] = '    <div class="col-lg-8">';

        $aHtml[] = '        <div class="input-group">';
//        <label for="fld_" class="col-lg-3 control-label">Afbeelding</label>
        $aHtml[] = !$oCompanyLogo?'No image selected':'<img src="/data_pub/company_logo/'.$mData->getId().'.'.$oCompanyLogo->getExt().'" width="200">';
        $aHtml[] = '        </div>';
        $aHtml[] = '    </div>';
        $aHtml[] = '</div>';


        return join(PHP_EOL, $aHtml);
    }

}