<?php
namespace Crud\Own_company\Field;

use Crud\Generic\Field\GenericDelete;
use model\Company\Company as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        $sTabPart = '';
        if(isset($_GET['tab']))
        {
            $sTabPart = '&tab='.$_GET['tab'];
        }
        return '/company/edit?_do=Delete&id='.$oModelObject->getId().$sTabPart;
    }

    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}