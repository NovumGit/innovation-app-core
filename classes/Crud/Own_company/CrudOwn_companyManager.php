<?php
namespace Crud\Own_company;

use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\Company\Company;
use Model\Company\CompanyQuery;

use LogicException;

class CrudOwn_companyManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'product';
    }
    function getNewFormTitle():string{
        return 'Eigen bedrijf toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Eigen bedrijf bewerken';
    }
    function getOverviewUrl():string
    {
        return '/company/overview';
    }
    function getCreateNewUrl():string
    {
        return '/company/edit';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CompanyName',
            'GeneralStreet',
            'GeneralNumber',
            'GeneralPostal',
            'GeneralCity',
            'Edit',
            'MakeDefault',
            'EditSettings'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'CompanyName',
            'ChamberOfCommerce',
            'VatNumber',
            'LegalFormId',
            'Iban',
            'Bic',
            'Phone',
            'Fax',
            'Website',
            'Email',
            'GeneralStreet',
            'GeneralNumber',
            'GeneralNumberAdd',
            'GeneralPostal',
            'GeneralCity',
            'HasInvoiceAddress',
            'InvoiceCompanyName',
            'InvoiceStreet',
            'InvoiceNumber',
            'InvoiceNumberAdd',
            'InvoicePostal',
            'InvoiceCity',
            'HasDeliveryAddress',
            'DeliveryCompanyName',
            'DeliveryStreet',
            'DeliveryNumber',
            'DeliveryNumberAdd',
            'DeliveryPostal',
            'DeliveryCity',
            'LogoUploader',
            'SelectedImage',
            'MaxShippingCosts'
        ];
    }
    /**
     * @param $aData
     * @return Company
     */
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new CompanyQuery();

            $oCompany = $oQuery->findOneById($aData['id']);

            if(!$oCompany instanceof Company)
            {
                throw new LogicException("Company should be an instance of SaleOrder but got ".get_class($oCompany)." in ".__METHOD__);
            }
        }
        else
        {
            $oCompany = new Company();
            $oCompany = $this->fillVo($oCompany, $aData);
        }
        return $oCompany;
    }

    function store(array $aData = null)
    {
        $oCompany = $this->getModel($aData);

        if(!empty($oCompany))
        {

            $oCompany = $this->fillVo($oCompany, $aData);
            $oCompany->save();
        }
        return $oCompany;
    }
    function fillVo(Company $oCompany, $aData)
    {
        if(isset($aData['slogan'])){ $oCompany->setSlogan($aData['slogan']); }
        if(isset($aData['is_default_supplier'])){ $oCompany->setIsDefaultSupplier($aData['is_default_supplier']); }
        if(isset($aData['is_deleted'])){ $oCompany->setItemDeleted($aData['is_deleted']); }
        if(isset($aData['company_name'])){ $oCompany->setCompanyName($aData['company_name']); }
        if(isset($aData['chamber_of_commerce'])){ $oCompany->setChamberOfCommerce($aData['chamber_of_commerce']); }
        if(isset($aData['vat_number'])){ $oCompany->setVatNumber($aData['vat_number']); }
        if(isset($aData['legal_form_id'])){
            if(!$aData['legal_form_id'])
            {
                $aData['legal_form_id'] = null;
            }
            $oCompany->setLegalFormId($aData['legal_form_id']);
        }


        if(isset($aData['iban'])){ $oCompany->setIban($aData['iban']); }
        if(isset($aData['phone'])){ $oCompany->setPhone($aData['phone']); }
        if(isset($aData['fax'])){ $oCompany->setFax($aData['fax']); }
        if(isset($aData['website'])){ $oCompany->setWebsite($aData['website']); }
        if(isset($aData['email'])){ $oCompany->setEmail($aData['email']); }

        if(isset($aData['general_street'])){ $oCompany->setGeneralStreet($aData['general_street']); }
        if(isset($aData['general_number'])){ $oCompany->setGeneralNumber($aData['general_number']); }
        if(isset($aData['general_number_add'])){ $oCompany->setGeneralNumberAdd($aData['general_number_add']); }
        if(isset($aData['general_postal'])){ $oCompany->setGeneralPostal($aData['general_postal']); }
        if(isset($aData['general_po_box'])){ $oCompany->setGeneralPoBox($aData['general_po_box']); }
        if(isset($aData['general_city'])){ $oCompany->setGeneralCity($aData['general_city']); }
        if(isset($aData['general_country_id'])){ $oCompany->setGeneralCountryId($aData['general_country_id']); }

        if(isset($aData['has_invoice_address'])){ $oCompany->setHasInvoiceAddress($aData['has_invoice_address']); }
        if(isset($aData['invoice_company_name'])){ $oCompany->setInvoiceCompanyName($aData['invoice_company_name']); }
        if(isset($aData['invoice_street'])){ $oCompany->setInvoiceStreet($aData['invoice_street']); }
        if(isset($aData['invoice_number'])){ $oCompany->setInvoiceNumber($aData['invoice_number']); }
        if(isset($aData['invoice_number_add'])){ $oCompany->setInvoiceNumberAdd($aData['invoice_number_add']); }
        if(isset($aData['invoice_postal'])){ $oCompany->setInvoicePostal($aData['invoice_postal']); }
        if(isset($aData['invoice_city'])){ $oCompany->setInvoiceCity($aData['invoice_city']); }
        if(isset($aData['invoice_country_id'])){ $oCompany->setInvoiceCountryId($aData['invoice_country_id']); }

        if(isset($aData['has_delivery_address'])){ $oCompany->setHasDeliveryAddress($aData['has_delivery_address']); }
        if(isset($aData['delivery_company_name'])){ $oCompany->setDeliveryCompanyName($aData['delivery_company_name']); }
        if(isset($aData['delivery_street'])){ $oCompany->setDeliveryStreet($aData['delivery_street']); }
        if(isset($aData['delivery_number'])){ $oCompany->setDeliveryNumber($aData['delivery_number']); }
        if(isset($aData['delivery_number_add'])){ $oCompany->setDeliveryNumberAdd($aData['delivery_number_add']); }
        if(isset($aData['delivery_postal'])){ $oCompany->setDeliveryPostal($aData['delivery_postal']); }
        if(isset($aData['delivery_city'])){ $oCompany->setDeliveryCity($aData['delivery_city']); }
        if(isset($aData['delivery_country_id'])){ $oCompany->setDeliveryCountryId($aData['delivery_country_id']); }

        if(isset($aData['company_logo_id'])){ $oCompany->setCompanyLogoId($aData['company_logo_id']); }
        if(isset($aData['max_shipping_costs'])){ $oCompany->setMaxShippingCosts($aData['max_shipping_costs']); }


        return $oCompany;
    }
}
