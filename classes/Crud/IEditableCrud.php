<?php
namespace Crud;

interface IEditableCrud extends IFormManager {

    function getCreateNewUrl():string;

}
