<?php
namespace Crud\Module\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Module\ICollectionField as ModuleField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Module\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ModuleField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ModuleField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ModuleField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ModuleField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ModuleField
	{
		return current($this->aFields);
	}
}
