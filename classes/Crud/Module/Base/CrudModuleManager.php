<?php
namespace Crud\Module\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Module\FieldIterator;
use Crud\Module\Field\Name;
use Crud\Module\Field\Title;
use Exception\LogicException;
use Model\Module\Map\ModuleTableMap;
use Model\Module\Module;
use Model\Module\ModuleQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Module instead if you need to override or add functionality.
 */
abstract class CrudModuleManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ModuleQuery::create();
	}


	public function getTableMap(): ModuleTableMap
	{
		return new ModuleTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint staan alle modules waaruit het systeem bestaat beschreven";
	}


	public function getEntityTitle(): string
	{
		return "Module";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "module toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "module aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Title'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Module
	 */
	public function getModel(array $aData = null): Module
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oModuleQuery = ModuleQuery::create();
		     $oModule = $oModuleQuery->findOneById($aData['id']);
		     if (!$oModule instanceof Module) {
		         throw new LogicException("Module should be an instance of Module but got something else." . __METHOD__);
		     }
		     $oModule = $this->fillVo($aData, $oModule);
		}
		else {
		     $oModule = new Module();
		     if (!empty($aData)) {
		         $oModule = $this->fillVo($aData, $oModule);
		     }
		}
		return $oModule;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Module
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Module
	{
		$oModule = $this->getModel($aData);


		 if(!empty($oModule))
		 {
		     $oModule = $this->fillVo($aData, $oModule);
		     $oModule->save();
		 }
		return $oModule;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Module $oModel
	 * @return Module
	 */
	protected function fillVo(array $aData, Module $oModel): Module
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
