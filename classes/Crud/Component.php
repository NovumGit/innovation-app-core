<?php
namespace Crud;

use Ui\RenderEditConfig;

/**
 * Represents any kind of Component that can be presented to a user.
 *
 * A component is like a field but can contain all kind of things such as a map or a sub table. It does not need all
 * the properties that a field has.
 *
 * Class Component
 * @package Crud
 */
abstract class Component extends CrudElement implements IComponent {


    private $oRenderEditConfig;

    function setRenderEditConfig(RenderEditConfig $oRenderEditConfig)
    {
        $this->oRenderEditConfig = $oRenderEditConfig;
    }
    function getRenderEditConfig():RenderEditConfig
    {
        if($this->oRenderEditConfig instanceof RenderEditConfig)
        {
            return $this->oRenderEditConfig;
        }
        return new RenderEditConfig();
    }

    function isMutable()
    {
        return true;
    }
    function isOverviewField(): bool
    {
        return $this instanceof IListableField;
    }
}
