<?php
namespace Crud\Formulier\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\Formulier\FieldIterator;
use Crud\Formulier\Field\Code;
use Crud\Formulier\Field\CrudEditorId;
use Crud\Formulier\Field\IntroText;
use Crud\Formulier\Field\Titel;
use Crud\Formulier\Field\WetId;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Eenoverheid\Formulier;
use Model\Eenoverheid\FormulierQuery;
use Model\Eenoverheid\Map\FormulierTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Formulier instead if you need to override or add functionality.
 */
abstract class CrudFormulierManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FormulierQuery::create();
	}


	public function getTableMap(): FormulierTableMap
	{
		return new FormulierTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat aanvraag formulieren voor een wet of dienst.";
	}


	public function getEntityTitle(): string
	{
		return "Formulier";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "formulieren toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "formulieren aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code', 'IntroText', 'WetId', 'CrudEditorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code', 'IntroText', 'WetId', 'CrudEditorId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Formulier
	 */
	public function getModel(array $aData = null): Formulier
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFormulierQuery = FormulierQuery::create();
		     $oFormulier = $oFormulierQuery->findOneById($aData['id']);
		     if (!$oFormulier instanceof Formulier) {
		         throw new LogicException("Formulier should be an instance of Formulier but got something else." . __METHOD__);
		     }
		     $oFormulier = $this->fillVo($aData, $oFormulier);
		}
		else {
		     $oFormulier = new Formulier();
		     if (!empty($aData)) {
		         $oFormulier = $this->fillVo($aData, $oFormulier);
		     }
		}
		return $oFormulier;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Formulier
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Formulier
	{
		$oFormulier = $this->getModel($aData);


		 if(!empty($oFormulier))
		 {
		     $oFormulier = $this->fillVo($aData, $oFormulier);
		     $oFormulier->save();
		 }
		return $oFormulier;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Formulier $oModel
	 * @return Formulier
	 */
	protected function fillVo(array $aData, Formulier $oModel): Formulier
	{
		if(isset($aData['titel'])) {
		     $oField = new Titel();
		     $mValue = $oField->sanitize($aData['titel']);
		     $oModel->setTitel($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['intro_text'])) {
		     $oField = new IntroText();
		     $mValue = $oField->sanitize($aData['intro_text']);
		     $oModel->setIntroText($mValue);
		}
		if(isset($aData['wet_id'])) {
		     $oField = new WetId();
		     $mValue = $oField->sanitize($aData['wet_id']);
		     $oModel->setWetId($mValue);
		}
		if(isset($aData['crud_editor_id'])) {
		     $oField = new CrudEditorId();
		     $mValue = $oField->sanitize($aData['crud_editor_id']);
		     $oModel->setCrudEditorId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
