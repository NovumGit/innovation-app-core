<?php
namespace Crud\Formulier\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Formulier\ICollectionField as FormulierField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Formulier\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FormulierField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FormulierField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FormulierField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FormulierField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FormulierField
	{
		return current($this->aFields);
	}
}
