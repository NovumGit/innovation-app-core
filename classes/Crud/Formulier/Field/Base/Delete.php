<?php 
namespace Crud\Formulier\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Eenoverheid\Formulier;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Formulier)
		{
		     return "//services/formulieren/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Formulier)
		{
		     return "//formulieren?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
