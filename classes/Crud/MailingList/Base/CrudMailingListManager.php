<?php
namespace Crud\MailingList\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\MailingList\FieldIterator;
use Crud\MailingList\Field\CreatedDate;
use Crud\MailingList\Field\Name;
use Exception\LogicException;
use Model\Marketing\MailingList;
use Model\Marketing\MailingListQuery;
use Model\Marketing\Map\MailingListTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify MailingList instead if you need to override or add functionality.
 */
abstract class CrudMailingListManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return MailingListQuery::create();
	}


	public function getTableMap(): MailingListTableMap
	{
		return new MailingListTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "MailingList";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mailing_list toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mailing_list aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return MailingList
	 */
	public function getModel(array $aData = null): MailingList
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oMailingListQuery = MailingListQuery::create();
		     $oMailingList = $oMailingListQuery->findOneById($aData['id']);
		     if (!$oMailingList instanceof MailingList) {
		         throw new LogicException("MailingList should be an instance of MailingList but got something else." . __METHOD__);
		     }
		     $oMailingList = $this->fillVo($aData, $oMailingList);
		}
		else {
		     $oMailingList = new MailingList();
		     if (!empty($aData)) {
		         $oMailingList = $this->fillVo($aData, $oMailingList);
		     }
		}
		return $oMailingList;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return MailingList
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MailingList
	{
		$oMailingList = $this->getModel($aData);


		 if(!empty($oMailingList))
		 {
		     $oMailingList = $this->fillVo($aData, $oMailingList);
		     $oMailingList->save();
		 }
		return $oMailingList;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param MailingList $oModel
	 * @return MailingList
	 */
	protected function fillVo(array $aData, MailingList $oModel): MailingList
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
