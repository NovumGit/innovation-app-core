<?php
namespace Crud\MailingList\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\MailingList\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
