<?php

namespace Crud\Task\Field;

use Crud\Task\Field\Base\Executable as BaseExecutable;

/**
 * Skeleton subclass for representing executable field from the task table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class Executable extends BaseExecutable
{
}
