<?php
namespace Crud\Task\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Task\ICollectionField;

/**
 * Base class that represents the 'last_finish' crud field from the 'task' table.
 * This class is auto generated and should not be modified.
 */
abstract class LastFinish extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'last_finish';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLastFinish';
	protected $sFqModelClassname = '\\\Model\Api\Task';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
