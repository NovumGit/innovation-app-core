<?php
namespace Crud\Task\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Task\FieldIterator;
use Crud\Task\Field\Executable;
use Crud\Task\Field\FinishCount;
use Crud\Task\Field\LastFinish;
use Crud\Task\Field\LastRunFinished;
use Crud\Task\Field\LastStart;
use Crud\Task\Field\RunCount;
use Exception\LogicException;
use Model\Api\Map\TaskTableMap;
use Model\Api\Task;
use Model\Api\TaskQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Task instead if you need to override or add functionality.
 */
abstract class CrudTaskManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return TaskQuery::create();
	}


	public function getTableMap(): TaskTableMap
	{
		return new TaskTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Task";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "task toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "task aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Executable', 'RunCount', 'FinishCount', 'LastRunFinished', 'LastStart', 'LastFinish'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Executable', 'RunCount', 'FinishCount', 'LastRunFinished', 'LastStart', 'LastFinish'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Task
	 */
	public function getModel(array $aData = null): Task
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oTaskQuery = TaskQuery::create();
		     $oTask = $oTaskQuery->findOneById($aData['id']);
		     if (!$oTask instanceof Task) {
		         throw new LogicException("Task should be an instance of Task but got something else." . __METHOD__);
		     }
		     $oTask = $this->fillVo($aData, $oTask);
		}
		else {
		     $oTask = new Task();
		     if (!empty($aData)) {
		         $oTask = $this->fillVo($aData, $oTask);
		     }
		}
		return $oTask;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Task
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Task
	{
		$oTask = $this->getModel($aData);


		 if(!empty($oTask))
		 {
		     $oTask = $this->fillVo($aData, $oTask);
		     $oTask->save();
		 }
		return $oTask;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Task $oModel
	 * @return Task
	 */
	protected function fillVo(array $aData, Task $oModel): Task
	{
		if(isset($aData['executable'])) {
		     $oField = new Executable();
		     $mValue = $oField->sanitize($aData['executable']);
		     $oModel->setExecutable($mValue);
		}
		if(isset($aData['run_count'])) {
		     $oField = new RunCount();
		     $mValue = $oField->sanitize($aData['run_count']);
		     $oModel->setRunCount($mValue);
		}
		if(isset($aData['finish_count'])) {
		     $oField = new FinishCount();
		     $mValue = $oField->sanitize($aData['finish_count']);
		     $oModel->setFinishCount($mValue);
		}
		if(isset($aData['last_run_finished'])) {
		     $oField = new LastRunFinished();
		     $mValue = $oField->sanitize($aData['last_run_finished']);
		     $oModel->setLastRunFinished($mValue);
		}
		if(isset($aData['last_start'])) {
		     $oField = new LastStart();
		     $mValue = $oField->sanitize($aData['last_start']);
		     $oModel->setLastStart($mValue);
		}
		if(isset($aData['last_finish'])) {
		     $oField = new LastFinish();
		     $mValue = $oField->sanitize($aData['last_finish']);
		     $oModel->setLastFinish($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
