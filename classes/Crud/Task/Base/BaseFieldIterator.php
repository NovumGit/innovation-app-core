<?php
namespace Crud\Task\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Task\ICollectionField as TaskField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Task\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param TaskField[] $aFields */
	private array $aFields = [];


	/**
	 * @param TaskField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof TaskField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(TaskField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): TaskField
	{
		return current($this->aFields);
	}
}
