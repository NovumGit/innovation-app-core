<?php
namespace Crud\Task\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Task\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
