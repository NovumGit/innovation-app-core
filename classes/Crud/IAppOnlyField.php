<?php
namespace Crud;


/**
 * By implementing this interface the system knows the field is only available in app context.
 * Field.isAppOnly will automagically return true when the interface is implemented
 *
 * Interface InterfaceAppViewField
 * @package Crud
 */
interface IAppOnlyField {

    function isConfigurable():bool;
    function getConfigUrl():?string;
}
