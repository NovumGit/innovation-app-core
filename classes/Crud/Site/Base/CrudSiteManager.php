<?php
namespace Crud\Site\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Site\FieldIterator;
use Crud\Site\Field\ContactFormToEmail;
use Crud\Site\Field\CustomFolder;
use Crud\Site\Field\Domain;
use Crud\Site\Field\FromEmail;
use Crud\Site\Field\GeneralPhone;
use Crud\Site\Field\InvoicingCompanyId;
use Crud\Site\Field\NumFooterBlocks;
use Exception\LogicException;
use Model\Cms\Map\SiteTableMap;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Site instead if you need to override or add functionality.
 */
abstract class CrudSiteManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteQuery::create();
	}


	public function getTableMap(): SiteTableMap
	{
		return new SiteTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Site";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Domain', 'CustomFolder', 'FromEmail', 'ContactFormToEmail', 'NumFooterBlocks', 'GeneralPhone', 'InvoicingCompanyId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Domain', 'CustomFolder', 'FromEmail', 'ContactFormToEmail', 'NumFooterBlocks', 'GeneralPhone', 'InvoicingCompanyId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Site
	 */
	public function getModel(array $aData = null): Site
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteQuery = SiteQuery::create();
		     $oSite = $oSiteQuery->findOneById($aData['id']);
		     if (!$oSite instanceof Site) {
		         throw new LogicException("Site should be an instance of Site but got something else." . __METHOD__);
		     }
		     $oSite = $this->fillVo($aData, $oSite);
		}
		else {
		     $oSite = new Site();
		     if (!empty($aData)) {
		         $oSite = $this->fillVo($aData, $oSite);
		     }
		}
		return $oSite;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Site
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Site
	{
		$oSite = $this->getModel($aData);


		 if(!empty($oSite))
		 {
		     $oSite = $this->fillVo($aData, $oSite);
		     $oSite->save();
		 }
		return $oSite;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Site $oModel
	 * @return Site
	 */
	protected function fillVo(array $aData, Site $oModel): Site
	{
		if(isset($aData['domain'])) {
		     $oField = new Domain();
		     $mValue = $oField->sanitize($aData['domain']);
		     $oModel->setDomain($mValue);
		}
		if(isset($aData['custom_folder'])) {
		     $oField = new CustomFolder();
		     $mValue = $oField->sanitize($aData['custom_folder']);
		     $oModel->setCustomFolder($mValue);
		}
		if(isset($aData['from_email'])) {
		     $oField = new FromEmail();
		     $mValue = $oField->sanitize($aData['from_email']);
		     $oModel->setFromEmail($mValue);
		}
		if(isset($aData['contact_form_to_email'])) {
		     $oField = new ContactFormToEmail();
		     $mValue = $oField->sanitize($aData['contact_form_to_email']);
		     $oModel->setContactFormToEmail($mValue);
		}
		if(isset($aData['num_footer_blocks'])) {
		     $oField = new NumFooterBlocks();
		     $mValue = $oField->sanitize($aData['num_footer_blocks']);
		     $oModel->setNumFooterBlocks($mValue);
		}
		if(isset($aData['general_phone'])) {
		     $oField = new GeneralPhone();
		     $mValue = $oField->sanitize($aData['general_phone']);
		     $oModel->setGeneralPhone($mValue);
		}
		if(isset($aData['invoicing_company_id'])) {
		     $oField = new InvoicingCompanyId();
		     $mValue = $oField->sanitize($aData['invoicing_company_id']);
		     $oModel->setInvoicingCompanyId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
