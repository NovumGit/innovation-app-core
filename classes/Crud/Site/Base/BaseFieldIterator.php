<?php
namespace Crud\Site\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Site\ICollectionField as SiteField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Site\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteField
	{
		return current($this->aFields);
	}
}
