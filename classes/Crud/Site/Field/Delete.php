<?php

namespace Crud\Site\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Cms\Site as ModelObject;
use InvalidArgumentException;

class Delete extends GenericDelete
{

    public function getDeleteUrl($oModelObject)
    {

        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got ' . get_class($oModelObject));
        }
        return "/admin/site/overview?_do=Delete&id=" . $oModelObject->getId();
    }

    public function getUnDeleteUrl($oUnused)
    {
        // undelete niet mogelijk bij dit type record
        return;
    }
}
