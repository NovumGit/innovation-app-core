<?php

namespace Crud\Site\Field;

use Crud\Field;
use Crud\IEventField;
use Exception\LogicException;
use Model\Cms\Site as ModelObject;

class ButtonConfigurable extends Field implements IEventField
{
    protected $sFieldName = null;
    protected $sFieldLabel = 'Faq categorieën bewerken';

    public function getIcon()
    {
        return 'info';
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getFieldName()
    {
        return $this->sFieldName;
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getOverviewHeader()
    {
        $aOut = [];
        $aOut[] = '<th class="iconcol">';
        $aOut[] = '    <a href="#" class="btn btn-default br2 btn-xs">';
        $aOut[] = '   <i class="fa fa-' . $this->getIcon() . '"></i>';
        $aOut[] = '    </a>';
        $aOut[] = '</th>';
        return join(PHP_EOL, $aOut);
    }

    public function getOverviewValue($mData)
    {
        if (!$mData instanceof ModelObject) {
            throw new LogicException("Expected an instance of Customer, got " . get_class($mData));
        }
        $oSite = $mData;
        $aOut = [];
        $aOut[] = '<td class="xx">';
        $aOut[] = ' <a title="Faq categorieën bewerken" href="/setting/faq_category/overview?site=' . $oSite->getDomain() . '" class="btn btn-success br2 btn-xs fs12 d">';
        $aOut[] = '  <i class="fa fa-' . $this->getIcon() . '"></i>';
        $aOut[] = ' </a>';
        $aOut[] = '</td>';
        return join(PHP_EOL, $aOut);
    }

    public function getEditHtml($mData, $bReadonly)
    {
        throw new LogicException("Delete field should not be there in edit view.");
    }
}
