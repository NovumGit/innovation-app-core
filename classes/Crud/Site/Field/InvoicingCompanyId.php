<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site\Field;

use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Cms\Site as ModelObject;
use Model\Company\CompanyQuery;

class InvoicingCompanyId extends Field implements IFilterableLookupField
{

    protected $sFieldName = 'invoicing_company_id';
    protected $sFieldLabel = 'Facturen boeken onder';
    protected $sIcon = 'building';
    protected $sGetter = 'getInvoicingCompanyId';

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getLookups($mSelectedItem = null)
    {
        $aCompanies = CompanyQuery::create()->orderByCompanyName()->find();
        $aOptions = Utils::makeSelectOptions($aCompanies, 'getCompanyName', $mSelectedItem);
        return $aOptions;
    }

    function getVisibleValue($iItemId)
    {
        return CompanyQuery::create()->findOneById($iItemId)->getCompanyName();
    }

    public function getDataType(): string
    {
        return 'lookup';
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
