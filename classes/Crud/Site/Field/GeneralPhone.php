<?php

/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\Site as ModelObject;

class GeneralPhone extends Field implements IFilterableField, IEditableField
{

    protected $sFieldName = 'general_phone';
    protected $sFieldLabel = 'Telefoonnummer';
    protected $sIcon = 'envelope';
    protected $sPlaceHolder = '';
    protected $sGetter = 'getGeneralPhone';

    function getDataType(): string
    {
        return 'string';
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    public function hasValidations()
    {
        return false;
    }

    public function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }

    public function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }

    public function getOverviewValue($oModelObject)
    {
        if (!$oModelObject instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }
        return '<td class="">' . $oModelObject->{$this->sGetter}() . '</td>';
    }

    public function getFieldTitle()
    {
        return $this->sFieldLabel;
    }

    public function getEditHtml($mData, $bReadonly)
    {
        if (!$mData instanceof ModelObject) {
            throw new InvalidArgumentException("Expected an instance of ModelObject in " . __METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
