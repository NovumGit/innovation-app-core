<?php

namespace Crud\Site\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Model\Cms\Site as ModelObject;

class Edit extends GenericEdit
{

    public function getEditUrl($oObject)
    {

        if (!$oObject instanceof ModelObject) {
            throw new LogicException('Expected an instance of SiteBanner but got ' . get_class($oObject));
        }
        return "/admin/site/edit?id={$oObject->getId()}";
    }
}
