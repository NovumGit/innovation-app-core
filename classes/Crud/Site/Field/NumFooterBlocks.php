<?php

namespace Crud\Site\Field;

use Crud\Site\Field\Base\NumFooterBlocks as BaseNumFooterBlocks;

/**
 * Skeleton subclass for representing num_footer_blocks field from the site table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class NumFooterBlocks extends BaseNumFooterBlocks
{
}
