<?php
namespace Crud\Site\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Site\ICollectionField;

/**
 * Base class that represents the 'invoicing_company_id' crud field from the 'site' table.
 * This class is auto generated and should not be modified.
 */
abstract class InvoicingCompanyId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'invoicing_company_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getInvoicingCompanyId';
	protected $sFqModelClassname = '\\\Model\Cms\Site';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
