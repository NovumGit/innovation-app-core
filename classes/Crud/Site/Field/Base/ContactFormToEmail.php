<?php
namespace Crud\Site\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Site\ICollectionField;

/**
 * Base class that represents the 'contact_form_to_email' crud field from the 'site' table.
 * This class is auto generated and should not be modified.
 */
abstract class ContactFormToEmail extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'contact_form_to_email';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getContactFormToEmail';
	protected $sFqModelClassname = '\\\Model\Cms\Site';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
