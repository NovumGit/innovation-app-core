<?php

namespace Crud\Site;

use Core\Utils;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Model\Cms\Map\SiteTableMap;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Exception\LogicException;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

class CrudSiteManager extends FormManager implements IConfigurableCrud, IApiExposable
{
    function getTableMap(): TableMap
    {
        return new SiteTableMap();
    }

    function getQueryObject(): ModelCriteria
    {
        return SiteQuery::create();
    }

    function getShortDescription(): string
    {
        return 'Define publicly accessible sites / domainnames';
    }

    function getEntityTitle(): string
    {
        return 'site';
    }
    function getNewFormTitle(): string
    {
        return 'Website instellingen';
    }
    function getEditFormTitle(): string
    {
        return 'Instellingen bewerken';
    }
    function getOverviewUrl(): string
    {
        return '/admin/site/overview';
    }
    function getCreateNewUrl(): string
    {
        return '/admin/site/edit';
    }
    function getDefaultOverviewFields(bool $bAddNs = false): ?array
    {
        $aOverviewColumns =  [
            'Domain',
            'FromEmail',
            'InvoicingCompanyId'
        ];
        if ($bAddNs) {
            array_walk($aOverviewColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }


    function getDefaultEditFields(bool $bAddNs = false): ?array
    {
        $aOverviewColumns =  [
            'Domain',
            'FromEmail',
            'InvoicingCompanyId'
        ];
        if ($bAddNs) {
            array_walk($aOverviewColumns, function (&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aOverviewColumns;
    }


    /**
     * @param $aData
     * @return Site
     */
    function getModel(array $aData = null)
    {
        if (isset($aData['site'])) {
            $oQuery = new SiteQuery();

            $oSite = $oQuery->findOneByDomain($aData['site']);

            if (!$oSite instanceof Site) {
                throw new LogicException("Site should be an instance of Site but got " . get_class($oSite) . " in " . __METHOD__);
            }
        } else {
            $oSite = new Site();
            $oSite = $this->fillVo($oSite, $aData);
        }
        return $oSite;
    }

    function store(array $aData = null)
    {
        $oSite = $this->getModel($aData);

        if (!empty($oSite)) {
            $oSite = $this->fillVo($oSite, $aData);
            $oSite->save();
        }
        return $oSite;
    }
    function fillVo(Site $oSite, $aData)
    {
        if (isset($aData['id'])) {
            $oSite->setId($aData['id']);
        }

        if (isset($aData['contact_form_to_email'])) {
            $oSite->setContactFormToEmail($aData['contact_form_to_email']);
        }
        if (isset($aData['from_email'])) {
            $oSite->setFromEmail($aData['from_email']);
        }
        if (isset($aData['invoicing_company_id'])) {
            if (!$aData['invoicing_company_id']) {
                $aData['invoicing_company_id'] = null;
            }
            $oSite->setInvoicingCompanyId($aData['invoicing_company_id']);
        }
        return $oSite;
    }
}
