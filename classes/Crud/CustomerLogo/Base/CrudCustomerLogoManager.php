<?php
namespace Crud\CustomerLogo\Base;

use Core\Utils;
use Crud;
use Crud\CustomerLogo\FieldIterator;
use Crud\CustomerLogo\Field\CustomerId;
use Crud\CustomerLogo\Field\FileExt;
use Crud\CustomerLogo\Field\Mime;
use Crud\CustomerLogo\Field\OriginalName;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerLogo;
use Model\Crm\CustomerLogoQuery;
use Model\Crm\Map\CustomerLogoTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerLogo instead if you need to override or add functionality.
 */
abstract class CrudCustomerLogoManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerLogoQuery::create();
	}


	public function getTableMap(): CustomerLogoTableMap
	{
		return new CustomerLogoTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerLogo";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "customer_logo toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "customer_logo aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'OriginalName', 'FileExt', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'OriginalName', 'FileExt', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerLogo
	 */
	public function getModel(array $aData = null): CustomerLogo
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerLogoQuery = CustomerLogoQuery::create();
		     $oCustomerLogo = $oCustomerLogoQuery->findOneById($aData['id']);
		     if (!$oCustomerLogo instanceof CustomerLogo) {
		         throw new LogicException("CustomerLogo should be an instance of CustomerLogo but got something else." . __METHOD__);
		     }
		     $oCustomerLogo = $this->fillVo($aData, $oCustomerLogo);
		}
		else {
		     $oCustomerLogo = new CustomerLogo();
		     if (!empty($aData)) {
		         $oCustomerLogo = $this->fillVo($aData, $oCustomerLogo);
		     }
		}
		return $oCustomerLogo;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerLogo
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerLogo
	{
		$oCustomerLogo = $this->getModel($aData);


		 if(!empty($oCustomerLogo))
		 {
		     $oCustomerLogo = $this->fillVo($aData, $oCustomerLogo);
		     $oCustomerLogo->save();
		 }
		return $oCustomerLogo;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerLogo $oModel
	 * @return CustomerLogo
	 */
	protected function fillVo(array $aData, CustomerLogo $oModel): CustomerLogo
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		if(isset($aData['mime'])) {
		     $oField = new Mime();
		     $mValue = $oField->sanitize($aData['mime']);
		     $oModel->setMime($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
