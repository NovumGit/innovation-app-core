<?php
namespace Crud\CustomerLogo\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerLogo\ICollectionField as CustomerLogoField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerLogo\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerLogoField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerLogoField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerLogoField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerLogoField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerLogoField
	{
		return current($this->aFields);
	}
}
