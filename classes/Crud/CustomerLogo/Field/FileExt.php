<?php
namespace Crud\CustomerLogo\Field;

use Crud\CustomerLogo\Field\Base\FileExt as BaseFileExt;

/**
 * Skeleton subclass for representing file_ext field from the customer_logo table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class FileExt extends BaseFileExt
{
}
