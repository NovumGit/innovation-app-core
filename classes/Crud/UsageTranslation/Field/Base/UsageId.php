<?php
namespace Crud\UsageTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UsageTranslation\ICollectionField;

/**
 * Base class that represents the 'usage_id' crud field from the 'mt_usage_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class UsageId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'usage_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUsageId';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\UsageTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
