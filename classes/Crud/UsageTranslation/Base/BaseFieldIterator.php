<?php
namespace Crud\UsageTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UsageTranslation\ICollectionField as UsageTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UsageTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UsageTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UsageTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UsageTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UsageTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UsageTranslationField
	{
		return current($this->aFields);
	}
}
