<?php
namespace Crud\UsageTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UsageTranslation\FieldIterator;
use Crud\UsageTranslation\Field\LanguageId;
use Crud\UsageTranslation\Field\Name;
use Crud\UsageTranslation\Field\UsageId;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\UsageTranslationTableMap;
use Model\Setting\MasterTable\UsageTranslation;
use Model\Setting\MasterTable\UsageTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UsageTranslation instead if you need to override or add functionality.
 */
abstract class CrudUsageTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UsageTranslationQuery::create();
	}


	public function getTableMap(): UsageTranslationTableMap
	{
		return new UsageTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UsageTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_usage_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_usage_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UsageId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UsageId', 'LanguageId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UsageTranslation
	 */
	public function getModel(array $aData = null): UsageTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUsageTranslationQuery = UsageTranslationQuery::create();
		     $oUsageTranslation = $oUsageTranslationQuery->findOneById($aData['id']);
		     if (!$oUsageTranslation instanceof UsageTranslation) {
		         throw new LogicException("UsageTranslation should be an instance of UsageTranslation but got something else." . __METHOD__);
		     }
		     $oUsageTranslation = $this->fillVo($aData, $oUsageTranslation);
		}
		else {
		     $oUsageTranslation = new UsageTranslation();
		     if (!empty($aData)) {
		         $oUsageTranslation = $this->fillVo($aData, $oUsageTranslation);
		     }
		}
		return $oUsageTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UsageTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UsageTranslation
	{
		$oUsageTranslation = $this->getModel($aData);


		 if(!empty($oUsageTranslation))
		 {
		     $oUsageTranslation = $this->fillVo($aData, $oUsageTranslation);
		     $oUsageTranslation->save();
		 }
		return $oUsageTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UsageTranslation $oModel
	 * @return UsageTranslation
	 */
	protected function fillVo(array $aData, UsageTranslation $oModel): UsageTranslation
	{
		if(isset($aData['usage_id'])) {
		     $oField = new UsageId();
		     $mValue = $oField->sanitize($aData['usage_id']);
		     $oModel->setUsageId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
