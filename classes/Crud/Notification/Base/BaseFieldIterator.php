<?php
namespace Crud\Notification\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Notification\ICollectionField as NotificationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Notification\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param NotificationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param NotificationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof NotificationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(NotificationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): NotificationField
	{
		return current($this->aFields);
	}
}
