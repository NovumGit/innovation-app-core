<?php
namespace Crud\Notification\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Notification\FieldIterator;
use Crud\Notification\Field\CreatedOn;
use Crud\Notification\Field\DismissedOn;
use Crud\Notification\Field\Message;
use Crud\Notification\Field\NotificationLevelId;
use Crud\Notification\Field\NotificationTypeId;
use Exception\LogicException;
use Model\System\Map\NotificationTableMap;
use Model\System\Notification;
use Model\System\NotificationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Notification instead if you need to override or add functionality.
 */
abstract class CrudNotificationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return NotificationQuery::create();
	}


	public function getTableMap(): NotificationTableMap
	{
		return new NotificationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Notification";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "notification toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "notification aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['NotificationLevelId', 'NotificationTypeId', 'CreatedOn', 'DismissedOn', 'Message'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['NotificationLevelId', 'NotificationTypeId', 'CreatedOn', 'DismissedOn', 'Message'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Notification
	 */
	public function getModel(array $aData = null): Notification
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oNotificationQuery = NotificationQuery::create();
		     $oNotification = $oNotificationQuery->findOneById($aData['id']);
		     if (!$oNotification instanceof Notification) {
		         throw new LogicException("Notification should be an instance of Notification but got something else." . __METHOD__);
		     }
		     $oNotification = $this->fillVo($aData, $oNotification);
		}
		else {
		     $oNotification = new Notification();
		     if (!empty($aData)) {
		         $oNotification = $this->fillVo($aData, $oNotification);
		     }
		}
		return $oNotification;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Notification
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Notification
	{
		$oNotification = $this->getModel($aData);


		 if(!empty($oNotification))
		 {
		     $oNotification = $this->fillVo($aData, $oNotification);
		     $oNotification->save();
		 }
		return $oNotification;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Notification $oModel
	 * @return Notification
	 */
	protected function fillVo(array $aData, Notification $oModel): Notification
	{
		if(isset($aData['notification_level_id'])) {
		     $oField = new NotificationLevelId();
		     $mValue = $oField->sanitize($aData['notification_level_id']);
		     $oModel->setNotificationLevelId($mValue);
		}
		if(isset($aData['notification_type_id'])) {
		     $oField = new NotificationTypeId();
		     $mValue = $oField->sanitize($aData['notification_type_id']);
		     $oModel->setNotificationTypeId($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['dismissed_on'])) {
		     $oField = new DismissedOn();
		     $mValue = $oField->sanitize($aData['dismissed_on']);
		     $oModel->setDismissedOn($mValue);
		}
		if(isset($aData['message'])) {
		     $oField = new Message();
		     $mValue = $oField->sanitize($aData['message']);
		     $oModel->setMessage($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
