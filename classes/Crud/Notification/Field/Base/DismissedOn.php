<?php
namespace Crud\Notification\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Notification\ICollectionField;

/**
 * Base class that represents the 'dismissed_on' crud field from the 'notification' table.
 * This class is auto generated and should not be modified.
 */
abstract class DismissedOn extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'dismissed_on';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDismissedOn';
	protected $sFqModelClassname = '\\\Model\System\Notification';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
