<?php
namespace Crud\Notification\Field;

use Crud\Notification\Field\Base\NotificationLevelId as BaseNotificationLevelId;

/**
 * Skeleton subclass for representing notification_level_id field from the notification table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class NotificationLevelId extends BaseNotificationLevelId
{
}
