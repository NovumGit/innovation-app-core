<?php
namespace Crud;

/**
 * Wanneer deze interface is geimplementeerd dan blijft het veld beschikbaar, ongeacht of het veld al in gebriuk is.
 *
 * @package Crud
 */
interface IRecurringField {}
