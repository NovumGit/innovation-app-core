<?php
namespace Crud\BackgroundTask\Base;

use Crud\BackgroundTask\ICollectionField as BackgroundTaskField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BackgroundTask\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BackgroundTaskField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BackgroundTaskField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BackgroundTaskField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BackgroundTaskField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BackgroundTaskField
	{
		return current($this->aFields);
	}
}
