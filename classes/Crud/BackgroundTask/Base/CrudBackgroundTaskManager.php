<?php
namespace Crud\BackgroundTask\Base;

use Core\Utils;
use Crud;
use Crud\BackgroundTask\FieldIterator;
use Crud\BackgroundTask\Field\Arguments;
use Crud\BackgroundTask\Field\Task;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\BackgroundTask;
use Model\System\BackgroundTaskQuery;
use Model\System\Map\BackgroundTaskTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BackgroundTask instead if you need to override or add functionality.
 */
abstract class CrudBackgroundTaskManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BackgroundTaskQuery::create();
	}


	public function getTableMap(): BackgroundTaskTableMap
	{
		return new BackgroundTaskTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BackgroundTask";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "background_task toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "background_task aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Task', 'Arguments'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Task', 'Arguments'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BackgroundTask
	 */
	public function getModel(array $aData = null): BackgroundTask
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBackgroundTaskQuery = BackgroundTaskQuery::create();
		     $oBackgroundTask = $oBackgroundTaskQuery->findOneById($aData['id']);
		     if (!$oBackgroundTask instanceof BackgroundTask) {
		         throw new LogicException("BackgroundTask should be an instance of BackgroundTask but got something else." . __METHOD__);
		     }
		     $oBackgroundTask = $this->fillVo($aData, $oBackgroundTask);
		}
		else {
		     $oBackgroundTask = new BackgroundTask();
		     if (!empty($aData)) {
		         $oBackgroundTask = $this->fillVo($aData, $oBackgroundTask);
		     }
		}
		return $oBackgroundTask;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BackgroundTask
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BackgroundTask
	{
		$oBackgroundTask = $this->getModel($aData);


		 if(!empty($oBackgroundTask))
		 {
		     $oBackgroundTask = $this->fillVo($aData, $oBackgroundTask);
		     $oBackgroundTask->save();
		 }
		return $oBackgroundTask;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BackgroundTask $oModel
	 * @return BackgroundTask
	 */
	protected function fillVo(array $aData, BackgroundTask $oModel): BackgroundTask
	{
		if(isset($aData['task'])) {
		     $oField = new Task();
		     $mValue = $oField->sanitize($aData['task']);
		     $oModel->setTask($mValue);
		}
		if(isset($aData['arguments'])) {
		     $oField = new Arguments();
		     $mValue = $oField->sanitize($aData['arguments']);
		     $oModel->setArguments($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
