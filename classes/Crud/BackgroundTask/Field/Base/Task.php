<?php
namespace Crud\BackgroundTask\Field\Base;

use Crud\BackgroundTask\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'task' crud field from the 'background_task' table.
 * This class is auto generated and should not be modified.
 */
abstract class Task extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'task';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTask';
	protected $sFqModelClassname = '\\\Model\System\BackgroundTask';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
