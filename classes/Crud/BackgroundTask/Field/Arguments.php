<?php
namespace Crud\BackgroundTask\Field;

use Crud\BackgroundTask\Field\Base\Arguments as BaseArguments;

/**
 * Skeleton subclass for representing arguments field from the background_task table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class Arguments extends BaseArguments
{
}
