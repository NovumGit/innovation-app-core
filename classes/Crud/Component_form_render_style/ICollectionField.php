<?php
namespace Crud\Component_form_render_style;

/**
 * Skeleton interface used for grouping fields that are belong to Component_form_render_style.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this interface to meet your application requirements.
 * This interface will only be generated once / when it does not exist already.
 */
interface ICollectionField extends Base\IBaseCollectionField
{
}
