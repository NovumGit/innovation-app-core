<?php
namespace Crud\Component_form_render_style\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_form_render_style\ICollectionField as Component_form_render_styleField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_form_render_style\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_form_render_styleField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_form_render_styleField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_form_render_styleField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_form_render_styleField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_form_render_styleField
	{
		return current($this->aFields);
	}
}
