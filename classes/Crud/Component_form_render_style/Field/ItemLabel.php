<?php
namespace Crud\Component_form_render_style\Field;

use Crud\Component_form_render_style\Field\Base\ItemLabel as BaseItemLabel;

/**
 * Skeleton subclass for representing item_label field from the component_form_render_style table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ItemLabel extends BaseItemLabel
{
}
