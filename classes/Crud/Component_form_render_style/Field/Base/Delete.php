<?php 
namespace Crud\Component_form_render_style\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Form\Component_form_render_style;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_form_render_style)
		{
		     return "//system/component_form_render_style/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_form_render_style)
		{
		     return "//component_form_render_style?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
