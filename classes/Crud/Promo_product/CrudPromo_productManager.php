<?php
namespace Crud\Promo_product;

use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Crud\FormManager;
use Model\PromoProduct;
use Model\PromoProductQuery;
use Propel\Runtime\Exception\LogicException;

class CrudPromo_productManager extends FormManager implements IEditableCrud, IConfigurableCrud
{
    function getCreateNewUrl():string
    {
        $sSite = $_GET['site'];
        return "/cms/promoproducts/edit?site=$sSite";
    }
    function getEntityTitle():string
    {
        throw new \Exception\LogicException("Not implemented yet");
        return 'promo_product';
    }
    function getNewFormTitle():string
    {
        return 'Promotie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Promotie wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'About'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'About'
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oPromoProductQuery = new PromoProductQuery();
            $oPromoProduct = $oPromoProductQuery->findOneById($aData['id']);
            if(!$oPromoProduct instanceof PromoProduct){
                throw new LogicException("PromoProduct should be an instance of PromoProduct but got ".get_class($oPromoProduct)." in ".__METHOD__);
            }
        }
        else
        {
            $oPromoProduct = $this->fillVo(new PromoProduct(), $aData['about']);
        }
        return $oPromoProduct;
    }
    function store(array $aData = null)
    {
        $oPromoProduct = $this->getModel($aData);
        $oPromoProduct->save();

        return $oPromoProduct;
    }
    function fillVo(PromoProduct $oPromoProduct, $aData):PromoProduct
    {
        if(isset($aData['about']))
        {
            $oPromoProduct->setAbout($aData['name']);
        }
        if(isset($aData['product_id']))
        {
            $oPromoProduct->setProductId($aData['product_id']);
        }
        return $oPromoProduct;
    }
}
