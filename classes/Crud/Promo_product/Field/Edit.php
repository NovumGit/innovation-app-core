<?php
namespace Crud\Promo_product\Field;

use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Exception\InvalidArgumentException;
use Model\PromoProduct;

class Edit extends GenericEdit{

    function getEditUrl($oPromoProduct){

        if(!$oPromoProduct instanceof PromoProduct)
        {
            throw new InvalidArgumentException('Expected an instance of \\model\\\PromoProduct but got '.get_class($oPromoProduct));
        }


        return '/cms/promoproducts/edit?site='.$_GET['site'].'&promo_product_id='.$oPromoProduct->getId();
    }
}