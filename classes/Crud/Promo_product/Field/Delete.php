<?php
namespace Crud\Promo_product\Field;

use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use InvalidArgumentException;
use Model\PromoProduct;

class Delete extends GenericDelete{


    function getDeleteUrl($oPromoProduct){

        if(!$oPromoProduct instanceof PromoProduct)
        {
            throw new InvalidArgumentException('Expected an instance of PromoProduct but got '.get_class($oPromoProduct));
        }

        return '/cms/promoproducts/edit?site='.$_GET['site'].'&_do=Delete&id='.$oPromoProduct->getId();
    }

    function getUnDeleteUrl($oPromoProduct){

        throw new LogicException("Promo products cannot be undeleted.");
    }
}