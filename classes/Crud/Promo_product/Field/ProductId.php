<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Promo_product\Field;

use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\PromoProduct as ModelObject;


class ProductId extends Field implements IFilterableField, IEditableField{

    protected $sFieldName = 'product_id';
    protected $sFieldLabel = 'Product';
    private $sIcon = 'package';
    private $sPlaceHolder = '';
    private $sGetter = 'getProductId';



    function getDataType():string
    {
        return 'string';
    }
    function getFieldName()
    {
        return $this->sFieldName;
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class=""><a href="/product/edit?id='.$oModelObject->{$this->sGetter}().'">'.$oModelObject->{$this->sGetter}().'</a></td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon, $bReadonly);
    }
}
