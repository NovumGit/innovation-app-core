<?php

namespace Crud;

use AdminModules\Field\AbstractStyleHelper;
use Hurah\Types\Type\Json;
use Hurah\Types\Type\Url;
use Core\DeferredAction;
use Core\Paginate;
use Core\Reflector;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Core\Utils;
use Exception;
use Exception\ClassNotFoundException;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Iterator;
use IteratorAggregate;
use Model\Logging\Except_log;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;
use Model\Setting\CrudManager\CrudView;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use ReflectionClass;
use ReflectionException;
use Ui\RenderEditConfig;

abstract class FormManager extends BaseManager implements IFormManager
{
    /**
     * FormManager constructor.
     * @param null $sCrudLayoutKey
     */
    function __construct($sCrudLayoutKey = null)
    {
        try
        {
            $sName = (new ReflectionClass($this))->getName();
            $this->sCrudLayoutKey = ($sCrudLayoutKey) ? $sName . '-' . $sCrudLayoutKey : $sName;
            CrudViewManager::init($this);
        } catch (Exception $e)
        {
            Except_log::register($e, true);
        }
    }

    private $oPaginate = null;
    private $aOverviewData;
    private $oOverviewQueryObject;

    abstract function getEditFormTitle(): string;

    abstract function getEntityTitle(): string;

    abstract function getNewFormTitle(): string;

    abstract function getModel(array $aData = null);

    function getApiEndpoint(): string
    {
        $sModule = strtolower($this->getModuleName());
        return '/v2/rest/' . $sModule;
    }

    function setCrudLayoutKey(string $sCrudLayoutkey): void
    {
        $this->sCrudLayoutKey = $sCrudLayoutkey;
    }

    function getCrudConfig(): ?CrudConfig
    {
        return CrudConfigQuery::create()->findOneByManagerName(get_class($this));
    }

    /**
     * @return string
     */
    function getModuleName(): string
    {
        $oReflector = new Reflector($this);

        //$oReflector = new \ReflectionClass($this);
        $aParts = array_reverse(explode(DIRECTORY_SEPARATOR, dirname($oReflector->getFileName())));
        return $aParts[0];
    }

    function getOverviewUrl(): string
    {
        // Het is de bedoeling dat deze worden overschreven in de subklasse maar dat is optioneel
        return '';
    }

    function getCreateNewUrl(): string
    {
        // Het is de bedoeling dat deze worden overschreven in de subklasse maar dat is optioneel
        return '';
    }

    function getOverviewData(): IteratorAggregate
    {
        return $this->aOverviewData;
    }

    function getPaginate(): ?Paginate
    {
        return $this->oPaginate;
    }

    /**
     * @param $aData
     * @return ActiveRecordInterface
     */
    abstract function store(array $aData = null);

    function storeCustomFields(ActiveRecordInterface $oObject, array $aData)
    {
        $aAllFields = $this->getFieldIterator();

        foreach ($aAllFields as $oField)
        {
            if ($oField instanceof ICustomCrudField)
            {
                $oField->save($oObject, $aData);
            }
        }
    }

    function beforeDelete(ActiveRecordInterface $oItem): bool
    {
        return true;
    }

    function afterDelete(ActiveRecordInterface $oItem): bool
    {
        return true;
    }

    function beforeUpdate(array $aData = null)
    {

    }

    function beforeInsert(array $aData = null)
    {

    }

    function afterInsert(array $aData = null, ActiveRecordInterface $oActiveRecord = null)
    {

    }

    function afterUpdate(array $aData = null, ActiveRecordInterface $oActiveRecord = null)
    {

    }

    /**
     * @param int $iId
     * @throws ClassNotFoundException
     * @throws LogicException
     */
    function delete(int $iId)
    {
        if ($this instanceof IApiExposable)
        {
            $oItem = $this->getQueryObject()->findOneBy('id', $iId);

            if ($oItem instanceof ActiveRecordInterface)
            {
                $this->beforeDelete($oItem);

                $oItem->delete();

                $this->afterDelete($oItem);
            } else
            {
                throw new ClassNotFoundException("Could not find record with id $iId.");
            }
        } else
        {
            $oReflector = new Reflector($this);
            throw new InvalidArgumentException("Manager " . $oReflector->getShortName() . " must implement IApiExposable.");
        }
    }

    function save(array $aData = null): ActiveRecordInterface
    {
        if (isset($aData['id']))
        {
            $this->beforeUpdate($aData);
        } else
        {
            $this->beforeInsert($aData);
        }

        $oObject = $this->store($aData);
        if (isset($aData['id']))
        {
            $this->afterInsert($aData, $oObject);
        } else
        {
            $this->afterUpdate($aData, $oObject);
        }
        return $oObject;
    }

    /**
     * @param $aPostedData
     * @param null $sFormLayoutKey
     * @return array|null
     * @throws PropelException
     * @throws ReflectionException
     */
    function getValidationErrors($aPostedData, $sFormLayoutKey = null): ?array
    {
        if ($sFormLayoutKey)
        {
            $aEditFields = $this->getConfiguredEditFields($sFormLayoutKey);
        } else
        {
            $aEditFields = $this->getDefaultEditFields();
        }

        $aFields = $this->loadCrudFields($aEditFields);
        $aOut = [];
        foreach ($aFields as $oField)
        {

            $aFieldValidationResponses = $oField->validate($aPostedData);

            if (is_array($aFieldValidationResponses) && !empty($aFieldValidationResponses))
            {
                foreach ($aFieldValidationResponses as $sError)
                {
                    $aOut[] = $sError;
                }
            }
        }

        return empty($aOut) ? null : $aOut;
    }

    /**
     * @param $aPostedData
     * @param null $sFormLayoutKey
     * @return bool
     */
    function isValid($aPostedData, $sFormLayoutKey = null)
    {
        $aErrors = $this->registerValidationErrorStatusMessages($aPostedData, $sFormLayoutKey, ['show_status_messages' => false]);
        return empty($aErrors);
    }

    /**
     * @param $aPostedData
     * @param null $sFormLayoutKey
     * @param array $aOptions
     * @return array
     * @throws PropelException
     * @throws ReflectionException
     */
    function registerValidationErrorStatusMessages($aPostedData, $sFormLayoutKey = null, $aOptions = []):array
    {
        $bShowStatusMessages = $aOptions['show_status_messages'] ?? true;

        $aEnabledEditFields = $aErrors = [];
        if ($sFormLayoutKey !== null)
        {
            $aEnabledEditFields = $this->getConfiguredEditFields($sFormLayoutKey);
        }
        if (empty($aEnabledEditFields))
        {
            $aEnabledEditFields = $this->getDefaultEditFields();
        }

        $aFields = $this->loadCrudFields($aEnabledEditFields);
        $bFirstMessageRegistered = false;

        foreach ($aFields as $oField)
        {

            if (!$oField instanceof Field)
            {
                throw new LogicException("Field veld moet een instance van Crud\\Field zijn, kreeg nu " . get_class($oField));
            }

            if($oField->defaultValue())
            {
                if($aPostedData['id'] ?? null && !isset($aPostedData[$oField->getFieldName()]))
                {
                    $aPostedData[$oField->getFieldName()] = $oField->defaultValue();
                }

            }

            $aFieldValidationResponses = $oField->validate($aPostedData);

            if ($aFieldValidationResponses !== false && !$aFieldValidationResponses === array())
            {
                throw new LogicException(get_class($oField) . "->validate mag alleen false of een array terug geven, niets anders.");
            }

            if (!empty($aFieldValidationResponses))
            {
                if (!$bFirstMessageRegistered)
                {
                    $bFirstMessageRegistered = true;
                    if($bShowStatusMessages)
                    {
                        StatusMessage::danger('De wijzigingen zijn niet opgeslagen want de inhoud van het formulier voldoet niet aan de criteria, pas waar nodig aan en probeer het nog eens.');
                    }
                }
                foreach ($aFieldValidationResponses as $sFieldValidationResponse)
                {
                    $aErrors[] = $sFieldValidationResponse;
                    if($bShowStatusMessages)
                    {
                        StatusMessage::danger($sFieldValidationResponse);
                    }
                }
            }
        }
        return $aErrors;
    }

    /**
     * @return string
     * @throws ReflectionException
     */
    function getFqClassName(): string
    {
        return (new \ReflectionClass($this))->getName();
    }

    /**
     * @param mixed $mData - Dit kan een propel Query object zijn, dan neemt het systeem ook gelijk paginering etc mee, of alleen de data (PropelQueryObject->find() resultaat)
     * @param int|null $iCurrentPage
     * @param int $iItemsPP
     * @return void
     */
    function setOverviewData(IteratorAggregate $mData = null, int $iCurrentPage = null, int $iItemsPP = 50): void
    {
        // \IteratorAggregate::
        if ($mData instanceof ModelCriteria)
        {

            $this->oOverviewQueryObject = $mData;
            $this->oPaginate = new Paginate($mData, $iCurrentPage, $iItemsPP);
            $this->aOverviewData = $this->oPaginate->getDataIterator();
        } else
        {
            if ($mData)
            {
                $this->aOverviewData = $mData;
            }
        }
    }

    /**
     * Returns the url of a page that allows you to configure a form this manager represents
     *
     * @param string $sFormName
     * @param string|null $sFormTitle
     * @return Url
     */
    function getFormEditUrl(string $sFormName, string $sFormTitle = null): Url
    {
        DeferredAction::register('after_editor_changed_url', Utils::getRequestUri());

        $oReflector = new Reflector($this);
        $aArgs = [
            'do_after_save' => 'after_editor_changed_url', 'module' => $this->getModuleName(), 'manager' => str_replace('Crud', '', $oReflector->getShortName()), 'name' => $sFormName, 'title' => $sFormTitle];
        return new Url('/generic/crud/editor_edit?' . http_build_query($aArgs));
    }

    /**
     * Returns the url of the page that allows you to configure the columns / layout of this view
     * @param CrudView $oCrudView
     * @return Url
     * @throws ReflectionException
     */
    function getOverviewEditUrl(CrudView $oCrudView): Url
    {

        DeferredAction::register('after_overview_changed_url', Utils::getRequestUri());
        $aArgs = [
            'r' => 'after_overview_changed_url',
            'module' => $this->getModuleName(),
            'manager' => preg_replace('/^Crud/', '', (new ReflectionClass($this))->getShortName()),
            'tab' => $oCrudView->getId()

        ];
        return new Url('/generic/crud/create?' . http_build_query($aArgs));
    }

    function getOverviewHtml(array $aFields = null, array $aArguments = null)
    {
        $sTableAttributes = '';
        $sTableClasses = '';
        if (isset($aArguments['table_attributes']))
        {
            $sTableAttributes = $aArguments['table_attributes'];
        }
        if (isset($aArguments['table_class']))
        {
            $sTableClasses = ' ' . $aArguments['table_class'];
        }
        $aOverviewHtml = [];

        if ($aFields)
        {
            $aOverviewFields = $aFields;
        } else
        {
            $aOverviewFields = $this->getDefaultOverviewFields();
        }
        $aCrudFields = $this->loadCrudFields($aOverviewFields);

        $aOverviewHtml[] = '<div class="table-responsive generated">';
        $aOverviewHtml[] = '    <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">';
        $aOverviewHtml[] = '        <table class="table allcp-form footable' . $sTableClasses . '" ' . $sTableAttributes . '>';
        $aOverviewHtml[] = '            <thead>';
        $aOverviewHtml[] = '                <tr class="bg-light">';

        foreach ($aCrudFields as $oCrudField)
        {
            if (!$oCrudField instanceof Field)
            {
                throw new LogicException("Field should be an instance of \\Crud\\Field in " . __METHOD__);
            }
            $aOverviewHtml[] = $oCrudField->getOverviewHeader();
        }
        $aOverviewHtml[] = '                </tr>';
        $aOverviewHtml[] = '            </thead>';
        $aOverviewHtml[] = '            <tbody>';
        if (!empty($this->aOverviewData))
        {
            foreach ($this->aOverviewData as $oData)
            {
                $sRowAttributes = '';
                if (is_array($oData) && isset($oData['id']))
                {
                    $sRowAttributes = ' data-id="' . $oData['id'] . '" ';
                } else
                {
                    if (is_object($oData) && method_exists($oData, 'getId'))
                    {
                        $sRowAttributes = ' data-id="' . $oData->getId() . '" ';
                    }
                }
                $aOverviewHtml[] = '    <tr' . $sRowAttributes . '>';

                foreach ($aCrudFields as $oCrudField)
                {
                    $aOverviewHtml[] = $oCrudField->getOverviewValue($oData);
                }
                $aOverviewHtml[] = '    </tr>';
            }
        }
        $aOverviewHtml[] = '        </tbody>';

        if (isset($aArguments['table_footer']))
        {
            $aOverviewHtml[] = $aArguments['table_footer']['result'];
        }

        $aOverviewHtml[] = '    </table>';
        $aOverviewHtml[] = '    <!--aftercrud-->'; // don't remove
        if ($this->oPaginate instanceof Paginate)
        {
            $sPaginateHtml = $this->oPaginate->getPaginateHtml();
            $aOverviewHtml[] = $sPaginateHtml;
        }
        $aOverviewHtml[] = '</div>';
        $aOverviewHtml[] = '</div>';
        return join(PHP_EOL, $aOverviewHtml);
    }

    /**
     * The filter gets passed a Field object and returns true when the Field may be included. You can sort right away.
     * Just return the index of the item in your callback instead of false.
     *
     * @param callable|null $callableFilter
     * @return array|null
     */
    function getAllFieldsAsArray(callable $callableFilter = null): ?array
    {
        $aOut = [];
        $aAllFields = $this->getFieldIterator($callableFilter);
        foreach ($aAllFields as $oField)
        {
            $aOut[] = $oField->toArray();
        }
        return $aOut;
    }

    /**
     * @param callable|null $filter
     * @return array
     */
    protected function getAllFieldObjectsArray(callable $filter = null):array
    {
        $aFields = $this->loadCrudFields($this->getAllAvailableFields());

        if (!is_callable($filter))
        {
            return $aFields;
        }

        usort($aFields, function ($oAField, $oBField)
        {
            if (!$oAField instanceof Field)
            {
                throw new LogicException('I am trying to compare fields but i got someting else');
            }
            if (!$oBField instanceof Field)
            {
                throw new LogicException('I am trying to compare fields but i got someting else');
            }

            return strcmp($oAField->getFieldLabel(), $oBField->getFieldLabel());
        });


        $aOut = [];
        $bSortBeforeOut = false;
        foreach ($aFields as $oField)
        {
            $iIndex = $filter($oField, $this);
            if ($iIndex !== false)
            {

                if ($iIndex === true)
                {
                    $aOut[] = $oField;
                } else
                {
                    $bSortBeforeOut = true;
                    $aOut[$iIndex] = $oField;
                }
            }
        }

        if ($bSortBeforeOut)
        {
            ksort($aOut);
        }
        return $aOut;
    }

    /**
     * @param callable|null $filter
     * @return ICrudFieldIterator
     */
    function getFieldIterator(callable $filter = null): ICrudFieldIterator
    {
        $aArray = $this->getAllFieldObjectsArray($filter);
        return new FieldCollection($aArray);
    }

    function getRenderedFormAsJson($sRendering = null): Json
    {
        return new Json($this->getRenderedFormAsArray($sRendering));
    }

    function getTableMap(): TableMap
    {
        return new TableMap();
    }

    function getRenderedFormAsArray($sRendering = null): array
    {
        $sManagerName = get_class($this);

        $oCrudEditor = CrudEditorManager::getCrudEditor($sManagerName, $sRendering);

        if (!$oCrudEditor instanceof CrudEditor)
        {
            $oCrudEditor = CrudEditorManager::generateNew($sManagerName, $sRendering);
        }

        $aCrudEditorBlocks = $oCrudEditor->getCrudEditorBlocksSorted();

        $aResult = ['form_id' => $sRendering, 'blocks' => []];
        $iPageNum = 1;
        $iFieldNum = 1;
        foreach ($aCrudEditorBlocks as $oCrudEditorBlock)
        {
            $iBlockFieldNum = 1;
            $aBlock = ['title' => $oCrudEditorBlock->getTitle(), 'sequence' => $iPageNum, 'fields' => []];
            $iPageNum++;

            $aCrudEditorBlockFields = $oCrudEditorBlock->getFieldsSorted();

            foreach ($aCrudEditorBlockFields as $oCrudEditorBlockField)
            {

                $sFieldClassname = $oCrudEditorBlockField->getField();
                $oField = new $sFieldClassname;

                if (!$oField instanceof Field)
                {
                    throw new \LogicException("Expected an instance of Field");
                }
                $aBlockField = $oField->toArray();
                $aBlockField['global_sequence'] = $iFieldNum;
                $aBlockField['block_sequence'] = $iBlockFieldNum;
                $aBlock['fields'][] = $aBlockField;
                $iBlockFieldNum++;
                $iFieldNum++;
            }
            $aResult['blocks'][] = $aBlock;
        }
        return $aResult;
    }

    private function getFieldsAsArray(FieldCollection $oFieldCollection, FieldBlockProperties $oFieldBlockProperties)
    {
        foreach ($oFieldCollection as $oField)
        {

        }
    }

    /**
     * @param $mData
     * @param string $sRendering - name veld uit de crud_editor tabel. Bepaald welke velden er op het scherm komen en in welke volgorde
     * @param RenderEditConfig $oRenderEditConfig
     * @return string
     */
    function getRenderedEditHtml($mData, $sRendering = null, RenderEditConfig $oRenderEditConfig = null): string
    {
        if ($oRenderEditConfig == null)
        {
            $oRenderEditConfig = new RenderEditConfig('Form');
        }

        if (!in_array($oRenderEditConfig->getLayout(), ['AllCp', 'Style1', 'Style2', 'Style3', 'StyleFormField', "PlainForm"]))
        {
            throw new LogicException('Argument $oRenderEditConfig->getLayout() must be AllCp, PlainForm, Style1, Style2 or Style3, it now is ' . $oRenderEditConfig->getLayout());
        }


        $sManagerName = get_class($this);
        CrudEditorManager::generateNew($sManagerName, $sRendering);

        $oCrudEditor = CrudEditorManager::getCrudEditor($sManagerName, $sRendering);

        if ($oCrudEditor === null && $oRenderEditConfig instanceof AbstractStyleHelper)
        {
            return $this->getEditHtml($mData, $oRenderEditConfig);
        }
        else
        {

            if ($oCrudEditor === null)
            {
                $sEditFormUrl = $this->getFormEditUrl($sRendering, $oRenderEditConfig->getTitle());
                $sPopupTitle = 'Configure form';

                if($oRenderEditConfig->getTitle())
                {
                    $sPopupTitle = 'Configure "' . $oRenderEditConfig->getTitle() . '"';
                }

                $aButtons = [
                    new StatusMessageButton("Configure", $sEditFormUrl, 'Configure form', 'success')];
                DeferredAction::register('after_editor_changed_url', Utils::getRequestUri());
                StatusModal::info("It seems like you are using this form for the first time and it is not configured yet", $sPopupTitle, $aButtons);
                Utils::redirect($sEditFormUrl);
            }
        }
        return $oRenderEditConfig->getStyleClassHelper()->renderForm($this, $oCrudEditor, $mData, $sRendering);
    }

    /**
     * @param $mData
     * @param RenderEditConfig|null $oRenderEditConfig
     * @return string
     */
    function getEditHtml($mData, RenderEditConfig $oRenderEditConfig = null)
    {
        if (!$oRenderEditConfig instanceof RenderEditConfig)
        {
            $oRenderEditConfig = new RenderEditConfig();
        }

        $aEditHtml = [];
        $iId = null;

        if (method_exists($mData, 'getId'))
        {

            $iId = $mData->getId();
        }

        $sFormTitle = $this->getNewFormTitle();
        if ($iId)
        {
            $sFormTitle = $this->getEditFormTitle();
        }

        $aEditHtml[] = '<input type="hidden" id="fld_do" name="_do" value="Store" />';
        $aEditHtml[] = '<input type="hidden" name="data[id]" value="' . $iId . '" />';

        $oMockBlock = new CrudEditorBlock();
        $oMockBlock->setTitle($sFormTitle);
        $oMockBlock->setWidth(11);

        $aEditHtml[] = $oRenderEditConfig->getStyleClassHelper()->getBlockHeader($oMockBlock, 1);

        $aDefaultEditFields = $this->getDefaultEditFields();
        $aCrudFields = $this->loadCrudFields($aDefaultEditFields);

        foreach ($aCrudFields as $oCrudField)
        {
            $oCrudField->setFieldArrayName($oRenderEditConfig->getFieldArrayName());
            $oCrudField->setRenderEditConfig($oRenderEditConfig);
            $oCrudField->setEditHtmlStyle($oRenderEditConfig->getLayout());
            $aEditHtml[] = $oCrudField->getEditHtml($mData, false);
        }

        if ($oRenderEditConfig->getButtonsEnabled())
        {
            $aEditHtml[] = '        <button type="submit" class="btn ladda-button btn-dark pull-right" data-style="expand-right">';
            $aEditHtml[] = '            <span class="ladda-label">' . Translate::fromCode('Opslaan') . '</span>';
            $aEditHtml[] = '            <span class="ladda-spinner"></span>';
            $aEditHtml[] = '        </button>';

            if ($this instanceof IConfigurableCrud)
            {
                $aEditHtml[] = '            <button name="next_overview" value="1" type="submit" class="btn ladda-button btn-dark pull-right" data-style="expand-right"  style="margin-right:10px;">';
                $aEditHtml[] = '                <span class="ladda-label">' . Translate::fromCode('Opslaan + naar overzicht') . '</span>';
                $aEditHtml[] = '                <span class="ladda-spinner"></span>';
                $aEditHtml[] = '            </button>';
            }
        }
        $aEditHtml[] = $oRenderEditConfig->getStyleClassHelper()->getBlockFooter(1);

        return join(PHP_EOL, $aEditHtml);
    }
}
