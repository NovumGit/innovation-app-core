<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 28-11-19
 * Time: 15:48
 */

namespace Crud;

use Core\Type\InterfaceOasPrimitive;
use DateTime;

interface IField extends IComponent
{
    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param array $aAddVars
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function sortableHeaderField($sFieldLabel, $sFieldDb, $aAddVars = []): string;

    /**
     * @param $sFieldLabel
     * @param $sTdClass
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function nonSortableHeaderField($sFieldLabel, $sTdClass): string;

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param DateTime|string|null $mFieldValue
     * @param bool $bReadOnly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editDatePicker($sFieldLabel, $sFieldDb, $mFieldValue = null, $bReadOnly = false): string;

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param DateTime|string|null $mFieldValue
     * @param bool $bReadOnly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editDateTimePicker($sFieldLabel, $sFieldDb, $mFieldValue = null, $bReadOnly = false): string;

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param string $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @param array $aParameters
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editTextField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, $bReadonly = false, $aParameters = []): string;

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param string $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editColorField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, $bReadonly = false): string;

    /**
     * @param string $sFieldLabel
     * @param string $sFieldTxt
     * @param string $sFieldUrl
     * @param string $sLinkTitle
     * @param array $aExtraArguments
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editLinkField($sFieldLabel, $sFieldTxt, $sFieldUrl, $sLinkTitle, $aExtraArguments = null);

    /**
     * @param string $sFieldLabel
     * @param string $sFieldDb
     * @param string $sFieldValue
     * @param boolean $bReadonly
     * @param string $sPlaceholder
     * @param array $aArguments
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editTextArea($sFieldLabel, $sFieldDb, $sFieldValue, bool $bReadonly, array $aArguments = null, $sPlaceholder = null): string;

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editRichTextField($sFieldLabel, $sFieldDb, $sFieldValue, $bReadonly): string;

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $sPlaceholder
     * @param null $sIcon
     * @param boolean $bReadonly
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editFileField($sFieldLabel, $sFieldDb, $sFieldValue, $sPlaceholder, $sIcon = null, bool $bReadonly = false);

    /**
     * @param ConfigEditPreviewImage $oConfigEditPreviewImage
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editImagePreview(ConfigEditPreviewImage $oConfigEditPreviewImage);

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $bReadonly
     * @param null $sIcon
     * @param array $aParameters
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editBooleanField($sFieldLabel, $sFieldDb, $sFieldValue, $bReadonly, $sIcon = null, $aParameters = []): string;

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $aOptions
     * @param $bReadonly
     * @param null $sIcon
     * @param null $sPlaceHolder
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editLookupField($sFieldLabel, $sFieldDb, $sFieldValue, $aOptions, $bReadonly, $sIcon = null, $sPlaceHolder = null);

    /**
     * @param $sFieldLabel
     * @param $sFieldDb
     * @param $sFieldValue
     * @param $aOptions
     * @param $bReadonly
     * @param null $sIcon
     * @param null $sPlaceHolder
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function editCurrencyField($sFieldLabel, $sFieldDb, $sFieldValue, $aOptions, $bReadonly, $sIcon = null, $sPlaceHolder = null);
}
