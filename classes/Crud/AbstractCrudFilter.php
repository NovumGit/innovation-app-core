<?php
namespace Crud;

use Exception\LogicException;
use Model\Marketing\MailingListsCriteria;
use Model\Setting\CrudManager\CrudViewHiddenFilter;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Model\Setting\CrudManager\FilterOperator;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;

abstract class AbstractCrudFilter
{
    abstract static function getFilters($iListId):array ;

    public static function  applyFilters(ModelCriteria $oQueryObject, $iFilterListId)
    {
        $aFilters = static::getFilters($iFilterListId);

        if(!empty($aFilters))
        {
            foreach($aFilters as $aFilter)
            {
                if(!$aFilter['object'] instanceof ICrudFilterModel)
                {
                    throw new LogicException("Expected an instance of ICrudFilterModel");
                }
                $sFilterMethod = 'filterBy'.$aFilter['filter_field']['classname'];

                if($sFilterMethod == 'filterByIsDeleted')
                {
                    $sFilterMethod = 'filterByItemDeleted';
                }
                $sFieldClassname = $aFilter['filter_field']['namespaced_classname'];
                $oField = new $sFieldClassname();

                $oFilterOperator = $aFilter['object']->getFilterOperator();
                if(!$oFilterOperator instanceof FilterOperator)
                {
                    throw new LogicException("Expected an instance of FilterOperator.");
                }

                $sOperator = $oFilterOperator->getName();

                if(method_exists($oField, 'addOverviewFilter'))
                {
                    $oQueryObject = $oField->addOverviewFilter($oQueryObject, $aFilter['actual_value'], self::operatorToCriteria($sOperator));
                }
                else
                {
                    if($sOperator == 'smaller_then_or_equal_or_null')
                    {
                        if(!$aFilter['filter_field']['field_object'] instanceof Field)
                        {
                            throw new LogicException('Expected an instance of Field');
                        }
                        // Probably does not cover 100% of the column names
                        $sColumnName = $aFilter['filter_field']['classname'];
                        $sModelName = $oQueryObject->getModelShortName();
                        $oQueryObject->where("($sModelName.$sColumnName <= ? OR $sModelName.$sColumnName IS NULL)", $aFilter['actual_value']);
                    }
                    else
                    {
                        $oQueryObject->$sFilterMethod($oFilterOperator->formatValue($aFilter['actual_value']), $oFilterOperator->toCriteria());
                    }
                }
            }
        }
        return $oQueryObject;
    }

    public static function filterField(ModelCriteria $oCriteria, string $Operator, string $sValue):ModelCriteria
    {


    }


    protected static function getVisibleFilterValue(IFilterableField $oField, $sActualValue, ICrudFilterModel $oInterfaceCrudfilterModel)
    {
        $oFilterOperator = null;

        if($oInterfaceCrudfilterModel instanceof CrudViewHiddenFilter || $oInterfaceCrudfilterModel instanceof MailingListsCriteria)
        {
            $oFilterOperator = FilterOperatorQuery::create()->findOneById($oInterfaceCrudfilterModel->getFilterOperatorId());
        }
        $sVisibleValue = null;

        $bIsInorNotInField = ($oFilterOperator instanceof FilterOperator && in_array($oFilterOperator->getName(), ['in', 'not_in']));

        if($oField->getDataType() == 'lookup' && !$bIsInorNotInField)
        {
            if(!$oField instanceof IFilterableLookupField)
            {
                throw new LogicException("Field should implement InterfaceFilterableLookupField but it does not.");
            }
            $sVisibleValue = $oField->getVisibleValue($sActualValue);
        }
        else if(in_array($oField->getDataType(), ['bsn', 'number', 'date', 'string']))
        {
            $sVisibleValue = $sActualValue;
        }
        else if($oField->getDataType() == 'boolean')
        {
            $sVisibleValue = $sActualValue ? 'Ja' : 'Nee';
        }
        else if(in_array($oField->getDataType(), ['string', 'lookup']) && $bIsInorNotInField)
        {
            if($oInterfaceCrudfilterModel instanceof ICrudFilterModel) {

                $oFilterOperator = $oInterfaceCrudfilterModel->getFilterOperator();

                if(!$oFilterOperator instanceof FilterOperator)
                {
                    throw new LogicException("Expected an instance of FilterOperator.");
                }

                if ($oField instanceof IFilterableLookupField) {
                    $aVisibleValues = [];
                    $aIds = explode(',', $sActualValue);
                    foreach ($aIds as $iId)
                    {
                        $aVisibleValues[] = $oField->getVisibleValue($iId);
                    }
                    $sActualValue = join(',', $aVisibleValues);
                }

                if($oFilterOperator->getName() == 'in' || $oFilterOperator->getName() == 'not_in')
                {
                    $sVisibleValue = str_replace(',', ' of ', $sActualValue);
                }
                else
                {
                    $sVisibleValue = $sActualValue;
                }
            }
            else
            {
                throw new LogicException("\$oCrudViewVisibleFilter should be an instance of CrudViewVisibleFilter or CrudViewHiddenFilter, this indicates a bug.");
            }
        }
        else
        {
            throw new LogicException("Datatype {$oField->getDataType()} is not supported yet.");
        }
        return $sVisibleValue;
    }
    protected static function buildFilterArray(ObjectCollection $aFilterIterator)
    {
        $aFilters = [];

        if(!$aFilterIterator->isEmpty())
        {
            foreach($aFilterIterator as $oFilterRecord)
            {
                $sFieldClassName = $oFilterRecord->getFilterName();
                $oField = new $sFieldClassName();
                if(!$oField instanceof Field)
                {
                    throw new LogicException("Expected an instance of field but got ".get_class($oField).".");
                }
                if(!$oField instanceof IFilterableField)
                {
                    throw new LogicException("Field ".$oField->getFieldTitle()." should implement IFilterableField but it does not.");
                }

                $sVisibleValue = self::getVisibleFilterValue($oField, $oFilterRecord->getFilterValue(), $oFilterRecord);
                $sActualValue = $oFilterRecord->getFilterValue();
                if($sActualValue == 'today')
                {
                    $sActualValue = date("Y-m-d");
                }
                else if($sActualValue == 'now')
                {
                    $sActualValue = date("Y-m-d H:i:s");
                }
                $aFilters[] = [
                    'object' => $oFilterRecord,
                    'filter_field' =>
                        [
                            'namespaced_classname' => $sFieldClassName,
                            'classname' => basename(str_replace('\\', '/', $sFieldClassName)),
                            'field_object' => $oField
                        ],
                    'visible_value' => $sVisibleValue,
                    'actual_value' => $sActualValue,
                ];
            }
        }
        return $aFilters;
    }
}
