<?php
namespace Crud\Branche\Base;

use Core\Utils;
use Crud;
use Crud\Branche\FieldIterator;
use Crud\Branche\Field\Description;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\MasterTable\Branche;
use Model\Setting\MasterTable\BrancheQuery;
use Model\Setting\MasterTable\Map\BrancheTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Branche instead if you need to override or add functionality.
 */
abstract class CrudBrancheManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BrancheQuery::create();
	}


	public function getTableMap(): BrancheTableMap
	{
		return new BrancheTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Branche";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_branche toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_branche aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Branche
	 */
	public function getModel(array $aData = null): Branche
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBrancheQuery = BrancheQuery::create();
		     $oBranche = $oBrancheQuery->findOneById($aData['id']);
		     if (!$oBranche instanceof Branche) {
		         throw new LogicException("Branche should be an instance of Branche but got something else." . __METHOD__);
		     }
		     $oBranche = $this->fillVo($aData, $oBranche);
		}
		else {
		     $oBranche = new Branche();
		     if (!empty($aData)) {
		         $oBranche = $this->fillVo($aData, $oBranche);
		     }
		}
		return $oBranche;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Branche
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Branche
	{
		$oBranche = $this->getModel($aData);


		 if(!empty($oBranche))
		 {
		     $oBranche = $this->fillVo($aData, $oBranche);
		     $oBranche->save();
		 }
		return $oBranche;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Branche $oModel
	 * @return Branche
	 */
	protected function fillVo(array $aData, Branche $oModel): Branche
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
