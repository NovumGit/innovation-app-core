<?php
namespace Crud\Branche\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Branche\ICollectionField as BrancheField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Branche\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BrancheField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BrancheField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BrancheField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BrancheField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BrancheField
	{
		return current($this->aFields);
	}
}
