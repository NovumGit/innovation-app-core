<?php
namespace Crud;


/**
 * By implementing this interface the system knows the field is only available in web / http context.
 * Field.isWebOnly will automagically return true when the interface is implemented
 *
 * Interface InterfaceAppViewField
 * @package Crud
 */
interface IWebOnlyField extends IField {

    function isConfigurable():bool;
    function getConfigUrl():?string;
}


