<?php
namespace Crud\SiteBanner\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteBanner\FieldIterator;
use Crud\SiteBanner\Field\AboutTxt;
use Crud\SiteBanner\Field\Alignment;
use Crud\SiteBanner\Field\BuyNowLink;
use Crud\SiteBanner\Field\FileExt;
use Crud\SiteBanner\Field\HasBuyNow;
use Crud\SiteBanner\Field\HasInfoButton;
use Crud\SiteBanner\Field\InfoLink;
use Crud\SiteBanner\Field\SiteId;
use Crud\SiteBanner\Field\Slogan;
use Crud\SiteBanner\Field\SubText;
use Exception\LogicException;
use Model\Cms\Map\SiteBannerTableMap;
use Model\Cms\SiteBanner;
use Model\Cms\SiteBannerQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteBanner instead if you need to override or add functionality.
 */
abstract class CrudSiteBannerManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteBannerQuery::create();
	}


	public function getTableMap(): SiteBannerTableMap
	{
		return new SiteBannerTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteBanner";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_banner toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_banner aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'AboutTxt', 'Alignment', 'Slogan', 'SubText', 'HasInfoButton', 'InfoLink', 'HasBuyNow', 'BuyNowLink', 'FileExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'AboutTxt', 'Alignment', 'Slogan', 'SubText', 'HasInfoButton', 'InfoLink', 'HasBuyNow', 'BuyNowLink', 'FileExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteBanner
	 */
	public function getModel(array $aData = null): SiteBanner
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteBannerQuery = SiteBannerQuery::create();
		     $oSiteBanner = $oSiteBannerQuery->findOneById($aData['id']);
		     if (!$oSiteBanner instanceof SiteBanner) {
		         throw new LogicException("SiteBanner should be an instance of SiteBanner but got something else." . __METHOD__);
		     }
		     $oSiteBanner = $this->fillVo($aData, $oSiteBanner);
		}
		else {
		     $oSiteBanner = new SiteBanner();
		     if (!empty($aData)) {
		         $oSiteBanner = $this->fillVo($aData, $oSiteBanner);
		     }
		}
		return $oSiteBanner;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteBanner
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteBanner
	{
		$oSiteBanner = $this->getModel($aData);


		 if(!empty($oSiteBanner))
		 {
		     $oSiteBanner = $this->fillVo($aData, $oSiteBanner);
		     $oSiteBanner->save();
		 }
		return $oSiteBanner;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteBanner $oModel
	 * @return SiteBanner
	 */
	protected function fillVo(array $aData, SiteBanner $oModel): SiteBanner
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['about_txt'])) {
		     $oField = new AboutTxt();
		     $mValue = $oField->sanitize($aData['about_txt']);
		     $oModel->setAboutTxt($mValue);
		}
		if(isset($aData['alignment'])) {
		     $oField = new Alignment();
		     $mValue = $oField->sanitize($aData['alignment']);
		     $oModel->setAlignment($mValue);
		}
		if(isset($aData['slogan'])) {
		     $oField = new Slogan();
		     $mValue = $oField->sanitize($aData['slogan']);
		     $oModel->setSlogan($mValue);
		}
		if(isset($aData['sub_text'])) {
		     $oField = new SubText();
		     $mValue = $oField->sanitize($aData['sub_text']);
		     $oModel->setSubText($mValue);
		}
		if(isset($aData['has_info_button'])) {
		     $oField = new HasInfoButton();
		     $mValue = $oField->sanitize($aData['has_info_button']);
		     $oModel->setHasInfoButton($mValue);
		}
		if(isset($aData['info_link'])) {
		     $oField = new InfoLink();
		     $mValue = $oField->sanitize($aData['info_link']);
		     $oModel->setInfoLink($mValue);
		}
		if(isset($aData['has_buy_now'])) {
		     $oField = new HasBuyNow();
		     $mValue = $oField->sanitize($aData['has_buy_now']);
		     $oModel->setHasBuyNow($mValue);
		}
		if(isset($aData['buy_now_link'])) {
		     $oField = new BuyNowLink();
		     $mValue = $oField->sanitize($aData['buy_now_link']);
		     $oModel->setBuyNowLink($mValue);
		}
		if(isset($aData['file_ext'])) {
		     $oField = new FileExt();
		     $mValue = $oField->sanitize($aData['file_ext']);
		     $oModel->setFileExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
