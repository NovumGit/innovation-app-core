<?php
namespace Crud\SiteBanner\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteBanner\ICollectionField as SiteBannerField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteBanner\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteBannerField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteBannerField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteBannerField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteBannerField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteBannerField
	{
		return current($this->aFields);
	}
}
