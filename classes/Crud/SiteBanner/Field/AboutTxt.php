<?php
namespace Crud\SiteBanner\Field;

use Crud\SiteBanner\Field\Base\AboutTxt as BaseAboutTxt;

/**
 * Skeleton subclass for representing about_txt field from the site_banner table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class AboutTxt extends BaseAboutTxt
{
}
