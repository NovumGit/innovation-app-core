<?php
namespace Crud\SiteBanner\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteBanner\ICollectionField;

/**
 * Base class that represents the 'has_info_button' crud field from the 'site_banner' table.
 * This class is auto generated and should not be modified.
 */
abstract class HasInfoButton extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'has_info_button';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHasInfoButton';
	protected $sFqModelClassname = '\\\Model\Cms\SiteBanner';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
