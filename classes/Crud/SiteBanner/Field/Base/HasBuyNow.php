<?php
namespace Crud\SiteBanner\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteBanner\ICollectionField;

/**
 * Base class that represents the 'has_buy_now' crud field from the 'site_banner' table.
 * This class is auto generated and should not be modified.
 */
abstract class HasBuyNow extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'has_buy_now';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHasBuyNow';
	protected $sFqModelClassname = '\\\Model\Cms\SiteBanner';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
