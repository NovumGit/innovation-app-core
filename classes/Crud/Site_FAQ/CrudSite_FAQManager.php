<?php
namespace Crud\Site_FAQ;

use Core\StatusMessage;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Exception\LogicException;
use Model\Cms\SiteFAQ;
use Model\Cms\SiteFAQQuery;

class CrudSite_FAQManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'site_faq';
    }
    function getNewFormTitle():string
    {
        return 'FAQ toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'FAQ bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/faq/overview?site='.$_GET['site'];
        }
        return '/cms/faq/overview?site='.$this->getArgument('site');
    }
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/faq/edit?site='.$_GET['site'];
        }
        return '/cms/faq/edit?site='.$this->getArgument('site');
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Question',
            'Answer',
            'Edit',
            'Delete'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Question',
            'Answer'
        ];
    }
    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new SiteFAQQuery();
            $oSiteFAQ = $oQuery->findOneById($aData['id']);
            if(!$oSiteFAQ instanceof SiteFAQ)
            {
                throw new LogicException("SiteFAQ should be an instance of Site FAQ but got ".get_class($oSiteBlog)." in ".__METHOD__);
            }
        }
        else
        {
            $oSiteFAQ = new SiteFAQ();
            $oSiteFAQ = $this->fillVo($oSiteFAQ, $aData);
        }
        return $oSiteFAQ;
    }

    function store(array $aData = null)
    {
        $oSiteFAQ = $this->getModel($aData);

        if(!empty($oSiteFAQ))
        {
            $oSiteFAQ = $this->fillVo($oSiteFAQ, $aData);
            $oSiteFAQ->save();
        }
        return $oSiteFAQ;
    }
    function delete($iFAQId)
    {
        $oFAQModel = SiteFAQQuery::create()->findOneById($iFAQId);
        $oFAQModel->delete();
        StatusMessage::success("FAQ verwijderd.");

    }
    function fillVo(SiteFAQ $oSiteFAQ, $aData)
    {
        if(isset($aData['language_id']))
        {
            $oSiteFAQ->setLanguageId($aData['language_id']);
        }
        if(isset($aData['site_faq_category_id']))
        {
            $oSiteFAQ->setSiteFaqCategoryId($aData['site_faq_category_id']);
        }
        if(isset($aData['site_id']))
        {
            $oSiteFAQ->setSiteId($aData['site_id']);
        }
        if(isset($aData['question']))
        {
            $oSiteFAQ->setQuestion($aData['question']);
        }
        if(isset($aData['answer']))
        {
            $oSiteFAQ->setAnswer($aData['answer']);
        }
        return $oSiteFAQ;
    }
}
