<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_FAQ\Field;

use Core\Translate;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Cms\SiteFAQ as ModelObject;


class Answer extends Field implements IFilterableField, IEditableField{

    private $sFieldName = 'answer';
    protected $sFieldLabel = 'Antwoord';
    private $sIcon = 'edit';
    private $sPlaceHolder = '';
    private $sGetter = 'getAnswer';


    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(empty($aPostedData['answer']))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode('Answer is a mandatory field');
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->sFieldLabel, $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->sFieldLabel;
    }
    function getEditHtml($mData, $bReadonly)
    {
        if(!$mData instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aArguments = [];
        return $this->editTextArea($this->sFieldLabel, $this->sFieldName, $mData->{$this->sGetter}(), $bReadonly, $aArguments);
    }
}
