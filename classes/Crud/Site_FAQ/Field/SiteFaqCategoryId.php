<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_FAQ\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Cms\Base\SiteFAQCategoryQuery;
use Model\Cms\SiteFAQ as ModelObject;
use Model\Cms\SiteFAQCategory;

class SiteFaqCategoryId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'site_faq_category_id';
    protected $sFieldLabel = 'Categorie';
    private $sIcon = 'tag';
    private $sGetter = 'getSiteFaqCategoryId';

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getLookups($mSelectedItem = null)
    {
        $aCategories = SiteFAQCategoryQuery::create()->orderByName()->find();
        $aOptions = Utils::makeSelectOptions($aCategories, 'getName', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        $oSiteFAQCategory = SiteFAQCategoryQuery::create()->findOneById($iItemId);

        if($oSiteFAQCategory instanceof SiteFAQCategory)
        {
            return $oSiteFAQCategory->getName();
        }
        return '';
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() {
        return true;
    }
    function validate($aPostedData)
    {
        $mResponse = false;
        if(!$aPostedData['site_faq_category_id'])
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("Category is a mandatory field");
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$this->getVisibleValue($oModelObject->{$this->sGetter}()).'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        $aOptions = $this->getLookups();
        return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
