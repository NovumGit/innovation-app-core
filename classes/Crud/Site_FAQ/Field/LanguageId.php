<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\Site_FAQ\Field;

use Core\Translate;
use Core\Utils;
use Crud\Field;
use Exception\InvalidArgumentException;
use Crud\IFilterableLookupField;
use Model\Cms\SiteFAQ as ModelObject;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Language as LanguageModel;

class LanguageId extends Field implements IFilterableLookupField{

    protected $sFieldName = 'language_id';
    protected $sFieldLabel = 'Taal';
    private $sIcon = 'globe';
    private $sGetter = 'getLanguageId';

    function getFieldName()
    {
        return $this->getFieldName();
    }
    function getLookups($mSelectedItem = null)
    {
        $aLanguages = LanguageQuery::create()->orderByDescription()->find();
        $aOptions = Utils::makeSelectOptions($aLanguages, 'getDescription', $mSelectedItem);
        return $aOptions;
    }
    function getVisibleValue($iItemId)
    {
        $oLanguage = LanguageQuery::create()->findOneById($iItemId);
        if($oLanguage instanceof LanguageModel)
        {
            return $oLanguage->getDescription();
        }
        return '';
    }
    function getDataType():string
    {
        return 'lookup';
    }
    function hasValidations() {
        return true;
    }
    function validate($aPostedData)
    {
        $mResponse = false;

        if(empty($mResponse))
        {
            $mResponse = [];
            $mResponse[] = Translate::fromCode("Language id is a mandatory field.");
        }

        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$this->getVisibleValue($oModelObject->{$this->sGetter}()).'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $aOptions = $this->getLookups();return $this->editLookupField($this->getTranslatedTitle(), $this->sFieldName, $oModelObject->{$this->sGetter}(), $aOptions, $bReadonly, $this->sIcon, 'Maak een keuze');
    }
}
