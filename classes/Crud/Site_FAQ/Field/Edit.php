<?php
namespace Crud\Site_FAQ\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Cms\SiteFAQ as ModelObject;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oModelObject){

        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException('Expected an instance of ModelObject but got '.get_class($oModelObject));
        }

        return '/cms/faq/edit?site='.$this->getArgument('site').'&id='.$oModelObject->getId();
    }
}