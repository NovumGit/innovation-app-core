<?php
namespace Crud\Component_table;

/**
 * Skeleton subclass for representing a Component_table.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudComponent_tableManager extends Base\CrudComponent_tableManager
{
}
