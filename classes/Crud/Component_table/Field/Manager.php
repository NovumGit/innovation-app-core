<?php
namespace Crud\Component_table\Field;

use Crud\Component_table\Field\Base\Manager as BaseManager;

/**
 * Skeleton subclass for representing manager field from the component_table table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Manager extends BaseManager
{
}
