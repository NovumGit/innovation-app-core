<?php
namespace Crud\Component_table\Field\Base;

use Crud\Component_table\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'layout_key' crud field from the 'component_table' table.
 * This class is auto generated and should not be modified.
 */
abstract class LayoutKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'layout_key';
	protected $sFieldLabel = 'Layout key';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLayoutKey';
	protected $sFqModelClassname = '\Model\System\LowCode\Table\Component_table';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
