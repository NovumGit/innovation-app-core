<?php
namespace Crud\Component_table\Field\Base;

use Crud\Component_table\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'manager' crud field from the 'component_table' table.
 * This class is auto generated and should not be modified.
 */
abstract class Manager extends GenericLookup implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'manager';
	protected $sFieldLabel = 'Data source';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getManager';
	protected $sFqModelClassname = '\Model\System\LowCode\Table\Component_table';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
