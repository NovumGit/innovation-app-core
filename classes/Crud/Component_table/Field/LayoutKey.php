<?php
namespace Crud\Component_table\Field;

use Crud\Component_table\Field\Base\LayoutKey as BaseLayoutKey;

/**
 * Skeleton subclass for representing layout_key field from the component_table table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class LayoutKey extends BaseLayoutKey
{
}
