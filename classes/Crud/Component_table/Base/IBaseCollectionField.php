<?php
namespace Crud\Component_table\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_table\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
