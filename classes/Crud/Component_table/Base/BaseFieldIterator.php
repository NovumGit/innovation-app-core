<?php
namespace Crud\Component_table\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_table\ICollectionField as Component_tableField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_table\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_tableField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_tableField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_tableField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_tableField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_tableField
	{
		return current($this->aFields);
	}
}
