<?php
namespace Crud\Component_table\Base;

use Core\Utils;
use Crud;
use Crud\Component_table\FieldIterator;
use Crud\Component_table\Field\LayoutKey;
use Crud\Component_table\Field\Manager;
use Crud\Component_table\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Table\Component_table;
use Model\System\LowCode\Table\Component_tableQuery;
use Model\System\LowCode\Table\Map\Component_tableTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_table instead if you need to override or add functionality.
 */
abstract class CrudComponent_tableManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_tableQuery::create();
	}


	public function getTableMap(): Component_tableTableMap
	{
		return new Component_tableTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Table component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_table";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_table toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_table aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'LayoutKey', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'LayoutKey', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_table
	 */
	public function getModel(array $aData = null): Component_table
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_tableQuery = Component_tableQuery::create();
		     $oComponent_table = $oComponent_tableQuery->findOneById($aData['id']);
		     if (!$oComponent_table instanceof Component_table) {
		         throw new LogicException("Component_table should be an instance of Component_table but got something else." . __METHOD__);
		     }
		     $oComponent_table = $this->fillVo($aData, $oComponent_table);
		}
		else {
		     $oComponent_table = new Component_table();
		     if (!empty($aData)) {
		         $oComponent_table = $this->fillVo($aData, $oComponent_table);
		     }
		}
		return $oComponent_table;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_table
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_table
	{
		$oComponent_table = $this->getModel($aData);


		 if(!empty($oComponent_table))
		 {
		     $oComponent_table = $this->fillVo($aData, $oComponent_table);
		     $oComponent_table->save();
		 }
		return $oComponent_table;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_table $oModel
	 * @return Component_table
	 */
	protected function fillVo(array $aData, Component_table $oModel): Component_table
	{
		if(isset($aData['manager'])) {
		     $oField = new Manager();
		     $mValue = $oField->sanitize($aData['manager']);
		     $oModel->setManager($mValue);
		}
		if(isset($aData['layout_key'])) {
		     $oField = new LayoutKey();
		     $mValue = $oField->sanitize($aData['layout_key']);
		     $oModel->setLayoutKey($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
