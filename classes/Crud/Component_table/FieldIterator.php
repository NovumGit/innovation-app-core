<?php
namespace Crud\Component_table;

use Crud\Component_table\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of Component_table crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
