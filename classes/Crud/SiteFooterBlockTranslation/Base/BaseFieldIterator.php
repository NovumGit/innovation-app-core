<?php
namespace Crud\SiteFooterBlockTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\SiteFooterBlockTranslation\ICollectionField as SiteFooterBlockTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\SiteFooterBlockTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param SiteFooterBlockTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param SiteFooterBlockTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof SiteFooterBlockTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(SiteFooterBlockTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): SiteFooterBlockTranslationField
	{
		return current($this->aFields);
	}
}
