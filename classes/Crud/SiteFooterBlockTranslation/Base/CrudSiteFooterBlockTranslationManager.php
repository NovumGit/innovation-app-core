<?php
namespace Crud\SiteFooterBlockTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFooterBlockTranslation\FieldIterator;
use Crud\SiteFooterBlockTranslation\Field\HtmlContents;
use Crud\SiteFooterBlockTranslation\Field\LanguageId;
use Crud\SiteFooterBlockTranslation\Field\SiteFooterBlockId;
use Crud\SiteFooterBlockTranslation\Field\Title;
use Exception\LogicException;
use Model\Cms\Map\SiteFooterBlockTranslationTableMap;
use Model\Cms\SiteFooterBlockTranslation;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFooterBlockTranslation instead if you need to override or add functionality.
 */
abstract class CrudSiteFooterBlockTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFooterBlockTranslationQuery::create();
	}


	public function getTableMap(): SiteFooterBlockTranslationTableMap
	{
		return new SiteFooterBlockTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFooterBlockTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_footer_block_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_footer_block_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteFooterBlockId', 'LanguageId', 'Title', 'HtmlContents'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteFooterBlockId', 'LanguageId', 'Title', 'HtmlContents'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFooterBlockTranslation
	 */
	public function getModel(array $aData = null): SiteFooterBlockTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFooterBlockTranslationQuery = SiteFooterBlockTranslationQuery::create();
		     $oSiteFooterBlockTranslation = $oSiteFooterBlockTranslationQuery->findOneById($aData['id']);
		     if (!$oSiteFooterBlockTranslation instanceof SiteFooterBlockTranslation) {
		         throw new LogicException("SiteFooterBlockTranslation should be an instance of SiteFooterBlockTranslation but got something else." . __METHOD__);
		     }
		     $oSiteFooterBlockTranslation = $this->fillVo($aData, $oSiteFooterBlockTranslation);
		}
		else {
		     $oSiteFooterBlockTranslation = new SiteFooterBlockTranslation();
		     if (!empty($aData)) {
		         $oSiteFooterBlockTranslation = $this->fillVo($aData, $oSiteFooterBlockTranslation);
		     }
		}
		return $oSiteFooterBlockTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFooterBlockTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFooterBlockTranslation
	{
		$oSiteFooterBlockTranslation = $this->getModel($aData);


		 if(!empty($oSiteFooterBlockTranslation))
		 {
		     $oSiteFooterBlockTranslation = $this->fillVo($aData, $oSiteFooterBlockTranslation);
		     $oSiteFooterBlockTranslation->save();
		 }
		return $oSiteFooterBlockTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFooterBlockTranslation $oModel
	 * @return SiteFooterBlockTranslation
	 */
	protected function fillVo(array $aData, SiteFooterBlockTranslation $oModel): SiteFooterBlockTranslation
	{
		if(isset($aData['site_footer_block_id'])) {
		     $oField = new SiteFooterBlockId();
		     $mValue = $oField->sanitize($aData['site_footer_block_id']);
		     $oModel->setSiteFooterBlockId($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['html_contents'])) {
		     $oField = new HtmlContents();
		     $mValue = $oField->sanitize($aData['html_contents']);
		     $oModel->setHtmlContents($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
