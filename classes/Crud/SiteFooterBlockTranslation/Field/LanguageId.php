<?php
namespace Crud\SiteFooterBlockTranslation\Field;

use Crud\SiteFooterBlockTranslation\Field\Base\LanguageId as BaseLanguageId;

/**
 * Skeleton subclass for representing language_id field from the site_footer_block_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class LanguageId extends BaseLanguageId
{
}
