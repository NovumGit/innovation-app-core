<?php
namespace Crud\SiteFooterBlockTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteFooterBlockTranslation\ICollectionField;

/**
 * Base class that represents the 'html_contents' crud field from the 'site_footer_block_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class HtmlContents extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'html_contents';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHtmlContents';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFooterBlockTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
