<?php
namespace Crud\DataModel\Field;

use Crud\DataModel\Field\Base\Name as BaseName;

/**
 * Skeleton subclass for representing name field from the data_model table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Name extends BaseName
{
}
