<?php 
namespace Crud\DataModel\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataModel\Model\DataModel;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataModel)
		{
		     return "//system/data_model/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataModel)
		{
		     return "//data_model?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
