<?php
namespace Crud\DataModel\Field\Base;

use Crud\DataModel\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'php_name' crud field from the 'data_model' table.
 * This class is auto generated and should not be modified.
 */
abstract class PhpName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'php_name';
	protected $sFieldLabel = 'PHP naam';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhpName';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\DataModel';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
