<?php
namespace Crud\DataModel\Field\Base;

use Crud\DataModel\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'api_description' crud field from the 'data_model' table.
 * This class is auto generated and should not be modified.
 */
abstract class ApiDescription extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'api_description';
	protected $sFieldLabel = 'API omschrijving';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getApiDescription';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\DataModel';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['api_description']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "API omschrijving" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
