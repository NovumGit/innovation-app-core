<?php
namespace Crud\DataModel\Field\Base;

use Crud\DataModel\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_ui_component' crud field from the 'data_model' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsUiComponent extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_ui_component';
	protected $sFieldLabel = 'Is UI component';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsUiComponent';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\DataModel';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
