<?php
namespace Crud\DataModel\Field\Base;

use Crud\DataModel\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'api_exposed' crud field from the 'data_model' table.
 * This class is auto generated and should not be modified.
 */
abstract class ApiExposed extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'api_exposed';
	protected $sFieldLabel = 'Beschikbaar in API';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getApiExposed';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Model\DataModel';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['api_exposed']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Beschikbaar in API" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
