<?php
namespace Crud\DataModel\Field;

use Crud\DataModel\Field\Base\IsPersistent as BaseIsPersistent;

/**
 * Skeleton subclass for representing is_persistent field from the data_model table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class IsPersistent extends BaseIsPersistent
{
}
