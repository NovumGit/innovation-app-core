<?php
namespace Crud\DataModel;

/**
 * Skeleton subclass for representing a DataModel.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudDataModelManager extends Base\CrudDataModelManager
{
}
