<?php
namespace Crud\DataModel\Base;

use Core\Utils;
use Crud;
use Crud\DataModel\FieldIterator;
use Crud\DataModel\Field\ApiDescription;
use Crud\DataModel\Field\ApiExposed;
use Crud\DataModel\Field\IsPersistent;
use Crud\DataModel\Field\IsUiComponent;
use Crud\DataModel\Field\ModuleId;
use Crud\DataModel\Field\Name;
use Crud\DataModel\Field\NamespaceName;
use Crud\DataModel\Field\PhpName;
use Crud\DataModel\Field\Title;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataModel\Model\DataModel;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\DataModel\Model\Map\DataModelTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataModel instead if you need to override or add functionality.
 */
abstract class CrudDataModelManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataModelQuery::create();
	}


	public function getTableMap(): DataModelTableMap
	{
		return new DataModelTableMap();
	}


	public function getShortDescription(): string
	{
		return "Modellen bepalen welke soorten data er in het systeem voorkomen.";
	}


	public function getEntityTitle(): string
	{
		return "DataModel";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "data_model toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "data_model aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'NamespaceName', 'PhpName', 'ModuleId', 'Title', 'ApiExposed', 'ApiDescription', 'IsPersistent', 'IsUiComponent'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'NamespaceName', 'PhpName', 'ModuleId', 'Title', 'ApiExposed', 'ApiDescription', 'IsPersistent', 'IsUiComponent'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataModel
	 */
	public function getModel(array $aData = null): DataModel
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataModelQuery = DataModelQuery::create();
		     $oDataModel = $oDataModelQuery->findOneById($aData['id']);
		     if (!$oDataModel instanceof DataModel) {
		         throw new LogicException("DataModel should be an instance of DataModel but got something else." . __METHOD__);
		     }
		     $oDataModel = $this->fillVo($aData, $oDataModel);
		}
		else {
		     $oDataModel = new DataModel();
		     if (!empty($aData)) {
		         $oDataModel = $this->fillVo($aData, $oDataModel);
		     }
		}
		return $oDataModel;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataModel
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataModel
	{
		$oDataModel = $this->getModel($aData);


		 if(!empty($oDataModel))
		 {
		     $oDataModel = $this->fillVo($aData, $oDataModel);
		     $oDataModel->save();
		 }
		return $oDataModel;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataModel $oModel
	 * @return DataModel
	 */
	protected function fillVo(array $aData, DataModel $oModel): DataModel
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['namespace'])) {
		     $oField = new NamespaceName();
		     $mValue = $oField->sanitize($aData['namespace']);
		     $oModel->setNamespaceName($mValue);
		}
		if(isset($aData['php_name'])) {
		     $oField = new PhpName();
		     $mValue = $oField->sanitize($aData['php_name']);
		     $oModel->setPhpName($mValue);
		}
		if(isset($aData['module_id'])) {
		     $oField = new ModuleId();
		     $mValue = $oField->sanitize($aData['module_id']);
		     $oModel->setModuleId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['api_exposed'])) {
		     $oField = new ApiExposed();
		     $mValue = $oField->sanitize($aData['api_exposed']);
		     $oModel->setApiExposed($mValue);
		}
		if(isset($aData['api_description'])) {
		     $oField = new ApiDescription();
		     $mValue = $oField->sanitize($aData['api_description']);
		     $oModel->setApiDescription($mValue);
		}
		if(isset($aData['is_persistent'])) {
		     $oField = new IsPersistent();
		     $mValue = $oField->sanitize($aData['is_persistent']);
		     $oModel->setIsPersistent($mValue);
		}
		if(isset($aData['is_ui_component'])) {
		     $oField = new IsUiComponent();
		     $mValue = $oField->sanitize($aData['is_ui_component']);
		     $oModel->setIsUiComponent($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
