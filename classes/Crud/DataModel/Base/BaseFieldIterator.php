<?php
namespace Crud\DataModel\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataModel\ICollectionField as DataModelField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataModel\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataModelField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataModelField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataModelField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataModelField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataModelField
	{
		return current($this->aFields);
	}
}
