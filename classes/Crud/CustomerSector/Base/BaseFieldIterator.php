<?php
namespace Crud\CustomerSector\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomerSector\ICollectionField as CustomerSectorField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomerSector\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomerSectorField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomerSectorField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomerSectorField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomerSectorField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomerSectorField
	{
		return current($this->aFields);
	}
}
