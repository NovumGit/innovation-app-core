<?php
namespace Crud\CustomerSector\Base;

use Core\Utils;
use Crud;
use Crud\CustomerSector\FieldIterator;
use Crud\CustomerSector\Field\Code;
use Crud\CustomerSector\Field\Description;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\CustomerSector;
use Model\Crm\CustomerSectorQuery;
use Model\Crm\Map\CustomerSectorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomerSector instead if you need to override or add functionality.
 */
abstract class CrudCustomerSectorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomerSectorQuery::create();
	}


	public function getTableMap(): CustomerSectorTableMap
	{
		return new CustomerSectorTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomerSector";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_customer_sector toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_customer_sector aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Description', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomerSector
	 */
	public function getModel(array $aData = null): CustomerSector
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomerSectorQuery = CustomerSectorQuery::create();
		     $oCustomerSector = $oCustomerSectorQuery->findOneById($aData['id']);
		     if (!$oCustomerSector instanceof CustomerSector) {
		         throw new LogicException("CustomerSector should be an instance of CustomerSector but got something else." . __METHOD__);
		     }
		     $oCustomerSector = $this->fillVo($aData, $oCustomerSector);
		}
		else {
		     $oCustomerSector = new CustomerSector();
		     if (!empty($aData)) {
		         $oCustomerSector = $this->fillVo($aData, $oCustomerSector);
		     }
		}
		return $oCustomerSector;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomerSector
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomerSector
	{
		$oCustomerSector = $this->getModel($aData);


		 if(!empty($oCustomerSector))
		 {
		     $oCustomerSector = $this->fillVo($aData, $oCustomerSector);
		     $oCustomerSector->save();
		 }
		return $oCustomerSector;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomerSector $oModel
	 * @return CustomerSector
	 */
	protected function fillVo(array $aData, CustomerSector $oModel): CustomerSector
	{
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
