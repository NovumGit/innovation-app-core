<?php
namespace Crud\ApiMailchimpList\Field;

use Crud\ApiMailchimpList\Field\Base\ToWhom as BaseToWhom;

/**
 * Skeleton subclass for representing to_whom field from the api_mailchimp_list table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class ToWhom extends BaseToWhom
{
}
