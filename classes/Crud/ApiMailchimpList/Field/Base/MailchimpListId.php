<?php
namespace Crud\ApiMailchimpList\Field\Base;

use Crud\ApiMailchimpList\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'mailchimp_list_id' crud field from the 'api_mailchimp_list' table.
 * This class is auto generated and should not be modified.
 */
abstract class MailchimpListId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'mailchimp_list_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMailchimpListId';
	protected $sFqModelClassname = '\\\Model\Api\ApiMailchimpList';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
