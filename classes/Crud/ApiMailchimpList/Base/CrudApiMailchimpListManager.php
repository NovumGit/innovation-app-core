<?php
namespace Crud\ApiMailchimpList\Base;

use Core\Utils;
use Crud;
use Crud\ApiMailchimpList\FieldIterator;
use Crud\ApiMailchimpList\Field\MailchimpListId;
use Crud\ApiMailchimpList\Field\ToWhom;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Api\ApiMailchimpList;
use Model\Api\ApiMailchimpListQuery;
use Model\Api\Map\ApiMailchimpListTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ApiMailchimpList instead if you need to override or add functionality.
 */
abstract class CrudApiMailchimpListManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ApiMailchimpListQuery::create();
	}


	public function getTableMap(): ApiMailchimpListTableMap
	{
		return new ApiMailchimpListTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ApiMailchimpList";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "api_mailchimp_list toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "api_mailchimp_list aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailchimpListId', 'ToWhom'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['MailchimpListId', 'ToWhom'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ApiMailchimpList
	 */
	public function getModel(array $aData = null): ApiMailchimpList
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oApiMailchimpListQuery = ApiMailchimpListQuery::create();
		     $oApiMailchimpList = $oApiMailchimpListQuery->findOneById($aData['id']);
		     if (!$oApiMailchimpList instanceof ApiMailchimpList) {
		         throw new LogicException("ApiMailchimpList should be an instance of ApiMailchimpList but got something else." . __METHOD__);
		     }
		     $oApiMailchimpList = $this->fillVo($aData, $oApiMailchimpList);
		}
		else {
		     $oApiMailchimpList = new ApiMailchimpList();
		     if (!empty($aData)) {
		         $oApiMailchimpList = $this->fillVo($aData, $oApiMailchimpList);
		     }
		}
		return $oApiMailchimpList;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ApiMailchimpList
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ApiMailchimpList
	{
		$oApiMailchimpList = $this->getModel($aData);


		 if(!empty($oApiMailchimpList))
		 {
		     $oApiMailchimpList = $this->fillVo($aData, $oApiMailchimpList);
		     $oApiMailchimpList->save();
		 }
		return $oApiMailchimpList;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ApiMailchimpList $oModel
	 * @return ApiMailchimpList
	 */
	protected function fillVo(array $aData, ApiMailchimpList $oModel): ApiMailchimpList
	{
		if(isset($aData['mailchimp_list_id'])) {
		     $oField = new MailchimpListId();
		     $mValue = $oField->sanitize($aData['mailchimp_list_id']);
		     $oModel->setMailchimpListId($mValue);
		}
		if(isset($aData['to_whom'])) {
		     $oField = new ToWhom();
		     $mValue = $oField->sanitize($aData['to_whom']);
		     $oModel->setToWhom($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
