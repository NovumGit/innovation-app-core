<?php
namespace Crud\ApiMailchimpList\Base;

use Crud\ApiMailchimpList\ICollectionField as ApiMailchimpListField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ApiMailchimpList\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ApiMailchimpListField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ApiMailchimpListField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ApiMailchimpListField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ApiMailchimpListField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ApiMailchimpListField
	{
		return current($this->aFields);
	}
}
