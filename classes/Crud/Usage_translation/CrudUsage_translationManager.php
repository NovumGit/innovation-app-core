<?php
namespace Crud\Usage_translation;

use Core\Utils;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\MasterTable\UsageTranslation;
use Model\Setting\MasterTable\UsageTranslationQuery;
use Exception\LogicException;

class CrudUsage_translationManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'gebruik taal';
    }
    function getOverviewUrl():string
    {
        return '/setting/mastertable/usage/translation/overview';
    }
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/usage/translation/edit';
    }
    function getNewFormTitle():string{
        return 'Gebruik toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Gebruik wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNs = false):?array
    {
        $aColumns = [
            'Name',
            'Edit',
            'Delete'
        ];
        if($bAddNs){
            array_walk($aColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }
    function getDefaultEditFields(bool $bAddNs = false):?array
    {
        $aColumns = [
            'Name'
        ];
        if($bAddNs){
            array_walk($aColumns, function(&$item) {
                $item = Utils::makeNamespace($this, $item);
            });
        }
        return $aColumns;
    }
    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
            $oUsageTranslationQuery = UsageTranslationQuery::create();
            $oUsageTranslation = $oUsageTranslationQuery->findOneById($aData['id']);
            if(!$oUsageTranslation instanceof UsageTranslation)
            {
                throw new LogicException("UsageTranslation should be an instance of UsageTranslation but got ".get_class($oUsageTranslation)." in ".__METHOD__);
            }
        }
        else
        {
            $oUsageTranslation = new UsageTranslation();
            if(!empty($aData))
            {
                $oUsageTranslation = $this->fillVo($aData, $oUsageTranslation);
            }
        }
        return $oUsageTranslation ;
    }
    function store(array $aData = null)
    {
        $oUsageTranslation = $this->getModel($aData);
        if(!empty($oUsageTranslation))
        {
            $oUsageTranslation = $this->fillVo($aData, $oUsageTranslation);
            $oUsageTranslation->save();
        }
        return $oUsageTranslation;
    }
    private function fillVo($aData, UsageTranslation $oUsageTranslation)
    {
        if(isset($aData['name'])){
            $oUsageTranslation->setName($aData['name']);
        }
        return $oUsageTranslation;
    }
}
