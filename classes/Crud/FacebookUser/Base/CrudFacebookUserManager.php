<?php
namespace Crud\FacebookUser\Base;

use Core\Utils;
use Crud;
use Crud\FacebookUser\FieldIterator;
use Crud\FacebookUser\Field\Created;
use Crud\FacebookUser\Field\Email;
use Crud\FacebookUser\Field\FacebookId;
use Crud\FacebookUser\Field\FirstName;
use Crud\FacebookUser\Field\Gender;
use Crud\FacebookUser\Field\LastName;
use Crud\FacebookUser\Field\Link;
use Crud\FacebookUser\Field\Modified;
use Crud\FacebookUser\Field\UserId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Account\FacebookUser;
use Model\Account\FacebookUserQuery;
use Model\Account\Map\FacebookUserTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify FacebookUser instead if you need to override or add functionality.
 */
abstract class CrudFacebookUserManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FacebookUserQuery::create();
	}


	public function getTableMap(): FacebookUserTableMap
	{
		return new FacebookUserTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "FacebookUser";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "facebook_user toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "facebook_user aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'FacebookId', 'Email', 'FirstName', 'LastName', 'Gender', 'Link', 'Created', 'Modified'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'FacebookId', 'Email', 'FirstName', 'LastName', 'Gender', 'Link', 'Created', 'Modified'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return FacebookUser
	 */
	public function getModel(array $aData = null): FacebookUser
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFacebookUserQuery = FacebookUserQuery::create();
		     $oFacebookUser = $oFacebookUserQuery->findOneById($aData['id']);
		     if (!$oFacebookUser instanceof FacebookUser) {
		         throw new LogicException("FacebookUser should be an instance of FacebookUser but got something else." . __METHOD__);
		     }
		     $oFacebookUser = $this->fillVo($aData, $oFacebookUser);
		}
		else {
		     $oFacebookUser = new FacebookUser();
		     if (!empty($aData)) {
		         $oFacebookUser = $this->fillVo($aData, $oFacebookUser);
		     }
		}
		return $oFacebookUser;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return FacebookUser
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): FacebookUser
	{
		$oFacebookUser = $this->getModel($aData);


		 if(!empty($oFacebookUser))
		 {
		     $oFacebookUser = $this->fillVo($aData, $oFacebookUser);
		     $oFacebookUser->save();
		 }
		return $oFacebookUser;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param FacebookUser $oModel
	 * @return FacebookUser
	 */
	protected function fillVo(array $aData, FacebookUser $oModel): FacebookUser
	{
		if(isset($aData['user_id'])) {
		     $oField = new UserId();
		     $mValue = $oField->sanitize($aData['user_id']);
		     $oModel->setUserId($mValue);
		}
		if(isset($aData['facebook_id'])) {
		     $oField = new FacebookId();
		     $mValue = $oField->sanitize($aData['facebook_id']);
		     $oModel->setFacebookId($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['last_name'])) {
		     $oField = new LastName();
		     $mValue = $oField->sanitize($aData['last_name']);
		     $oModel->setLastName($mValue);
		}
		if(isset($aData['gender'])) {
		     $oField = new Gender();
		     $mValue = $oField->sanitize($aData['gender']);
		     $oModel->setGender($mValue);
		}
		if(isset($aData['link'])) {
		     $oField = new Link();
		     $mValue = $oField->sanitize($aData['link']);
		     $oModel->setLink($mValue);
		}
		if(isset($aData['created'])) {
		     $oField = new Created();
		     $mValue = $oField->sanitize($aData['created']);
		     $oModel->setCreated($mValue);
		}
		if(isset($aData['modified'])) {
		     $oField = new Modified();
		     $mValue = $oField->sanitize($aData['modified']);
		     $oModel->setModified($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
