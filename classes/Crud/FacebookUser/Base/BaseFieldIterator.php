<?php
namespace Crud\FacebookUser\Base;

use Crud\BaseCrudFieldIterator;
use Crud\FacebookUser\ICollectionField as FacebookUserField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\FacebookUser\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FacebookUserField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FacebookUserField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FacebookUserField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FacebookUserField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FacebookUserField
	{
		return current($this->aFields);
	}
}
