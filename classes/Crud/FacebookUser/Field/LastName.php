<?php
namespace Crud\FacebookUser\Field;

use Crud\FacebookUser\Field\Base\LastName as BaseLastName;

/**
 * Skeleton subclass for representing last_name field from the facebook_user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class LastName extends BaseLastName
{
}
