<?php
namespace Crud\FacebookUser\Field\Base;

use Crud\FacebookUser\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'user_id' crud field from the 'facebook_user' table.
 * This class is auto generated and should not be modified.
 */
abstract class UserId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'user_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUserId';
	protected $sFqModelClassname = '\\\Model\Account\FacebookUser';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
