<?php
namespace Crud\FacebookUser\Field;

use Crud\FacebookUser\Field\Base\FirstName as BaseFirstName;

/**
 * Skeleton subclass for representing first_name field from the facebook_user table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class FirstName extends BaseFirstName
{
}
