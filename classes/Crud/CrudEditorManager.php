<?php

namespace Crud;

use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;
use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorQuery;
use ReflectionClass;

class CrudEditorManager
{
    public static function getCrudEditor(string $sManagerName, string $sRendering)
    {
        $oCrudConfigQuery = CrudConfigQuery::create();
        $oCrudConfig = $oCrudConfigQuery->findOneByManagerName($sManagerName);
        $oCrudEditorQuery = CrudEditorQuery::create();
        $oCrudEditorQuery->filterByCrudConfigId($oCrudConfig->getId());
        $oCrudEditorQuery->filterByName($sRendering);
        return $oCrudEditorQuery->findOne();
    }
    public static function generateNew(string $sManagerName, string $sRendering)
    {
        $oManager = new $sManagerName();

        if (!$oManager instanceof FormManager) {
            throw new \Exception\LogicException("Expected an instance of FormManager");
        }

        $oCrudConfigQuery = CrudConfigQuery::create();
        $oCrudConfig = $oCrudConfigQuery->findOneByManagerName($sManagerName);

        if (!$oCrudConfig instanceof CrudConfig) {
            $oCrudConfig = new CrudConfig();
            $oCrudConfig->setManagerName($sManagerName);
        }

        $oCrudEditorQuery = CrudEditorQuery::create();
        $oCrudEditorQuery->filterByCrudConfigId($oCrudConfig->getId());
        $oCrudEditorQuery->filterByName($sRendering);
        $oCrudEditor = $oCrudEditorQuery->findOne();

        if (!$oCrudEditor instanceof CrudEditor) {
            $oCrudEditor = new CrudEditor();
            $oCrudEditor->setCrudConfig($oCrudConfig);
            $oCrudEditor->setName($sRendering);
            $oCrudEditor->setPrivateDescription("This is a non persisent temp editor, not stored anywhere");
        }

        $oCrudEditorBlock = new CrudEditorBlock();
        $oCrudEditorBlock->setCrudEditor($oCrudEditor);
        $oCrudEditorBlock->setSorting(1);
        $oCrudEditorBlock->setWidth(12);

        $oCrudEditor->addCrudEditorBlock($oCrudEditorBlock);

        $aFields = $oManager->getFieldIterator(function (CrudElement $oElement, FormManager $oCurrManager) {

            $oReflector = new ReflectionClass($oElement);
            $aAllFields =  $oCurrManager->getDefaultEditFields();
            $sLongName = $oReflector->getName();
            $sShortName = $oReflector->getShortName();

            $iIndex = array_search($sLongName, $aAllFields);
            if ($iIndex) {
                return $iIndex;
            }
            $iIndex = array_search($sShortName, $aAllFields);
            if ($iIndex) {
                return $iIndex;
            }

            return false;
        });

        foreach ($aFields as $iIndex => $oField) {
            $oBlockField = new CrudEditorBlockField();
            $oBlockField->setReadonlyField(false);
            $oBlockField->setSorting($iIndex);
            $oBlockField->setWidth(12);
            $oBlockField->setField($oField->getFqn());
            $oBlockField->setCrudEditorBlock($oCrudEditorBlock);
            $oCrudEditorBlock->addCrudEditorBlockField($oBlockField);
        }


        return $oCrudEditor;
    }
}
