<?php
namespace Crud\Material\Field;

use Crud\Field;
use InvalidArgumentException;
use Respect\Validation\Validator;
use Model\Setting\MasterTable\ProductMaterial;
use Model\Setting\MasterTable\ProductMaterialQuery;

class Name extends Field{

    protected $sFieldLabel = 'Materiaal';


    function hasValidations() { return true; }
    function validate($aPostedData)
    {
        $mResponse = false;

        $oValidator = Validator::alpha();
        if(empty($aPostedData['id']))
        {
            $oProductType = ProductMaterialQuery::create()->findOneByName($aPostedData['name']);

            if($oProductType instanceof ProductMaterial)
            {
                $mResponse[] = 'Er is al een materiaal met deze naam.';
            }
        }
        $sName = trim($aPostedData['name']);
        if(empty($sName)){

            $mResponse[] = 'Het veld materiaal naam mag niet leeg zijn.';

        }else if(!$oValidator->length(2, 120)->validate($aPostedData['name'])){

            $mResponse[] = 'Het veld type materiaal moet minimaal 2 en mag maximaal 120 tekens lang zijn.';
        }

        return $mResponse;
    }
    function getFieldTitle(){
        return 'Product materiaal';
    }

    function getOverviewHeader()
    {
        return $this->sortableHeaderField('Materiaal', 'name');
    }
    function getOverviewValue($oProductMaterial)
    {
        if(!$oProductMaterial instanceof ProductMaterial)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Setting\\MasterTable\\ProductMaterial in ".__METHOD__);
        }
        return '<td class="">'.$oProductMaterial->getName().'</td>';
    }

    function getEditHtml($mData, $bReadonly)
    {

        if(!$mData instanceof ProductMaterial)
        {
            throw new InvalidArgumentException("Expected an instance of  \\model\\Setting\\MasterTable\\ProductMaterial in ".__METHOD__);
        }

        $aOut = [];
        $aOut[] = '<div class="form-group">';
        $aOut[] = '    <label for="product_type" class="col-lg-3 control-label">Product materiaal</label>';

        $aOut[] = '    <div class="col-lg-8">';

        $aOut[] = '            <div class="input-group">';
        $aOut[] = '            <span class="input-group-addon">';
        $aOut[] = '                <i class="fa fa-user"></i>';
        $aOut[] = '            </span>';
        $aOut[] = '            <input name="data[name]" id="product_type" class="form-control" placeholder="Product materiaal" value="'.$mData->getName().'" type="text">';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        return join(PHP_EOL, $aOut);
    }

}