<?php
namespace Crud\Material\Field;

use Crud\Generic\Field\GenericDelete;
use Model\Setting\MasterTable\ProductMaterial;
use InvalidArgumentException;

class Delete extends GenericDelete{

    function getDeleteUrl($oProductMaterial){

        if(!$oProductMaterial instanceof ProductMaterial)
        {
            throw new InvalidArgumentException('Expected an instance of ProductMaterial but got '.get_class($oProductMaterial));
        }

        return '/setting/mastertable/material/edit?_do=Delete&id='.$oProductMaterial->getId();
    }
    function getUnDeleteUrl($oUnused){
        // undelete niet mogelijk bij dit type record
        return;
    }
}