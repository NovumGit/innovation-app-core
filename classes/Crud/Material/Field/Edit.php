<?php
namespace Crud\Material\Field;

use Crud\Generic\Field\GenericEdit;
use Model\Setting\MasterTable\ProductMaterial;
use InvalidArgumentException;

class Edit extends GenericEdit{


    function getEditUrl($oProductMaterial){

        if(!$oProductMaterial instanceof ProductMaterial)
        {
            throw new InvalidArgumentException('Expected an instance of ProductMaterial but got '.get_class($oProductMaterial));
        }

        return '/setting/mastertable/material/edit?id='.$oProductMaterial->getId();
    }
}