<?php
namespace Crud\Material;

use Crud\IEditableCrud;
use Crud\FormManager;
use Model\Setting\MasterTable\ProductMaterialQuery;
use Model\Setting\MasterTable\ProductMaterial;
use Propel\Runtime\Exception\LogicException;

class CrudMaterialManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        return '/setting/mastertable/material/edit';
    }
    function getEntityTitle():string
    {
        return 'materiaal';
    }
    function getNewFormTitle():string{
        return 'Materiaal toevoegen';
    }

    function getEditFormTitle():string
    {
        return 'Materiaal wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {

        return [
            'Name',
            'Delete',
            'Edit'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name'
        ];
    }

    function getModel(array $aData = null)
    {

        if($aData['id'])
        {

            $oProductMaterialQuery = new ProductMaterialQuery();

            $oProductMaterial = $oProductMaterialQuery->findOneById($aData['id']);

            if(!$oProductMaterial instanceof ProductMaterial){
                throw new LogicException("ProductMaterial should be an instance of ProductMaterial but got ".get_class($oProductMaterial)." in ".__METHOD__);
            }


        }
        else
        {
            $oProductMaterial = new ProductMaterial();
            if(isset($aData['name']))
            {
                $oProductMaterial->setName($aData['name']);
            }
        }

        return $oProductMaterial;
    }

    function store(array $aData = null)
    {
        $oProductMaterial = $this->getModel($aData);

        if(!empty($oProductMaterial))
        {
            if(isset($aData['name']))
            {
                $oProductMaterial->setName($aData['name']);
            }
            $oProductMaterial->save();
        }
        return $oProductMaterial;
    }
}
