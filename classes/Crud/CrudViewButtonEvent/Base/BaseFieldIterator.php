<?php
namespace Crud\CrudViewButtonEvent\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudViewButtonEvent\ICollectionField as CrudViewButtonEventField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudViewButtonEvent\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudViewButtonEventField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudViewButtonEventField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudViewButtonEventField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudViewButtonEventField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudViewButtonEventField
	{
		return current($this->aFields);
	}
}
