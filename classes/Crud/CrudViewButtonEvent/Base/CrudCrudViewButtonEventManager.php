<?php
namespace Crud\CrudViewButtonEvent\Base;

use Core\Utils;
use Crud;
use Crud\CrudViewButtonEvent\FieldIterator;
use Crud\CrudViewButtonEvent\Field\CrudViewButtonId;
use Crud\CrudViewButtonEvent\Field\EventClass;
use Crud\CrudViewButtonEvent\Field\Sorting;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewButtonEvent;
use Model\Setting\CrudManager\CrudViewButtonEventQuery;
use Model\Setting\CrudManager\Map\CrudViewButtonEventTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudViewButtonEvent instead if you need to override or add functionality.
 */
abstract class CrudCrudViewButtonEventManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewButtonEventQuery::create();
	}


	public function getTableMap(): CrudViewButtonEventTableMap
	{
		return new CrudViewButtonEventTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn acties opgeslagen die worden uitgevoerd als men op een knop bij een overzicht drukt";
	}


	public function getEntityTitle(): string
	{
		return "CrudViewButtonEvent";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view_button_event toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view_button_event aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewButtonId', 'EventClass', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewButtonId', 'EventClass', 'Sorting'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudViewButtonEvent
	 */
	public function getModel(array $aData = null): CrudViewButtonEvent
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewButtonEventQuery = CrudViewButtonEventQuery::create();
		     $oCrudViewButtonEvent = $oCrudViewButtonEventQuery->findOneById($aData['id']);
		     if (!$oCrudViewButtonEvent instanceof CrudViewButtonEvent) {
		         throw new LogicException("CrudViewButtonEvent should be an instance of CrudViewButtonEvent but got something else." . __METHOD__);
		     }
		     $oCrudViewButtonEvent = $this->fillVo($aData, $oCrudViewButtonEvent);
		}
		else {
		     $oCrudViewButtonEvent = new CrudViewButtonEvent();
		     if (!empty($aData)) {
		         $oCrudViewButtonEvent = $this->fillVo($aData, $oCrudViewButtonEvent);
		     }
		}
		return $oCrudViewButtonEvent;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudViewButtonEvent
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudViewButtonEvent
	{
		$oCrudViewButtonEvent = $this->getModel($aData);


		 if(!empty($oCrudViewButtonEvent))
		 {
		     $oCrudViewButtonEvent = $this->fillVo($aData, $oCrudViewButtonEvent);
		     $oCrudViewButtonEvent->save();
		 }
		return $oCrudViewButtonEvent;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudViewButtonEvent $oModel
	 * @return CrudViewButtonEvent
	 */
	protected function fillVo(array $aData, CrudViewButtonEvent $oModel): CrudViewButtonEvent
	{
		if(isset($aData['crud_view_button_id'])) {
		     $oField = new CrudViewButtonId();
		     $mValue = $oField->sanitize($aData['crud_view_button_id']);
		     $oModel->setCrudViewButtonId($mValue);
		}
		if(isset($aData['event_class'])) {
		     $oField = new EventClass();
		     $mValue = $oField->sanitize($aData['event_class']);
		     $oModel->setEventClass($mValue);
		}
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
