<?php
namespace Crud\CrudViewButtonEvent\Field;

use Crud\CrudViewButtonEvent\Field\Base\CrudViewButtonId as BaseCrudViewButtonId;

/**
 * Skeleton subclass for representing crud_view_button_id field from the crud_view_button_event table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudViewButtonId extends BaseCrudViewButtonId
{
}
