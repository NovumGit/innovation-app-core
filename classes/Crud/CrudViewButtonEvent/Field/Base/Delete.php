<?php 
namespace Crud\CrudViewButtonEvent\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudViewButtonEvent;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewButtonEvent)
		{
		     return "//system/crud_view_button_event/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewButtonEvent)
		{
		     return "//crud_view_button_event?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
