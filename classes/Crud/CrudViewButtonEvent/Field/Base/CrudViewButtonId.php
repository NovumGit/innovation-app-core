<?php
namespace Crud\CrudViewButtonEvent\Field\Base;

use Core\Utils;
use Crud\CrudViewButtonEvent\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\Setting\CrudManager\CrudViewButtonQuery;

/**
 * Base class that represents the 'crud_view_button_id' crud field from the 'crud_view_button_event' table.
 * This class is auto generated and should not be modified.
 */
abstract class CrudViewButtonId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'crud_view_button_id';
	protected $sFieldLabel = 'Knop';
	protected $sIcon = 'flash';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCrudViewButtonId';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewButtonEvent';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\Setting\CrudManager\CrudViewButtonQuery::create()->orderBytitle()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "gettitle", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\Setting\CrudManager\CrudViewButtonQuery::create()->findOneById($iItemId)->gettitle();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
