<?php
namespace Crud\ProductUnit\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ProductUnit\ICollectionField;

/**
 * Base class that represents the 'name_plural' crud field from the 'mt_unit' table.
 * This class is auto generated and should not be modified.
 */
abstract class NamePlural extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'name_plural';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getNamePlural';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\ProductUnit';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
