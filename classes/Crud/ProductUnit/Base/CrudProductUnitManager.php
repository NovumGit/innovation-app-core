<?php
namespace Crud\ProductUnit\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ProductUnit\FieldIterator;
use Crud\ProductUnit\Field\Code;
use Crud\ProductUnit\Field\Name;
use Crud\ProductUnit\Field\NamePlural;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ProductUnitTableMap;
use Model\Setting\MasterTable\ProductUnit;
use Model\Setting\MasterTable\ProductUnitQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ProductUnit instead if you need to override or add functionality.
 */
abstract class CrudProductUnitManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ProductUnitQuery::create();
	}


	public function getTableMap(): ProductUnitTableMap
	{
		return new ProductUnitTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ProductUnit";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_unit toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_unit aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'NamePlural', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'NamePlural', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ProductUnit
	 */
	public function getModel(array $aData = null): ProductUnit
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oProductUnitQuery = ProductUnitQuery::create();
		     $oProductUnit = $oProductUnitQuery->findOneById($aData['id']);
		     if (!$oProductUnit instanceof ProductUnit) {
		         throw new LogicException("ProductUnit should be an instance of ProductUnit but got something else." . __METHOD__);
		     }
		     $oProductUnit = $this->fillVo($aData, $oProductUnit);
		}
		else {
		     $oProductUnit = new ProductUnit();
		     if (!empty($aData)) {
		         $oProductUnit = $this->fillVo($aData, $oProductUnit);
		     }
		}
		return $oProductUnit;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ProductUnit
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ProductUnit
	{
		$oProductUnit = $this->getModel($aData);


		 if(!empty($oProductUnit))
		 {
		     $oProductUnit = $this->fillVo($aData, $oProductUnit);
		     $oProductUnit->save();
		 }
		return $oProductUnit;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ProductUnit $oModel
	 * @return ProductUnit
	 */
	protected function fillVo(array $aData, ProductUnit $oModel): ProductUnit
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['name_plural'])) {
		     $oField = new NamePlural();
		     $mValue = $oField->sanitize($aData['name_plural']);
		     $oModel->setNamePlural($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
