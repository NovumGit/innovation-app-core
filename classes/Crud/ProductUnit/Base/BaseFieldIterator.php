<?php
namespace Crud\ProductUnit\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ProductUnit\ICollectionField as ProductUnitField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ProductUnit\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ProductUnitField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ProductUnitField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ProductUnitField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ProductUnitField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ProductUnitField
	{
		return current($this->aFields);
	}
}
