<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_primary_key' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsPrimaryKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'is_primary_key';
	protected $sFieldLabel = 'Is primary key';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsPrimaryKey';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
