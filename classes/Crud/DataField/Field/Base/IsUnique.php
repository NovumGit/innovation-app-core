<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'is_unique' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 * Last modified: 2020-Nov-Sat 2:46:00
 */
abstract class IsUnique extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'is_unique';
	protected $sFieldLabel = 'Is uniek';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIsUnique';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['is_unique']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Is uniek" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
