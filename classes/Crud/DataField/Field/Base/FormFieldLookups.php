<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'form_field_lookups' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class FormFieldLookups extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'form_field_lookups';
	protected $sFieldLabel = 'Lookup origin';
	protected $sIcon = 'table';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFormFieldLookups';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
