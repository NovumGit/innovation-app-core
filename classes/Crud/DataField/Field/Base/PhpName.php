<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'php_name' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class PhpName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'php_name';
	protected $sFieldLabel = 'Php naam';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPhpName';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
