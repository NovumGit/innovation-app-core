<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericInteger;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'size' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class Size extends GenericInteger implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'size';
	protected $sFieldLabel = 'Maat / size';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSize';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
