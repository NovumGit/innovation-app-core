<?php
namespace Crud\DataField\Field\Base;

use Crud\DataField\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'required' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class Required extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'required';
	protected $sFieldLabel = 'Verplicht veld';
	protected $sIcon = 'check';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRequired';
	protected $sFqModelClassname = '\\\Model\System\DataModel\Field\DataField';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['required']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Verplicht veld" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
