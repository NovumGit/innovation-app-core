<?php
namespace Crud\DataField\Field\Base;

use Core\Utils;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\UI\FormUIComponentQuery;

/**
 * Base class that represents the 'form_ui_component_id' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class FormUiComponentId extends GenericLookup implements IFilterableField, IEditableField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'form_ui_component_id';

	protected $sFieldLabel = 'UI component';

	protected $sIcon = 'list-alt';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getFormUiComponentId';

	protected $sFqModelClassname = '\Model\System\DataModel\Field\DataField';


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\FormUIComponentQuery::create()->orderByname()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getname", $mSelectedItem, "getId");
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\FormUIComponentQuery::create()->findOneById($iItemId)->getname();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['form_ui_component_id']) || empty($aPostedData['form_ui_component_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "UI component" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
