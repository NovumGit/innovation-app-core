<?php 
namespace Crud\DataField\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataModel\Field\DataField;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataField)
		{
		     return "//system/data_field/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof DataField)
		{
		     return "//data_field?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
