<?php
namespace Crud\DataField\Field\Base;

use Crud\Generic\Field\GenericBoolean;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'is_ui_component' crud field from the 'data_field' table.
 * This class is auto generated and should not be modified.
 */
abstract class IsUiComponent extends GenericBoolean implements IFilterableField, IEditableField
{
	protected $sFieldName = 'is_ui_component';

	protected $sFieldLabel = 'Is UI component';

	protected $sIcon = 'check';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getIsUiComponent';

	protected $sFqModelClassname = '\Model\System\DataModel\Field\DataField';
}
