<?php
namespace Crud\DataField\Field;

use Crud\DataField\Field\Base\Required as BaseRequired;

/**
 * Skeleton subclass for representing required field from the data_field table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Required extends BaseRequired
{
}
