<?php
namespace Crud\DataField\Base;

use Core\Utils;
use Crud;
use Crud\DataField\FieldIterator;
use Crud\DataField\Field\AutoIncrement;
use Crud\DataField\Field\DataModelId;
use Crud\DataField\Field\DataTypeId;
use Crud\DataField\Field\DefaultValue;
use Crud\DataField\Field\FormFieldLookups;
use Crud\DataField\Field\FormFieldTypeId;
use Crud\DataField\Field\IconId;
use Crud\DataField\Field\IsPrimaryKey;
use Crud\DataField\Field\Label;
use Crud\DataField\Field\Name;
use Crud\DataField\Field\PhpName;
use Crud\DataField\Field\Required;
use Crud\DataField\Field\Size;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Field\Map\DataFieldTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify DataField instead if you need to override or add functionality.
 */
abstract class CrudDataFieldManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return DataFieldQuery::create();
	}


	public function getTableMap(): DataFieldTableMap
	{
		return new DataFieldTableMap();
	}


	public function getShortDescription(): string
	{
		return "De eigenschappen of velden die in een model zijn opgeslagen zijn hierin vastgelegd.";
	}


	public function getEntityTitle(): string
	{
		return "DataField";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "data_field toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "data_field aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DataModelId', 'Name', 'PhpName', 'Label', 'Size', 'IconId', 'FormFieldTypeId', 'FormFieldLookups', 'DataTypeId', 'Required', 'IsPrimaryKey', 'AutoIncrement', 'DefaultValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['DataModelId', 'Name', 'PhpName', 'Label', 'Size', 'IconId', 'FormFieldTypeId', 'FormFieldLookups', 'DataTypeId', 'Required', 'IsPrimaryKey', 'AutoIncrement', 'DefaultValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return DataField
	 */
	public function getModel(array $aData = null): DataField
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oDataFieldQuery = DataFieldQuery::create();
		     $oDataField = $oDataFieldQuery->findOneById($aData['id']);
		     if (!$oDataField instanceof DataField) {
		         throw new LogicException("DataField should be an instance of DataField but got something else." . __METHOD__);
		     }
		     $oDataField = $this->fillVo($aData, $oDataField);
		}
		else {
		     $oDataField = new DataField();
		     if (!empty($aData)) {
		         $oDataField = $this->fillVo($aData, $oDataField);
		     }
		}
		return $oDataField;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return DataField
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): DataField
	{
		$oDataField = $this->getModel($aData);


		 if(!empty($oDataField))
		 {
		     $oDataField = $this->fillVo($aData, $oDataField);
		     $oDataField->save();
		 }
		return $oDataField;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param DataField $oModel
	 * @return DataField
	 */
	protected function fillVo(array $aData, DataField $oModel): DataField
	{
		if(isset($aData['data_model_id'])) {
		     $oField = new DataModelId();
		     $mValue = $oField->sanitize($aData['data_model_id']);
		     $oModel->setDataModelId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['php_name'])) {
		     $oField = new PhpName();
		     $mValue = $oField->sanitize($aData['php_name']);
		     $oModel->setPhpName($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		if(isset($aData['size'])) {
		     $oField = new Size();
		     $mValue = $oField->sanitize($aData['size']);
		     $oModel->setSize($mValue);
		}
		if(isset($aData['icon_id'])) {
		     $oField = new IconId();
		     $mValue = $oField->sanitize($aData['icon_id']);
		     $oModel->setIconId($mValue);
		}
		if(isset($aData['form_field_type_id'])) {
		     $oField = new FormFieldTypeId();
		     $mValue = $oField->sanitize($aData['form_field_type_id']);
		     $oModel->setFormFieldTypeId($mValue);
		}
		if(isset($aData['form_field_lookups'])) {
		     $oField = new FormFieldLookups();
		     $mValue = $oField->sanitize($aData['form_field_lookups']);
		     $oModel->setFormFieldLookups($mValue);
		}
		if(isset($aData['data_type_id'])) {
		     $oField = new DataTypeId();
		     $mValue = $oField->sanitize($aData['data_type_id']);
		     $oModel->setDataTypeId($mValue);
		}
		if(isset($aData['required'])) {
		     $oField = new Required();
		     $mValue = $oField->sanitize($aData['required']);
		     $oModel->setRequired($mValue);
		}
		if(isset($aData['is_primary_key'])) {
		     $oField = new IsPrimaryKey();
		     $mValue = $oField->sanitize($aData['is_primary_key']);
		     $oModel->setIsPrimaryKey($mValue);
		}
		if(isset($aData['auto_increment'])) {
		     $oField = new AutoIncrement();
		     $mValue = $oField->sanitize($aData['auto_increment']);
		     $oModel->setAutoIncrement($mValue);
		}
		if(isset($aData['default_value'])) {
		     $oField = new DefaultValue();
		     $mValue = $oField->sanitize($aData['default_value']);
		     $oModel->setDefaultValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
