<?php
namespace Crud\DataField\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\DataField\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
