<?php
namespace Crud\DataField\Base;

use Crud\BaseCrudFieldIterator;
use Crud\DataField\ICollectionField as DataFieldField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\DataField\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param DataFieldField[] $aFields */
	private array $aFields = [];


	/**
	 * @param DataFieldField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof DataFieldField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(DataFieldField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): DataFieldField
	{
		return current($this->aFields);
	}
}
