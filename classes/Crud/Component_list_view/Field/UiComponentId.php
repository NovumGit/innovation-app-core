<?php
namespace Crud\Component_list_view\Field;

use Crud\Component_list_view\Field\Base\UiComponentId as BaseUiComponentId;

/**
 * Skeleton subclass for representing ui_component_id field from the component_list_view table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class UiComponentId extends BaseUiComponentId
{
}
