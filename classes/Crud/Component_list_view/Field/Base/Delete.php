<?php 
namespace Crud\Component_list_view\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\ListView\Component_list_view;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_list_view)
		{
		     return "//system/component_list_view/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_list_view)
		{
		     return "//component_list_view?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
