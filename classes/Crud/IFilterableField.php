<?php
namespace Crud;

interface IFilterableField extends IField {

    function getDataType():string;
    function getFieldName();
}
