<?php
namespace Crud\Component_button_color_type\Field\Base;

use Crud\Component_button_color_type\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'item_label' crud field from the 'component_button_color_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemLabel extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_label';
	protected $sFieldLabel = 'Type';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemLabel';
	protected $sFqModelClassname = '\Model\System\LowCode\Button\Component_button_color_type';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
