<?php
namespace Crud\BankTransactionCategory\Field;

use Crud\Generic\Field\GenericEditTextfield;
use Model\Finance\BankTransactionCategory;

class Description extends GenericEditTextfield
{
	protected $sFieldName = 'description';
	protected $sFieldLabel = 'Naam / omschrijving';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = 'Geef een omschrijving';
	protected $sGetter = 'getDescription';
	protected $sFqModelClassname = BankTransactionCategory::class;
}