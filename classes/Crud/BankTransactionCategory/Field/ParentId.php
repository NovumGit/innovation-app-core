<?php
namespace Crud\BankTransactionCategory\Field;

use Crud\Generic\Field\GenericLookup;
use Exception\LogicException;
use Model\Finance\BankTransactionCategory;
use Model\Finance\BankTransactionCategoryQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class ParentId extends GenericLookup
{
	protected $sFieldName = 'parent_id';
	protected $sFieldLabel = 'Parent';
	protected $sIcon = 'question-circle';
	protected $sPlaceHolder = 'Where statements';
	protected $sGetter = 'getParentId';
	protected $sFqModelClassname = BankTransactionCategory::class;

	function getLookups($iSelectedItem = null)
	{

		$aMenu = BankTransactionCategoryQuery::getTree();

		$aDropdownOptions = [];
	    foreach ($aMenu as $Id => $sLabel)
	    {
		    $sSelected = $iSelectedItem == $Id ? 'selected' : '';
		    $aDropdownOptions[] = ['id' => $Id, 'selected' => $sSelected, 'label' => $sLabel];
	    }
		return $aDropdownOptions;
	}
	function getVisibleValue($iItemId)
	{
		$oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
		$oBankTransactionCategory = $oBankTransactionCategoryQuery->findOneById($iItemId);
		return $oBankTransactionCategory->getDescription();
	}

	private function getTree($iParentId = null, array &$aItems, string $sDescriptionPrefix = '')
	{
		$oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
		$iCriteria = $iParentId === null ? Criteria::ISNULL : Criteria::EQUAL;
		$oBankTransactionCategoryQuery->filterByParentId($iParentId, $iCriteria);
		$oBankTransactionCategoryQuery->orderByDescription();
		$aBankTransactionCategories = $oBankTransactionCategoryQuery->find();

		foreach($aBankTransactionCategories as $oCategory)
		{
			if(!$oCategory instanceof BankTransactionCategory)
			{
				throw new LogicException("Expected an instance of Category");
			}
			if($sDescriptionPrefix)
			{
				$sDescription = $sDescriptionPrefix . ' -> ' .  $oCategory->getDescription();
			}
			else
			{
				$sDescription = $oCategory->getDescription();
			}

			$aItems[$oCategory->getId()] = $sDescription;
			$this->getTree($oCategory->getId(), $aItems, $sDescription);
		}
		return $aItems;
	}
}