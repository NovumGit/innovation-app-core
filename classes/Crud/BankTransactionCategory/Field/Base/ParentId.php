<?php
namespace Crud\BankTransactionCategory\Field\Base;

use Crud\BankTransactionCategory\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'parent_id' crud field from the 'bank_transaction_category' table.
 * This class is auto generated and should not be modified.
 */
abstract class ParentId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'parent_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getParentId';
	protected $sFqModelClassname = '\\\Model\Finance\BankTransactionCategory';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
