<?php
namespace Crud\BankTransactionCategory\Base;

use Core\Utils;
use Crud;
use Crud\BankTransactionCategory\FieldIterator;
use Crud\BankTransactionCategory\Field\Description;
use Crud\BankTransactionCategory\Field\ParentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Finance\BankTransactionCategory;
use Model\Finance\BankTransactionCategoryQuery;
use Model\Finance\Map\BankTransactionCategoryTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify BankTransactionCategory instead if you need to override or add functionality.
 */
abstract class CrudBankTransactionCategoryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BankTransactionCategoryQuery::create();
	}


	public function getTableMap(): BankTransactionCategoryTableMap
	{
		return new BankTransactionCategoryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "BankTransactionCategory";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "bank_transaction_category toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "bank_transaction_category aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ParentId', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ParentId', 'Description'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return BankTransactionCategory
	 */
	public function getModel(array $aData = null): BankTransactionCategory
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
		     $oBankTransactionCategory = $oBankTransactionCategoryQuery->findOneById($aData['id']);
		     if (!$oBankTransactionCategory instanceof BankTransactionCategory) {
		         throw new LogicException("BankTransactionCategory should be an instance of BankTransactionCategory but got something else." . __METHOD__);
		     }
		     $oBankTransactionCategory = $this->fillVo($aData, $oBankTransactionCategory);
		}
		else {
		     $oBankTransactionCategory = new BankTransactionCategory();
		     if (!empty($aData)) {
		         $oBankTransactionCategory = $this->fillVo($aData, $oBankTransactionCategory);
		     }
		}
		return $oBankTransactionCategory;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return BankTransactionCategory
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): BankTransactionCategory
	{
		$oBankTransactionCategory = $this->getModel($aData);


		 if(!empty($oBankTransactionCategory))
		 {
		     $oBankTransactionCategory = $this->fillVo($aData, $oBankTransactionCategory);
		     $oBankTransactionCategory->save();
		 }
		return $oBankTransactionCategory;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param BankTransactionCategory $oModel
	 * @return BankTransactionCategory
	 */
	protected function fillVo(array $aData, BankTransactionCategory $oModel): BankTransactionCategory
	{
		if(isset($aData['parent_id'])) {
		     $oField = new ParentId();
		     $mValue = $oField->sanitize($aData['parent_id']);
		     $oModel->setParentId($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
