<?php
namespace Crud\BankTransactionCategory\Base;

use Crud\BankTransactionCategory\ICollectionField as BankTransactionCategoryField;
use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\BankTransactionCategory\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param BankTransactionCategoryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param BankTransactionCategoryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof BankTransactionCategoryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(BankTransactionCategoryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): BankTransactionCategoryField
	{
		return current($this->aFields);
	}
}
