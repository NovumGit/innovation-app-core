<?php
namespace Crud\BankTransactionCategory;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Finance\BankTransactionCategory;
use Exception\LogicException;
use Model\Finance\Base\BankTransactionCategoryQuery;

class CrudBankTransactionCategoryManager extends FormManager implements IConfigurableCrud {

    function getEntityTitle():string
    {
        return 'bank_transaction_category';
    }
    function getOverviewUrl():string
    {
        return '/finance/money/tree';
    }
    function getCreateNewUrl():string
    {
        return '/finance/money/tree/edit_item';
    }
    function getNewFormTitle():string{
        return 'Transactie categorie toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Transactie categorie wijzigen';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Description',
            'ParentId'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{
	    return [
		    'Description',
		    'ParentId'
	    ];
    }

    function getModel(array $aData = null)
    {
        if(isset($aData['id']) && $aData['id'])
        {
        	$oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
	        $oBankTransactionCategory = $oBankTransactionCategoryQuery->findOneById($aData['id']);

            if(!$oBankTransactionCategory instanceof BankTransactionCategory)
            {
                throw new LogicException("{ should be an instance of Brand but got ".get_class($oBankTransactionCategory)." in ".__METHOD__);
            }
        }
        else
        {
	        $oBankTransactionCategory = new BankTransactionCategory();
            if(!empty($aData))
            {
	            $oBankTransactionCategory = $this->fillVo($aData, $oBankTransactionCategory);
            }
        }
        return $oBankTransactionCategory;
    }

    /**
     * @param $aData
     * @return BankTransactionCategory|\Propel\Runtime\ActiveRecord\ActiveRecordInterface
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function store(array $aData = null)
    {
	    $oBankTransactionCategory = $this->getModel($aData);

        if(!empty($oBankTransactionCategory))
        {
	        $oBankTransactionCategory = $this->fillVo($aData, $oBankTransactionCategory);
	        $oBankTransactionCategory->save();
        }
        return $oBankTransactionCategory;
    }
    private function fillVo($aData, BankTransactionCategory $oBankTransactionCategory)
    {
        if(isset($aData['description'])){$oBankTransactionCategory->setDescription($aData['description']);}
	    if(isset($aData['parent_id'])){$oBankTransactionCategory->setParentId($aData['parent_id']);}
	    if(isset($aData['queries'])){$oBankTransactionCategory->setQueries($aData['queries']);}

        return $oBankTransactionCategory;
    }
}
