<?php
namespace Crud\NewsletterSubscriber\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\NewsletterSubscriber\ICollectionField as NewsletterSubscriberField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\NewsletterSubscriber\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param NewsletterSubscriberField[] $aFields */
	private array $aFields = [];


	/**
	 * @param NewsletterSubscriberField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof NewsletterSubscriberField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(NewsletterSubscriberField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): NewsletterSubscriberField
	{
		return current($this->aFields);
	}
}
