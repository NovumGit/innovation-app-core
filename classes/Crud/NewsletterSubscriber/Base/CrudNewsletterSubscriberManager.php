<?php
namespace Crud\NewsletterSubscriber\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\NewsletterSubscriber\FieldIterator;
use Crud\NewsletterSubscriber\Field\CompanyName;
use Crud\NewsletterSubscriber\Field\ConfirmHash;
use Crud\NewsletterSubscriber\Field\Confirmed;
use Crud\NewsletterSubscriber\Field\CreatedOn;
use Crud\NewsletterSubscriber\Field\CustomerId;
use Crud\NewsletterSubscriber\Field\Email;
use Crud\NewsletterSubscriber\Field\FirstName;
use Crud\NewsletterSubscriber\Field\LastName;
use Exception\LogicException;
use Model\Crm\Map\NewsletterSubscriberTableMap;
use Model\Crm\NewsletterSubscriber;
use Model\Crm\NewsletterSubscriberQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify NewsletterSubscriber instead if you need to override or add functionality.
 */
abstract class CrudNewsletterSubscriberManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return NewsletterSubscriberQuery::create();
	}


	public function getTableMap(): NewsletterSubscriberTableMap
	{
		return new NewsletterSubscriberTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "NewsletterSubscriber";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "newsletter_subscriber toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "newsletter_subscriber aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'CompanyName', 'Email', 'FirstName', 'LastName', 'Confirmed', 'ConfirmHash', 'CreatedOn'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CustomerId', 'CompanyName', 'Email', 'FirstName', 'LastName', 'Confirmed', 'ConfirmHash', 'CreatedOn'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return NewsletterSubscriber
	 */
	public function getModel(array $aData = null): NewsletterSubscriber
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oNewsletterSubscriberQuery = NewsletterSubscriberQuery::create();
		     $oNewsletterSubscriber = $oNewsletterSubscriberQuery->findOneById($aData['id']);
		     if (!$oNewsletterSubscriber instanceof NewsletterSubscriber) {
		         throw new LogicException("NewsletterSubscriber should be an instance of NewsletterSubscriber but got something else." . __METHOD__);
		     }
		     $oNewsletterSubscriber = $this->fillVo($aData, $oNewsletterSubscriber);
		}
		else {
		     $oNewsletterSubscriber = new NewsletterSubscriber();
		     if (!empty($aData)) {
		         $oNewsletterSubscriber = $this->fillVo($aData, $oNewsletterSubscriber);
		     }
		}
		return $oNewsletterSubscriber;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return NewsletterSubscriber
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): NewsletterSubscriber
	{
		$oNewsletterSubscriber = $this->getModel($aData);


		 if(!empty($oNewsletterSubscriber))
		 {
		     $oNewsletterSubscriber = $this->fillVo($aData, $oNewsletterSubscriber);
		     $oNewsletterSubscriber->save();
		 }
		return $oNewsletterSubscriber;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param NewsletterSubscriber $oModel
	 * @return NewsletterSubscriber
	 */
	protected function fillVo(array $aData, NewsletterSubscriber $oModel): NewsletterSubscriber
	{
		if(isset($aData['customer_id'])) {
		     $oField = new CustomerId();
		     $mValue = $oField->sanitize($aData['customer_id']);
		     $oModel->setCustomerId($mValue);
		}
		if(isset($aData['company_name'])) {
		     $oField = new CompanyName();
		     $mValue = $oField->sanitize($aData['company_name']);
		     $oModel->setCompanyName($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['first_name'])) {
		     $oField = new FirstName();
		     $mValue = $oField->sanitize($aData['first_name']);
		     $oModel->setFirstName($mValue);
		}
		if(isset($aData['last_name'])) {
		     $oField = new LastName();
		     $mValue = $oField->sanitize($aData['last_name']);
		     $oModel->setLastName($mValue);
		}
		if(isset($aData['confirmed'])) {
		     $oField = new Confirmed();
		     $mValue = $oField->sanitize($aData['confirmed']);
		     $oModel->setConfirmed($mValue);
		}
		if(isset($aData['confirm_hash'])) {
		     $oField = new ConfirmHash();
		     $mValue = $oField->sanitize($aData['confirm_hash']);
		     $oModel->setConfirmHash($mValue);
		}
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
