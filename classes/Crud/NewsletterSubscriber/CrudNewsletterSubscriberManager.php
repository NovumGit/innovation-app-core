<?php
namespace Crud\NewsletterSubscriber;

use Crud\FormManager;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Crm\NewsletterSubscriber;
use Model\Crm\NewsletterSubscriberQuery;

class CrudNewsletterSubscriberManager extends FormManager implements IConfigurableCrud
{
    function getEntityTitle():string
    {
        return 'newsletter_subscriber';
    }
    function getNewFormTitle():string{
        return 'Nieuwsbrief toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'Nieuwsbrief toevoegen bewerken';
    }
    function getOverviewUrl():string
    {
        return '/newsletter/overview/subscribers';
    }
    function getCreateNewUrl():string
    {
        return '/newsletter/edit/subscriber';
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'CustomerId',
            'CompanyName',
            'Email',
            'FirstName',
            'LastName',
            'Confirmed',
            'ConfirmHash'
        ];
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'CustomerId',
            'CompanyName',
            'Email',
            'FirstName',
            'LastName',
            'Confirmed',
            'ConfirmHash'
        ];
    }

    function getModel(array $aData = null)
    {
        if($aData['id'])
        {
            $oQuery = new NewsletterSubscriberQuery();
            $oNewsletterSubscriber = $oQuery->findOneById($aData['id']);
            if(!$oNewsletterSubscriber instanceof NewsletterSubscriber)
            {
                throw new LogicException("NewsletterSubscriber should be an instance of NewsletterSubscriber but got ".get_class($oNewsletterSubscriber)." in ".__METHOD__);
            }
        }
        else
        {
            $oNewsletterSubscriber = new NewsletterSubscriber();
            $oNewsletterSubscriber = $this->fillVo($oNewsletterSubscriber, $aData);
        }
        return $oNewsletterSubscriber;
    }

    function store(array $aData = null)
    {
        $oNewsletterSubscriber = $this->getModel($aData);

        if(!empty($oNewsletterSubscriber))
        {
            $oNewsletterSubscriber = $this->fillVo($oNewsletterSubscriber, $aData);
            $oNewsletterSubscriber->save();
        }
        return $oNewsletterSubscriber;
    }
    function fillVo(NewsletterSubscriber $oNewsletterSubscriber, $aData)
    {

        if(isset($aData['customer_id'])){ $oNewsletterSubscriber->setCustomerId($aData['customer_id']); }
        if(isset($aData['company_name'])){ $oNewsletterSubscriber->setCompanyName($aData['company_name']); }
        if(isset($aData['email'])){ $oNewsletterSubscriber->setEmail($aData['email']); }
        if(isset($aData['first_name'])){ $oNewsletterSubscriber->setFirstName($aData['first_name']); }
        if(isset($aData['last_name'])){ $oNewsletterSubscriber->setLastName($aData['last_name']); }
        if(isset($aData['confirmed'])){ $oNewsletterSubscriber->setConfirmed($aData['confirmed']); }
        if(isset($aData['confirm_hash'])){ $oNewsletterSubscriber->setConfirmHash($aData['confirm_hash']); }

        return $oNewsletterSubscriber;
    }

}
