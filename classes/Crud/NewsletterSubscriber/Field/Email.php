<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\NewsletterSubscriber\Field;

use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;

use Model\Crm\NewsletterSubscriber as ModelObject;


class Email extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'email';
    protected $sFieldLabel = 'E-mailadres';
    private $sIcon = 'envelope';
    private $sPlaceHolder = '';
    private $sGetter = 'getEmail';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }

    function getDataType():string
    {
        return 'string';
    }

    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return '<td class="">'.$oModelObject->{$this->sGetter}().'</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }

        return $this->editTextField($this->sFieldLabel, $this->sFieldName, $oModelObject->{$this->sGetter}(), $this->sPlaceHolder, $this->sIcon);
    }
}
