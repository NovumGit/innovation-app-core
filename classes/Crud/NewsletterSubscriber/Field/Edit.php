<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\NewsletterSubscriber\Field;


use Crud\Generic\Field\GenericEdit;
use Exception\LogicException;
use Model\Crm\NewsletterSubscriber;


class Edit extends GenericEdit
{
    function getEditUrl($oObject)
    {
        if(!$oObject instanceof NewsletterSubscriber)
        {
            throw new LogicException("Expected an instance of NewsletterSubscriber.");
        }

        return '/newsletter/edit/subscriber?id='.$oObject->getId();
    }
}