<?php
namespace Crud\NewsletterSubscriber\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\NewsletterSubscriber\ICollectionField;

/**
 * Base class that represents the 'first_name' crud field from the 'newsletter_subscriber' table.
 * This class is auto generated and should not be modified.
 */
abstract class FirstName extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'first_name';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFirstName';
	protected $sFqModelClassname = '\\\Model\Crm\NewsletterSubscriber';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
