<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\NewsletterSubscriber\Field;


use Crud\Generic\Field\GenericDelete;
use Exception\LogicException;
use Model\Crm\NewsletterSubscriber;

class Delete extends GenericDelete
{
    function getUnDeleteUrl($oObject)
    {
        throw new LogicException("Newsletter subscribers are permanently deleted, this method should never be called");
    }
    function getDeleteUrl($oObject)
    {
        if(!$oObject instanceof NewsletterSubscriber)
        {
            throw new LogicException("Expected an instance of NewsletterSubscriber.");
        }
        return '/newsletter/edit/subscriber?id='.$oObject->getId().'&_do=Delete';
    }
}