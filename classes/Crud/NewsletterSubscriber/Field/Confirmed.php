<?php
/*
 * @Author Anton Boutkam
 * @generated
 */
namespace Crud\NewsletterSubscriber\Field;

use Core\Translate;
use Crud\Field;
use Crud\IDisplayableField;
use Exception\InvalidArgumentException;
use Crud\IFilterableField;
use Crud\IEditableField;
use Model\Crm\NewsletterSubscriber as ModelObject;

class Confirmed extends Field implements IFilterableField, IEditableField, IDisplayableField {

    protected $sFieldName = 'confirmed';
    protected $sFieldLabel = 'Bevestigd';
    private $sGetter = 'getConfirmed';

    function getGetter()
    {
        return $this->sGetter;
    }

    function getFieldName()
    {
        return $this->sFieldName;
    }
    function getDataType():string
    {
        return 'boolean';
    }
    function hasValidations() { return false; }
    function validate($aPostedData)
    {
        $mResponse = false;
        return $mResponse;
    }
    function getOverviewHeader()
    {
        return $this->sortableHeaderField($this->getTranslatedTitle(), $this->sFieldName);
    }
    function getOverviewValue($oModelObject)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        $sYesNo = $oModelObject->getConfirmed() ? Translate::fromCode('Ja') : Translate::fromCode('Nee');
        return '<td class="">' . $sYesNo . '</td>';
    }
    function getFieldTitle(){
        return $this->getTranslatedTitle();
    }
    function getEditHtml($oModelObject, $bReadonly)
    {
        if(!$oModelObject instanceof ModelObject)
        {
            throw new InvalidArgumentException("Expected an instance of ModelObject in ".__METHOD__);
        }
        return $this->editBooleanField($this->sFieldLabel, $this->sFieldName, $oModelObject->getConfirmed(), $bReadonly);
    }
}
