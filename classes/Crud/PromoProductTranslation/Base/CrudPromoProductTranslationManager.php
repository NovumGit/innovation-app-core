<?php
namespace Crud\PromoProductTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PromoProductTranslation\FieldIterator;
use Crud\PromoProductTranslation\Field\AltUrl;
use Crud\PromoProductTranslation\Field\Description;
use Crud\PromoProductTranslation\Field\HasImage;
use Crud\PromoProductTranslation\Field\ImageExt;
use Crud\PromoProductTranslation\Field\LanguageId;
use Crud\PromoProductTranslation\Field\PromoProductId;
use Crud\PromoProductTranslation\Field\Title;
use Exception\LogicException;
use Model\Map\PromoProductTranslationTableMap;
use Model\PromoProductTranslation;
use Model\PromoProductTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PromoProductTranslation instead if you need to override or add functionality.
 */
abstract class CrudPromoProductTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PromoProductTranslationQuery::create();
	}


	public function getTableMap(): PromoProductTranslationTableMap
	{
		return new PromoProductTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PromoProductTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "promo_product_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "promo_product_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PromoProductId', 'AltUrl', 'LanguageId', 'Title', 'Description', 'HasImage', 'ImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['PromoProductId', 'AltUrl', 'LanguageId', 'Title', 'Description', 'HasImage', 'ImageExt'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PromoProductTranslation
	 */
	public function getModel(array $aData = null): PromoProductTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPromoProductTranslationQuery = PromoProductTranslationQuery::create();
		     $oPromoProductTranslation = $oPromoProductTranslationQuery->findOneById($aData['id']);
		     if (!$oPromoProductTranslation instanceof PromoProductTranslation) {
		         throw new LogicException("PromoProductTranslation should be an instance of PromoProductTranslation but got something else." . __METHOD__);
		     }
		     $oPromoProductTranslation = $this->fillVo($aData, $oPromoProductTranslation);
		}
		else {
		     $oPromoProductTranslation = new PromoProductTranslation();
		     if (!empty($aData)) {
		         $oPromoProductTranslation = $this->fillVo($aData, $oPromoProductTranslation);
		     }
		}
		return $oPromoProductTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PromoProductTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PromoProductTranslation
	{
		$oPromoProductTranslation = $this->getModel($aData);


		 if(!empty($oPromoProductTranslation))
		 {
		     $oPromoProductTranslation = $this->fillVo($aData, $oPromoProductTranslation);
		     $oPromoProductTranslation->save();
		 }
		return $oPromoProductTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PromoProductTranslation $oModel
	 * @return PromoProductTranslation
	 */
	protected function fillVo(array $aData, PromoProductTranslation $oModel): PromoProductTranslation
	{
		if(isset($aData['promo_product_id'])) {
		     $oField = new PromoProductId();
		     $mValue = $oField->sanitize($aData['promo_product_id']);
		     $oModel->setPromoProductId($mValue);
		}
		if(isset($aData['alt_url'])) {
		     $oField = new AltUrl();
		     $mValue = $oField->sanitize($aData['alt_url']);
		     $oModel->setAltUrl($mValue);
		}
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['has_image'])) {
		     $oField = new HasImage();
		     $mValue = $oField->sanitize($aData['has_image']);
		     $oModel->setHasImage($mValue);
		}
		if(isset($aData['image_ext'])) {
		     $oField = new ImageExt();
		     $mValue = $oField->sanitize($aData['image_ext']);
		     $oModel->setImageExt($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
