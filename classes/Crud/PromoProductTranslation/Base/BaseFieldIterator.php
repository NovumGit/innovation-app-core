<?php
namespace Crud\PromoProductTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\PromoProductTranslation\ICollectionField as PromoProductTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\PromoProductTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PromoProductTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PromoProductTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PromoProductTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PromoProductTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PromoProductTranslationField
	{
		return current($this->aFields);
	}
}
