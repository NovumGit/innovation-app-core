<?php
namespace Crud\PromoProductTranslation\Field;

use Crud\PromoProductTranslation\Field\Base\PromoProductId as BasePromoProductId;

/**
 * Skeleton subclass for representing promo_product_id field from the promo_product_translation table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class PromoProductId extends BasePromoProductId
{
}
