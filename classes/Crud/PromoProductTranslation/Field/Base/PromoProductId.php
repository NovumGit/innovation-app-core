<?php
namespace Crud\PromoProductTranslation\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\PromoProductTranslation\ICollectionField;

/**
 * Base class that represents the 'promo_product_id' crud field from the 'promo_product_translation' table.
 * This class is auto generated and should not be modified.
 */
abstract class PromoProductId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'promo_product_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPromoProductId';
	protected $sFqModelClassname = '\\\Model\PromoProductTranslation';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
