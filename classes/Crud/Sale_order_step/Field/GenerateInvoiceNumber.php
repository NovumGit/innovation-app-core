<?php
namespace Crud\Sale_order_step\Field;

use Crud\Sale_order_step\AbstractSale_order_step_field;
use Core\Translate;

class GenerateInvoiceNumber extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Factuurnummer genereren';

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }
    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->getInvoiceNumber() ? true : false;
    }
    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();

        $sClass = $oSaleOrder->isFullyPaid() ? 'success' : 'warning';
        $sChecked = $oSaleOrder->isFullyPaid() ? 'checked' : '';

        $aOut[] = '<li class="task-item '.$sClass.' item-'.$sChecked.'">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';

        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Factuur gegenereerd');
        $aOut[] = '        <span class="pull-right">';

        if(!$this->isDone())
        {
            $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Generate_invoice_number&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
            $aOut[] = '        '.Translate::fromCode('Genereer nu');
            $aOut[] = '                <i class="fa fa-money"></i>';
            $aOut[] = '             </a>';

        }
        else
        {
            $aOut[] = '             <a href="#">';
            $aOut[] = '        '.Translate::fromCode('Factuurnummer gegenereerd');
            $aOut[] = '';
            $aOut[] = '             </a>';
        }

        $aOut[] = '        </span>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';

        return join(PHP_EOL, $aOut);
    }
}
