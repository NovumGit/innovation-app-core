<?php
namespace Crud\Sale_order_step\Field;

use Crud\Sale_order_step\AbstractSale_order_step_field;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery;
use Core\Translate;

class InvoiceMailed extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Factuur gemailed';

    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        $oInvoiceMailType = Sale_order_notification_typeQuery::create()->findOneByCode('invoice_email');


        $oSaleOrderNotification = SaleOrderNotificationQuery::create()
            ->filterBySaleOrderNotificationTypeId($oInvoiceMailType->getId())
            ->filterBySaleOrderId($oSaleOrder->getId())
            ->findOne();

        return ($oSaleOrderNotification instanceof SaleOrderNotification);
    }

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }

    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();
        $oInvoiceMailType = Sale_order_notification_typeQuery::create()->findOneByCode('invoice_email');

        $sChecked = $this->isDone() ? 'checked' : '';

        $aOut[] = '<li class="task-item success item-'.$sChecked.'">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Factuur gemailed');
        $aOut[] = '        <span class="pull-right">';
        $aOut[] = '            <a href="/order/completed/status?order_id='.$oSaleOrder->getId().'&_do=TriggerEvent&event=Send_status_email&event_argument[notification_type_id]='.$oInvoiceMailType->getId().'&event_argument[order_id]='.$oSaleOrder->getId().'">';

        if( $this->isDone())
        {
            $aOut[] = '        '.Translate::fromCode('Nogmaals mailen');
        }
        else
        {
            $aOut[] = '        '.Translate::fromCode('Nu mailen');
        }
        $aOut[] = '                <i class="fa fa-envelope"></i>';
        $aOut[] = '            </a>';
        $aOut[] = '        </span>';
        $aOut[] = '    </div>';
        $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';

        return join(PHP_EOL, $aOut);
    }
}
