<?php
namespace Crud\Sale_order_step\Field;

use Core\Translate;
use Crud\Sale_order_step\AbstractSale_order_step_field;

class OrderPrepared extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Klaar gezet';

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }
    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->getIsReadyForShipping();
    }
    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();

        $sClass = $this->isDone() ? 'success' : 'warning';
        $sChecked = $this->isDone() ? 'checked' : '';

        $aOut[] = '<li class="task-item '.$sClass.' item-'.$sChecked.'">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Klaar voor verzending');
        $aOut[] = '        <span class="pull-right">';


        $sNewState = $this->isDone() ? '0' : '1';

        $aOut[] = '             <a href="/order/completed/status?order_id='.$oSaleOrder->getId().'&_do=TriggerEvent&event=Change_field&event_argument[value]='.$sNewState.'&event_argument[field]=IsReadyForShipping&event_argument[order_id]='.$oSaleOrder->getId().'">';
        if($this->isDone())
        {
            $aOut[] = '                '.Translate::fromCode('Terugdraaien');
            $aOut[] = '                <i class="fa fa-undo"></i>';
        }
        else
        {
            $aOut[] = '                '.Translate::fromCode('Nu klaarzetten');
            $aOut[] = '                <i class="fa fa-bolt"></i>';
        }

        $aOut[] = '            </a>';

        $aOut[] = '        </span>';



        $aOut[] = '    </div>';
        $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';

        return join(PHP_EOL, $aOut);
    }
}
