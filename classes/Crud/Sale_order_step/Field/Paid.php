<?php
namespace Crud\Sale_order_step\Field;

use Core\Translate;
use Crud\Sale_order_step\AbstractSale_order_step_field;

class Paid extends AbstractSale_order_step_field {

    protected $sFieldLabel = 'Betaald';

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }
    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->isFullyPaid();
    }
    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();

        $sClass = $oSaleOrder->isFullyPaid() ? 'success' : 'warning';
        $sChecked = $oSaleOrder->isFullyPaid() ? 'checked' : '';

        $aOut[] = '<li class="task-item '.$sClass.' item-'.$sChecked.'">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';

        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Order betaald');
        $aOut[] = '        <span class="pull-right">';

        if(!$this->isDone())
        {
            $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Change_field&event_argument[value]=1&event_argument[field]=IsFullyPaid&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
            $aOut[] = '        '.Translate::fromCode('Nu betalen');
            $aOut[] = '                <i class="fa fa-money"></i>';
            $aOut[] = '             </a>';

        }
        else
        {
            $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Change_field&event_argument[value]=0&event_argument[field]=IsFullyPaid&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
            $aOut[] = '        '.Translate::fromCode('Terugdraaien');
            $aOut[] = '                <i class="fa fa-undo"></i>';
            $aOut[] = '             </a>';
        }

        $aOut[] = '        </span>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';

        return join(PHP_EOL, $aOut);
    }
}