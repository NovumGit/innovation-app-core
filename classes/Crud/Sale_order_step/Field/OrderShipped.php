<?php
namespace Crud\Sale_order_step\Field;

use Core\Translate;
use Crud\Sale_order_step\AbstractSale_order_step_field;

class OrderShipped extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Bestelling verzonden';

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }

    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->getIsFullyShipped();
    }
    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();

        $sClass = $this->isDone() ? 'success' : 'warning';
        $sChecked = $this->isDone() ? 'checked' : '';

        $aOut[] = '<li class="task-item ' . $sClass . ' item-' . $sChecked . '">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" ' . $sChecked . '>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Bestelling verzonden');
        $aOut[] = '        <span class="pull-right">';

        if(!$this->isDone())
        {
            $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Change_field&event_argument[value]=1&event_argument[field]=IsFullyShipped&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
            $aOut[] = '                Nu verzenden';
            $aOut[] = '                <i class="fa fa-envelope"></i>';
        }
        else
        {
            $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Change_field&event_argument[value]=0&event_argument[field]=IsFullyShipped&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
            $aOut[] = '                Terugdraaien';
            $aOut[] = '                <i class="fa fa-undo"></i>';
        }
        $aOut[] = '            </a>';

        $aOut[] = '        </span>';


        $aOut[] = '    </div>';
        $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';
        return join(PHP_EOL, $aOut);
    }
}