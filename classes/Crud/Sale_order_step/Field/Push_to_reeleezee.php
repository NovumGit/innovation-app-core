<?php
namespace Crud\Sale_order_step\Field;

use Api\Push\Accounting\Reeleezee\Reeleezee_config;
use Crud\Sale_order_step\AbstractSale_order_step_field;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery;
use Core\Translate;

class Push_to_reeleezee extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Naar Reeleezee gepushed';

    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->getIsPushedToAccounting();
    }

    function getLinkUrl()
    {
        return '';
    }


    function getHtml():string
    {


        // Dit systeem maakt alleen maar een json bestandje aan met het juiste ordernummer.
        // De order staat vanaf dat moment in de queue.
        $oSaleOrder = $this->getSaleOrder();
        if(Reeleezee_config::filter($oSaleOrder))
        {
            $sChecked = $oSaleOrder->getIsPushedToAccounting() ? 'checked' : '';

            $aOut[] = '<li class="task-item success item-'.$sChecked.'">';
            $aOut[] = '    <div class="task-handle">';
            $aOut[] = '        <div class="checkbox-custom">';
            $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
            $aOut[] = '             <label for="task0"></label>';
            $aOut[] = '        </div>';
            $aOut[] = '    </div>';
            $aOut[] = '    <div class="task-desc">';
            $aOut[] = '        '.Translate::fromCode('Gepushed naar Reeleezee');
            $aOut[] = '        <span class="pull-right">';
            $aOut[] = '            <a href="/order/completed/status?order_id='.$oSaleOrder->getId().'&_do=PushToReeleezee">';

            if( $oSaleOrder->getIsPushedToAccounting())
            {
                $aOut[] = '        '.Translate::fromCode('Nogmaals pushen');
            }
            else
            {
                $aOut[] = '        '.Translate::fromCode('Nu pushen');
            }
            $aOut[] = '                <i class="fa fa-cloud-upload"></i>';
            $aOut[] = '            </a>';
            $aOut[] = '        </span>';
            $aOut[] = '    </div>';
            $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
            $aOut[] = '</li>';

            return join(PHP_EOL, $aOut);
        }
        return '';
    }
}
