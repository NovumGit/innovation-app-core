<?php
namespace Crud\Sale_order_step\Field;

use Core\Translate;
use Crud\Sale_order_step\AbstractSale_order_step_field;

class OrderChecked extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Gecontroleerd';

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }

    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        return $oSaleOrder->getIsShippingChecked();
    }
    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();

        $sClass = $this->isDone() ? 'success' : 'warning';
        $sChecked = $this->isDone() ? 'checked' : '';

        $aOut[] = '<li class="task-item ' . $sClass . ' item-' . $sChecked . '">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" ' . $sChecked . '>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Verzending gecontroleerd');
        $aOut[] = '        <span class="pull-right">';


        $sNewState = $this->isDone() ? '0' : '1';

        $aOut[] = '             <a href="/order/completed/status?order_id=' . $oSaleOrder->getId() . '&_do=TriggerEvent&event=Change_field&event_argument[value]=' . $sNewState . '&event_argument[field]=IsShippingChecked&event_argument[order_id]=' . $oSaleOrder->getId() . '">';
        if ($this->isDone()) {
            $aOut[] = '                '.Translate::fromCode('Terugdraaien');
            $aOut[] = '                <i class="fa fa-undo"></i>';
        } else {
            $aOut[] = '                 '.Translate::fromCode('Gecontroleerd');
            $aOut[] = '                <i class="fa fa-bolt"></i>';
        }

        $aOut[] = '            </a>';

        $aOut[] = '        </span>';


        $aOut[] = '    </div>';
        $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';
        return join(PHP_EOL, $aOut);
    }
}