<?php
namespace Crud\Sale_order_step\Field;

use Core\Translate;
use Crud\Sale_order_step\AbstractSale_order_step_field;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery;

class TrackAndTraceMailed extends AbstractSale_order_step_field  {

    protected $sFieldLabel = 'Track & trace registreren + mailen';

    function isDone():bool
    {
        $oSaleOrder = $this->getSaleOrder();
        $oInvoiceMailType = Sale_order_notification_typeQuery::create()->findOneByCode('track_and_trace');


        $oSaleOrderNotification = SaleOrderNotificationQuery::create()
            ->filterBySaleOrderNotificationTypeId($oInvoiceMailType->getId())
            ->filterBySaleOrderId($oSaleOrder->getId())
            ->findOne();

        return ($oSaleOrderNotification instanceof SaleOrderNotification);
    }

    function getLinkUrl()
    {
        // TODO: Implement getLinkUrl() method.
    }

    function getHtml():string
    {
        $oSaleOrder = $this->getSaleOrder();
        $oInvoiceMailType = Sale_order_notification_typeQuery::create()->findOneByCode('track_and_trace');

        if(!$oSaleOrder->getShippingHasTrackTrace())
        {
            return '';
        }
        $sChecked = $this->isDone() ? 'checked' : '';

        $aOut[] = '<li class="task-item success item-'.$sChecked.'">';
        $aOut[] = '    <div class="task-handle">';
        $aOut[] = '        <div class="checkbox-custom">';
        $aOut[] = '            <input id="task0" type="checkbox" '.$sChecked.'>';
        $aOut[] = '             <label for="task0"></label>';
        $aOut[] = '        </div>';
        $aOut[] = '    </div>';
        $aOut[] = '    <div class="task-desc">';
        $aOut[] = '        '.Translate::fromCode('Track & trace registreren + mailen');
        $aOut[] = '        <span class="pull-right">';
        $aOut[] = '            <a href="/order/edit/edit?order_id='.$oSaleOrder->getId().'&r=return_order_summary&view=track_trace">';
        if( $this->isDone())
        {
            $aOut[] = '        '.Translate::fromCode('Nu wijzigen');
        }
        else
        {
            $aOut[] = '        '.Translate::fromCode('Nu invoeren');
        }

        $aOut[] = '                <i class="fa fa-globe"></i>';
        $aOut[] = '            </a>';
        $aOut[] = '        </span>';
        $aOut[] = '    </div>';
        $aOut[] = '<div class="task-menu ui-sortable-handle"></div>';
        $aOut[] = '</li>';

        return join(PHP_EOL, $aOut);
    }
}
