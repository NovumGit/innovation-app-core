<?php
namespace Crud\Sale_order_step;

use Crud\Link_blockManager;

class CrudSale_order_stepManager extends Link_blockManager
{
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Paid',
            'InvoiceMailed',
            'OrderPacked',
            'PackingslipAdded',
            'OrderChecked',
            'OrderPrepared',
            'OrderShipped',
            'TrackAndTraceMailed',
        ];
    }

    /*
    function getEntityTitle():string
    {
        return 'Order steps';
    }

    function getNewFormTitle():string{
        return 'StatusId toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'StatusId bewerken';
    }
    function getOverviewUrl():string
    {
        return '/admin/mastertable/sale_order_status/overview';
    }
    function getCreateNewUrl():string
    {
        return '/admin/mastertable/sale_order_status/edit';
    }

    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array{

        return [
            'Name',
        ];
    }

    function getModel(array $aData = null)
    {
        var_dump($aData);
        if($aData['id'])
        {
            $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneById($aData['id']);

            if(!$oSaleOrderStatus instanceof SaleOrderStatus)
            {
                var_dump($oSaleOrderStatus);
                throw new LogicException("SaleOrderStatus should be an instance of SaleOrderStatus but got ".get_class($oSaleOrderStatus)." in ".__METHOD__);
            }
        }
        else
        {
            $oSaleOrderStatus = new SaleOrderStatus();
            $oSaleOrderStatus = $this->fillVo($oSaleOrderStatus, $aData);
        }
        return $oSaleOrderStatus;
    }
    function store(array $aData = null)
    {
        $oSaleOrderStatus = $this->getModel($aData);

        if(!empty($oSaleOrderStatus))
        {

            $oSaleOrderStatus = $this->fillVo($oSaleOrderStatus, $aData);
            $oSaleOrderStatus->save();
        }
        return $oSaleOrderStatus;
    }
    function fillVo(SaleOrderStatus $oSaleOrderStatus, $aData)
    {

        if(isset($aData['name'])){
            $oSaleOrderStatus->setName($aData['name']);
        }
        if(isset($aData['code'])){
            $oSaleOrderStatus->setCode($aData['code']);
        }
        if(isset($aData['is_deletable'])){
            $oSaleOrderStatus->setIsDeletable($aData['is_deletable']);
        }
        return $oSaleOrderStatus;
    }
    */
}
