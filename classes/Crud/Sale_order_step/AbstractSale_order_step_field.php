<?php
namespace Crud\Sale_order_step;

use Crud\Link;
use Model\Sale\SaleOrder;

abstract class AbstractSale_order_step_field extends Link
{
    function getSaleOrder():SaleOrder
    {
        return $this->getArgument('sale_order');
    }
    abstract public function isDone():bool;
    abstract public function getHtml():string;

}

