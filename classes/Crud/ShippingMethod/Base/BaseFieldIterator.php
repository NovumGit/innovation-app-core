<?php
namespace Crud\ShippingMethod\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\ShippingMethod\ICollectionField as ShippingMethodField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ShippingMethod\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ShippingMethodField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ShippingMethodField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ShippingMethodField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ShippingMethodField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ShippingMethodField
	{
		return current($this->aFields);
	}
}
