<?php
namespace Crud\ShippingMethod\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\ShippingMethod\FieldIterator;
use Crud\ShippingMethod\Field\Code;
use Crud\ShippingMethod\Field\IsDefaultFree;
use Crud\ShippingMethod\Field\IsTrackTrace;
use Crud\ShippingMethod\Field\MaxWeightGrams;
use Crud\ShippingMethod\Field\MinWeightGrams;
use Crud\ShippingMethod\Field\Name;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\ShippingMethodTableMap;
use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ShippingMethod instead if you need to override or add functionality.
 */
abstract class CrudShippingMethodManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ShippingMethodQuery::create();
	}


	public function getTableMap(): ShippingMethodTableMap
	{
		return new ShippingMethodTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ShippingMethod";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_shipping_method toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_shipping_method aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'IsDefaultFree', 'IsTrackTrace', 'MinWeightGrams', 'MaxWeightGrams'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Code', 'IsDefaultFree', 'IsTrackTrace', 'MinWeightGrams', 'MaxWeightGrams'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ShippingMethod
	 */
	public function getModel(array $aData = null): ShippingMethod
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oShippingMethodQuery = ShippingMethodQuery::create();
		     $oShippingMethod = $oShippingMethodQuery->findOneById($aData['id']);
		     if (!$oShippingMethod instanceof ShippingMethod) {
		         throw new LogicException("ShippingMethod should be an instance of ShippingMethod but got something else." . __METHOD__);
		     }
		     $oShippingMethod = $this->fillVo($aData, $oShippingMethod);
		}
		else {
		     $oShippingMethod = new ShippingMethod();
		     if (!empty($aData)) {
		         $oShippingMethod = $this->fillVo($aData, $oShippingMethod);
		     }
		}
		return $oShippingMethod;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ShippingMethod
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ShippingMethod
	{
		$oShippingMethod = $this->getModel($aData);


		 if(!empty($oShippingMethod))
		 {
		     $oShippingMethod = $this->fillVo($aData, $oShippingMethod);
		     $oShippingMethod->save();
		 }
		return $oShippingMethod;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ShippingMethod $oModel
	 * @return ShippingMethod
	 */
	protected function fillVo(array $aData, ShippingMethod $oModel): ShippingMethod
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		if(isset($aData['is_default_free'])) {
		     $oField = new IsDefaultFree();
		     $mValue = $oField->sanitize($aData['is_default_free']);
		     $oModel->setIsDefaultFree($mValue);
		}
		if(isset($aData['is_track_trace'])) {
		     $oField = new IsTrackTrace();
		     $mValue = $oField->sanitize($aData['is_track_trace']);
		     $oModel->setIsTrackTrace($mValue);
		}
		if(isset($aData['min_weight_grams'])) {
		     $oField = new MinWeightGrams();
		     $mValue = $oField->sanitize($aData['min_weight_grams']);
		     $oModel->setMinWeightGrams($mValue);
		}
		if(isset($aData['max_weight_grams'])) {
		     $oField = new MaxWeightGrams();
		     $mValue = $oField->sanitize($aData['max_weight_grams']);
		     $oModel->setMaxWeightGrams($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
