<?php
namespace Crud\ShippingMethod\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\ShippingMethod\ICollectionField;

/**
 * Base class that represents the 'max_weight_grams' crud field from the 'mt_shipping_method' table.
 * This class is auto generated and should not be modified.
 */
abstract class MaxWeightGrams extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'max_weight_grams';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getMaxWeightGrams';
	protected $sFqModelClassname = '\\\Model\Setting\MasterTable\ShippingMethod';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
