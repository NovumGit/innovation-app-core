<?php
namespace Crud\ShippingMethod\Field;

use Crud\ShippingMethod\Field\Base\IsDefaultFree as BaseIsDefaultFree;

/**
 * Skeleton subclass for representing is_default_free field from the mt_shipping_method table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class IsDefaultFree extends BaseIsDefaultFree
{
}
