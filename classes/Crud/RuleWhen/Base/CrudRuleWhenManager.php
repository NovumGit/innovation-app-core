<?php
namespace Crud\RuleWhen\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\RuleWhen\FieldIterator;
use Crud\RuleWhen\Field\RuleId;
use Exception\LogicException;
use Model\Rule\Map\RuleWhenTableMap;
use Model\Rule\RuleWhen;
use Model\Rule\RuleWhenQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify RuleWhen instead if you need to override or add functionality.
 */
abstract class CrudRuleWhenManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return RuleWhenQuery::create();
	}


	public function getTableMap(): RuleWhenTableMap
	{
		return new RuleWhenTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "RuleWhen";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "rule_when toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "rule_when aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RuleId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['RuleId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return RuleWhen
	 */
	public function getModel(array $aData = null): RuleWhen
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oRuleWhenQuery = RuleWhenQuery::create();
		     $oRuleWhen = $oRuleWhenQuery->findOneById($aData['id']);
		     if (!$oRuleWhen instanceof RuleWhen) {
		         throw new LogicException("RuleWhen should be an instance of RuleWhen but got something else." . __METHOD__);
		     }
		     $oRuleWhen = $this->fillVo($aData, $oRuleWhen);
		}
		else {
		     $oRuleWhen = new RuleWhen();
		     if (!empty($aData)) {
		         $oRuleWhen = $this->fillVo($aData, $oRuleWhen);
		     }
		}
		return $oRuleWhen;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return RuleWhen
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): RuleWhen
	{
		$oRuleWhen = $this->getModel($aData);


		 if(!empty($oRuleWhen))
		 {
		     $oRuleWhen = $this->fillVo($aData, $oRuleWhen);
		     $oRuleWhen->save();
		 }
		return $oRuleWhen;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param RuleWhen $oModel
	 * @return RuleWhen
	 */
	protected function fillVo(array $aData, RuleWhen $oModel): RuleWhen
	{
		if(isset($aData['rule_id'])) {
		     $oField = new RuleId();
		     $mValue = $oField->sanitize($aData['rule_id']);
		     $oModel->setRuleId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
