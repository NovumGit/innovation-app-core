<?php
namespace Crud\RuleWhen\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\RuleWhen\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
