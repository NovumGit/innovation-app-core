<?php
namespace Crud\RuleWhen\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\RuleWhen\ICollectionField;

/**
 * Base class that represents the 'rule_id' crud field from the 'rule_when' table.
 * This class is auto generated and should not be modified.
 */
abstract class RuleId extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'rule_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getRuleId';
	protected $sFqModelClassname = '\\\Model\Rule\RuleWhen';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
