<?php
namespace Crud\RuleWhen\Field;

use Crud\RuleWhen\Field\Base\RuleId as BaseRuleId;

/**
 * Skeleton subclass for representing rule_id field from the rule_when table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class RuleId extends BaseRuleId
{
}
