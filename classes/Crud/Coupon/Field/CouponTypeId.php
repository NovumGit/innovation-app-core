<?php

namespace Crud\Coupon\Field;

use Crud\Coupon\Field\Base\CouponTypeId as BaseCouponTypeId;

/**
 * Skeleton subclass for representing coupon_type_id field from the coupon table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class CouponTypeId extends BaseCouponTypeId
{
}
