<?php

namespace Crud\Coupon\Field;

use Crud\Coupon\Field\Base\RedeemCode as BaseRedeemCode;

/**
 * Skeleton subclass for representing redeem_code field from the coupon table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class RedeemCode extends BaseRedeemCode
{
}
