<?php
namespace Crud\Coupon\Base;

use Core\Utils;
use Crud;
use Crud\Coupon\FieldIterator;
use Crud\Coupon\Field\CouponTypeId;
use Crud\Coupon\Field\GivenOut;
use Crud\Coupon\Field\RedeemCode;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Marketing\Coupon;
use Model\Marketing\CouponQuery;
use Model\Marketing\Map\CouponTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Coupon instead if you need to override or add functionality.
 */
abstract class CrudCouponManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CouponQuery::create();
	}


	public function getTableMap(): CouponTableMap
	{
		return new CouponTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Coupon";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "coupon toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "coupon aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CouponTypeId', 'RedeemCode', 'GivenOut'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CouponTypeId', 'RedeemCode', 'GivenOut'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Coupon
	 */
	public function getModel(array $aData = null): Coupon
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCouponQuery = CouponQuery::create();
		     $oCoupon = $oCouponQuery->findOneById($aData['id']);
		     if (!$oCoupon instanceof Coupon) {
		         throw new LogicException("Coupon should be an instance of Coupon but got something else." . __METHOD__);
		     }
		     $oCoupon = $this->fillVo($aData, $oCoupon);
		}
		else {
		     $oCoupon = new Coupon();
		     if (!empty($aData)) {
		         $oCoupon = $this->fillVo($aData, $oCoupon);
		     }
		}
		return $oCoupon;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Coupon
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Coupon
	{
		$oCoupon = $this->getModel($aData);


		 if(!empty($oCoupon))
		 {
		     $oCoupon = $this->fillVo($aData, $oCoupon);
		     $oCoupon->save();
		 }
		return $oCoupon;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Coupon $oModel
	 * @return Coupon
	 */
	protected function fillVo(array $aData, Coupon $oModel): Coupon
	{
		if(isset($aData['coupon_type_id'])) {
		     $oField = new CouponTypeId();
		     $mValue = $oField->sanitize($aData['coupon_type_id']);
		     $oModel->setCouponTypeId($mValue);
		}
		if(isset($aData['redeem_code'])) {
		     $oField = new RedeemCode();
		     $mValue = $oField->sanitize($aData['redeem_code']);
		     $oModel->setRedeemCode($mValue);
		}
		if(isset($aData['given_out'])) {
		     $oField = new GivenOut();
		     $mValue = $oField->sanitize($aData['given_out']);
		     $oModel->setGivenOut($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
