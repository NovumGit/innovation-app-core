<?php
namespace Crud\Coupon\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Coupon\ICollectionField as CouponField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Coupon\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CouponField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CouponField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CouponField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CouponField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CouponField
	{
		return current($this->aFields);
	}
}
