<?php
namespace Crud\Wet\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Wet\FieldIterator;
use Crud\Wet\Field\Code;
use Crud\Wet\Field\Titel;
use Exception\LogicException;
use Model\Eenoverheid\Map\WetTableMap;
use Model\Eenoverheid\Wet;
use Model\Eenoverheid\WetQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Wet instead if you need to override or add functionality.
 */
abstract class CrudWetManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return WetQuery::create();
	}


	public function getTableMap(): WetTableMap
	{
		return new WetTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat de wet of dienst waar informatie voor nodig is.";
	}


	public function getEntityTitle(): string
	{
		return "Wet";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "wet toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "wet aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Titel', 'Code'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Wet
	 */
	public function getModel(array $aData = null): Wet
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oWetQuery = WetQuery::create();
		     $oWet = $oWetQuery->findOneById($aData['id']);
		     if (!$oWet instanceof Wet) {
		         throw new LogicException("Wet should be an instance of Wet but got something else." . __METHOD__);
		     }
		     $oWet = $this->fillVo($aData, $oWet);
		}
		else {
		     $oWet = new Wet();
		     if (!empty($aData)) {
		         $oWet = $this->fillVo($aData, $oWet);
		     }
		}
		return $oWet;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Wet
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Wet
	{
		$oWet = $this->getModel($aData);


		 if(!empty($oWet))
		 {
		     $oWet = $this->fillVo($aData, $oWet);
		     $oWet->save();
		 }
		return $oWet;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Wet $oModel
	 * @return Wet
	 */
	protected function fillVo(array $aData, Wet $oModel): Wet
	{
		if(isset($aData['titel'])) {
		     $oField = new Titel();
		     $mValue = $oField->sanitize($aData['titel']);
		     $oModel->setTitel($mValue);
		}
		if(isset($aData['code'])) {
		     $oField = new Code();
		     $mValue = $oField->sanitize($aData['code']);
		     $oModel->setCode($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
