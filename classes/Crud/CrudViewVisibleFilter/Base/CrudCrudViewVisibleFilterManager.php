<?php
namespace Crud\CrudViewVisibleFilter\Base;

use Core\Utils;
use Crud;
use Crud\CrudViewVisibleFilter\FieldIterator;
use Crud\CrudViewVisibleFilter\Field\CrudViewId;
use Crud\CrudViewVisibleFilter\Field\FilterDefaultValue;
use Crud\CrudViewVisibleFilter\Field\FilterName;
use Crud\CrudViewVisibleFilter\Field\FilterOperatorId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery;
use Model\Setting\CrudManager\Map\CrudViewVisibleFilterTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CrudViewVisibleFilter instead if you need to override or add functionality.
 */
abstract class CrudCrudViewVisibleFilterManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CrudViewVisibleFilterQuery::create();
	}


	public function getTableMap(): CrudViewVisibleFilterTableMap
	{
		return new CrudViewVisibleFilterTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint welke filters beschikbaar zijn gemaakt bij een overzicht.";
	}


	public function getEntityTitle(): string
	{
		return "CrudViewVisibleFilter";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "crud_view_visible_filter toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "crud_view_visible_filter aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FilterOperatorId', 'FilterName', 'FilterDefaultValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CrudViewId', 'FilterOperatorId', 'FilterName', 'FilterDefaultValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CrudViewVisibleFilter
	 */
	public function getModel(array $aData = null): CrudViewVisibleFilter
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCrudViewVisibleFilterQuery = CrudViewVisibleFilterQuery::create();
		     $oCrudViewVisibleFilter = $oCrudViewVisibleFilterQuery->findOneById($aData['id']);
		     if (!$oCrudViewVisibleFilter instanceof CrudViewVisibleFilter) {
		         throw new LogicException("CrudViewVisibleFilter should be an instance of CrudViewVisibleFilter but got something else." . __METHOD__);
		     }
		     $oCrudViewVisibleFilter = $this->fillVo($aData, $oCrudViewVisibleFilter);
		}
		else {
		     $oCrudViewVisibleFilter = new CrudViewVisibleFilter();
		     if (!empty($aData)) {
		         $oCrudViewVisibleFilter = $this->fillVo($aData, $oCrudViewVisibleFilter);
		     }
		}
		return $oCrudViewVisibleFilter;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CrudViewVisibleFilter
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CrudViewVisibleFilter
	{
		$oCrudViewVisibleFilter = $this->getModel($aData);


		 if(!empty($oCrudViewVisibleFilter))
		 {
		     $oCrudViewVisibleFilter = $this->fillVo($aData, $oCrudViewVisibleFilter);
		     $oCrudViewVisibleFilter->save();
		 }
		return $oCrudViewVisibleFilter;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CrudViewVisibleFilter $oModel
	 * @return CrudViewVisibleFilter
	 */
	protected function fillVo(array $aData, CrudViewVisibleFilter $oModel): CrudViewVisibleFilter
	{
		if(isset($aData['crud_view_id'])) {
		     $oField = new CrudViewId();
		     $mValue = $oField->sanitize($aData['crud_view_id']);
		     $oModel->setCrudViewId($mValue);
		}
		if(isset($aData['filter_operator_id'])) {
		     $oField = new FilterOperatorId();
		     $mValue = $oField->sanitize($aData['filter_operator_id']);
		     $oModel->setFilterOperatorId($mValue);
		}
		if(isset($aData['filter_name'])) {
		     $oField = new FilterName();
		     $mValue = $oField->sanitize($aData['filter_name']);
		     $oModel->setFilterName($mValue);
		}
		if(isset($aData['filter_default_value'])) {
		     $oField = new FilterDefaultValue();
		     $mValue = $oField->sanitize($aData['filter_default_value']);
		     $oModel->setFilterDefaultValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
