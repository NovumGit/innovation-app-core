<?php
namespace Crud\CrudViewVisibleFilter\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CrudViewVisibleFilter\ICollectionField as CrudViewVisibleFilterField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CrudViewVisibleFilter\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CrudViewVisibleFilterField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CrudViewVisibleFilterField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CrudViewVisibleFilterField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CrudViewVisibleFilterField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CrudViewVisibleFilterField
	{
		return current($this->aFields);
	}
}
