<?php
namespace Crud\CrudViewVisibleFilter\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\CrudViewVisibleFilter\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
