<?php
namespace Crud\CrudViewVisibleFilter\Field;

use Crud\CrudViewVisibleFilter\Field\Base\FilterDefaultValue as BaseFilterDefaultValue;

/**
 * Skeleton subclass for representing filter_default_value field from the crud_view_visible_filter table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FilterDefaultValue extends BaseFilterDefaultValue
{
}
