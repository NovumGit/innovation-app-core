<?php
namespace Crud\CrudViewVisibleFilter\Field\Base;

use Crud\CrudViewVisibleFilter\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'filter_default_value' crud field from the 'crud_view_visible_filter' table.
 * This class is auto generated and should not be modified.
 */
abstract class FilterDefaultValue extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'filter_default_value';
	protected $sFieldLabel = 'Waarde';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFilterDefaultValue';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\CrudViewVisibleFilter';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['filter_default_value']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Waarde" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
