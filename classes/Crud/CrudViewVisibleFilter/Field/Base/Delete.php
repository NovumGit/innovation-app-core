<?php 
namespace Crud\CrudViewVisibleFilter\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\CrudViewVisibleFilter;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewVisibleFilter)
		{
		     return "//system/crud_view_visible_filter/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof CrudViewVisibleFilter)
		{
		     return "//crud_view_visible_filter?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
