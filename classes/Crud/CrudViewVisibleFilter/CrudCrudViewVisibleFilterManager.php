<?php
namespace Crud\CrudViewVisibleFilter;

/**
 * Skeleton subclass for representing a CrudViewVisibleFilter.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudCrudViewVisibleFilterManager extends Base\CrudCrudViewVisibleFilterManager
{
}
