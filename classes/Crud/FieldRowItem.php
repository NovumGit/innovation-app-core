<?php
namespace Crud;


use Model\Setting\CrudManager\CrudEditorBlockField;

final class FieldRowItem
{
    private $oCrudEditorBlockField;
    private $iWidth;
    function __construct(CrudEditorBlockField $oCrudEditorBlockField, int $iWidth)
    {
        $this->iWidth = $iWidth;
        $this->oCrudEditorBlockField = $oCrudEditorBlockField;
    }

    function getField():Field
    {
        return $this->oCrudEditorBlockField->getFieldObject();
    }

    function getWidth():int
    {
        return $this->iWidth;
    }

    function getBlockField():CrudEditorBlockField
    {
        return $this->oCrudEditorBlockField;
    }
}
