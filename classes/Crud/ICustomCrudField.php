<?php
namespace Crud;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

interface ICustomCrudField{
    function store(ActiveRecordInterface $oObject, array $aData);
}
