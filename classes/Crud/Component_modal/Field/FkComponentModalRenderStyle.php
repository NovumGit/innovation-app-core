<?php
namespace Crud\Component_modal\Field;

use Crud\Component_modal\Field\Base\FkComponentModalRenderStyle as BaseFkComponentModalRenderStyle;

/**
 * Skeleton subclass for representing fk_component_modal_render_style field from the component_modal table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentModalRenderStyle extends BaseFkComponentModalRenderStyle
{
}
