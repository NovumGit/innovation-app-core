<?php
namespace Crud\Component_modal\Field;

use Crud\Component_modal\Field\Base\FkComponentModalButtonLocation as BaseFkComponentModalButtonLocation;

/**
 * Skeleton subclass for representing fk_component_modal_button_location field from the component_modal table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentModalButtonLocation extends BaseFkComponentModalButtonLocation
{
}
