<?php
namespace Crud\Component_modal\Field;

use Crud\Component_modal\Field\Base\ComponentKey as BaseComponentKey;

/**
 * Skeleton subclass for representing component_key field from the component_modal table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ComponentKey extends BaseComponentKey
{
}
