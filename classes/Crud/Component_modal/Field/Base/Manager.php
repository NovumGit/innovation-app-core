<?php
namespace Crud\Component_modal\Field\Base;

use Crud\Component_modal\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'manager' crud field from the 'component_modal' table.
 * This class is auto generated and should not be modified.
 */
abstract class Manager extends GenericLookup implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'manager';
	protected $sFieldLabel = 'Reference to a CrudManager';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getManager';
	protected $sFqModelClassname = '\Model\System\LowCode\Modal\Component_modal';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
