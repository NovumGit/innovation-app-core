<?php
namespace Crud\Component_modal\Field\Base;

use Crud\Component_modal\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'component_key' crud field from the 'component_modal' table.
 * This class is auto generated and should not be modified.
 */
abstract class ComponentKey extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'component_key';
	protected $sFieldLabel = 'Layout key';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getComponentKey';
	protected $sFqModelClassname = '\Model\System\LowCode\Modal\Component_modal';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
