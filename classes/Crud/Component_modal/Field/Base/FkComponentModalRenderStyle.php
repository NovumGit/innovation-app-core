<?php
namespace Crud\Component_modal\Field\Base;

use Core\Utils;
use Crud\Component_modal\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\System\LowCode\Modal\Component_modal_render_styleQuery;

/**
 * Base class that represents the 'fk_component_modal_render_style' crud field from the 'component_modal' table.
 * This class is auto generated and should not be modified.
 */
abstract class FkComponentModalRenderStyle extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField
{
	protected $sFieldName = 'fk_component_modal_render_style';
	protected $sFieldLabel = 'Form rendering style';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getFkComponentModalRenderStyle';
	protected $sFqModelClassname = '\Model\System\LowCode\Modal\Component_modal';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\LowCode\Modal\Component_modal_render_styleQuery::create()->orderByItemLabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getItemLabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\LowCode\Modal\Component_modal_render_styleQuery::create()->findOneById($iItemId)->getItemLabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
