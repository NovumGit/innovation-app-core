<?php 
namespace Crud\Component_modal\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\LowCode\Modal\Component_modal;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_modal)
		{
		     return "//system/component_modal/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Component_modal)
		{
		     return "//component_modal?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
