<?php
namespace Crud\Component_modal\Base;

use Core\Utils;
use Crud;
use Crud\Component_modal\FieldIterator;
use Crud\Component_modal\Field\ComponentKey;
use Crud\Component_modal\Field\FkComponentModalButtonLocation;
use Crud\Component_modal\Field\FkComponentModalRenderStyle;
use Crud\Component_modal\Field\Manager;
use Crud\Component_modal\Field\Title;
use Crud\Component_modal\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Modal\Component_modal;
use Model\System\LowCode\Modal\Component_modalQuery;
use Model\System\LowCode\Modal\Map\Component_modalTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_modal instead if you need to override or add functionality.
 */
abstract class CrudComponent_modalManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_modalQuery::create();
	}


	public function getTableMap(): Component_modalTableMap
	{
		return new Component_modalTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Modal component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_modal";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_modal toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_modal aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'ComponentKey', 'Title', 'FkComponentModalRenderStyle', 'FkComponentModalButtonLocation', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Manager', 'ComponentKey', 'Title', 'FkComponentModalRenderStyle', 'FkComponentModalButtonLocation', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_modal
	 */
	public function getModel(array $aData = null): Component_modal
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_modalQuery = Component_modalQuery::create();
		     $oComponent_modal = $oComponent_modalQuery->findOneById($aData['id']);
		     if (!$oComponent_modal instanceof Component_modal) {
		         throw new LogicException("Component_modal should be an instance of Component_modal but got something else." . __METHOD__);
		     }
		     $oComponent_modal = $this->fillVo($aData, $oComponent_modal);
		}
		else {
		     $oComponent_modal = new Component_modal();
		     if (!empty($aData)) {
		         $oComponent_modal = $this->fillVo($aData, $oComponent_modal);
		     }
		}
		return $oComponent_modal;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_modal
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_modal
	{
		$oComponent_modal = $this->getModel($aData);


		 if(!empty($oComponent_modal))
		 {
		     $oComponent_modal = $this->fillVo($aData, $oComponent_modal);
		     $oComponent_modal->save();
		 }
		return $oComponent_modal;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_modal $oModel
	 * @return Component_modal
	 */
	protected function fillVo(array $aData, Component_modal $oModel): Component_modal
	{
		if(isset($aData['manager'])) {
		     $oField = new Manager();
		     $mValue = $oField->sanitize($aData['manager']);
		     $oModel->setManager($mValue);
		}
		if(isset($aData['component_key'])) {
		     $oField = new ComponentKey();
		     $mValue = $oField->sanitize($aData['component_key']);
		     $oModel->setComponentKey($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['fk_component_modal_render_style'])) {
		     $oField = new FkComponentModalRenderStyle();
		     $mValue = $oField->sanitize($aData['fk_component_modal_render_style']);
		     $oModel->setFkComponentModalRenderStyle($mValue);
		}
		if(isset($aData['fk_component_modal_button_location'])) {
		     $oField = new FkComponentModalButtonLocation();
		     $mValue = $oField->sanitize($aData['fk_component_modal_button_location']);
		     $oModel->setFkComponentModalButtonLocation($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
