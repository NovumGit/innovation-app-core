<?php
namespace Crud\Component_modal\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_modal\ICollectionField as Component_modalField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_modal\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_modalField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_modalField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_modalField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_modalField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_modalField
	{
		return current($this->aFields);
	}
}
