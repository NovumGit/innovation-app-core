<?php
namespace Crud\Component_modal\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_modal\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
