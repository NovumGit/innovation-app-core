<?php
namespace Crud\SiteFooterBlock\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\SiteFooterBlock\FieldIterator;
use Crud\SiteFooterBlock\Field\SiteId;
use Crud\SiteFooterBlock\Field\Sorting;
use Crud\SiteFooterBlock\Field\Type;
use Crud\SiteFooterBlock\Field\Width;
use Exception\LogicException;
use Model\Cms\Map\SiteFooterBlockTableMap;
use Model\Cms\SiteFooterBlock;
use Model\Cms\SiteFooterBlockQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify SiteFooterBlock instead if you need to override or add functionality.
 */
abstract class CrudSiteFooterBlockManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return SiteFooterBlockQuery::create();
	}


	public function getTableMap(): SiteFooterBlockTableMap
	{
		return new SiteFooterBlockTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "SiteFooterBlock";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "site_footer_block toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "site_footer_block aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Sorting', 'SiteId', 'Width', 'Type'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Sorting', 'SiteId', 'Width', 'Type'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return SiteFooterBlock
	 */
	public function getModel(array $aData = null): SiteFooterBlock
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oSiteFooterBlockQuery = SiteFooterBlockQuery::create();
		     $oSiteFooterBlock = $oSiteFooterBlockQuery->findOneById($aData['id']);
		     if (!$oSiteFooterBlock instanceof SiteFooterBlock) {
		         throw new LogicException("SiteFooterBlock should be an instance of SiteFooterBlock but got something else." . __METHOD__);
		     }
		     $oSiteFooterBlock = $this->fillVo($aData, $oSiteFooterBlock);
		}
		else {
		     $oSiteFooterBlock = new SiteFooterBlock();
		     if (!empty($aData)) {
		         $oSiteFooterBlock = $this->fillVo($aData, $oSiteFooterBlock);
		     }
		}
		return $oSiteFooterBlock;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return SiteFooterBlock
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): SiteFooterBlock
	{
		$oSiteFooterBlock = $this->getModel($aData);


		 if(!empty($oSiteFooterBlock))
		 {
		     $oSiteFooterBlock = $this->fillVo($aData, $oSiteFooterBlock);
		     $oSiteFooterBlock->save();
		 }
		return $oSiteFooterBlock;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param SiteFooterBlock $oModel
	 * @return SiteFooterBlock
	 */
	protected function fillVo(array $aData, SiteFooterBlock $oModel): SiteFooterBlock
	{
		if(isset($aData['sorting'])) {
		     $oField = new Sorting();
		     $mValue = $oField->sanitize($aData['sorting']);
		     $oModel->setSorting($mValue);
		}
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['width'])) {
		     $oField = new Width();
		     $mValue = $oField->sanitize($aData['width']);
		     $oModel->setWidth($mValue);
		}
		if(isset($aData['type'])) {
		     $oField = new Type();
		     $mValue = $oField->sanitize($aData['type']);
		     $oModel->setType($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
