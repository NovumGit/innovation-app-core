<?php
namespace Crud\SiteFooterBlock\Field;

use Crud\SiteFooterBlock\Field\Base\Width as BaseWidth;

/**
 * Skeleton subclass for representing width field from the site_footer_block table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class Width extends BaseWidth
{
}
