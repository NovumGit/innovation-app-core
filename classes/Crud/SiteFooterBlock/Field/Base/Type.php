<?php
namespace Crud\SiteFooterBlock\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\SiteFooterBlock\ICollectionField;

/**
 * Base class that represents the 'type' crud field from the 'site_footer_block' table.
 * This class is auto generated and should not be modified.
 */
abstract class Type extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'type';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getType';
	protected $sFqModelClassname = '\\\Model\Cms\SiteFooterBlock';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
