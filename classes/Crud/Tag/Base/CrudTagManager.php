<?php
namespace Crud\Tag\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Tag\FieldIterator;
use Crud\Tag\Field\Tag;
use Exception\LogicException;
use Model\Product\Tag\Map\TagTableMap;
use Model\Product\Tag\Tag as TagTag;
use Model\Product\Tag\TagQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Tag instead if you need to override or add functionality.
 */
abstract class CrudTagManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return TagQuery::create();
	}


	public function getTableMap(): TagTableMap
	{
		return new TagTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Tag";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "tag toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "tag aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Tag'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Tag'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Tag
	 */
	public function getModel(array $aData = null): TagTag
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oTagQuery = TagQuery::create();
		     $oTag = $oTagQuery->findOneById($aData['id']);
		     if (!$oTag instanceof Tag) {
		         throw new LogicException("Tag should be an instance of Tag but got something else." . __METHOD__);
		     }
		     $oTag = $this->fillVo($aData, $oTag);
		}
		else {
		     $oTag = new Tag();
		     if (!empty($aData)) {
		         $oTag = $this->fillVo($aData, $oTag);
		     }
		}
		return $oTag;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Tag
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): TagTag
	{
		$oTag = $this->getModel($aData);


		 if(!empty($oTag))
		 {
		     $oTag = $this->fillVo($aData, $oTag);
		     $oTag->save();
		 }
		return $oTag;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Tag $oModel
	 * @return Tag
	 */
	protected function fillVo(array $aData, TagTag $oModel): TagTag
	{
		if(isset($aData['tag'])) {
		     $oField = new Tag();
		     $mValue = $oField->sanitize($aData['tag']);
		     $oModel->setTag($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
