<?php
namespace Crud\Tag\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Tag\ICollectionField as TagField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Tag\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param TagField[] $aFields */
	private array $aFields = [];


	/**
	 * @param TagField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof TagField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(TagField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): TagField
	{
		return current($this->aFields);
	}
}
