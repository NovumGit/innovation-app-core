<?php
namespace Crud\Tag\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Tag\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
