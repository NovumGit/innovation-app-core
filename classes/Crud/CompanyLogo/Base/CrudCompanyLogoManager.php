<?php
namespace Crud\CompanyLogo\Base;

use Core\Utils;
use Crud;
use Crud\CompanyLogo\FieldIterator;
use Crud\CompanyLogo\Field\Ext;
use Crud\CompanyLogo\Field\Mime;
use Crud\CompanyLogo\Field\OriginalName;
use Crud\CompanyLogo\Field\OwnCompanyId;
use Crud\CompanyLogo\Field\Size;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Company\CompanyLogo;
use Model\Company\CompanyLogoQuery;
use Model\Company\Map\CompanyLogoTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CompanyLogo instead if you need to override or add functionality.
 */
abstract class CrudCompanyLogoManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CompanyLogoQuery::create();
	}


	public function getTableMap(): CompanyLogoTableMap
	{
		return new CompanyLogoTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CompanyLogo";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "own_company_logo toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "own_company_logo aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OwnCompanyId', 'OriginalName', 'Ext', 'Size', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OwnCompanyId', 'OriginalName', 'Ext', 'Size', 'Mime'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CompanyLogo
	 */
	public function getModel(array $aData = null): CompanyLogo
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCompanyLogoQuery = CompanyLogoQuery::create();
		     $oCompanyLogo = $oCompanyLogoQuery->findOneById($aData['id']);
		     if (!$oCompanyLogo instanceof CompanyLogo) {
		         throw new LogicException("CompanyLogo should be an instance of CompanyLogo but got something else." . __METHOD__);
		     }
		     $oCompanyLogo = $this->fillVo($aData, $oCompanyLogo);
		}
		else {
		     $oCompanyLogo = new CompanyLogo();
		     if (!empty($aData)) {
		         $oCompanyLogo = $this->fillVo($aData, $oCompanyLogo);
		     }
		}
		return $oCompanyLogo;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CompanyLogo
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CompanyLogo
	{
		$oCompanyLogo = $this->getModel($aData);


		 if(!empty($oCompanyLogo))
		 {
		     $oCompanyLogo = $this->fillVo($aData, $oCompanyLogo);
		     $oCompanyLogo->save();
		 }
		return $oCompanyLogo;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CompanyLogo $oModel
	 * @return CompanyLogo
	 */
	protected function fillVo(array $aData, CompanyLogo $oModel): CompanyLogo
	{
		if(isset($aData['own_company_id'])) {
		     $oField = new OwnCompanyId();
		     $mValue = $oField->sanitize($aData['own_company_id']);
		     $oModel->setOwnCompanyId($mValue);
		}
		if(isset($aData['original_name'])) {
		     $oField = new OriginalName();
		     $mValue = $oField->sanitize($aData['original_name']);
		     $oModel->setOriginalName($mValue);
		}
		if(isset($aData['ext'])) {
		     $oField = new Ext();
		     $mValue = $oField->sanitize($aData['ext']);
		     $oModel->setExt($mValue);
		}
		if(isset($aData['size'])) {
		     $oField = new Size();
		     $mValue = $oField->sanitize($aData['size']);
		     $oModel->setSize($mValue);
		}
		if(isset($aData['mime'])) {
		     $oField = new Mime();
		     $mValue = $oField->sanitize($aData['mime']);
		     $oModel->setMime($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
