<?php
namespace Crud\CompanyLogo\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CompanyLogo\ICollectionField as CompanyLogoField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CompanyLogo\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CompanyLogoField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CompanyLogoField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CompanyLogoField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CompanyLogoField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CompanyLogoField
	{
		return current($this->aFields);
	}
}
