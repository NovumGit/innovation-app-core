<?php
namespace Crud\CompanyLogo\Field;

use Crud\CompanyLogo\Field\Base\Size as BaseSize;

/**
 * Skeleton subclass for representing size field from the own_company_logo table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class Size extends BaseSize
{
}
