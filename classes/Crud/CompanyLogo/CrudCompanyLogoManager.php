<?php
namespace Crud\CompanyLogo;

/**
 * Skeleton subclass for representing a CompanyLogo.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class CrudCompanyLogoManager extends Base\CrudCompanyLogoManager
{
}
