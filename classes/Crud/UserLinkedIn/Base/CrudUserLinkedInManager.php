<?php
namespace Crud\UserLinkedIn\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UserLinkedIn\FieldIterator;
use Crud\UserLinkedIn\Field\CreatedOn;
use Crud\UserLinkedIn\Field\Email;
use Crud\UserLinkedIn\Field\Headline;
use Crud\UserLinkedIn\Field\PictureUrl;
use Crud\UserLinkedIn\Field\PublicProfileUrl;
use Crud\UserLinkedIn\Field\Token;
use Crud\UserLinkedIn\Field\TokenExpires;
use Crud\UserLinkedIn\Field\UserId;
use Exception\LogicException;
use Model\Crm\Map\UserLinkedInTableMap;
use Model\Crm\UserLinkedIn;
use Model\Crm\UserLinkedInQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UserLinkedIn instead if you need to override or add functionality.
 */
abstract class CrudUserLinkedInManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UserLinkedInQuery::create();
	}


	public function getTableMap(): UserLinkedInTableMap
	{
		return new UserLinkedInTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UserLinkedIn";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "user_linkedin toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "user_linkedin aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'UserId', 'Token', 'TokenExpires', 'Email', 'Headline', 'PictureUrl', 'PublicProfileUrl'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'UserId', 'Token', 'TokenExpires', 'Email', 'Headline', 'PictureUrl', 'PublicProfileUrl'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UserLinkedIn
	 */
	public function getModel(array $aData = null): UserLinkedIn
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUserLinkedInQuery = UserLinkedInQuery::create();
		     $oUserLinkedIn = $oUserLinkedInQuery->findOneById($aData['id']);
		     if (!$oUserLinkedIn instanceof UserLinkedIn) {
		         throw new LogicException("UserLinkedIn should be an instance of UserLinkedIn but got something else." . __METHOD__);
		     }
		     $oUserLinkedIn = $this->fillVo($aData, $oUserLinkedIn);
		}
		else {
		     $oUserLinkedIn = new UserLinkedIn();
		     if (!empty($aData)) {
		         $oUserLinkedIn = $this->fillVo($aData, $oUserLinkedIn);
		     }
		}
		return $oUserLinkedIn;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UserLinkedIn
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UserLinkedIn
	{
		$oUserLinkedIn = $this->getModel($aData);


		 if(!empty($oUserLinkedIn))
		 {
		     $oUserLinkedIn = $this->fillVo($aData, $oUserLinkedIn);
		     $oUserLinkedIn->save();
		 }
		return $oUserLinkedIn;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UserLinkedIn $oModel
	 * @return UserLinkedIn
	 */
	protected function fillVo(array $aData, UserLinkedIn $oModel): UserLinkedIn
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['user_id'])) {
		     $oField = new UserId();
		     $mValue = $oField->sanitize($aData['user_id']);
		     $oModel->setUserId($mValue);
		}
		if(isset($aData['token'])) {
		     $oField = new Token();
		     $mValue = $oField->sanitize($aData['token']);
		     $oModel->setToken($mValue);
		}
		if(isset($aData['token_expires'])) {
		     $oField = new TokenExpires();
		     $mValue = $oField->sanitize($aData['token_expires']);
		     $oModel->setTokenExpires($mValue);
		}
		if(isset($aData['email'])) {
		     $oField = new Email();
		     $mValue = $oField->sanitize($aData['email']);
		     $oModel->setEmail($mValue);
		}
		if(isset($aData['headline'])) {
		     $oField = new Headline();
		     $mValue = $oField->sanitize($aData['headline']);
		     $oModel->setHeadline($mValue);
		}
		if(isset($aData['picture_url'])) {
		     $oField = new PictureUrl();
		     $mValue = $oField->sanitize($aData['picture_url']);
		     $oModel->setPictureUrl($mValue);
		}
		if(isset($aData['public_profile_url'])) {
		     $oField = new PublicProfileUrl();
		     $mValue = $oField->sanitize($aData['public_profile_url']);
		     $oModel->setPublicProfileUrl($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
