<?php
namespace Crud\UserLinkedIn\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UserLinkedIn\ICollectionField as UserLinkedInField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UserLinkedIn\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UserLinkedInField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UserLinkedInField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UserLinkedInField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UserLinkedInField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UserLinkedInField
	{
		return current($this->aFields);
	}
}
