<?php
namespace Crud\UserLinkedIn\Field;

use Crud\UserLinkedIn\Field\Base\PictureUrl as BasePictureUrl;

/**
 * Skeleton subclass for representing picture_url field from the user_linkedin table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:28:10
 */
final class PictureUrl extends BasePictureUrl
{
}
