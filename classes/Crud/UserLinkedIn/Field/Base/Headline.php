<?php
namespace Crud\UserLinkedIn\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UserLinkedIn\ICollectionField;

/**
 * Base class that represents the 'headline' crud field from the 'user_linkedin' table.
 * This class is auto generated and should not be modified.
 */
abstract class Headline extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'headline';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHeadline';
	protected $sFqModelClassname = '\\\Model\Crm\UserLinkedIn';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
