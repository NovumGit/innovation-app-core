<?php
namespace Crud\UserLinkedIn\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UserLinkedIn\ICollectionField;

/**
 * Base class that represents the 'picture_url' crud field from the 'user_linkedin' table.
 * This class is auto generated and should not be modified.
 */
abstract class PictureUrl extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'picture_url';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getPictureUrl';
	protected $sFqModelClassname = '\\\Model\Crm\UserLinkedIn';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
