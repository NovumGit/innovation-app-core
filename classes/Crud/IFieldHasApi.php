<?php
namespace Crud;

/***
 * Upon generation of the CrudManager and CrudField classes a CrudApiTrait is made.
 * By using this trait you will confirm to the contract that this interface defines.
 * During code generation this interface allows the generator to magically generate additional classes that interact
 * with the api.
 *
 * Interface IFieldHasApi
 * @package Crud
 */

interface IFieldHasApi extends IField
{
    // These come from implementing a CrudApiTrait
    public function getDocumentationUrl(): string;
    public function getApiUrl(): string;
    public function getApiNamespace(): string;
    public function getApiVersion(): string;

    // These are usually generated during code generation.
    public function getModule(): string;
    public function getModuleDir(): string;
}
