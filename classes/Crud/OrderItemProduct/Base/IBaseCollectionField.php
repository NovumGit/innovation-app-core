<?php
namespace Crud\OrderItemProduct\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\OrderItemProduct\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
