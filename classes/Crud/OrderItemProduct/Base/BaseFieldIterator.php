<?php
namespace Crud\OrderItemProduct\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\OrderItemProduct\ICollectionField as OrderItemProductField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\OrderItemProduct\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param OrderItemProductField[] $aFields */
	private array $aFields = [];


	/**
	 * @param OrderItemProductField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof OrderItemProductField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(OrderItemProductField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): OrderItemProductField
	{
		return current($this->aFields);
	}
}
