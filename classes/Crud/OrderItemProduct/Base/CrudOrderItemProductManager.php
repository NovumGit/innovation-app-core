<?php
namespace Crud\OrderItemProduct\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\OrderItemProduct\FieldIterator;
use Crud\OrderItemProduct\Field\CategoryName;
use Crud\OrderItemProduct\Field\Height;
use Crud\OrderItemProduct\Field\MaterialName;
use Crud\OrderItemProduct\Field\ProductId;
use Crud\OrderItemProduct\Field\SaleOrderItemId;
use Crud\OrderItemProduct\Field\SalePrice;
use Crud\OrderItemProduct\Field\Thickness;
use Crud\OrderItemProduct\Field\Weight;
use Crud\OrderItemProduct\Field\Width;
use Exception\LogicException;
use Model\Sale\Map\OrderItemProductTableMap;
use Model\Sale\OrderItemProduct;
use Model\Sale\OrderItemProductQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify OrderItemProduct instead if you need to override or add functionality.
 */
abstract class CrudOrderItemProductManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return OrderItemProductQuery::create();
	}


	public function getTableMap(): OrderItemProductTableMap
	{
		return new OrderItemProductTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "OrderItemProduct";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "sale_order_item_product toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "sale_order_item_product aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderItemId', 'ProductId', 'SalePrice', 'Width', 'Weight', 'Height', 'Thickness', 'CategoryName', 'MaterialName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SaleOrderItemId', 'ProductId', 'SalePrice', 'Width', 'Weight', 'Height', 'Thickness', 'CategoryName', 'MaterialName'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return OrderItemProduct
	 */
	public function getModel(array $aData = null): OrderItemProduct
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oOrderItemProductQuery = OrderItemProductQuery::create();
		     $oOrderItemProduct = $oOrderItemProductQuery->findOneById($aData['id']);
		     if (!$oOrderItemProduct instanceof OrderItemProduct) {
		         throw new LogicException("OrderItemProduct should be an instance of OrderItemProduct but got something else." . __METHOD__);
		     }
		     $oOrderItemProduct = $this->fillVo($aData, $oOrderItemProduct);
		}
		else {
		     $oOrderItemProduct = new OrderItemProduct();
		     if (!empty($aData)) {
		         $oOrderItemProduct = $this->fillVo($aData, $oOrderItemProduct);
		     }
		}
		return $oOrderItemProduct;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return OrderItemProduct
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): OrderItemProduct
	{
		$oOrderItemProduct = $this->getModel($aData);


		 if(!empty($oOrderItemProduct))
		 {
		     $oOrderItemProduct = $this->fillVo($aData, $oOrderItemProduct);
		     $oOrderItemProduct->save();
		 }
		return $oOrderItemProduct;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param OrderItemProduct $oModel
	 * @return OrderItemProduct
	 */
	protected function fillVo(array $aData, OrderItemProduct $oModel): OrderItemProduct
	{
		if(isset($aData['sale_order_item_id'])) {
		     $oField = new SaleOrderItemId();
		     $mValue = $oField->sanitize($aData['sale_order_item_id']);
		     $oModel->setSaleOrderItemId($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['sale_price'])) {
		     $oField = new SalePrice();
		     $mValue = $oField->sanitize($aData['sale_price']);
		     $oModel->setSalePrice($mValue);
		}
		if(isset($aData['width'])) {
		     $oField = new Width();
		     $mValue = $oField->sanitize($aData['width']);
		     $oModel->setWidth($mValue);
		}
		if(isset($aData['weight'])) {
		     $oField = new Weight();
		     $mValue = $oField->sanitize($aData['weight']);
		     $oModel->setWeight($mValue);
		}
		if(isset($aData['height'])) {
		     $oField = new Height();
		     $mValue = $oField->sanitize($aData['height']);
		     $oModel->setHeight($mValue);
		}
		if(isset($aData['thickness'])) {
		     $oField = new Thickness();
		     $mValue = $oField->sanitize($aData['thickness']);
		     $oModel->setThickness($mValue);
		}
		if(isset($aData['category_name'])) {
		     $oField = new CategoryName();
		     $mValue = $oField->sanitize($aData['category_name']);
		     $oModel->setCategoryName($mValue);
		}
		if(isset($aData['material_name'])) {
		     $oField = new MaterialName();
		     $mValue = $oField->sanitize($aData['material_name']);
		     $oModel->setMaterialName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
