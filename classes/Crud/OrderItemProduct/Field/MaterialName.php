<?php
namespace Crud\OrderItemProduct\Field;

use Crud\OrderItemProduct\Field\Base\MaterialName as BaseMaterialName;

/**
 * Skeleton subclass for representing material_name field from the sale_order_item_product table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class MaterialName extends BaseMaterialName
{
}
