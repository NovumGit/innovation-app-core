<?php
namespace Crud\OrderItemProduct\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\OrderItemProduct\ICollectionField;

/**
 * Base class that represents the 'height' crud field from the 'sale_order_item_product' table.
 * This class is auto generated and should not be modified.
 */
abstract class Height extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'height';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getHeight';
	protected $sFqModelClassname = '\\\Model\Sale\OrderItemProduct';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
