<?php
namespace Crud\OrderItemProduct\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;
use Crud\OrderItemProduct\ICollectionField;

/**
 * Base class that represents the 'sale_order_item_id' crud field from the 'sale_order_item_product' table.
 * This class is auto generated and should not be modified.
 */
abstract class SaleOrderItemId extends GenericString implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'sale_order_item_id';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSaleOrderItemId';
	protected $sFqModelClassname = '\\\Model\Sale\OrderItemProduct';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['sale_order_item_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
