<?php
namespace Crud\OrderItemProduct\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\OrderItemProduct\ICollectionField;

/**
 * Base class that represents the 'sale_price' crud field from the 'sale_order_item_product' table.
 * This class is auto generated and should not be modified.
 */
abstract class SalePrice extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'sale_price';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getSalePrice';
	protected $sFqModelClassname = '\\\Model\Sale\OrderItemProduct';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
