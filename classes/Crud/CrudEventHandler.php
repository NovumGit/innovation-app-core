<?php
namespace Crud;

use Core\Event\Type\Collection;
use Model\System\Event\EventTypeQuery;

class CrudEventHandler
{
    private $oFormManager;
    function __construct(IFormManager $oFormManager)
    {
        $this->oFormManager = $oFormManager;
    }
    function getFormManager():IFormManager
    {
        return $this->oFormManager;
    }

    function getEventTypes():Collection
    {
        $oObjectCollection = EventTypeQuery::create()->find();
        $aEventsTypes = [];
        if(!$oObjectCollection->isEmpty())
        {
            foreach($oObjectCollection as $oEventType)
            {
                $aEventsTypes[] = $oEventType;
            }
        }
        return new Collection($aEventsTypes);
    }

    function getCallbacks()
    {

    }
}
