<?php
namespace Crud\ContactMessageType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ContactMessageType\ICollectionField as ContactMessageTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\ContactMessageType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param ContactMessageTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param ContactMessageTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof ContactMessageTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(ContactMessageTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): ContactMessageTypeField
	{
		return current($this->aFields);
	}
}
