<?php
namespace Crud\ContactMessageType\Base;

use Core\Utils;
use Crud;
use Crud\ContactMessageType\FieldIterator;
use Crud\ContactMessageType\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\ContactMessageType;
use Model\ContactMessageTypeQuery;
use Model\Map\ContactMessageTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify ContactMessageType instead if you need to override or add functionality.
 */
abstract class CrudContactMessageTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return ContactMessageTypeQuery::create();
	}


	public function getTableMap(): ContactMessageTypeTableMap
	{
		return new ContactMessageTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "ContactMessageType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "contact_message_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "contact_message_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return ContactMessageType
	 */
	public function getModel(array $aData = null): ContactMessageType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oContactMessageTypeQuery = ContactMessageTypeQuery::create();
		     $oContactMessageType = $oContactMessageTypeQuery->findOneById($aData['id']);
		     if (!$oContactMessageType instanceof ContactMessageType) {
		         throw new LogicException("ContactMessageType should be an instance of ContactMessageType but got something else." . __METHOD__);
		     }
		     $oContactMessageType = $this->fillVo($aData, $oContactMessageType);
		}
		else {
		     $oContactMessageType = new ContactMessageType();
		     if (!empty($aData)) {
		         $oContactMessageType = $this->fillVo($aData, $oContactMessageType);
		     }
		}
		return $oContactMessageType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return ContactMessageType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): ContactMessageType
	{
		$oContactMessageType = $this->getModel($aData);


		 if(!empty($oContactMessageType))
		 {
		     $oContactMessageType = $this->fillVo($aData, $oContactMessageType);
		     $oContactMessageType->save();
		 }
		return $oContactMessageType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param ContactMessageType $oModel
	 * @return ContactMessageType
	 */
	protected function fillVo(array $aData, ContactMessageType $oModel): ContactMessageType
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
