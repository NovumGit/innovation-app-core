<?php
namespace Crud\ContactMessageType\Field;

use Crud\ContactMessageType\Field\Base\Name as BaseName;

/**
 * Skeleton subclass for representing name field from the contact_message_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:28:10
 */
final class Name extends BaseName
{
}
