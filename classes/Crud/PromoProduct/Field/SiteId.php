<?php
namespace Crud\PromoProduct\Field;

use Crud\PromoProduct\Field\Base\SiteId as BaseSiteId;

/**
 * Skeleton subclass for representing site_id field from the promo_product table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class SiteId extends BaseSiteId
{
}
