<?php
namespace Crud\PromoProduct\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\PromoProduct\ICollectionField as PromoProductField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\PromoProduct\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PromoProductField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PromoProductField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PromoProductField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PromoProductField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PromoProductField
	{
		return current($this->aFields);
	}
}
