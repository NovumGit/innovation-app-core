<?php
namespace Crud\PromoProduct\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\PromoProduct\FieldIterator;
use Crud\PromoProduct\Field\About;
use Crud\PromoProduct\Field\ProductId;
use Crud\PromoProduct\Field\SiteId;
use Exception\LogicException;
use Model\Map\PromoProductTableMap;
use Model\PromoProduct;
use Model\PromoProductQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify PromoProduct instead if you need to override or add functionality.
 */
abstract class CrudPromoProductManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PromoProductQuery::create();
	}


	public function getTableMap(): PromoProductTableMap
	{
		return new PromoProductTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "PromoProduct";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "promo_product toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "promo_product aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'ProductId', 'About'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['SiteId', 'ProductId', 'About'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return PromoProduct
	 */
	public function getModel(array $aData = null): PromoProduct
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPromoProductQuery = PromoProductQuery::create();
		     $oPromoProduct = $oPromoProductQuery->findOneById($aData['id']);
		     if (!$oPromoProduct instanceof PromoProduct) {
		         throw new LogicException("PromoProduct should be an instance of PromoProduct but got something else." . __METHOD__);
		     }
		     $oPromoProduct = $this->fillVo($aData, $oPromoProduct);
		}
		else {
		     $oPromoProduct = new PromoProduct();
		     if (!empty($aData)) {
		         $oPromoProduct = $this->fillVo($aData, $oPromoProduct);
		     }
		}
		return $oPromoProduct;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return PromoProduct
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): PromoProduct
	{
		$oPromoProduct = $this->getModel($aData);


		 if(!empty($oPromoProduct))
		 {
		     $oPromoProduct = $this->fillVo($aData, $oPromoProduct);
		     $oPromoProduct->save();
		 }
		return $oPromoProduct;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param PromoProduct $oModel
	 * @return PromoProduct
	 */
	protected function fillVo(array $aData, PromoProduct $oModel): PromoProduct
	{
		if(isset($aData['site_id'])) {
		     $oField = new SiteId();
		     $mValue = $oField->sanitize($aData['site_id']);
		     $oModel->setSiteId($mValue);
		}
		if(isset($aData['product_id'])) {
		     $oField = new ProductId();
		     $mValue = $oField->sanitize($aData['product_id']);
		     $oModel->setProductId($mValue);
		}
		if(isset($aData['about'])) {
		     $oField = new About();
		     $mValue = $oField->sanitize($aData['about']);
		     $oModel->setAbout($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
