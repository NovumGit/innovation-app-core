<?php
namespace Crud\Component_button\Field\Base;

use Core\Utils;
use Crud\Component_button\ICollectionField;
use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\IRequiredField;
use Model\System\UI\UIComponentQuery;

/**
 * Base class that represents the 'ui_component_id' crud field from the 'component_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class UiComponentId extends GenericLookup implements IFilterableField, IEditableField, ICollectionField, IFilterableLookupField, IRequiredField
{
	protected $sFieldName = 'ui_component_id';
	protected $sFieldLabel = 'UiComponent';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getUiComponentId';
	protected $sFqModelClassname = '\Model\System\LowCode\Button\Component_button';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getLookups($mSelectedItem = null)
	{
		$aAllRows = \Model\System\UI\UIComponentQuery::create()->orderBylabel()->find();
		$aOptions = \Core\Utils::makeSelectOptions($aAllRows, "getlabel", $mSelectedItem, "getId");
		$aOptions = $this->filterLookups($aOptions);
		return $aOptions;
	}


	public function getVisibleValue($iItemId = null)
	{
		if($iItemId){
		    return \Model\System\UI\UIComponentQuery::create()->findOneById($iItemId)->getlabel();
		}
		return null;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['ui_component_id']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "UiComponent" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
