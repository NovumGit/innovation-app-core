<?php
namespace Crud\Component_button\Field\Base;

use Crud\Component_button\ICollectionField;
use Crud\Generic\Field\GenericIcon;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'icon' crud field from the 'component_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class Icon extends GenericIcon implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'icon';
	protected $sFieldLabel = 'Icon';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getIcon';
	protected $sFqModelClassname = '\Model\System\LowCode\Button\Component_button';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
