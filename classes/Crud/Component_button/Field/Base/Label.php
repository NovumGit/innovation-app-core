<?php
namespace Crud\Component_button\Field\Base;

use Crud\Component_button\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'label' crud field from the 'component_button' table.
 * This class is auto generated and should not be modified.
 */
abstract class Label extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'label';
	protected $sFieldLabel = 'Label';
	protected $sIcon = 'tag';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getLabel';
	protected $sFqModelClassname = '\Model\System\LowCode\Button\Component_button';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
