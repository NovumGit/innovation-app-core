<?php
namespace Crud\Component_button\Field;

use Crud\Component_button\Field\Base\FkComponentButtonAlign as BaseFkComponentButtonAlign;

/**
 * Skeleton subclass for representing fk_component_button_align field from the component_button table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class FkComponentButtonAlign extends BaseFkComponentButtonAlign
{
}
