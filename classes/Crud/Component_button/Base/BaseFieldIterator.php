<?php
namespace Crud\Component_button\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_button\ICollectionField as Component_buttonField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_button\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_buttonField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_buttonField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_buttonField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_buttonField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_buttonField
	{
		return current($this->aFields);
	}
}
