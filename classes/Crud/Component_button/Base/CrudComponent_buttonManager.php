<?php
namespace Crud\Component_button\Base;

use Core\Utils;
use Crud;
use Crud\Component_button\FieldIterator;
use Crud\Component_button\Field\FkComponentButtonAlign;
use Crud\Component_button\Field\FkComponentButtonColorType;
use Crud\Component_button\Field\FkComponentButtonEvent;
use Crud\Component_button\Field\FkComponentButtonPage;
use Crud\Component_button\Field\FkComponentButtonSize;
use Crud\Component_button\Field\Flow;
use Crud\Component_button\Field\Icon;
use Crud\Component_button\Field\Info;
use Crud\Component_button\Field\Label;
use Crud\Component_button\Field\Title;
use Crud\Component_button\Field\UiComponentId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Button\Component_button;
use Model\System\LowCode\Button\Component_buttonQuery;
use Model\System\LowCode\Button\Map\Component_buttonTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_button instead if you need to override or add functionality.
 */
abstract class CrudComponent_buttonManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_buttonQuery::create();
	}


	public function getTableMap(): Component_buttonTableMap
	{
		return new Component_buttonTableMap();
	}


	public function getShortDescription(): string
	{
		return "Bevat configuratiegegevens van Button component, gebruikt bij het weergeven van apps.";
	}


	public function getEntityTitle(): string
	{
		return "Component_button";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_button toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_button aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Info', 'Title', 'Label', 'FkComponentButtonSize', 'FkComponentButtonAlign', 'FkComponentButtonColorType', 'Icon', 'FkComponentButtonEvent', 'FkComponentButtonPage', 'Flow', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Info', 'Title', 'Label', 'FkComponentButtonSize', 'FkComponentButtonAlign', 'FkComponentButtonColorType', 'Icon', 'FkComponentButtonEvent', 'FkComponentButtonPage', 'Flow', 'UiComponentId'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_button
	 */
	public function getModel(array $aData = null): Component_button
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_buttonQuery = Component_buttonQuery::create();
		     $oComponent_button = $oComponent_buttonQuery->findOneById($aData['id']);
		     if (!$oComponent_button instanceof Component_button) {
		         throw new LogicException("Component_button should be an instance of Component_button but got something else." . __METHOD__);
		     }
		     $oComponent_button = $this->fillVo($aData, $oComponent_button);
		}
		else {
		     $oComponent_button = new Component_button();
		     if (!empty($aData)) {
		         $oComponent_button = $this->fillVo($aData, $oComponent_button);
		     }
		}
		return $oComponent_button;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_button
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_button
	{
		$oComponent_button = $this->getModel($aData);


		 if(!empty($oComponent_button))
		 {
		     $oComponent_button = $this->fillVo($aData, $oComponent_button);
		     $oComponent_button->save();
		 }
		return $oComponent_button;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_button $oModel
	 * @return Component_button
	 */
	protected function fillVo(array $aData, Component_button $oModel): Component_button
	{
		if(isset($aData['info'])) {
		     $oField = new Info();
		     $mValue = $oField->sanitize($aData['info']);
		     $oModel->setInfo($mValue);
		}
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['label'])) {
		     $oField = new Label();
		     $mValue = $oField->sanitize($aData['label']);
		     $oModel->setLabel($mValue);
		}
		if(isset($aData['fk_component_button_size'])) {
		     $oField = new FkComponentButtonSize();
		     $mValue = $oField->sanitize($aData['fk_component_button_size']);
		     $oModel->setFkComponentButtonSize($mValue);
		}
		if(isset($aData['fk_component_button_align'])) {
		     $oField = new FkComponentButtonAlign();
		     $mValue = $oField->sanitize($aData['fk_component_button_align']);
		     $oModel->setFkComponentButtonAlign($mValue);
		}
		if(isset($aData['fk_component_button_color_type'])) {
		     $oField = new FkComponentButtonColorType();
		     $mValue = $oField->sanitize($aData['fk_component_button_color_type']);
		     $oModel->setFkComponentButtonColorType($mValue);
		}
		if(isset($aData['icon'])) {
		     $oField = new Icon();
		     $mValue = $oField->sanitize($aData['icon']);
		     $oModel->setIcon($mValue);
		}
		if(isset($aData['fk_component_button_event'])) {
		     $oField = new FkComponentButtonEvent();
		     $mValue = $oField->sanitize($aData['fk_component_button_event']);
		     $oModel->setFkComponentButtonEvent($mValue);
		}
		if(isset($aData['fk_component_button_page'])) {
		     $oField = new FkComponentButtonPage();
		     $mValue = $oField->sanitize($aData['fk_component_button_page']);
		     $oModel->setFkComponentButtonPage($mValue);
		}
		if(isset($aData['flow'])) {
		     $oField = new Flow();
		     $mValue = $oField->sanitize($aData['flow']);
		     $oModel->setFlow($mValue);
		}
		if(isset($aData['ui_component_id'])) {
		     $oField = new UiComponentId();
		     $mValue = $oField->sanitize($aData['ui_component_id']);
		     $oModel->setUiComponentId($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
