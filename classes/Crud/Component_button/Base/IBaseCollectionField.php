<?php
namespace Crud\Component_button\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_button\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
