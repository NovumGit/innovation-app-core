<?php
namespace Crud\Site_FAQ_category_translation;

use Core\StatusMessage;
use Crud\FormManager;
use Crud\IEditableCrud;
use Exception\LogicException;
use Model\Cms\SiteFAQCategoryTranslation;
use Model\Cms\SiteFAQCategoryTranslationQuery;

class CrudSite_FAQ_category_translationManager extends FormManager implements IEditableCrud
{
    function getCreateNewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/setting/faq_category/edit?site='.$_GET['site'];
        }
        throw new LogicException();
    }
    function getEntityTitle():string
    {
        return 'site_faq_category_translation';
    }
    function getNewFormTitle():string
    {
        return 'FAQ categorie vertaling toevoegen';
    }
    function getEditFormTitle():string
    {
        return 'FAQ categorie vertaling bewerken';
    }
    function getOverviewUrl():string
    {
        if(isset($_GET['site']))
        {
            return '/cms/faq/category/overview?site='.$_GET['site'];
        }
        return '/cms/faq/category/overview?site='.$this->getArgument('site');
    }
    function getDefaultOverviewFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name',
            'Edit',
            'Delete'
        ];
    }
    function getDefaultEditFields(bool $bAddNamespaceUnimplemented = false):?array
    {
        return [
            'Name'
        ];
    }
    function getModel(array $aData = null)
    {
        $oSiteFAQCategoryTranslation = null;
        if(isset($aData['id']) && $aData['id'])
        {
            $oQuery = new SiteFAQCategoryTranslationQuery();
            $oSiteFAQCategoryTranslation = $oQuery->findOneById($aData['id']);
            if(!$oSiteFAQCategoryTranslation instanceof SiteFAQCategoryTranslation)
            {
                throw new LogicException("SiteFAQCategory should be an instance of Site FAQ category translation but got ".get_class($oSiteFAQCategoryTranslation)." in ".__METHOD__);
            }
        }
        if($oSiteFAQCategoryTranslation instanceof SiteFAQCategoryTranslation && isset($aData['language_id']) && isset($aData['category_id']))
        {
            $oSiteFAQCategoryTranslationQuery = SiteFAQCategoryTranslationQuery::create();
            $oSiteFAQCategoryTranslationQuery->filterByLanguageId($aData['language_id']);
            $oSiteFAQCategoryTranslationQuery->filterBySiteFaqCategoryId($aData['category_id']);

            $oSiteFAQCategoryTranslation = $oSiteFAQCategoryTranslationQuery->findOne();
        }
        if(!$oSiteFAQCategoryTranslation instanceof SiteFAQCategoryTranslation)
        {
            $oSiteFAQCategoryTranslation = new SiteFAQCategoryTranslation();
            $oSiteFAQCategoryTranslation = $this->fillVo($oSiteFAQCategoryTranslation, $aData);
        }
        return $oSiteFAQCategoryTranslation;
    }
    function store(array $aData = null)
    {
        $oSiteFAQCategoryTranslation = $this->getModel($aData);
        if(!empty($oSiteFAQCategoryTranslation))
        {
            $oSiteFAQCategoryTranslation = $this->fillVo($oSiteFAQCategoryTranslation, $aData);
            $oSiteFAQCategoryTranslation->save();
        }
        return $oSiteFAQCategoryTranslation;
    }
    function delete($iSiteFAQCategoryTranslationId)
    {
        $oSiteFAQCategoryTranslation = SiteFAQCategoryTranslationQuery::create()->findOneById($iSiteFAQCategoryTranslationId);
        $oSiteFAQCategoryTranslation->delete();
        StatusMessage::success("FAQ categorie vertaling verwijderd.");
    }

    function fillVo(SiteFAQCategoryTranslation $oSiteFAQCategoryTranslation, $aData)
    {

        if(isset($aData['language_id']))
        {
            $oSiteFAQCategoryTranslation->setLanguageId($aData['language_id']);
        }
        if(isset($aData['category_id']))
        {
            $oSiteFAQCategoryTranslation->setSiteFaqCategoryId($aData['category_id']);
        }
        if(isset($aData['name']))
        {
            $oSiteFAQCategoryTranslation->setName($aData['name']);
        }
        return $oSiteFAQCategoryTranslation;
    }
}
