<?php
namespace Crud\CustomMediaNodeType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CustomMediaNodeType\ICollectionField as CustomMediaNodeTypeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CustomMediaNodeType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CustomMediaNodeTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CustomMediaNodeTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CustomMediaNodeTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CustomMediaNodeTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CustomMediaNodeTypeField
	{
		return current($this->aFields);
	}
}
