<?php
namespace Crud\CustomMediaNodeType\Base;

use Core\Utils;
use Crud;
use Crud\CustomMediaNodeType\FieldIterator;
use Crud\CustomMediaNodeType\Field\CreatedDate;
use Crud\CustomMediaNodeType\Field\Type;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Cms\CustomMediaNodeType;
use Model\Cms\CustomMediaNodeTypeQuery;
use Model\Cms\Map\CustomMediaNodeTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CustomMediaNodeType instead if you need to override or add functionality.
 */
abstract class CrudCustomMediaNodeTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CustomMediaNodeTypeQuery::create();
	}


	public function getTableMap(): CustomMediaNodeTypeTableMap
	{
		return new CustomMediaNodeTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CustomMediaNodeType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "custom_media_node_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "custom_media_node_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Type', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Type', 'CreatedDate'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CustomMediaNodeType
	 */
	public function getModel(array $aData = null): CustomMediaNodeType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCustomMediaNodeTypeQuery = CustomMediaNodeTypeQuery::create();
		     $oCustomMediaNodeType = $oCustomMediaNodeTypeQuery->findOneById($aData['id']);
		     if (!$oCustomMediaNodeType instanceof CustomMediaNodeType) {
		         throw new LogicException("CustomMediaNodeType should be an instance of CustomMediaNodeType but got something else." . __METHOD__);
		     }
		     $oCustomMediaNodeType = $this->fillVo($aData, $oCustomMediaNodeType);
		}
		else {
		     $oCustomMediaNodeType = new CustomMediaNodeType();
		     if (!empty($aData)) {
		         $oCustomMediaNodeType = $this->fillVo($aData, $oCustomMediaNodeType);
		     }
		}
		return $oCustomMediaNodeType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CustomMediaNodeType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CustomMediaNodeType
	{
		$oCustomMediaNodeType = $this->getModel($aData);


		 if(!empty($oCustomMediaNodeType))
		 {
		     $oCustomMediaNodeType = $this->fillVo($aData, $oCustomMediaNodeType);
		     $oCustomMediaNodeType->save();
		 }
		return $oCustomMediaNodeType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CustomMediaNodeType $oModel
	 * @return CustomMediaNodeType
	 */
	protected function fillVo(array $aData, CustomMediaNodeType $oModel): CustomMediaNodeType
	{
		if(isset($aData['type'])) {
		     $oField = new Type();
		     $mValue = $oField->sanitize($aData['type']);
		     $oModel->setType($mValue);
		}
		if(isset($aData['created_date'])) {
		     $oField = new CreatedDate();
		     $mValue = $oField->sanitize($aData['created_date']);
		     $oModel->setCreatedDate($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
