<?php
namespace Crud\CustomMediaNodeType\Field\Base;

use Crud\CustomMediaNodeType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'created_date' crud field from the 'custom_media_node_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class CreatedDate extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'created_date';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCreatedDate';
	protected $sFqModelClassname = '\\\Model\Cms\CustomMediaNodeType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
