<?php
namespace Crud\CustomMediaNodeType\Field;

use Crud\CustomMediaNodeType\Field\Base\CreatedDate as BaseCreatedDate;

/**
 * Skeleton subclass for representing created_date field from the custom_media_node_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:01
 */
final class CreatedDate extends BaseCreatedDate
{
}
