<?php
namespace Crud\CompanySetting\Base;

use Core\Utils;
use Crud;
use Crud\CompanySetting\FieldIterator;
use Crud\CompanySetting\Field\InvoiceNumber;
use Crud\CompanySetting\Field\InvoicePrefix;
use Crud\CompanySetting\Field\OrderNumber;
use Crud\CompanySetting\Field\OrderPrefix;
use Crud\CompanySetting\Field\OwnCompanyId;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Company\CompanySetting;
use Model\Company\CompanySettingQuery;
use Model\Company\Map\CompanySettingTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CompanySetting instead if you need to override or add functionality.
 */
abstract class CrudCompanySettingManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CompanySettingQuery::create();
	}


	public function getTableMap(): CompanySettingTableMap
	{
		return new CompanySettingTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CompanySetting";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "own_company_setting toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "own_company_setting aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OwnCompanyId', 'OrderNumber', 'OrderPrefix', 'InvoiceNumber', 'InvoicePrefix'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['OwnCompanyId', 'OrderNumber', 'OrderPrefix', 'InvoiceNumber', 'InvoicePrefix'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CompanySetting
	 */
	public function getModel(array $aData = null): CompanySetting
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCompanySettingQuery = CompanySettingQuery::create();
		     $oCompanySetting = $oCompanySettingQuery->findOneById($aData['id']);
		     if (!$oCompanySetting instanceof CompanySetting) {
		         throw new LogicException("CompanySetting should be an instance of CompanySetting but got something else." . __METHOD__);
		     }
		     $oCompanySetting = $this->fillVo($aData, $oCompanySetting);
		}
		else {
		     $oCompanySetting = new CompanySetting();
		     if (!empty($aData)) {
		         $oCompanySetting = $this->fillVo($aData, $oCompanySetting);
		     }
		}
		return $oCompanySetting;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CompanySetting
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): CompanySetting
	{
		$oCompanySetting = $this->getModel($aData);


		 if(!empty($oCompanySetting))
		 {
		     $oCompanySetting = $this->fillVo($aData, $oCompanySetting);
		     $oCompanySetting->save();
		 }
		return $oCompanySetting;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CompanySetting $oModel
	 * @return CompanySetting
	 */
	protected function fillVo(array $aData, CompanySetting $oModel): CompanySetting
	{
		if(isset($aData['own_company_id'])) {
		     $oField = new OwnCompanyId();
		     $mValue = $oField->sanitize($aData['own_company_id']);
		     $oModel->setOwnCompanyId($mValue);
		}
		if(isset($aData['order_number'])) {
		     $oField = new OrderNumber();
		     $mValue = $oField->sanitize($aData['order_number']);
		     $oModel->setOrderNumber($mValue);
		}
		if(isset($aData['order_prefix'])) {
		     $oField = new OrderPrefix();
		     $mValue = $oField->sanitize($aData['order_prefix']);
		     $oModel->setOrderPrefix($mValue);
		}
		if(isset($aData['invoice_number'])) {
		     $oField = new InvoiceNumber();
		     $mValue = $oField->sanitize($aData['invoice_number']);
		     $oModel->setInvoiceNumber($mValue);
		}
		if(isset($aData['invoice_prefix'])) {
		     $oField = new InvoicePrefix();
		     $mValue = $oField->sanitize($aData['invoice_prefix']);
		     $oModel->setInvoicePrefix($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
