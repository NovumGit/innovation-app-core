<?php
namespace Crud\CompanySetting\Base;

use Crud\BaseCrudFieldIterator;
use Crud\CompanySetting\ICollectionField as CompanySettingField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\CompanySetting\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param CompanySettingField[] $aFields */
	private array $aFields = [];


	/**
	 * @param CompanySettingField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof CompanySettingField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(CompanySettingField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): CompanySettingField
	{
		return current($this->aFields);
	}
}
