<?php
namespace Crud\CompanySetting\Field\Base;

use Crud\CompanySetting\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'invoice_prefix' crud field from the 'own_company_setting' table.
 * This class is auto generated and should not be modified.
 */
abstract class InvoicePrefix extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'invoice_prefix';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getInvoicePrefix';
	protected $sFqModelClassname = '\\\Model\Company\CompanySetting';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
