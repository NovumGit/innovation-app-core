<?php
namespace Crud\CompanySetting\Field\Base;

use Crud\CompanySetting\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'order_number' crud field from the 'own_company_setting' table.
 * This class is auto generated and should not be modified.
 */
abstract class OrderNumber extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'order_number';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getOrderNumber';
	protected $sFqModelClassname = '\\\Model\Company\CompanySetting';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
