<?php
namespace Crud\CompanySetting\Field;

use Crud\CompanySetting\Field\Base\InvoiceNumber as BaseInvoiceNumber;

/**
 * Skeleton subclass for representing invoice_number field from the own_company_setting table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class InvoiceNumber extends BaseInvoiceNumber
{
}
