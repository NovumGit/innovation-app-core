<?php
namespace Crud\CompanySetting\Field;

use Crud\CompanySetting\Field\Base\OrderNumber as BaseOrderNumber;

/**
 * Skeleton subclass for representing order_number field from the own_company_setting table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class OrderNumber extends BaseOrderNumber
{
}
