<?php
namespace Crud\UserRegistry\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UserRegistry\ICollectionField;

/**
 * Base class that represents the 'item_value' crud field from the 'user_registry' table.
 * This class is auto generated and should not be modified.
 */
abstract class ItemValue extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'item_value';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getItemValue';
	protected $sFqModelClassname = '\\\Model\User\UserRegistry';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
