<?php
namespace Crud\UserRegistry\Field;

use Crud\UserRegistry\Field\Base\ItemValue as BaseItemValue;

/**
 * Skeleton subclass for representing item_value field from the user_registry table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class ItemValue extends BaseItemValue
{
}
