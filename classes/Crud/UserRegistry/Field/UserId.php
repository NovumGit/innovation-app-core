<?php
namespace Crud\UserRegistry\Field;

use Crud\UserRegistry\Field\Base\UserId as BaseUserId;

/**
 * Skeleton subclass for representing user_id field from the user_registry table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:04
 */
final class UserId extends BaseUserId
{
}
