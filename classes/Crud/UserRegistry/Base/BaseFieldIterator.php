<?php
namespace Crud\UserRegistry\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UserRegistry\ICollectionField as UserRegistryField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UserRegistry\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UserRegistryField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UserRegistryField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UserRegistryField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UserRegistryField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UserRegistryField
	{
		return current($this->aFields);
	}
}
