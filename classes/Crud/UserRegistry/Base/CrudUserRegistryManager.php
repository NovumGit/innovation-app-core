<?php
namespace Crud\UserRegistry\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UserRegistry\FieldIterator;
use Crud\UserRegistry\Field\ItemKey;
use Crud\UserRegistry\Field\ItemValue;
use Crud\UserRegistry\Field\UserId;
use Exception\LogicException;
use Model\User\Map\UserRegistryTableMap;
use Model\User\UserRegistry;
use Model\User\UserRegistryQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UserRegistry instead if you need to override or add functionality.
 */
abstract class CrudUserRegistryManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UserRegistryQuery::create();
	}


	public function getTableMap(): UserRegistryTableMap
	{
		return new UserRegistryTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "UserRegistry";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "user_registry toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "user_registry aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'ItemKey', 'ItemValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['UserId', 'ItemKey', 'ItemValue'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UserRegistry
	 */
	public function getModel(array $aData = null): UserRegistry
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUserRegistryQuery = UserRegistryQuery::create();
		     $oUserRegistry = $oUserRegistryQuery->findOneById($aData['id']);
		     if (!$oUserRegistry instanceof UserRegistry) {
		         throw new LogicException("UserRegistry should be an instance of UserRegistry but got something else." . __METHOD__);
		     }
		     $oUserRegistry = $this->fillVo($aData, $oUserRegistry);
		}
		else {
		     $oUserRegistry = new UserRegistry();
		     if (!empty($aData)) {
		         $oUserRegistry = $this->fillVo($aData, $oUserRegistry);
		     }
		}
		return $oUserRegistry;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UserRegistry
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UserRegistry
	{
		$oUserRegistry = $this->getModel($aData);


		 if(!empty($oUserRegistry))
		 {
		     $oUserRegistry = $this->fillVo($aData, $oUserRegistry);
		     $oUserRegistry->save();
		 }
		return $oUserRegistry;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UserRegistry $oModel
	 * @return UserRegistry
	 */
	protected function fillVo(array $aData, UserRegistry $oModel): UserRegistry
	{
		if(isset($aData['user_id'])) {
		     $oField = new UserId();
		     $mValue = $oField->sanitize($aData['user_id']);
		     $oModel->setUserId($mValue);
		}
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_value'])) {
		     $oField = new ItemValue();
		     $mValue = $oField->sanitize($aData['item_value']);
		     $oModel->setItemValue($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
