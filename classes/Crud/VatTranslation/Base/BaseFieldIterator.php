<?php
namespace Crud\VatTranslation\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\VatTranslation\ICollectionField as VatTranslationField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\VatTranslation\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param VatTranslationField[] $aFields */
	private array $aFields = [];


	/**
	 * @param VatTranslationField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof VatTranslationField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(VatTranslationField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): VatTranslationField
	{
		return current($this->aFields);
	}
}
