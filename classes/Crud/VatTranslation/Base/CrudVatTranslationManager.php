<?php
namespace Crud\VatTranslation\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\VatTranslation\FieldIterator;
use Crud\VatTranslation\Field\LanguageId;
use Crud\VatTranslation\Field\Name;
use Crud\VatTranslation\Field\VatId;
use Exception\LogicException;
use Model\Setting\MasterTable\Map\VatTranslationTableMap;
use Model\Setting\MasterTable\VatTranslation;
use Model\Setting\MasterTable\VatTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify VatTranslation instead if you need to override or add functionality.
 */
abstract class CrudVatTranslationManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return VatTranslationQuery::create();
	}


	public function getTableMap(): VatTranslationTableMap
	{
		return new VatTranslationTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "VatTranslation";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "mt_vat_translation toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "mt_vat_translation aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'VatId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['LanguageId', 'VatId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return VatTranslation
	 */
	public function getModel(array $aData = null): VatTranslation
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oVatTranslationQuery = VatTranslationQuery::create();
		     $oVatTranslation = $oVatTranslationQuery->findOneById($aData['id']);
		     if (!$oVatTranslation instanceof VatTranslation) {
		         throw new LogicException("VatTranslation should be an instance of VatTranslation but got something else." . __METHOD__);
		     }
		     $oVatTranslation = $this->fillVo($aData, $oVatTranslation);
		}
		else {
		     $oVatTranslation = new VatTranslation();
		     if (!empty($aData)) {
		         $oVatTranslation = $this->fillVo($aData, $oVatTranslation);
		     }
		}
		return $oVatTranslation;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return VatTranslation
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): VatTranslation
	{
		$oVatTranslation = $this->getModel($aData);


		 if(!empty($oVatTranslation))
		 {
		     $oVatTranslation = $this->fillVo($aData, $oVatTranslation);
		     $oVatTranslation->save();
		 }
		return $oVatTranslation;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param VatTranslation $oModel
	 * @return VatTranslation
	 */
	protected function fillVo(array $aData, VatTranslation $oModel): VatTranslation
	{
		if(isset($aData['language_id'])) {
		     $oField = new LanguageId();
		     $mValue = $oField->sanitize($aData['language_id']);
		     $oModel->setLanguageId($mValue);
		}
		if(isset($aData['vat_id'])) {
		     $oField = new VatId();
		     $mValue = $oField->sanitize($aData['vat_id']);
		     $oModel->setVatId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
