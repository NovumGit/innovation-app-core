<?php
namespace Crud\CouponType\Field\Base;

use Crud\CouponType\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'coupon_type' crud field from the 'coupon_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class CouponType extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'coupon_type';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCouponType';
	protected $sFqModelClassname = '\\\Model\Marketing\CouponType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
