<?php
namespace Crud\CouponType\Field;

use Crud\CouponType\Field\Base\MultiRedeem as BaseMultiRedeem;

/**
 * Skeleton subclass for representing multi_redeem field from the coupon_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:03
 */
final class MultiRedeem extends BaseMultiRedeem
{
}
