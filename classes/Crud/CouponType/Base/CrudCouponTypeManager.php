<?php
namespace Crud\CouponType\Base;

use Core\Utils;
use Crud;
use Crud\CouponType\FieldIterator;
use Crud\CouponType\Field\CouponType;
use Crud\CouponType\Field\DiscountAmount;
use Crud\CouponType\Field\DiscountPercentage;
use Crud\CouponType\Field\MultiRedeem;
use Crud\CouponType\Field\Title;
use Crud\CouponType\Field\ValidAfter;
use Crud\CouponType\Field\ValidUntil;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Marketing\CouponType as MarketingCouponType;
use Model\Marketing\CouponTypeQuery;
use Model\Marketing\Map\CouponTypeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify CouponType instead if you need to override or add functionality.
 */
abstract class CrudCouponTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return CouponTypeQuery::create();
	}


	public function getTableMap(): CouponTypeTableMap
	{
		return new CouponTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "CouponType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "coupon_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "coupon_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'CouponType', 'MultiRedeem', 'DiscountAmount', 'DiscountPercentage', 'ValidAfter', 'ValidUntil'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'CouponType', 'MultiRedeem', 'DiscountAmount', 'DiscountPercentage', 'ValidAfter', 'ValidUntil'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return CouponType
	 */
	public function getModel(array $aData = null): MarketingCouponType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oCouponTypeQuery = CouponTypeQuery::create();
		     $oCouponType = $oCouponTypeQuery->findOneById($aData['id']);
		     if (!$oCouponType instanceof CouponType) {
		         throw new LogicException("CouponType should be an instance of CouponType but got something else." . __METHOD__);
		     }
		     $oCouponType = $this->fillVo($aData, $oCouponType);
		}
		else {
		     $oCouponType = new CouponType();
		     if (!empty($aData)) {
		         $oCouponType = $this->fillVo($aData, $oCouponType);
		     }
		}
		return $oCouponType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return CouponType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): MarketingCouponType
	{
		$oCouponType = $this->getModel($aData);


		 if(!empty($oCouponType))
		 {
		     $oCouponType = $this->fillVo($aData, $oCouponType);
		     $oCouponType->save();
		 }
		return $oCouponType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param CouponType $oModel
	 * @return CouponType
	 */
	protected function fillVo(array $aData, MarketingCouponType $oModel): MarketingCouponType
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['coupon_type'])) {
		     $oField = new CouponType();
		     $mValue = $oField->sanitize($aData['coupon_type']);
		     $oModel->setCouponType($mValue);
		}
		if(isset($aData['multi_redeem'])) {
		     $oField = new MultiRedeem();
		     $mValue = $oField->sanitize($aData['multi_redeem']);
		     $oModel->setMultiRedeem($mValue);
		}
		if(isset($aData['discount_amount'])) {
		     $oField = new DiscountAmount();
		     $mValue = $oField->sanitize($aData['discount_amount']);
		     $oModel->setDiscountAmount($mValue);
		}
		if(isset($aData['discount_percentage'])) {
		     $oField = new DiscountPercentage();
		     $mValue = $oField->sanitize($aData['discount_percentage']);
		     $oModel->setDiscountPercentage($mValue);
		}
		if(isset($aData['valid_after'])) {
		     $oField = new ValidAfter();
		     $mValue = $oField->sanitize($aData['valid_after']);
		     $oModel->setValidAfter($mValue);
		}
		if(isset($aData['valid_until'])) {
		     $oField = new ValidUntil();
		     $mValue = $oField->sanitize($aData['valid_until']);
		     $oModel->setValidUntil($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
