<?php
namespace Crud\Payment\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\Payment\ICollectionField as PaymentField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Payment\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param PaymentField[] $aFields */
	private array $aFields = [];


	/**
	 * @param PaymentField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof PaymentField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(PaymentField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): PaymentField
	{
		return current($this->aFields);
	}
}
