<?php
namespace Crud\Payment\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\Payment\FieldIterator;
use Crud\Payment\Field\CreatedOn;
use Crud\Payment\Field\PayUrl;
use Crud\Payment\Field\PspId;
use Crud\Payment\Field\SaleOrderId;
use Crud\Payment\Field\Status;
use Exception\LogicException;
use Model\Finance\Map\PaymentTableMap;
use Model\Finance\Payment;
use Model\Finance\PaymentQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Payment instead if you need to override or add functionality.
 */
abstract class CrudPaymentManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return PaymentQuery::create();
	}


	public function getTableMap(): PaymentTableMap
	{
		return new PaymentTableMap();
	}


	public function getShortDescription(): string
	{
		return "";
	}


	public function getEntityTitle(): string
	{
		return "Payment";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "payment toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "payment aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'PspId', 'PayUrl', 'Status'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['CreatedOn', 'SaleOrderId', 'PspId', 'PayUrl', 'Status'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Payment
	 */
	public function getModel(array $aData = null): Payment
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oPaymentQuery = PaymentQuery::create();
		     $oPayment = $oPaymentQuery->findOneById($aData['id']);
		     if (!$oPayment instanceof Payment) {
		         throw new LogicException("Payment should be an instance of Payment but got something else." . __METHOD__);
		     }
		     $oPayment = $this->fillVo($aData, $oPayment);
		}
		else {
		     $oPayment = new Payment();
		     if (!empty($aData)) {
		         $oPayment = $this->fillVo($aData, $oPayment);
		     }
		}
		return $oPayment;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Payment
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Payment
	{
		$oPayment = $this->getModel($aData);


		 if(!empty($oPayment))
		 {
		     $oPayment = $this->fillVo($aData, $oPayment);
		     $oPayment->save();
		 }
		return $oPayment;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Payment $oModel
	 * @return Payment
	 */
	protected function fillVo(array $aData, Payment $oModel): Payment
	{
		if(isset($aData['created_on'])) {
		     $oField = new CreatedOn();
		     $mValue = $oField->sanitize($aData['created_on']);
		     $oModel->setCreatedOn($mValue);
		}
		if(isset($aData['sale_order_id'])) {
		     $oField = new SaleOrderId();
		     $mValue = $oField->sanitize($aData['sale_order_id']);
		     $oModel->setSaleOrderId($mValue);
		}
		if(isset($aData['psp_id'])) {
		     $oField = new PspId();
		     $mValue = $oField->sanitize($aData['psp_id']);
		     $oModel->setPspId($mValue);
		}
		if(isset($aData['pay_url'])) {
		     $oField = new PayUrl();
		     $mValue = $oField->sanitize($aData['pay_url']);
		     $oModel->setPayUrl($mValue);
		}
		if(isset($aData['status'])) {
		     $oField = new Status();
		     $mValue = $oField->sanitize($aData['status']);
		     $oModel->setStatus($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
