<?php
namespace Crud\Payment\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\Payment\ICollectionField;

/**
 * Base class that represents the 'created_on' crud field from the 'payment' table.
 * This class is auto generated and should not be modified.
 */
abstract class CreatedOn extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'created_on';
	protected $sFieldLabel = '';
	protected $sIcon = '';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getCreatedOn';
	protected $sFqModelClassname = '\\\Model\Finance\Payment';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
