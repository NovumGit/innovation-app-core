<?php

namespace Crud\Payment\Field;

use Crud\Payment\Field\Base\PspId as BasePspId;

/**
 * Skeleton subclass for representing psp_id field from the payment table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Sat 2:46:02
 */
final class PspId extends BasePspId
{
}
