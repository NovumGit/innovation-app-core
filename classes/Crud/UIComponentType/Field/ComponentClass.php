<?php

namespace Crud\UIComponentType\Field;

use Crud\UIComponentType\Field\Base\ComponentClass as BaseComponentClass;
use LowCode\ComponentFactory;

/**
 * Skeleton subclass for representing component_class field from the ui_component_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ComponentClass extends BaseComponentClass
{
    function getLookups($iSelectedItem = null)
    {
        $aAllComponents = ComponentFactory::getAll();
        $aRows = [];
        foreach ($aAllComponents as $oComponent)
        {
            $aRow = [
                'id' => $oComponent->getComponentXml()->getId(), 'label' => $oComponent->getName()];
            if ($iSelectedItem == $aRow['id'])
            {
                $aRow['selected'] = 'selected';
            }
            $aRows[] = $aRow;
        }
        return $aRows;
    }

    function getVisibleValue($iItemId)
    {
        if ($iItemId)
        {
            $aAllComponents = ComponentFactory::getAll();
            foreach ($aAllComponents as $oComponent)
            {
                if ($iItemId == $oComponent->getComponentXml()->getId())
                {
                    return $oComponent->getName();
                }
            }
        }
        return null;
    }
}
