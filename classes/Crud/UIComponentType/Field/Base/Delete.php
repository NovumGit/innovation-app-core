<?php 
namespace Crud\UIComponentType\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\UI\UIComponentType;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponentType)
		{
		     return "//system/ui_component_type/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof UIComponentType)
		{
		     return "//ui_component_type?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
