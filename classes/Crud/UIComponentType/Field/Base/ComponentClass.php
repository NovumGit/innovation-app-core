<?php
namespace Crud\UIComponentType\Field\Base;

use Crud\Generic\Field\GenericLookup;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UIComponentType\ICollectionField;

/**
 * Base class that represents the 'component_class' crud field from the 'ui_component_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class ComponentClass extends GenericLookup implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'component_class';
	protected $sFieldLabel = 'Component class';
	protected $sIcon = 'code';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getComponentClass';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponentType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function getDataType(): string
	{
		return 'lookup';
	}
}
