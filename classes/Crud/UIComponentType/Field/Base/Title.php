<?php
namespace Crud\UIComponentType\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\UIComponentType\ICollectionField;

/**
 * Base class that represents the 'title' crud field from the 'ui_component_type' table.
 * This class is auto generated and should not be modified.
 */
abstract class Title extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'title';
	protected $sFieldLabel = 'Component title';
	protected $sIcon = 'code';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getTitle';
	protected $sFqModelClassname = '\\\Model\System\UI\UIComponentType';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
