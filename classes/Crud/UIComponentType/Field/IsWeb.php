<?php
namespace Crud\UIComponentType\Field;

use Crud\UIComponentType\Field\Base\IsWeb as BaseIsWeb;

/**
 * Skeleton subclass for representing is_web field from the ui_component_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class IsWeb extends BaseIsWeb
{
}
