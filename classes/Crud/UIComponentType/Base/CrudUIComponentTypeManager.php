<?php
namespace Crud\UIComponentType\Base;

use Core\Utils;
use Crud;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Crud\UIComponentType\FieldIterator;
use Crud\UIComponentType\Field\AcceptAny;
use Crud\UIComponentType\Field\ComponentClass;
use Crud\UIComponentType\Field\IsApp;
use Crud\UIComponentType\Field\IsWeb;
use Crud\UIComponentType\Field\Title;
use Exception\LogicException;
use Model\System\UI\Map\UIComponentTypeTableMap;
use Model\System\UI\UIComponentType;
use Model\System\UI\UIComponentTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify UIComponentType instead if you need to override or add functionality.
 */
abstract class CrudUIComponentTypeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return UIComponentTypeQuery::create();
	}


	public function getTableMap(): UIComponentTypeTableMap
	{
		return new UIComponentTypeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Dit endpoint omschrijft de verschillende UI component typen, wordt automagisch gevuld.";
	}


	public function getEntityTitle(): string
	{
		return "UIComponentType";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "ui_component_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "ui_component_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'IsWeb', 'IsApp', 'ComponentClass', 'AcceptAny'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Title', 'IsWeb', 'IsApp', 'ComponentClass', 'AcceptAny'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return UIComponentType
	 */
	public function getModel(array $aData = null): UIComponentType
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oUIComponentTypeQuery = UIComponentTypeQuery::create();
		     $oUIComponentType = $oUIComponentTypeQuery->findOneById($aData['id']);
		     if (!$oUIComponentType instanceof UIComponentType) {
		         throw new LogicException("UIComponentType should be an instance of UIComponentType but got something else." . __METHOD__);
		     }
		     $oUIComponentType = $this->fillVo($aData, $oUIComponentType);
		}
		else {
		     $oUIComponentType = new UIComponentType();
		     if (!empty($aData)) {
		         $oUIComponentType = $this->fillVo($aData, $oUIComponentType);
		     }
		}
		return $oUIComponentType;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return UIComponentType
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): UIComponentType
	{
		$oUIComponentType = $this->getModel($aData);


		 if(!empty($oUIComponentType))
		 {
		     $oUIComponentType = $this->fillVo($aData, $oUIComponentType);
		     $oUIComponentType->save();
		 }
		return $oUIComponentType;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param UIComponentType $oModel
	 * @return UIComponentType
	 */
	protected function fillVo(array $aData, UIComponentType $oModel): UIComponentType
	{
		if(isset($aData['title'])) {
		     $oField = new Title();
		     $mValue = $oField->sanitize($aData['title']);
		     $oModel->setTitle($mValue);
		}
		if(isset($aData['is_web'])) {
		     $oField = new IsWeb();
		     $mValue = $oField->sanitize($aData['is_web']);
		     $oModel->setIsWeb($mValue);
		}
		if(isset($aData['is_app'])) {
		     $oField = new IsApp();
		     $mValue = $oField->sanitize($aData['is_app']);
		     $oModel->setIsApp($mValue);
		}
		if(isset($aData['component_class'])) {
		     $oField = new ComponentClass();
		     $mValue = $oField->sanitize($aData['component_class']);
		     $oModel->setComponentClass($mValue);
		}
		if(isset($aData['accept_any'])) {
		     $oField = new AcceptAny();
		     $mValue = $oField->sanitize($aData['accept_any']);
		     $oModel->setAcceptAny($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
