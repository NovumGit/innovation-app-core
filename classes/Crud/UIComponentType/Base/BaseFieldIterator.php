<?php
namespace Crud\UIComponentType\Base;

use Crud\BaseCrudFieldIterator;
use Crud\ICrudFieldIterator;
use Crud\UIComponentType\ICollectionField as UIComponentTypeField;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\UIComponentType\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param UIComponentTypeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param UIComponentTypeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof UIComponentTypeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(UIComponentTypeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): UIComponentTypeField
	{
		return current($this->aFields);
	}
}
