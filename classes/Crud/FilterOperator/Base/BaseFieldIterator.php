<?php
namespace Crud\FilterOperator\Base;

use Crud\BaseCrudFieldIterator;
use Crud\FilterOperator\ICollectionField as FilterOperatorField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\FilterOperator\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param FilterOperatorField[] $aFields */
	private array $aFields = [];


	/**
	 * @param FilterOperatorField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof FilterOperatorField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(FilterOperatorField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): FilterOperatorField
	{
		return current($this->aFields);
	}
}
