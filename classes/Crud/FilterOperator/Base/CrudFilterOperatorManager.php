<?php
namespace Crud\FilterOperator\Base;

use Core\Utils;
use Crud;
use Crud\FilterOperator\FieldIterator;
use Crud\FilterOperator\Field\Description;
use Crud\FilterOperator\Field\DescriptionSentence;
use Crud\FilterOperator\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\FilterOperator;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Model\Setting\CrudManager\Map\FilterOperatorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify FilterOperator instead if you need to override or add functionality.
 */
abstract class CrudFilterOperatorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return FilterOperatorQuery::create();
	}


	public function getTableMap(): FilterOperatorTableMap
	{
		return new FilterOperatorTableMap();
	}


	public function getShortDescription(): string
	{
		return "In dit endpoint zijn alle operators opgelagen";
	}


	public function getEntityTitle(): string
	{
		return "FilterOperator";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "filter_operator toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "filter_operator aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Description', 'DescriptionSentence'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['Name', 'Description', 'DescriptionSentence'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return FilterOperator
	 */
	public function getModel(array $aData = null): FilterOperator
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oFilterOperatorQuery = FilterOperatorQuery::create();
		     $oFilterOperator = $oFilterOperatorQuery->findOneById($aData['id']);
		     if (!$oFilterOperator instanceof FilterOperator) {
		         throw new LogicException("FilterOperator should be an instance of FilterOperator but got something else." . __METHOD__);
		     }
		     $oFilterOperator = $this->fillVo($aData, $oFilterOperator);
		}
		else {
		     $oFilterOperator = new FilterOperator();
		     if (!empty($aData)) {
		         $oFilterOperator = $this->fillVo($aData, $oFilterOperator);
		     }
		}
		return $oFilterOperator;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return FilterOperator
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): FilterOperator
	{
		$oFilterOperator = $this->getModel($aData);


		 if(!empty($oFilterOperator))
		 {
		     $oFilterOperator = $this->fillVo($aData, $oFilterOperator);
		     $oFilterOperator->save();
		 }
		return $oFilterOperator;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param FilterOperator $oModel
	 * @return FilterOperator
	 */
	protected function fillVo(array $aData, FilterOperator $oModel): FilterOperator
	{
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		if(isset($aData['description'])) {
		     $oField = new Description();
		     $mValue = $oField->sanitize($aData['description']);
		     $oModel->setDescription($mValue);
		}
		if(isset($aData['description_sentence'])) {
		     $oField = new DescriptionSentence();
		     $mValue = $oField->sanitize($aData['description_sentence']);
		     $oModel->setDescriptionSentence($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
