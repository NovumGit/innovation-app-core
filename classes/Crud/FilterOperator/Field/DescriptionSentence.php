<?php
namespace Crud\FilterOperator\Field;

use Crud\FilterOperator\Field\Base\DescriptionSentence as BaseDescriptionSentence;

/**
 * Skeleton subclass for representing description_sentence field from the filter_operator table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class DescriptionSentence extends BaseDescriptionSentence
{
}
