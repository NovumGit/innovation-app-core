<?php
namespace Crud\FilterOperator\Field\Base;

use Crud\FilterOperator\ICollectionField;
use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;

/**
 * Base class that represents the 'description_sentence' crud field from the 'filter_operator' table.
 * This class is auto generated and should not be modified.
 */
abstract class DescriptionSentence extends GenericString implements IFilterableField, IEditableField, ICollectionField
{
	protected $sFieldName = 'description_sentence';
	protected $sFieldLabel = 'Description short';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDescriptionSentence';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\FilterOperator';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}
}
