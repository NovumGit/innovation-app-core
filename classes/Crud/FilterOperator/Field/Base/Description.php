<?php
namespace Crud\FilterOperator\Field\Base;

use Crud\FilterOperator\ICollectionField;
use Crud\Generic\Field\GenericTextarea;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'description' crud field from the 'filter_operator' table.
 * This class is auto generated and should not be modified.
 */
abstract class Description extends GenericTextarea implements IFilterableField, IEditableField, ICollectionField, IRequiredField
{
	protected $sFieldName = 'description';
	protected $sFieldLabel = 'Description';
	protected $sIcon = 'edit';
	protected $sPlaceHolder = '';
	protected $sGetter = 'getDescription';
	protected $sFqModelClassname = '\\\Model\Setting\CrudManager\FilterOperator';


	public function sanitize($value)
	{
		return parent::sanitize($value);
	}


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['description']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Description" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
