<?php 
namespace Crud\FilterOperator\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Setting\CrudManager\FilterOperator;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterOperator)
		{
		     return "//system/filter_operator/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof FilterOperator)
		{
		     return "//filter_operator?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
