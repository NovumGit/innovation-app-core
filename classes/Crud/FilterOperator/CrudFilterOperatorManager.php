<?php
namespace Crud\FilterOperator;

/**
 * Skeleton subclass for representing a FilterOperator.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudFilterOperatorManager extends Base\CrudFilterOperatorManager
{
}
