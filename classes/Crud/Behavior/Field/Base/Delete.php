<?php 
namespace Crud\Behavior\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\System\DataModel\Behavior\Behavior;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Behavior)
		{
		     return "//system/behavior/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Behavior)
		{
		     return "//behavior?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
