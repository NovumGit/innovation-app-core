<?php
namespace Crud\Behavior\Field;

use Crud\Behavior\Field\Base\BehaviorTypeId as BaseBehaviorTypeId;

/**
 * Skeleton subclass for representing behavior_type_id field from the behavior table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 * Last modified: 2020-Nov-Mon 12:56:38
 */
final class BehaviorTypeId extends BaseBehaviorTypeId
{
}
