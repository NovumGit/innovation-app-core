<?php
namespace Crud\Behavior\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Behavior\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
