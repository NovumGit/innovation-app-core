<?php
namespace Crud\Behavior\Base;

use Core\Utils;
use Crud;
use Crud\Behavior\FieldIterator;
use Crud\Behavior\Field\BehaviorTypeId;
use Crud\Behavior\Field\DataModelId;
use Crud\Behavior\Field\Name;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\DataModel\Behavior\Behavior;
use Model\System\DataModel\Behavior\BehaviorQuery;
use Model\System\DataModel\Behavior\Map\BehaviorTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Behavior instead if you need to override or add functionality.
 */
abstract class CrudBehaviorManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return BehaviorQuery::create();
	}


	public function getTableMap(): BehaviorTableMap
	{
		return new BehaviorTableMap();
	}


	public function getShortDescription(): string
	{
		return "Met behaviors kun je tabellen taggen om bepaald gedrag te vertonen http://propelorm.org/documentation/cookbook/writing-behavior.html.";
	}


	public function getEntityTitle(): string
	{
		return "Behavior";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "behavior toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "behavior aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['BehaviorTypeId', 'DataModelId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['BehaviorTypeId', 'DataModelId', 'Name'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Behavior
	 */
	public function getModel(array $aData = null): Behavior
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oBehaviorQuery = BehaviorQuery::create();
		     $oBehavior = $oBehaviorQuery->findOneById($aData['id']);
		     if (!$oBehavior instanceof Behavior) {
		         throw new LogicException("Behavior should be an instance of Behavior but got something else." . __METHOD__);
		     }
		     $oBehavior = $this->fillVo($aData, $oBehavior);
		}
		else {
		     $oBehavior = new Behavior();
		     if (!empty($aData)) {
		         $oBehavior = $this->fillVo($aData, $oBehavior);
		     }
		}
		return $oBehavior;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Behavior
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Behavior
	{
		$oBehavior = $this->getModel($aData);


		 if(!empty($oBehavior))
		 {
		     $oBehavior = $this->fillVo($aData, $oBehavior);
		     $oBehavior->save();
		 }
		return $oBehavior;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Behavior $oModel
	 * @return Behavior
	 */
	protected function fillVo(array $aData, Behavior $oModel): Behavior
	{
		if(isset($aData['behavior_type_id'])) {
		     $oField = new BehaviorTypeId();
		     $mValue = $oField->sanitize($aData['behavior_type_id']);
		     $oModel->setBehaviorTypeId($mValue);
		}
		if(isset($aData['data_model_id'])) {
		     $oField = new DataModelId();
		     $mValue = $oField->sanitize($aData['data_model_id']);
		     $oModel->setDataModelId($mValue);
		}
		if(isset($aData['name'])) {
		     $oField = new Name();
		     $mValue = $oField->sanitize($aData['name']);
		     $oModel->setName($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
