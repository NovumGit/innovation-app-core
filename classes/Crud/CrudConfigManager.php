<?php
namespace Crud;

use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;

final class CrudConfigManager
{

    /**
     * @param BaseManager $oCrudManager
     * @return CrudConfig
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final static function getOrCreate(IBaseManager $oCrudManager):CrudConfig
    {
        $sManagerName = $oCrudManager->getManagerClassName(true);
        $oQuery = CrudConfigQuery::create();
        $oCrudConfig = $oQuery->filterByManagerName($sManagerName)->findOneOrCreate();

        if($oCrudConfig->isNew())
        {
            $oCrudConfig->save();
        }

        return $oCrudConfig;
    }
}
