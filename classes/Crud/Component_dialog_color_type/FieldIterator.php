<?php
namespace Crud\Component_dialog_color_type;

use Crud\Component_dialog_color_type\Base\BaseFieldIterator;

/**
 * Skeleton crud field iterator for representing a collection of Component_dialog_color_type crud fields.
 *
 *
 *
 *
 *
 *
 *
 * You may/can add additional methods to this class to meet your application requirements.
 * This class will only be generated once / when it does not exist already.
 */
final class FieldIterator extends BaseFieldIterator
{
}
