<?php
namespace Crud\Component_dialog_color_type\Base;

use Crud\IField;

/**
 * This interface is automatically generated, do not modify manually.
 * Modify Crud\Component_dialog_color_type\ICollectionField instead if you need to override or add functionality.
 */
interface IBaseCollectionField extends IField
{
}
