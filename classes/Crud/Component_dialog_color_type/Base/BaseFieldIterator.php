<?php
namespace Crud\Component_dialog_color_type\Base;

use Crud\BaseCrudFieldIterator;
use Crud\Component_dialog_color_type\ICollectionField as Component_dialog_color_typeField;
use Crud\ICrudFieldIterator;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Crud\Component_dialog_color_type\FieldIterator instead if you need to override or add functionality.
 */
abstract class BaseFieldIterator extends BaseCrudFieldIterator implements ICrudFieldIterator
{
	/** @param Component_dialog_color_typeField[] $aFields */
	private array $aFields = [];


	/**
	 * @param Component_dialog_color_typeField[] $aFields
	 */
	public function __construct(array $aFields)
	{
		foreach($aFields as $oField) {
		   if($oField instanceof Component_dialog_color_typeField ) {
		       $this->aFields[] = $oField;
		   }
		}
	}


	public function key(): int
	{
		return key($this->aFields);
	}


	public function next(): void
	{
		next($this->aFields);
	}


	public function valid(): bool
	{
		$key = key($this->aFields);
		return ($key !== null && $key !== false);
	}


	public function rewind(): void
	{
		reset($this->aFields);
	}


	public function add(Component_dialog_color_typeField $oField): void
	{
		$this->aFields[] = $oField;
	}


	public function current(): Component_dialog_color_typeField
	{
		return current($this->aFields);
	}
}
