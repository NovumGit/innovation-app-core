<?php
namespace Crud\Component_dialog_color_type\Base;

use Core\Utils;
use Crud;
use Crud\Component_dialog_color_type\FieldIterator;
use Crud\Component_dialog_color_type\Field\ItemKey;
use Crud\Component_dialog_color_type\Field\ItemLabel;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\System\LowCode\Dialog\Component_dialog_color_type;
use Model\System\LowCode\Dialog\Component_dialog_color_typeQuery;
use Model\System\LowCode\Dialog\Map\Component_dialog_color_typeTableMap;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Component_dialog_color_type instead if you need to override or add functionality.
 */
abstract class CrudComponent_dialog_color_typeManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	public function getQueryObject(): ModelCriteria
	{
		return Component_dialog_color_typeQuery::create();
	}


	public function getTableMap(): Component_dialog_color_typeTableMap
	{
		return new Component_dialog_color_typeTableMap();
	}


	public function getShortDescription(): string
	{
		return "Used for validation and to populate dropdown values.";
	}


	public function getEntityTitle(): string
	{
		return "Component_dialog_color_type";
	}


	public function getOverviewUrl(): string
	{
		return "";
	}


	public function getEditUrl(): string
	{
		return "";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "component_dialog_color_type toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "component_dialog_color_type aanpassen";
	}


	public function getDefaultOverviewFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		      $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	public function getDefaultEditFields(bool $bAddNs = false): array
	{
		$aOverviewColumns = ['ItemKey', 'ItemLabel'];
		if($bAddNs){
		   array_walk($aOverviewColumns, function(&$item) {
		       $item = Utils::makeNamespace($this, $item);
		   });
		}
		return $aOverviewColumns;
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array|null $aData
	 * @return Component_dialog_color_type
	 */
	public function getModel(array $aData = null): Component_dialog_color_type
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oComponent_dialog_color_typeQuery = Component_dialog_color_typeQuery::create();
		     $oComponent_dialog_color_type = $oComponent_dialog_color_typeQuery->findOneById($aData['id']);
		     if (!$oComponent_dialog_color_type instanceof Component_dialog_color_type) {
		         throw new LogicException("Component_dialog_color_type should be an instance of Component_dialog_color_type but got something else." . __METHOD__);
		     }
		     $oComponent_dialog_color_type = $this->fillVo($aData, $oComponent_dialog_color_type);
		}
		else {
		     $oComponent_dialog_color_type = new Component_dialog_color_type();
		     if (!empty($aData)) {
		         $oComponent_dialog_color_type = $this->fillVo($aData, $oComponent_dialog_color_type);
		     }
		}
		return $oComponent_dialog_color_type;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Component_dialog_color_type
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Component_dialog_color_type
	{
		$oComponent_dialog_color_type = $this->getModel($aData);


		 if(!empty($oComponent_dialog_color_type))
		 {
		     $oComponent_dialog_color_type = $this->fillVo($aData, $oComponent_dialog_color_type);
		     $oComponent_dialog_color_type->save();
		 }
		return $oComponent_dialog_color_type;
	}


	/**
	 * Fills the model object with data coming from a client.
	 * @param array $aData
	 * @param Component_dialog_color_type $oModel
	 * @return Component_dialog_color_type
	 */
	protected function fillVo(array $aData, Component_dialog_color_type $oModel): Component_dialog_color_type
	{
		if(isset($aData['item_key'])) {
		     $oField = new ItemKey();
		     $mValue = $oField->sanitize($aData['item_key']);
		     $oModel->setItemKey($mValue);
		}
		if(isset($aData['item_label'])) {
		     $oField = new ItemLabel();
		     $mValue = $oField->sanitize($aData['item_label']);
		     $oModel->setItemLabel($mValue);
		}
		return $oModel;
	}


	/**
	 * @param callable|null $filter
	 * @return FieldIterator
	 */
	public function getFieldIterator(callable $filter = null): FieldIterator
	{
		$aArray = $this->getAllFieldObjectsArray($filter);
		return new FieldIterator($aArray);
	}
}
