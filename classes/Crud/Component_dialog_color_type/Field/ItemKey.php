<?php
namespace Crud\Component_dialog_color_type\Field;

use Crud\Component_dialog_color_type\Field\Base\ItemKey as BaseItemKey;

/**
 * Skeleton subclass for representing item_key field from the component_dialog_color_type table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ItemKey extends BaseItemKey
{
}
