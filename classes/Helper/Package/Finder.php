<?php
namespace Helper\Package;

use Helper\Package\Components\PackageLocationManager;
use Helper\Package\Components\InfoIterator;

class Finder {

    private PackageLocationManager $oPackageLocationManager;
    function __construct()
    {
        $this->oPackageLocationManager = new PackageLocationManager();
    }
    function getAll():InfoIterator
    {
        return $this->oPackageLocationManager->getAllPackages();
    }

}

