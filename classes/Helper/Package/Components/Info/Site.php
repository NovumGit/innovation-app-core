<?php

namespace Helper\Package\Components\Info;

use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Hurah\Types\Type\SiteJson;
use Hurah\Types\Type\SystemId;
use Core\Utils;
use Helper\Package\Components\BasePackageInfo;
use Helper\Package\Components\IPackageInfo;
use Hi\Helpers\DirectoryStructure;

class Site extends BasePackageInfo implements IPackageInfo {

    function getSystemId(): SystemId {
        $oSiteJsonPath = new SiteJson($this->getInstallDir());
        return $oSiteJsonPath->getSystemId();
    }
    function getInstallDir():Path {
        $sInstallDir = $this->getComposer()->getExtra()['install_dir'];
        $oDirectoryIterator= new DirectoryStructure();
        return Utils::makePath($oDirectoryIterator->getPublicDir(true), $sInstallDir);
    }

}
