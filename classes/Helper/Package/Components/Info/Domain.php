<?php

namespace Helper\Package\Components\Info;

use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Hurah\Types\Type\SystemId;
use Core\Utils;
use Helper\Package\Components\BasePackageInfo;
use Helper\Package\Components\IPackageInfo;
use Hi\Helpers\DirectoryStructure;

class Domain extends BasePackageInfo implements IPackageInfo {

    public function getPackageType(): PluginType {
        return new PluginType(PluginType::DOMAIN);
    }
    function getSystemId(): SystemId {
        return new SystemId($this->getComposer()->getExtra()['system_id']);
    }
    function getInstallDir():Path {
        $oDirectoryIterator= new DirectoryStructure();
        return Utils::makePath($oDirectoryIterator->getDomainDir(true), $this->getSystemId());
    }
}
