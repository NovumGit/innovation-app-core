<?php
namespace Helper\Package\Components;

use Core\BaseIterator;

final class InfoIterator extends BaseIterator {

    function add(IPackageInfo $oPackage):void{
        $this->array[] = $oPackage;
    }
    function addCollection(InfoIterator $oInfoIterator):void{
        foreach ($oInfoIterator as $oPackageInfo)
        {
            $this->add($oPackageInfo);
        }
    }
    function current() : IPackageInfo {
        return $this->array[$this->position];
    }
}
