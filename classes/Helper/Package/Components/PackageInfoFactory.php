<?php
namespace Helper\Package\Components;

use Hurah\Types\Type\Composer;
use Core\Reflector;
use Exception\LogicException;

/**
 * Seeks locally installed packages and collects information on them.
 *
 * Class Local
 * @package Cli\Composer\Package\Helper
 * @internal
 */
class PackageInfoFactory {

    /**
     * Return a package info object based on it's composer.json file.
     * @param Composer $oComposer
     */
    static function get(Composer $oComposer):IPackageInfo {


        $sFqn = '\\Helper\\Package\\Components\\Info\\' . ucfirst((string) $oComposer->getType());

        $oReflector = new Reflector($sFqn);
        if(!$oReflector->implementsInterface(IPackageInfo::class))
        {
            throw new LogicException("No supported info object for plugin type {$oComposer->getType()} available.");
        }

        return new $sFqn($oComposer);
    }
}
