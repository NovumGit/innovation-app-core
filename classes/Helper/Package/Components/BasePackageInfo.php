<?php

namespace Helper\Package\Components;

use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Composer\Name;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Core\Utils;
use Helper\Git\Git;

abstract class BasePackageInfo implements IPackageInfo {

    private Composer $oComposer;

    function getComposer(): Composer {
        return $this->oComposer;
    }

    function __construct(Composer $oComposer) {
        $this->oComposer = $oComposer;
    }

    public function getPackageType(): PluginType {
        return $this->oComposer->getType();
    }

    public function getComposerName(): Name {
        return $this->oComposer->getName();
    }

    public function isInstalled(): bool {
        return is_dir($this->getInstallDir());
    }
    function getServiceName() : Composer\ServiceName
    {
        return Composer\ServiceName::fromSystemId($this->getSystemId());
    }
    function getVendor() : Composer\Vendor
    {
        return Composer\Vendor::fromSystemId($this->getSystemId());
    }

    public function hasGit(): bool {
        return file_exists($this->getGitPath());
    }

    public function getGitPath():?Path {
        return Utils::makePath($this->getInstallDir(), '.git');
    }
    public function getGitConfig(): ?array {
        return Git::getConfig($this->getGitPath());
    }

}
