<?php
namespace Helper\Package\Components;

use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PathCollection;
use Hurah\Types\Type\PluginType;
use Core\Utils;

/**
 * Class PackageTypeLocationManager
 * @package Helper\Package
 * @internal
 */
class TypedLocationManager {

    private PluginType $oType;
    private PathCollection $oPathCollection;
    function __construct(PluginType $oType, ?Path $oPath = null){
        $this->oType = $oType;
        $this->oPathCollection = new PathCollection();
        if($oPath)
        {
            $this->oPathCollection->add($oPath);
        }
    }
    function addPath(Path $oPath)
    {
        $this->oPathCollection->add($oPath);
    }

    /**
     * Returns the directories that may contain packages (in sub directories)
     * @return PathCollection
     */
    private function getPackageLocations():PathCollection {
        return $this->oPathCollection;
    }

    /**
     * Returns all the locations that actually have a package of this type.
     * @return PathCollection
     */
    private function getPackageDirs():PathCollection {
        $aPackageDirs = [];
        $oPathIterator = $this->getPackageLocations()->getIterator();
        foreach($oPathIterator as $oPath)
        {
            if($oPath->exists() && $oPath->isDir()){

                foreach($oPath->getDirectoryIterator() as $oPackageDir)
                {
                    if(!$oPackageDir->isDir() || $oPackageDir->isDot())
                    {
                        continue;
                    }
                    $sComposerLocation = Utils::makePath($oPackageDir->getPathname(), 'composer.json');
                    if(!file_exists($sComposerLocation))
                    {
                        continue;
                    }
                    $oComposerFile = new Path($sComposerLocation);
                    $oComposer = Composer::fromPath($oComposerFile);

                    if((string)$oComposer->getType() === (string)$this->oType)
                    {
                        $aPackageDirs[] = new Path($oPackageDir->getPathname());
                    }
                }
            }
        }
        return new PathCollection($aPackageDirs);
    }

    function getPackages():InfoIterator
    {
        $oInfoIterator = new InfoIterator();
        $oPackageDirIterator = $this->getPackageDirs()->getIterator();

        foreach($oPackageDirIterator as $oPackageLocation)
        {
            $sComposerLocation = Utils::makePath($oPackageLocation, 'composer.json');

            $oInfoIterator->add(PackageInfoFactory::get(Composer::fromPath($sComposerLocation)));
        }
        return $oInfoIterator;
    }
}
