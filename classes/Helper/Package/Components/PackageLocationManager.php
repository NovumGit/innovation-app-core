<?php

namespace Helper\Package\Components;

use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Hi\Helpers\DirectoryStructure;

/**
 * Seeks locally installed packages and collects information on them.
 *
 * Class Local
 * @package Cli\Composer\Package\Helper
 * @internal
 */
class PackageLocationManager {

    /**
     * @var TypedLocationManager[]
     */
    private array $aTypedLocationManagers = [];

    function __construct() {

        $oDirectoryStructure = new DirectoryStructure();

        $aPluginTypes = PluginType::getAll();
        array_walk($aPluginTypes, function (PluginType $oPluginType, $key, $oDirectoryStructure) {
            $this->initPackageTypeLocationManager($oPluginType);
            $oDirectory = $oDirectoryStructure->getPluginRespositoryDir($oPluginType, true);
            $this->getTypedLocationManager($oPluginType)->addPath(new Path($oDirectory));
        }, $oDirectoryStructure);

        $aOtherDirs = [
            [
                PluginType::DOMAIN,
                $oDirectoryStructure->getDomainDir(true),
            ],
            [
                PluginType::API,
                $oDirectoryStructure->getPublicDir(true),
            ],
            [
                PluginType::SITE,
                $oDirectoryStructure->getPublicDir(true),
            ],
        ];
        foreach ($aOtherDirs as $aProps) {
            $this->getTypedLocationManager(new PluginType($aProps[0]))->addPath(new Path($aProps[1]));
        }
    }

    function getAllPackages(): InfoIterator {
        $oInfoIterator = new InfoIterator();
        foreach ($this->getTypedLocationManagers() as $oTypedLocationManager) {

            $oInfoIterator->addCollection($oTypedLocationManager->getPackages());
        }
        return $oInfoIterator;
    }

    /**
     * @return TypedLocationManager[]
     */
    private function getTypedLocationManagers() {
        return $this->aTypedLocationManagers;
    }

    private function getTypedLocationManager(PluginType $oPluginType): TypedLocationManager {
        return $this->aTypedLocationManagers[(string)$oPluginType];
    }

    private function initPackageTypeLocationManager(PluginType $oPluginType): void {
        $this->aTypedLocationManagers[(string)$oPluginType] = new TypedLocationManager($oPluginType);
    }

}
