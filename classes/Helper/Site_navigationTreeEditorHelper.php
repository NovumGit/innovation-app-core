<?php
namespace Helper;
use Model\Category\CategoryQuery;
use Model\Cms\Site_navigationQuery;

class Site_navigationTreeEditorHelper
{
    static function getTree($iCategoryParentId = null)
    {
        $aOut = [];
        $oSite_navigationQuery = Site_navigationQuery::create();
        $oSite_navigationQuery->filterByParentId($iCategoryParentId);
        $oSite_navigationQuery->orderBySorting();
        $aSite_navigationItems = $oSite_navigationQuery->find();

        if(!$aSite_navigationItems->isEmpty())
        {
            foreach($aSite_navigationItems as $oSite_navigationItem)
            {
                $aState = [];
                if($iCategoryParentId == null)
                {
                    $aState['Opened'] = true;
                }
                $sDataAttr = '';
                if(!empty($aState))
                {
                    $sDataAttr = 'data-jstree="'.json_encode($aState).'"';
                }

                $aOut[] = "    <li id=\"c_{$oSite_navigationItem->getId()}\" $sDataAttr>";
                $aOut[] = '     '.$oSite_navigationItem->getName();

                $aChildren = Site_navigationQuery::create()->findByParentId($oSite_navigationItem->getId());
                if(!$aChildren->isEmpty())
                {
                    $aOut[] = '     <ul>';
                    $aOut[] = '     '.self::getTree($oSite_navigationItem->getId());
                    $aOut[] = '     </ul>';
                }

                $aOut[] = '    </li>';
            }
        }
        return join(PHP_EOL, $aOut);
    }
}