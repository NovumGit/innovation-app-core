<?php
namespace Helper;

use Core\Translate;
use Crud\AbstractCrudFilter;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\Field;
use Exception\LogicException;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Model\Setting\CrudManager\CrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\CrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditorQuery;

class FilterHelper extends AbstractCrudFilter
{
    static function getDefaultVisibleFilterConfig($iTabId)
    {
        $aFiltersOut = [];
        $aFilters = self::getVisibleTabFilters($iTabId);

        if(!empty($aFilters)) {

            foreach ($aFilters as $aFilter)
            {
                $oField = $aFilter['filter_field']['field_object'];
                if(!$oField instanceof Field)
                {
                    throw new LogicException('Field should be an instance of Field');
                }
                if(!$oField instanceof IFilterableField)
                {
                    throw new LogicException('Field should be an instance of IFilterableField');
                }
                if($aFilter['object'] instanceof CrudViewVisibleFilter)
                {
                    $aFiltersOut[$aFilter['object']->getId()] = $aFilter['actual_value'];
                }

                $aFiltersOut[$aFilter['filter_field']['classname']] = $aFilter['actual_value'];
                $aFiltersOut[$aFilter['filter_field']['classname']] = $aFilter['actual_value'];
            }
        }

        return $aFiltersOut;
    }
    /**
     * @param null $iCrudViewId (in de get variabele heet deze vaak tabid
     * @return array
     */
    static function getVisibleTabFilters($iCrudViewId = null)
    {
        if(!is_numeric($iCrudViewId))
        {
            throw new LogicException("Het tab id moet een nummerieke waarde bevatten.");
        }
        $oCrudViewVisibleFilterQuery = CrudViewVisibleFilterQuery::create();
        $oCrudViewVisibleFilterQuery->filterByCrudViewId($iCrudViewId);
        $aCrudViewVisibleFilters = $oCrudViewVisibleFilterQuery->find();
        $aFilters = [];


        if(!empty($aCrudViewVisibleFilters))
        {
            foreach($aCrudViewVisibleFilters as $oCrudViewVisibleFilter)
            {

                $sFieldClassName = $oCrudViewVisibleFilter->getFilterName();

                if(!class_exists($sFieldClassName))
                {
//                    CrudViewVisibleFilterQuery::create()->findOneById($oCrudViewVisibleFilter->getId())->delete();
                    throw new LogicException("Geen bijbehorende klasse gevonden bij Crudveld $sFieldClassName, we hebben het veld automatisch verwijderd uit de view, je kan op F5 drukken.");
                }
                $oField = new $sFieldClassName();
                if(!$oField instanceof Field)
                {
                    throw new LogicException("Expected an instance of field but got ".get_class($oField).".");
                }
                if(!$oField instanceof IFilterableField)
                {
                    throw new LogicException("Field ".$oField->getFieldTitle()." should implement IFilterableField but it does not.");
                }

                $sVisibleValue = self::getVisibleFilterValue($oField, $oCrudViewVisibleFilter->getFilterDefaultValue(), $oCrudViewVisibleFilter);

                $aFilters[] = [
                    'object' => $oCrudViewVisibleFilter,
                    'filter_field' =>
                        [
                            'namespaced_classname' => $sFieldClassName,
                            'crud_view_visible_filter_id' => $oCrudViewVisibleFilter->getId(),
                            'classname' => basename(str_replace('\\', '/', $sFieldClassName)),
                            'field_object' => $oField
                        ],
                    'visible_value' => $sVisibleValue,
                    'actual_value' => $oCrudViewVisibleFilter->getFilterDefaultValue(),
                ];
            }
        }
        return $aFilters;

    }
    /**
     * @param null $iListId (in de get variabele heet deze vaak tabid
     * @return array
     */
    public static function getFilters($iListId):array
    {
        if(!is_numeric($iListId))
        {
            throw new LogicException("Het tab id moet een nummerieke waarde bevatten.");
        }
        $oCrudViewHiddenFilterQuery = CrudViewHiddenFilterQuery::create();
        $oCrudViewHiddenFilterQuery->filterByCrudViewId($iListId);
        $aCrudViewHiddenFilters = $oCrudViewHiddenFilterQuery->find();
        return self::buildFilterArray($aCrudViewHiddenFilters);
    }

    /**
     * @param ModelCriteria $oQueryObject
     * @param $aFilters
     * @param callable $sOnGenericSearchField
     * @param array or null $aCustomQueryFields - the fieldnames on what $sOnGenericSearchField should replace normal fiters.
     * @return ModelCriteria
     * @throws \Exception
     */
    public static function generateVisibleFilters(ModelCriteria $oQueryObject, $aFilters, $sOnGenericSearchField = null, $aCustomQueryFields = null){
        if(!empty($aFilters)){
            foreach($aFilters as $iFilterId => $sFilterValue)
            {
                if($sFilterValue !== '')
                {
                    $oFilter = CrudViewVisibleFilterQuery::create()->findOneById($iFilterId);

                    if($oFilter instanceof CrudViewVisibleFilter){
                        $sBaseColumnName = basename(str_replace('\\', '/', $oFilter->getFilterName()));
                        $oFilterOperator = FilterOperatorQuery::create()->findOneById($oFilter->getFilterOperatorId());

                        if(($sBaseColumnName == 'GenericSearchfield' || is_array($aCustomQueryFields) && in_array($sBaseColumnName, $aCustomQueryFields)) && is_callable($sOnGenericSearchField))
                        {
                            $oQueryObject = $sOnGenericSearchField($oQueryObject, $sFilterValue, $oFilterOperator, $sBaseColumnName);
                        }
                        else
                        {
                            $oQueryObject->filterBy($sBaseColumnName, $oFilterOperator->formatValue($sFilterValue), $oFilterOperator->toCriteria());
                        }
                    }
                }
            }
        }

        return $oQueryObject;
    }

    /**
     * @param $iTabId
     * @param array $aArguments
     *                  extra_hidden_fields
     *                  show_thumb_list_buttons
     * @return string
     */
    public static function renderHtml($iTabId, $aArguments = [])
    {
        $aVisibleFilters = FilterHelper::getVisibleTabFilters($iTabId);
        $aFilter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : null;
        $aOut = [];
        $aOut[] = '<form method="get">';
        $aOut[] = '    <div class="row">';
        $bAnyElementOpen = false;
        if(!empty($aVisibleFilters))
        {

            if(!empty($aArguments['extra_hidden_fields']))
            {
                foreach($aArguments['extra_hidden_fields'] as $sFieldName => $sFieldValue)
                {
                    $aOut[] = '<input type="hidden" name="'.$sFieldName.'" value="'.$sFieldValue.'">';
                }
            }

            $iTotalColWidth = 0;

            $iFilterCount = count($aVisibleFilters);

            $iColWidth = 2;

            if($iFilterCount == 1)
            {
                $iColWidth = 10;
            }
			else if($iFilterCount == 2)
			{
				$iColWidth = 5;
			}
			else if($iFilterCount == 3)
			{
				$iColWidth = 3;
			}
            else if($iFilterCount == 4)
            {
                $iColWidth = 3;
            }
            foreach($aVisibleFilters as $aVisibleFilter)
            {
                $iVisibleFilterId = $aVisibleFilter['filter_field']['crud_view_visible_filter_id'];
                $oField = $aVisibleFilter['filter_field']['field_object'];
                if(!$oField instanceof Field)
                {
                    throw new LogicException("Filterfield must be an instance of Field.");
                }
                if(!$oField instanceof IFilterableField)
                {
                    throw new LogicException("Filterfield must implement IFilterableField.");
                }

                $aOut[] = '         <div class="col-md-'.$iColWidth.' pb5">';
                if($oField->getDataType() == 'boolean')
                {
                    $iTotalColWidth = $iTotalColWidth + $iColWidth;
                    $aOut[] = '<label class="field select">';
                    $aOut[] = '    <select name="filter['.$iVisibleFilterId.']" class="form-control filter_autosubmit">';
                    $aOut[] = '         <option value="">'.$oField->getFieldTitle().'</option>';
                    $sValue = isset($aFilter[$iVisibleFilterId]) ? $aFilter[$iVisibleFilterId] : $aVisibleFilter['actual_value'];
                    $sSelected = ('1' == $sValue) ? 'selected="selected"' : '';
                    $aOut[] = '        <option '.$sSelected.' value="1">Ja</option>';
                    $sSelected = ('0' == $sValue) ? 'selected="selected"' : '';
                    $aOut[] = '        <option '.$sSelected.' value="0">Nee</option>';
                    $aOut[] = '    </select>';
                    $aOut[] = '    <i class="arrow double"></i>';
                    $aOut[] = '</label>';
                }
                else if($oField->getDataType() == 'lookup')
                {
                    $iTotalColWidth +=2;
                    if(!$oField instanceof IFilterableLookupField)
                    {
                        throw new LogicException("Lookup field should implement InterfaceFilterableLookupField in order to be filterable.");
                    }
                    $aOut[] = '<label class="field select">';
                    $aOut[] = '    <select name="filter['.$iVisibleFilterId.']" class="form-control filter_autosubmit">';
                    $aOut[] = '        <option value="">'.$oField->getFieldTitle().'</option>';
                    $aLookups = $oField->getLookups();
                    if(!empty($aLookups))
                    {
                        foreach($aLookups as $aLookup)
                        {
                            $sValue = isset($aFilter[$iVisibleFilterId]) ? $aFilter[$iVisibleFilterId] : $aVisibleFilter['actual_value'];
                            $sSelected = ($aLookup['id'] == $sValue) ? 'selected="selected"' : '';
                            $aOut[] = '        <option '.$sSelected.' value="'.$aLookup['id'].'">'.$aLookup['label'].'</option>';
                        }
                    }
                    $aOut[] = '    </select>';
                    $aOut[] = '    <i class="arrow double"></i>';
                    $aOut[] = '</label>';
                }
                else if($oField->getDataType() == 'number' )
                {
                    $iTotalColWidth +=2;
                    $aOut[] = '<div class="input-group">';
                    $aOut[] = '    <span class="input-group-addon">';
                    $aOut[] = '        <i class="fa fa-search"></i>';
                    $aOut[] = '    </span>';

                    $sValue = isset($aFilter[$iVisibleFilterId]) ? $aFilter[$iVisibleFilterId] : $aVisibleFilter['actual_value'];

                    $aOut[] = '    <input name="filter['.$iVisibleFilterId.']" value="'.$sValue.'" type="text" id="icon-filter" class="form-control filter_autosubmit" placeholder="'.strtolower($oField->getFieldTitle()).'">';

                    $aOut[] = '</div>';
                }
                else if(in_array($oField->getDataType(), ['string', 'bsn']))
                {
                    $iTotalColWidth +=2;
                    $aOut[] = '<div class="input-group">';
	                $aOut[] = '    <span class="input-group-addon">';
                    $aOut[] = '        <i class="fa fa-search"></i>';
	                $aOut[] = '    </span>';

                    $sValue = isset($aFilter[$iVisibleFilterId]) ? $aFilter[$iVisibleFilterId] : $aVisibleFilter['actual_value'];
                    if($oField->getFieldName() == 'generic_search_field')
                    {
                        $aOut[] = '    <input name="filter['.$iVisibleFilterId.']" style="width:80%;" value="'.$sValue.'" type="text" id="icon-filter" class="form-control filter_autosubmit" placeholder="Zoeken">';
                    }
                    else
                    {
                        $aOut[] = '    <input name="filter['.$iVisibleFilterId.']" value="'.$sValue.'" type="text" id="icon-filter" class="form-control filter_autosubmit" placeholder="'.strtolower($oField->getFieldTitle()).'">';
                    }
                    $aOut[] = '</div>';
                }
                else
                {
                    throw new LogicException("Unsupported datatype ".$oField->getDataType());
                }
                $aOut[] = '         </div>';

                if(($iTotalColWidth % 12) === 0)
                {
                    $aOut[] = '    </div>';
                    $aOut[] = '    <div class="row">';
                }
            }

            if(isset($aArguments['show_thumb_list_buttons']) && $aArguments['show_thumb_list_buttons'])
            {
                $iNextColWidth = 4;
            }
            else
            {
                $iNextColWidth = 2;
            }
            $iTotalColWidth += $iNextColWidth;

            if(($iTotalColWidth % 12) === 0)
            {
                $aOut[] = '    </div>';
                $aOut[] = '    <div class="row">';
            }

            $bAnyElementOpen = true;
            $aOut[] = '<div class="col-md-'.$iNextColWidth.' pb5">';
            $aOut[] = '    <div class="input-group">';
            $aOut[] = '        <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Zoeken</button>';

        }

        if(!$bAnyElementOpen && isset($aArguments['show_thumb_list_buttons']))
        {
            $bAnyElementOpen = true;
            $aOut[] = '<div class="col-md-12 pb5">';
            $aOut[] = '    <div class="input-group">';
        }

        if(isset($aArguments['show_thumb_list_buttons']) && $aArguments['show_thumb_list_buttons'])
        {
            $sListClass = $aArguments['show_thumb_list_view'] == 'list' ? 'primary' : 'default';
            $sGridClass = $aArguments['show_thumb_list_view'] == 'grid' ? 'primary' : 'default';

            $aOut[] = '        <a title="'.Translate::fromCode('Lijst weergave').'" href="#" id="list" class="toggle_view btn btn-'.$sListClass.' ml10">';
            $aOut[] = '            <span class="fa fa-list"></span>';
            $aOut[] = '        </a>';
            $aOut[] = '        <a title="'.Translate::fromCode('Grid weergave').'" href="#" id="grid" class="toggle_view btn btn-'.$sGridClass.' ml10">';
            $aOut[] = '            <span class="fa fa-table"></span>';
            $aOut[] = '        </a>';

        }

        if(isset($aArguments['show_thumb_list_view']) && $aArguments['show_thumb_list_view'] == 'grid')
        {
	        // Only when we look at the grid view we want to be able to schale the thumbs
	        // Only when we look at the grid view we want to be able to schale the thumbs
		    if(isset($aArguments['show_tumb_scaler']) && $aArguments['show_tumb_scaler'])
		    {

			    $iThumbSizeMin = $aArguments['thumb_sizes_available'][0];
			    $iThumbSizeMax = array_reverse($aArguments['thumb_sizes_available'])[0];
			    $aSizes = $aArguments['thumb_sizes_available'];

				$iCurrentSize = $_SESSION['thumb_size'] ?? 3;



				if($iCurrentSize > $iThumbSizeMax)
				{
					$iCurrentSize = $iThumbSizeMax;
				}
				else if($iCurrentSize < $iThumbSizeMin)
				{
					$iCurrentSize = $iThumbSizeMin;
				}

				$iCurrentSizeKey = array_flip($aSizes)[$iCurrentSize];

				$iPrevStepArrayKey = ($iCurrentSizeKey - 1) < 0 ? 0 : $iCurrentSizeKey - 1;
				$iNextStepArrayKey = ($iCurrentSizeKey + 1) > count($aSizes) - 1 ? count($aSizes) - 1 : ($iCurrentSizeKey + 1);

				$iPrevStep = $aSizes[$iPrevStepArrayKey];
				$iNextStep = $aSizes[$iNextStepArrayKey];

			    $sSmallerColor = $iCurrentSize == $iThumbSizeMin ? 'default' : 'success';
			    $sLargerColor = $iCurrentSize == $iThumbSizeMax ? 'default' : 'success';

			    $aOut[] = '        <a data-size="' . $iPrevStep . '" title="' . Translate::fromCode('Verkleinen') . '" href="#" class="toggle_thumb_size btn btn-' . $sSmallerColor . ' ml10">';
	            $aOut[] = '        		<span class="fa fa-lg fa-search-minus"></span>';
		        $aOut[] = '        </a>';
	            $aOut[] = '        <a data-size="' . $iNextStep . '" title="' . Translate::fromCode('Vergroten') . '" href="#" class="toggle_thumb_size btn btn-' . $sLargerColor . ' ml10">';
		        $aOut[] = '        		<span class="fa fa-lg fa-search-plus"></span>';
	            $aOut[] = '        </a>';
		    }
        }
        if(!$bAnyElementOpen)
        {
            return '';
        }
        $aOut[] = '    </div>';
        $aOut[] = '</div>';
        $aOut[] = '</div>';
        $aOut[] = '</form>';

        return join(PHP_EOL, $aOut);
    }
}
