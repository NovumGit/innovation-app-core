<?php
namespace Helper;

class AutoTranslationHelper{


    /*
     * trnsl.1.1.20160728T083850Z.b2d8e8227855c297.a20f6255a0e09e28b1226ff7174fddacc4ebf294
        Azerbaijan	az	Macedonian	mk
        Albanian	sq	Maori	mi
        English	en	Marathi	mr
        Arabic	ar	Mongolian	mn
        Armenian	hy	German	de
        Afrikaans	af	Nepali	ne
        Basque	eu	language code	no
        Bashkir	ba	Punjabi	pa
        Belarusian	be	Persian	fa
        Bengali	bn	Polish	pl
        Bulgarian	bg	Portuguese	pt
        Bosnian	bs	Romanian	ro
        Welsh	cy	Russian	ru
        Hungarian	hu	Cebuano	ceb
        Vietnamese	vi	Serbian	sr
        Haitian (Creole)	ht	Sinhala	si
        Galician	gl	Slovakian	sk
        Dutch	nl	Slovenian	sl
        Greek	el	Swahili	sw
        Georgian	ka	Sundanese	su
        Gujarati	gu	Tajik	tg
        Danish	da	Thai	th
        Hebrew	he	Tagalog	tl
        Yiddish	yi	Tamil	ta
        Indonesian	id	Tatar	tt
        Irish	ga	Telugu	te
        Italian	it	Turkish	tr
        Icelandic	is	Udmurt	udm
        Spanish	es	Uzbek	uz
        Kazakh	kk	Ukrainian	uk
        Kannada	kn	Urdu	ur
        Catalan	ca	Finnish	fi
        Kyrgyz	ky	French	fr
        Chinese	zh	Hindi	hi
        Korean	ko	Croatian	hr
        Latin	la	Czech	cs
        Latvian	ly	Swedish	sv
        Lithuanian	lt	Scottish Gaelic	gd
        Malagasy	mg	Estonian	et
        Malay	ms	Esperanto	eo
        Malayalam	ml	Javanese	jv
        Maltese	mt	Japanese	ja
     */

    public static function doTranslate($sString, $sLangFrom, $sLangTo)
    {

        if(strpos($sLangFrom, '_'))
        {
            $sLangFrom = explode('_', $sLangFrom)[0];
        }
        if(strpos($sLangTo, '_'))
        {
            $sLangTo = explode('_', $sLangTo)[0];
        }

        $sDirection = $sLangFrom.'-'.$sLangTo;
        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, "https://translate.yandex.net/api/v1.5/tr.json/translate?lang=$sDirection&key=trnsl.1.1.20160728T083850Z.b2d8e8227855c297.a20f6255a0e09e28b1226ff7174fddacc4ebf294");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['text' => $sString]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $sServerOutputJson = curl_exec ($ch);

        curl_close ($ch);

        $aServerOutput = json_decode($sServerOutputJson, true);

        if($aServerOutput['code'] == 200 && isset($aServerOutput['text'][0]))
        {
            return $aServerOutput['text'][0];
        }
    }
}