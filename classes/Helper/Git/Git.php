<?php

namespace Helper\Git;

use Hurah\Types\Type\GitUri;
use Hurah\Types\Type\Path;
use Core\Utils;
use Helper\Ssh\Key;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class Git {

    static function init(Path $oDirectory, ?GitUri $remoteUrl, string $sName, string $sEmail, OutputInterface $output): void {

        $finder = new ExecutableFinder();
        $gitBin = $finder->find('git');

        $aCommands = [];

        $aConfig = self::getConfig($oDirectory);

        if(!isset($aConfig['global.user.email']))
        {
            $aCommands[] = [$gitBin, 'config', '--global', 'user.email', $sEmail];
        }

        if(!isset($aConfig['global.user.name']))
        {
            $aCommands[] = [$gitBin, 'config', '--global', 'user.name', $sName];
        }

        $aCommands[] = [$gitBin, 'init'];
        $aCommands[] = [$gitBin, 'add', '.'];
        $aCommands[] = [$gitBin, 'commit', '-m "first commit"'];
        $aCommands[] = [$gitBin, 'branch', '-M', 'main'];

        $output->write("<comment>Checking if SSH keys are present</comment> ");
        if(!($oKeyFile = Utils::makePath('/root', '.ssh', 'id_rsa'))->exists())
        {
            $output->writeln("<fg=red>No, creating now</>");
            if(!($oKeyDir = $oKeyFile->dirname())->isDir())
            {
                $output->writeln("<comment>make dir:</comment> $oKeyDir");
                $oKeyDir->makeDir();
            }

            Key::create($oKeyFile, $output);
        }
        else
        {
            $output->writeln("<info>YES!</info>");
        }

        chmod($oDirectory, 0777);
        if($remoteUrl)
        {
            $finder = new ExecutableFinder();
            $sshBin = $finder->find('ssh');
            $sshAccept = new Process([$sshBin, '-o', 'StrictHostKeyChecking=no', 'git@github.com']);
            $sshAccept->run();

            $aCommands[] = [$gitBin, 'remote', 'add', 'origin', $remoteUrl];
            $aCommands[] = [$gitBin, 'push', '-u', 'origin', 'main'];
        }

        if (!$gitBin) {
            return;
        }
        foreach ($aCommands as $aCommand) {
            $output->write('<info>Executing:</info> ' . join(' ', $aCommand));
            $oCommand = new Process($aCommand, $oDirectory);

            $result = $oCommand->run();

            if ($result === 0) {
                $output->writeln(" <info>success</info>");
            } else {

                if($aCommand[1] == 'push' && $result == 128)
                {
                    $output->writeln(" <error>failed</error>");
                    $output->writeln('');
                    $output->writeln('');
                    $output->writeln("<error> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Important <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< </error>");
                    $output->writeln(<<<EOT
Your git repository has been created but we where unable to push it to the remote server. This is normal as there
is no means of authentication configured for us yet. We have generated an ssh key that you can use. All you have to do
is go to the repository settings at your git hosting provider (GitHub, Gitlab, etc) and paste the key below and give it
write access. An important note is that you should only grant access to this specific repository, not to everything.
EOT);

                    $oPubKey = Utils::makePath("$oKeyFile.pub");
                    $output->writeln('');
                    $output->writeln('');
                    $output->writeln('----------------------------------copy the text below-------------------------------');
                    $output->write($oPubKey->getFile()->contents());
                    $output->writeln('----------------------------------copy the text above-------------------------------');
                }
                else
                {
                    $output->writeln(" <error>failed with exit code {$result}</error>");
                    $output->writeln($oCommand->getErrorOutput());
                }
            }
        }
    }

    static function getConfig(Path $oGitPath): ?array {
        if (!$oGitPath->exists()) {
            return null;
        }

        $aGitConfig = [];
        $finder = new ExecutableFinder();
        $gitBin = $finder->find('git');

        if (!$gitBin) {
            return null;
        }
        $sPrevCwd = getcwd();
        chdir(dirname($oGitPath));
        $cmd = new Process([
            $gitBin,
            'config',
            '-l',
        ]);
        $cmd->run();
        chdir($sPrevCwd);

        if ($cmd->isSuccessful()) {
            $aGitConfig = [];
            preg_match_all('{^([^=]+)=(.*)$}m', $cmd->getOutput(), $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $aGitConfig[$match[1]] = $match[2];
            }

            return $aGitConfig;
        }

        return $aGitConfig;
    }
}
