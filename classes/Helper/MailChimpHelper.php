<?php
namespace Helper;

use Core\Translate;
use Crud\CrudViewManager;
use Crud\FormManager;
use Exception\LogicException;
use Model\Crm\CustomerQuery;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;

class MailChimpHelper
{
    static function countLocalMatchingItems($sForeignMailChimpId):int
    {
        if(self::syncsWithNuiCart($sForeignMailChimpId))
        {
            $oCrudView = self::getCrudView($sForeignMailChimpId);
            return self::getQueryObject($oCrudView)->count();
        }
        return 0;
    }
    static function getQueryObjectByForeignMailchimpId($sForeignMailChimpId):CustomerQuery
    {
        $oCrudView = self::getCrudView($sForeignMailChimpId);
        if(!$oCrudView instanceof CrudView)
        {
            throw new LogicException('CrudView not found, first check syncsWithNuiCart before you call this method.');
        }
        return self::getQueryObject($oCrudView);
    }
    private static function getCrudView($sForeignMailChimpId)
    {
        $sCrudViewName = MailChimpHelper::getMailChimpListId($sForeignMailChimpId);
        $oCrudView = CrudViewQuery::create()->findOneByName($sCrudViewName);
        return $oCrudView;
    }
    private static function getQueryObject(CrudView $oCrudview):CustomerQuery
    {
        $oCustomerQuery = CustomerQuery::create();
        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $oCrudview->getId());
        // Er zijn geen visinble filters dus dit kan weg volgens mij.
        // $oCustomerQuery = FilterHelper::generateVisibleFilters($oCustomerQuery, $aFilterArguments);

        return $oCustomerQuery;
    }

    static function syncsWithNuiCart($sForeignMailChimpId)
    {
        $oCrudView = self::getCrudView($sForeignMailChimpId);
        return ($oCrudView instanceof CrudView);
    }
    static function getMailChimpListId($sForeignMailChimpId)
    {
        return 'mailchimp_list_'.$sForeignMailChimpId;
    }

    static function getSampleData(CrudView $oCrudview, FormManager $oCrudMailChimpManager, $iCurrentPage = 1)
    {
        $oCustomerQuery = self::getQueryObject($oCrudview);

        $oCrudMailChimpManager->setOverviewData($oCustomerQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aViewData['filter_html'] = FilterHelper::renderHtml($oCrudview->getId());
        $aViewData['overview_table'] = $oCrudMailChimpManager->getOverviewHtml($aFields);
        $aViewData['title'] = Translate::fromCode('Klanten');
        return $aViewData;
    }
}