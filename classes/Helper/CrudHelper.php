<?php
namespace Helper;

use Crud\FormManager;
use Crud\CrudViewManager;
use model\Setting\CrudManager\CrudEditorQuery;
use model\Setting\CrudManager\CrudConfig;
use model\Setting\CrudManager\CrudEditor;
use model\Setting\CrudManager\CrudConfigQuery;
use model\Setting\CrudManager\CrudView;
use Exception\LogicException;


class CrudHelper{

    /**
     * @return CrudView
     */
    static function getDefaultCrudView(FormManager $oManager)
    {
        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oManager);

        $oCrudview = current($aCrudViews);

        if (!$oCrudview instanceof CrudView) {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }

        return $oCrudview;
    }

    /**
     * @param String FormManager class name
     * @return CrudEditor[]
     */
    static function editorCreateIfNotExists($sManagerClassName)
    {
        $oCrudConfig = CrudConfigQuery::create()->findOneByManagerName($sManagerClassName);

        if(!$oCrudConfig instanceof CrudConfig)
        {
            $oCrudConfig = new CrudConfig();
            $oCrudConfig->setManagerName($sManagerClassName);
            $oCrudConfig->save();
        }

        $aCrudEditors = CrudEditorQuery::create()->findByCrudConfigId($oCrudConfig->getId());

        if($aCrudEditors->isEmpty())
        {
            $oCrudEditor = new CrudEditor();
            $oCrudEditor->setName('default_website_contact_form');
            $oCrudEditor->setCrudConfigId($oCrudConfig->getId());
            $oCrudEditor->setTitle('Standaard contactformulier');
            $oCrudEditor->save();

            $aCrudEditors = CrudEditorQuery::create()->findByCrudConfigId($oCrudConfig->getId());
        }

        return $aCrudEditors;
    }
}
