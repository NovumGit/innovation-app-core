<?php
namespace Helper;

use Core\Cfg;
use Crud\BaseManager;

class EventHelper
{

    static function getAllByScope($sScopeName)
    {
        $aEvents = false;

        $sPath = '../admin_modules/'.$sScopeName.'/Event/*';
        $aEventFiles = glob($sPath);

        if(!empty($aEventFiles))
        {
            $aEvents = [];
            foreach($aEventFiles as $sEventFile)
            {
                $sNamespacedClassname = str_replace('/', '\\', $sEventFile);
                $sNamespacedClassname = preg_replace('#^.\\\#', '\\', $sNamespacedClassname);
                $sNamespacedClassname = preg_replace('#\.php#', '', $sNamespacedClassname);
                $sNamespacedClassname = str_replace('Default\\', '', $sNamespacedClassname);
                $sNamespacedClassname = str_replace('..\admin_modules', 'AdminModules', $sNamespacedClassname);

                $aEvents[] =
                [
                    'class' => new $sNamespacedClassname($_GET, $_POST),
                    'current_scope' => $sScopeName,
                    'class_name' => $sNamespacedClassname
                ];
            }
        }

        if(Cfg::get('CUSTOM_NAMESPACE'))
        {
            $sFqManagerName = 'Crud\\Custom\\' . Cfg::get('CUSTOM_NAMESPACE') . '\\' .  $sScopeName . '\\Crud' . $sScopeName . 'Manager';
            if(class_exists($sFqManagerName))
            {
                $oFormManager = new $sFqManagerName();
                if($oFormManager instanceof BaseManager)
                {
                    $aActions = $oFormManager->getAllAvailableActions();
                }
            }
        }
        return $aEvents;
    }
}
