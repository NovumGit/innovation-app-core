<?php

namespace Helper;

use Core\Cfg;
use Core\Utils;
use Crud\Field;
use Crud\FormManager;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Exception\LogicException;
use Exception\NullPointerException;

class CrudManagerHelper {

    public static function getAllFieldObjects(FormManager $oCrudManager, $sModule, $sFilterField = null) {
        $aFields = [];
        $oSelectedField = null;
        $aSelectedFieldLookups = null;
        foreach ($oCrudManager->getAllAvailableFields() as $sFieldName) {
            $oField = self::getField($sFieldName, $sModule);
            if ($oField instanceof IFilterableField && $oField instanceof Field) {
                $sSelected = '';

                if ($sFilterField == $sFieldName) {
                    $sSelected = 'selected="selected"';
                    $oSelectedField = $oField;
                }
                $aField = [
                    'name'     => $sFieldName,
                    'selected' => $sSelected,
                    'title'    => $oField->getFieldTitle(),
                ];

                if ($oField->getDataType() == 'lookup') {
                    if ($oField instanceof IFilterableLookupField) {
                        if ($sFilterField == $sFieldName) {
                            $aSelectedFieldLookups = $oField->getLookups();
                        }
                    } else {
                        throw new LogicException("Filtervelden van het datatype lookup moeten InterfaceFilterableLookupField implementeren, dit is niet gebeurd in " . get_class($oField) . ".");
                    }
                }
                $aFields[] = $aField;
            }
        }

        if ($aFields) {
            Utils::usort_strcmp($aFields, 'title');
        }

        return ['fields'                 => $aFields,
                'selected_field'         => $oSelectedField,
                'selected_field_lookups' => $aSelectedFieldLookups,
        ];
    }

    /**
     * @param $sFieldName
     * @param $sModule
     * @return Field
     */
    private static function getField($sFieldName, $sModule): Field {
        $sCrudNamespace = "\\Crud\\$sModule";
        $sCustomCrudFieldLocation = $sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";

        if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($sCustomCrudFieldLocation)) {
            $sFieldClassName = $sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
            if (class_exists($sFieldClassName)) {
                $oField = new $sFieldClassName();
            }
        }

        // $sModule
        $sCrudNamespace = "\\Crud\\Custom\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sModule";
        $sCustomCrudFieldLocation = $sCrudNamespace . "\\Field\\$sFieldName";

        if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName")) {
            $sFieldClassName = $sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
            if (class_exists($sFieldClassName)) {
                $oField = new $sFieldClassName();
            }
        }

        $sBasicFieldClassName = "\\Crud\\$sModule\\Field\\$sFieldName";

        if (!isset($oField) && class_exists($sBasicFieldClassName)) {
            $oField = new $sBasicFieldClassName();
        }
        if (!$oField instanceof Field) {
            throw new NullPointerException("Could not find CrudField, looked in: [$sCustomCrudFieldLocation, $sBasicFieldClassName].");
        }
        return $oField;
    }
}
