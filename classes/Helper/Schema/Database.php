<?php
namespace Helper\Schema;

class Database extends \SimpleXMLElement
{
    private $table;
    private $defaultIdMethod;

    function getName()
    {
        return $this->attributes()->name;
    }
    function getDefaultIdMethod()
    {
        return $this->attributes()->defaultIdMethod;
    }
    function getExternalSchemas():ExternalSchemaIterator
    {

        $oExternalSchemaIterator = new ExternalSchemaIterator();
        $externalSchema = $this->{'external-schema'};
        if(is_iterable($externalSchema))
        {
            foreach ($externalSchema as $schema)
            {
                $oExternalSchemaIterator->append(new ExternalSchema($schema->attributes()));
            }
        }

        return $oExternalSchemaIterator;
    }
    function getCrudRoot()
    {
        return $this->attributes()->crudRoot;
    }
    function getCustom()
    {
        return $this->attributes()->custom;
    }
    function getCrudNamespace()
    {
        return $this->attributes()->crudNamespace;
    }

    function getModules():ModuleIterator
    {
        $oModuleIterator = new ModuleIterator();

        if(is_iterable($this->modules->module))
        {
            foreach($this->modules->module as $x => $module)
            {
                $oModuleIterator->append(new Module($module, $this));
            }
        }

        return $oModuleIterator;
    }

    /**
     * return TableIterator
     */
    function getTables():TableIterator
    {
        $oTableIterator = new TableIterator();

        foreach($this->table as $x => $table)
        {
            $oTableIterator->append(new Table($table, $this));
        }

        return $oTableIterator;
    }
}
