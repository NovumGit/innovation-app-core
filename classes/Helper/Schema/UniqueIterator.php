<?php
namespace Helper\Schema;

class UniqueIterator extends \ArrayIterator
{
    function current():Unique
    {
        return parent::current();
    }
    function asArray(): array
    {
        $aOut = [];
        $aColumns = $this;
        if ($aColumns)
        {
            foreach ($aColumns as $oColumn)
            {
                $aOut[] = $oColumn->getColumnName();
            }
        }
        return $aOut;
    }
}
