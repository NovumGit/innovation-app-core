<?php
namespace Helper\Schema;

use Model\System\DataModel\Model\ModelConstraintQuery;

class ForeignKey
{
    private $foreign_key;

    function __construct(\SimpleXMLElement $foreign_key)
    {
        $this->foreign_key = $foreign_key;
    }
    function getPhpName():string {
        return (string) $this->foreign_key->attributes()->phpName;
    }
    function getRefPhpName()
    {
        return (string) $this->foreign_key->attributes()->refPhpName;
    }

    function getForeignTable()
    {
        return $this->foreign_key->attributes()->foreignTable;
    }
    function getOnDelete():string
    {
        return (string) $this->foreign_key->attributes()->onDelete;
    }
    function getOnUpdate():string
    {
        return (string) $this->foreign_key->attributes()->onUpdate;
    }
    function getLocalColumn()
    {
        return (string) $this->foreign_key->reference[0]->attributes()->local[0];
    }
    function getForeignColumn()
    {
        return (string) $this->foreign_key->reference[0]->attributes()->foreign[0];
    }
    function toPropel(Table $oTable)
    {
        $oModelConstraintQuery = ModelConstraintQuery::create();
        $oModelConstraintQuery->filterByLocalModelField($oTable->getName(), $this->getLocalColumn());
        $oModelConstraintQuery->filterByForeignModelField($this->getForeignTable(), $this->getForeignColumn());
        $oModelConstraint = $oModelConstraintQuery->findOneOrCreate();
        $oModelConstraint->setPhpName($this->getPhpName());
        $oModelConstraint->setRefPhpName($this->getRefPhpName());
        $oModelConstraint->setOnDelete($this->getOnDelete());
        $oModelConstraint->setOnUpdate($this->getOnUpdate());


        $oModelConstraint->save();

        echo "{$oModelConstraint->getId()} created foreign-key from {$oTable->getName()}.{$this->getLocalColumn()} to {$this->getForeignTable()}.{$this->getForeignColumn()}" . PHP_EOL;
    }
}
