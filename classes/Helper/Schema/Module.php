<?php
namespace Helper\Schema;


use Core\Utils;
use Model\Module\ModuleQuery;
use Propel\Runtime\Exception\PropelException;

class Module
{
    private $module;
    private Database $database;

    function __construct(Database $module, Database $database)
    {
        $this->module = $module;
        $this->database = $database;
    }
    function getDatabase():Database
    {
        return $this->database;
    }
    function getName():string
    {
        return trim($this->module->attributes()->name);
    }
    function getTitle()
    {
        return $this->module->attributes()->title;
    }
    function getIcon()
    {
        return $this->module->attributes()->icon;
    }
    function getModuleDir()
    {
        return Utils::camelCase($this->getName());
    }

    /**
     * @param bool $bMakePersistent Stores the model to the database before returning
     * @param bool $bDeep When true iterates down to the tables, foreign keys and columns
     * @return \Model\Module\Module
     * @throws PropelException
     */
    function toPropel(bool $bMakePersistent = false, bool $bDeep = false):\Model\Module\Module {
        echo "Module name {$this->getName()} " . PHP_EOL;

        $sName = $this->getName();
        if($this->getDatabase()->getCustom())
        {
            $sModuleName = "AdminModules\\Custom\\{$this->getDatabase()->getCustom()}\\{$this->getName()}";
        }
        else
        {
            $sModuleName = "AdminModules\\{$this->getDatabase()->getName()}";
        }


        $oModuleQuery = ModuleQuery::create()->filterByName($sModuleName);
        $oModule = $oModuleQuery->findOneOrCreate();
        $oModule->setTitle($this->getTitle());
        if($bMakePersistent)
        {
            $oModule->save();
        }
/*
        if($this->getTables()->count() && $bDeep)
        {
            /**
             * First store tables and columns
             * /
            foreach ($this->getTables() as $oTable)
            {
                $oTable->toPropel($bMakePersistent, true);
            }
        }
*/
        return $oModule;
    }

    function getTables():TableIterator
    {
        $oTableIterator = new TableIterator();

        foreach ($this->database->getTables() as $oTable)
        {
            // echo $oTable->getModuleName().' === ' . $this->getName() . ' ' . $oTable->getTitle() . PHP_EOL;
            if($oTable->getModuleName() === $this->getName())
            {
                $oTableIterator->append($oTable);
            }

        }
        return $oTableIterator;
    }

}
