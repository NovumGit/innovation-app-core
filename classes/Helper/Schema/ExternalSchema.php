<?php
namespace Helper\Schema;

use Hurah\Types\Type\Path;
use SimpleXMLElement;

class ExternalSchema
{
    private SimpleXMLElement $external_schema;

    function __construct(SimpleXMLElement $external_schema)
    {
        $this->external_schema = $external_schema;
    }
    function getFileName():string
    {
        $sFileName = (string) $this->external_schema->filename;
        return $sFileName;
    }
    function getPath():Path
    {
        return new Path($this->getFileName());
    }
}
