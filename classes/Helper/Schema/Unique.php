<?php
namespace Helper\Schema;

class Unique
{
    private $unique;

    function __construct(\SimpleXMLElement $unique)
    {
        $this->unique = $unique;
    }

    function getName():string
    {
        return (string)$this->unique->attributes()['name'];
    }

    /**
     * @return string[]
     */
    function getColumns():array
    {
        $aOut = [];
        foreach($this->unique->{'unique-column'} as $column)
        {
            $aOut[] = (string) $column['name'];
        }
        return $aOut;
    }

}
