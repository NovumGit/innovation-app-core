<?php

namespace Helper\Schema;

use Core\Utils;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Model\DataModel;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\DataModel\Model\UniqueFieldGroupFieldQuery;
use Model\System\DataModel\Model\UniqueFieldGroupQuery;

class Table
{
    private $table;
    private $database;

    function __construct(Database $table, Database $database)
    {
        $this->table = $table;
        $this->database = $database;
    }

    function getDatabase(): Database
    {
        return $this->database;
    }

    function getName()
    {
        return trim($this->table->attributes()->name);
    }

    function getShortDescription()
    {
        return trim($this->table->attributes()->description);
    }

    function getSkipCruds()
    {
        return (trim($this->table->attributes()->skipCruds) === 'true');
    }
    function getSkipControllers()
    {
        return (trim($this->table->attributes()->skipControllers) === 'true')   ;
    }
    function getSkipMenu()
    {
        return (trim($this->table->attributes()->skipMenu) === 'true')   ;
    }


    function getApiDescription()
    {
        return trim($this->table->attributes()->apiDesc);
    }

    function getModuleName()
    {
        return trim($this->table->attributes()->module);
    }

    function getModule(): ?Module
    {
        $aModules = $this->getDatabase()->getModules();
        foreach ($aModules as $oModule)
        {
            if ($oModule->getName() === $this->getModuleName())
            {
                return $oModule;
            }
        }
        return null;
    }

    function getTitle():string
    {
        return (string) $this->table->attributes()->title;
    }

    function getPhpName()
    {
        $sPhpName = (string) $this->table->attributes()->phpName;

        if(!$sPhpName)
        {
            $sPhpName = Utils::camelCase((string) $this->table->getName());
        }
        return $sPhpName;
    }

    function getApiExposed():bool
    {
        return (bool) $this->table->attributes()->apiExposed;
    }

    function getModelClass($bAddNamespace = false)
    {
        if ($bAddNamespace)
        {
            return $this->getModelNamespace() . '\\' . $this->getPhpName();
        }
        return $this->getPhpName();
    }

    function getQueryClass($bAddNamespace = false)
    {
        if ($bAddNamespace)
        {
            return $this->getModelNamespace() . '\\' . $this->getPhpName() . 'Query';
        }
        return $this->getPhpName() . 'Query';
    }

    function getMapClass($bAddNamespace = false)
    {
        if ($bAddNamespace)
        {
            return $this->getModelNamespace() . '\\Map\\' . $this->getPhpName() . 'TableMap';
        }
        return $this->getPhpName() . 'Map';
    }

    function getColumnArray($aFilter = [])
    {
        $aAllColumns = $this->getColumns($aFilter);
        $aReturn = [];
        foreach ($aAllColumns as $oColumn)
        {
            $sName = null;
            if ($oColumn->getPhpName())
            {
                $sName = $oColumn->getPhpName();
            } else
            {
                $sName = Utils::camelCase($oColumn->getName());
            }

            if (in_array($sName, $aFilter))
            {
                continue;
            }

            $aReturn[] = $sName;
        }
        return $aReturn;
    }

    function getModelNamespace()
    {
        return '\\' . $this->table->attributes()->namespace;
    }

    function getCrudRoot()
    {
        $sCrudNamespace = (string) $this->table->attributes()->crudRoot;
        if($sCrudNamespace)
        {
            return $sCrudNamespace;
        }
        return $this->database->getCrudRoot();
    }
    function getCrudNamespace()
    {
        $sCrudNamespace = (string) $this->table->attributes()->crudNamespace;
        if($sCrudNamespace)
        {
            return $sCrudNamespace;
        }
        return (string) $this->database->getCrudNamespace();
    }

    function getCrudDir()
    {
        return $this->database->getCrudRoot();
    }

    /**
     * return ColumnIterator
     */
    function getColumns(array $aFilter = null): ColumnIterator
    {
        $oColumnIterator = new ColumnIterator();

        foreach ($this->table->column as $x => $column)
        {
            if (is_array($aFilter) && in_array((string)$column->getName(), $aFilter))
            {
                continue;
            }

            $oColumnIterator->append(new Column($column));
        }

        return $oColumnIterator;
    }

    function getForeignKeys(): ForeignKeyIterator
    {
        $oFkIterator = new ForeignKeyIterator();

        foreach ($this->table->{'foreign-key'} as $x => $foreignKey)
        {
            $oFkIterator->append(new ForeignKey($foreignKey));
        }
        return $oFkIterator;
    }



    function getUnique(): UniqueIterator
    {
        $oFkIterator = new UniqueIterator();

        foreach ($this->table->unique as $x => $unique)
        {
            $oFkIterator->append(new Unique($unique));
        }
        return $oFkIterator;
    }

    public function toPropel(bool $bMakePersistent, bool $bDeep)
    {
        $oDataModelQuery = DataModelQuery::create();
        $oDataModelQuery->filterByName($this->getName());
        $oDataModel = $oDataModelQuery->findOneOrCreate();
        $oDataModel->setNamespaceName($this->getModelNamespace());
        $oDataModel->setPhpName($this->getPhpName());

        $oDataModel->setTitle($this->getTitle());

        $oDataModel->setApiExposed($this->getApiExposed());
        $oDataModel->setApiDescription($this->getApiDescription());
        $oDataModel->setIsPersistent(true);
        $oDataModel->setIsUiComponent(true);

        $oDataModel->save();

        if(!$bDeep)
        {
            return $oDataModel;
        }

        if($this->getColumns()->count())
        {
            $aColumnIterator = $this->getColumns();
            foreach ($aColumnIterator as $oColumn)
            {
                echo "Creating column {$oColumn->getName()}" . PHP_EOL;
                $oDataField = $oColumn->toPropel($bMakePersistent, $oDataModel, $this);
                $oDataModel->addDataField($oDataField);
            }
        }

        echo "Delete model uniques" . PHP_EOL;
        $oUniqueFieldGroupQuery = UniqueFieldGroupQuery::create();
        $oUniqueFieldGroupQuery->filterByDataModelId($oDataModel->getId());

        if($this->getUnique())
        {

            $aUniqueBlocks = $this->getUnique();
            foreach($aUniqueBlocks as $aUniqueBlock)
            {

                $oUniqueFieldGroupQuery = UniqueFieldGroupQuery::create();
                $oUniqueFieldGroupQuery->filterByDataModelId($oDataModel->getId());
                $oUniqueFieldGroupQuery->filterByName($aUniqueBlock->getName());
                $oUniqueFieldGroup = $oUniqueFieldGroupQuery->findOneOrCreate();
                $oUniqueFieldGroup->save();

                $i = 0;
                foreach ($aUniqueBlock->getColumns() as $sFieldName)
                {
                    echo "Adding unique {$aUniqueBlock->getName()} - $sFieldName" . PHP_EOL;
                    $oDataFieldQuery = DataFieldQuery::create();
                    $oDataFieldQuery->filterByDataModelId($oDataModel->getId());
                    $oDataFieldQuery->filterByName($sFieldName);
                    $oDataField = $oDataFieldQuery->findOneOrCreate();

                    $oUniqueFieldGroupFieldQuery = UniqueFieldGroupFieldQuery::create();
                    $oUniqueFieldGroupFieldQuery->filterByUniqueFieldGroupId($oUniqueFieldGroup->getId());
                    $oUniqueFieldGroupFieldQuery->filterByDataFieldId($oDataField->getId());
                    $oUniqueFieldGroupField = $oUniqueFieldGroupFieldQuery->findOneOrCreate();
                    $oUniqueFieldGroupField->setSorting($i);
                    ++$i;
                    $oUniqueFieldGroupField->save();
                }
            }
        }

        return $oDataModel;
    }
}

