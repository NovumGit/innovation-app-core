<?php
namespace Helper\Schema;

use Core\Utils;
use Exception;
use Exception\LogicException;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Model\DataModel;

class Column
{
    private $column;
    function __construct(\SimpleXMLElement $column)
    {
        $this->column = $column;
    }
    function getClassName()
    {
        if($this->getPhpName())
        {
            return $this->getPhpName();
        }
        return Utils::camelCase($this->getName());
    }
    function getLookupVisible() : string
    {
        return (string) $this->column->attributes()->lookupVisible;
    }

    function getLookupsVisibleColumn():?array
    {
        $sValue = (string)$this->column->attributes()->lookupVisible;
        if(strpos($sValue, 'http') === 0)
        {
            preg_match_all('/{(.+)}/', $sValue, $aMatches);
            $sUrl = str_replace($aMatches[0][0], '', $sValue);
            $aOut = [
                'url' => $sUrl,
                'value' => $aMatches[1][0]
            ];
            return $aOut;

        }
        else if(strpos($sValue, '.'))
        {
            $aValue = explode('.', $sValue);
            $aOut = [
                'full_class' => $aValue[0],
                'method' => $aValue[1],
            ];
            if (preg_match('@\\\\([\w]+)$@', $aValue[0], $matches))
            {
                // Class zonder namespace
                $aOut['class'] = $matches[1];
            }
            return $aOut;
        }
        if(!is_array($sValue) && $sValue)
        {
            throw new LogicException("Lookup values of " . $this->getPhpName() . ' ' .$this->getName() . " must be of type array or string, got $sValue");
        }
        return empty($sValue) ? null : $sValue;
    }
    function getName()
    {
        return (string)$this->column->attributes()->name;
    }
    function getType():string
    {
        $sType = trim((string) $this->column->attributes()->type);
        if(empty($sType))
        {
            return 'VARCHAR';
        }
        return  $sType ?? 'VARCHAR';
    }
    function getPlaceholder()
    {
        return (string)$this->column->attributes()->placeholder;
    }
    function getDefaultValue():string
    {
        return (string)$this->column->attributes()->default;
    }
    function getPhpName()
    {
        return (string)$this->column->attributes()->phpName;
    }
    function getTitle():string
    {
        return (string)$this->column->attributes()->title;
    }
    function getRequired():bool
    {
        return  ((string) $this->column->attributes()->required) == 'true';
    }
    function getSize():?int
    {
        return (int) $this->column->attributes()->size ?? null;
    }
    function getIsPrimaryKey():bool
    {
        return  ((string) $this->column->attributes()->primaryKey) == 'true';
    }
    function getIsAutoIncrement():bool
    {
        return  ((string) $this->column->attributes()->autoIncrement) == 'true';
    }
    function getForm()
    {
        return (string) $this->column->attributes()->form ?? 'STRING';
    }
    function getIcon()
    {
        return (string) $this->column->attributes()->icon ?? 'tag';
    }

    public function toPropel(bool $bMakePersistent, DataModel $oModel, Table $oTable):DataField
    {
        $oDataFieldQuery = DataFieldQuery::create();
        $oDataFieldQuery->filterByDataModelId($oModel->getId());
        $oDataFieldQuery->filterByName($this->getName());
        $oDataField = $oDataFieldQuery->findOneOrCreate();


        if($this->getPhpName())
        {
            $oDataField->setPhpName($this->getPhpName());
        }
        /*
        else
        {
            $oDataField->setPhpName(Utils::camelCase($this->getName()));
        }
        */
        try
        {
            $oDataField->setDataTypeByName($this->getType());
        }
        catch (Exception $e)
        {
            $oDataField->setDataTypeId(null);
        }

        $oDataField->setLabel($this->getTitle());
        $oDataField->setIconByName($this->getIcon());

        try
        {
            $oDataField->setFormFieldTypeByName($this->getForm());
        }
        catch (Exception $e)
        {
            $oDataField->setFormFieldTypeId(null);
        }

        if(!empty($sLookups = $this->getLookupVisible()))
        {
            $oDataField->setFormFieldLookups($sLookups);
        }
        else
        {
            $oDataField->setFormFieldLookups(null);
        }

        $oDataField->setDefaultValue($this->getDefaultValue());
        $oDataField->setSize($this->getSize());
        $oDataField->setAutoIncrement($this->getIsAutoIncrement());
        $oDataField->setRequired($this->getRequired());
        // $oDataField->setIsUnique(false);
        $oDataField->setDataModelId($oModel->getId());
        $oDataField->setIsPrimaryKey($this->getIsPrimaryKey());

        /*
        foreach($oTable->getUnique() as $oUnique)
        {
            if($oUnique->getColumnName() === $this->getName())
            {
                echo "Making column {$this->getName()} Unique"  . PHP_EOL;
                $oDataField->setIsUnique(true);
            }
        }
        */

        if($bMakePersistent)
        {
            $oDataField->save();
        }

        return $oDataField;
        // $oDataField->setIsUnique($this->get());

    }

}
