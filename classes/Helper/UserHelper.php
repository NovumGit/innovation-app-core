<?php
namespace Helper;

use Exception\LogicException;
use Facebook\GraphNodes\GraphUser;
use Model\Account\FacebookUser;
use Model\Account\FacebookUserQuery;
use Model\Account\User;
use Model\Account\UserQuery;

class UserHelper{


    static function storeFromFacebook(GraphUser $oGraphUser)
    {
        $sGraphUserEmail = $oGraphUser->getEmail();

        if(!$sGraphUserEmail)
        {
            throw new LogicException("Graph user doest not contain an e-mail address, this would lead to problems so we are throwing this error, sorry!");
        }

        $oUserQuery = UserQuery::create();
        $oUser = $oUserQuery->filterByEmail($oGraphUser->getEmail())->filterBySourceNetwork('facebook')->findOne();

        if(!$oUser instanceof User)
        {
            $oUser = new User();
            $oUser->setCreated(time());
        }
        else
        {
            $oUser->setModified(time());
        }
        $oUser->setFirstName($oGraphUser->getFirstName());
        $oUser->setLastName($oGraphUser->getLastName());
        $oUser->setEmail($oGraphUser->getEmail());
        $oUser->setSourceNetwork('facebook');
        $oUser->save();


        $oFacebookUserQuery = FacebookUserQuery::create();
        $oFacebookUser = $oFacebookUserQuery->findOneByFacebookId($oGraphUser->getId());

        if(!$oFacebookUser instanceof FacebookUser)
        {
            $oFacebookUser = new FacebookUser();
            $oFacebookUser->setCreated(time());
            $oFacebookUser->setFacebookId($oGraphUser->getId());
            $oFacebookUser->setUserId($oUser->getId());
        }
        else
        {
            $oFacebookUser->setModified(time());
        }

        $oFacebookUser->setEmail($oGraphUser->getEmail());
        $oFacebookUser->setFirstName($oGraphUser->getFirstName());
        $oFacebookUser->setLastName($oGraphUser->getLastName());
        $oFacebookUser->setGender($oGraphUser->getGender());
        $oFacebookUser->setLink($oGraphUser->getLink());
        $oFacebookUser->save();

        return $oUser;

    }

}