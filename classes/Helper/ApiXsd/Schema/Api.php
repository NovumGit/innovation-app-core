<?php
namespace Helper\ApiXsd\Schema;

use \SimpleXMLElement;

final class Api extends SimpleXMLElement
{
    private $title;
    private $description;
    private $api_version;
    private $type;
    private $organisation;
    private $contacts;
    private $api_dir;
    private $nlx_local_in;
    private $nlx_local_out;
    private $endpoint_url;
    private $api_namespace;
    private $documentation_url;
    private $authorization_model;
    private $ca_cert_path;
    private $servers;
    private $whitelist;

    final public function getTitle(): string
    {
        return (string)$this->title;
    }

    final public function getDescription(): string
    {
        return (string)$this->description;
    }
    final public function getApiVersion(): string
    {
        return (string)$this->api_version;
    }
    final public function getOrganisation(): string
    {
        return (string)$this->organisation;
    }

    final public function getApi_dir(): string
    {
        return (string)$this->api_dir;
    }

    final public function getEndpoint_url($bStripProtocol = false): string
    {
        $sEndpointUrl = (string)$this->endpoint_url;
        if($bStripProtocol)
        {
            $sEndpointUrl = str_replace('http://', '', $sEndpointUrl);
            $sEndpointUrl = str_replace('https://', '', $sEndpointUrl);
        }

        return $sEndpointUrl;
    }

    final public function getApiNamespace(): string
    {
        return (string)$this->api_namespace;
    }
    
    final public function getNlxLocalIn(): string
    {
        return (string)$this->nlx_local_in;
    }
    
    final public function getNlxLocalOut(): string
    {
        return (string)$this->nlx_local_out;
    }

    final public function getDocumentation_url(): string
    {
        return (string)$this->documentation_url;
    }

    final public function getAuthorization_model(): string
    {
        return (string)$this->authorization_model;
    }
/*
    final public function getName() :string
    {
        return $this->name;
    }
    final public function getEmail() :string
    {
        return $this->email;
    }
    final public function getType() :string
    {
        return $this->type;
    }
*/
    /**
     * @return array
     */
    final public function getServers(): array
    {
        $aServers = [];
        foreach($this->servers->server as $server)
        {
            $aServers[] = [
                "description" => (string) $server->attributes()['description'],
                "url" =>  (string) $server->attributes()['url']
            ];
        }
        return $aServers;
    }
    /**
     * @return array
     */
    final public function getContactArray(): array
    {
        $aContacts = [];
        foreach($this->contacts->contact as $contact)
        {
            $aContacts[] = [
                "email" => $contact->attributes()->email,
                "type" => $contact->attributes()->type,
                "name" => $contact->attributes()->name,
            ];
        }
        return $aContacts;
    }

    /**
     * @param $sFilterType [TECHNICAL, SUPPORT]
     * @return array
     */
    final public function getContacts(string $sFilterType = null): array
    {
        $aContacts = [];
        foreach($this->contacts->contact as $contact)
        {
            if($sFilterType == null)
            {
                $aContacts[] = $contact;
            }
            if($sFilterType == $contact->attributes()->type)
            {
                $aContacts[] = $contact;
            }
        }
        return $aContacts;
    }
    final public function getSupport_contact(): ?string
    {
        $aContact = $this->getContacts('SUPPORT')[0];
        return (string)$aContact->attributes()['email'];
    }
    final public function getTech_contact(): ?string
    {
        $aContact = $this->getContacts('TECHNICAL')[0];
        print_r((string)$aContact->attributes()['email']);
        return (string)$aContact->attributes()['email'];
    }

    final public function getAuthorization_whitelist(): string
    {
        return (string)$this->whitelist;
    }

    final public function getCa_cert_path(): string
    {
        return (string)$this->ca_cert_path;
    }
}