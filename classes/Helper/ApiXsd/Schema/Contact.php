<?php
namespace Helper\ApiXsd\Schema;

use \SimpleXMLElement;

final class Contact extends SimpleXMLElement
{

    final public function getName() :string
    {
        return $this->name;
    }
    final public function getEmail() :string
    {
        return $this->email;
    }
    final public function getType() :string
    {
        return $this->type;
    }

}