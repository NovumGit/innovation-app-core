<?php
namespace Helper;

use Crud\CrudViewManager;
use Model\Setting\CrudManager\CrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonQuery;

class OverviewButtonHelper{

    static function deleteButton($sCrudClass, $iTabId, $iButtonId)
    {
        $oCrudView = CrudViewManager::getView(new $sCrudClass, $iTabId);

        $oCrudViewButtonQuery = CrudViewButtonQuery::create();
        $oCrudViewButtonQuery->filterByCrudViewId($oCrudView->getId());
        $oCrudViewButtonQuery->filterById($iButtonId);
        $oCrudViewButtonQuery->delete();
    }

    static function createBlankButton($sCrudClass, $iTabId, $sButtonTitle)
    {
        $oCrudView = CrudViewManager::getView(new $sCrudClass, $iTabId);

        // We schuiven de sorting van alle overige buttons even een stukje op zodat deze button de eerste wordt.
        $aButtons = $oCrudView->getCrudViewButtons();
        if(!$aButtons->isEmpty())
        {
            foreach($aButtons as $oButton)
            {
                $iSorting = $oButton->getSorting();
                $oButton->setSorting(++$iSorting);
                $oButton->save();
            }
        }
        $oNewCrudViewButton = new CrudViewButton();
        $oNewCrudViewButton->setTitle($sButtonTitle);
        $oNewCrudViewButton->setSorting(0);
        $oNewCrudViewButton->setCrudViewId($oCrudView->getId());
        $oNewCrudViewButton->save();
    }
    static function getViewButtons($iTabId)
    {
        $oCrudViewButtonQuery = CrudViewButtonQuery::create();
        $oCrudViewButtonQuery->filterByCrudViewId($iTabId);
        $aCrudOverviewButtons = $oCrudViewButtonQuery->orderBySorting();

        return $aCrudOverviewButtons;
    }
    public static function getButtons($iCrudViewId)
    {
        $oCrudViewButtonQuery = CrudViewButtonQuery::create();
        $oCrudViewButtonQuery->filterByCrudViewId($iCrudViewId);
        $aCrudEditorButtons =  $oCrudViewButtonQuery->orderBySorting()->find();
        return $aCrudEditorButtons;
    }
}