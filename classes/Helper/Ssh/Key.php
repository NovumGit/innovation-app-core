<?php
namespace Helper\Ssh;

use Hurah\Types\Type\Path;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class Key {

    static function create(Path $oKeyFile, ?OutputInterface $output)
    {
        $finder = new ExecutableFinder();
        $keyGenBin = $finder->find('ssh-keygen');

        $aCommand = [
            $keyGenBin, '-t', 'rsa', "-f", "{$oKeyFile}", '-q', '-P', '',
        ];

        $output->write('<comment>Running:</comment> ' . join(' ', $aCommand));

        $oCommand = new Process($aCommand, $oKeyFile->dirname());

        $result = $oCommand->run();

        if ($result === 0) {
            $output->writeln(" <info>success</info>");
        } else {
            $output->writeln(" <error>failed with exit code {$result}</error>");
            $output->writeln($oCommand->getErrorOutput());
        }
    }
}
