<?php
namespace Helper;

use Core\Config;
use Model\Category\Category;
use Model\Category\CategoryQuery;
use Model\CustomerQuery;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Model\Sale\SaleOrderItem;
use Model\Sale\OrderItemProduct;
use Model\Setting\MasterTable\Base\ProductMaterialQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\ProductMaterial;

class SaleHelper{
	static function addProductToOrder($iProductId, $iOrderId, $fSalePrice, $iQuantity = 1, Language $oLanguage = null)
	{
		$oProduct = CustomerQuery::create()->findOneById($iProductId);
        if($oProduct->getDerrivedFromId())
        {
            $oSpecsProduct = CustomerQuery::create()->findOneById($oProduct->getDerrivedFromId());
        }
        else
        {
            $oSpecsProduct = $oProduct;
        }
        if($oLanguage instanceof Language)
        {
            $oProductTranslation = ProductTranslationQuery::create()->filterByProductId($oSpecsProduct->getId())->filterByLanguageId($oLanguage->getId())->findOne();
            if($oProductTranslation instanceof ProductTranslation)
            {
                $sDescription = $oProductTranslation->getTitle();
            }
        }
        if(empty($sDescription))
        {
            $sDescription = $oSpecsProduct->getTitle();
        }
		$oSaleOrderItem = new SaleOrderItem();
		$oSaleOrderItem->setCreatedOn(date(Config::getDateTimeFormat()));
		$oSaleOrderItem->setSaleOrderId($iOrderId);
        $oSaleOrderItem->setSingleItemWeight($oProduct->getWeight());
        $oSaleOrderItem->setProductNumber($oProduct->getNumber());
        $oSaleOrderItem->setDescription($sDescription. ' '.$oProduct->getQuantityPerPack());
		$oSaleOrderItem->setQuantity($iQuantity);
		$oSaleOrderItem->setPrice($fSalePrice);
		$oSaleOrderItem->setVatPercentage($oProduct->getVat()->getPercentage());
		$oSaleOrderItem->save();

		$oSaleOrderItemProduct = new OrderItemProduct();
		$oSaleOrderItemProduct->setSaleOrderItemId($oSaleOrderItem->getId());
		$oSaleOrderItemProduct->setProductId($oProduct->getId());
		$oSaleOrderItemProduct->setSalePrice($fSalePrice);
		$oSaleOrderItemProduct->setWidth($oProduct->getWidth());
		$oSaleOrderItemProduct->setHeight($oProduct->getHeight());
		$oSaleOrderItemProduct->setThickness($oProduct->getThickness());

        $oProductCategory = CategoryQuery::create()->findOneById($oProduct->getCategoryId());
        if($oProductCategory instanceof Category)
        {
            $oSaleOrderItemProduct->setCategoryName($oProductCategory->getName());
        }
        $oProductMaterial = ProductMaterialQuery::create()->findOneById($oProduct->getMaterialId());
		if($oProductMaterial instanceof ProductMaterial)
		{
			$oSaleOrderItemProduct->setMaterialName($oProductMaterial->getName());
		}
		$oSaleOrderItemProduct->save();
	}
}