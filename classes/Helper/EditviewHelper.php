<?php

namespace Helper;

use Core\Cfg;
use Core\Debug;
use Core\Header;
use Core\Mime\JsonMime;
use Crud\BaseManager;
use Model\Setting\CrudManager\Base\CrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;
use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockQuery;
use Model\Setting\CrudManager\CrudEditorQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use ReflectionClass;

class EditviewHelper {

    /**
     * @param BaseManager $oCrudManager
     * @param $sName
     * @param $sTitle
     * @return CrudEditor
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function loadEditorByName(BaseManager $oCrudManager, $sName, $sTitle) {
        $sCrudManagerName = get_class($oCrudManager);
        $oCrudConfigQuery = CrudConfigQuery::create();
        $oCrudConfig = $oCrudConfigQuery->findOneByManagerName($sCrudManagerName);

        if (!$oCrudConfig instanceof CrudConfig) {
            $oCrudConfig = new CrudConfig();
            $oCrudConfig->setManagerName($sCrudManagerName);
            $oCrudConfig->save();
        }

        $oCrudEditorQuery = CrudEditorQuery::create();
        $oCrudEditorQuery->filterByName($sName);
        $oCrudEditorQuery->filterByCrudConfigId($oCrudConfig->getId());
        $oCrudEditor = $oCrudEditorQuery->findOne();

        if (!$oCrudEditor instanceof CrudEditor) {
            $oCrudEditor = new CrudEditor();
            $oCrudEditor->setName($sName);
            $oCrudEditor->setCrudConfigId($oCrudConfig->getId());
        }
        $oCrudEditor->setTitle($sTitle);
        $oCrudEditor->save();

        $aCrudEditorBlocks = self::getEditorBlocks($oCrudEditor->getId());

        if ($aCrudEditorBlocks->isEmpty()) {
            $oCrudEditorBlock = new CrudEditorBlock();
            $oCrudEditorBlock->setCrudEditorId($oCrudEditor->getId());
            $oCrudEditorBlock->setTitle($sTitle);
            $oCrudEditorBlock->setWidth(12);
            $oCrudEditorBlock->setSorting(0);
            $oCrudEditorBlock->save();

            $aFields = $oCrudManager->getDefaultEditFields();
            $iSorting = 0;
            if ($aFields) {
                foreach ($aFields as $sFieldName) {
                    $oReflectionClass = new ReflectionClass($oCrudManager);
                    $oCrudEditorBlockField = new CrudEditorBlockField();
                    $oCrudEditorBlockField->setWidth(12);
                    $oCrudEditorBlockField->setCrudEditorBlockId($oCrudEditorBlock->getId());
                    $oCrudEditorBlockField->setSorting($iSorting);
                    $oCrudEditorBlockField->setField($oReflectionClass->getNamespaceName() . '\\Field\\' . $sFieldName);
                    $oCrudEditorBlockField->setReadonlyField(false);
                    $oCrudEditorBlockField->save();
                    $iSorting++;
                }
            }
        }
        return $oCrudEditor;
    }

    static function getEditorBlocks($iCrudEditorId) {
        $oCrudEditorBlockQuery = CrudEditorBlockQuery::create();
        $oCrudEditorBlockQuery->filterByCrudEditorId($iCrudEditorId);
        $oCrudEditorBlockQuery->orderBySorting();
        $aCrudEditorBlocks = $oCrudEditorBlockQuery->find();
        return $aCrudEditorBlocks;
    }

    static function getCrudfieldsInUse(CrudEditor $oCrudEditor) {
        $aOut = [];
        $aBlocks = $oCrudEditor->getCrudEditorBlocks();
        foreach ($aBlocks as $oBlock) {
            foreach ($oBlock->getFieldObjects() as $oField) {

                $aOut[] = $oField['classname'];
            }
        }
        return $aOut;
    }

    private static function distillFieldIds(array $aFields) {
        $aDistilled = [];
        if (is_iterable($aFields)) {
            foreach ($aFields as $aFieldSettings) {
                if ($aFieldSettings['iBlockFieldId'] === 'new') {
                    continue;
                }
                $aDistilled[] = $aFieldSettings['iBlockFieldId'];
            }
        }
        return $aDistilled;
    }
    private static function distillBlockIds(array $aPostedData) {
        $aDistilled = [];
        Debug::pre_r($aPostedData);
        if (is_iterable($aPostedData)) {
            foreach ($aPostedData as $aBlockSettings) {
                var_dump($aBlockSettings);
                if ($aBlockSettings['iEditorBlockId'] !== 'new') {
                    $aDistilled[] = $aBlockSettings['iEditorBlockId'];
                }
            }
        }
        return $aDistilled;
    }
    private static function getCrudEditorBlockQuery(int $iCrudEditorId):CrudEditorBlockQuery
    {
        $oCrudEditorBlockQuery = CrudEditorBlockQuery::create();
        $oCrudEditorBlockQuery->filterByCrudEditorId($iCrudEditorId);
        return $oCrudEditorBlockQuery;
    }
    private static function getCrudEditorBlockFieldQuery(int $iCrudEditorId, int $iCrudEditorBlockId):CrudEditorBlockFieldQuery
    {
        $oCrudEditorBlock = self::getCrudEditorBlock($iCrudEditorId, $iCrudEditorBlockId);
        $oCrudEditorBlockFieldQuery = CrudEditorBlockFieldQuery::create();
        $oCrudEditorBlockFieldQuery->filterByCrudEditorBlock($oCrudEditorBlock);
        return $oCrudEditorBlockFieldQuery;
    }

    private static function getCrudEditorBlock(int $iCrudEditorId, $iCrudEditorBlockId):CrudEditorBlock
    {
        $oCrudEditorBlockQuery = self::getCrudEditorBlockQuery($iCrudEditorId);
        $oCrudEditorBlockQuery->filterById($iCrudEditorBlockId);
        return $oCrudEditorBlockQuery->findOne();
    }
    private static function removeClearedBlocks(int $iCrudEditorId, array $aPostedData) :void {
        $aBlocksToKeep = self::distillBlockIds($aPostedData);
        $oCrudEditorBlockQuery = self::getCrudEditorBlockQuery($iCrudEditorId);
        $oCrudEditorBlockQuery->filterById($aBlocksToKeep, Criteria::NOT_IN);
        $oCrudEditorBlockQuery->delete();
    }
    private static function reOrderBlocks(int $iCrudEditorId, array $aPostedData)
    {
        if(is_iterable($aPostedData))
        {
            foreach ($aPostedData as $aBlock)
            {
                if($aBlock['iEditorBlockId'] === 'new')
                {
                    continue;
                }
                $oCrudEditorBlockQuery = self::getCrudEditorBlockQuery($iCrudEditorId);
                $oCrudEditorBlock = $oCrudEditorBlockQuery->filterById($aBlock['iEditorBlockId']);
                $oCrudEditor = $oCrudEditorBlock->findOne();
                $oCrudEditor->setSorting($aBlock['sorting']);
                $oCrudEditor->save();

            }
        }
    }

    private static function removeClearedFields(int $iCrudEditorId, $iCrudEditorBlockId, $aFields)
    {
        if($iCrudEditorBlockId !== 'new')
        {
            $aFieldIds = self::distillFieldIds($aFields);
            $oCrudEditorBlockFieldQuery = self::getCrudEditorBlockFieldQuery($iCrudEditorId, $iCrudEditorBlockId);
            $oCrudEditorBlockFieldQuery->filterById($aFieldIds, Criteria::NOT_IN);
            $oCrudEditorBlockFieldQuery->delete();
        }
    }
    private static function getCrudEditorBlockField(int $iBlockFieldId):CrudEditorBlockField
    {
        $oCrudEditorBlockFieldQuery = CrudEditorBlockFieldQuery::create();
        return $oCrudEditorBlockFieldQuery->findOneById($iBlockFieldId);
    }
    private static function reOrderFields($iCrudEditorBlockId, array $aFields):void
    {
        if($iCrudEditorBlockId === 'new')
        {
            return;
        }

        if(is_iterable($aFields))
        {
            foreach ($aFields as $aField)
            {
                if ($aField['iBlockFieldId'] === 'new') {
                    continue;
                }
                $oField = self::getCrudEditorBlockField($aField['iBlockFieldId']);
                $oField->setCrudEditorBlockId($iCrudEditorBlockId);
                $oField->setSorting($aField['sorting']);
                $oField->setWidth($aField['width']);
                $oField->setReadonlyField($aField['read_only']);
                $oField->save();
            }
        }
   }
    private static function createBlock(int $iCrudEditorId, array $aBlock):int  {
       $oCrudEditorBlock = new CrudEditorBlock();
       $oCrudEditorBlock->setCrudEditorId($iCrudEditorId);
       $oCrudEditorBlock->setSorting($aBlock['sorting']);
       $oCrudEditorBlock->setTitle($aBlock['sEditBlockTitle'] ?? 'Eigenschappen');
       $oCrudEditorBlock->setWidth($aCrudEditorBlock['width'] ?? 12);
       $oCrudEditorBlock->save();
       return $oCrudEditorBlock->getId();
   }
    private static function createNewBlocks(int $iCrudEditorId, array $aPostedData) {
        if(is_iterable($aPostedData))
        {
            foreach ($aPostedData as &$aBlock)
            {
                if($aBlock['iEditorBlockId'] !== 'new')
                {
                    continue;
                }
                if(empty($aBlock['fields']))
                {
                    continue;
                }
                $aBlock['iEditorBlockId'] = self::createBlock($iCrudEditorId, $aBlock);
            }
        }
       return $aPostedData;
   }

    private static function createNewFields($iCrudEditorBlockId, array $aFields, string $sCrudNamespace):array
    {
        if(is_iterable($aFields))
        {
            foreach ($aFields as &$aField)
            {
                if ($aField['iBlockFieldId'] !== 'new') {
                    continue;
                }
                if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\" . $aField['fieldName'])) {
                    $sFullFieldname = $sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\" . $aField['fieldName'];
                } else {
                    $sFullFieldname = $sCrudNamespace . "\\Field\\" . $aField['fieldName'];
                }
                $oCrudEditorBlockField = new CrudEditorBlockField();
                $oCrudEditorBlockField->setCrudEditorBlockId($iCrudEditorBlockId);
                $oCrudEditorBlockField->setSorting($aField['sorting']);
                $oCrudEditorBlockField->setField(substr($sFullFieldname, 1, strlen($sFullFieldname)));
                $oCrudEditorBlockField->setWidth($aField['width'] ?? 12);
                $oCrudEditorBlockField->setReadonlyField($aField['read_only'] ?? true);
                $oCrudEditorBlockField->save();
                $aField['iBlockFieldId'] = $oCrudEditorBlockField->getId();
            }
        }
        return $aFields;
    }
    public static function storeLayout($iCrudEditorId, $aPostedData, $sCrudNamespace):array {

        self::removeClearedBlocks($iCrudEditorId, $aPostedData ?? []);
        self::reOrderBlocks($iCrudEditorId, $aPostedData ?? []);

        // Creates new blocks and replaces iEditorBlockId=new with the actual id of the block
        $aPostedData = self::createNewBlocks($iCrudEditorId, $aPostedData ?? []);

        foreach ($aPostedData as $aBlock)
        {
            self::removeClearedFields($iCrudEditorId, $aBlock['iEditorBlockId'], $aBlock['fields']);
            self::reOrderFields($aBlock['iEditorBlockId'], $aBlock['fields']);

            // Creates new fields and replaces iBlockFieldId=new with the actual id of the field
            $aBlock['fields'] = self::createNewFields($aBlock['iEditorBlockId'], $aBlock['fields'], $sCrudNamespace);
        }
        return $aPostedData;
    }
}
