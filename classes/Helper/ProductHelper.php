<?php
namespace Helper;

use Core\Cfg;
use Exception\LogicException;
use Model\Product;
use Model\Product\ProductTag;
use Model\Product\ProductTagQuery;
use Model\Product\RelatedProduct;
use Model\Product\RelatedProductQuery;
// use Model\Product\Tag\TagQuery;
// use Model\Product\Tag\Tag;

class ProductHelper{
/*
    static function addTagIfNotExists(Product $oProduct, $sTag)
    {
        $oTagQuery = TagQuery::create();
        $oTag = $oTagQuery->findOneByTag($sTag);
        if(!$oTag instanceof Tag)
        {
            $oTag = new Tag();
            $oTag->setTag($sTag);
            $oTag->save();
        }

        $oProductTagQuery = new ProductTagQuery();
        $oProductTagQuery->filterByTagId($oTag->getId());
        $oProductTagQuery->filterByProductId($oProduct->getId());
        $oProductTag = $oProductTagQuery->findOne();

        if(!$oProductTag instanceof ProductTag)
        {
            $oProductTag = new ProductTag();
            $oProductTag->setTagId($oTag->getId());
            $oProductTag->setProductId($oProduct->getId());
            $oProductTag->save();
        }
    }
*/
/*
    static function addRelatedIfNotExists(Product $oProduct, $iRelatedId)
    {
        $oRelatedProductQuery = RelatedProductQuery::create();
        $oRelatedProduct = $oRelatedProductQuery->findByOtherProductId($iRelatedId);
        if(!$oRelatedProduct instanceof RelatedProduct)
        {
            $oRelatedProduct = new RelatedProduct();
            $oRelatedProduct->setProductId($oProduct->getId());
            $oRelatedProduct->setOtherProductId($iRelatedId);
            $oRelatedProduct->save();
        }
    }
*/
    static function getProductDataDir(Product $oProduct)
    {
        if(!is_dir(Cfg::get('DATA_DIR').'/product/'))
        {
            mkdir(Cfg::get('DATA_DIR').'/product/');
        }
        return Cfg::get('DATA_DIR').'/product/'.$oProduct->getId();
    }

    static function getProductImageDir(Product $oProduct)
    {
        $sDir = self::getProductDataDir($oProduct).'/images/';

        if(!is_dir($sDir))
        {
            mkdir($sDir);
        }

       return $sDir;
    }

    static function makeDirectoryStructure(Product $oProduct)
    {
        $sDataDir = Config::getPublicDataDir();

        if(!is_dir($sDataDir))
        {
            throw new LogicException("De data directory bestaat niet: $sDataDir.");
        }
        if(!is_writeable($sDataDir))
        {
            throw new LogicException("De data directory is read-only: $sDataDir.");
        }

        $sProductFileDir = self::getProductDataDir($oProduct);

        if(!is_dir($sProductFileDir))
        {
            mkdir($sProductFileDir);
        }
        if(!is_writeable($sProductFileDir))
        {
            throw new LogicException("De product data directory is niet beschrijfbaar: $sProductFileDir.");
        }

        $sProductImageDir = self::getProductImageDir($oProduct);

        if(!is_dir($sProductImageDir))
        {
            mkdir($sProductImageDir);
        }
        if(!is_writeable($sProductImageDir))
        {
            throw new LogicException("De product image directory is niet beschrijfbaar: $sProductImageDir.");
        }
    }

}
