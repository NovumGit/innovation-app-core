<?php
namespace Helper;

use Model\Category\Base\CategoryQuery;
use Model\Category\ProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery;

class ProductMultiCategoryHelper
{
    static function getTree($iCategoryParentId = null, $iProductId = null)
    {
        $aOut = [];
        $aCategories = CategoryQuery::create()->filterByParentId($iCategoryParentId)->find();
        if(!$aCategories->isEmpty())
        {
            foreach($aCategories as $oCategory)
            {

                $sAttributes = '';
                $oProductMultiCategoryQuery = ProductMultiCategoryQuery::create();
                $oProductMultiCategoryQuery->findOneByCategoryId($oCategory->getId());
                $oProductMultiCategoryQuery->findOneByProductId($iProductId);
                $oProductMultiCategory = $oProductMultiCategoryQuery->findOne();

                if($oProductMultiCategory instanceof ProductMultiCategory)
                {
                    $sAttributes .= 'class="jstree-open" data-jstree=\'{"opened":true,"selected":true}\'';

                }

                $aOut[] = "                <li $sAttributes id=\"{$oCategory->getId()}\">";
                $aOut[] = '                             '.$oCategory->getName();

                $oCategoryQuery = CategoryQuery::create();
                $aCategories = $oCategoryQuery->findByParentId($oCategory->getId());

                if(!$aCategories->isEmpty())
                {
                    $aOut[] = '                             <ul>';
                    $aOut[] = '                             ' . self::getTree($oCategory->getId(), $iProductId);
                    $aOut[] = '                             </ul>';
                }

                $aOut[] = '                </li>';
            }
        }
        return join(PHP_EOL, $aOut);
    }
}