<?php
namespace Helper;
use Model\Category\CategoryQuery;

class CategoryTreeEditorHelper
{
    static function getTree($iCategoryParentId = null)
    {
        $aOut = [];
        $oCategoryQuery = CategoryQuery::create();
        $oCategoryQuery->filterByParentId($iCategoryParentId);
        $oCategoryQuery->orderBySorting();
        $aCategories = $oCategoryQuery->find();

        if(!$aCategories->isEmpty())
        {
            foreach($aCategories as $oCategory)
            {
                $aState = [];
                if($iCategoryParentId == null)
                {
                    $aState['Opened'] = true;
                }
                $sDataAttr = '';
                if(!empty($aState))
                {
                    $sDataAttr = 'data-jstree="'.json_encode($aState).'"';
                }

                $aOut[] = "    <li id=\"c_{$oCategory->getId()}\" $sDataAttr>";
                $aOut[] = '     '.$oCategory->getName();

                $aChildren = CategoryQuery::create()->findByParentId($oCategory->getId());
                if(!$aChildren->isEmpty())
                {
                    $aOut[] = '     <ul>';
                    $aOut[] = '     '.self::getTree($oCategory->getId());
                    $aOut[] = '     </ul>';
                }

                $aOut[] = '    </li>';
            }
        }
        return join(PHP_EOL, $aOut);
    }
}