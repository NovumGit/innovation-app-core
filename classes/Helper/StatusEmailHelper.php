<?php
namespace Helper;
use Core\Cfg;
use Core\Mailer;
use Core\MailMessage;
use Model\Crm\Base\CustomerQuery;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;

class StatusEmailHelper
{
    static function sendCustomerStatusEmail($iCustomerId, $sNoticationMailCode)
    {
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneByCode($sNoticationMailCode);
        $oLanguage = LanguageQuery::create()->findOneById($oCustomer->getLanguageId());

        $sSender = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_subject_'.$oLanguage->getId());

        $sDomain = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN');
        $sUrl = $sDomain."/".$oLanguage->getShopUrlPrefix().'/mail/customer?customer_id='.$iCustomerId.'&notification_type_id='.$oSale_order_notification_type->getId();
        $sContents = file_get_contents($sUrl);

        $oSaleOrderNotification = new SaleOrderNotification();
        $oSaleOrderNotification->setSaleOrderId(null);
        $oSaleOrderNotification->setSaleOrderNotificationTypeId($oSale_order_notification_type->getId());
        $oSaleOrderNotification->setFromEmail($sSender);
        $oSaleOrderNotification->setToEmail($oCustomer->getEmail());
        $oSaleOrderNotification->setContent(strip_tags($sContents));
        $oSaleOrderNotification->setTitle($sSubject);
        $oSaleOrderNotification->setDateSent(time());
        $oSaleOrderNotification->setSenderIp($_SERVER['REMOTE_ADDR']);
        $oSaleOrderNotification->save();

        $oMailMessage = new MailMessage();
        $oMailMessage->setFrom($sSender);
        $oMailMessage->setTo($oCustomer->getEmail());
        $oMailMessage->setSubject($sSubject);
        $oMailMessage->setBody($sContents);

        Mailer::send($oMailMessage);
    }
}