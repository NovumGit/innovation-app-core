<?php
namespace Helper;

use Model\Marketing\Coupon;
use Model\Marketing\CouponQuery;
use DateTime;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;

class CouponHelper{

    static function isInvalid($sCouponCode)
    {
        $oCoupon = CouponQuery::create()->findOneByRedeemCode($sCouponCode);

        if(!$oCoupon instanceof Coupon)
        {
            return 'redeem_code_not_found';
        }

        $oCouponType = $oCoupon->getCouponType();

        $oSaleOrder = SaleOrderQuery::create()->findOneByCouponCode($sCouponCode);

        if(($oSaleOrder instanceof SaleOrder || $oCoupon->getGivenOut()) && !$oCouponType->getMultiRedeem())
        {
            return 'redeem_code_already_redeemed';
        }


        if($oCouponType->getValidAfter() == null)
        {
            return 'redeem_code_no_start_date';
        }
        else if((new DateTime("now"))->format('U') <= $oCouponType->getValidAfter()->format('U'))
        {
            return 'redeem_code_not_valid_yet';
        }

        if($oCouponType->getValidUntil() == null)
        {
            return 'redeem_code_no_end_date';
        }
        if($oCouponType->getValidUntil()->format('U') <= (new DateTime("now"))->format('U'))
        {
            return 'redeem_code_not_valid_anymore';
        }

        return false;
    }
}