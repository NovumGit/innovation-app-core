<?php
namespace Helper;

use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudConfigQuery;
use Model\Setting\CrudManager\CrudConfig;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Model\Setting\CrudManager\CrudEditorQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class EditViewButtonHelper{

    static function deleteButton($sCrudClass, $sEditorName, $iButtonId)
    {
        $oCrudEditor = self::getEditor($sCrudClass, $sEditorName);

        $oCrudEditorButtonQuery = CrudEditorButtonQuery::create();
        $oCrudEditorButtonQuery->filterByCrudEditorId($oCrudEditor->getId());
        $oCrudEditorButtonQuery->filterById($iButtonId);
        $oCrudEditorButtonQuery->delete();
    }

    static function createBlankButton($sCrudClass, $sEditorName, $sButtonTitle)
    {
        $oCrudEditor = self::getEditor($sCrudClass, $sEditorName);

        // We schuiven de sorting van alle overige buttons even een stukje op zodat deze button de eerste wordt.
        $aButtons = $oCrudEditor->getCrudEditorButtons();
        if(!$aButtons->isEmpty())
        {
            foreach($aButtons as $oButton)
            {
                $iSorting = $oButton->getSorting();
                $oButton->setSorting(++$iSorting);
                $oButton->save();
            }
        }
        $oNewCrudEditorButton = new CrudEditorButton();
        $oNewCrudEditorButton->setTitle($sButtonTitle);
        $oNewCrudEditorButton->setSorting(0);
        $oNewCrudEditorButton->setCrudEditorId($oCrudEditor->getId());
        $oNewCrudEditorButton->save();
    }
    static function getViewButtons($sCrudClass, $sEditorName)
    {
        $oCrudEditor = self::getEditor($sCrudClass, $sEditorName);

        if(!$oCrudEditor instanceof CrudEditor)
        {
            return null;
        }

        $oCrudEditorButtonQuery = CrudEditorButtonQuery::create();
        $oCrudEditorButtonQuery->filterByCrudEditorId($oCrudEditor->getId());
        $aCrudEditorButtons =  $oCrudEditorButtonQuery->orderBySorting();

        return $aCrudEditorButtons;
    }
    public static function getVisibleButtons($sCrudClass, $sEditorName, ActiveRecordInterface $oModel)
    {
        $oCrudEditor = self::getEditor($sCrudClass, $sEditorName);
        if(!$oCrudEditor)
        {
            return null;
        }
        $oCrudEditorButtonQuery = CrudEditorButtonQuery::create();
        $oCrudEditorButtonQuery->filterByCrudEditorId($oCrudEditor->getId());
        $aCrudEditorButtons =  $oCrudEditorButtonQuery->orderBySorting()->find();

        $aVisibleButtons = false;
        if(!$aCrudEditorButtons->isEmpty())
        {
            foreach($aCrudEditorButtons as $oCrudEditorButton)
            {
                $aFilters = $oCrudEditorButton->getCrudEditorButtonVisibileFilters();
                if(!$aFilters->isEmpty())
                {
                    // Alle Filters moeten ok zijn om door te mogen voor de ijstkast.
                    $bAllFiltersOk = true;
                    foreach($aFilters as $oFilter)
                    {
                        // Om te controleren of alle filters ok zijn houden we per filter bij of die ok is.
                        // Zodra er een niet ok is, kunnen we stoppen.
                        $bCurrentFilterOk = false;
                        $sOperatorName = $oFilter->getFilterOperator()->getName();

                        $sNamespaceLessClass = substr($oFilter->getFilterName(), strrpos($oFilter->getFilterName(), '\\') + 1);
                        $sAutoGetter = 'get'.$sNamespaceLessClass;
                        $sCurrentObjectValue = $oModel->$sAutoGetter();

                        if($sOperatorName == 'equals')
                        {
                            if($sCurrentObjectValue == $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'larger_then')
                        {
                            if($sCurrentObjectValue > $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'larger_then_or_equal')
                        {
                            if($sCurrentObjectValue >= $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'smaller_then')
                        {
                            if($sCurrentObjectValue < $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'smaller_then_or_equal')
                        {
                            if($sCurrentObjectValue <= $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'in')
                        {
                            $aValues = explode(',', $oFilter->getFilterValue());
                            if(in_array($sCurrentObjectValue, $aValues))
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'not_in')
                        {
                            $aValues = explode(',', $oFilter->getFilterValue());
                            if(!in_array($sCurrentObjectValue, $aValues))
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'starts_with')
                        {
                            if(strpos(strtolower($oFilter->getFilterValue()), strtolower($sCurrentObjectValue)) === 0)
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'ends_on')
                        {
                            if(strpos(strtolower(strrev($oFilter->getFilterValue())), strtolower(strrev($sCurrentObjectValue))) === 0)
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'contains_text')
                        {
                            if(strpos(strtolower($oFilter->getFilterValue()), strtolower($sCurrentObjectValue)))
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'not_equals')
                        {
                            if($sCurrentObjectValue != $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }
                        else if($sOperatorName == 'yes_no')
                        {
                            if($sCurrentObjectValue == $oFilter->getFilterValue())
                            {
                                $bCurrentFilterOk = true;
                            }
                        }

                        if(!$bCurrentFilterOk)
                        {
                            $bAllFiltersOk = false;
                            break;
                        }
                        /*
                        $sNamespaceLessClass = substr($oFilter->getFilterName(), strrpos($oFilter->getFilterName(), '\\') + 1);
                        $sAutoGetter = 'get'.$sNamespaceLessClass;


                        echo $sNamespaceLessClass." ".$oModel->$sAutoGetter()." ".$oFilter->getFilterOperator()->getName()."<br>";
                        // $oFilter->getFilterName()
                        // $oModel->get
                        //$oFilter->getFilterValue();
                        //$oFilter->getFilterOperator()
                        //$oFilter->getFilterName()
                        */
                    }
                    if($bAllFiltersOk)
                    {
                        if(!$aVisibleButtons)
                        {
                            $aVisibleButtons = [];
                        }
                        $aVisibleButtons[] = $oCrudEditorButton;
                    }
                }
            }
        }

        return $aVisibleButtons;
    }
    private static function getEditor($sCrudClass, $sEditorName)
    {
        $oCrudConfig = CrudConfigQuery::create()->findOneByManagerName($sCrudClass);
        if(!$oCrudConfig instanceof CrudConfig)
        {
            throw new LogicException("Attempt to load buttons for crud config ($sCrudClass) that does not exist.");
        }
        $oCrudEditorQuery = CrudEditorQuery::create();
        $oCrudEditorQuery->filterByName($sEditorName);
        $oCrudEditorQuery->filterByCrudConfigId($oCrudConfig->getId());
        $oCrudEditor = $oCrudEditorQuery->findOne();

        if(!$oCrudEditor instanceof CrudEditor)
        {
            return null;
        }
        return $oCrudEditor;
    }

}