<?php
namespace Helper;

use Model\Company\Company;
use Model\Company\CompanySetting;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\CustomerCompanyQuery;
use Model\Crm\CustomerQuery;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\Base\SaleOrderStatusQuery;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\SaleOrderStatus;

class SaleOrderHelper{

    static function generateOrderNumber(Company $oCompany, SaleOrder $oSaleOrder)
    {
        if(!$oSaleOrder->getOrderNumber() || $oSaleOrder->getOrderNumber() == 'proforma')
        {
            $oCompanySetting = $oCompany->getCompanySettings()->getFirst();

            if (!$oCompanySetting instanceof CompanySetting) {
                $oCompanySetting = new CompanySetting();
                $oCompanySetting->setOwnCompanyId($oCompany->getId());
                $oCompanySetting->setOrderNumber('0');
                $oCompanySetting->setOrderPrefix('');
                $oCompanySetting->save();
            }
            $iOrderNumber = (int) $oCompanySetting->getOrderNumber();
            $iOrderNumber ++;
            $oCompanySetting->setOrderNumber($iOrderNumber);
            $oCompanySetting->save();

            $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneByCode('new');
            if($oSaleOrderStatus instanceof SaleOrderStatus)
            {
                $oSaleOrder->setSaleOrderStatusId($oSaleOrderStatus->getId());
            }
            $oSaleOrder->setOrderNumber($oCompanySetting->getOrderPrefix().$iOrderNumber);
            $oSaleOrder->save();
        }
        return $oSaleOrder;
    }

    static function generateInvoiceNumber(Company $oCompany, SaleOrder $oSaleOrder)
    {

        if(!$oSaleOrder->getInvoiceNumber() || $oSaleOrder->getInvoiceNumber() == 'proforma')
        {
            $oCompanySetting = $oCompany->getCompanySettings()->getFirst();

            if (!$oCompanySetting instanceof CompanySetting)
            {
                $oCompanySetting = new CompanySetting();
                $oCompanySetting->setOwnCompanyId($oCompany->getId());
                $oCompanySetting->setInvoiceNumber('0');
                $oCompanySetting->setInvoicePrefix('');
                $oCompanySetting->save();
            }


            $iInvoiceNumber = (int) $oCompanySetting->getInvoiceNumber();
            $iInvoiceNumber ++;
            $oCompanySetting->setInvoiceNumber($iInvoiceNumber);
            $oCompanySetting->save();
            $oSaleOrder->setInvoiceNumber($oCompanySetting->getInvoicePrefix().$iInvoiceNumber);
            $oSaleOrder->save();
        }
        return $oSaleOrder;
    }
    static function addDeliveryAddressToSaleOrder(SaleOrder $oSaleOrder, $iDeliveryAddressId):SaleOrder{
        $oCustomerAddress = CustomerAddressQuery::create()->findOneById($iDeliveryAddressId);
        $oSaleOrder->setCustomerDeliveryAddressId($iDeliveryAddressId);
        $oSaleOrder->setCustomerDeliveryCompanyName($oCustomerAddress->getCompanyName());
        $oSaleOrder->setCustomerDeliveryAttnName($oCustomerAddress->getAttnName());
        $oSaleOrder->setCustomerDeliveryStreet($oCustomerAddress->getStreet());
        $oSaleOrder->setCustomerDeliveryNumber($oCustomerAddress->getNumber());
        $oSaleOrder->setCustomerDeliveryNumberAdd($oCustomerAddress->getNumberAdd());
        $oSaleOrder->setCustomerDeliveryPostal($oCustomerAddress->getPostal());
        $oSaleOrder->setCustomerDeliveryCity($oCustomerAddress->getCity());


        $sCountry = null;
        if($oCountry = $oCustomerAddress->getCountry())
        {
            $sCountry = $oCountry->getName();
        }
        $oSaleOrder->setCustomerDeliveryCountry($sCountry);

        $sState  = null;
        if($oState = $oCustomerAddress->getUsaState())
        {
            $sState = $oState->getName();
        }
        $oSaleOrder->setCustomerDeliveryUsaState($sState);
        
        $oSaleOrder->setCustomerDeliveryAddressL1($oCustomerAddress->getAddressL1());
        $oSaleOrder->setCustomerDeliveryAddressL2($oCustomerAddress->getAddressL2());
        return $oSaleOrder;
    }
    static function addInvoiceAddressToSaleOrder(SaleOrder $oSaleOrder, $iInvoiceAddressId):SaleOrder{
        $oCustomerAddress = CustomerAddressQuery::create()->findOneById($iInvoiceAddressId);
        $oSaleOrder->setCustomerInvoiceAddressId($iInvoiceAddressId);
        $oSaleOrder->setCustomerInvoiceCompanyName($oCustomerAddress->getCompanyName());
        $oSaleOrder->setCustomerInvoiceAttnName($oCustomerAddress->getAttnName());
        $oSaleOrder->setCustomerInvoiceStreet($oCustomerAddress->getStreet());
        $oSaleOrder->setCustomerInvoiceNumber($oCustomerAddress->getNumber());
        $oSaleOrder->setCustomerInvoiceNumberAdd($oCustomerAddress->getNumberAdd());
        $oSaleOrder->setCustomerInvoicePostal($oCustomerAddress->getPostal());
        $oSaleOrder->setCustomerInvoiceCity($oCustomerAddress->getCity());

        $sCountry = null;
        if($oCountry = $oCustomerAddress->getCountry())
        {
            $sCountry = $oCountry->getName();
        }
        $oSaleOrder->setCustomerInvoiceCountry($sCountry);

        $sState  = null;
        if($oState = $oCustomerAddress->getUsaState())
        {
            $sState = $oState->getName();
        }
        $oSaleOrder->setCustomerInvoiceUsaState($sState);

        $oSaleOrder->setCustomerInvoiceAddressL1($oCustomerAddress->getAddressL1());
        $oSaleOrder->setCustomerInvoiceAddressL2($oCustomerAddress->getAddressL2());
        return $oSaleOrder;

    }
    static function addCustomerToSaleOrder(SaleOrder $oSaleOrder, $iCustomerId)
    {
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        if($oCustomer->getLanguageId())
        {
            $oSaleOrder->setLanguageId($oCustomer->getLanguageId());
        }
        else
        {
            $oDefaultLanguage = LanguageQuery::create()->findOneByIsDefaultWebshop(true);
            $oSaleOrder->setLanguageId($oDefaultLanguage->getId());
        }
        $oSaleOrder->setCustomerId($oCustomer->getId());
        $oSaleOrder->setItemDeleted(false);

        $oSaleOrder->setDebitor($oCustomer->getDebitor());

        $oSaleOrder->setCustomerPhone($oCustomer->getPhone());
        $oSaleOrder->setCustomerFax($oCustomer->getFax());
        $oSaleOrder->setCustomerEmail($oCustomer->getEmail());

        $oCustomerCompany = CustomerCompanyQuery::create()->findOneByCustomerId($iCustomerId);

        if($oCustomerCompany instanceof CustomerCompany)
        {
            $oSaleOrder->setCompanyName($oCustomerCompany->getCompanyName());
            $oSaleOrder->setCustomerChamberOfCommerce($oCustomerCompany->getChamberOfCommerce());
            $oSaleOrder->setCustomerVatNumber($oCustomerCompany->getVatNumber());
        }
        return $oSaleOrder;
    }

    public static function addOwnCompanyToSaleOrder(SaleOrder $oSaleOrder, Company $oCompany)
    {
        $oSaleOrder->setOurPhone($oCompany->getPhone());
        $oSaleOrder->setOurFax($oCompany->getFax());
        $oSaleOrder->setOurWebsite($oCompany->getWebsite());
        $oSaleOrder->setOurEmail($oCompany->getEmail());
        $oSaleOrder->setOurVatNumber($oCompany->getVatNumber());
        $oSaleOrder->setOurChamberOfCommerce($oCompany->getChamberOfCommerce());
        $oSaleOrder->setOurIbanNumber($oCompany->getIban());
        $oSaleOrder->setOurBicNumber($oCompany->getBic());
        $oSaleOrder->setOurGeneralPoBox($oCompany->getGeneralPoBox());
        $oSaleOrder->setOurGeneralCompanyName($oCompany->getCompanyName());
        $oSaleOrder->setOurGeneralStreet($oCompany->getGeneralStreet());
        $oSaleOrder->setOurGeneralNumber($oCompany->getGeneralNumber());
        $oSaleOrder->setOurGeneralNumberAdd($oCompany->getGeneralNumberAdd());
        $oSaleOrder->setOurGeneralPostal($oCompany->getGeneralPostal());
        $oSaleOrder->setOurGeneralCity($oCompany->getGeneralCity());

        $oSaleOrder->setOurGeneralCountry('The Netherlands');
        return $oSaleOrder;
    }
}
