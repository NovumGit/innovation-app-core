<?php
namespace Helper;

use Core\Config;
use Exception\LogicException;
use model\Cms\Base\SitePage;

class PageHelper{

    static function getPageDataDir(SitePage $oSitePage)
    {
        $sDir = Config::getDataDir().'/page/'.$oSitePage->getId().'/';
        if(!is_dir($sDir))
        {
            mkdir($sDir, 0777, true);
            chmod($sDir, 0777);
        }
        else if(!is_writable($sDir))
        {
            throw new LogicException("Map $sDir is niet beschrijfbaar.");
        }
        return $sDir;
    }


    static function getPagePictureDir(SitePage $oSitePage)
    {
//        $sDir = Config::getPublicDataDir().'/page/'.$oSite->getId().'/pictures/';
        $sDir = self::getPageDataDir($oSitePage).'pictures/';
        if(!is_dir($sDir))
        {
            if(!is_writable($sDir))
            {
                throw new LogicException("Map ".self::getPageDataDir($oSitePage)." is niet beschrijfbaar.");
            }
            mkdir($sDir, 0777, true);
            chmod($sDir, 0777);
        }
        return $sDir;
    }

    static function makeDirectoryStructure(SitePage $oSitePage)
    {
        $sDataDir = Config::getDataDir();

        if(!is_dir($sDataDir))
        {
            throw new LogicException("De data directory bestaat niet: $sDataDir.");
        }
        if(!is_writeable($sDataDir))
        {
            throw new LogicException("De data directory is read-only: $sDataDir.");
        }

        $sSitePageFileDir = self::getPageDataDir($oSitePage);

        if(!is_dir($sSitePageFileDir))
        {
            mkdir($sSitePageFileDir);
        }
        if(!is_writeable($sSitePageFileDir))
        {
            throw new LogicException("De product data directory bestaat niet: $sSitePageFileDir.");
        }

        $sSitePageImageDir = self::getPagePictureDir($oSitePage);

        if(!is_dir($sSitePageImageDir))
        {
            mkdir($sSitePageImageDir);
        }
        if(!is_writeable($sSitePageImageDir))
        {
            throw new LogicException("De product image directory bestaat niet: $sSitePageImageDir.");
        }
    }

}
