<?php
namespace Helper;

use Hurah\Types\Type\Path;
use DOMDocument;
use Exception\LogicException;
use Helper\Schema\Database;

class Schema
{
    private Database $oDatabase;
    private function __construct(string $sXml){

        $oDom = new DOMDocument();
        $oDom->loadXML($sXml);

        $oDatabase = simplexml_load_string($sXml, Database::class, LIBXML_NOCDATA);

        if (!$oDatabase instanceof Database) {
            throw new LogicException("Could not parse schema.xml");
        }
        $this->oDatabase = $oDatabase;
    }
    function getDatabase():Database
    {
        return $this->oDatabase;
    }
    static function fromFilename(Path $oFileName):self
    {
        return new self(file_get_contents($oFileName));
    }
    static function fromPath(Path $oPath):self
    {
        echo "Loading file {$oPath->getFile()->getValue()}" . PHP_EOL;
        return new self($oPath->getFile()->contents());
    }

}
