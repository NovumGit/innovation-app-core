<?php
require_once '../../../vendor/autoload.php';

use Core\Utils;
use Gregwar\Image\Image;

ini_set('display_errors', 1);
error_reporting(E_ALL);

$sRequestedOriginalFile = $_GET['requested_file'];
$sRequestedFile = $sRequestedOriginalFile;

$bMustCrop = false;
if(preg_match('/(?<width>[0-9]+)x(?<height>[0-9]+)\/(?<original>[0-9]+\.(?<ext>jpg|png|jpeg)?)/', $sRequestedOriginalFile, $aMatches))
{
    $bMustCrop = true;
    $sRequestedFile = $aMatches['original'];
}
if(preg_match('/^[0-9]+\.(jpg|png|jpeg)?$/', $sRequestedFile))
{
    $sExt = pathinfo($sRequestedFile, PATHINFO_EXTENSION);

    $sOriginalFile = '../../../data/img/product/'.$aMatches['original'];
    $sThumbFile = '../../../data/img/product/sliced/admin_'.$aMatches['width'].'x'.$aMatches['height'].'/'.$aMatches['original'];

    if(file_exists($sOriginalFile))
    {
        if($bMustCrop)
        {
            if(!is_dir(dirname($sThumbFile)))
            {
                mkdir(dirname($sThumbFile), 0777, true);
            }

            $bMakeFile = true;

            if(!file_exists($sThumbFile))
            {
                $bMakeFile = true;
            }
            if(file_exists($sThumbFile) && filemtime($sOriginalFile) < filemtime($sThumbFile))
            {
                $bMakeFile = true;
            }

            if($bMakeFile)
            {
                $sImage = Image::open($sOriginalFile)->zoomCrop($aMatches['width'], $aMatches['height'], 0, 'center')->get();
                file_put_contents($sThumbFile, $sImage);
            }

            if($sExt)
            {
                Utils::showHeader($sThumbFile);
            }
            else
            {
                Utils::showHeader('test.jpg');
            }

            readfile($sThumbFile);
        }
        else
        {
            echo file_get_contents($sOriginalFile);
        }
    }
    else
    {
        Utils::showHeader('../file-not-found.jpg');
        echo file_get_contents('http://placehold.it/300?text=File not found');
    }
}
else
{
    exit('unsupported file type');
}