<?php
require_once '../../../vendor/autoload.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

use Gregwar\Image\Image;


if(preg_match('/^[0-9]+\/main-recipe-image\.(jpg|png|jpeg)$/', $_GET['requested_file']))
{

    $sExt = pathinfo($_GET['requested_file'], PATHINFO_EXTENSION);

    if($sExt == 'jpg' || $sExt == 'jpeg' )
    {
        header('Content-type: image/jpeg');
    }
    else if($sExt == 'png')
    {
        header('Content-type: image/png');
    }

	$sOriginalFile = '../../../data/img/recipe/'.$_GET['requested_file'];
    if(isset($_GET['w']) && isset($_GET['h']) && $_GET['w'] && $_GET['h'])
    {
	    $sImage = Image::open($sOriginalFile)->zoomCrop($_GET['w'], $_GET['h'], 0, 'center')->get();
	    echo $sImage;
    }
    else
    {
	    readfile($sOriginalFile);
    }



}
else
{
    exit('unsupported file type');
}
