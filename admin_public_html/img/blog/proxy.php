<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

function showHeader($sExt)
{
    if($sExt == 'jpg' || $sExt == 'jpeg' )
    {
        header('Content-type: image/jpeg');
    }
    else if($sExt == 'png')
    {
        header('Content-type: image/png');
    }
}
if(preg_match('/(jpg|png|jpeg)$/', $_GET['requested_file']))
{
    $sExt = pathinfo($_GET['requested_file'], PATHINFO_EXTENSION);
    showHeader($sExt);

    $sRequestedFile = '../../../data/img/blog/'.$_GET['requested_file'];

    if(!file_exists($sRequestedFile) && preg_match('/sliced\/[0-9]+x[0-9]+\//', $_GET['requested_file']))
    {
        $sRequestedFile = preg_replace('/sliced\/[0-9]+x[0-9]+\//', '', $sRequestedFile);

    }


    echo file_get_contents($sRequestedFile);
    //readfile('../../../data/banner/'.$_GET['requested_file']);
}
else
{
    exit('unsupported file type');
}



//file_get_contents('../data/category/')