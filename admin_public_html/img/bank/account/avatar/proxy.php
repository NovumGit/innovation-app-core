<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

function showHeader($sExt)
{
    if($sExt == 'jpg' || $sExt == 'jpeg' )
    {
        header('Content-type: image/jpeg');
    }
    else if($sExt == 'png')
    {
        header('Content-type: image/png');
    }
}
if(preg_match('/(jpg|png|jpeg)$/', $_GET['requested_file']))
{
    $sExt = pathinfo($_GET['requested_file'], PATHINFO_EXTENSION);
    showHeader($sExt);

    $sRequestedFile = '../../../../../data/finance/bank-avatars/'.$_GET['requested_file'];

    readfile($sRequestedFile);
}
else
{
    exit('unsupported file type');
}



//file_get_contents('../data/category/')