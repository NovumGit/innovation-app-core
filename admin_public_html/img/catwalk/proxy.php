<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

if(preg_match('/^[0-9]+\/catwalk\.(jpg|png|jpeg)$/', $_GET['requested_file']))
{

    $sExt = pathinfo($_GET['requested_file'], PATHINFO_EXTENSION);

    if($sExt == 'jpg' || $sExt == 'jpeg' )
    {
        header('Content-type: image/jpeg');
    }
    else if($sExt == 'png')
    {
        header('Content-type: image/png');
    }

    readfile('../../../data/img/catwalk/'.$_GET['requested_file']);
}
else
{
    exit('unsupported file type');
}
