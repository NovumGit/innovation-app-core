<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

function showHeader($sExt)
{
    if($sExt == 'jpg' || $sExt == 'jpeg' )
    {
        header('Content-type: image/jpeg');
    }
    else if($sExt == 'png')
    {
        header('Content-type: image/png');
    }
}

if(preg_match('/^[0-9]+\.(jpg|png|jpeg)$/', $_GET['requested_file']))
{
    $sExt = pathinfo($_GET['requested_file'], PATHINFO_EXTENSION);


    if(file_exists('../../../../data/img/cms/cover/'.$_GET['requested_file']))
    {

     //   showHeader($sExt);
        echo file_get_contents('../../../../data/img/cms/cover/'.$_GET['requested_file']);
    }
    else
    {

        showHeader('jpg');
        echo file_get_contents('../../file-not-found.jpg');
    }

   // readfile('../data/product/'.$_GET['requested_file']);
}
else
{
    exit('unsupported file type');
}



//file_get_contents('../data/category/')