

const oTabCollection = $('a.toggle.tab');
oTabCollection.each((i, item) => {

    $(item).click((e) => {
        e.preventDefault();

        let tabGroup = $(item).data('group');
        let activeMarker = $(item).data('active-marker');

        console.log('.' + tabGroup);
        $('.' + tabGroup + ' li').removeClass('active');
        console.log('.' + activeMarker);
        $('.' + activeMarker).addClass('active');

        let sTabToShow = $(item).data('show');

        $(item).parent().addClass('active');
        $('.tabs.' + sTabToShow).css('display', 'block');
    });
});
/*

*/
// Orders en relaties gebruiken deze functie
$('.other_address').on('change', function(e){
    e.preventDefault();

    var sVisibleBlock = $(this).val() === 1 ? $(this).data('type') : 'general';
    var sHiddenBlock = $(this).val() === 1 ? 'general' : $(this).data('type');

    $('.address_' + $(this).data('type') + '_' + sVisibleBlock).css('display', 'block');
    $('.address_' + $(this).data('type') + '_' + sHiddenBlock).css('display', 'none');

});

// Session keepalive feature
setInterval(function(){
    $.get('/tamtam');
}, 840000); // 14 mins * 60 * 1000 = 840000

(function($) {
    Core.init();

    if($('.auto-modal').length > 0)
    {
        $.magnificPopup.open({
            removalDelay: 500,
            items: {
                src: '.auto-modal'
            },
            callbacks: {
                beforeOpen: function (e) {
                    this.st.mainClass = 'mfp-zoomIn';
                }
            },
            midClick: true
        });
    }

    var toggleSidebar = function(sForce)
    {
        var sState = 'large';
        var oBody = $('body');

        console.log(sForce);
        if(typeof sForce == 'undefined')
        {
            console.log('xxx');
            if(oBody.hasClass('sb-l-o'))
            {
                sState = 'minimal';
            }
        }

        console.log(sState);

        if(sState === 'large')
        {
            console.log('add!');
            oBody.addClass('sb-l-o');
            oBody.removeClass('sb-l-m');
        }
        else
        {
            console.log('remove?');
            oBody.addClass('sb-l-m');
            oBody.removeClass('sb-l-o');
        }

        var oDynamicToggle = $('.menu_toggle_along');

        if(oDynamicToggle.length > 1)
        {
            oDynamicToggle.each(function(i, e){
                if(sState === 'minimal')
                {
                    $(this).addClass($(this).data('menu_closed'));
                    $(this).removeClass($(this).data('menu_open'));
                }
                else
                {
                    $(this).addClass($(this).data('menu_open'));
                    $(this).removeClass($(this).data('menu_closed'));
                }
            });
        }
        var oPostData = {
            _do : 'storeMenuSize',
            state : sState
        };
        $.post('/generic/ajax', oPostData);
    };

    $('.sidebar-toggler').on('click', function()
    {
        toggleSidebar();
    });
    $('#sidebar_left_toggle').on('click', function()
    {
        toggleSidebar();
    });




    $('.sidebar-menu li a.accordion-toggle').on('click', function()
    {
        var  sModuleId = $(this).prop('id');

        var oPostData = {
            _do : 'storeMenuState',
            state : $(this).hasClass('menu-open'),
            module : sModuleId.replace('module_', '')
        };

        $.post('/generic/ajax', oPostData);
    });

/*
    $('.datetime_picker').datetimepicker({
        locale : 'nl',
        format: 'DD-MM-YYYY HH:mm',
         pick12HourFormat: false
    });
*/
    // Init Summernote
    $('.summernote-quick').summernote({
        height: 179,
        focus: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });



})(jQuery);

function bindModalCloseButton()
{
    console.log('bind2');

    $('#btn_modal_close').click((e) => {

        console.log('click');
        const serviceModal = $('.auto-modal');
        console.log('automodal');
        serviceModal.fadeOut('fast', () => {
            console.log('fadedout');
        });

    });
}

