const oMainContent = $('#content_wrapper');
const sInitialContentWrapperPaddingTop = oMainContent.css('paddingTop');
const sInitialContentWrapperMarginLeft = oMainContent.css('marginLeft');

const oNavbarTop = $('#navbar_top');
const oSidebarLeft = $('#sidebar_left');

const Fullscreen =
{

    toggle : () =>
    {

        oMainContent.css('marginLeft');

        if(oNavbarTop.is(':visible'))
        {
            this.hideNavbarTop();
        }
        else
        {
            this.showNavbarTop();
        }

        if(oSidebarLeft.is(':visible'))
        {
            this.hideSidebarLeft();
        }
        else
        {
            this.showSidebarLeft();
        }
    },
    enable()
    {
        this.hideSidebarLeft();
        this.hideNavbarTop();
    },
    disable()
    {
        this.showNavbarTop();
        this.showSidebarLeft();
    },
    hideNavbarTop()
    {
        oNavbarTop.fadeOut(500);
        setTimeout(function () {
            oMainContent.animate({'paddingTop' : '0px'}, 200);
        }, 500)
    },
    showNavbarTop()
    {
        this.showNavbarTop();
        oMainContent.animate({'paddingTop' : sInitialContentWrapperPaddingTop}, 200);
        setTimeout(function () {
            oNavbarTop.fadeIn(500);
        }, 500)
    },
    hideSidebarLeft()
    {
        oSidebarLeft.fadeOut(500);
        setTimeout(function(){
            oMainContent.animate({'marginLeft' : '0px'}, 200);
        }, 500);
    },
    showSidebarLeft()
    {
        oMainContent.animate({'marginLeft' : sInitialContentWrapperMarginLeft}, 200);
        setTimeout(function(){
            oSidebarLeft.fadeIn(500);
        }, 500)
    }
};
