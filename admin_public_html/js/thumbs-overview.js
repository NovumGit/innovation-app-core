$(document).ready(function () {

    $('.toggle_thumb_size').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/generic/ajax?_do=ToggleThumbSize',
            async: true,
            method: 'POST',
            data: {new_size: $(this).data('size')},
            beforeSend: null,
            cache: false,
            success: function () {
                console.log('reload');
                location.reload();
            },
            dataType: 'json',
            error: function (xhr, status, error) {
                console.log('fuck');
            }
        });
    });

    $('.toggle_view').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/generic/ajax?_do=ToggleView',
            async: true,
            method: 'POST',
            data: {new_view: $(this).attr('id')},
            beforeSend: null,
            cache: false,
            success: function () {
                console.log('reload');
                location.reload();
            },
            dataType: 'json',
            error: function (xhr, status, error) {
                console.log('fuck');
            }
        });
    });

    $('.popupimage').magnificPopup({
        type: 'image',
        callbacks: {
            beforeOpen: function (e) {
                // Indicate active overlay
                $('body').addClass('mfp-bg-open');

                // Magnific Animation
                this.st.mainClass = 'mfp-zoomIn';

                // Animation notify class
                this.contentContainer.addClass('mfp-with-anim');
            },
            afterClose: function (e) {

                setTimeout(function () {
                    $('body').removeClass('mfp-bg-open');
                    $(window).trigger('resize');
                }, 1000)

            },
            elementParse: function (item) {
                item.src = item.el.data('original');
            }
        },
        overflowY: 'scroll',
        removalDelay: 200,
        prependTo: $('#content_wrapper')
    });
});
