/// <reference path ="./typescript/src/schema-editor/node_modules/@types/jquery/index.d.ts"/>

class StatusMessage {

    static getTemplate(sType, sLabel, sMessage, sElementId) {

        let sLabelColor = sType === 'warning' ? 'black' : 'white';
        let sTemplate = '';
        // sTemplate += '<section id="content" class="table-layout animated fadeIn">';
        // sTemplate += '   <div class="panel">';

        if ($('#' + sElementId).length <= 0) {
            // if($('#' + sElementId).length )
            sTemplate += '       <div class="bs-component" id="' + sElementId + '">';
            sTemplate += '           <div class="alert alert-' + sType + ' dark alert-dismissable" style="color:' + sLabelColor + ';">';
            sTemplate += '               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            sTemplate += '               <i class="fa fa-info pr10"></i>';
            sTemplate += '               <strong>' + sLabel + ':</strong> ' + sMessage;
            sTemplate += '           </div>';
            sTemplate += '       </div>';
            //sTemplate += '   </div>';
            // sTemplate += '</section>';
        } else {
            console.log("Already showing message with id " + sElementId + ", skipping for now.");
        }
        return sTemplate;
    }

    static show(sLevel, sMessage, sTitle, sElementId) {
        const sTemplate = this.getTemplate(sLevel, sTitle, sMessage, sElementId);
        if (sTemplate) {
            $('#status_message_container').prepend(sTemplate);
        }

    }

    static success(sTitle, sMessage, sElementId) {
        this.show('success', sMessage, sTitle, sElementId);
    }
    static alert(sTitle, sMessage, sElementId) {
        this.show('alert', sMessage, sTitle, sElementId);
    }
    static warning(sTitle, sMessage, sElementId) {
        console.warn("WARNING");
        this.show('warning', sMessage, sTitle, sElementId);
    }
    static danger(sTitle, sMessage, sElementId) {
        this.show('danger', sMessage, sTitle, sElementId);
    }
}

