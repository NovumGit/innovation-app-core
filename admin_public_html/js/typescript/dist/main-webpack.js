/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./EventHandler.ts":
/*!*************************!*\
  !*** ./EventHandler.ts ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EventHandler = (function () {
        function EventHandler() {
        }
        EventHandler.trigger = function (event, props, state) {
            return new Promise(function (resolve, reject) {
                console.log('EventHandler.trigger');
                event.trigger(props, state).then(resolve()).catch(reject());
            });
        };
        return EventHandler;
    }());
    exports.EventHandler = EventHandler;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./EventStack.ts":
/*!***********************!*\
  !*** ./EventStack.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./EventHandler */ "./EventHandler.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, EventHandler_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var EventStack = (function () {
        function EventStack() {
            var _this = this;
            this.execute = function () {
                _this.stack.forEach(function (event) {
                    EventHandler_1.EventHandler.trigger(event.handler, event.props, event.state);
                });
            };
        }
        EventStack.prototype.register = function (event, props, state) {
            this.stack.push({ handler: event, props: props, state: state });
        };
        return EventStack;
    }());
    exports.EventStack = EventStack;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./event/AbstractEvent.ts":
/*!********************************!*\
  !*** ./event/AbstractEvent.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./types/StatusMessage */ "./event/types/StatusMessage.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, StatusMessage_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AbstractEvent = (function () {
        function AbstractEvent() {
        }
        AbstractEvent.prototype.getPropType = function () {
            return this.propType;
        };
        AbstractEvent.prototype.getStateType = function () {
            return this.stateType;
        };
        AbstractEvent.prototype.getRejectHandler = function (reject, title, message) {
            var _this = this;
            return function (textStatus) { return __awaiter(_this, void 0, void 0, function () {
                var errors, oState, oState;
                return __generator(this, function (_a) {
                    console.log(textStatus);
                    errors = null;
                    if (textStatus.errors) {
                        errors = textStatus.errors;
                    }
                    if (errors) {
                        oState = {
                            title: title,
                            errors: errors,
                            elementId: "error_dialog",
                            StatusMessageColor: StatusMessage_1.StatusMessageColors.warning
                        };
                        reject("Validation errors occured");
                    }
                    else {
                        oState = {
                            title: title,
                            message: message.replace('{textStatus}', textStatus),
                            elementId: "error_dialog",
                            StatusMessageColor: StatusMessage_1.StatusMessageColors.warning
                        };
                    }
                    return [2];
                });
            }); };
        };
        return AbstractEvent;
    }());
    exports.AbstractEvent = AbstractEvent;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./event/IEvent.ts":
/*!*************************!*\
  !*** ./event/IEvent.ts ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var BaseProps = (function () {
        function BaseProps() {
        }
        return BaseProps;
    }());
    exports.BaseProps = BaseProps;
    var BaseState = (function () {
        function BaseState() {
        }
        return BaseState;
    }());
    exports.BaseState = BaseState;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./event/types/StatusMessage.ts":
/*!**************************************!*\
  !*** ./event/types/StatusMessage.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var StatusMessageColors;
    (function (StatusMessageColors) {
        StatusMessageColors["info"] = "info";
        StatusMessageColors["success"] = "success";
        StatusMessageColors["alert"] = "alert";
        StatusMessageColors["warning"] = "warning";
        StatusMessageColors["danger"] = "danger";
    })(StatusMessageColors = exports.StatusMessageColors || (exports.StatusMessageColors = {}));
    var StatusState = (function () {
        function StatusState() {
        }
        return StatusState;
    }());
    exports.StatusState = StatusState;
    var StatusValidationState = (function () {
        function StatusValidationState() {
        }
        return StatusValidationState;
    }());
    exports.StatusValidationState = StatusValidationState;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./schema-editor/widget/Editor */ "./schema-editor/widget/Editor.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, Editor_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    Editor_1.Editor.start();
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/Ajax.ts":
/*!*******************************!*\
  !*** ./schema-editor/Ajax.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../event/AbstractEvent */ "./event/AbstractEvent.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, AbstractEvent_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AjaxAction;
    (function (AjaxAction) {
        AjaxAction[AjaxAction["DELETE"] = 0] = "DELETE";
        AjaxAction[AjaxAction["POST"] = 1] = "POST";
        AjaxAction[AjaxAction["PUT"] = 2] = "PUT";
        AjaxAction[AjaxAction["PATCH"] = 3] = "PATCH";
        AjaxAction[AjaxAction["GET"] = 4] = "GET";
    })(AjaxAction = exports.AjaxAction || (exports.AjaxAction = {}));
    var AjaxProps = (function () {
        function AjaxProps() {
        }
        return AjaxProps;
    }());
    exports.AjaxProps = AjaxProps;
    var AjaxState = (function () {
        function AjaxState() {
        }
        return AjaxState;
    }());
    exports.AjaxState = AjaxState;
    var Ajax = (function (_super) {
        __extends(Ajax, _super);
        function Ajax() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Ajax.prototype.trigger = function (props, state) {
            return new Promise(function (resolve, reject) {
                console.log('Ajax(action, data)', props.action, state.data);
                var oData = {
                    '_do': props.action,
                    'data': state.data
                };
                console.log("\t", window.location.href, "POST", oData);
                return $.post(window.location.href, oData, function (data) {
                    console.log("\t", "server response", data);
                    if (data.errors) {
                        reject(data.errors);
                    }
                    else {
                        resolve(data);
                    }
                }, 'json')
                    .fail(function () {
                    reject('Ajax call failed');
                });
            });
        };
        return Ajax;
    }(AbstractEvent_1.AbstractEvent));
    exports.Ajax = Ajax;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/actions/DeleteProperty.ts":
/*!*************************************************!*\
  !*** ./schema-editor/actions/DeleteProperty.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../../event/AbstractEvent */ "./event/AbstractEvent.ts"), __webpack_require__(/*! ../../event/IEvent */ "./event/IEvent.ts"), __webpack_require__(/*! ../Ajax */ "./schema-editor/Ajax.ts"), __webpack_require__(/*! ../../EventHandler */ "./EventHandler.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, AbstractEvent_1, IEvent_1, Ajax_1, EventHandler_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var DeletePropertyProps = (function (_super) {
        __extends(DeletePropertyProps, _super);
        function DeletePropertyProps() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DeletePropertyProps;
    }(IEvent_1.BaseProps));
    var DeletePropertyState = (function (_super) {
        __extends(DeletePropertyState, _super);
        function DeletePropertyState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DeletePropertyState;
    }(IEvent_1.BaseState));
    var DeletePropertyPromiseType = (function () {
        function DeletePropertyPromiseType() {
        }
        DeletePropertyPromiseType.prototype.resolve = function () {
        };
        DeletePropertyPromiseType.prototype.onRejected = function (callback) {
        };
        DeletePropertyPromiseType.prototype.onFulfilled = function (callback) {
        };
        return DeletePropertyPromiseType;
    }());
    var DeleteProperty = (function (_super) {
        __extends(DeleteProperty, _super);
        function DeleteProperty() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DeleteProperty.prototype.trigger = function (props, state) {
            return new Promise(function (resolve, reject) {
                EventHandler_1.EventHandler.trigger(new Ajax_1.Ajax(), { endpoint: props.endpoint, action: props.action }, { data: state.widget.form.getData() })
                    .then(function (value) {
                    resolve(value);
                })
                    .catch(function (value) {
                    reject(value);
                });
            });
        };
        return DeleteProperty;
    }(AbstractEvent_1.AbstractEvent));
    exports.DeleteProperty = DeleteProperty;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/contracts/AbstractWidget.ts":
/*!***************************************************!*\
  !*** ./schema-editor/contracts/AbstractWidget.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../model/PropertyForm */ "./schema-editor/model/PropertyForm.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, PropertyForm_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var KeyVal = (function () {
        function KeyVal() {
        }
        return KeyVal;
    }());
    var AbstractWidget = (function () {
        function AbstractWidget() {
            this.data_keeper = [];
            this.form = new PropertyForm_1.EditPropertyForm();
        }
        AbstractWidget.prototype.getPlaceHolder = function () {
            return $('#' + this.type + '_placeholder');
        };
        AbstractWidget.prototype.createPlaceholder = function () {
            var elem = (document.createElement('div'));
            console.log('AbstractWidget.createPlaceholder', this.type + '_placeholder');
            elem.setAttribute('id', this.type + '_placeholder');
            elem.style.display = 'none';
            return elem;
        };
        AbstractWidget.prototype.setData = function (data) {
            console.log('AbstractWidget.setData', data);
            var aFieldNames = this.form.getFieldNames();
            var oFormData = this.form.getFieldManager();
            var fieldName;
            this.data_keeper = [];
            for (var i = 0; i < aFieldNames.length; i++) {
                fieldName = aFieldNames[i].toString();
                console.log("\t", fieldName, data);
                if (typeof fieldName === 'undefined') {
                    continue;
                }
                else if (typeof data === 'undefined') {
                    continue;
                }
                else if (!data.hasOwnProperty(fieldName)) {
                    continue;
                }
                this.data_keeper.push({ key: fieldName, value: data[fieldName] });
                oFormData[fieldName].val(data[fieldName]);
            }
            return this;
        };
        AbstractWidget.prototype.notifySaved = function () {
            console.log("Placeholder for save notification");
            return this;
        };
        AbstractWidget.prototype.getPanel = function () {
            console.log('AbstractWidget.getPanel()', this.selector);
            return $(this.selector);
        };
        AbstractWidget.prototype.hide = function (immediate) {
            var _this = this;
            if (immediate === void 0) { immediate = false; }
            var panel = this.getPanel();
            if (panel.length) {
                if (immediate) {
                    panel.replaceWith(this.createPlaceholder());
                }
                else {
                    panel.fadeOut(200, function () {
                        panel.replaceWith(_this.createPlaceholder());
                    });
                }
            }
            return this;
        };
        AbstractWidget.prototype.show = function () {
            console.log('AbstractWidget.show()');
            this.getPlaceHolder().replaceWith(this.template);
            this.getPanel().show();
            var aFieldNames = this.form.getFieldNames();
            var oFormFieldManager = this.form.getFieldManager();
            var currentFieldName;
            var keeperFieldName;
            var field;
            console.log("\t", aFieldNames);
            for (var i = 0; i < aFieldNames.length; i++) {
                currentFieldName = aFieldNames[i];
                for (var y = 0; y < this.data_keeper.length; y++) {
                    keeperFieldName = this.data_keeper[y].key;
                    if (currentFieldName === keeperFieldName) {
                        field = oFormFieldManager[currentFieldName];
                        if (field.is('select') && $.trim(this.data_keeper[y].value) == '') {
                            continue;
                        }
                        field.val(this.data_keeper[y].value);
                    }
                }
            }
            this.bindEvents();
            return this;
        };
        return AbstractWidget;
    }());
    exports.AbstractWidget = AbstractWidget;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/contracts/Form.ts":
/*!*****************************************!*\
  !*** ./schema-editor/contracts/Form.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var FieldValues = (function () {
        function FieldValues() {
        }
        return FieldValues;
    }());
    exports.FieldValues = FieldValues;
    var AbstractForm = (function () {
        function AbstractForm(fields) {
            this.fields = fields;
        }
        AbstractForm.prototype.getFieldNames = function () {
            return Object.keys(this.fields);
        };
        AbstractForm.prototype.getFields = function () {
            console.log('AbstractForm.getFields()');
            var aOut = [];
            for (var sField in this.getFieldNames()) {
                aOut.push(this.fields[sField]);
            }
            return aOut;
        };
        AbstractForm.prototype.getFieldManager = function () {
            return this.fields;
        };
        AbstractForm.prototype.syncData = function (model) {
            var _this = this;
            var aModel = model;
            this.getFieldNames().forEach(function (fieldName) {
                if (_this.fields.hasOwnProperty(fieldName) && typeof (_this.fields[fieldName].val()) != 'undefined') {
                    aModel[fieldName] = _this.fields[fieldName].val().toString();
                }
            });
            return aModel;
        };
        return AbstractForm;
    }());
    exports.AbstractForm = AbstractForm;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/Button.ts":
/*!***************************************!*\
  !*** ./schema-editor/model/Button.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Button = (function () {
        function Button(selector, callback, thisWidget) {
            this.button = $(selector);
            this.button.on('click', function (event) {
                callback(event, thisWidget);
            });
            this.widget = thisWidget;
        }
        return Button;
    }());
    exports.Button = Button;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/ErrorBlock.ts":
/*!*******************************************!*\
  !*** ./schema-editor/model/ErrorBlock.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ErrorBlock = (function () {
        function ErrorBlock(oModel) {
            this.model = oModel;
            this.element = $('.error_block');
            this.error_ul = $('.error_ul');
        }
        ErrorBlock.prototype.clear = function () {
            this.error_ul.html('');
        };
        ErrorBlock.prototype.hide = function () {
            this.clear();
            this.element.fadeOut(200);
        };
        ErrorBlock.prototype.show = function (data) {
            var _this = this;
            console.log('ErrorBlock.show', data.errors);
            var aKeys = this.model.form.getFieldNames();
            var aErrorList = [];
            aKeys.forEach(function (field_name) {
                if (typeof (data.errors[field_name]) != 'undefined') {
                    _this.model.form.getFieldManager()[field_name].addClass('error');
                    var element = document.createElement('li');
                    element.innerHTML = data.errors[field_name];
                    console.log('ErrorBlock.show', 'element.toString()', element.toString());
                    aErrorList.push(element.outerHTML);
                }
            });
            console.log('ErrorBlock.show', aErrorList);
            this.error_ul.html(aErrorList.join("\n"));
            this.element.fadeIn(200);
            $('.error').on('focus', function (e) {
                $(_this).removeClass('error');
            });
        };
        return ErrorBlock;
    }());
    exports.ErrorBlock = ErrorBlock;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/Messenger.ts":
/*!******************************************!*\
  !*** ./schema-editor/model/Messenger.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Message = (function () {
        function Message() {
        }
        return Message;
    }());
    exports.Message = Message;
    var MessageHandler = (function () {
        function MessageHandler(topic, callback) {
            this.topic = topic;
            this.callback = callback;
        }
        MessageHandler.prototype.handle = function (message) {
            this.callback(message);
        };
        return MessageHandler;
    }());
    var MessageDispenser = (function () {
        function MessageDispenser() {
        }
        MessageDispenser.addListener = function (handler) {
            MessageDispenser.aHandlers.push(handler);
        };
        MessageDispenser.distribute = function (rawMessage) {
            var message;
            for (var i = 0; i < MessageDispenser.aHandlers.length; i++) {
                var handler = MessageDispenser.aHandlers[i];
                message = new Message();
                message.topic = rawMessage.topic;
                message.content = rawMessage.data;
                if (handler.topic == message.topic) {
                    handler.handle(message);
                }
            }
        };
        MessageDispenser.aHandlers = [];
        return MessageDispenser;
    }());
    var Messenger = (function () {
        function Messenger() {
        }
        Messenger.startListener = function () {
            Messenger.isListening = true;
            console.log('Start listening');
            var callback = function (messageEvent) {
                if (typeof messageEvent.data !== "undefined") {
                    try {
                        console.log('type', typeof messageEvent.data);
                        if (typeof messageEvent.data == 'string') {
                            var rawMessage = JSON.parse(messageEvent.data);
                            MessageDispenser.distribute(rawMessage);
                        }
                    }
                    catch (e) {
                        console.error("Could not parse message" + messageEvent.data);
                        console.error(e.toString());
                    }
                }
            };
            console.log('Adding EventListener');
            window.addEventListener('message', callback);
        };
        Messenger.send = function (topic, content) {
            console.log('Messenger.send(topic, content)', topic, content);
            var oMessage = {
                topic: topic,
                content: content
            };
            var oErdEditor = document.getElementById('erd_editor_window');
            oErdEditor.contentWindow.postMessage(JSON.stringify(oMessage), '*');
        };
        Messenger.onReceive = function (topic, handler) {
            if (!Messenger.isListening) {
                console.log('Startig EventListener');
                Messenger.startListener();
                console.log('EventListener started');
            }
            console.log('Add event listener for: ' + topic);
            var messageHandler = new MessageHandler(topic, handler);
            MessageDispenser.addListener(messageHandler);
        };
        return Messenger;
    }());
    exports.Messenger = Messenger;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/Model.ts":
/*!**************************************!*\
  !*** ./schema-editor/model/Model.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GenericModel = (function () {
        function GenericModel() {
        }
        return GenericModel;
    }());
    exports.GenericModel = GenericModel;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/ModelForm.ts":
/*!******************************************!*\
  !*** ./schema-editor/model/ModelForm.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../contracts/Form */ "./schema-editor/contracts/Form.ts"), __webpack_require__(/*! ./Model */ "./schema-editor/model/Model.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, Form_1, Model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ModelModel = (function (_super) {
        __extends(ModelModel, _super);
        function ModelModel() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.id = null;
            return _this;
        }
        return ModelModel;
    }(Model_1.GenericModel));
    exports.ModelModel = ModelModel;
    var ModelFormFields = (function (_super) {
        __extends(ModelFormFields, _super);
        function ModelFormFields() {
            var _this = _super.call(this) || this;
            _this.name = $('#fld_name');
            _this.title = $('#fld_title');
            _this.module_id = $('#fld_module_id');
            _this.create_new_module = $('#fld_create_new_module');
            _this.new_module_name = $('#fld_new_module');
            return _this;
        }
        return ModelFormFields;
    }(ModelModel));
    exports.ModelFormFields = ModelFormFields;
    var EditModelFormFields = (function (_super) {
        __extends(EditModelFormFields, _super);
        function EditModelFormFields() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditModelFormFields;
    }(ModelFormFields));
    exports.EditModelFormFields = EditModelFormFields;
    var ModelValues = (function (_super) {
        __extends(ModelValues, _super);
        function ModelValues() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ModelValues;
    }(ModelModel));
    var NewModelForm = (function (_super) {
        __extends(NewModelForm, _super);
        function NewModelForm() {
            var _this = _super.call(this, new ModelFormFields()) || this;
            _this.fields = new ModelFormFields();
            _this.fields.name.on('keyup', _this.modelNameFilter);
            _this.fields.module_id.on('change', _this.toggleCreateNewModule);
            return _this;
        }
        NewModelForm.prototype.getData = function () {
            return undefined;
        };
        NewModelForm.prototype.toggleCreateNewModule = function () {
            var bCreateNewModule = ($('option:selected', this.fields.module_id).data('action') === '_create_new_') ? 1 : 0;
            var new_module_field_block = $('#new_module_field_block');
            new_module_field_block.css({
                'display': bCreateNewModule ? 'block' : 'none'
            });
            this.fields.create_new_module.val(bCreateNewModule);
            return false;
        };
        NewModelForm.prototype.modelNameFilter = function (event) {
            event.preventDefault();
            var newVal;
            newVal = this.fields.name.val()
                .toString()
                .toLowerCase()
                .replace(/^[^a-z]/, '')
                .replace(/^[^a-z0-9_]+$/, '');
            this.fields.name.val(newVal);
            return false;
        };
        return NewModelForm;
    }(Form_1.AbstractForm));
    exports.NewModelForm = NewModelForm;
    var EditModelForm = (function (_super) {
        __extends(EditModelForm, _super);
        function EditModelForm() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditModelForm;
    }(NewModelForm));
    exports.EditModelForm = EditModelForm;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/PropertyForm.ts":
/*!*********************************************!*\
  !*** ./schema-editor/model/PropertyForm.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../contracts/Form */ "./schema-editor/contracts/Form.ts"), __webpack_require__(/*! ./Model */ "./schema-editor/model/Model.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, Form_1, Model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PropertyModel = (function (_super) {
        __extends(PropertyModel, _super);
        function PropertyModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PropertyModel;
    }(Model_1.GenericModel));
    exports.PropertyModel = PropertyModel;
    var PropertyValues = (function (_super) {
        __extends(PropertyValues, _super);
        function PropertyValues() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PropertyValues;
    }(PropertyModel));
    exports.PropertyValues = PropertyValues;
    var PropertyFormFields = (function (_super) {
        __extends(PropertyFormFields, _super);
        function PropertyFormFields() {
            var _this = _super.call(this) || this;
            _this.label = $('#fld_label');
            _this.name = $('#fld_name');
            _this.form_field_type_id = $('#fld_form_field_type_id');
            _this.id = $('#fld_id');
            _this.data_type_id = $('#fld_data_type_id');
            _this.form_field_lookups = $('#fld_form_field_lookups');
            _this.required = $('#fld_required');
            _this.is_unique = $('#fld_is_unique');
            _this.is_primary_key = $('#fld_is_primary_key');
            _this.icon = $('#fld_icon');
            _this.fk_model_field = $('#fld_fk_model_field');
            _this.fk_model = $('#fld_fk_model');
            return _this;
        }
        return PropertyFormFields;
    }(PropertyModel));
    exports.PropertyFormFields = PropertyFormFields;
    var EditPropertyFormFields = (function (_super) {
        __extends(EditPropertyFormFields, _super);
        function EditPropertyFormFields() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditPropertyFormFields;
    }(PropertyFormFields));
    exports.EditPropertyFormFields = EditPropertyFormFields;
    var NewPropertyForm = (function (_super) {
        __extends(NewPropertyForm, _super);
        function NewPropertyForm() {
            var _this = _super.call(this, new PropertyFormFields()) || this;
            console.log('NewPropertyForm.constructor');
            _this.fields = new PropertyFormFields();
            return _this;
        }
        NewPropertyForm.prototype.getFieldManager = function () {
            console.log('NewPropertyForm.getFieldManager');
            return new PropertyFormFields();
        };
        NewPropertyForm.prototype.getData = function () {
            console.log('NewPropertyForm.syncData()');
            var aData = _super.prototype.syncData.call(this, new PropertyValues());
            console.log("\t", "fields", this.fields);
            console.log("\t", "data", aData);
            aData['table_name'] = this.table_name;
            return aData;
        };
        return NewPropertyForm;
    }(Form_1.AbstractForm));
    exports.NewPropertyForm = NewPropertyForm;
    var EditPropertyForm = (function (_super) {
        __extends(EditPropertyForm, _super);
        function EditPropertyForm() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditPropertyForm;
    }(NewPropertyForm));
    exports.EditPropertyForm = EditPropertyForm;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/model/Template.ts":
/*!*****************************************!*\
  !*** ./schema-editor/model/Template.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TypeType;
    (function (TypeType) {
        TypeType["popup"] = "popup";
        TypeType["panel"] = "panel";
    })(TypeType = exports.TypeType || (exports.TypeType = {}));
    var SubjectType;
    (function (SubjectType) {
        SubjectType["model"] = "model";
        SubjectType["property"] = "property";
    })(SubjectType = exports.SubjectType || (exports.SubjectType = {}));
    var Template = (function () {
        function Template() {
        }
        Template.getForm = function (widget) {
            var oData = {
                _do: 'GetForm',
                type: widget.type,
                subject: widget.subject
            };
            $.get('/modeller/schema/show', oData, function (data) {
                widget.template = data.form;
            }, 'json').then(function () {
                console.log("Template loaded " + widget.type + ' ' + widget.subject);
            });
        };
        return Template;
    }());
    exports.Template = Template;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/widget/Editor.ts":
/*!****************************************!*\
  !*** ./schema-editor/widget/Editor.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./ModelPanel */ "./schema-editor/widget/ModelPanel.ts"), __webpack_require__(/*! ./PropertyPanel */ "./schema-editor/widget/PropertyPanel.ts"), __webpack_require__(/*! ./ModelPopup */ "./schema-editor/widget/ModelPopup.ts"), __webpack_require__(/*! ./PropertyPopup */ "./schema-editor/widget/PropertyPopup.ts"), __webpack_require__(/*! ../model/Messenger */ "./schema-editor/model/Messenger.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ModelPanel_1, PropertyPanel_1, ModelPopup_1, PropertyPopup_1, Messenger_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Editor = (function () {
        function Editor() {
        }
        Editor.init = function () {
            Editor.modelPanel = new ModelPanel_1.ModelPanel(this);
            Editor.modelPopup = new ModelPopup_1.ModelPopup(this);
            Editor.propertyPanel = new PropertyPanel_1.PropertyPanel(this);
            Editor.propertyPopup = new PropertyPopup_1.PropertyPopup(this);
        };
        Editor.start = function () {
            this.init();
            Editor.modelPanel.hide();
            Editor.modelPopup.hide();
            Editor.propertyPanel.hide();
            Editor.propertyPopup.hide();
            Messenger_1.Messenger.onReceive('add_model', Editor.showAddModelPopup);
            Messenger_1.Messenger.onReceive('edit_model', Editor.showEditModelPanel);
            Messenger_1.Messenger.onReceive('add_property', Editor.showAddPropertyPopup);
            Messenger_1.Messenger.onReceive('edit_property', Editor.showEditPropertyPanel);
        };
        Editor.getAllPanels = function () {
            return [Editor.modelPanel, Editor.modelPopup, Editor.propertyPanel, Editor.propertyPopup];
        };
        Editor.activePanel = function (newActivePanel) {
            var aAllViews = Editor.getAllPanels();
            for (var i = 0; i < aAllViews.length; i++) {
                aAllViews[i].hide(true);
            }
            console.log('Editor.activePanel.newActivePanel', newActivePanel);
            newActivePanel.show();
        };
        Editor.showEditPropertyPanel = function (message) {
            console.log('Editor.showEditPropertyPanel.message', message);
            Editor.activePanel(Editor.propertyPanel);
            Editor.propertyPanel.setData(message.content);
        };
        Editor.showEditModelPanel = function (message) {
            console.log('Editor.showEditModelPanel.message', message);
            Editor.activePanel(Editor.modelPanel);
            Editor.modelPanel.setData(message.content);
        };
        Editor.showAddPropertyPopup = function (message) {
            console.log('Editor.showAddPropertyPopup', message.content);
            Editor.activePanel(Editor.propertyPopup);
            Editor.propertyPopup.setData(message.content);
        };
        Editor.showAddModelPopup = function () {
            console.log('Editor.showAddModelPopup');
            Editor.activePanel(Editor.modelPopup);
        };
        Editor.onPropertyDeleted = function (data) {
            console.log('Editor.onPropertyDeleted', data);
        };
        Editor.onPropertySaved = function (data) {
            console.log('Editor.onPropertySaved', data);
        };
        Editor.onPropertyCreated = function (data) {
            console.log('Editor.onPropertyCreated', data);
            console.log('Editor.onPropertyCreated', 'Editor.propertyPopup.hide()');
            Editor.propertyPopup.hide();
            Messenger_1.Messenger.send('property_added', data.content);
            console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.show()');
            Editor.propertyPanel.show();
            console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.setData(data.content);', data.content);
            Editor.propertyPanel.setData(data.content);
        };
        Editor.onModelCreated = function (data) {
            console.log('Editor.onModelCreated', data);
            Messenger_1.Messenger.send('model_added', data);
            Editor.modelPopup.hide();
            Editor.modelPanel.show();
            Editor.modelPanel.setData(data.content);
        };
        return Editor;
    }());
    exports.Editor = Editor;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/widget/ModelPanel.ts":
/*!********************************************!*\
  !*** ./schema-editor/widget/ModelPanel.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../model/ModelForm */ "./schema-editor/model/ModelForm.ts"), __webpack_require__(/*! ../contracts/AbstractWidget */ "./schema-editor/contracts/AbstractWidget.ts"), __webpack_require__(/*! ../model/Template */ "./schema-editor/model/Template.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ModelForm_1, AbstractWidget_1, Template_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ModelPanel = (function (_super) {
        __extends(ModelPanel, _super);
        function ModelPanel(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#edit_model_panel';
            _this.type = Template_1.TypeType.panel;
            _this.subject = Template_1.SubjectType.model;
            _this.editor = editor;
            _this.form = new ModelForm_1.EditModelForm();
            Template_1.Template.getForm(_this);
            return _this;
        }
        ModelPanel.prototype.bindEvents = function () {
            return this;
        };
        return ModelPanel;
    }(AbstractWidget_1.AbstractWidget));
    exports.ModelPanel = ModelPanel;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/widget/ModelPopup.ts":
/*!********************************************!*\
  !*** ./schema-editor/widget/ModelPopup.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../model/ErrorBlock */ "./schema-editor/model/ErrorBlock.ts"), __webpack_require__(/*! ../model/Button */ "./schema-editor/model/Button.ts"), __webpack_require__(/*! ../model/ModelForm */ "./schema-editor/model/ModelForm.ts"), __webpack_require__(/*! ../contracts/AbstractWidget */ "./schema-editor/contracts/AbstractWidget.ts"), __webpack_require__(/*! ./Editor */ "./schema-editor/widget/Editor.ts"), __webpack_require__(/*! ../model/Template */ "./schema-editor/model/Template.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ErrorBlock_1, Button_1, ModelForm_1, AbstractWidget_1, Editor_1, Template_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ModelPopup = (function (_super) {
        __extends(ModelPopup, _super);
        function ModelPopup(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#add_model_popup';
            _this.create = function (event, selfWidget) {
                var error_block = new ErrorBlock_1.ErrorBlock(_this);
                error_block.clear();
                event.preventDefault();
                var oData = {
                    '_do': 'CreateModel',
                    'data': selfWidget.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_1.Editor.onModelCreated(data);
                    }
                }, 'json');
            };
            _this.type = Template_1.TypeType.popup;
            _this.subject = Template_1.SubjectType.model;
            _this.editor = editor;
            _this.form = new ModelForm_1.NewModelForm();
            Template_1.Template.getForm(_this);
            return _this;
        }
        ModelPopup.prototype.bindEvents = function () {
            this.create_button = new Button_1.Button('#fld_create_model', this.create, this);
            return this;
        };
        ModelPopup.prototype.show = function () {
            this.getPlaceHolder().replaceWith(this.template);
            this.bindEvents();
            this.form = new ModelForm_1.NewModelForm();
            return this;
        };
        return ModelPopup;
    }(AbstractWidget_1.AbstractWidget));
    exports.ModelPopup = ModelPopup;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/widget/PropertyPanel.ts":
/*!***********************************************!*\
  !*** ./schema-editor/widget/PropertyPanel.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../model/PropertyForm */ "./schema-editor/model/PropertyForm.ts"), __webpack_require__(/*! ../contracts/AbstractWidget */ "./schema-editor/contracts/AbstractWidget.ts"), __webpack_require__(/*! ./Editor */ "./schema-editor/widget/Editor.ts"), __webpack_require__(/*! ../model/Template */ "./schema-editor/model/Template.ts"), __webpack_require__(/*! ../model/Button */ "./schema-editor/model/Button.ts"), __webpack_require__(/*! ../Ajax */ "./schema-editor/Ajax.ts"), __webpack_require__(/*! ../model/ErrorBlock */ "./schema-editor/model/ErrorBlock.ts"), __webpack_require__(/*! ../model/Messenger */ "./schema-editor/model/Messenger.ts"), __webpack_require__(/*! ../../EventHandler */ "./EventHandler.ts"), __webpack_require__(/*! ../actions/DeleteProperty */ "./schema-editor/actions/DeleteProperty.ts"), __webpack_require__(/*! ../../EventStack */ "./EventStack.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, PropertyForm_1, AbstractWidget_1, Editor_1, Template_1, Button_1, Ajax_1, ErrorBlock_1, Messenger_1, EventHandler_1, DeleteProperty_1, EventStack_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PropertyPanel = (function (_super) {
        __extends(PropertyPanel, _super);
        function PropertyPanel(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#edit_property_panel';
            _this.save = function (event, selfClass) {
                console.log('PropertyPanel.save()');
                event.preventDefault();
                console.log("Use error_block");
                console.log('this', _this);
                var error_block = new ErrorBlock_1.ErrorBlock(_this);
                var oData = {
                    '_do': 'CreateProperty',
                    'data': selfClass.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_1.Editor.onPropertyCreated({ content: data, topic: 'property' });
                    }
                }, 'json');
            };
            _this.confirmDelete = function (event, propertyPanel) {
                var deleteEvents = new EventStack_1.EventStack();
                deleteEvents.register(new DeleteProperty_1.DeleteProperty(), { endpoint: "/" }, { data: propertyPanel.form.getData() });
                console.log('PropertyPanel.delete()');
                event.preventDefault();
                Ajax_1.Ajax('DeleteProperty', _this, Editor_1.Editor.onPropertyDeleted);
                Messenger_1.Messenger.send('property_deleted', _this.form.getData());
            };
            _this.delete = function (event, propertyPanel) {
                console.log('PropertyPanel.delete()');
                console.log("\t", "propertyPanel", propertyPanel);
                console.log("\t", "propertyPanel.form", propertyPanel.form);
                console.log("\t", "propertyPanel.form.fields", propertyPanel.form.fields);
                console.log("\t", "propertyPanel.form.syncData()", propertyPanel.form.getData());
                EventHandler_1.EventHandler.trigger(new DeleteProperty_1.DeleteProperty(), { endpoint: "" }, { data: propertyPanel.form.getData(), widget: propertyPanel });
                event.preventDefault();
            };
            _this.editor = editor;
            _this.type = Template_1.TypeType.panel;
            _this.subject = Template_1.SubjectType.property;
            _this.form = new PropertyForm_1.EditPropertyForm();
            Template_1.Template.getForm(_this);
            return _this;
        }
        PropertyPanel.prototype.bindEvents = function () {
            return this;
        };
        PropertyPanel.prototype.show = function () {
            console.log('PropertyPanel.show()');
            _super.prototype.show.call(this);
            this.bindEvents();
            this.form = new PropertyForm_1.EditPropertyForm();
            console.log('PropertyPanel.show() -> bind-click-confirm-delete');
            this.confirm_delete_button = new Button_1.Button('#fld_confirm_delete_button', this.confirmDelete, this);
            console.log('PropertyPanel.show() -> bind-click-delete');
            this.delete_button = new Button_1.Button('#fld_delete_property', this.delete, this);
            console.log('PropertyPanel.show() -> bind-click-save');
            this.create_button = new Button_1.Button('#fld_save_property', this.save, this);
            return this;
        };
        PropertyPanel.prototype.setData = function (data) {
            console.log('PropertyPanel.setData', data);
            if (typeof data.table_name != 'undefined') {
                console.log("\t", 'this.form.table_name = ' + data.table_name);
                this.form.table_name = data.table_name;
            }
            _super.prototype.setData.call(this, data);
            return this;
        };
        return PropertyPanel;
    }(AbstractWidget_1.AbstractWidget));
    exports.PropertyPanel = PropertyPanel;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./schema-editor/widget/PropertyPopup.ts":
/*!***********************************************!*\
  !*** ./schema-editor/widget/PropertyPopup.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ../model/ErrorBlock */ "./schema-editor/model/ErrorBlock.ts"), __webpack_require__(/*! ../model/PropertyForm */ "./schema-editor/model/PropertyForm.ts"), __webpack_require__(/*! ../contracts/AbstractWidget */ "./schema-editor/contracts/AbstractWidget.ts"), __webpack_require__(/*! ./Editor */ "./schema-editor/widget/Editor.ts"), __webpack_require__(/*! ../model/Template */ "./schema-editor/model/Template.ts"), __webpack_require__(/*! ../model/Button */ "./schema-editor/model/Button.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ErrorBlock_1, PropertyForm_1, AbstractWidget_1, Editor_1, Template_1, Button_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PropertyPopup = (function (_super) {
        __extends(PropertyPopup, _super);
        function PropertyPopup(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#add_property_popup';
            _this.create = function (event, selfClass) {
                console.log("PropertyPopup.save");
                event.preventDefault();
                console.log("\t", "Use error_block");
                console.log("\t", 'this', _this);
                console.log("\t", 'form data', selfClass.form.getData());
                var error_block = new ErrorBlock_1.ErrorBlock(_this);
                var oData = {
                    '_do': 'CreateProperty',
                    'data': selfClass.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_1.Editor.onPropertyCreated({ content: data, topic: 'property' });
                    }
                }, 'json');
            };
            console.log('PropertyPopup.constructor');
            _this.editor = editor;
            _this.type = Template_1.TypeType.popup;
            _this.subject = Template_1.SubjectType.property;
            Template_1.Template.getForm(_this);
            return _this;
        }
        PropertyPopup.prototype.bindEvents = function () {
            console.log('bind #fld_create_property');
            this.create_button = new Button_1.Button('#fld_create_property', this.create, this);
            this.fkToggler();
            return this;
        };
        PropertyPopup.prototype.fkToggler = function () {
            var oModelBlock = $('#blk_fk_model');
            var oModelField = $('#fld_fk_model');
            var oModelFieldField = $('#fld_fk_model_field');
            var oModelFieldBlock = $('#blk_fk_model_field');
            var oFormFieldType = $('#fld_form_field_type_id');
            var loadModelFields = function () {
                if (oModelField.val()) {
                    var oData = {
                        _do: 'GetModelFields',
                        model_id: oModelField.val()
                    };
                    $.get(window.location.href, oData, function (data) {
                        console.log('data', data);
                        oModelFieldField.html('');
                        data.forEach(function (option) {
                            oModelFieldField.append('<option>' + option.label + '</option>')
                                .attr('value', option.id);
                        });
                        console.log(data);
                    }, 'json').then(function () {
                        console.log("Model fields loaded");
                    });
                    oModelFieldBlock.css('display', 'block');
                }
                else {
                    oModelFieldBlock.css('display', 'none');
                }
            };
            oModelField.on('change', function () {
                console.log('chaaaaaaaaaaa');
                loadModelFields();
            });
            var toggleBlocks = function () {
                var isFk = (oFormFieldType.find(':selected').data('is-foreign-key') === 1);
                oModelBlock.css('display', isFk ? 'block' : 'none');
                loadModelFields();
            };
            console.log('PropertyPopup.fkToggle');
            toggleBlocks();
            oFormFieldType.on('change', function () {
                toggleBlocks();
            });
            return this;
        };
        PropertyPopup.prototype.setData = function (data) {
            console.log('PropertyPopup.setData', data);
            if (typeof data.table_name != 'undefined') {
                this.form.table_name = data.table_name;
            }
            _super.prototype.setData.call(this, data);
            return this;
        };
        PropertyPopup.prototype.show = function () {
            console.log('PropertyPopup.show');
            console.log("\t", 'template', this.template);
            this.getPlaceHolder().replaceWith(this.template);
            this.bindEvents();
            this.form = new PropertyForm_1.NewPropertyForm();
            return this;
        };
        PropertyPopup.prototype.hide = function (immediate) {
            if (immediate === void 0) { immediate = false; }
            if (typeof window['closePopup'] === "function") {
                window['closePopup']();
            }
            this.getPanel().replaceWith(this.createPlaceholder());
            return this;
        };
        return PropertyPopup;
    }(AbstractWidget_1.AbstractWidget));
    exports.PropertyPopup = PropertyPopup;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vRXZlbnRIYW5kbGVyLnRzIiwid2VicGFjazovLy8uL0V2ZW50U3RhY2sudHMiLCJ3ZWJwYWNrOi8vLy4vZXZlbnQvQWJzdHJhY3RFdmVudC50cyIsIndlYnBhY2s6Ly8vLi9ldmVudC9JRXZlbnQudHMiLCJ3ZWJwYWNrOi8vLy4vZXZlbnQvdHlwZXMvU3RhdHVzTWVzc2FnZS50cyIsIndlYnBhY2s6Ly8vLi9tYWluLnRzIiwid2VicGFjazovLy8uL3NjaGVtYS1lZGl0b3IvQWpheC50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL2FjdGlvbnMvRGVsZXRlUHJvcGVydHkudHMiLCJ3ZWJwYWNrOi8vLy4vc2NoZW1hLWVkaXRvci9jb250cmFjdHMvQWJzdHJhY3RXaWRnZXQudHMiLCJ3ZWJwYWNrOi8vLy4vc2NoZW1hLWVkaXRvci9jb250cmFjdHMvRm9ybS50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL21vZGVsL0J1dHRvbi50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL21vZGVsL0Vycm9yQmxvY2sudHMiLCJ3ZWJwYWNrOi8vLy4vc2NoZW1hLWVkaXRvci9tb2RlbC9NZXNzZW5nZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc2NoZW1hLWVkaXRvci9tb2RlbC9Nb2RlbC50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL21vZGVsL01vZGVsRm9ybS50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL21vZGVsL1Byb3BlcnR5Rm9ybS50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL21vZGVsL1RlbXBsYXRlLnRzIiwid2VicGFjazovLy8uL3NjaGVtYS1lZGl0b3Ivd2lkZ2V0L0VkaXRvci50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL3dpZGdldC9Nb2RlbFBhbmVsLnRzIiwid2VicGFjazovLy8uL3NjaGVtYS1lZGl0b3Ivd2lkZ2V0L01vZGVsUG9wdXAudHMiLCJ3ZWJwYWNrOi8vLy4vc2NoZW1hLWVkaXRvci93aWRnZXQvUHJvcGVydHlQYW5lbC50cyIsIndlYnBhY2s6Ly8vLi9zY2hlbWEtZWRpdG9yL3dpZGdldC9Qcm9wZXJ0eVBvcHVwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7OztJQy9FQTtRQUFBO1FBZUEsQ0FBQztRQVpVLG9CQUFPLEdBQWQsVUFBK0UsS0FBUyxFQUFFLEtBQXFCLEVBQUUsS0FBdUI7WUFFcEksT0FBTyxJQUFJLE9BQU8sQ0FBa0IsVUFBQyxPQUFhLEVBQUUsTUFBWTtnQkFFNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUNwQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztZQUVoRSxDQUFDLENBQUM7UUFJTixDQUFDO1FBQ0wsbUJBQUM7SUFBRCxDQUFDO0lBZlksb0NBQVk7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDRXpCO1FBQUE7WUFBQSxpQkFnQkM7WUFQRyxZQUFPLEdBQUc7Z0JBRU4sS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxLQUFLO29CQUNyQiwyQkFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsRSxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7UUFFTCxDQUFDO1FBWkcsNkJBQVEsR0FBUixVQUFTLEtBQVMsRUFBRSxLQUFxQixFQUFFLEtBQXVCO1lBRTlELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUcsS0FBSyxFQUFFLEtBQUssRUFBRyxLQUFLLEVBQUMsQ0FBQztRQUNuRSxDQUFDO1FBU0wsaUJBQUM7SUFBRCxDQUFDO0lBaEJZLGdDQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0F2QjtRQUFBO1FBd0RBLENBQUM7UUE5Q0csbUNBQVcsR0FBWDtZQUVJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDO1FBQ0Qsb0NBQVksR0FBWjtZQUVJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDO1FBQ0Qsd0NBQWdCLEdBQWhCLFVBQWlCLE1BQVksRUFBRSxLQUFjLEVBQUUsT0FBZ0I7WUFBL0QsaUJBcUNDO1lBbkNHLE9BQU8sVUFBTyxVQUFlOzs7b0JBR3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3BCLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBRWxCLElBQUcsVUFBVSxDQUFDLE1BQU0sRUFDcEI7d0JBQ0ksTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7cUJBQzlCO29CQUVELElBQUcsTUFBTSxFQUNUO3dCQUVRLE1BQU0sR0FBeUI7NEJBQy9CLEtBQUssRUFBRyxLQUFLOzRCQUNiLE1BQU0sRUFBRyxNQUFNOzRCQUNmLFNBQVMsRUFBRyxjQUFjOzRCQUMxQixrQkFBa0IsRUFBRyxtQ0FBbUIsQ0FBQyxPQUFPO3lCQUNuRCxDQUFDO3dCQUdGLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO3FCQUN2Qzt5QkFFRDt3QkFDUSxNQUFNLEdBQWU7NEJBQ3JCLEtBQUssRUFBRyxLQUFLOzRCQUNiLE9BQU8sRUFBRyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUM7NEJBQ3JELFNBQVMsRUFBRyxjQUFjOzRCQUMxQixrQkFBa0IsRUFBRyxtQ0FBbUIsQ0FBQyxPQUFPO3lCQUNuRCxDQUFDO3FCQUNMOzs7aUJBRUosQ0FBQztRQUNOLENBQUM7UUFDTCxvQkFBQztJQUFELENBQUM7SUF4RHFCLHNDQUFhOzs7Ozs7Ozs7Ozs7Ozs7OztJQ0xuQztRQUFBO1FBQXVCLENBQUM7UUFBRCxnQkFBQztJQUFELENBQUM7SUFBWCw4QkFBUztJQUN0QjtRQUFBO1FBQXdCLENBQUM7UUFBRCxnQkFBQztJQUFELENBQUM7SUFBWiw4QkFBUzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNJdEIsSUFBWSxtQkFNWDtJQU5ELFdBQVksbUJBQW1CO1FBQzNCLG9DQUFhO1FBQ2IsMENBQW1CO1FBQ25CLHNDQUFlO1FBQ2YsMENBQW1CO1FBQ25CLHdDQUFpQjtJQUNyQixDQUFDLEVBTlcsbUJBQW1CLEdBQW5CLDJCQUFtQixLQUFuQiwyQkFBbUIsUUFNOUI7SUFTRDtRQUFBO1FBTUEsQ0FBQztRQUFELGtCQUFDO0lBQUQsQ0FBQztJQU5ZLGtDQUFXO0lBT3hCO1FBQUE7UUFNQSxDQUFDO1FBQUQsNEJBQUM7SUFBRCxDQUFDO0lBTlksc0RBQXFCOzs7Ozs7Ozs7Ozs7Ozs7OztJQ1BsQyxlQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ2hCZixJQUFZLFVBTVg7SUFORCxXQUFZLFVBQVU7UUFDbEIsK0NBQU07UUFDTiwyQ0FBSTtRQUNKLHlDQUFHO1FBQ0gsNkNBQUs7UUFDTCx5Q0FBRztJQUNQLENBQUMsRUFOVyxVQUFVLEdBQVYsa0JBQVUsS0FBVixrQkFBVSxRQU1yQjtJQWNEO1FBQUE7UUFHQSxDQUFDO1FBQUQsZ0JBQUM7SUFBRCxDQUFDO0lBSFksOEJBQVM7SUFJdEI7UUFBQTtRQUVBLENBQUM7UUFBRCxnQkFBQztJQUFELENBQUM7SUFGWSw4QkFBUztJQUd0QjtRQUFtSCx3QkFBdUI7UUFBMUk7O1FBcUNBLENBQUM7UUE5Qkcsc0JBQU8sR0FBUCxVQUFRLEtBQVEsRUFBRSxLQUFRO1lBQ3RCLE9BQU8sSUFBSSxPQUFPLENBQUssVUFBQyxPQUFPLEVBQUUsTUFBTTtnQkFFbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFNUQsSUFBSSxLQUFLLEdBQUc7b0JBQ1IsS0FBSyxFQUFFLEtBQUssQ0FBQyxNQUFNO29CQUNuQixNQUFNLEVBQUUsS0FBSyxDQUFDLElBQUk7aUJBQ3JCLENBQUM7Z0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN2RCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLFVBQUMsSUFBSTtvQkFFNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBRTNDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFDZjt3QkFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN2Qjt5QkFFRDt3QkFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2pCO2dCQUVMLENBQUMsRUFBRSxNQUFNLENBQUM7cUJBQ1QsSUFBSSxDQUFDO29CQUNGLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUMvQixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBRVAsQ0FBQztRQUNMLFdBQUM7SUFBRCxDQUFDLENBckNrSCw2QkFBYSxHQXFDL0g7SUFyQ1ksb0JBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ3pCakI7UUFBa0MsdUNBQVM7UUFBM0M7O1FBSUEsQ0FBQztRQUFELDBCQUFDO0lBQUQsQ0FBQyxDQUppQyxrQkFBUyxHQUkxQztJQUVEO1FBQWtDLHVDQUFTO1FBQTNDOztRQUVBLENBQUM7UUFBRCwwQkFBQztJQUFELENBQUMsQ0FGaUMsa0JBQVMsR0FFMUM7SUFFRDtRQUFBO1FBV0EsQ0FBQztRQVRHLDJDQUFPLEdBQVA7UUFHQSxDQUFDO1FBQ0QsOENBQVUsR0FBVixVQUFXLFFBQTJCO1FBQ3RDLENBQUM7UUFFRCwrQ0FBVyxHQUFYLFVBQVksUUFBMkI7UUFDdkMsQ0FBQztRQUNMLGdDQUFDO0lBQUQsQ0FBQztJQUNEO1FBQXdJLGtDQUF1QjtRQUEvSjs7UUFrQ0EsQ0FBQztRQTdCRyxnQ0FBTyxHQUFQLFVBQVEsS0FBUSxFQUFFLEtBQVE7WUFFdEIsT0FBTyxJQUFJLE9BQU8sQ0FBSyxVQUFDLE9BQU8sRUFBRSxNQUFNO2dCQUVuQywyQkFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLFdBQUksRUFBRSxFQUFFLEVBQUMsUUFBUSxFQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUMsRUFBRSxFQUFDLElBQUksRUFBRyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQyxDQUFDO3FCQUNySCxJQUFJLENBQUMsZUFBSztvQkFDUCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ25CLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsZUFBSztvQkFDUixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2xCLENBQUMsQ0FBQyxDQUFDO1lBZVgsQ0FBQyxDQUFDLENBQUM7UUFHUCxDQUFDO1FBQ0wscUJBQUM7SUFBRCxDQUFDLENBbEN1SSw2QkFBYSxHQWtDcEo7SUFsQ1ksd0NBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDdkIzQjtRQUFBO1FBR0EsQ0FBQztRQUFELGFBQUM7SUFBRCxDQUFDO0lBQ0Q7UUFVSTtZQUZRLGdCQUFXLEdBQWMsRUFBRSxDQUFDO1lBSWhDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSwrQkFBZ0IsRUFBRSxDQUFDO1FBQ3ZDLENBQUM7UUFDRCx1Q0FBYyxHQUFkO1lBRUksT0FBTyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDLENBQUM7UUFDL0MsQ0FBQztRQUVELDBDQUFpQixHQUFqQjtZQUVJLElBQUksSUFBSSxHQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxjQUFjLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDNUIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELGdDQUFPLEdBQVAsVUFBUSxJQUE4QjtZQUVsQyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxDQUFDO1lBRTVDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDNUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUM1QyxJQUFJLFNBQWtCLENBQUM7WUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQzFDO2dCQUNJLFNBQVMsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBRXRDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFFbkMsSUFBRyxPQUFPLFNBQVMsS0FBSyxXQUFXLEVBQ25DO29CQUNJLFNBQVM7aUJBQ1o7cUJBQ0ksSUFBRyxPQUFPLElBQUksS0FBSyxXQUFXLEVBQ25DO29CQUNJLFNBQVM7aUJBQ1o7cUJBQ0ksSUFBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEVBQ3ZDO29CQUNJLFNBQVM7aUJBQ1o7Z0JBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBQyxHQUFHLEVBQUcsU0FBUyxFQUFFLEtBQUssRUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUMsQ0FBQyxDQUFDO2dCQUNsRSxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2FBQzdDO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELG9DQUFXLEdBQVg7WUFFSSxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDakQsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELGlDQUFRLEdBQVI7WUFFSSxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN4RCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUIsQ0FBQztRQUNELDZCQUFJLEdBQUosVUFBSyxTQUEyQjtZQUFoQyxpQkFrQkM7WUFsQkksNkNBQTJCO1lBRTVCLElBQUksS0FBSyxHQUFVLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNuQyxJQUFHLEtBQUssQ0FBQyxNQUFNLEVBQ2Y7Z0JBQ0ksSUFBRyxTQUFTLEVBQ1o7b0JBQ0ksS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO2lCQUMvQztxQkFFRDtvQkFDSSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRTt3QkFFZixLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7b0JBQ2hELENBQUMsQ0FBQztpQkFDTDthQUNKO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELDZCQUFJLEdBQUo7WUFFSSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFFckMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFakQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRXZCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDNUMsSUFBSSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3BELElBQUksZ0JBQXVCLENBQUM7WUFDNUIsSUFBSSxlQUFzQixDQUFDO1lBQzNCLElBQUksS0FBWSxDQUFDO1lBRWpCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBRS9CLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUMxQztnQkFDSSxnQkFBZ0IsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFDaEQ7b0JBQ0ksZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUUxQyxJQUFHLGdCQUFnQixLQUFLLGVBQWUsRUFDdkM7d0JBR0ksS0FBSyxHQUFHLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLENBQUM7d0JBRTVDLElBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxFQUNoRTs0QkFDSSxTQUFTO3lCQUNaO3dCQUdELEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDeEM7aUJBQ0o7YUFDSjtZQUNELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNsQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0wscUJBQUM7SUFBRCxDQUFDO0lBcklxQix3Q0FBYzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNDcEM7UUFBQTtRQUVBLENBQUM7UUFBRCxrQkFBQztJQUFELENBQUM7SUFGWSxrQ0FBVztJQWN4QjtRQU1JLHNCQUFZLE1BQXdCO1lBRWhDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3pCLENBQUM7UUFFRCxvQ0FBYSxHQUFiO1lBRUksT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQyxDQUFDO1FBRUQsZ0NBQVMsR0FBVDtZQUVJLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUV4QyxJQUFJLElBQUksR0FBYyxFQUFFLENBQUM7WUFFekIsS0FBSyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2FBQ2xDO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELHNDQUFlLEdBQWY7WUFFSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkIsQ0FBQztRQUVELCtCQUFRLEdBQVIsVUFBWSxLQUFTO1lBQXJCLGlCQWFDO1lBWEcsSUFBTSxNQUFNLEdBQU8sS0FBSyxDQUFDO1lBRXpCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFTO2dCQUVuQyxJQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLE9BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksV0FBVyxFQUMvRjtvQkFFSSxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDL0Q7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sTUFBTSxDQUFDO1FBQ2xCLENBQUM7UUFFTCxtQkFBQztJQUFELENBQUM7SUFoRHFCLG9DQUFZOzs7Ozs7Ozs7Ozs7Ozs7OztJQ2pCbEM7UUFLSSxnQkFBWSxRQUFpQixFQUFFLFFBQW1CLEVBQUUsVUFBMkI7WUFFM0UsSUFBSSxDQUFDLE1BQU0sR0FBSSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsS0FBdUI7Z0JBQzVDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUM3QixDQUFDO1FBQ0wsYUFBQztJQUFELENBQUM7SUFiWSx3QkFBTTs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNGbkI7UUFNSSxvQkFBWSxNQUFnQjtZQUV4QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztZQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUVuQyxDQUFDO1FBQ0QsMEJBQUssR0FBTDtZQUVJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzNCLENBQUM7UUFDRCx5QkFBSSxHQUFKO1lBRUksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUNELHlCQUFJLEdBQUosVUFBSyxJQUE4QjtZQUFuQyxpQkF5QkM7WUF0QkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDNUMsSUFBSSxVQUFVLEdBQWMsRUFBRSxDQUFDO1lBRS9CLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFVO2dCQUNyQixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksV0FBVyxFQUFFO29CQUNqRCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ2hFLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxvQkFBb0IsRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztvQkFFekUsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3RDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFFSCxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUV6QixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxLQUFJLENBQUMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBQ0wsaUJBQUM7SUFBRCxDQUFDO0lBaERZLGdDQUFVOzs7Ozs7Ozs7Ozs7Ozs7OztJQ0p2QjtRQUFBO1FBR0EsQ0FBQztRQUFELGNBQUM7SUFBRCxDQUFDO0lBSFksMEJBQU87SUFjcEI7UUFLSSx3QkFBWSxLQUFjLEVBQUUsUUFBMEQ7WUFFbEYsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFFN0IsQ0FBQztRQUNELCtCQUFNLEdBQU4sVUFBTyxPQUEwQztZQUU3QyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNCLENBQUM7UUFFTCxxQkFBQztJQUFELENBQUM7SUFNRDtRQUFBO1FBd0JBLENBQUM7UUFyQmlCLDRCQUFXLEdBQXpCLFVBQTBCLE9BQXdCO1lBRTlDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0MsQ0FBQztRQUNhLDJCQUFVLEdBQXhCLFVBQXlCLFVBQXVCO1lBRTVDLElBQUksT0FBMEMsQ0FBQztZQUMvQyxLQUFLLElBQUksQ0FBQyxHQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFDekQ7Z0JBQ0ksSUFBTSxPQUFPLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUU5QyxPQUFPLEdBQUcsSUFBSSxPQUFPLEVBQTJCLENBQUM7Z0JBQ2pELE9BQU8sQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztnQkFDakMsT0FBTyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO2dCQUVsQyxJQUFHLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLEtBQUssRUFDakM7b0JBQ0ksT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDM0I7YUFDSjtRQUNMLENBQUM7UUF0QmMsMEJBQVMsR0FBc0IsRUFBRSxDQUFDO1FBdUJyRCx1QkFBQztLQUFBO0lBRUQ7UUFBQTtRQXdEQSxDQUFDO1FBcERrQix1QkFBYSxHQUE1QjtZQUNJLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUUvQixJQUFJLFFBQVEsR0FBRyxVQUFDLFlBQTBCO2dCQUV0QyxJQUFJLE9BQU8sWUFBWSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7b0JBQzFDLElBQ0E7d0JBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsT0FBTyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzlDLElBQUksT0FBTyxZQUFZLENBQUMsSUFBSSxJQUFJLFFBQVEsRUFBRTs0QkFFdEMsSUFBSSxVQUFVLEdBQWdCLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUM1RCxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQzNDO3FCQUNKO29CQUNELE9BQU8sQ0FBQyxFQUNSO3dCQUNJLE9BQU8sQ0FBQyxLQUFLLENBQUMseUJBQXlCLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUM3RCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO3FCQUMvQjtpQkFDSjtZQUNMLENBQUMsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztZQUNwQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2pELENBQUM7UUFFTSxjQUFJLEdBQVgsVUFBWSxLQUFhLEVBQUUsT0FBWTtZQUVuQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM5RCxJQUFJLFFBQVEsR0FBRztnQkFDWCxLQUFLLEVBQUUsS0FBSztnQkFDWixPQUFPLEVBQUUsT0FBTzthQUNuQixDQUFDO1lBRUYsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzNDLFVBQVcsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUYsQ0FBQztRQUVNLG1CQUFTLEdBQWhCLFVBQWlCLEtBQVksRUFBRSxPQUF3RDtZQUVuRixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2dCQUNyQyxTQUFTLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQzthQUN4QztZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDaEQsSUFBTSxjQUFjLEdBQUcsSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzFELGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUVqRCxDQUFDO1FBQ0wsZ0JBQUM7SUFBRCxDQUFDO0lBeERZLDhCQUFTOzs7Ozs7Ozs7Ozs7Ozs7OztJQy9EdEI7UUFBQTtRQUVBLENBQUM7UUFBRCxtQkFBQztJQUFELENBQUM7SUFGWSxvQ0FBWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDTXpCO1FBQW1DLDhCQUFlO1FBQWxEO1lBQUEscUVBU0M7WUFORyxRQUFFLEdBQU0sSUFBSSxDQUFDOztRQU1qQixDQUFDO1FBQUQsaUJBQUM7SUFBRCxDQUFDLENBVGtDLG9CQUFZLEdBUzlDO0lBVFksZ0NBQVU7SUFXdkI7UUFBcUMsbUNBQWtCO1FBQ25EO1lBQUEsWUFFSSxpQkFBTyxTQU1WO1lBTEcsS0FBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDM0IsS0FBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0IsS0FBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNyQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDckQsS0FBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQzs7UUFDaEQsQ0FBQztRQUNMLHNCQUFDO0lBQUQsQ0FBQyxDQVZvQyxVQUFVLEdBVTlDO0lBVlksMENBQWU7SUFXNUI7UUFBeUMsdUNBQWU7UUFBeEQ7O1FBQTBELENBQUM7UUFBRCwwQkFBQztJQUFELENBQUMsQ0FBbEIsZUFBZSxHQUFHO0lBQTlDLGtEQUFtQjtJQUNoQztRQUEwQiwrQkFBa0I7UUFBNUM7O1FBQTZDLENBQUM7UUFBRCxrQkFBQztJQUFELENBQUMsQ0FBcEIsVUFBVSxHQUFVO0lBRTlDO1FBQWtDLGdDQUE2QjtRQUUzRDtZQUFBLFlBQ0ksa0JBQU0sSUFBSSxlQUFlLEVBQUUsQ0FBQyxTQVEvQjtZQVBHLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQztZQUdwQyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUduRCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDOztRQUNuRSxDQUFDO1FBQ0QsOEJBQU8sR0FBUDtZQUNJLE9BQU8sU0FBUyxDQUFDO1FBQ3JCLENBQUM7UUFFTyw0Q0FBcUIsR0FBN0I7WUFDSSxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUUvRyxJQUFNLHNCQUFzQixHQUFHLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBRTVELHNCQUFzQixDQUFDLEdBQUcsQ0FDdEI7Z0JBQ0ksU0FBUyxFQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU07YUFDbEQsQ0FDSixDQUFDO1lBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNwRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBQ08sc0NBQWUsR0FBdkIsVUFBd0IsS0FBdUI7WUFDM0MsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRXZCLElBQUksTUFBZSxDQUFDO1lBQ3BCLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7aUJBQzFCLFFBQVEsRUFBRTtpQkFDVixXQUFXLEVBQUU7aUJBQ2IsT0FBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUM7aUJBQ3RCLE9BQU8sQ0FBQyxlQUFlLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFFbEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFDTCxtQkFBQztJQUFELENBQUMsQ0ExQ2lDLG1CQUFZLEdBMEM3QztJQTFDWSxvQ0FBWTtJQTJDekI7UUFBbUMsaUNBQVk7UUFBL0M7O1FBQWlFLENBQUM7UUFBRCxvQkFBQztJQUFELENBQUMsQ0FBL0IsWUFBWSxHQUFtQjtJQUFyRCxzQ0FBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDcEUxQjtRQUFzQyxpQ0FBZTtRQUFyRDs7UUFrQkEsQ0FBQztRQUFELG9CQUFDO0lBQUQsQ0FBQyxDQWxCcUMsb0JBQVksR0FrQmpEO0lBbEJZLHNDQUFhO0lBb0IxQjtRQUFvQyxrQ0FBcUI7UUFBekQ7O1FBQTBELENBQUM7UUFBRCxxQkFBQztJQUFELENBQUMsQ0FBdkIsYUFBYSxHQUFVO0lBQTlDLHdDQUFjO0lBRTNCO1FBQXdDLHNDQUFxQjtRQUV6RDtZQUFBLFlBRUksaUJBQU8sU0FlVjtZQWRHLEtBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzdCLEtBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUd2RCxLQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QixLQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzNDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUN2RCxLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3JDLEtBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0MsS0FBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDM0IsS0FBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUMvQyxLQUFJLENBQUMsUUFBUSxHQUFJLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQzs7UUFDeEMsQ0FBQztRQUNMLHlCQUFDO0lBQUQsQ0FBQyxDQXBCdUMsYUFBYSxHQW9CcEQ7SUFwQlksZ0RBQWtCO0lBcUIvQjtRQUE0QywwQ0FBa0I7UUFBOUQ7O1FBQ0EsQ0FBQztRQUFELDZCQUFDO0lBQUQsQ0FBQyxDQUQyQyxrQkFBa0IsR0FDN0Q7SUFEWSx3REFBc0I7SUFFbkM7UUFBcUMsbUNBQWdDO1FBSWpFO1lBQUEsWUFDSSxrQkFBTSxJQUFJLGtCQUFrQixFQUFFLENBQUMsU0FHbEM7WUFGRyxPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUM7WUFDM0MsS0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGtCQUFrQixFQUFFLENBQUM7O1FBQzNDLENBQUM7UUFFRCx5Q0FBZSxHQUFmO1lBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sSUFBSSxrQkFBa0IsRUFBRSxDQUFDO1FBQ3BDLENBQUM7UUFFRCxpQ0FBTyxHQUFQO1lBRUksT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1lBQzFDLElBQUksS0FBSyxHQUFHLGlCQUFNLFFBQVEsWUFBQyxJQUFJLGNBQWMsRUFBRSxDQUFDLENBQUM7WUFFakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFFakMsS0FBSyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDdEMsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUNMLHNCQUFDO0lBQUQsQ0FBQyxDQTFCb0MsbUJBQVksR0EwQmhEO0lBMUJZLDBDQUFlO0lBMkI1QjtRQUFzQyxvQ0FBZTtRQUFyRDs7UUFBdUUsQ0FBQztRQUFELHVCQUFDO0lBQUQsQ0FBQyxDQUFsQyxlQUFlLEdBQW1CO0lBQTNELDRDQUFnQjs7Ozs7Ozs7Ozs7Ozs7Ozs7SUN2RTdCLElBQVksUUFHWDtJQUhELFdBQVksUUFBUTtRQUNoQiwyQkFBZTtRQUNmLDJCQUFlO0lBQ25CLENBQUMsRUFIVyxRQUFRLEdBQVIsZ0JBQVEsS0FBUixnQkFBUSxRQUduQjtJQUNELElBQVksV0FHWDtJQUhELFdBQVksV0FBVztRQUNuQiw4QkFBZTtRQUNmLG9DQUFxQjtJQUN6QixDQUFDLEVBSFcsV0FBVyxHQUFYLG1CQUFXLEtBQVgsbUJBQVcsUUFHdEI7SUFFRDtRQUFBO1FBb0JBLENBQUM7UUFsQlUsZ0JBQU8sR0FBZCxVQUFlLE1BQWdCO1lBRTNCLElBQU0sS0FBSyxHQUFHO2dCQUNWLEdBQUcsRUFBRyxTQUFTO2dCQUNmLElBQUksRUFBRyxNQUFNLENBQUMsSUFBSTtnQkFDbEIsT0FBTyxFQUFHLE1BQU0sQ0FBQyxPQUFPO2FBQzNCLENBQUM7WUFFRixDQUFDLENBQUMsR0FBRyxDQUFDLHVCQUF1QixFQUFFLEtBQUssRUFBRSxVQUFDLElBQVk7Z0JBRS9DLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUVoQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUVaLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUVMLGVBQUM7SUFBRCxDQUFDO0lBcEJZLDRCQUFROzs7Ozs7Ozs7Ozs7Ozs7OztJQ1RyQjtRQUFBO1FBcUdBLENBQUM7UUE5RlUsV0FBSSxHQUFYO1lBRUksTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLHVCQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekMsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLHVCQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekMsTUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLDZCQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLDZCQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQztRQUNNLFlBQUssR0FBWjtZQUVJLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekIsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV6QixNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzVCLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFNUIscUJBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzNELHFCQUFTLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUU3RCxxQkFBUyxDQUFDLFNBQVMsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDakUscUJBQVMsQ0FBQyxTQUFTLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ3ZFLENBQUM7UUFDYyxtQkFBWSxHQUEzQjtZQUVJLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDOUYsQ0FBQztRQUVNLGtCQUFXLEdBQWxCLFVBQW1CLGNBQXNCO1lBRXJDLElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUN4QyxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFDeEM7Z0JBQ0ksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMzQjtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLEVBQUUsY0FBYyxDQUFDLENBQUM7WUFDakUsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLENBQUM7UUFDTSw0QkFBcUIsR0FBNUIsVUFBNkIsT0FBMEM7WUFFbkUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQ0FBc0MsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFbEQsQ0FBQztRQUNNLHlCQUFrQixHQUF6QixVQUEwQixPQUEwQztZQUVoRSxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzFELE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQyxDQUFDO1FBQ00sMkJBQW9CLEdBQTNCLFVBQTRCLE9BQTBDO1lBRWxFLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVELE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3pDLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRCxDQUFDO1FBQ00sd0JBQWlCLEdBQXhCO1lBRUksT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFDLENBQUM7UUFDTSx3QkFBaUIsR0FBeEIsVUFBeUIsSUFBcUM7WUFFMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNsRCxDQUFDO1FBQ00sc0JBQWUsR0FBdEIsVUFBdUIsSUFBcUM7WUFFeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBQ00sd0JBQWlCLEdBQXhCLFVBQXlCLElBQXFDO1lBRTFELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsRUFBRSw2QkFBNkIsQ0FBQyxDQUFDO1lBQ3ZFLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFFNUIscUJBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRS9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsNkJBQTZCLENBQUMsQ0FBQztZQUN2RSxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRTVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3JHLE1BQU0sQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQyxDQUFDO1FBQ00scUJBQWMsR0FBckIsVUFBc0IsSUFBcUM7WUFFdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUUzQyxxQkFBUyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN6QixNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QyxDQUFDO1FBRUwsYUFBQztJQUFELENBQUM7SUFyR1ksd0JBQU07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0duQjtRQUFnQyw4QkFBYztRQVExQyxvQkFBWSxNQUFlO1lBQTNCLFlBRUksaUJBQU8sU0FRVjtZQWhCTSxjQUFRLEdBQVUsbUJBQW1CLENBQUM7WUFTekMsS0FBSSxDQUFDLElBQUksR0FBRyxtQkFBUSxDQUFDLEtBQUssQ0FBQztZQUMzQixLQUFJLENBQUMsT0FBTyxHQUFHLHNCQUFXLENBQUMsS0FBSyxDQUFDO1lBRWpDLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSx5QkFBYSxFQUFFLENBQUM7WUFFaEMsbUJBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLENBQUM7O1FBQzNCLENBQUM7UUFDRCwrQkFBVSxHQUFWO1lBSUksT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNMLGlCQUFDO0lBQUQsQ0FBQyxDQXpCK0IsK0JBQWMsR0F5QjdDO0lBekJZLGdDQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNBdkI7UUFBZ0MsOEJBQWM7UUFNMUMsb0JBQVksTUFBZTtZQUEzQixZQUVJLGlCQUFPLFNBUVY7WUFkTSxjQUFRLEdBQVUsa0JBQWtCLENBQUM7WUE4QnBDLFlBQU0sR0FBRyxVQUFDLEtBQXlCLEVBQUUsVUFBdUI7Z0JBRWhFLElBQUksV0FBVyxHQUFHLElBQUksdUJBQVUsQ0FBQyxLQUFJLENBQUMsQ0FBQztnQkFDdkMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNwQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBRXZCLElBQUksS0FBSyxHQUFHO29CQUNSLEtBQUssRUFBRSxhQUFhO29CQUNwQixNQUFNLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7aUJBQ3BDLENBQUM7Z0JBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsVUFBQyxJQUFJO29CQUVyQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQ2Y7d0JBQ0ksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDMUI7eUJBRUQ7d0JBQ0ksZUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDL0I7Z0JBRUwsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRWYsQ0FBQztZQTdDRyxLQUFJLENBQUMsSUFBSSxHQUFHLG1CQUFRLENBQUMsS0FBSyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxPQUFPLEdBQUcsc0JBQVcsQ0FBQyxLQUFLLENBQUM7WUFDakMsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLHdCQUFZLEVBQUUsQ0FBQztZQUUvQixtQkFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsQ0FBQzs7UUFDM0IsQ0FBQztRQUNELCtCQUFVLEdBQVY7WUFFSSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksZUFBTSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDeEUsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUVELHlCQUFJLEdBQUo7WUFFSSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLHdCQUFZLEVBQUUsQ0FBQztZQUUvQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBMEJMLGlCQUFDO0lBQUQsQ0FBQyxDQXhEK0IsK0JBQWMsR0F3RDdDO0lBeERZLGdDQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNPdkI7UUFBbUMsaUNBQWM7UUFXN0MsdUJBQVksTUFBZTtZQUEzQixZQUVJLGlCQUFPLFNBTVY7WUFqQkQsY0FBUSxHQUFZLHNCQUFzQixDQUFDO1lBb0RuQyxVQUFJLEdBQUcsVUFBQyxLQUF5QixFQUFFLFNBQTBCO2dCQUNqRSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3BDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxXQUFXLEdBQWtCLElBQUksdUJBQVUsQ0FBQyxLQUFJLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxLQUFLLEdBQUc7b0JBQ1IsS0FBSyxFQUFFLGdCQUFnQjtvQkFDdkIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO2lCQUNuQyxDQUFDO2dCQUNGLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLFVBQUMsSUFBSTtvQkFFckMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUNmO3dCQUNJLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQzFCO3lCQUVEO3dCQUNJLGVBQU0sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFDLE9BQU8sRUFBRyxJQUFJLEVBQUUsS0FBSyxFQUFHLFVBQVUsRUFBQyxDQUFDLENBQUM7cUJBQ2xFO2dCQUVMLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVmLENBQUMsQ0FBQztZQUVNLG1CQUFhLEdBQUcsVUFBQyxLQUF5QixFQUFFLGFBQTZCO2dCQUU3RSxJQUFNLFlBQVksR0FBSSxJQUFJLHVCQUFVLEVBQXFFLENBQUM7Z0JBRTFHLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSwrQkFBYyxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUcsR0FBRyxFQUFDLEVBQUUsRUFBQyxJQUFJLEVBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQyxDQUFDLENBQUM7Z0JBcUJyRyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3RDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFFdkIsV0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksRUFBRSxlQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDdkQscUJBQVMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQzVELENBQUMsQ0FBQztZQUNNLFlBQU0sR0FBRyxVQUFDLEtBQXlCLEVBQUUsYUFBNkI7Z0JBQ3RFLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxvQkFBb0IsRUFBRSxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzVELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLDJCQUEyQixFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLCtCQUErQixFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztnQkFFakYsMkJBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSwrQkFBYyxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUcsRUFBRSxFQUFDLEVBQUUsRUFBQyxJQUFJLEVBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUksYUFBYSxFQUFDLENBQUMsQ0FBQztnQkFDNUgsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzNCLENBQUMsQ0FBQztZQXpHRSxLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixLQUFJLENBQUMsSUFBSSxHQUFHLG1CQUFRLENBQUMsS0FBSyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxPQUFPLEdBQUcsc0JBQVcsQ0FBQyxRQUFRLENBQUM7WUFDcEMsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLCtCQUFnQixFQUFFLENBQUM7WUFDbkMsbUJBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLENBQUM7O1FBQzNCLENBQUM7UUFDRCxrQ0FBVSxHQUFWO1lBRUksT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELDRCQUFJLEdBQUo7WUFFSSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFcEMsaUJBQU0sSUFBSSxXQUFFLENBQUM7WUFDYixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLCtCQUFnQixFQUFFLENBQUM7WUFFbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO1lBQ2pFLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLGVBQU0sQ0FBQyw0QkFBNEIsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBRWhHLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksZUFBTSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFM0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFNLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUV2RSxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsK0JBQU8sR0FBUCxVQUFRLElBQThCO1lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFM0MsSUFBRyxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksV0FBVyxFQUN4QztnQkFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSx5QkFBeUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQy9ELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDMUM7WUFDRCxpQkFBTSxPQUFPLFlBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQXFFTCxvQkFBQztJQUFELENBQUMsQ0ExSGtDLCtCQUFjLEdBMEhoRDtJQTFIWSxzQ0FBYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDTjFCO1FBQW1DLGlDQUFjO1FBUTdDLHVCQUFZLE1BQWU7WUFBM0IsWUFDSSxpQkFBTyxTQU1WO1lBYkQsY0FBUSxHQUFXLHFCQUFxQixDQUFDO1lBbUhqQyxZQUFNLEdBQUcsVUFBQyxLQUF5QixFQUFFLFNBQXlCO2dCQUVsRSxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBRWxDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUksQ0FBQyxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO2dCQUd6RCxJQUFJLFdBQVcsR0FBa0IsSUFBSSx1QkFBVSxDQUFDLEtBQUksQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLEtBQUssR0FBRztvQkFDUixLQUFLLEVBQUUsZ0JBQWdCO29CQUN2QixNQUFNLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7aUJBQ25DLENBQUM7Z0JBQ0YsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsVUFBQyxJQUFJO29CQUVyQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQ2Y7d0JBQ0ksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDMUI7eUJBRUQ7d0JBQ0ksZUFBTSxDQUFDLGlCQUFpQixDQUFDLEVBQUMsT0FBTyxFQUFHLElBQUksRUFBRSxLQUFLLEVBQUcsVUFBVSxFQUFDLENBQUMsQ0FBQztxQkFDbEU7Z0JBRUwsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRWYsQ0FBQztZQXZJRyxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDekMsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsS0FBSSxDQUFDLElBQUksR0FBRyxtQkFBUSxDQUFDLEtBQUssQ0FBQztZQUMzQixLQUFJLENBQUMsT0FBTyxHQUFHLHNCQUFXLENBQUMsUUFBUSxDQUFDO1lBQ3BDLG1CQUFRLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxDQUFDOztRQUMzQixDQUFDO1FBQ0Qsa0NBQVUsR0FBVjtZQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksZUFBTSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2pCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxpQ0FBUyxHQUFUO1lBQ0ksSUFBTSxXQUFXLEdBQUcsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3ZDLElBQU0sV0FBVyxHQUFHLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN2QyxJQUFNLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBRWxELElBQU0sZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDbEQsSUFBTSxjQUFjLEdBQUcsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFFcEQsSUFBTSxlQUFlLEdBQUc7Z0JBRXBCLElBQUcsV0FBVyxDQUFDLEdBQUcsRUFBRSxFQUNwQjtvQkFDSSxJQUFNLEtBQUssR0FBRzt3QkFDVixHQUFHLEVBQUcsZ0JBQWdCO3dCQUN0QixRQUFRLEVBQUcsV0FBVyxDQUFDLEdBQUcsRUFBRTtxQkFDL0IsQ0FBQztvQkFFRixDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxVQUFDLElBQXNDO3dCQUV0RSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDMUIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUcxQixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBTTs0QkFDaEIsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztpQ0FDM0QsSUFBSSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQ2xDLENBQUMsQ0FBQyxDQUFDO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBRXRCLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBRVosT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO29CQUN2QyxDQUFDLENBQUMsQ0FBQztvQkFDSCxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2lCQUM1QztxQkFFRDtvQkFDSSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2lCQUMzQztZQUNMLENBQUMsQ0FBQztZQUVGLFdBQVcsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUM3QixlQUFlLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztZQUNILElBQU0sWUFBWSxHQUFHO2dCQUNqQixJQUFNLElBQUksR0FBYSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLFdBQVcsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDcEQsZUFBZSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDO1lBRUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBR3RDLFlBQVksRUFBRSxDQUFDO1lBRWYsY0FBYyxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3hCLFlBQVksRUFBRSxDQUFDO1lBQ25CLENBQUMsQ0FBQyxDQUFDO1lBSUgsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELCtCQUFPLEdBQVAsVUFBUSxJQUE4QjtZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxDQUFDO1lBRTNDLElBQUcsT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLFdBQVcsRUFDeEM7Z0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzthQUMxQztZQUNELGlCQUFNLE9BQU8sWUFBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsNEJBQUksR0FBSjtZQUNJLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTdDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNsQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksOEJBQWUsRUFBRSxDQUFDO1lBQ2xDLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCw0QkFBSSxHQUFKLFVBQUssU0FBMEI7WUFBMUIsNkNBQTBCO1lBRTNCLElBQUcsT0FBTyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssVUFBVSxFQUM3QztnQkFFSSxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQzthQUMxQjtZQUVELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztZQUN0RCxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDO1FBK0JMLG9CQUFDO0lBQUQsQ0FBQyxDQWxKa0MsK0JBQWMsR0FrSmhEO0lBbEpZLHNDQUFhIiwiZmlsZSI6Im1haW4td2VicGFjay5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vbWFpbi50c1wiKTtcbiIsImltcG9ydCB7QmFzZVByb21pc2VUeXBlLCBCYXNlUHJvcHMsIEJhc2VTdGF0ZSwgSUV2ZW50fSBmcm9tIFwiLi9ldmVudC9JRXZlbnRcIjtcbmltcG9ydCB7QWJzdHJhY3RFdmVudH0gZnJvbSBcIi4vZXZlbnQvQWJzdHJhY3RFdmVudFwiO1xuXG5leHBvcnQgY2xhc3MgRXZlbnRIYW5kbGVyPEUgZXh0ZW5kcyBJRXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGUsIEJhc2VQcm9taXNlVHlwZT4sIFQgZXh0ZW5kcyBBYnN0cmFjdEV2ZW50PEJhc2VQcm9wcywgQmFzZVN0YXRlLCBCYXNlUHJvbWlzZVR5cGU+PntcblxuXG4gICAgc3RhdGljIHRyaWdnZXI8VCBleHRlbmRzIEFic3RyYWN0RXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGUsIEJhc2VQcm9taXNlVHlwZT4+KGV2ZW50IDogVCwgcHJvcHMgOiBUWydwcm9wVHlwZSddLCBzdGF0ZSA6ICBUWydzdGF0ZVR5cGUnXSk6UHJvbWlzZTxUWydwcm9taXNlVHlwZSddPlxuICAgIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPEJhc2VQcm9taXNlVHlwZT4oKHJlc29sdmUgOiBhbnksIHJlamVjdCA6IGFueSkgPT4ge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnRXZlbnRIYW5kbGVyLnRyaWdnZXInKTtcbiAgICAgICAgICAgIGV2ZW50LnRyaWdnZXIocHJvcHMsIHN0YXRlKS50aGVuKHJlc29sdmUoKSkuY2F0Y2gocmVqZWN0KCkpO1xuXG4gICAgICAgIH0pXG5cblxuXG4gICAgfVxufVxuIiwiaW1wb3J0IHtCYXNlUHJvcHMsIEJhc2VTdGF0ZSwgSUV2ZW50fSBmcm9tIFwiLi9ldmVudC9JRXZlbnRcIjtcbmltcG9ydCB7QWJzdHJhY3RFdmVudH0gZnJvbSBcIi4vZXZlbnQvQWJzdHJhY3RFdmVudFwiO1xuaW1wb3J0IHtFdmVudEhhbmRsZXJ9IGZyb20gXCIuL0V2ZW50SGFuZGxlclwiO1xuXG5cbmV4cG9ydCBjbGFzcyBFdmVudFN0YWNrPEUgZXh0ZW5kcyBJRXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGU+LCBUIGV4dGVuZHMgQWJzdHJhY3RFdmVudDxCYXNlUHJvcHMsIEJhc2VTdGF0ZT4+e1xuXG4gICAgc3RhY2sgOiB7aGFuZGxlciA6IFQsIHByb3BzIDogVFsncHJvcFR5cGUnXSwgc3RhdGUgOiBUWydzdGF0ZVR5cGUnXX1bXTtcblxuICAgIHJlZ2lzdGVyKGV2ZW50IDogVCwgcHJvcHMgOiBUWydwcm9wVHlwZSddLCBzdGF0ZSA6ICBUWydzdGF0ZVR5cGUnXSlcbiAgICB7XG4gICAgICAgIHRoaXMuc3RhY2sucHVzaCh7aGFuZGxlcjogZXZlbnQsIHByb3BzIDogcHJvcHMsIHN0YXRlIDogc3RhdGV9KVxuICAgIH1cblxuICAgIGV4ZWN1dGUgPSAoKTp2b2lkID0+XG4gICAge1xuICAgICAgICB0aGlzLnN0YWNrLmZvckVhY2goKGV2ZW50KSA9PiB7XG4gICAgICAgICAgICBFdmVudEhhbmRsZXIudHJpZ2dlcihldmVudC5oYW5kbGVyLCBldmVudC5wcm9wcywgZXZlbnQuc3RhdGUpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiIsImltcG9ydCB7QmFzZVByb21pc2VUeXBlLCBCYXNlUHJvcHMsIEJhc2VTdGF0ZSwgSUV2ZW50fSBmcm9tIFwiLi9JRXZlbnRcIjtcbmltcG9ydCB7YWpheEZhaWxIYW5kbGVyfSBmcm9tIFwiLi9odHRwXCI7XG5pbXBvcnQge1N0YXR1c01lc3NhZ2VDb2xvcnMsIFN0YXR1c1N0YXRlLCBTdGF0dXNWYWxpZGF0aW9uU3RhdGV9IGZyb20gXCIuL3R5cGVzL1N0YXR1c01lc3NhZ2VcIjtcblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQWJzdHJhY3RFdmVudDxQIGV4dGVuZHMgQmFzZVByb3BzLCBTIGV4dGVuZHMgQmFzZVN0YXRlLCBQVCBleHRlbmRzIEJhc2VQcm9taXNlVHlwZT4gaW1wbGVtZW50cyBJRXZlbnQ8UCwgUywgUFQ+e1xuXG4gICAgYWJzdHJhY3QgcHJvcFR5cGUgOiBQO1xuICAgIGFic3RyYWN0IHN0YXRlVHlwZTogUztcbiAgICBhYnN0cmFjdCBwcm9taXNlVHlwZSA6IFBUO1xuXG5cblxuICAgIGFic3RyYWN0IHRyaWdnZXIocHJvcHMgOiBQLCBzdGF0ZSA6IFMpOiBQcm9taXNlPFBUPjtcblxuICAgIGdldFByb3BUeXBlKCkgOiBQXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5wcm9wVHlwZTtcbiAgICB9XG4gICAgZ2V0U3RhdGVUeXBlKCkgOiBTXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5zdGF0ZVR5cGU7XG4gICAgfVxuICAgIGdldFJlamVjdEhhbmRsZXIocmVqZWN0IDogYW55LCB0aXRsZSA6IHN0cmluZywgbWVzc2FnZSA6IHN0cmluZyk6YWpheEZhaWxIYW5kbGVyXG4gICAge1xuICAgICAgICByZXR1cm4gYXN5bmMgKHRleHRTdGF0dXM6IGFueSkgPT4ge1xuXG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRleHRTdGF0dXMpO1xuICAgICAgICAgICAgbGV0IGVycm9ycyA9IG51bGw7XG5cbiAgICAgICAgICAgIGlmKHRleHRTdGF0dXMuZXJyb3JzKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGVycm9ycyA9IHRleHRTdGF0dXMuZXJyb3JzO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZihlcnJvcnMpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgLy8gVmFsaWRhdGllIGVycm9yc1xuICAgICAgICAgICAgICAgIGxldCBvU3RhdGU6U3RhdHVzVmFsaWRhdGlvblN0YXRlID0ge1xuICAgICAgICAgICAgICAgICAgICB0aXRsZSA6IHRpdGxlLFxuICAgICAgICAgICAgICAgICAgICBlcnJvcnMgOiBlcnJvcnMsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRJZCA6IFwiZXJyb3JfZGlhbG9nXCIsXG4gICAgICAgICAgICAgICAgICAgIFN0YXR1c01lc3NhZ2VDb2xvciA6IFN0YXR1c01lc3NhZ2VDb2xvcnMud2FybmluZ1xuICAgICAgICAgICAgICAgIH07XG5cblxuICAgICAgICAgICAgICAgIHJlamVjdChcIlZhbGlkYXRpb24gZXJyb3JzIG9jY3VyZWRcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGV0IG9TdGF0ZTpTdGF0dXNTdGF0ZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGUgOiB0aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZSA6IG1lc3NhZ2UucmVwbGFjZSgne3RleHRTdGF0dXN9JywgdGV4dFN0YXR1cyksXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRJZCA6IFwiZXJyb3JfZGlhbG9nXCIsXG4gICAgICAgICAgICAgICAgICAgIFN0YXR1c01lc3NhZ2VDb2xvciA6IFN0YXR1c01lc3NhZ2VDb2xvcnMud2FybmluZ1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfTtcbiAgICB9XG59XG4iLCJleHBvcnQgY2xhc3MgQmFzZVByb3Bze31cbmV4cG9ydCBjbGFzcyBCYXNlU3RhdGUge31cblxuZXhwb3J0IGludGVyZmFjZSBCYXNlUHJvbWlzZVR5cGUgZXh0ZW5kcyBQcm9taXNlTGlrZTxhbnk+e1xuICAgIG9uRnVsZmlsbGVkKGNhbGxiYWNrIDogKGFyZyA6IGFueSkgPT4gYW55KTp2b2lkO1xuICAgIG9uUmVqZWN0ZWQoY2FsbGJhY2sgOiAoYXJnIDogYW55KSA9PiBhbnkpOnZvaWQ7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJRXZlbnQ8cHJvcGVydHlUeXBlIGV4dGVuZHMgQmFzZVByb3BzLCBzdGF0ZVR5cGUgZXh0ZW5kcyBCYXNlU3RhdGUsIHByb21pc2VUeXBlIGV4dGVuZHMgQmFzZVByb21pc2VUeXBlPiB7XG5cblxuICAgIHByb3BUeXBlIDogcHJvcGVydHlUeXBlO1xuICAgIHN0YXRlVHlwZSA6IHN0YXRlVHlwZTtcbiAgICBwcm9taXNlVHlwZSA6IHByb21pc2VUeXBlO1xuXG4gICAgdHJpZ2dlcihwcm9wcyA6IHByb3BlcnR5VHlwZSwgc3RhdGUgOiBzdGF0ZVR5cGUsICk6IFByb21pc2U8cHJvbWlzZVR5cGU+O1xuXG59XG4iLCJpbXBvcnQge3ZhbGlkYXRpb25FcnJvcnN9IGZyb20gXCIuL3ZhbGlkYXRpb25cIjtcbmltcG9ydCB7QmFzZVByb3BzLCBCYXNlU3RhdGUsIElFdmVudH0gZnJvbSBcIi4uL0lFdmVudFwiO1xuaW1wb3J0IHtFdmVudFN0YWNrfSBmcm9tIFwiLi4vLi4vRXZlbnRTdGFja1wiO1xuaW1wb3J0IHtBYnN0cmFjdEV2ZW50fSBmcm9tIFwiLi4vQWJzdHJhY3RFdmVudFwiO1xuXG5leHBvcnQgZW51bSBTdGF0dXNNZXNzYWdlQ29sb3JzIHtcbiAgICBpbmZvID0gXCJpbmZvXCIsXG4gICAgc3VjY2VzcyA9IFwic3VjY2Vzc1wiLFxuICAgIGFsZXJ0ID0gXCJhbGVydFwiLFxuICAgIHdhcm5pbmcgPSBcIndhcm5pbmdcIixcbiAgICBkYW5nZXIgPSBcImRhbmdlclwiXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgU3RhdHVzTWVzc2FnZUJ1dHRvbntcbiAgICBsYWJlbCA6IHN0cmluZyxcbiAgICBhY3Rpb24gOiBJRXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGU+IHwgRXZlbnRTdGFjazxJRXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGU+LCBBYnN0cmFjdEV2ZW50PEJhc2VQcm9wcywgQmFzZVN0YXRlPj4sXG4gICAgdGl0bGUgOiBzdHJpbmcsXG4gICAgdHlwZSA6IFN0YXR1c01lc3NhZ2VDb2xvcnNcbn1cblxuZXhwb3J0IGNsYXNzIFN0YXR1c1N0YXRlIGltcGxlbWVudHMgQmFzZVN0YXRle1xuICAgIG1lc3NhZ2U/OiBzdHJpbmc7XG4gICAgdGl0bGUgOiBzdHJpbmc7XG4gICAgZWxlbWVudElkIDogc3RyaW5nO1xuICAgIFN0YXR1c01lc3NhZ2VDb2xvciA6IFN0YXR1c01lc3NhZ2VDb2xvcnM7XG4gICAgYnV0dG9ucz8gOiBTdGF0dXNNZXNzYWdlQnV0dG9uW107XG59XG5leHBvcnQgY2xhc3MgU3RhdHVzVmFsaWRhdGlvblN0YXRlIGltcGxlbWVudHMgU3RhdHVzU3RhdGV7XG4gICAgZXJyb3JzOiB2YWxpZGF0aW9uRXJyb3JzO1xuICAgIG1lc3NhZ2U/OiBzdHJpbmc7XG4gICAgdGl0bGUgOiBzdHJpbmc7XG4gICAgZWxlbWVudElkIDogc3RyaW5nO1xuICAgIFN0YXR1c01lc3NhZ2VDb2xvciA6IFN0YXR1c01lc3NhZ2VDb2xvcnNcbn1cbiIsIi8qXG5TdGFydCBsb3djb2RlIGVkaXRvclxuKi9cbi8qXG5pbXBvcnQgKiBhcyBFdmVudCBmcm9tIFwiLi9sb3djb2RlL2V2ZW50L21haW5cIjtcbmltcG9ydCAqIGFzIEFwcERlc2lnbmVyIGZyb20gXCIuL2xvd2NvZGUvYXBwLWRlc2lnbmVyL21haW5cIjtcblxuY29uc29sZS5sb2coXCJTdGFydGluZyBldmVudCBydW5uZXIgbW9kdWxlXCIpO1xuRXZlbnQubWFpbigpO1xuXG5jb25zb2xlLmxvZyhcIlN0YXJ0aW5nIGFwcCBkZXNpZ25lciBtb2R1bGVcIik7XG5BcHBEZXNpZ25lci5tYWluKCk7XG4qL1xuLypcblN0YXJ0IHNjaGVtYSBlZGl0b3JcbiovXG5cbmltcG9ydCB7RWRpdG9yfSBmcm9tIFwiLi9zY2hlbWEtZWRpdG9yL3dpZGdldC9FZGl0b3JcIjtcblxuXG5FZGl0b3Iuc3RhcnQoKTtcbiIsImltcG9ydCB7SU1lc3NhZ2VDb250ZW50R2VuZXJpY3N9IGZyb20gXCIuL21vZGVsL01lc3NlbmdlclwiO1xuaW1wb3J0IHtBYnN0cmFjdEV2ZW50fSBmcm9tIFwiLi4vZXZlbnQvQWJzdHJhY3RFdmVudFwiO1xuaW1wb3J0IHtCYXNlUHJvbWlzZVR5cGUsIEJhc2VQcm9wcywgQmFzZVN0YXRlLCBJRXZlbnR9IGZyb20gXCIuLi9ldmVudC9JRXZlbnRcIjtcblxuZXhwb3J0IGVudW0gQWpheEFjdGlvbiB7XG4gICAgREVMRVRFLFxuICAgIFBPU1QsXG4gICAgUFVULFxuICAgIFBBVENILFxuICAgIEdFVFxufVxuaW50ZXJmYWNlIElBamF4UHJvcGVydHlQcm9wcyBleHRlbmRzIEJhc2VQcm9wc3tcbiAgICBlbmRwb2ludCA6IHN0cmluZztcbiAgICBhY3Rpb24gOiBBamF4QWN0aW9uO1xufVxuaW50ZXJmYWNlIElBamF4UHJvcGVydHlTdGF0ZSBleHRlbmRzIEJhc2VTdGF0ZXtcbiAgICBkYXRhIDoge31cbn1cbmV4cG9ydCBpbnRlcmZhY2UgSUFqYXhQcm9taXNlVHlwZSBleHRlbmRzIEJhc2VQcm9taXNlVHlwZSwgUHJvbWlzZUxpa2U8YW55PntcbiAgICBvbkZ1bGZpbGxlZChjYWxsYmFjayA6IChhcmcgOiBhbnkpID0+IGFueSk6dm9pZDtcbiAgICBvblJlamVjdGVkKGNhbGxiYWNrIDogKGFyZyA6IGFueSkgPT4gYW55KTp2b2lkO1xuXG59XG5cbmV4cG9ydCBjbGFzcyBBamF4UHJvcHMgaW1wbGVtZW50cyBJQWpheFByb3BlcnR5UHJvcHN7XG4gICAgZW5kcG9pbnQgOiBzdHJpbmc7XG4gICAgYWN0aW9uIDogQWpheEFjdGlvbjtcbn1cbmV4cG9ydCBjbGFzcyBBamF4U3RhdGUgaW1wbGVtZW50cyBJQWpheFByb3BlcnR5U3RhdGV7XG4gICAgZGF0YSA6IHt9XG59XG5leHBvcnQgY2xhc3MgQWpheDxQIGV4dGVuZHMgSUFqYXhQcm9wZXJ0eVByb3BzLCBTIGV4dGVuZHMgSUFqYXhQcm9wZXJ0eVN0YXRlLCBQVCBleHRlbmRzIElBamF4UHJvbWlzZVR5cGU+IGV4dGVuZHMgQWJzdHJhY3RFdmVudDxQLCBTLCBQVD4gaW1wbGVtZW50cyBJRXZlbnQ8UCwgUywgUFQ+XG57XG4gICAgcHJvcFR5cGUgOiBQO1xuICAgIHN0YXRlVHlwZSA6IFM7XG4gICAgcHJvbWlzZVR5cGUgOiBQVDtcblxuXG4gICAgdHJpZ2dlcihwcm9wczogUCwgc3RhdGU6IFMpOiBQcm9taXNlPFBUPiB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxQVD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZygnQWpheChhY3Rpb24sIGRhdGEpJywgcHJvcHMuYWN0aW9uLCBzdGF0ZS5kYXRhKTtcblxuICAgICAgICAgICAgbGV0IG9EYXRhID0ge1xuICAgICAgICAgICAgICAgICdfZG8nOiBwcm9wcy5hY3Rpb24sXG4gICAgICAgICAgICAgICAgJ2RhdGEnOiBzdGF0ZS5kYXRhXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJcXHRcIiwgd2luZG93LmxvY2F0aW9uLmhyZWYsIFwiUE9TVFwiLCBvRGF0YSk7XG4gICAgICAgICAgICByZXR1cm4gJC5wb3N0KHdpbmRvdy5sb2NhdGlvbi5ocmVmLCBvRGF0YSwgKGRhdGEpID0+IHtcblxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsIFwic2VydmVyIHJlc3BvbnNlXCIsIGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGRhdGEuZXJyb3JzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShkYXRhKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0sICdqc29uJylcbiAgICAgICAgICAgIC5mYWlsKCgpID0+IHtcbiAgICAgICAgICAgICAgICByZWplY3QoJ0FqYXggY2FsbCBmYWlsZWQnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgIH1cbn1cbiIsImltcG9ydCB7QWJzdHJhY3RFdmVudH0gZnJvbSBcIi4uLy4uL2V2ZW50L0Fic3RyYWN0RXZlbnRcIjtcbmltcG9ydCB7QmFzZVByb21pc2VUeXBlLCBCYXNlUHJvcHMsIEJhc2VTdGF0ZSwgSUV2ZW50fSBmcm9tIFwiLi4vLi4vZXZlbnQvSUV2ZW50XCI7XG5pbXBvcnQge0FqYXgsIEFqYXhBY3Rpb259IGZyb20gXCIuLi9BamF4XCI7XG5pbXBvcnQge0lXaWRnZXR9IGZyb20gXCIuLi9jb250cmFjdHMvSVdpZGdldFwiO1xuaW1wb3J0IHtFdmVudEhhbmRsZXJ9IGZyb20gXCIuLi8uLi9FdmVudEhhbmRsZXJcIjtcblxuY2xhc3MgRGVsZXRlUHJvcGVydHlQcm9wcyBleHRlbmRzIEJhc2VQcm9wcyB7XG5cbiAgICBlbmRwb2ludCA6IHN0cmluZztcbiAgICBhY3Rpb24gOiBBamF4QWN0aW9uO1xufVxuXG5jbGFzcyBEZWxldGVQcm9wZXJ0eVN0YXRlIGV4dGVuZHMgQmFzZVN0YXRlIHtcbiAgICB3aWRnZXQ6IElXaWRnZXQ7XG59XG5cbmNsYXNzIERlbGV0ZVByb3BlcnR5UHJvbWlzZVR5cGUgaW1wbGVtZW50cyBCYXNlUHJvbWlzZVR5cGUge1xuXG4gICAgcmVzb2x2ZSgpXG4gICAge1xuXG4gICAgfVxuICAgIG9uUmVqZWN0ZWQoY2FsbGJhY2s6IChhcmc6IGFueSkgPT4gYW55KTogYW55IHtcbiAgICB9XG5cbiAgICBvbkZ1bGZpbGxlZChjYWxsYmFjazogKGFyZzogYW55KSA9PiBhbnkpOiBhbnkge1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBEZWxldGVQcm9wZXJ0eTxQIGV4dGVuZHMgRGVsZXRlUHJvcGVydHlQcm9wcywgUyBleHRlbmRzIERlbGV0ZVByb3BlcnR5U3RhdGUsIFBUIGV4dGVuZHMgRGVsZXRlUHJvcGVydHlQcm9taXNlVHlwZT4gZXh0ZW5kcyBBYnN0cmFjdEV2ZW50PFAsIFMsIFBUPiBpbXBsZW1lbnRzIElFdmVudDxQLCBTLCBQVD57XG4gICAgcHJvbWlzZVR5cGU6IFBUO1xuICAgIHByb3BUeXBlOiBQO1xuICAgIHN0YXRlVHlwZTogUztcblxuICAgIHRyaWdnZXIocHJvcHM6IFAsIHN0YXRlOiBTKTogUHJvbWlzZTxQVD4ge1xuXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxQVD4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXG4gICAgICAgICAgICBFdmVudEhhbmRsZXIudHJpZ2dlcihuZXcgQWpheCgpLCB7ZW5kcG9pbnQgOiBwcm9wcy5lbmRwb2ludCwgYWN0aW9uIDogcHJvcHMuYWN0aW9ufSwge2RhdGEgOiBzdGF0ZS53aWRnZXQuZm9ybS5nZXREYXRhKCl9KVxuICAgICAgICAgICAgICAgIC50aGVuKHZhbHVlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2godmFsdWUgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QodmFsdWUpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4vKlxuICAgICAgICAgICAgY29uc3QgZXJyb3JfYmxvY2sgPSBuZXcgRXJyb3JCbG9jayh3aWRnZXQpO1xuICAgICAgICAgICAgZXJyb3JfYmxvY2suY2xlYXIoKTtcbiAgICAgICAgICAgIGVycm9yX2Jsb2NrLnNob3coZGF0YSk7XG5cblxuXG4gICAgICAgICAgICBBamF4KCdEZWxldGVQcm9wZXJ0eScsIHN0YXRlLndpZGdldCwgRWRpdG9yLm9uUHJvcGVydHlEZWxldGVkKTtcbiAgICAgICAgICAgIE1lc3Nlbmdlci5zZW5kKCdwcm9wZXJ0eV9kZWxldGVkJywgc3RhdGUuZGF0YSk7XG5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdEZWxldGVQcm9wZXJ0eS50cmlnZ2VyJyk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlxcdFwiLCBzdGF0ZS5kYXRhKTtcbiovXG4gICAgICAgIH0pO1xuXG5cbiAgICB9XG59XG5cbiIsImltcG9ydCB7SUZvcm19IGZyb20gXCIuL0Zvcm1cIjtcbmltcG9ydCB7U3ViamVjdFR5cGUsIFR5cGVUeXBlfSBmcm9tIFwiLi4vbW9kZWwvVGVtcGxhdGVcIjtcbmltcG9ydCB7SU1lc3NhZ2VDb250ZW50R2VuZXJpY3N9IGZyb20gXCIuLi9tb2RlbC9NZXNzZW5nZXJcIjtcbmltcG9ydCB7RWRpdFByb3BlcnR5Rm9ybX0gZnJvbSBcIi4uL21vZGVsL1Byb3BlcnR5Rm9ybVwiO1xuXG5jbGFzcyBLZXlWYWwge1xuICAgIGtleSA6IHN0cmluZztcbiAgICB2YWx1ZSA6IHN0cmluZztcbn1cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBBYnN0cmFjdFdpZGdldCB7XG5cbiAgICBzZWxlY3RvcjpzdHJpbmc7XG4gICAgZm9ybTpJRm9ybTtcbiAgICB0ZW1wbGF0ZSA6IHN0cmluZztcbiAgICB0eXBlIDogVHlwZVR5cGU7XG4gICAgc3ViamVjdCA6IFN1YmplY3RUeXBlO1xuICAgIC8vIFRoZSBhY3R1YWwgZm9ybSBtYXkgYmUgYWRkZWQgdG8gdGhlIGRvbSBsYXRlci5cbiAgICBwcml2YXRlIGRhdGFfa2VlcGVyIDogS2V5VmFsW10gPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKClcbiAgICB7XG4gICAgICAgIHRoaXMuZm9ybSA9IG5ldyBFZGl0UHJvcGVydHlGb3JtKCk7XG4gICAgfVxuICAgIGdldFBsYWNlSG9sZGVyKCk6SlF1ZXJ5XG4gICAge1xuICAgICAgICByZXR1cm4gJCgnIycgKyB0aGlzLnR5cGUgKyAnX3BsYWNlaG9sZGVyJyk7XG4gICAgfVxuICAgIGFic3RyYWN0IGJpbmRFdmVudHMoKTp0aGlzO1xuICAgIGNyZWF0ZVBsYWNlaG9sZGVyKCk6SFRNTERpdkVsZW1lbnRcbiAgICB7XG4gICAgICAgIHZhciBlbGVtID0gPEhUTUxEaXZFbGVtZW50Pihkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSk7XG4gICAgICAgIGNvbnNvbGUubG9nKCdBYnN0cmFjdFdpZGdldC5jcmVhdGVQbGFjZWhvbGRlcicsIHRoaXMudHlwZSArICdfcGxhY2Vob2xkZXInKTtcbiAgICAgICAgZWxlbS5zZXRBdHRyaWJ1dGUoJ2lkJywgdGhpcy50eXBlICsgJ19wbGFjZWhvbGRlcicpO1xuICAgICAgICBlbGVtLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgIHJldHVybiBlbGVtO1xuICAgIH1cblxuICAgIHNldERhdGEoZGF0YTogIElNZXNzYWdlQ29udGVudEdlbmVyaWNzKTogdGhpcyB7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ0Fic3RyYWN0V2lkZ2V0LnNldERhdGEnLCBkYXRhKTtcblxuICAgICAgICBsZXQgYUZpZWxkTmFtZXMgPSB0aGlzLmZvcm0uZ2V0RmllbGROYW1lcygpO1xuICAgICAgICBsZXQgb0Zvcm1EYXRhID0gdGhpcy5mb3JtLmdldEZpZWxkTWFuYWdlcigpO1xuICAgICAgICBsZXQgZmllbGROYW1lIDogc3RyaW5nO1xuICAgICAgICB0aGlzLmRhdGFfa2VlcGVyID0gW107XG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBhRmllbGROYW1lcy5sZW5ndGg7IGkrKylcbiAgICAgICAge1xuICAgICAgICAgICAgZmllbGROYW1lID0gYUZpZWxkTmFtZXNbaV0udG9TdHJpbmcoKTtcblxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJcXHRcIiwgZmllbGROYW1lLCBkYXRhKTtcblxuICAgICAgICAgICAgaWYodHlwZW9mIGZpZWxkTmFtZSA9PT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmKHR5cGVvZiBkYXRhID09PSAndW5kZWZpbmVkJylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYoIWRhdGEuaGFzT3duUHJvcGVydHkoZmllbGROYW1lKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5kYXRhX2tlZXBlci5wdXNoKHtrZXkgOiBmaWVsZE5hbWUsIHZhbHVlIDogZGF0YVtmaWVsZE5hbWVdfSk7XG4gICAgICAgICAgICBvRm9ybURhdGFbZmllbGROYW1lXS52YWwoZGF0YVtmaWVsZE5hbWVdKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBub3RpZnlTYXZlZCgpOnRoaXNcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiUGxhY2Vob2xkZXIgZm9yIHNhdmUgbm90aWZpY2F0aW9uXCIpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgZ2V0UGFuZWwoKTpKUXVlcnlcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdBYnN0cmFjdFdpZGdldC5nZXRQYW5lbCgpJywgdGhpcy5zZWxlY3Rvcik7XG4gICAgICAgIHJldHVybiAkKHRoaXMuc2VsZWN0b3IpO1xuICAgIH1cbiAgICBoaWRlKGltbWVkaWF0ZSA6IGJvb2xlYW4gPSBmYWxzZSk6dGhpc1xuICAgIHtcbiAgICAgICAgbGV0IHBhbmVsOkpRdWVyeSA9IHRoaXMuZ2V0UGFuZWwoKTtcbiAgICAgICAgaWYocGFuZWwubGVuZ3RoKVxuICAgICAgICB7XG4gICAgICAgICAgICBpZihpbW1lZGlhdGUpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgcGFuZWwucmVwbGFjZVdpdGgodGhpcy5jcmVhdGVQbGFjZWhvbGRlcigpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBwYW5lbC5mYWRlT3V0KDIwMCwgKCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIHBhbmVsLnJlcGxhY2VXaXRoKHRoaXMuY3JlYXRlUGxhY2Vob2xkZXIoKSk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBzaG93KCk6dGhpc1xuICAgIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0Fic3RyYWN0V2lkZ2V0LnNob3coKScpO1xuXG4gICAgICAgIHRoaXMuZ2V0UGxhY2VIb2xkZXIoKS5yZXBsYWNlV2l0aCh0aGlzLnRlbXBsYXRlKTtcblxuICAgICAgICB0aGlzLmdldFBhbmVsKCkuc2hvdygpO1xuXG4gICAgICAgIGxldCBhRmllbGROYW1lcyA9IHRoaXMuZm9ybS5nZXRGaWVsZE5hbWVzKCk7XG4gICAgICAgIGxldCBvRm9ybUZpZWxkTWFuYWdlciA9IHRoaXMuZm9ybS5nZXRGaWVsZE1hbmFnZXIoKTtcbiAgICAgICAgbGV0IGN1cnJlbnRGaWVsZE5hbWU6c3RyaW5nO1xuICAgICAgICBsZXQga2VlcGVyRmllbGROYW1lOnN0cmluZztcbiAgICAgICAgbGV0IGZpZWxkOkpRdWVyeTtcblxuICAgICAgICBjb25zb2xlLmxvZyhcIlxcdFwiLCBhRmllbGROYW1lcyk7XG5cbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGFGaWVsZE5hbWVzLmxlbmd0aDsgaSsrKVxuICAgICAgICB7XG4gICAgICAgICAgICBjdXJyZW50RmllbGROYW1lID0gYUZpZWxkTmFtZXNbaV07XG4gICAgICAgICAgICBmb3IobGV0IHkgPSAwOyAgeSA8IHRoaXMuZGF0YV9rZWVwZXIubGVuZ3RoOyB5KyspXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAga2VlcGVyRmllbGROYW1lID0gdGhpcy5kYXRhX2tlZXBlclt5XS5rZXk7XG5cbiAgICAgICAgICAgICAgICBpZihjdXJyZW50RmllbGROYW1lID09PSBrZWVwZXJGaWVsZE5hbWUpXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhrZWVwZXJGaWVsZE5hbWUgKyAnID0+ICcgKyB0aGlzLmRhdGFfa2VlcGVyW3ldLnZhbHVlKTtcblxuICAgICAgICAgICAgICAgICAgICBmaWVsZCA9IG9Gb3JtRmllbGRNYW5hZ2VyW2N1cnJlbnRGaWVsZE5hbWVdO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKGZpZWxkLmlzKCdzZWxlY3QnKSAmJiAkLnRyaW0odGhpcy5kYXRhX2tlZXBlclt5XS52YWx1ZSkgPT0gJycpXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdBYnN0cmFjdFdpZGdldC5zaG93LicgKyBmaWVsZC5wcm9wKCduYW1lJykgKyAnLnZhbCgpJywga2VlcGVyRmllbGROYW1lLCB0aGlzLmRhdGFfa2VlcGVyW3ldLnZhbHVlKTtcblxuICAgICAgICAgICAgICAgICAgICBmaWVsZC52YWwodGhpcy5kYXRhX2tlZXBlclt5XS52YWx1ZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuYmluZEV2ZW50cygpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG59XG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID1cIi4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvanF1ZXJ5L2luZGV4LmQudHNcIi8+XG5cbmltcG9ydCB7R2VuZXJpY01vZGVsfSBmcm9tIFwiLi4vbW9kZWwvTW9kZWxcIjtcblxuZXhwb3J0IGludGVyZmFjZSBJRmllbGQgZXh0ZW5kcyBKUXVlcnkge1xuXG59XG5leHBvcnQgaW50ZXJmYWNlIElGaWVsZFZhbHVlcyB7XG4gICAgW2tleTogc3RyaW5nXSA6IHN0cmluZ1xufVxuZXhwb3J0IGNsYXNzIEZpZWxkVmFsdWVzIGltcGxlbWVudHMgSUZpZWxkVmFsdWVze1xuICAgIFtrZXk6IHN0cmluZ10gOiBzdHJpbmdcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJRm9ybSB7XG5cbiAgICBnZXRGaWVsZE1hbmFnZXIoKTpHZW5lcmljTW9kZWw8SUZpZWxkPjtcbiAgICBzeW5jRGF0YTxUPihtb2RlbCA6IFQpOlQ7XG4gICAgZ2V0RGF0YSgpOklNb2RlbFZhbHVlcztcbiAgICBnZXRGaWVsZE5hbWVzKCk6c3RyaW5nW11cbn1cbmV4cG9ydCBpbnRlcmZhY2UgSU1vZGVsVmFsdWVzIGV4dGVuZHMgR2VuZXJpY01vZGVsPHN0cmluZz57XG4gICAgW2tleTogc3RyaW5nXTogc3RyaW5nO1xufVxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEFic3RyYWN0Rm9ybTxGaWVsZENvbGxlY3Rpb24gZXh0ZW5kcyBHZW5lcmljTW9kZWw8SUZpZWxkPj4gaW1wbGVtZW50cyBJRm9ybXtcblxuICAgIHB1YmxpYyBmaWVsZHMgOiBGaWVsZENvbGxlY3Rpb247XG5cbiAgICBhYnN0cmFjdCBnZXREYXRhKCk6IElNb2RlbFZhbHVlcztcblxuICAgIGNvbnN0cnVjdG9yKGZpZWxkcyA6IEZpZWxkQ29sbGVjdGlvbilcbiAgICB7XG4gICAgICAgIHRoaXMuZmllbGRzID0gZmllbGRzO1xuICAgIH1cblxuICAgIGdldEZpZWxkTmFtZXMoKTpzdHJpbmdbXVxuICAgIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKHRoaXMuZmllbGRzKTtcbiAgICB9XG5cbiAgICBnZXRGaWVsZHMoKSA6IElGaWVsZFtdXG4gICAge1xuICAgICAgICBjb25zb2xlLmxvZygnQWJzdHJhY3RGb3JtLmdldEZpZWxkcygpJyk7XG5cbiAgICAgICAgbGV0IGFPdXQgOiBJRmllbGRbXSA9IFtdO1xuXG4gICAgICAgIGZvciggbGV0IHNGaWVsZCBpbiB0aGlzLmdldEZpZWxkTmFtZXMoKSkge1xuICAgICAgICAgICAgYU91dC5wdXNoKHRoaXMuZmllbGRzW3NGaWVsZF0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBhT3V0O1xuICAgIH1cblxuICAgIGdldEZpZWxkTWFuYWdlcigpOkZpZWxkQ29sbGVjdGlvblxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRzO1xuICAgIH1cblxuICAgIHN5bmNEYXRhPFQ+KG1vZGVsIDogVCk6IFQge1xuXG4gICAgICAgIGNvbnN0IGFNb2RlbCA6IFQgPSBtb2RlbDtcblxuICAgICAgICB0aGlzLmdldEZpZWxkTmFtZXMoKS5mb3JFYWNoKChmaWVsZE5hbWUpID0+IHtcblxuICAgICAgICAgICAgaWYodGhpcy5maWVsZHMuaGFzT3duUHJvcGVydHkoZmllbGROYW1lKSAmJiB0eXBlb2YodGhpcy5maWVsZHNbZmllbGROYW1lXS52YWwoKSkgIT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgLy9AdHMtaWdub3JlXG4gICAgICAgICAgICAgICAgYU1vZGVsW2ZpZWxkTmFtZV0gPSB0aGlzLmZpZWxkc1tmaWVsZE5hbWVdLnZhbCgpLnRvU3RyaW5nKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gYU1vZGVsO1xuICAgIH1cblxufVxuXG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID1cIi4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvanF1ZXJ5L2luZGV4LmQudHNcIi8+XG5cbmltcG9ydCB7QWJzdHJhY3RXaWRnZXR9IGZyb20gXCIuLi9jb250cmFjdHMvQWJzdHJhY3RXaWRnZXRcIjtcbmltcG9ydCB7SVdpZGdldH0gZnJvbSBcIi4uL2NvbnRyYWN0cy9JV2lkZ2V0XCI7XG5cbnR5cGUgQ2FsbGJhY2sgPSAoZXZlbnQ6SlF1ZXJ5LkNsaWNrRXZlbnQsIHdpZGdldCA6IEFic3RyYWN0V2lkZ2V0KSA9PiB2b2lkO1xuXG5leHBvcnQgY2xhc3MgQnV0dG9uIHtcblxuICAgIHByaXZhdGUgYnV0dG9uOiBKUXVlcnk7XG4gICAgcHJpdmF0ZSB3aWRnZXQ6IElXaWRnZXQ7XG5cbiAgICBjb25zdHJ1Y3RvcihzZWxlY3RvciA6IHN0cmluZywgY2FsbGJhY2sgOiBDYWxsYmFjaywgdGhpc1dpZGdldCA6IEFic3RyYWN0V2lkZ2V0KSB7XG5cbiAgICAgICAgdGhpcy5idXR0b24gPSAgJChzZWxlY3Rvcik7XG4gICAgICAgIHRoaXMuYnV0dG9uLm9uKCdjbGljaycsIChldmVudDpKUXVlcnkuQ2xpY2tFdmVudCkgPT4ge1xuICAgICAgICAgICAgY2FsbGJhY2soZXZlbnQsIHRoaXNXaWRnZXQpO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy53aWRnZXQgPSB0aGlzV2lkZ2V0O1xuICAgIH1cbn1cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPVwiLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9qcXVlcnkvaW5kZXguZC50c1wiLz5cblxuaW1wb3J0IHtJV2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0lXaWRnZXRcIjtcbmltcG9ydCB7RXJyb3JSZXNwb25zZX0gZnJvbSBcIi4vRXJyb3JSZXNwb25zZVwiO1xuXG5leHBvcnQgY2xhc3MgRXJyb3JCbG9jayB7XG5cbiAgICBwcml2YXRlIGVsZW1lbnQgOiBKUXVlcnk7XG4gICAgcHJpdmF0ZSBtb2RlbCA6IElXaWRnZXQ7XG4gICAgcHJpdmF0ZSBlcnJvcl91bDogSlF1ZXJ5O1xuXG4gICAgY29uc3RydWN0b3Iob01vZGVsIDogSVdpZGdldClcbiAgICB7XG4gICAgICAgIHRoaXMubW9kZWwgPSBvTW9kZWw7XG4gICAgICAgIHRoaXMuZWxlbWVudCA9ICQoJy5lcnJvcl9ibG9jaycpO1xuICAgICAgICB0aGlzLmVycm9yX3VsID0gJCgnLmVycm9yX3VsJyk7XG5cbiAgICB9XG4gICAgY2xlYXIoKVxuICAgIHtcbiAgICAgICAgdGhpcy5lcnJvcl91bC5odG1sKCcnKTtcbiAgICB9XG4gICAgaGlkZSgpXG4gICAge1xuICAgICAgICB0aGlzLmNsZWFyKCk7XG4gICAgICAgIHRoaXMuZWxlbWVudC5mYWRlT3V0KDIwMCk7XG4gICAgfVxuICAgIHNob3coZGF0YSA6IHtlcnJvcnM6IEVycm9yUmVzcG9uc2V9KVxuICAgIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnRXJyb3JCbG9jay5zaG93JywgZGF0YS5lcnJvcnMpO1xuICAgICAgICBsZXQgYUtleXMgPSB0aGlzLm1vZGVsLmZvcm0uZ2V0RmllbGROYW1lcygpO1xuICAgICAgICBsZXQgYUVycm9yTGlzdCA6IHN0cmluZ1tdID0gW107XG5cbiAgICAgICAgYUtleXMuZm9yRWFjaCgoZmllbGRfbmFtZSkgPT4ge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiAoZGF0YS5lcnJvcnNbZmllbGRfbmFtZV0pICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tb2RlbC5mb3JtLmdldEZpZWxkTWFuYWdlcigpW2ZpZWxkX25hbWVdLmFkZENsYXNzKCdlcnJvcicpO1xuICAgICAgICAgICAgICAgIGxldCBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGknKTtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmlubmVySFRNTCA9IGRhdGEuZXJyb3JzW2ZpZWxkX25hbWVdO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFcnJvckJsb2NrLnNob3cnLCAnZWxlbWVudC50b1N0cmluZygpJywgZWxlbWVudC50b1N0cmluZygpKTtcblxuICAgICAgICAgICAgICAgIGFFcnJvckxpc3QucHVzaChlbGVtZW50Lm91dGVySFRNTCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdFcnJvckJsb2NrLnNob3cnLCBhRXJyb3JMaXN0KTtcbiAgICAgICAgdGhpcy5lcnJvcl91bC5odG1sKGFFcnJvckxpc3Quam9pbihcIlxcblwiKSk7XG4gICAgICAgIHRoaXMuZWxlbWVudC5mYWRlSW4oMjAwKTtcblxuICAgICAgICAkKCcuZXJyb3InKS5vbignZm9jdXMnLCAoZSkgPT4ge1xuICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnZXJyb3InKTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIiwiXG5leHBvcnQgY2xhc3MgTWVzc2FnZTxUPiB7XG4gICAgdG9waWM6IHN0cmluZztcbiAgICBjb250ZW50OiBUXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSU1lc3NhZ2VDb250ZW50R2VuZXJpY3Mge1xuICAgIFtuYW1lOiBzdHJpbmddOiBzdHJpbmdcbn1cblxuaW50ZXJmYWNlIE1lc3NhZ2VIYW5kbGVyIHtcbiAgICBoYW5kbGUobWVzc2FnZSA6IE1lc3NhZ2U8SU1lc3NhZ2VDb250ZW50R2VuZXJpY3M+KTp2b2lkO1xufVxuZXhwb3J0IHR5cGUgTWVzc2FnZUhhbmRsZXJDYWxsYmFjazxUPiA9IChtZXNzYWdlIDogTWVzc2FnZTxUPikgPT4gdm9pZDtcblxuY2xhc3MgTWVzc2FnZUhhbmRsZXIge1xuXG4gICAgcHJpdmF0ZSBjYWxsYmFjazogTWVzc2FnZUhhbmRsZXJDYWxsYmFjazxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz47XG4gICAgdG9waWMgOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3Rvcih0b3BpYyA6IHN0cmluZywgY2FsbGJhY2sgOiBNZXNzYWdlSGFuZGxlckNhbGxiYWNrPElNZXNzYWdlQ29udGVudEdlbmVyaWNzPilcbiAgICB7XG4gICAgICAgIHRoaXMudG9waWMgPSB0b3BpYztcbiAgICAgICAgdGhpcy5jYWxsYmFjayA9IGNhbGxiYWNrO1xuXG4gICAgfVxuICAgIGhhbmRsZShtZXNzYWdlIDogTWVzc2FnZTxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz4pOnZvaWRcbiAgICB7XG4gICAgICAgIHRoaXMuY2FsbGJhY2sobWVzc2FnZSk7XG4gICAgfVxuXG59XG5pbnRlcmZhY2UgUmF3TWVzc2FnZSB7XG4gICAgdG9waWM6c3RyaW5nLFxuICAgIGRhdGE6SU1lc3NhZ2VDb250ZW50R2VuZXJpY3Ncbn1cblxuY2xhc3MgTWVzc2FnZURpc3BlbnNlciB7XG4gICAgcHJpdmF0ZSBzdGF0aWMgYUhhbmRsZXJzIDogTWVzc2FnZUhhbmRsZXJbXSA9IFtdO1xuXG4gICAgcHVibGljIHN0YXRpYyBhZGRMaXN0ZW5lcihoYW5kbGVyIDogTWVzc2FnZUhhbmRsZXIpXG4gICAge1xuICAgICAgICBNZXNzYWdlRGlzcGVuc2VyLmFIYW5kbGVycy5wdXNoKGhhbmRsZXIpO1xuICAgIH1cbiAgICBwdWJsaWMgc3RhdGljIGRpc3RyaWJ1dGUocmF3TWVzc2FnZSA6IFJhd01lc3NhZ2UpXG4gICAge1xuICAgICAgICBsZXQgbWVzc2FnZSA6IE1lc3NhZ2U8SU1lc3NhZ2VDb250ZW50R2VuZXJpY3M+O1xuICAgICAgICBmb3IgKGxldCBpPSAwOyBpIDwgTWVzc2FnZURpc3BlbnNlci5hSGFuZGxlcnMubGVuZ3RoOyBpKyspXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0IGhhbmRsZXIgPSBNZXNzYWdlRGlzcGVuc2VyLmFIYW5kbGVyc1tpXTtcblxuICAgICAgICAgICAgbWVzc2FnZSA9IG5ldyBNZXNzYWdlPElNZXNzYWdlQ29udGVudEdlbmVyaWNzPigpO1xuICAgICAgICAgICAgbWVzc2FnZS50b3BpYyA9IHJhd01lc3NhZ2UudG9waWM7XG4gICAgICAgICAgICBtZXNzYWdlLmNvbnRlbnQgPSByYXdNZXNzYWdlLmRhdGE7XG5cbiAgICAgICAgICAgIGlmKGhhbmRsZXIudG9waWMgPT0gbWVzc2FnZS50b3BpYylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBoYW5kbGVyLmhhbmRsZShtZXNzYWdlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE1lc3NlbmdlciB7XG5cbiAgICBwcml2YXRlIHN0YXRpYyBpc0xpc3RlbmluZzogYm9vbGVhbjtcblxuICAgIHByaXZhdGUgc3RhdGljIHN0YXJ0TGlzdGVuZXIoKSB7XG4gICAgICAgIE1lc3Nlbmdlci5pc0xpc3RlbmluZyA9IHRydWU7XG4gICAgICAgIGNvbnNvbGUubG9nKCdTdGFydCBsaXN0ZW5pbmcnKTtcblxuICAgICAgICBsZXQgY2FsbGJhY2sgPSAobWVzc2FnZUV2ZW50OiBNZXNzYWdlRXZlbnQpID0+IHtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlRXZlbnQuZGF0YSAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgICAgIHRyeVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ3R5cGUnLCB0eXBlb2YgbWVzc2FnZUV2ZW50LmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2VFdmVudC5kYXRhID09ICdzdHJpbmcnKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByYXdNZXNzYWdlIDogUmF3TWVzc2FnZSA9IEpTT04ucGFyc2UobWVzc2FnZUV2ZW50LmRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgTWVzc2FnZURpc3BlbnNlci5kaXN0cmlidXRlKHJhd01lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhdGNoIChlKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkNvdWxkIG5vdCBwYXJzZSBtZXNzYWdlXCIgKyBtZXNzYWdlRXZlbnQuZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZS50b1N0cmluZygpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIGNvbnNvbGUubG9nKCdBZGRpbmcgRXZlbnRMaXN0ZW5lcicpO1xuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbWVzc2FnZScsIGNhbGxiYWNrKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgc2VuZCh0b3BpYzogc3RyaW5nLCBjb250ZW50OiBhbnkpIHtcblxuICAgICAgICBjb25zb2xlLmxvZygnTWVzc2VuZ2VyLnNlbmQodG9waWMsIGNvbnRlbnQpJywgdG9waWMsIGNvbnRlbnQpO1xuICAgICAgICBsZXQgb01lc3NhZ2UgPSB7XG4gICAgICAgICAgICB0b3BpYzogdG9waWMsXG4gICAgICAgICAgICBjb250ZW50OiBjb250ZW50XG4gICAgICAgIH07XG5cbiAgICAgICAgY29uc3Qgb0VyZEVkaXRvciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdlcmRfZWRpdG9yX3dpbmRvdycpO1xuICAgICAgICAoPEhUTUxJRnJhbWVFbGVtZW50PiBvRXJkRWRpdG9yKS5jb250ZW50V2luZG93LnBvc3RNZXNzYWdlKEpTT04uc3RyaW5naWZ5KG9NZXNzYWdlKSwgJyonKTtcbiAgICB9XG5cbiAgICBzdGF0aWMgb25SZWNlaXZlKHRvcGljOnN0cmluZywgaGFuZGxlcjogTWVzc2FnZUhhbmRsZXJDYWxsYmFjazxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz4pIHtcblxuICAgICAgICBpZiAoIU1lc3Nlbmdlci5pc0xpc3RlbmluZykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ1N0YXJ0aWcgRXZlbnRMaXN0ZW5lcicpO1xuICAgICAgICAgICAgTWVzc2VuZ2VyLnN0YXJ0TGlzdGVuZXIoKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdFdmVudExpc3RlbmVyIHN0YXJ0ZWQnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnNvbGUubG9nKCdBZGQgZXZlbnQgbGlzdGVuZXIgZm9yOiAnICsgdG9waWMpO1xuICAgICAgICBjb25zdCBtZXNzYWdlSGFuZGxlciA9IG5ldyBNZXNzYWdlSGFuZGxlcih0b3BpYywgaGFuZGxlcik7XG4gICAgICAgIE1lc3NhZ2VEaXNwZW5zZXIuYWRkTGlzdGVuZXIobWVzc2FnZUhhbmRsZXIpO1xuXG4gICAgfVxufVxuIiwiZXhwb3J0IGNsYXNzIEdlbmVyaWNNb2RlbDxUPntcbiAgICBba2V5OiBzdHJpbmddOiBUXG59XG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID1cIi4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvanF1ZXJ5L2luZGV4LmQudHNcIi8+XG5cbmltcG9ydCB7QWJzdHJhY3RGb3JtLCBJRmllbGQsIElGb3JtfSBmcm9tIFwiLi4vY29udHJhY3RzL0Zvcm1cIjtcbmltcG9ydCB7R2VuZXJpY01vZGVsfSBmcm9tIFwiLi9Nb2RlbFwiO1xuXG4vLyBXaGF0IGZpZWxkcyBkbyB3ZSBoYXZlXG5leHBvcnQgY2xhc3MgTW9kZWxNb2RlbDxUPiBleHRlbmRzIEdlbmVyaWNNb2RlbDxUPiB7XG4gICAgW25hbWU6IHN0cmluZ106IFRcblxuICAgIGlkOiBUID0gbnVsbDtcbiAgICBuYW1lOiBUO1xuICAgIHRpdGxlOiBUO1xuICAgIG1vZHVsZV9pZDogVDtcbiAgICBjcmVhdGVfbmV3X21vZHVsZTogVDtcbiAgICBuZXdfbW9kdWxlX25hbWU6IFQ7XG59XG5cbmV4cG9ydCBjbGFzcyBNb2RlbEZvcm1GaWVsZHMgZXh0ZW5kcyBNb2RlbE1vZGVsPElGaWVsZD4ge1xuICAgIGNvbnN0cnVjdG9yKClcbiAgICB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMubmFtZSA9ICQoJyNmbGRfbmFtZScpO1xuICAgICAgICB0aGlzLnRpdGxlID0gJCgnI2ZsZF90aXRsZScpO1xuICAgICAgICB0aGlzLm1vZHVsZV9pZCA9ICQoJyNmbGRfbW9kdWxlX2lkJyk7XG4gICAgICAgIHRoaXMuY3JlYXRlX25ld19tb2R1bGUgPSAkKCcjZmxkX2NyZWF0ZV9uZXdfbW9kdWxlJyk7XG4gICAgICAgIHRoaXMubmV3X21vZHVsZV9uYW1lID0gJCgnI2ZsZF9uZXdfbW9kdWxlJyk7XG4gICAgfVxufVxuZXhwb3J0IGNsYXNzIEVkaXRNb2RlbEZvcm1GaWVsZHMgZXh0ZW5kcyBNb2RlbEZvcm1GaWVsZHMge31cbmNsYXNzIE1vZGVsVmFsdWVzIGV4dGVuZHMgTW9kZWxNb2RlbDxzdHJpbmc+e31cblxuZXhwb3J0IGNsYXNzIE5ld01vZGVsRm9ybSBleHRlbmRzIEFic3RyYWN0Rm9ybTxNb2RlbEZvcm1GaWVsZHM+IGltcGxlbWVudHMgSUZvcm17XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIobmV3IE1vZGVsRm9ybUZpZWxkcygpKTtcbiAgICAgICAgdGhpcy5maWVsZHMgPSBuZXcgTW9kZWxGb3JtRmllbGRzKCk7XG5cbiAgICAgICAgLy8gZmlyc3QgaXRlbSBpcyBjaGFyLCBvbmx5IG51bWJlcnMsIGxldHRlcnMgYW5kIHVuZGVyc2NvcmVzLlxuICAgICAgICB0aGlzLmZpZWxkcy5uYW1lLm9uKCdrZXl1cCcsIHRoaXMubW9kZWxOYW1lRmlsdGVyKTtcblxuICAgICAgICAvLyB0b2dnbGUgY3JlYXRlIG5ldyBmb3JtIGZpZWxkIGFuZCBrZWVwIHRyYWNrIG9mIGNyZWF0ZSBuZXcgIG1vZHVsZSBvciB1c2UgZXhpc3RpbmcgbW9kdWxlXG4gICAgICAgIHRoaXMuZmllbGRzLm1vZHVsZV9pZC5vbignY2hhbmdlJywgdGhpcy50b2dnbGVDcmVhdGVOZXdNb2R1bGUpO1xuICAgIH1cbiAgICBnZXREYXRhKCk6IE1vZGVsVmFsdWVzIHtcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBwcml2YXRlIHRvZ2dsZUNyZWF0ZU5ld01vZHVsZSgpOmZhbHNlIHtcbiAgICAgICAgbGV0IGJDcmVhdGVOZXdNb2R1bGUgPSAoJCgnb3B0aW9uOnNlbGVjdGVkJywgdGhpcy5maWVsZHMubW9kdWxlX2lkKS5kYXRhKCdhY3Rpb24nKSA9PT0gJ19jcmVhdGVfbmV3XycpID8gMSA6IDA7XG5cbiAgICAgICAgY29uc3QgbmV3X21vZHVsZV9maWVsZF9ibG9jayA9ICQoJyNuZXdfbW9kdWxlX2ZpZWxkX2Jsb2NrJyk7XG5cbiAgICAgICAgbmV3X21vZHVsZV9maWVsZF9ibG9jay5jc3MoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgJ2Rpc3BsYXknIDogYkNyZWF0ZU5ld01vZHVsZSA/ICdibG9jaycgOiAnbm9uZSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgICAgdGhpcy5maWVsZHMuY3JlYXRlX25ld19tb2R1bGUudmFsKGJDcmVhdGVOZXdNb2R1bGUpO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHByaXZhdGUgbW9kZWxOYW1lRmlsdGVyKGV2ZW50OkpRdWVyeS5LZXlVcEV2ZW50KTpmYWxzZSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgbGV0IG5ld1ZhbCA6IHN0cmluZztcbiAgICAgICAgbmV3VmFsID0gdGhpcy5maWVsZHMubmFtZS52YWwoKVxuICAgICAgICAgICAgLnRvU3RyaW5nKClcbiAgICAgICAgICAgIC50b0xvd2VyQ2FzZSgpXG4gICAgICAgICAgICAucmVwbGFjZSgvXlteYS16XS8sICcnKVxuICAgICAgICAgICAgLnJlcGxhY2UoL15bXmEtejAtOV9dKyQvLCAnJyk7XG5cbiAgICAgICAgdGhpcy5maWVsZHMubmFtZS52YWwobmV3VmFsKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBFZGl0TW9kZWxGb3JtIGV4dGVuZHMgTmV3TW9kZWxGb3JtIGltcGxlbWVudHMgSUZvcm17fVxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9XCIuLi8uLi9ub2RlX21vZHVsZXMvQHR5cGVzL2pxdWVyeS9pbmRleC5kLnRzXCIvPlxuXG5pbXBvcnQge0Fic3RyYWN0Rm9ybSwgSUZpZWxkLCBJRm9ybX0gZnJvbSBcIi4uL2NvbnRyYWN0cy9Gb3JtXCI7XG5pbXBvcnQge0dlbmVyaWNNb2RlbH0gZnJvbSBcIi4vTW9kZWxcIjtcblxuLy8gV2hhdCBmaWVsZHMgZG8gd2UgaGF2ZVxuZXhwb3J0IGNsYXNzIFByb3BlcnR5TW9kZWw8VD4gZXh0ZW5kcyBHZW5lcmljTW9kZWw8VD57XG4gICAgW25hbWU6IHN0cmluZ106IFRcblxuICAgIC8vIHJlcXVpcmVkXG4gICAgbGFiZWwgOiBUO1xuICAgIG5hbWUgOiBUO1xuICAgIGZvcm1fZmllbGRfdHlwZV9pZCA6IFQ7XG5cbiAgICAvLyBvcHRpb25hbFxuICAgIGlkPyA6IFQ7XG4gICAgZGF0YV90eXBlX2lkPyA6IFQ7XG4gICAgZm9ybV9maWVsZF9sb29rdXBzPyA6IFQ7XG4gICAgcmVxdWlyZWQ/IDogVDtcbiAgICBpc191bmlxdWU/IDogVDtcbiAgICBpc19wcmltYXJ5X2tleT8gOiBUO1xuICAgIGljb24/IDogVDtcbiAgICBma19tb2RlbF9maWVsZD8gOiBUO1xuICAgIGZrX21vZGVsPyA6IFQ7XG59XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eVZhbHVlcyBleHRlbmRzIFByb3BlcnR5TW9kZWw8c3RyaW5nPnt9XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eUZvcm1GaWVsZHMgZXh0ZW5kcyBQcm9wZXJ0eU1vZGVsPElGaWVsZD57XG5cbiAgICBjb25zdHJ1Y3RvcigpXG4gICAge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICB0aGlzLmxhYmVsID0gJCgnI2ZsZF9sYWJlbCcpO1xuICAgICAgICB0aGlzLm5hbWUgPSAkKCcjZmxkX25hbWUnKTtcbiAgICAgICAgdGhpcy5mb3JtX2ZpZWxkX3R5cGVfaWQgPSAkKCcjZmxkX2Zvcm1fZmllbGRfdHlwZV9pZCcpO1xuXG4gICAgICAgIC8vIG51bGxhYmxlIGl0ZW1zXG4gICAgICAgIHRoaXMuaWQgPSAkKCcjZmxkX2lkJyk7XG4gICAgICAgIHRoaXMuZGF0YV90eXBlX2lkID0gJCgnI2ZsZF9kYXRhX3R5cGVfaWQnKTtcbiAgICAgICAgdGhpcy5mb3JtX2ZpZWxkX2xvb2t1cHMgPSAkKCcjZmxkX2Zvcm1fZmllbGRfbG9va3VwcycpO1xuICAgICAgICB0aGlzLnJlcXVpcmVkID0gJCgnI2ZsZF9yZXF1aXJlZCcpO1xuICAgICAgICB0aGlzLmlzX3VuaXF1ZSA9ICQoJyNmbGRfaXNfdW5pcXVlJyk7XG4gICAgICAgIHRoaXMuaXNfcHJpbWFyeV9rZXkgPSAkKCcjZmxkX2lzX3ByaW1hcnlfa2V5Jyk7XG4gICAgICAgIHRoaXMuaWNvbiA9ICQoJyNmbGRfaWNvbicpO1xuICAgICAgICB0aGlzLmZrX21vZGVsX2ZpZWxkID0gJCgnI2ZsZF9ma19tb2RlbF9maWVsZCcpO1xuICAgICAgICB0aGlzLmZrX21vZGVsID0gICQoJyNmbGRfZmtfbW9kZWwnKTtcbiAgICB9XG59XG5leHBvcnQgY2xhc3MgRWRpdFByb3BlcnR5Rm9ybUZpZWxkcyBleHRlbmRzIFByb3BlcnR5Rm9ybUZpZWxkcyB7XG59XG5leHBvcnQgY2xhc3MgTmV3UHJvcGVydHlGb3JtIGV4dGVuZHMgQWJzdHJhY3RGb3JtPFByb3BlcnR5Rm9ybUZpZWxkcz4gaW1wbGVtZW50cyBJRm9ybXtcblxuICAgIHRhYmxlX25hbWUgOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIobmV3IFByb3BlcnR5Rm9ybUZpZWxkcygpKTtcbiAgICAgICAgY29uc29sZS5sb2coJ05ld1Byb3BlcnR5Rm9ybS5jb25zdHJ1Y3RvcicpO1xuICAgICAgICB0aGlzLmZpZWxkcyA9IG5ldyBQcm9wZXJ0eUZvcm1GaWVsZHMoKTtcbiAgICB9XG5cbiAgICBnZXRGaWVsZE1hbmFnZXIoKTogUHJvcGVydHlGb3JtRmllbGRzIHtcbiAgICAgICAgY29uc29sZS5sb2coJ05ld1Byb3BlcnR5Rm9ybS5nZXRGaWVsZE1hbmFnZXInKTtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9wZXJ0eUZvcm1GaWVsZHMoKTtcbiAgICB9XG5cbiAgICBnZXREYXRhKCk6UHJvcGVydHlWYWx1ZXN7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ05ld1Byb3BlcnR5Rm9ybS5zeW5jRGF0YSgpJyk7XG4gICAgICAgIGxldCBhRGF0YSA9IHN1cGVyLnN5bmNEYXRhKG5ldyBQcm9wZXJ0eVZhbHVlcygpKTtcblxuICAgICAgICBjb25zb2xlLmxvZyhcIlxcdFwiLCBcImZpZWxkc1wiLCB0aGlzLmZpZWxkcyk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsIFwiZGF0YVwiLCBhRGF0YSk7XG5cbiAgICAgICAgYURhdGFbJ3RhYmxlX25hbWUnXSA9IHRoaXMudGFibGVfbmFtZTtcbiAgICAgICAgcmV0dXJuIGFEYXRhO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBFZGl0UHJvcGVydHlGb3JtIGV4dGVuZHMgTmV3UHJvcGVydHlGb3JtIGltcGxlbWVudHMgSUZvcm17fVxuIiwiaW1wb3J0IHtJV2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0lXaWRnZXRcIjtcblxuLy8vIDxyZWZlcmVuY2UgcGF0aCA9XCIuLi8uLi9ub2RlX21vZHVsZXMvQHR5cGVzL2pxdWVyeS9pbmRleC5kLnRzXCIvPlxuXG5pbnRlcmZhY2UgSUZvcm0ge1xuICAgIGZvcm0gOiBzdHJpbmc7XG59XG5leHBvcnQgZW51bSBUeXBlVHlwZSB7XG4gICAgcG9wdXAgPSBcInBvcHVwXCIsXG4gICAgcGFuZWwgPSAncGFuZWwnXG59XG5leHBvcnQgZW51bSBTdWJqZWN0VHlwZSB7XG4gICAgbW9kZWwgPSBcIm1vZGVsXCIsXG4gICAgcHJvcGVydHkgPSBcInByb3BlcnR5XCJcbn1cblxuZXhwb3J0IGNsYXNzIFRlbXBsYXRle1xuXG4gICAgc3RhdGljIGdldEZvcm0od2lkZ2V0IDogSVdpZGdldClcbiAgICB7XG4gICAgICAgIGNvbnN0IG9EYXRhID0ge1xuICAgICAgICAgICAgX2RvIDogJ0dldEZvcm0nLFxuICAgICAgICAgICAgdHlwZSA6IHdpZGdldC50eXBlLFxuICAgICAgICAgICAgc3ViamVjdCA6IHdpZGdldC5zdWJqZWN0XG4gICAgICAgIH07XG5cbiAgICAgICAgJC5nZXQoJy9tb2RlbGxlci9zY2hlbWEvc2hvdycsIG9EYXRhLCAoZGF0YSA6IElGb3JtKSA9PiB7XG5cbiAgICAgICAgICAgIHdpZGdldC50ZW1wbGF0ZSA9IGRhdGEuZm9ybTtcblxuICAgICAgICB9LCAnanNvbicpLnRoZW4oKCkgPT4ge1xuXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIlRlbXBsYXRlIGxvYWRlZCBcIiArIHdpZGdldC50eXBlICsgJyAnICsgd2lkZ2V0LnN1YmplY3QpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiIsImltcG9ydCB7TW9kZWxQYW5lbH0gZnJvbSBcIi4vTW9kZWxQYW5lbFwiO1xuaW1wb3J0IHtQcm9wZXJ0eVBhbmVsfSBmcm9tIFwiLi9Qcm9wZXJ0eVBhbmVsXCI7XG5pbXBvcnQge01vZGVsUG9wdXB9IGZyb20gXCIuL01vZGVsUG9wdXBcIjtcbmltcG9ydCB7UHJvcGVydHlQb3B1cH0gZnJvbSBcIi4vUHJvcGVydHlQb3B1cFwiO1xuaW1wb3J0IHtJTWVzc2FnZUNvbnRlbnRHZW5lcmljcywgTWVzc2FnZSwgTWVzc2VuZ2VyfSBmcm9tIFwiLi4vbW9kZWwvTWVzc2VuZ2VyXCI7XG5pbXBvcnQge0lXaWRnZXR9IGZyb20gXCIuLi9jb250cmFjdHMvSVdpZGdldFwiO1xuXG5leHBvcnQgY2xhc3MgRWRpdG9yIHtcblxuICAgIHByaXZhdGUgc3RhdGljIG1vZGVsUGFuZWwgOiBNb2RlbFBhbmVsO1xuICAgIHByaXZhdGUgc3RhdGljIG1vZGVsUG9wdXAgOiBNb2RlbFBvcHVwO1xuICAgIHByaXZhdGUgc3RhdGljIHByb3BlcnR5UGFuZWwgOiBQcm9wZXJ0eVBhbmVsO1xuICAgIHByaXZhdGUgc3RhdGljIHByb3BlcnR5UG9wdXAgOiBQcm9wZXJ0eVBvcHVwO1xuXG4gICAgc3RhdGljIGluaXQoKVxuICAgIHtcbiAgICAgICAgRWRpdG9yLm1vZGVsUGFuZWwgPSBuZXcgTW9kZWxQYW5lbCh0aGlzKTtcbiAgICAgICAgRWRpdG9yLm1vZGVsUG9wdXAgPSBuZXcgTW9kZWxQb3B1cCh0aGlzKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UGFuZWwgPSBuZXcgUHJvcGVydHlQYW5lbCh0aGlzKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UG9wdXAgPSBuZXcgUHJvcGVydHlQb3B1cCh0aGlzKTtcbiAgICB9XG4gICAgc3RhdGljIHN0YXJ0KCk6dm9pZFxuICAgIHtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgICAgIEVkaXRvci5tb2RlbFBhbmVsLmhpZGUoKTtcbiAgICAgICAgRWRpdG9yLm1vZGVsUG9wdXAuaGlkZSgpO1xuXG4gICAgICAgIEVkaXRvci5wcm9wZXJ0eVBhbmVsLmhpZGUoKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UG9wdXAuaGlkZSgpO1xuXG4gICAgICAgIE1lc3Nlbmdlci5vblJlY2VpdmUoJ2FkZF9tb2RlbCcsIEVkaXRvci5zaG93QWRkTW9kZWxQb3B1cCk7XG4gICAgICAgIE1lc3Nlbmdlci5vblJlY2VpdmUoJ2VkaXRfbW9kZWwnLCBFZGl0b3Iuc2hvd0VkaXRNb2RlbFBhbmVsKTtcblxuICAgICAgICBNZXNzZW5nZXIub25SZWNlaXZlKCdhZGRfcHJvcGVydHknLCBFZGl0b3Iuc2hvd0FkZFByb3BlcnR5UG9wdXApO1xuICAgICAgICBNZXNzZW5nZXIub25SZWNlaXZlKCdlZGl0X3Byb3BlcnR5JywgRWRpdG9yLnNob3dFZGl0UHJvcGVydHlQYW5lbCk7XG4gICAgfVxuICAgIHByaXZhdGUgc3RhdGljIGdldEFsbFBhbmVscygpOklXaWRnZXRbXVxuICAgIHtcbiAgICAgICAgcmV0dXJuIFtFZGl0b3IubW9kZWxQYW5lbCwgRWRpdG9yLm1vZGVsUG9wdXAsIEVkaXRvci5wcm9wZXJ0eVBhbmVsLCBFZGl0b3IucHJvcGVydHlQb3B1cF07XG4gICAgfVxuXG4gICAgc3RhdGljIGFjdGl2ZVBhbmVsKG5ld0FjdGl2ZVBhbmVsOklXaWRnZXQpXG4gICAge1xuICAgICAgICBjb25zdCBhQWxsVmlld3MgPSBFZGl0b3IuZ2V0QWxsUGFuZWxzKCk7XG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBhQWxsVmlld3MubGVuZ3RoOyBpKyspXG4gICAgICAgIHtcbiAgICAgICAgICAgIGFBbGxWaWV3c1tpXS5oaWRlKHRydWUpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKCdFZGl0b3IuYWN0aXZlUGFuZWwubmV3QWN0aXZlUGFuZWwnLCBuZXdBY3RpdmVQYW5lbCk7XG4gICAgICAgIG5ld0FjdGl2ZVBhbmVsLnNob3coKTtcbiAgICB9XG4gICAgc3RhdGljIHNob3dFZGl0UHJvcGVydHlQYW5lbChtZXNzYWdlIDogTWVzc2FnZTxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz4pOnZvaWRcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFZGl0b3Iuc2hvd0VkaXRQcm9wZXJ0eVBhbmVsLm1lc3NhZ2UnLCBtZXNzYWdlKTtcbiAgICAgICAgRWRpdG9yLmFjdGl2ZVBhbmVsKEVkaXRvci5wcm9wZXJ0eVBhbmVsKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UGFuZWwuc2V0RGF0YShtZXNzYWdlLmNvbnRlbnQpO1xuXG4gICAgfVxuICAgIHN0YXRpYyBzaG93RWRpdE1vZGVsUGFuZWwobWVzc2FnZSA6IE1lc3NhZ2U8SU1lc3NhZ2VDb250ZW50R2VuZXJpY3M+KTp2b2lkXG4gICAge1xuICAgICAgICBjb25zb2xlLmxvZygnRWRpdG9yLnNob3dFZGl0TW9kZWxQYW5lbC5tZXNzYWdlJywgbWVzc2FnZSk7XG4gICAgICAgIEVkaXRvci5hY3RpdmVQYW5lbChFZGl0b3IubW9kZWxQYW5lbCk7XG4gICAgICAgIEVkaXRvci5tb2RlbFBhbmVsLnNldERhdGEobWVzc2FnZS5jb250ZW50KTtcbiAgICB9XG4gICAgc3RhdGljIHNob3dBZGRQcm9wZXJ0eVBvcHVwKG1lc3NhZ2UgOiBNZXNzYWdlPElNZXNzYWdlQ29udGVudEdlbmVyaWNzPik6dm9pZFxuICAgIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0VkaXRvci5zaG93QWRkUHJvcGVydHlQb3B1cCcsIG1lc3NhZ2UuY29udGVudCk7XG4gICAgICAgIEVkaXRvci5hY3RpdmVQYW5lbChFZGl0b3IucHJvcGVydHlQb3B1cCk7XG4gICAgICAgIEVkaXRvci5wcm9wZXJ0eVBvcHVwLnNldERhdGEobWVzc2FnZS5jb250ZW50KTtcbiAgICB9XG4gICAgc3RhdGljIHNob3dBZGRNb2RlbFBvcHVwKCk6dm9pZFxuICAgIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0VkaXRvci5zaG93QWRkTW9kZWxQb3B1cCcpO1xuICAgICAgICBFZGl0b3IuYWN0aXZlUGFuZWwoRWRpdG9yLm1vZGVsUG9wdXApO1xuICAgIH1cbiAgICBzdGF0aWMgb25Qcm9wZXJ0eURlbGV0ZWQoZGF0YTpNZXNzYWdlPElNZXNzYWdlQ29udGVudEdlbmVyaWNzPik6dm9pZFxuICAgIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0VkaXRvci5vblByb3BlcnR5RGVsZXRlZCcsIGRhdGEpO1xuICAgIH1cbiAgICBzdGF0aWMgb25Qcm9wZXJ0eVNhdmVkKGRhdGE6TWVzc2FnZTxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz4pOnZvaWRcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFZGl0b3Iub25Qcm9wZXJ0eVNhdmVkJywgZGF0YSk7XG4gICAgfVxuICAgIHN0YXRpYyBvblByb3BlcnR5Q3JlYXRlZChkYXRhOk1lc3NhZ2U8SU1lc3NhZ2VDb250ZW50R2VuZXJpY3M+KTp2b2lkXG4gICAge1xuICAgICAgICBjb25zb2xlLmxvZygnRWRpdG9yLm9uUHJvcGVydHlDcmVhdGVkJywgZGF0YSk7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ0VkaXRvci5vblByb3BlcnR5Q3JlYXRlZCcsICdFZGl0b3IucHJvcGVydHlQb3B1cC5oaWRlKCknKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UG9wdXAuaGlkZSgpO1xuXG4gICAgICAgIE1lc3Nlbmdlci5zZW5kKCdwcm9wZXJ0eV9hZGRlZCcsIGRhdGEuY29udGVudCk7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ0VkaXRvci5vblByb3BlcnR5Q3JlYXRlZCcsICdFZGl0b3IucHJvcGVydHlQYW5lbC5zaG93KCknKTtcbiAgICAgICAgRWRpdG9yLnByb3BlcnR5UGFuZWwuc2hvdygpO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdFZGl0b3Iub25Qcm9wZXJ0eUNyZWF0ZWQnLCAnRWRpdG9yLnByb3BlcnR5UGFuZWwuc2V0RGF0YShkYXRhLmNvbnRlbnQpOycsIGRhdGEuY29udGVudCk7XG4gICAgICAgIEVkaXRvci5wcm9wZXJ0eVBhbmVsLnNldERhdGEoZGF0YS5jb250ZW50KTtcbiAgICB9XG4gICAgc3RhdGljIG9uTW9kZWxDcmVhdGVkKGRhdGE6TWVzc2FnZTxJTWVzc2FnZUNvbnRlbnRHZW5lcmljcz4pOnZvaWRcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFZGl0b3Iub25Nb2RlbENyZWF0ZWQnLCBkYXRhKTtcblxuICAgICAgICBNZXNzZW5nZXIuc2VuZCgnbW9kZWxfYWRkZWQnLCBkYXRhKTtcbiAgICAgICAgRWRpdG9yLm1vZGVsUG9wdXAuaGlkZSgpO1xuICAgICAgICBFZGl0b3IubW9kZWxQYW5lbC5zaG93KCk7XG4gICAgICAgIEVkaXRvci5tb2RlbFBhbmVsLnNldERhdGEoZGF0YS5jb250ZW50KTtcbiAgICB9XG5cbn1cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPVwiLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9qcXVlcnkvaW5kZXguZC50c1wiLz5cblxuaW1wb3J0IHtJV2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0lXaWRnZXRcIjtcbmltcG9ydCB7RXJyb3JCbG9ja30gZnJvbSBcIi4uL21vZGVsL0Vycm9yQmxvY2tcIjtcblxuaW1wb3J0IHtFZGl0TW9kZWxGb3JtfSBmcm9tIFwiLi4vbW9kZWwvTW9kZWxGb3JtXCI7XG5pbXBvcnQge0Fic3RyYWN0V2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0Fic3RyYWN0V2lkZ2V0XCI7XG5pbXBvcnQge0VkaXRvcn0gZnJvbSBcIi4vRWRpdG9yXCI7XG5pbXBvcnQge1N1YmplY3RUeXBlLCBUZW1wbGF0ZSwgVHlwZVR5cGV9IGZyb20gXCIuLi9tb2RlbC9UZW1wbGF0ZVwiO1xuXG5leHBvcnQgY2xhc3MgTW9kZWxQYW5lbCBleHRlbmRzIEFic3RyYWN0V2lkZ2V0IGltcGxlbWVudHMgSVdpZGdldHtcblxuICAgIHB1YmxpYyBzZWxlY3RvcjpzdHJpbmcgPSAnI2VkaXRfbW9kZWxfcGFuZWwnO1xuICAgIHB1YmxpYyBmb3JtOkVkaXRNb2RlbEZvcm07XG4gICAgcHVibGljIGVycm9yX2Jsb2NrOkVycm9yQmxvY2s7XG5cbiAgICBwcml2YXRlIGVkaXRvciA6IEVkaXRvcjtcblxuICAgIGNvbnN0cnVjdG9yKGVkaXRvciA6IEVkaXRvcilcbiAgICB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMudHlwZSA9IFR5cGVUeXBlLnBhbmVsO1xuICAgICAgICB0aGlzLnN1YmplY3QgPSBTdWJqZWN0VHlwZS5tb2RlbDtcblxuICAgICAgICB0aGlzLmVkaXRvciA9IGVkaXRvcjtcbiAgICAgICAgdGhpcy5mb3JtID0gbmV3IEVkaXRNb2RlbEZvcm0oKTtcblxuICAgICAgICBUZW1wbGF0ZS5nZXRGb3JtKHRoaXMpO1xuICAgIH1cbiAgICBiaW5kRXZlbnRzKCk6dGhpc1xuICAgIHtcblxuXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbn1cbiIsIi8vLyA8cmVmZXJlbmNlIHBhdGggPVwiLi4vLi4vbm9kZV9tb2R1bGVzL0B0eXBlcy9qcXVlcnkvaW5kZXguZC50c1wiLz5cblxuaW1wb3J0IHtJV2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0lXaWRnZXRcIjtcbmltcG9ydCB7RXJyb3JCbG9ja30gZnJvbSBcIi4uL21vZGVsL0Vycm9yQmxvY2tcIjtcbmltcG9ydCB7QnV0dG9ufSBmcm9tIFwiLi4vbW9kZWwvQnV0dG9uXCI7XG5pbXBvcnQge05ld01vZGVsRm9ybX0gZnJvbSBcIi4uL21vZGVsL01vZGVsRm9ybVwiO1xuaW1wb3J0IHtBYnN0cmFjdFdpZGdldH0gZnJvbSBcIi4uL2NvbnRyYWN0cy9BYnN0cmFjdFdpZGdldFwiO1xuaW1wb3J0IHtFZGl0b3J9IGZyb20gXCIuL0VkaXRvclwiO1xuaW1wb3J0IHtTdWJqZWN0VHlwZSwgVGVtcGxhdGUsIFR5cGVUeXBlfSBmcm9tIFwiLi4vbW9kZWwvVGVtcGxhdGVcIjtcblxuZXhwb3J0IGNsYXNzIE1vZGVsUG9wdXAgZXh0ZW5kcyBBYnN0cmFjdFdpZGdldCBpbXBsZW1lbnRzIElXaWRnZXR7XG5cbiAgICBwdWJsaWMgc2VsZWN0b3I6c3RyaW5nID0gJyNhZGRfbW9kZWxfcG9wdXAnO1xuICAgIHB1YmxpYyBjcmVhdGVfYnV0dG9uOkJ1dHRvbjtcbiAgICBwcml2YXRlIGVkaXRvcjpFZGl0b3I7XG5cbiAgICBjb25zdHJ1Y3RvcihlZGl0b3IgOiBFZGl0b3IpXG4gICAge1xuICAgICAgICBzdXBlcigpO1xuXG4gICAgICAgIHRoaXMudHlwZSA9IFR5cGVUeXBlLnBvcHVwO1xuICAgICAgICB0aGlzLnN1YmplY3QgPSBTdWJqZWN0VHlwZS5tb2RlbDtcbiAgICAgICAgdGhpcy5lZGl0b3IgPSBlZGl0b3I7XG4gICAgICAgIHRoaXMuZm9ybSA9IG5ldyBOZXdNb2RlbEZvcm0oKTtcblxuICAgICAgICBUZW1wbGF0ZS5nZXRGb3JtKHRoaXMpO1xuICAgIH1cbiAgICBiaW5kRXZlbnRzKCk6dGhpc1xuICAgIHtcbiAgICAgICAgdGhpcy5jcmVhdGVfYnV0dG9uID0gbmV3IEJ1dHRvbignI2ZsZF9jcmVhdGVfbW9kZWwnLCB0aGlzLmNyZWF0ZSwgdGhpcyk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIHNob3coKTogdGhpcyB7XG5cbiAgICAgICAgdGhpcy5nZXRQbGFjZUhvbGRlcigpLnJlcGxhY2VXaXRoKHRoaXMudGVtcGxhdGUpO1xuICAgICAgICB0aGlzLmJpbmRFdmVudHMoKTtcbiAgICAgICAgdGhpcy5mb3JtID0gbmV3IE5ld01vZGVsRm9ybSgpO1xuXG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIHByaXZhdGUgY3JlYXRlID0gKGV2ZW50IDogSlF1ZXJ5LkNsaWNrRXZlbnQsIHNlbGZXaWRnZXQgOiBNb2RlbFBvcHVwKSA6IHZvaWQgPT4ge1xuXG4gICAgICAgIGxldCBlcnJvcl9ibG9jayA9IG5ldyBFcnJvckJsb2NrKHRoaXMpO1xuICAgICAgICBlcnJvcl9ibG9jay5jbGVhcigpO1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGxldCBvRGF0YSA9IHtcbiAgICAgICAgICAgICdfZG8nOiAnQ3JlYXRlTW9kZWwnLFxuICAgICAgICAgICAgJ2RhdGEnOiBzZWxmV2lkZ2V0LmZvcm0uZ2V0RGF0YSgpXG4gICAgICAgIH07XG4gICAgICAgICQucG9zdCh3aW5kb3cubG9jYXRpb24uaHJlZiwgb0RhdGEsIChkYXRhKSA9PiB7XG5cbiAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBlcnJvcl9ibG9jay5zaG93KGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIEVkaXRvci5vbk1vZGVsQ3JlYXRlZChkYXRhKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9LCAnanNvbicpO1xuXG4gICAgfVxufVxuIiwiLy8vIDxyZWZlcmVuY2UgcGF0aCA9XCIuLi8uLi9ub2RlX21vZHVsZXMvQHR5cGVzL2pxdWVyeS9pbmRleC5kLnRzXCIvPlxuXG5pbXBvcnQge0lXaWRnZXR9IGZyb20gXCIuLi9jb250cmFjdHMvSVdpZGdldFwiO1xuaW1wb3J0IHtFZGl0UHJvcGVydHlGb3JtfSBmcm9tIFwiLi4vbW9kZWwvUHJvcGVydHlGb3JtXCI7XG5pbXBvcnQge0Fic3RyYWN0V2lkZ2V0fSBmcm9tIFwiLi4vY29udHJhY3RzL0Fic3RyYWN0V2lkZ2V0XCI7XG5pbXBvcnQge0VkaXRvcn0gZnJvbSBcIi4vRWRpdG9yXCI7XG5pbXBvcnQge1N1YmplY3RUeXBlLCBUZW1wbGF0ZSwgVHlwZVR5cGV9IGZyb20gXCIuLi9tb2RlbC9UZW1wbGF0ZVwiO1xuaW1wb3J0IHtCdXR0b259IGZyb20gXCIuLi9tb2RlbC9CdXR0b25cIjtcbmltcG9ydCB7QWpheH0gZnJvbSBcIi4uL0FqYXhcIjtcbmltcG9ydCB7RXJyb3JCbG9ja30gZnJvbSBcIi4uL21vZGVsL0Vycm9yQmxvY2tcIjtcbmltcG9ydCB7SU1lc3NhZ2VDb250ZW50R2VuZXJpY3MsIE1lc3Nlbmdlcn0gZnJvbSBcIi4uL21vZGVsL01lc3NlbmdlclwiO1xuaW1wb3J0IHtFdmVudEhhbmRsZXJ9IGZyb20gXCIuLi8uLi9FdmVudEhhbmRsZXJcIjtcbmltcG9ydCB7RGVsZXRlUHJvcGVydHl9IGZyb20gXCIuLi9hY3Rpb25zL0RlbGV0ZVByb3BlcnR5XCI7XG5pbXBvcnQge0V2ZW50U3RhY2t9IGZyb20gXCIuLi8uLi9FdmVudFN0YWNrXCI7XG5pbXBvcnQge0Jhc2VQcm9wcywgQmFzZVN0YXRlLCBJRXZlbnR9IGZyb20gXCIuLi8uLi9ldmVudC9JRXZlbnRcIjtcbmltcG9ydCB7QWJzdHJhY3RFdmVudH0gZnJvbSBcIi4uLy4uL2V2ZW50L0Fic3RyYWN0RXZlbnRcIjtcblxuZXhwb3J0IGNsYXNzIFByb3BlcnR5UGFuZWwgZXh0ZW5kcyBBYnN0cmFjdFdpZGdldCBpbXBsZW1lbnRzIElXaWRnZXR7XG5cbiAgICBzZWxlY3RvciA6IHN0cmluZyA9ICcjZWRpdF9wcm9wZXJ0eV9wYW5lbCc7XG4gICAgdGVtcGxhdGUgOiBzdHJpbmc7XG4gICAgZm9ybSA6IEVkaXRQcm9wZXJ0eUZvcm07XG5cbiAgICBjb25maXJtX2RlbGV0ZV9idXR0b246QnV0dG9uO1xuICAgIGRlbGV0ZV9idXR0b246QnV0dG9uO1xuICAgIGNyZWF0ZV9idXR0b246QnV0dG9uO1xuXG4gICAgcHJpdmF0ZSBlZGl0b3IgOiBFZGl0b3I7XG4gICAgY29uc3RydWN0b3IoZWRpdG9yIDogRWRpdG9yKVxuICAgIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgdGhpcy5lZGl0b3IgPSBlZGl0b3I7XG4gICAgICAgIHRoaXMudHlwZSA9IFR5cGVUeXBlLnBhbmVsO1xuICAgICAgICB0aGlzLnN1YmplY3QgPSBTdWJqZWN0VHlwZS5wcm9wZXJ0eTtcbiAgICAgICAgdGhpcy5mb3JtID0gbmV3IEVkaXRQcm9wZXJ0eUZvcm0oKTtcbiAgICAgICAgVGVtcGxhdGUuZ2V0Rm9ybSh0aGlzKTtcbiAgICB9XG4gICAgYmluZEV2ZW50cygpOnRoaXNcbiAgICB7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgICBzaG93KCk6IHRoaXNcbiAgICB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdQcm9wZXJ0eVBhbmVsLnNob3coKScpO1xuXG4gICAgICAgIHN1cGVyLnNob3coKTtcbiAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgICAgIHRoaXMuZm9ybSA9IG5ldyBFZGl0UHJvcGVydHlGb3JtKCk7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5UGFuZWwuc2hvdygpIC0+IGJpbmQtY2xpY2stY29uZmlybS1kZWxldGUnKTtcbiAgICAgICAgdGhpcy5jb25maXJtX2RlbGV0ZV9idXR0b24gPSBuZXcgQnV0dG9uKCcjZmxkX2NvbmZpcm1fZGVsZXRlX2J1dHRvbicsIHRoaXMuY29uZmlybURlbGV0ZSwgdGhpcyk7XG5cbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5UGFuZWwuc2hvdygpIC0+IGJpbmQtY2xpY2stZGVsZXRlJyk7XG4gICAgICAgIHRoaXMuZGVsZXRlX2J1dHRvbiA9IG5ldyBCdXR0b24oJyNmbGRfZGVsZXRlX3Byb3BlcnR5JywgdGhpcy5kZWxldGUsIHRoaXMpO1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdQcm9wZXJ0eVBhbmVsLnNob3coKSAtPiBiaW5kLWNsaWNrLXNhdmUnKTtcbiAgICAgICAgdGhpcy5jcmVhdGVfYnV0dG9uID0gbmV3IEJ1dHRvbignI2ZsZF9zYXZlX3Byb3BlcnR5JywgdGhpcy5zYXZlLCB0aGlzKTtcblxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgc2V0RGF0YShkYXRhOiAgSU1lc3NhZ2VDb250ZW50R2VuZXJpY3MpOiB0aGlzIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5UGFuZWwuc2V0RGF0YScsIGRhdGEpO1xuXG4gICAgICAgIGlmKHR5cGVvZiBkYXRhLnRhYmxlX25hbWUgIT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsICd0aGlzLmZvcm0udGFibGVfbmFtZSA9ICcgKyBkYXRhLnRhYmxlX25hbWUpO1xuICAgICAgICAgICAgdGhpcy5mb3JtLnRhYmxlX25hbWUgPSBkYXRhLnRhYmxlX25hbWU7XG4gICAgICAgIH1cbiAgICAgICAgc3VwZXIuc2V0RGF0YShkYXRhKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIHByaXZhdGUgc2F2ZSA9IChldmVudCA6IEpRdWVyeS5DbGlja0V2ZW50LCBzZWxmQ2xhc3MgOiBBYnN0cmFjdFdpZGdldCkgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZygnUHJvcGVydHlQYW5lbC5zYXZlKCknKTtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgY29uc29sZS5sb2coXCJVc2UgZXJyb3JfYmxvY2tcIik7XG4gICAgICAgIGNvbnNvbGUubG9nKCd0aGlzJywgdGhpcyk7XG4gICAgICAgIGxldCBlcnJvcl9ibG9jayA6IEVycm9yQmxvY2sgICA9IG5ldyBFcnJvckJsb2NrKHRoaXMpO1xuICAgICAgICBsZXQgb0RhdGEgPSB7XG4gICAgICAgICAgICAnX2RvJzogJ0NyZWF0ZVByb3BlcnR5JyxcbiAgICAgICAgICAgICdkYXRhJzogc2VsZkNsYXNzLmZvcm0uZ2V0RGF0YSgpXG4gICAgICAgIH07XG4gICAgICAgICQucG9zdCh3aW5kb3cubG9jYXRpb24uaHJlZiwgb0RhdGEsIChkYXRhKSA9PiB7XG5cbiAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycylcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBlcnJvcl9ibG9jay5zaG93KGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIEVkaXRvci5vblByb3BlcnR5Q3JlYXRlZCh7Y29udGVudCA6IGRhdGEsIHRvcGljIDogJ3Byb3BlcnR5J30pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0sICdqc29uJyk7XG5cbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBjb25maXJtRGVsZXRlID0gKGV2ZW50IDogSlF1ZXJ5LkNsaWNrRXZlbnQsIHByb3BlcnR5UGFuZWwgOiBQcm9wZXJ0eVBhbmVsKSA9PiB7XG5cbiAgICAgICAgY29uc3QgZGVsZXRlRXZlbnRzICA9IG5ldyBFdmVudFN0YWNrPElFdmVudDxCYXNlUHJvcHMsIEJhc2VTdGF0ZT4sIEFic3RyYWN0RXZlbnQ8QmFzZVByb3BzLCBCYXNlU3RhdGU+PigpO1xuXG4gICAgICAgIGRlbGV0ZUV2ZW50cy5yZWdpc3RlcihuZXcgRGVsZXRlUHJvcGVydHkoKSwge2VuZHBvaW50IDogXCIvXCJ9LCB7ZGF0YSA6IHByb3BlcnR5UGFuZWwuZm9ybS5nZXREYXRhKCl9KTtcbi8qXG4gICAgICAgIGNvbnN0IHN0YXR1c1N0YXRlICA6IFN0YXR1c1N0YXRlID0ge1xuICAgICAgICAgICAgdGl0bGUgOiBcIkFyZSB5b3Ugc3VyZT9cIixcbiAgICAgICAgICAgIG1lc3NhZ2UgOiBcIlRoaXMgd2lsbCBkZWxldGUgdGhlIGNvbHVtbiBhbmQgYWxsIHRoZSBkYXRhIGluIGl0LCBhcmUgeW91IHN1cmU/XCIsXG4gICAgICAgICAgICBlbGVtZW50SWQgOiBcImNvbmZpcm1fc3VyZVwiLFxuICAgICAgICAgICAgU3RhdHVzTWVzc2FnZUNvbG9yOiBTdGF0dXNNZXNzYWdlQ29sb3JzLndhcm5pbmcsXG4gICAgICAgICAgICBidXR0b25zIDogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgbGFiZWwgOiBcIlllc1wiLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZSA6IFwiWWVzIGkgYW0gc3VyZVwiLFxuICAgICAgICAgICAgICAgICAgICB0eXBlIDogU3RhdHVzTWVzc2FnZUNvbG9ycy53YXJuaW5nLFxuICAgICAgICAgICAgICAgICAgICBhY3Rpb24gOiBkZWxldGVFdmVudHNcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgKi9cblxuICAgICAgICAvLyBFdmVudEhhbmRsZXIudHJpZ2dlcihuZXcgU3RhdHVzTWVzc2FnZSgpLCB7ZW5kcG9pbnQgOiAnLyd9LCBzdGF0dXNTdGF0ZSk7XG5cbiAgICAgICAgLy8gU3RhdHVzTWVzc2FnZS53YXJuaW5nKFwiU3VyZT9cIiwgXCJBcmUgeW91IHN1cmU/XCIsIFwic3VyZV9kZWxldGVfcHJvcGVydHlcIik7XG4gICAgICAgIGNvbnNvbGUubG9nKCdQcm9wZXJ0eVBhbmVsLmRlbGV0ZSgpJyk7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgQWpheCgnRGVsZXRlUHJvcGVydHknLCB0aGlzLCBFZGl0b3Iub25Qcm9wZXJ0eURlbGV0ZWQpO1xuICAgICAgICBNZXNzZW5nZXIuc2VuZCgncHJvcGVydHlfZGVsZXRlZCcsIHRoaXMuZm9ybS5nZXREYXRhKCkpO1xuICAgIH07XG4gICAgcHJpdmF0ZSBkZWxldGUgPSAoZXZlbnQgOiBKUXVlcnkuQ2xpY2tFdmVudCwgcHJvcGVydHlQYW5lbCA6IFByb3BlcnR5UGFuZWwpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5UGFuZWwuZGVsZXRlKCknKTtcbiAgICAgICAgY29uc29sZS5sb2coXCJcXHRcIiwgXCJwcm9wZXJ0eVBhbmVsXCIsIHByb3BlcnR5UGFuZWwpO1xuICAgICAgICBjb25zb2xlLmxvZyhcIlxcdFwiLCBcInByb3BlcnR5UGFuZWwuZm9ybVwiLCBwcm9wZXJ0eVBhbmVsLmZvcm0pO1xuICAgICAgICBjb25zb2xlLmxvZyhcIlxcdFwiLCBcInByb3BlcnR5UGFuZWwuZm9ybS5maWVsZHNcIiwgcHJvcGVydHlQYW5lbC5mb3JtLmZpZWxkcyk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsIFwicHJvcGVydHlQYW5lbC5mb3JtLnN5bmNEYXRhKClcIiwgcHJvcGVydHlQYW5lbC5mb3JtLmdldERhdGEoKSk7XG5cbiAgICAgICAgRXZlbnRIYW5kbGVyLnRyaWdnZXIobmV3IERlbGV0ZVByb3BlcnR5KCksIHtlbmRwb2ludCA6IFwiXCJ9LCB7ZGF0YSA6IHByb3BlcnR5UGFuZWwuZm9ybS5nZXREYXRhKCksIHdpZGdldCAgOiBwcm9wZXJ0eVBhbmVsfSk7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfTtcblxuXG59XG4iLCIvLy8gPHJlZmVyZW5jZSBwYXRoID1cIi4uLy4uL25vZGVfbW9kdWxlcy9AdHlwZXMvanF1ZXJ5L2luZGV4LmQudHNcIi8+XG5cbmltcG9ydCB7SVdpZGdldH0gZnJvbSBcIi4uL2NvbnRyYWN0cy9JV2lkZ2V0XCI7XG5pbXBvcnQge0Vycm9yQmxvY2t9IGZyb20gXCIuLi9tb2RlbC9FcnJvckJsb2NrXCI7XG5pbXBvcnQge05ld1Byb3BlcnR5Rm9ybX0gZnJvbSBcIi4uL21vZGVsL1Byb3BlcnR5Rm9ybVwiO1xuaW1wb3J0IHtBYnN0cmFjdFdpZGdldH0gZnJvbSBcIi4uL2NvbnRyYWN0cy9BYnN0cmFjdFdpZGdldFwiO1xuaW1wb3J0IHtFZGl0b3J9IGZyb20gXCIuL0VkaXRvclwiO1xuaW1wb3J0IHtTdWJqZWN0VHlwZSwgVGVtcGxhdGUsIFR5cGVUeXBlfSBmcm9tIFwiLi4vbW9kZWwvVGVtcGxhdGVcIjtcbmltcG9ydCB7SU1lc3NhZ2VDb250ZW50R2VuZXJpY3N9IGZyb20gXCIuLi9tb2RlbC9NZXNzZW5nZXJcIjtcbmltcG9ydCB7QnV0dG9ufSBmcm9tIFwiLi4vbW9kZWwvQnV0dG9uXCI7XG5cbmV4cG9ydCBjbGFzcyBQcm9wZXJ0eVBvcHVwIGV4dGVuZHMgQWJzdHJhY3RXaWRnZXQgaW1wbGVtZW50cyBJV2lkZ2V0e1xuXG4gICAgc2VsZWN0b3IgOnN0cmluZyA9ICcjYWRkX3Byb3BlcnR5X3BvcHVwJztcbiAgICBwdWJsaWMgY3JlYXRlX2J1dHRvbjpCdXR0b247XG4gICAgdGVtcGxhdGUgOiBzdHJpbmc7XG5cbiAgICBmb3JtIDogTmV3UHJvcGVydHlGb3JtO1xuICAgIHByaXZhdGUgZWRpdG9yIDogRWRpdG9yO1xuICAgIGNvbnN0cnVjdG9yKGVkaXRvciA6IEVkaXRvcikge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICBjb25zb2xlLmxvZygnUHJvcGVydHlQb3B1cC5jb25zdHJ1Y3RvcicpO1xuICAgICAgICB0aGlzLmVkaXRvciA9IGVkaXRvcjtcbiAgICAgICAgdGhpcy50eXBlID0gVHlwZVR5cGUucG9wdXA7XG4gICAgICAgIHRoaXMuc3ViamVjdCA9IFN1YmplY3RUeXBlLnByb3BlcnR5O1xuICAgICAgICBUZW1wbGF0ZS5nZXRGb3JtKHRoaXMpO1xuICAgIH1cbiAgICBiaW5kRXZlbnRzKCk6dGhpcyB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdiaW5kICNmbGRfY3JlYXRlX3Byb3BlcnR5Jyk7XG4gICAgICAgIHRoaXMuY3JlYXRlX2J1dHRvbiA9IG5ldyBCdXR0b24oJyNmbGRfY3JlYXRlX3Byb3BlcnR5JywgdGhpcy5jcmVhdGUsIHRoaXMpO1xuICAgICAgICB0aGlzLmZrVG9nZ2xlcigpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgZmtUb2dnbGVyKCkgOiB0aGlzIHtcbiAgICAgICAgY29uc3Qgb01vZGVsQmxvY2sgPSAkKCcjYmxrX2ZrX21vZGVsJyk7XG4gICAgICAgIGNvbnN0IG9Nb2RlbEZpZWxkID0gJCgnI2ZsZF9ma19tb2RlbCcpO1xuICAgICAgICBjb25zdCBvTW9kZWxGaWVsZEZpZWxkID0gJCgnI2ZsZF9ma19tb2RlbF9maWVsZCcpO1xuXG4gICAgICAgIGNvbnN0IG9Nb2RlbEZpZWxkQmxvY2sgPSAkKCcjYmxrX2ZrX21vZGVsX2ZpZWxkJyk7XG4gICAgICAgIGNvbnN0IG9Gb3JtRmllbGRUeXBlID0gJCgnI2ZsZF9mb3JtX2ZpZWxkX3R5cGVfaWQnKTtcblxuICAgICAgICBjb25zdCBsb2FkTW9kZWxGaWVsZHMgPSAoKSA9PlxuICAgICAgICB7XG4gICAgICAgICAgICBpZihvTW9kZWxGaWVsZC52YWwoKSlcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjb25zdCBvRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgX2RvIDogJ0dldE1vZGVsRmllbGRzJyxcbiAgICAgICAgICAgICAgICAgICAgbW9kZWxfaWQgOiBvTW9kZWxGaWVsZC52YWwoKVxuICAgICAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgICAgICAkLmdldCh3aW5kb3cubG9jYXRpb24uaHJlZiwgb0RhdGEsIChkYXRhIDoge2lkIDogbnVtYmVyLCBsYWJlbCA6IHN0cmluZ31bXSkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkYXRhJywgZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIG9Nb2RlbEZpZWxkRmllbGQuaHRtbCgnJyk7XG5cblxuICAgICAgICAgICAgICAgICAgICBkYXRhLmZvckVhY2goKG9wdGlvbikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgb01vZGVsRmllbGRGaWVsZC5hcHBlbmQoJzxvcHRpb24+JyArIG9wdGlvbi5sYWJlbCArICc8L29wdGlvbj4nKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCd2YWx1ZScsIG9wdGlvbi5pZCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgIH0sICdqc29uJykudGhlbigoKSA9PiB7XG5cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJNb2RlbCBmaWVsZHMgbG9hZGVkXCIpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIG9Nb2RlbEZpZWxkQmxvY2suY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgb01vZGVsRmllbGRCbG9jay5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIG9Nb2RlbEZpZWxkLm9uKCdjaGFuZ2UnLCAoKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygnY2hhYWFhYWFhYWFhYScpO1xuICAgICAgICAgICAgbG9hZE1vZGVsRmllbGRzKCk7XG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCB0b2dnbGVCbG9ja3MgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBpc0ZrIDogYm9vbGVhbiA9IChvRm9ybUZpZWxkVHlwZS5maW5kKCc6c2VsZWN0ZWQnKS5kYXRhKCdpcy1mb3JlaWduLWtleScpID09PSAxKTtcbiAgICAgICAgICAgIG9Nb2RlbEJsb2NrLmNzcygnZGlzcGxheScsIGlzRmsgPyAnYmxvY2snIDogJ25vbmUnKTtcbiAgICAgICAgICAgIGxvYWRNb2RlbEZpZWxkcygpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGNvbnNvbGUubG9nKCdQcm9wZXJ0eVBvcHVwLmZrVG9nZ2xlJyk7XG5cblxuICAgICAgICB0b2dnbGVCbG9ja3MoKTtcblxuICAgICAgICBvRm9ybUZpZWxkVHlwZS5vbignY2hhbmdlJywgKCkgPT4ge1xuICAgICAgICAgICAgdG9nZ2xlQmxvY2tzKCk7XG4gICAgICAgIH0pO1xuXG5cblxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgc2V0RGF0YShkYXRhOiAgSU1lc3NhZ2VDb250ZW50R2VuZXJpY3MpOiB0aGlzIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1Byb3BlcnR5UG9wdXAuc2V0RGF0YScsIGRhdGEpO1xuXG4gICAgICAgIGlmKHR5cGVvZiBkYXRhLnRhYmxlX25hbWUgIT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgIHtcbiAgICAgICAgICAgIHRoaXMuZm9ybS50YWJsZV9uYW1lID0gZGF0YS50YWJsZV9uYW1lO1xuICAgICAgICB9XG4gICAgICAgIHN1cGVyLnNldERhdGEoZGF0YSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgICBzaG93KCk6IHRoaXMge1xuICAgICAgICBjb25zb2xlLmxvZygnUHJvcGVydHlQb3B1cC5zaG93Jyk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsICd0ZW1wbGF0ZScsIHRoaXMudGVtcGxhdGUpO1xuXG4gICAgICAgIHRoaXMuZ2V0UGxhY2VIb2xkZXIoKS5yZXBsYWNlV2l0aCh0aGlzLnRlbXBsYXRlKTtcbiAgICAgICAgdGhpcy5iaW5kRXZlbnRzKCk7XG4gICAgICAgIHRoaXMuZm9ybSA9IG5ldyBOZXdQcm9wZXJ0eUZvcm0oKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIGhpZGUoaW1tZWRpYXRlOiBib29sZWFuID0gZmFsc2UpOiB0aGlzIHtcbiAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICBpZih0eXBlb2Ygd2luZG93WydjbG9zZVBvcHVwJ10gPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAge1xuICAgICAgICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgICAgICAgd2luZG93WydjbG9zZVBvcHVwJ10oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZ2V0UGFuZWwoKS5yZXBsYWNlV2l0aCh0aGlzLmNyZWF0ZVBsYWNlaG9sZGVyKCkpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBwcml2YXRlIGNyZWF0ZSA9IChldmVudCA6IEpRdWVyeS5DbGlja0V2ZW50LCBzZWxmQ2xhc3MgOiBQcm9wZXJ0eVBvcHVwKSA9PiB7XG5cbiAgICAgICAgY29uc29sZS5sb2coXCJQcm9wZXJ0eVBvcHVwLnNhdmVcIik7XG5cbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgY29uc29sZS5sb2coXCJcXHRcIiwgXCJVc2UgZXJyb3JfYmxvY2tcIik7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsICd0aGlzJywgdGhpcyk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiXFx0XCIsICdmb3JtIGRhdGEnLCBzZWxmQ2xhc3MuZm9ybS5nZXREYXRhKCkpO1xuXG5cbiAgICAgICAgbGV0IGVycm9yX2Jsb2NrIDogRXJyb3JCbG9jayAgID0gbmV3IEVycm9yQmxvY2sodGhpcyk7XG4gICAgICAgIGxldCBvRGF0YSA9IHtcbiAgICAgICAgICAgICdfZG8nOiAnQ3JlYXRlUHJvcGVydHknLFxuICAgICAgICAgICAgJ2RhdGEnOiBzZWxmQ2xhc3MuZm9ybS5nZXREYXRhKClcbiAgICAgICAgfTtcbiAgICAgICAgJC5wb3N0KHdpbmRvdy5sb2NhdGlvbi5ocmVmLCBvRGF0YSwgKGRhdGEpID0+IHtcblxuICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzKVxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGVycm9yX2Jsb2NrLnNob3coZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgRWRpdG9yLm9uUHJvcGVydHlDcmVhdGVkKHtjb250ZW50IDogZGF0YSwgdG9waWMgOiAncHJvcGVydHknfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSwgJ2pzb24nKTtcblxuICAgIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiIn0=