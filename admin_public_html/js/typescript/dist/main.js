var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("event/IEvent", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var BaseProps = (function () {
        function BaseProps() {
        }
        return BaseProps;
    }());
    exports.BaseProps = BaseProps;
    var BaseState = (function () {
        function BaseState() {
        }
        return BaseState;
    }());
    exports.BaseState = BaseState;
});
define("event/http", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var httpMethod;
    (function (httpMethod) {
        httpMethod["GET"] = "GET";
        httpMethod["POST"] = "POST";
        httpMethod["PUT"] = "PUT";
        httpMethod["DELETE"] = "DELETE";
        httpMethod["PATCH"] = "PATCH";
    })(httpMethod = exports.httpMethod || (exports.httpMethod = {}));
    function httpCall(sUrl, eMethod, data, done, fail) {
        var deleteRequest = $.ajax({
            url: sUrl,
            method: eMethod,
            contentType: "application/json",
            data: data,
            dataType: "json"
        });
        deleteRequest.done(function (msg) {
            console.log('resolve http call');
            console.log(msg.errors);
            if (msg.errors) {
                fail(msg);
                return;
            }
            done(msg);
        });
        deleteRequest.fail(function (jqXHR, textStatus) {
            console.log('fail http call');
            console.log(jqXHR);
            fail(textStatus);
        });
    }
    exports.httpCall = httpCall;
});
define("EventStack", ["require", "exports", "EventHandler"], function (require, exports, EventHandler_1) {
    "use strict";
    exports.__esModule = true;
    var EventStack = (function () {
        function EventStack() {
            var _this = this;
            this.execute = function () {
                _this.stack.forEach(function (event) {
                    EventHandler_1.EventHandler.trigger(event.handler, event.props, event.state);
                });
            };
        }
        EventStack.prototype.register = function (event, props, state) {
            this.stack.push({ handler: event, props: props, state: state });
        };
        return EventStack;
    }());
    exports.EventStack = EventStack;
});
define("event/types/StatusMessage", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var StatusMessageColors;
    (function (StatusMessageColors) {
        StatusMessageColors["info"] = "info";
        StatusMessageColors["success"] = "success";
        StatusMessageColors["alert"] = "alert";
        StatusMessageColors["warning"] = "warning";
        StatusMessageColors["danger"] = "danger";
    })(StatusMessageColors = exports.StatusMessageColors || (exports.StatusMessageColors = {}));
    var StatusState = (function () {
        function StatusState() {
        }
        return StatusState;
    }());
    exports.StatusState = StatusState;
    var StatusValidationState = (function () {
        function StatusValidationState() {
        }
        return StatusValidationState;
    }());
    exports.StatusValidationState = StatusValidationState;
});
define("event/AbstractEvent", ["require", "exports", "event/types/StatusMessage"], function (require, exports, StatusMessage_1) {
    "use strict";
    exports.__esModule = true;
    var AbstractEvent = (function () {
        function AbstractEvent() {
        }
        AbstractEvent.prototype.getPropType = function () {
            return this.propType;
        };
        AbstractEvent.prototype.getStateType = function () {
            return this.stateType;
        };
        AbstractEvent.prototype.getRejectHandler = function (reject, title, message) {
            var _this = this;
            return function (textStatus) { return __awaiter(_this, void 0, void 0, function () {
                var errors, oState, oState;
                return __generator(this, function (_a) {
                    console.log(textStatus);
                    errors = null;
                    if (textStatus.errors) {
                        errors = textStatus.errors;
                    }
                    if (errors) {
                        oState = {
                            title: title,
                            errors: errors,
                            elementId: "error_dialog",
                            StatusMessageColor: StatusMessage_1.StatusMessageColors.warning
                        };
                        reject("Validation errors occured");
                    }
                    else {
                        oState = {
                            title: title,
                            message: message.replace('{textStatus}', textStatus),
                            elementId: "error_dialog",
                            StatusMessageColor: StatusMessage_1.StatusMessageColors.warning
                        };
                    }
                    return [2];
                });
            }); };
        };
        return AbstractEvent;
    }());
    exports.AbstractEvent = AbstractEvent;
});
define("EventHandler", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var EventHandler = (function () {
        function EventHandler() {
        }
        EventHandler.trigger = function (event, props, state) {
            return new Promise(function (resolve, reject) {
                console.log('EventHandler.trigger');
                event.trigger(props, state).then(resolve())["catch"](reject());
            });
        };
        return EventHandler;
    }());
    exports.EventHandler = EventHandler;
});
define("schema-editor/model/Model", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var GenericModel = (function () {
        function GenericModel() {
        }
        return GenericModel;
    }());
    exports.GenericModel = GenericModel;
});
define("schema-editor/contracts/Form", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var FieldValues = (function () {
        function FieldValues() {
        }
        return FieldValues;
    }());
    exports.FieldValues = FieldValues;
    var AbstractForm = (function () {
        function AbstractForm(fields) {
            this.fields = fields;
        }
        AbstractForm.prototype.getFieldNames = function () {
            return Object.keys(this.fields);
        };
        AbstractForm.prototype.getFields = function () {
            console.log('AbstractForm.getFields()');
            var aOut = [];
            for (var sField in this.getFieldNames()) {
                aOut.push(this.fields[sField]);
            }
            return aOut;
        };
        AbstractForm.prototype.getFieldManager = function () {
            return this.fields;
        };
        AbstractForm.prototype.syncData = function (model) {
            var _this = this;
            var aModel = model;
            this.getFieldNames().forEach(function (fieldName) {
                if (_this.fields.hasOwnProperty(fieldName) && typeof (_this.fields[fieldName].val()) != 'undefined') {
                    aModel[fieldName] = _this.fields[fieldName].val().toString();
                }
            });
            return aModel;
        };
        return AbstractForm;
    }());
    exports.AbstractForm = AbstractForm;
});
define("schema-editor/model/Template", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var TypeType;
    (function (TypeType) {
        TypeType["popup"] = "popup";
        TypeType["panel"] = "panel";
    })(TypeType = exports.TypeType || (exports.TypeType = {}));
    var SubjectType;
    (function (SubjectType) {
        SubjectType["model"] = "model";
        SubjectType["property"] = "property";
    })(SubjectType = exports.SubjectType || (exports.SubjectType = {}));
    var Template = (function () {
        function Template() {
        }
        Template.getForm = function (widget) {
            var oData = {
                _do: 'GetForm',
                type: widget.type,
                subject: widget.subject
            };
            $.get('/modeller/schema/show', oData, function (data) {
                widget.template = data.form;
            }, 'json').then(function () {
                console.log("Template loaded " + widget.type + ' ' + widget.subject);
            });
        };
        return Template;
    }());
    exports.Template = Template;
});
define("schema-editor/model/Messenger", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Message = (function () {
        function Message() {
        }
        return Message;
    }());
    exports.Message = Message;
    var MessageHandler = (function () {
        function MessageHandler(topic, callback) {
            this.topic = topic;
            this.callback = callback;
        }
        MessageHandler.prototype.handle = function (message) {
            this.callback(message);
        };
        return MessageHandler;
    }());
    var MessageDispenser = (function () {
        function MessageDispenser() {
        }
        MessageDispenser.addListener = function (handler) {
            MessageDispenser.aHandlers.push(handler);
        };
        MessageDispenser.distribute = function (rawMessage) {
            var message;
            for (var i = 0; i < MessageDispenser.aHandlers.length; i++) {
                var handler = MessageDispenser.aHandlers[i];
                message = new Message();
                message.topic = rawMessage.topic;
                message.content = rawMessage.data;
                if (handler.topic == message.topic) {
                    handler.handle(message);
                }
            }
        };
        MessageDispenser.aHandlers = [];
        return MessageDispenser;
    }());
    var Messenger = (function () {
        function Messenger() {
        }
        Messenger.startListener = function () {
            Messenger.isListening = true;
            console.log('Start listening');
            var callback = function (messageEvent) {
                if (typeof messageEvent.data !== "undefined") {
                    try {
                        console.log('type', typeof messageEvent.data);
                        if (typeof messageEvent.data == 'string') {
                            var rawMessage = JSON.parse(messageEvent.data);
                            MessageDispenser.distribute(rawMessage);
                        }
                    }
                    catch (e) {
                        console.error("Could not parse message" + messageEvent.data);
                        console.error(e.toString());
                    }
                }
            };
            console.log('Adding EventListener');
            window.addEventListener('message', callback);
        };
        Messenger.send = function (topic, content) {
            console.log('Messenger.send(topic, content)', topic, content);
            var oMessage = {
                topic: topic,
                content: content
            };
            var oErdEditor = document.getElementById('erd_editor_window');
            oErdEditor.contentWindow.postMessage(JSON.stringify(oMessage), '*');
        };
        Messenger.onReceive = function (topic, handler) {
            if (!Messenger.isListening) {
                console.log('Startig EventListener');
                Messenger.startListener();
                console.log('EventListener started');
            }
            console.log('Add event listener for: ' + topic);
            var messageHandler = new MessageHandler(topic, handler);
            MessageDispenser.addListener(messageHandler);
        };
        return Messenger;
    }());
    exports.Messenger = Messenger;
});
define("schema-editor/contracts/IWidget", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("schema-editor/model/ErrorResponse", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
});
define("schema-editor/model/ErrorBlock", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var ErrorBlock = (function () {
        function ErrorBlock(oModel) {
            this.model = oModel;
            this.element = $('.error_block');
            this.error_ul = $('.error_ul');
        }
        ErrorBlock.prototype.clear = function () {
            this.error_ul.html('');
        };
        ErrorBlock.prototype.hide = function () {
            this.clear();
            this.element.fadeOut(200);
        };
        ErrorBlock.prototype.show = function (data) {
            var _this = this;
            console.log('ErrorBlock.show', data.errors);
            var aKeys = this.model.form.getFieldNames();
            var aErrorList = [];
            aKeys.forEach(function (field_name) {
                if (typeof (data.errors[field_name]) != 'undefined') {
                    _this.model.form.getFieldManager()[field_name].addClass('error');
                    var element = document.createElement('li');
                    element.innerHTML = data.errors[field_name];
                    console.log('ErrorBlock.show', 'element.toString()', element.toString());
                    aErrorList.push(element.outerHTML);
                }
            });
            console.log('ErrorBlock.show', aErrorList);
            this.error_ul.html(aErrorList.join("\n"));
            this.element.fadeIn(200);
            $('.error').on('focus', function (e) {
                $(_this).removeClass('error');
            });
        };
        return ErrorBlock;
    }());
    exports.ErrorBlock = ErrorBlock;
});
define("schema-editor/model/ModelForm", ["require", "exports", "schema-editor/contracts/Form", "schema-editor/model/Model"], function (require, exports, Form_1, Model_1) {
    "use strict";
    exports.__esModule = true;
    var ModelModel = (function (_super) {
        __extends(ModelModel, _super);
        function ModelModel() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.id = null;
            return _this;
        }
        return ModelModel;
    }(Model_1.GenericModel));
    exports.ModelModel = ModelModel;
    var ModelFormFields = (function (_super) {
        __extends(ModelFormFields, _super);
        function ModelFormFields() {
            var _this = _super.call(this) || this;
            _this.name = $('#fld_name');
            _this.title = $('#fld_title');
            _this.module_id = $('#fld_module_id');
            _this.create_new_module = $('#fld_create_new_module');
            _this.new_module_name = $('#fld_new_module');
            return _this;
        }
        return ModelFormFields;
    }(ModelModel));
    exports.ModelFormFields = ModelFormFields;
    var EditModelFormFields = (function (_super) {
        __extends(EditModelFormFields, _super);
        function EditModelFormFields() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditModelFormFields;
    }(ModelFormFields));
    exports.EditModelFormFields = EditModelFormFields;
    var ModelValues = (function (_super) {
        __extends(ModelValues, _super);
        function ModelValues() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return ModelValues;
    }(ModelModel));
    var NewModelForm = (function (_super) {
        __extends(NewModelForm, _super);
        function NewModelForm() {
            var _this = _super.call(this, new ModelFormFields()) || this;
            _this.fields = new ModelFormFields();
            _this.fields.name.on('keyup', _this.modelNameFilter);
            _this.fields.module_id.on('change', _this.toggleCreateNewModule);
            return _this;
        }
        NewModelForm.prototype.getData = function () {
            return undefined;
        };
        NewModelForm.prototype.toggleCreateNewModule = function () {
            var bCreateNewModule = ($('option:selected', this.fields.module_id).data('action') === '_create_new_') ? 1 : 0;
            var new_module_field_block = $('#new_module_field_block');
            new_module_field_block.css({
                'display': bCreateNewModule ? 'block' : 'none'
            });
            this.fields.create_new_module.val(bCreateNewModule);
            return false;
        };
        NewModelForm.prototype.modelNameFilter = function (event) {
            event.preventDefault();
            var newVal;
            newVal = this.fields.name.val()
                .toString()
                .toLowerCase()
                .replace(/^[^a-z]/, '')
                .replace(/^[^a-z0-9_]+$/, '');
            this.fields.name.val(newVal);
            return false;
        };
        return NewModelForm;
    }(Form_1.AbstractForm));
    exports.NewModelForm = NewModelForm;
    var EditModelForm = (function (_super) {
        __extends(EditModelForm, _super);
        function EditModelForm() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditModelForm;
    }(NewModelForm));
    exports.EditModelForm = EditModelForm;
});
define("schema-editor/model/PropertyForm", ["require", "exports", "schema-editor/contracts/Form", "schema-editor/model/Model"], function (require, exports, Form_2, Model_2) {
    "use strict";
    exports.__esModule = true;
    var PropertyModel = (function (_super) {
        __extends(PropertyModel, _super);
        function PropertyModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PropertyModel;
    }(Model_2.GenericModel));
    exports.PropertyModel = PropertyModel;
    var PropertyValues = (function (_super) {
        __extends(PropertyValues, _super);
        function PropertyValues() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return PropertyValues;
    }(PropertyModel));
    exports.PropertyValues = PropertyValues;
    var PropertyFormFields = (function (_super) {
        __extends(PropertyFormFields, _super);
        function PropertyFormFields() {
            var _this = _super.call(this) || this;
            _this.label = $('#fld_label');
            _this.name = $('#fld_name');
            _this.form_field_type_id = $('#fld_form_field_type_id');
            _this.id = $('#fld_id');
            _this.data_type_id = $('#fld_data_type_id');
            _this.form_field_lookups = $('#fld_form_field_lookups');
            _this.required = $('#fld_required');
            _this.is_unique = $('#fld_is_unique');
            _this.is_primary_key = $('#fld_is_primary_key');
            _this.icon = $('#fld_icon');
            _this.fk_model_field = $('#fld_fk_model_field');
            _this.fk_model = $('#fld_fk_model');
            return _this;
        }
        return PropertyFormFields;
    }(PropertyModel));
    exports.PropertyFormFields = PropertyFormFields;
    var EditPropertyFormFields = (function (_super) {
        __extends(EditPropertyFormFields, _super);
        function EditPropertyFormFields() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditPropertyFormFields;
    }(PropertyFormFields));
    exports.EditPropertyFormFields = EditPropertyFormFields;
    var NewPropertyForm = (function (_super) {
        __extends(NewPropertyForm, _super);
        function NewPropertyForm() {
            var _this = _super.call(this, new PropertyFormFields()) || this;
            console.log('NewPropertyForm.constructor');
            _this.fields = new PropertyFormFields();
            return _this;
        }
        NewPropertyForm.prototype.getFieldManager = function () {
            console.log('NewPropertyForm.getFieldManager');
            return new PropertyFormFields();
        };
        NewPropertyForm.prototype.getData = function () {
            console.log('NewPropertyForm.syncData()');
            var aData = _super.prototype.syncData.call(this, new PropertyValues());
            console.log("\t", "fields", this.fields);
            console.log("\t", "data", aData);
            aData['table_name'] = this.table_name;
            return aData;
        };
        return NewPropertyForm;
    }(Form_2.AbstractForm));
    exports.NewPropertyForm = NewPropertyForm;
    var EditPropertyForm = (function (_super) {
        __extends(EditPropertyForm, _super);
        function EditPropertyForm() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return EditPropertyForm;
    }(NewPropertyForm));
    exports.EditPropertyForm = EditPropertyForm;
});
define("schema-editor/contracts/AbstractWidget", ["require", "exports", "schema-editor/model/PropertyForm"], function (require, exports, PropertyForm_1) {
    "use strict";
    exports.__esModule = true;
    var KeyVal = (function () {
        function KeyVal() {
        }
        return KeyVal;
    }());
    var AbstractWidget = (function () {
        function AbstractWidget() {
            this.data_keeper = [];
            this.form = new PropertyForm_1.EditPropertyForm();
        }
        AbstractWidget.prototype.getPlaceHolder = function () {
            return $('#' + this.type + '_placeholder');
        };
        AbstractWidget.prototype.createPlaceholder = function () {
            var elem = (document.createElement('div'));
            console.log('AbstractWidget.createPlaceholder', this.type + '_placeholder');
            elem.setAttribute('id', this.type + '_placeholder');
            elem.style.display = 'none';
            return elem;
        };
        AbstractWidget.prototype.setData = function (data) {
            console.log('AbstractWidget.setData', data);
            var aFieldNames = this.form.getFieldNames();
            var oFormData = this.form.getFieldManager();
            var fieldName;
            this.data_keeper = [];
            for (var i = 0; i < aFieldNames.length; i++) {
                fieldName = aFieldNames[i].toString();
                console.log("\t", fieldName, data);
                if (typeof fieldName === 'undefined') {
                    continue;
                }
                else if (typeof data === 'undefined') {
                    continue;
                }
                else if (!data.hasOwnProperty(fieldName)) {
                    continue;
                }
                this.data_keeper.push({ key: fieldName, value: data[fieldName] });
                oFormData[fieldName].val(data[fieldName]);
            }
            return this;
        };
        AbstractWidget.prototype.notifySaved = function () {
            console.log("Placeholder for save notification");
            return this;
        };
        AbstractWidget.prototype.getPanel = function () {
            console.log('AbstractWidget.getPanel()', this.selector);
            return $(this.selector);
        };
        AbstractWidget.prototype.hide = function (immediate) {
            var _this = this;
            if (immediate === void 0) { immediate = false; }
            var panel = this.getPanel();
            if (panel.length) {
                if (immediate) {
                    panel.replaceWith(this.createPlaceholder());
                }
                else {
                    panel.fadeOut(200, function () {
                        panel.replaceWith(_this.createPlaceholder());
                    });
                }
            }
            return this;
        };
        AbstractWidget.prototype.show = function () {
            console.log('AbstractWidget.show()');
            this.getPlaceHolder().replaceWith(this.template);
            this.getPanel().show();
            var aFieldNames = this.form.getFieldNames();
            var oFormFieldManager = this.form.getFieldManager();
            var currentFieldName;
            var keeperFieldName;
            var field;
            console.log("\t", aFieldNames);
            for (var i = 0; i < aFieldNames.length; i++) {
                currentFieldName = aFieldNames[i];
                for (var y = 0; y < this.data_keeper.length; y++) {
                    keeperFieldName = this.data_keeper[y].key;
                    if (currentFieldName === keeperFieldName) {
                        field = oFormFieldManager[currentFieldName];
                        if (field.is('select') && $.trim(this.data_keeper[y].value) == '') {
                            continue;
                        }
                        field.val(this.data_keeper[y].value);
                    }
                }
            }
            this.bindEvents();
            return this;
        };
        return AbstractWidget;
    }());
    exports.AbstractWidget = AbstractWidget;
});
define("schema-editor/widget/ModelPanel", ["require", "exports", "schema-editor/model/ModelForm", "schema-editor/contracts/AbstractWidget", "schema-editor/model/Template"], function (require, exports, ModelForm_1, AbstractWidget_1, Template_1) {
    "use strict";
    exports.__esModule = true;
    var ModelPanel = (function (_super) {
        __extends(ModelPanel, _super);
        function ModelPanel(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#edit_model_panel';
            _this.type = Template_1.TypeType.panel;
            _this.subject = Template_1.SubjectType.model;
            _this.editor = editor;
            _this.form = new ModelForm_1.EditModelForm();
            Template_1.Template.getForm(_this);
            return _this;
        }
        ModelPanel.prototype.bindEvents = function () {
            return this;
        };
        return ModelPanel;
    }(AbstractWidget_1.AbstractWidget));
    exports.ModelPanel = ModelPanel;
});
define("schema-editor/model/Button", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Button = (function () {
        function Button(selector, callback, thisWidget) {
            this.button = $(selector);
            this.button.on('click', function (event) {
                callback(event, thisWidget);
            });
            this.widget = thisWidget;
        }
        return Button;
    }());
    exports.Button = Button;
});
define("schema-editor/Ajax", ["require", "exports", "event/AbstractEvent"], function (require, exports, AbstractEvent_1) {
    "use strict";
    exports.__esModule = true;
    var AjaxAction;
    (function (AjaxAction) {
        AjaxAction[AjaxAction["DELETE"] = 0] = "DELETE";
        AjaxAction[AjaxAction["POST"] = 1] = "POST";
        AjaxAction[AjaxAction["PUT"] = 2] = "PUT";
        AjaxAction[AjaxAction["PATCH"] = 3] = "PATCH";
        AjaxAction[AjaxAction["GET"] = 4] = "GET";
    })(AjaxAction = exports.AjaxAction || (exports.AjaxAction = {}));
    var AjaxProps = (function () {
        function AjaxProps() {
        }
        return AjaxProps;
    }());
    exports.AjaxProps = AjaxProps;
    var AjaxState = (function () {
        function AjaxState() {
        }
        return AjaxState;
    }());
    exports.AjaxState = AjaxState;
    var Ajax = (function (_super) {
        __extends(Ajax, _super);
        function Ajax() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Ajax.prototype.trigger = function (props, state) {
            return new Promise(function (resolve, reject) {
                console.log('Ajax(action, data)', props.action, state.data);
                var oData = {
                    '_do': props.action,
                    'data': state.data
                };
                console.log("\t", window.location.href, "POST", oData);
                return $.post(window.location.href, oData, function (data) {
                    console.log("\t", "server response", data);
                    if (data.errors) {
                        reject(data.errors);
                    }
                    else {
                        resolve(data);
                    }
                }, 'json')
                    .fail(function () {
                    reject('Ajax call failed');
                });
            });
        };
        return Ajax;
    }(AbstractEvent_1.AbstractEvent));
    exports.Ajax = Ajax;
});
define("schema-editor/actions/DeleteProperty", ["require", "exports", "event/AbstractEvent", "event/IEvent", "schema-editor/Ajax", "EventHandler"], function (require, exports, AbstractEvent_2, IEvent_1, Ajax_1, EventHandler_2) {
    "use strict";
    exports.__esModule = true;
    var DeletePropertyProps = (function (_super) {
        __extends(DeletePropertyProps, _super);
        function DeletePropertyProps() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DeletePropertyProps;
    }(IEvent_1.BaseProps));
    var DeletePropertyState = (function (_super) {
        __extends(DeletePropertyState, _super);
        function DeletePropertyState() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return DeletePropertyState;
    }(IEvent_1.BaseState));
    var DeletePropertyPromiseType = (function () {
        function DeletePropertyPromiseType() {
        }
        DeletePropertyPromiseType.prototype.resolve = function () {
        };
        DeletePropertyPromiseType.prototype.onRejected = function (callback) {
        };
        DeletePropertyPromiseType.prototype.onFulfilled = function (callback) {
        };
        return DeletePropertyPromiseType;
    }());
    var DeleteProperty = (function (_super) {
        __extends(DeleteProperty, _super);
        function DeleteProperty() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        DeleteProperty.prototype.trigger = function (props, state) {
            return new Promise(function (resolve, reject) {
                EventHandler_2.EventHandler.trigger(new Ajax_1.Ajax(), { endpoint: props.endpoint, action: props.action }, { data: state.widget.form.getData() })
                    .then(function (value) {
                    resolve(value);
                })["catch"](function (value) {
                    reject(value);
                });
            });
        };
        return DeleteProperty;
    }(AbstractEvent_2.AbstractEvent));
    exports.DeleteProperty = DeleteProperty;
});
define("schema-editor/widget/PropertyPanel", ["require", "exports", "schema-editor/model/PropertyForm", "schema-editor/contracts/AbstractWidget", "schema-editor/widget/Editor", "schema-editor/model/Template", "schema-editor/model/Button", "schema-editor/Ajax", "schema-editor/model/ErrorBlock", "schema-editor/model/Messenger", "EventHandler", "schema-editor/actions/DeleteProperty", "EventStack"], function (require, exports, PropertyForm_2, AbstractWidget_2, Editor_1, Template_2, Button_1, Ajax_2, ErrorBlock_1, Messenger_1, EventHandler_3, DeleteProperty_1, EventStack_1) {
    "use strict";
    exports.__esModule = true;
    var PropertyPanel = (function (_super) {
        __extends(PropertyPanel, _super);
        function PropertyPanel(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#edit_property_panel';
            _this.save = function (event, selfClass) {
                console.log('PropertyPanel.save()');
                event.preventDefault();
                console.log("Use error_block");
                console.log('this', _this);
                var error_block = new ErrorBlock_1.ErrorBlock(_this);
                var oData = {
                    '_do': 'CreateProperty',
                    'data': selfClass.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_1.Editor.onPropertyCreated({ content: data, topic: 'property' });
                    }
                }, 'json');
            };
            _this.confirmDelete = function (event, propertyPanel) {
                var deleteEvents = new EventStack_1.EventStack();
                deleteEvents.register(new DeleteProperty_1.DeleteProperty(), { endpoint: "/" }, { data: propertyPanel.form.getData() });
                console.log('PropertyPanel.delete()');
                event.preventDefault();
                Ajax_2.Ajax('DeleteProperty', _this, Editor_1.Editor.onPropertyDeleted);
                Messenger_1.Messenger.send('property_deleted', _this.form.getData());
            };
            _this["delete"] = function (event, propertyPanel) {
                console.log('PropertyPanel.delete()');
                console.log("\t", "propertyPanel", propertyPanel);
                console.log("\t", "propertyPanel.form", propertyPanel.form);
                console.log("\t", "propertyPanel.form.fields", propertyPanel.form.fields);
                console.log("\t", "propertyPanel.form.syncData()", propertyPanel.form.getData());
                EventHandler_3.EventHandler.trigger(new DeleteProperty_1.DeleteProperty(), { endpoint: "" }, { data: propertyPanel.form.getData(), widget: propertyPanel });
                event.preventDefault();
            };
            _this.editor = editor;
            _this.type = Template_2.TypeType.panel;
            _this.subject = Template_2.SubjectType.property;
            _this.form = new PropertyForm_2.EditPropertyForm();
            Template_2.Template.getForm(_this);
            return _this;
        }
        PropertyPanel.prototype.bindEvents = function () {
            return this;
        };
        PropertyPanel.prototype.show = function () {
            console.log('PropertyPanel.show()');
            _super.prototype.show.call(this);
            this.bindEvents();
            this.form = new PropertyForm_2.EditPropertyForm();
            console.log('PropertyPanel.show() -> bind-click-confirm-delete');
            this.confirm_delete_button = new Button_1.Button('#fld_confirm_delete_button', this.confirmDelete, this);
            console.log('PropertyPanel.show() -> bind-click-delete');
            this.delete_button = new Button_1.Button('#fld_delete_property', this["delete"], this);
            console.log('PropertyPanel.show() -> bind-click-save');
            this.create_button = new Button_1.Button('#fld_save_property', this.save, this);
            return this;
        };
        PropertyPanel.prototype.setData = function (data) {
            console.log('PropertyPanel.setData', data);
            if (typeof data.table_name != 'undefined') {
                console.log("\t", 'this.form.table_name = ' + data.table_name);
                this.form.table_name = data.table_name;
            }
            _super.prototype.setData.call(this, data);
            return this;
        };
        return PropertyPanel;
    }(AbstractWidget_2.AbstractWidget));
    exports.PropertyPanel = PropertyPanel;
});
define("schema-editor/widget/ModelPopup", ["require", "exports", "schema-editor/model/ErrorBlock", "schema-editor/model/Button", "schema-editor/model/ModelForm", "schema-editor/contracts/AbstractWidget", "schema-editor/widget/Editor", "schema-editor/model/Template"], function (require, exports, ErrorBlock_2, Button_2, ModelForm_2, AbstractWidget_3, Editor_2, Template_3) {
    "use strict";
    exports.__esModule = true;
    var ModelPopup = (function (_super) {
        __extends(ModelPopup, _super);
        function ModelPopup(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#add_model_popup';
            _this.create = function (event, selfWidget) {
                var error_block = new ErrorBlock_2.ErrorBlock(_this);
                error_block.clear();
                event.preventDefault();
                var oData = {
                    '_do': 'CreateModel',
                    'data': selfWidget.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_2.Editor.onModelCreated(data);
                    }
                }, 'json');
            };
            _this.type = Template_3.TypeType.popup;
            _this.subject = Template_3.SubjectType.model;
            _this.editor = editor;
            _this.form = new ModelForm_2.NewModelForm();
            Template_3.Template.getForm(_this);
            return _this;
        }
        ModelPopup.prototype.bindEvents = function () {
            this.create_button = new Button_2.Button('#fld_create_model', this.create, this);
            return this;
        };
        ModelPopup.prototype.show = function () {
            this.getPlaceHolder().replaceWith(this.template);
            this.bindEvents();
            this.form = new ModelForm_2.NewModelForm();
            return this;
        };
        return ModelPopup;
    }(AbstractWidget_3.AbstractWidget));
    exports.ModelPopup = ModelPopup;
});
define("schema-editor/widget/PropertyPopup", ["require", "exports", "schema-editor/model/ErrorBlock", "schema-editor/model/PropertyForm", "schema-editor/contracts/AbstractWidget", "schema-editor/widget/Editor", "schema-editor/model/Template", "schema-editor/model/Button"], function (require, exports, ErrorBlock_3, PropertyForm_3, AbstractWidget_4, Editor_3, Template_4, Button_3) {
    "use strict";
    exports.__esModule = true;
    var PropertyPopup = (function (_super) {
        __extends(PropertyPopup, _super);
        function PropertyPopup(editor) {
            var _this = _super.call(this) || this;
            _this.selector = '#add_property_popup';
            _this.create = function (event, selfClass) {
                console.log("PropertyPopup.save");
                event.preventDefault();
                console.log("\t", "Use error_block");
                console.log("\t", 'this', _this);
                console.log("\t", 'form data', selfClass.form.getData());
                var error_block = new ErrorBlock_3.ErrorBlock(_this);
                var oData = {
                    '_do': 'CreateProperty',
                    'data': selfClass.form.getData()
                };
                $.post(window.location.href, oData, function (data) {
                    if (data.errors) {
                        error_block.show(data);
                    }
                    else {
                        Editor_3.Editor.onPropertyCreated({ content: data, topic: 'property' });
                    }
                }, 'json');
            };
            console.log('PropertyPopup.constructor');
            _this.editor = editor;
            _this.type = Template_4.TypeType.popup;
            _this.subject = Template_4.SubjectType.property;
            Template_4.Template.getForm(_this);
            return _this;
        }
        PropertyPopup.prototype.bindEvents = function () {
            console.log('bind #fld_create_property');
            this.create_button = new Button_3.Button('#fld_create_property', this.create, this);
            this.fkToggler();
            return this;
        };
        PropertyPopup.prototype.fkToggler = function () {
            var oModelBlock = $('#blk_fk_model');
            var oModelField = $('#fld_fk_model');
            var oModelFieldField = $('#fld_fk_model_field');
            var oModelFieldBlock = $('#blk_fk_model_field');
            var oFormFieldType = $('#fld_form_field_type_id');
            var loadModelFields = function () {
                if (oModelField.val()) {
                    var oData = {
                        _do: 'GetModelFields',
                        model_id: oModelField.val()
                    };
                    $.get(window.location.href, oData, function (data) {
                        console.log('data', data);
                        oModelFieldField.html('');
                        data.forEach(function (option) {
                            oModelFieldField.append('<option>' + option.label + '</option>')
                                .attr('value', option.id);
                        });
                        console.log(data);
                    }, 'json').then(function () {
                        console.log("Model fields loaded");
                    });
                    oModelFieldBlock.css('display', 'block');
                }
                else {
                    oModelFieldBlock.css('display', 'none');
                }
            };
            oModelField.on('change', function () {
                console.log('chaaaaaaaaaaa');
                loadModelFields();
            });
            var toggleBlocks = function () {
                var isFk = (oFormFieldType.find(':selected').data('is-foreign-key') === 1);
                oModelBlock.css('display', isFk ? 'block' : 'none');
                loadModelFields();
            };
            console.log('PropertyPopup.fkToggle');
            toggleBlocks();
            oFormFieldType.on('change', function () {
                toggleBlocks();
            });
            return this;
        };
        PropertyPopup.prototype.setData = function (data) {
            console.log('PropertyPopup.setData', data);
            if (typeof data.table_name != 'undefined') {
                this.form.table_name = data.table_name;
            }
            _super.prototype.setData.call(this, data);
            return this;
        };
        PropertyPopup.prototype.show = function () {
            console.log('PropertyPopup.show');
            console.log("\t", 'template', this.template);
            this.getPlaceHolder().replaceWith(this.template);
            this.bindEvents();
            this.form = new PropertyForm_3.NewPropertyForm();
            return this;
        };
        PropertyPopup.prototype.hide = function (immediate) {
            if (immediate === void 0) { immediate = false; }
            if (typeof window['closePopup'] === "function") {
                window['closePopup']();
            }
            this.getPanel().replaceWith(this.createPlaceholder());
            return this;
        };
        return PropertyPopup;
    }(AbstractWidget_4.AbstractWidget));
    exports.PropertyPopup = PropertyPopup;
});
define("schema-editor/widget/Editor", ["require", "exports", "schema-editor/widget/ModelPanel", "schema-editor/widget/PropertyPanel", "schema-editor/widget/ModelPopup", "schema-editor/widget/PropertyPopup", "schema-editor/model/Messenger"], function (require, exports, ModelPanel_1, PropertyPanel_1, ModelPopup_1, PropertyPopup_1, Messenger_2) {
    "use strict";
    exports.__esModule = true;
    var Editor = (function () {
        function Editor() {
        }
        Editor.init = function () {
            Editor.modelPanel = new ModelPanel_1.ModelPanel(this);
            Editor.modelPopup = new ModelPopup_1.ModelPopup(this);
            Editor.propertyPanel = new PropertyPanel_1.PropertyPanel(this);
            Editor.propertyPopup = new PropertyPopup_1.PropertyPopup(this);
        };
        Editor.start = function () {
            this.init();
            Editor.modelPanel.hide();
            Editor.modelPopup.hide();
            Editor.propertyPanel.hide();
            Editor.propertyPopup.hide();
            Messenger_2.Messenger.onReceive('add_model', Editor.showAddModelPopup);
            Messenger_2.Messenger.onReceive('edit_model', Editor.showEditModelPanel);
            Messenger_2.Messenger.onReceive('add_property', Editor.showAddPropertyPopup);
            Messenger_2.Messenger.onReceive('edit_property', Editor.showEditPropertyPanel);
        };
        Editor.getAllPanels = function () {
            return [Editor.modelPanel, Editor.modelPopup, Editor.propertyPanel, Editor.propertyPopup];
        };
        Editor.activePanel = function (newActivePanel) {
            var aAllViews = Editor.getAllPanels();
            for (var i = 0; i < aAllViews.length; i++) {
                aAllViews[i].hide(true);
            }
            console.log('Editor.activePanel.newActivePanel', newActivePanel);
            newActivePanel.show();
        };
        Editor.showEditPropertyPanel = function (message) {
            console.log('Editor.showEditPropertyPanel.message', message);
            Editor.activePanel(Editor.propertyPanel);
            Editor.propertyPanel.setData(message.content);
        };
        Editor.showEditModelPanel = function (message) {
            console.log('Editor.showEditModelPanel.message', message);
            Editor.activePanel(Editor.modelPanel);
            Editor.modelPanel.setData(message.content);
        };
        Editor.showAddPropertyPopup = function (message) {
            console.log('Editor.showAddPropertyPopup', message.content);
            Editor.activePanel(Editor.propertyPopup);
            Editor.propertyPopup.setData(message.content);
        };
        Editor.showAddModelPopup = function () {
            console.log('Editor.showAddModelPopup');
            Editor.activePanel(Editor.modelPopup);
        };
        Editor.onPropertyDeleted = function (data) {
            console.log('Editor.onPropertyDeleted', data);
        };
        Editor.onPropertySaved = function (data) {
            console.log('Editor.onPropertySaved', data);
        };
        Editor.onPropertyCreated = function (data) {
            console.log('Editor.onPropertyCreated', data);
            console.log('Editor.onPropertyCreated', 'Editor.propertyPopup.hide()');
            Editor.propertyPopup.hide();
            Messenger_2.Messenger.send('property_added', data.content);
            console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.show()');
            Editor.propertyPanel.show();
            console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.setData(data.content);', data.content);
            Editor.propertyPanel.setData(data.content);
        };
        Editor.onModelCreated = function (data) {
            console.log('Editor.onModelCreated', data);
            Messenger_2.Messenger.send('model_added', data);
            Editor.modelPopup.hide();
            Editor.modelPanel.show();
            Editor.modelPanel.setData(data.content);
        };
        return Editor;
    }());
    exports.Editor = Editor;
});
define("main", ["require", "exports", "schema-editor/widget/Editor"], function (require, exports, Editor_4) {
    "use strict";
    exports.__esModule = true;
    Editor_4.Editor.start();
});
//# sourceMappingURL=main.js.map