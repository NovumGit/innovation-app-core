"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventHandler = /** @class */ (function () {
    function EventHandler() {
    }
    EventHandler.trigger = function (event, props, state) {
        event.trigger(props, state);
    };
    return EventHandler;
}());
exports.EventHandler = EventHandler;
