"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventHandler_1 = require("./EventHandler");
var EventStack = /** @class */ (function () {
    function EventStack() {
        var _this = this;
        this.execute = function () {
            _this.stack.forEach(function (event) {
                EventHandler_1.EventHandler.trigger(event.handler, event.props, event.state);
            });
        };
    }
    EventStack.prototype.register = function (event, props, state) {
        this.stack.push({ handler: event, props: props, state: state });
    };
    return EventStack;
}());
exports.EventStack = EventStack;
