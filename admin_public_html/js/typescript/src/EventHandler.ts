import {BasePromiseType, BaseProps, BaseState, IEvent} from "./event/IEvent";
import {AbstractEvent} from "./event/AbstractEvent";

export class EventHandler<E extends IEvent<BaseProps, BaseState, BasePromiseType>, T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>>{


    static trigger<T extends AbstractEvent<BaseProps, BaseState, BasePromiseType>>(event : T, props : T['propType'], state :  T['stateType']):Promise<T['promiseType']>
    {
        return new Promise<BasePromiseType>((resolve : any, reject : any) => {

            console.log('EventHandler.trigger');
            event.trigger(props, state).then(resolve()).catch(reject());

        })



    }
}
