import {DropHandler} from './DropHandler';
import {DragHandler} from './DragHandler';

export function main() {
    console.log('start app designer');

    DropHandler.run();
    DragHandler.run();
}
