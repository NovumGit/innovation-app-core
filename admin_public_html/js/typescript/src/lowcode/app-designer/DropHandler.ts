import DroppableEventUIParam = JQueryUI.DroppableEventUIParam;

export class DropHandler {

    static run(): void {


        this.bindDropzone();
    }

    private static bindDropzone = () => {
        $(".droppable_zone").each(function (i, e) {

            let currentElement = $(e);
            console.log(currentElement);
            currentElement.droppable({
                accept: ".draggable",
                over: (evt: JQueryEventObject, ui: DroppableEventUIParam) => {
                    currentElement.addClass('over');
                },
                out: (evt: JQueryEventObject, ui: DroppableEventUIParam) => {
                    currentElement.removeClass('over');
                },
                drop: (evt, ui) => {


                    let iUiComponentTypeId = $(ui.helper[0]).data('component-type');
                    let iUiComponentTitle = $(ui.helper[0]).data('component-title');

                    let iComponentId = currentElement.data('ui-component-id');

                    let sComponentLocation = currentElement.data('ui-component-location');

                    let oData = {
                        "_do": "AddComponent",
                        "ui_component_type_id": iUiComponentTypeId,
                        "parent_location" : sComponentLocation,
                        "parent_id": iComponentId,
                        "label": iUiComponentTitle
                    };

                    const callback = (data: string) => {
                        console.log(data);
                    };

                    $.post('/apps/app/designer/component?ui_component_id=' + iComponentId, oData, callback, "json");

                    window.location = window.location;
                    $('.component_canvas').append('<div class="droppable_zone" data-ui-component-id="' + iComponentId + '">apended</div>');
                    DropHandler.bindDropzone();

                }
            });
        });

    };


}
