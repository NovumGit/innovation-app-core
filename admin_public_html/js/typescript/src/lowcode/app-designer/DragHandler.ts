

export class DragHandler {

    static run(): void {


        this.bindDraggable();
    }

    private static bindDraggable = () => {
        $( ".draggable.component" ).draggable({
            addClasses: true,
            helper : "clone",
            drag : (evt, ui) => {

                $(ui.helper[0]).addClass('dragging');
            },
            stop : (evt, ui) => {

                $(ui.helper[0]).removeClass('dragging');
            },
            revert : 'invalid'
        });

    };


}
