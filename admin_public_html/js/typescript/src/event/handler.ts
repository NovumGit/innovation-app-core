import {Event} from "./event";

import {Delete} from "./events/Delete";
import {Patch} from "./events/Patch";
import {Post} from "./events/Post";
import {Redirect} from "./events/Redirect";
import {Reload} from "./events/Reload";
import {StatusModal} from "./events/StatusModal";
import {StatusMessage} from "./events/StatusMessage";

import {Store} from "./events/Store";


export class Handler {


    async handle(eventSequence : Event[])
    {
        console.log("Handler::handle");

        let event:Event;
        for(let i = 0; i < eventSequence.length; i++)
        {
            event = eventSequence[i];
            console.log("Handler::handle -> " + event.name);

            try
            {
                if(event.name == 'Delete')
                {
                    await (new Delete).trigger(event.props, event.state);
                }
                else if(event.name == 'Patch')
                {
                    await (new Patch).trigger(event.props, event.state);
                }
                else if(event.name == 'Post')
                {
                    await (new Post).trigger(event.props, event.state);
                }
                else if(event.name == 'Redirect')
                {
                    await (new Redirect).trigger(event.props);
                }
                else if(event.name == 'Reload')
                {
                    await (new Reload).trigger();
                }
                else if(event.name == 'StatusModal')
                {
                    await (new StatusModal).trigger(event.props, event.state);
                }
                else if(event.name == 'StatusMessage')
                {
                    await (new StatusMessage).trigger(event.props, event.state);
                }
                else if(event.name == 'Store')
                {
                    await (new Store).trigger(event.props, event.state);
                }
                else
                {
                    console.error("event missing "  + event.name);

                }

            }
            catch (err)
            {
                console.error(err);
            }
            finally {
                console.log("Handler::done!");
            }
        }



    }

}
