import {httpCall, httpMethod} from "../http";
import * as t from "../types/overheid.d";
import {AbstractEvent} from "../AbstractEvent";
import {BaseProps, BaseState, IEvent} from "../IEvent";

class DeleteState implements BaseState {
    id : string;
    context : {
        form : {
            id : string
        }
    }
}
class DeleteProps implements BaseProps{
    datasource : t.datasource;
    endpoint:string;
}


export class Delete extends AbstractEvent<DeleteProps, DeleteState> implements IEvent<DeleteProps, DeleteState>{

    propertyType : DeleteProps;
    stateType : DeleteState;

    trigger(props : Delete['propertyType'], state: Delete['stateType']):Promise<string>
    {
        return new Promise((resolve:any, reject:any) => {

            let sUrl = props.datasource.url + props.endpoint;
            let rejectHandler = this.getRejectHandler(reject, "Whoops", "Sorry we where unable to delete that item\n {textStatus}");

            httpCall(sUrl, httpMethod.DELETE, '', resolve, rejectHandler);
        });
    }
}
