import {BaseProps, BaseState, IEvent} from "../IEvent";
import {parseForm} from "../util";
import {httpCall, httpMethod} from "../http";
import * as t from "../types/overheid";
import {AbstractEvent} from "../AbstractEvent";

class MessageState implements BaseState{
    context : {
        form : {
            id : string
        }
    }
}

class MessageProps implements BaseProps{
    datasource : t.datasource;
    endpoint : string;
    defaults : {}
}


export class Message extends AbstractEvent<MessageProps, MessageState> implements IEvent<MessageProps, MessageState>{

    trigger(props : MessageProps, state : MessageState):Promise<string>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            let rejectHandler = this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");

            httpCall(sUrl, httpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
