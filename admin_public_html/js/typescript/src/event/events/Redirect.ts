import {BaseProps, BaseState, IEvent} from "../IEvent";
import {AbstractEvent} from "../AbstractEvent";

export class RedirectProps implements BaseProps{
    location : string;
    endpoint : string;
}
export class RedirectState implements BaseState{}


export class Redirect extends AbstractEvent<RedirectProps, RedirectState> implements IEvent<RedirectProps, RedirectState>{

    propType  : RedirectProps;
    stateType  : RedirectState;


    trigger(props : Redirect['propType']):Promise<string>
    {
        return new Promise((resolve : any) => {

            window.location.href = props.location;
            resolve("Redirecting the user to: " + props.location);
        });

    }
}
