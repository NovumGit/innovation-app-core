import {BaseProps, BaseState, IEvent} from "../IEvent";
import {parseForm} from "../util";
import {httpCall, httpMethod} from "../http";
import * as t from "../types/overheid";
import {AbstractEvent} from "../AbstractEvent";

class PatchState implements BaseState{
    context : {
        form : {
            id : string
        }
    }
}

class PatchProps implements BaseProps{
    datasource : t.datasource;
    endpoint : string;
    defaults : {}
}



export class Patch extends AbstractEvent<PatchProps, PatchState> implements IEvent<PatchProps, PatchState>{

    trigger(props : PatchProps, state : PatchState):Promise<string>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            let rejectHandler = this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");

            httpCall(sUrl, httpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
