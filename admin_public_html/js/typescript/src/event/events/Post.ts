import {BaseProps, BaseState, IEvent} from "../IEvent";
import {parseForm} from "../util";
import {httpCall, httpMethod} from "../http";
import * as t from "../types/overheid.d";
import {AbstractEvent} from "../AbstractEvent";

export interface PostProps extends BaseProps{
    datasource : t.datasource;
    defaults : {};
    endpoint : string;
}

export interface PostState extends BaseState{
    context : {
        form : {
             id : string
        }
    }
}

export class Post<P extends PostProps, S extends PostState> extends AbstractEvent<BaseProps, BaseState> implements IEvent<BaseProps, BaseState>{

    trigger(props : P, state : S):Promise<string>
    {
        return new Promise((resolve : any, reject : any) => {

            const sUrl = props.datasource.url + props.endpoint;
            let data = parseForm(state.context.form.id);

            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }

            let rejectHandler = this.getRejectHandler(reject, "Whoops", "Het aanmaken is mislukt");
            httpCall(sUrl, httpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });

    }
}
