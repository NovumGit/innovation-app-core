"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("../util");
var http_1 = require("../http");
var AbstractEvent_1 = require("../AbstractEvent");
var PatchState = /** @class */ (function () {
    function PatchState() {
    }
    return PatchState;
}());
var PatchProps = /** @class */ (function () {
    function PatchProps() {
    }
    return PatchProps;
}());
var Patch = /** @class */ (function (_super) {
    __extends(Patch, _super);
    function Patch() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Patch.prototype.trigger = function (props, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sUrl = props.datasource.url + props.endpoint;
            var data = util_1.parseForm(state.context.form.id);
            if (props.defaults) {
                data = Object.assign(props.defaults, data);
            }
            var rejectHandler = _this.getRejectHandler(reject, "Whoops", "Het wijzigen is mislukt");
            http_1.httpCall(sUrl, http_1.httpMethod.POST, JSON.stringify(data), resolve, rejectHandler);
        });
    };
    return Patch;
}(AbstractEvent_1.AbstractEvent));
exports.Patch = Patch;
