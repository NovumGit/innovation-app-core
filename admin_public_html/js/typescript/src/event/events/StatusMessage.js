"use strict";
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var StatusMessage_1 = require("../types/StatusMessage");
var AbstractEvent_1 = require("../AbstractEvent");
var StatusMessage = /** @class */ (function (_super) {
    __extends(StatusMessage, _super);
    function StatusMessage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StatusMessage.prototype.trigger = function (props, state) {
        return new Promise(function (resolve) {
            if (state instanceof StatusMessage_1.StatusValidationState) {
                state.errors.forEach(function (e) {
                    state.message = "- " + e + "\n";
                });
            }
            if (state.message) {
                StatusMessage.show(state.StatusMessageColor, state.message, state.title, state.elementId);
            }
            resolve("Showing status message");
        });
    };
    StatusMessage.getTemplate = function (sType, sLabel, sMessage, sElementId) {
        var sLabelColor = sType === 'warning' ? 'black' : 'white';
        var sTemplate = '';
        // sTemplate += '<section id="content" class="table-layout animated fadeIn">';
        // sTemplate += '   <div class="panel">';
        if ($('#' + sElementId).length <= 0) {
            // if($('#' + sElementId).length )
            sTemplate += '       <div class="bs-component" id="' + sElementId + '">';
            sTemplate += '           <div class="alert alert-' + sType + ' dark alert-dismissable" style="color:' + sLabelColor + ';">';
            sTemplate += '               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            sTemplate += '               <i class="fa fa-info pr10" />';
            sTemplate += '               <strong>' + sLabel + ':</strong> ' + sMessage;
            sTemplate += '           </div>';
            sTemplate += '       </div>';
            //sTemplate += '   </div>';
            // sTemplate += '</section>';
        }
        else {
            console.log("Already showing message with id " + sElementId + ", skipping for now.");
        }
        return sTemplate;
    };
    StatusMessage.show = function (sType, sMessage, sTitle, sElementId) {
        var sTemplate = StatusMessage.getTemplate(sType, sTitle, sMessage, sElementId);
        if (sTemplate) {
            $('#status_message_container').html(sTemplate);
        }
    };
    return StatusMessage;
}(AbstractEvent_1.AbstractEvent));
exports.StatusMessage = StatusMessage;
