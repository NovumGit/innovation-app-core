import {parseForm} from "../util";
import {Post, PostProps, PostState} from "./Post";
import {Patch} from "./Patch";
import * as t from "../types/overheid";
import {AbstractEvent} from "../AbstractEvent";
import {BaseProps, BaseState, IEvent} from "../IEvent";

export interface StoreState extends BaseState{
    context : {
        form : {
            id : string
        }
    }
}
export interface StoreProps extends BaseProps{
    datasource : t.datasource,
    defaults : {}
    endpoint : string
}

export class Store extends AbstractEvent<StoreProps, StoreState> implements IEvent<StoreProps, StoreState>{

    trigger(props : StoreProps, state : StoreState):Promise<string>
    {
        return new Promise<string>((resolve : any, reject : any) => {

            let formData = parseForm(state.context.form.id);

            console.log('IForm data');
            console.log(JSON.stringify(formData));
            if (typeof(formData.id) == 'undefined' || !formData.id.trim()) {
                console.log('Store::trigger -> posting (no id)');
                props.endpoint = props.endpoint + '/';

                (new Post<PostProps, PostState>()).trigger(props, state)
                    .then(() => {
                        console.log("post done");
                        resolve("posting done");
                    }).catch((reason => {
                        reject(reason);
                    })
                );

            } else {
                console.log('Store::trigger -> patching (' + formData.id + ')');
                props.endpoint = props.endpoint + '/' + formData.id;

                (new Patch()).trigger(props, state)
                    .then(() => {
                        console.log('Patching done');
                        resolve('patching done');
                    });
            }

        });

    }
}

