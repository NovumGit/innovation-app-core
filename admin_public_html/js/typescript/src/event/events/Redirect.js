"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractEvent_1 = require("../AbstractEvent");
var RedirectProps = /** @class */ (function () {
    function RedirectProps() {
    }
    return RedirectProps;
}());
exports.RedirectProps = RedirectProps;
var RedirectState = /** @class */ (function () {
    function RedirectState() {
    }
    return RedirectState;
}());
exports.RedirectState = RedirectState;
var Redirect = /** @class */ (function (_super) {
    __extends(Redirect, _super);
    function Redirect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Redirect.prototype.trigger = function (props) {
        return new Promise(function (resolve) {
            window.location.href = props.location;
            resolve("Redirecting the user to: " + props.location);
        });
    };
    return Redirect;
}(AbstractEvent_1.AbstractEvent));
exports.Redirect = Redirect;
