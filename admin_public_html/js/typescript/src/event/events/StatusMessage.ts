/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>

import {BasePromiseType, BaseProps, IEvent} from "../IEvent";
import * as sT from "../types/StatusMessage";
import {StatusState, StatusValidationState} from "../types/StatusMessage";
import {AbstractEvent} from "../AbstractEvent"; import {Promise} from "es6-promise";

export interface StatusMessageProps extends BaseProps {
    type : sT.StatusMessageColors;
    label : string;
    message : string;
    elementId : string;
}

export class StatusMessagePromise implements BasePromiseType {


    then;
    constructor(resolve : (resolve : any) => any, reject : (reject : string) => any) {
    }

    onFulfilled(callback:(arg: any) => any): void{
        console.log('fulfilled');
    }
    onRejected(callback:(arg: any) => any): void{
        console.log('rejected');
    }
}
export class StatusMessage<P extends StatusMessageProps, S extends StatusState, PT extends StatusMessagePromise> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT> {

    propType: P;
    stateType: S;
    promiseType: PT;

    getPropType() : P {
        return this.propType;
    }
    getStateType() : S {
        return this.stateType;
    }
    getPromiseType() : PT {
        return this.promiseType;
    }
    trigger<PT>(props: P, state: S): Promise<PT> {
        return new Promise<PT>((resolve : any) => {

            StatusMessage.show(props.type, props.label, props.message, props.elementId);
            resolve();
        });

        /*
        new StatusMessagePromise((resolve: any) => {
            if (state instanceof StatusValidationState) {
                state.errors.forEach((e) => {
                    state.message = "- " + e + "\n";
                });
            }

            if (state.message) {
                StatusMessage.show(state.StatusMessageColor, state.message, state.title, state.elementId);
            }
            resolve("Showing status message");
        });

         */
    }

    private static getTemplate(sType: sT.StatusMessageColors, sLabel: string, sMessage: string, sElementId: string) {

        let sLabelColor = sType === 'warning' ? 'black' : 'white';
        let sTemplate = '';
        // sTemplate += '<section id="content" class="table-layout animated fadeIn">';
        // sTemplate += '   <div class="panel">';

        if ($('#' + sElementId).length <= 0) {
            // if($('#' + sElementId).length )
            sTemplate += '       <div class="bs-component" id="' + sElementId + '">';
            sTemplate += '           <div class="alert alert-' + sType + ' dark alert-dismissable" style="color:' + sLabelColor + ';">';
            sTemplate += '               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>';
            sTemplate += '               <i class="fa fa-info pr10" />';
            sTemplate += '               <strong>' + sLabel + ':</strong> ' + sMessage;
            sTemplate += '           </div>';
            sTemplate += '       </div>';
            //sTemplate += '   </div>';
            // sTemplate += '</section>';
        } else {
            console.log("Already showing message with id " + sElementId + ", skipping for now.");
        }
        return sTemplate;
    }

    private static show(sType: sT.StatusMessageColors, sMessage: string, sTitle: string, sElementId: string) {
        const sTemplate = StatusMessage.getTemplate(sType, sTitle, sMessage, sElementId);
        if (sTemplate) {
            $('#status_message_container').html(sTemplate);
        }
    }

    /*
    private static success(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.success, sMessage, sTitle, sElementId);
    }
    private static alert(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.alert, sMessage, sTitle, sElementId);
    }
    private static warning(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.warning, sMessage, sTitle, sElementId);
    }
    private static danger(sTitle: string, sMessage: string, sElementId: string) {
        this.show(sT.StatusMessageColors.danger, sMessage, sTitle, sElementId);
    }
    */
}
