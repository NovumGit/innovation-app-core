"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("../http");
var AbstractEvent_1 = require("../AbstractEvent");
var DeleteState = /** @class */ (function () {
    function DeleteState() {
    }
    return DeleteState;
}());
var DeleteProps = /** @class */ (function () {
    function DeleteProps() {
    }
    return DeleteProps;
}());
var Delete = /** @class */ (function (_super) {
    __extends(Delete, _super);
    function Delete() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Delete.prototype.trigger = function (props, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var sUrl = props.datasource.url + props.endpoint;
            var rejectHandler = _this.getRejectHandler(reject, "Whoops", "Sorry we where unable to delete that item\n {textStatus}");
            http_1.httpCall(sUrl, http_1.httpMethod.DELETE, '', resolve, rejectHandler);
        });
    };
    return Delete;
}(AbstractEvent_1.AbstractEvent));
exports.Delete = Delete;
