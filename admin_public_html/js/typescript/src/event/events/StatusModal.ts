import {BaseProps, IEvent} from "../IEvent";
import {AbstractEvent} from "../AbstractEvent";
import * as sT from "../types/StatusMessage";
import {StatusMessageButton, StatusState, StatusValidationState} from "../types/StatusMessage";

export class StatusModalProps extends BaseProps{
    endpoint : string;
}

export class StatusModal<P extends StatusModalProps, S extends StatusState> extends AbstractEvent<StatusModalProps, StatusState> implements IEvent<StatusModalProps, StatusState>{

    propType : StatusModalProps;
    stateType : StatusState;

    trigger(props : P, state : S): void {
        if(state instanceof StatusValidationState)
        {
            if(state.errors)
            {
                state.message = '';

                state.errors.forEach((e) => {
                    state.message += "- " + e + "<br>\n";
                })
            }
        }
        if(state.message)
        {
            this.show(state.StatusMessageColor, state.message, state.title, state.elementId);
            //@ts-ignore
            // bindModalCloseButton();
        }
    }

    private getTemplate(sType: sT.StatusMessageColors, sLabel: string, sMessage: string, sElementId: string, aButtons?:StatusMessageButton[]) {

        // let sLabelColor = sType === 'warning' ? 'black' : 'white';
        let sTemplate = '';

        if ($('#' + sElementId).length <= 0) {

            sTemplate += '       <div class="auto-modal popup-basic bg-none mfp-with-anim">';
            sTemplate += '          <div class="panel">';
            sTemplate += '              <div class="panel-heading">';
            sTemplate += '                  <span class="panel-icon">';
            sTemplate += '                      <i class="fa fa-' + sType + ' text-' + sType + '" />';
            sTemplate += '                  </span>';
            sTemplate += '                  <span class="panel-title text-' + sType + '">';
            sTemplate += '                      ' + sLabel;
            sTemplate += '                  </span>';
            sTemplate += '              </div>';
            sTemplate += '              <div class="panel-body">';
            sTemplate += '                  <p>';
            sTemplate += '                      ' + sMessage;
            sTemplate += '                  </p>';
            sTemplate += '              </div>';

            sTemplate += '              <div class="panel-footer text-right">';
            if(aButtons && aButtons.length > 0)
            {
                aButtons.forEach((e) => {

                    sTemplate += '       <a title="' + e.title + '" class="btn btn-default btn-' + e.type + '" href="' + e.action + '">';
                    sTemplate += '          ' + e.label;
                    sTemplate += '       </a>'
                });
            }
            else
            {
                sTemplate += '       <button type="button" class="btn btn-default" id="btn_modal_close">Sluiten</button>';

            }
            sTemplate += '       </div>';
            sTemplate += '       </div>';
            sTemplate += '       </div>';

        } else {
            console.log("Already showing message with id " + sElementId + ", skipping for now.");
        }
        return sTemplate;
    }

    private show(sType: sT.StatusMessageColors, sMessage: string, sTitle: string, sElementId: string) {
        const sTemplate = this.getTemplate(sType, sTitle, sMessage, sElementId);
        if (sTemplate) {
            $('#status_modal_container').html(sTemplate);
            // @ts-ignore
            $.magnificPopup.open({
                removalDelay: 500,
                items: {
                    src: '.auto-modal'
                },
                callbacks: {
                    beforeOpen: function () {
                        this.st.mainClass = 'mfp-zoomIn';
                    }
                },
                midClick: true
            });
        }
    }
}

