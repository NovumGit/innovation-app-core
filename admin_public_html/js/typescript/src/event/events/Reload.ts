import {BaseProps, BaseState, IEvent} from "../IEvent";
import {AbstractEvent} from "../AbstractEvent";

export class Reload extends AbstractEvent<BaseProps, BaseState> implements IEvent<BaseProps, BaseState>{


    trigger():Promise<string>
    {
        return new Promise((resolve) => {

            window.location = window.location;
            resolve("Reloading the page");
        });

    }
}
