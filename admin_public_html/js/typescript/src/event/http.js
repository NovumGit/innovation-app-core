"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var httpMethod;
(function (httpMethod) {
    httpMethod["GET"] = "GET";
    httpMethod["POST"] = "POST";
    httpMethod["PUT"] = "PUT";
    httpMethod["DELETE"] = "DELETE";
    httpMethod["PATCH"] = "PATCH";
})(httpMethod = exports.httpMethod || (exports.httpMethod = {}));
function httpCall(sUrl, eMethod, data, done, fail) {
    var deleteRequest = $.ajax({
        url: sUrl,
        method: eMethod,
        contentType: "application/json",
        data: data,
        dataType: "json"
    });
    deleteRequest.done(function (msg) {
        console.log('resolve http call');
        console.log(msg.errors);
        if (msg.errors) {
            fail(msg);
            return;
        }
        done(msg);
    });
    deleteRequest.fail(function (jqXHR, textStatus) {
        console.log('fail http call');
        console.log(jqXHR);
        fail(textStatus);
    });
}
exports.httpCall = httpCall;
