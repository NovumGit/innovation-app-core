export class BaseProps{}
export class BaseState {}

export interface BasePromiseType extends PromiseLike<any>{
    onFulfilled(callback : (arg : any) => any):void;
    onRejected(callback : (arg : any) => any):void;
}


export interface IEvent<propertyType extends BaseProps, stateType extends BaseState, promiseType extends BasePromiseType> {


    propType : propertyType;
    stateType : stateType;
    promiseType : promiseType;

    trigger(props : propertyType, state : stateType, ): Promise<promiseType>;

}
