import {Handler} from "./handler";

export function main() {
    let eventButtons = document.getElementsByClassName('event_button');

    for(let i = 0; i < eventButtons.length; i++)
    {
        eventButtons[i].addEventListener('click', (event) => {

            let sEventData = eventButtons[i].getAttribute('data-event');
            let aEventData = JSON.parse(sEventData);

            console.info("Received event data");
            const handler = new Handler();
            handler.handle(aEventData);
            event.preventDefault();
        });
    }
}
