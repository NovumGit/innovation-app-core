

export enum httpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    PATCH = 'PATCH'
}

export interface ajaxDone {
    (message: string): Promise<string>;
}

export interface ajaxFailHandler {
    (textStatus: any): void;
}

export function httpCall(sUrl : string, eMethod : httpMethod, data : string, done : ajaxDone, fail : ajaxFailHandler) {

    var deleteRequest = $.ajax({
        url: sUrl,
        method: eMethod,
        contentType: "application/json",
        data: data,
        dataType: "json"
    });

    deleteRequest.done((msg) => {
        console.log('resolve http call');
        console.log(msg.errors);
        if(msg.errors)
        {
            fail(msg);
            return;
        }
        done(msg);
    });

    deleteRequest.fail((jqXHR, textStatus) => {
        console.log('fail http call');
        console.log(jqXHR);
        fail(textStatus);
    });
}
