"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StatusMessageColors;
(function (StatusMessageColors) {
    StatusMessageColors["info"] = "info";
    StatusMessageColors["success"] = "success";
    StatusMessageColors["alert"] = "alert";
    StatusMessageColors["warning"] = "warning";
    StatusMessageColors["danger"] = "danger";
})(StatusMessageColors = exports.StatusMessageColors || (exports.StatusMessageColors = {}));
var StatusState = /** @class */ (function () {
    function StatusState() {
    }
    return StatusState;
}());
exports.StatusState = StatusState;
var StatusValidationState = /** @class */ (function () {
    function StatusValidationState() {
    }
    return StatusValidationState;
}());
exports.StatusValidationState = StatusValidationState;
