import {validationErrors} from "./validation";
import {BaseProps, BaseState, IEvent} from "../IEvent";
import {EventStack} from "../../EventStack";
import {AbstractEvent} from "../AbstractEvent";

export enum StatusMessageColors {
    info = "info",
    success = "success",
    alert = "alert",
    warning = "warning",
    danger = "danger"
}

export interface StatusMessageButton{
    label : string,
    action : IEvent<BaseProps, BaseState> | EventStack<IEvent<BaseProps, BaseState>, AbstractEvent<BaseProps, BaseState>>,
    title : string,
    type : StatusMessageColors
}

export class StatusState implements BaseState{
    message?: string;
    title : string;
    elementId : string;
    StatusMessageColor : StatusMessageColors;
    buttons? : StatusMessageButton[];
}
export class StatusValidationState implements StatusState{
    errors: validationErrors;
    message?: string;
    title : string;
    elementId : string;
    StatusMessageColor : StatusMessageColors
}
