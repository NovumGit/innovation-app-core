import {BasePromiseType, BaseProps, BaseState, IEvent} from "./IEvent";
import {ajaxFailHandler} from "./http";
import {StatusMessageColors, StatusState, StatusValidationState} from "./types/StatusMessage";


export abstract class AbstractEvent<P extends BaseProps, S extends BaseState, PT extends BasePromiseType> implements IEvent<P, S, PT>{

    abstract propType : P;
    abstract stateType: S;
    abstract promiseType : PT;



    abstract trigger(props : P, state : S): Promise<PT>;

    getPropType() : P
    {
        return this.propType;
    }
    getStateType() : S
    {
        return this.stateType;
    }
    getRejectHandler(reject : any, title : string, message : string):ajaxFailHandler
    {
        return async (textStatus: any) => {


            console.log(textStatus);
            let errors = null;

            if(textStatus.errors)
            {
                errors = textStatus.errors;
            }

            if(errors)
            {
                // Validatie errors
                let oState:StatusValidationState = {
                    title : title,
                    errors : errors,
                    elementId : "error_dialog",
                    StatusMessageColor : StatusMessageColors.warning
                };


                reject("Validation errors occured");
            }
            else
            {
                let oState:StatusState = {
                    title : title,
                    message : message.replace('{textStatus}', textStatus),
                    elementId : "error_dialog",
                    StatusMessageColor : StatusMessageColors.warning
                };
            }

        };
    }
}
