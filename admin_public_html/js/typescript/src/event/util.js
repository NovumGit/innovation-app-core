"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function parseForm(formId) {
    console.log("parse form " + formId);
    var oForm = $('#' + formId).find('input, select');
    var dataArray = oForm.serializeArray();
    var formData = {};
    // console.log(dataArray);
    dataArray.forEach(function (e, i) {
        var name = e.name.replace('data[', '').replace(']', '');
        // console.log('1', name, e.value);
        // console.log('e', e);
        formData[name] = e.value;
    });
    console.log(JSON.stringify(formData));
    return formData;
}
exports.parseForm = parseForm;
