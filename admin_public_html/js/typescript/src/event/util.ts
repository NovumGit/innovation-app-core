type formDataType = { [key: string]: string; };



export function parseForm (formId :string):formDataType{
    console.log("parse form " + formId);

    let oForm = $('#' + formId).find('input, select');

    let dataArray = oForm.serializeArray();
    type AssociativeArray<T = unknown> = {[key: string]: T | undefined} | T[];

    let formData:formDataType = {};

    // console.log(dataArray);
    dataArray.forEach((e, i) => {


        let name = e.name.replace('data[', '').replace(']', '');
        // console.log('1', name, e.value);
        // console.log('e', e);
        formData[name] = e.value;
    });

    console.log(JSON.stringify(formData));
    return formData;
}
