import {BaseProps, BaseState, IEvent} from "./event/IEvent";
import {AbstractEvent} from "./event/AbstractEvent";
import {EventHandler} from "./EventHandler";


export class EventStack<E extends IEvent<BaseProps, BaseState>, T extends AbstractEvent<BaseProps, BaseState>>{

    stack : {handler : T, props : T['propType'], state : T['stateType']}[];

    register(event : T, props : T['propType'], state :  T['stateType'])
    {
        this.stack.push({handler: event, props : props, state : state})
    }

    execute = ():void =>
    {
        this.stack.forEach((event) => {
            EventHandler.trigger(event.handler, event.props, event.state);
        });
    }

}
