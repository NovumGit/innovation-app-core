import {AbstractEvent} from "../../event/AbstractEvent";
import {BasePromiseType, BaseProps, BaseState, IEvent} from "../../event/IEvent";
import {Ajax, AjaxAction} from "../Ajax";
import {IWidget} from "../contracts/IWidget";
import {EventHandler} from "../../EventHandler";

class DeletePropertyProps extends BaseProps {

    endpoint : string;
    action : AjaxAction;
}

class DeletePropertyState extends BaseState {
    widget: IWidget;
}

class DeletePropertyPromiseType implements BasePromiseType {

    resolve()
    {

    }
    onRejected(callback: (arg: any) => any): any {
    }

    onFulfilled(callback: (arg: any) => any): any {
    }
}
export class DeleteProperty<P extends DeletePropertyProps, S extends DeletePropertyState, PT extends DeletePropertyPromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT>{
    promiseType: PT;
    propType: P;
    stateType: S;

    trigger(props: P, state: S): Promise<PT> {

        return new Promise<PT>((resolve, reject) => {

            EventHandler.trigger(new Ajax(), {endpoint : props.endpoint, action : props.action}, {data : state.widget.form.getData()})
                .then(value => {
                    resolve(value);
                })
                .catch(value => {
                    reject(value);
                });

/*
            const error_block = new ErrorBlock(widget);
            error_block.clear();
            error_block.show(data);



            Ajax('DeleteProperty', state.widget, Editor.onPropertyDeleted);
            Messenger.send('property_deleted', state.data);

            console.log('DeleteProperty.trigger');
            console.log("\t", state.data);
*/
        });


    }
}

