/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>

import {IWidget} from "../contracts/IWidget";
import {ErrorBlock} from "../model/ErrorBlock";
import {NewPropertyForm} from "../model/PropertyForm";
import {AbstractWidget} from "../contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../model/Template";
import {IMessageContentGenerics} from "../model/Messenger";
import {Button} from "../model/Button";

export class PropertyPopup extends AbstractWidget implements IWidget{

    selector :string = '#add_property_popup';
    public create_button:Button;
    template : string;

    form : NewPropertyForm;
    private editor : Editor;
    constructor(editor : Editor) {
        super();
        console.log('PropertyPopup.constructor');
        this.editor = editor;
        this.type = TypeType.popup;
        this.subject = SubjectType.property;
        Template.getForm(this);
    }
    bindEvents():this {
        console.log('bind #fld_create_property');
        this.create_button = new Button('#fld_create_property', this.create, this);
        this.fkToggler();
        return this;
    }
    fkToggler() : this {
        const oModelBlock = $('#blk_fk_model');
        const oModelField = $('#fld_fk_model');
        const oModelFieldField = $('#fld_fk_model_field');

        const oModelFieldBlock = $('#blk_fk_model_field');
        const oFormFieldType = $('#fld_form_field_type_id');

        const loadModelFields = () =>
        {
            if(oModelField.val())
            {
                const oData = {
                    _do : 'GetModelFields',
                    model_id : oModelField.val()
                };

                $.get(window.location.href, oData, (data : {id : number, label : string}[]) => {

                    console.log('data', data);
                    oModelFieldField.html('');


                    data.forEach((option) => {
                        oModelFieldField.append('<option>' + option.label + '</option>')
                            .attr('value', option.id);
                    });
                    console.log(data);

                }, 'json').then(() => {

                    console.log("Model fields loaded");
                });
                oModelFieldBlock.css('display', 'block');
            }
            else
            {
                oModelFieldBlock.css('display', 'none');
            }
        };

        oModelField.on('change', () => {
            console.log('chaaaaaaaaaaa');
            loadModelFields();
        });
        const toggleBlocks = () => {
            const isFk : boolean = (oFormFieldType.find(':selected').data('is-foreign-key') === 1);
            oModelBlock.css('display', isFk ? 'block' : 'none');
            loadModelFields();
        };

        console.log('PropertyPopup.fkToggle');


        toggleBlocks();

        oFormFieldType.on('change', () => {
            toggleBlocks();
        });



        return this;
    }
    setData(data:  IMessageContentGenerics): this {
        console.log('PropertyPopup.setData', data);

        if(typeof data.table_name != 'undefined')
        {
            this.form.table_name = data.table_name;
        }
        super.setData(data);
        return this;
    }
    show(): this {
        console.log('PropertyPopup.show');
        console.log("\t", 'template', this.template);

        this.getPlaceHolder().replaceWith(this.template);
        this.bindEvents();
        this.form = new NewPropertyForm();
        return this;
    }
    hide(immediate: boolean = false): this {
        // @ts-ignore
        if(typeof window['closePopup'] === "function")
        {
            // @ts-ignore
            window['closePopup']();
        }

        this.getPanel().replaceWith(this.createPlaceholder());
        return this;
    }

    private create = (event : JQuery.ClickEvent, selfClass : PropertyPopup) => {

        console.log("PropertyPopup.save");

        event.preventDefault();
        console.log("\t", "Use error_block");
        console.log("\t", 'this', this);
        console.log("\t", 'form data', selfClass.form.getData());


        let error_block : ErrorBlock   = new ErrorBlock(this);
        let oData = {
            '_do': 'CreateProperty',
            'data': selfClass.form.getData()
        };
        $.post(window.location.href, oData, (data) => {

            if (data.errors)
            {
                error_block.show(data);
            }
            else
            {
                Editor.onPropertyCreated({content : data, topic : 'property'});
            }

        }, 'json');

    }
}
