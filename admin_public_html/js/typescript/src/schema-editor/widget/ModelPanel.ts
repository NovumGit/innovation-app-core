/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>

import {IWidget} from "../contracts/IWidget";
import {ErrorBlock} from "../model/ErrorBlock";

import {EditModelForm} from "../model/ModelForm";
import {AbstractWidget} from "../contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../model/Template";

export class ModelPanel extends AbstractWidget implements IWidget{

    public selector:string = '#edit_model_panel';
    public form:EditModelForm;
    public error_block:ErrorBlock;

    private editor : Editor;

    constructor(editor : Editor)
    {
        super();
        this.type = TypeType.panel;
        this.subject = SubjectType.model;

        this.editor = editor;
        this.form = new EditModelForm();

        Template.getForm(this);
    }
    bindEvents():this
    {


        return this;
    }
}
