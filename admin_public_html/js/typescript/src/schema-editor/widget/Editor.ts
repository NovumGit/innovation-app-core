import {ModelPanel} from "./ModelPanel";
import {PropertyPanel} from "./PropertyPanel";
import {ModelPopup} from "./ModelPopup";
import {PropertyPopup} from "./PropertyPopup";
import {IMessageContentGenerics, Message, Messenger} from "../model/Messenger";
import {IWidget} from "../contracts/IWidget";

export class Editor {

    private static modelPanel : ModelPanel;
    private static modelPopup : ModelPopup;
    private static propertyPanel : PropertyPanel;
    private static propertyPopup : PropertyPopup;

    static init()
    {
        Editor.modelPanel = new ModelPanel(this);
        Editor.modelPopup = new ModelPopup(this);
        Editor.propertyPanel = new PropertyPanel(this);
        Editor.propertyPopup = new PropertyPopup(this);
    }
    static start():void
    {
        this.init();
        Editor.modelPanel.hide();
        Editor.modelPopup.hide();

        Editor.propertyPanel.hide();
        Editor.propertyPopup.hide();

        Messenger.onReceive('add_model', Editor.showAddModelPopup);
        Messenger.onReceive('edit_model', Editor.showEditModelPanel);

        Messenger.onReceive('add_property', Editor.showAddPropertyPopup);
        Messenger.onReceive('edit_property', Editor.showEditPropertyPanel);
    }
    private static getAllPanels():IWidget[]
    {
        return [Editor.modelPanel, Editor.modelPopup, Editor.propertyPanel, Editor.propertyPopup];
    }

    static activePanel(newActivePanel:IWidget)
    {
        const aAllViews = Editor.getAllPanels();
        for(let i = 0; i < aAllViews.length; i++)
        {
            aAllViews[i].hide(true);
        }
        console.log('Editor.activePanel.newActivePanel', newActivePanel);
        newActivePanel.show();
    }
    static showEditPropertyPanel(message : Message<IMessageContentGenerics>):void
    {
        console.log('Editor.showEditPropertyPanel.message', message);
        Editor.activePanel(Editor.propertyPanel);
        Editor.propertyPanel.setData(message.content);

    }
    static showEditModelPanel(message : Message<IMessageContentGenerics>):void
    {
        console.log('Editor.showEditModelPanel.message', message);
        Editor.activePanel(Editor.modelPanel);
        Editor.modelPanel.setData(message.content);
    }
    static showAddPropertyPopup(message : Message<IMessageContentGenerics>):void
    {
        console.log('Editor.showAddPropertyPopup', message.content);
        Editor.activePanel(Editor.propertyPopup);
        Editor.propertyPopup.setData(message.content);
    }
    static showAddModelPopup():void
    {
        console.log('Editor.showAddModelPopup');
        Editor.activePanel(Editor.modelPopup);
    }
    static onPropertyDeleted(data:Message<IMessageContentGenerics>):void
    {
        console.log('Editor.onPropertyDeleted', data);
    }
    static onPropertySaved(data:Message<IMessageContentGenerics>):void
    {
        console.log('Editor.onPropertySaved', data);
    }
    static onPropertyCreated(data:Message<IMessageContentGenerics>):void
    {
        console.log('Editor.onPropertyCreated', data);

        console.log('Editor.onPropertyCreated', 'Editor.propertyPopup.hide()');
        Editor.propertyPopup.hide();

        Messenger.send('property_added', data.content);

        console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.show()');
        Editor.propertyPanel.show();

        console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.setData(data.content);', data.content);
        Editor.propertyPanel.setData(data.content);
    }
    static onModelCreated(data:Message<IMessageContentGenerics>):void
    {
        console.log('Editor.onModelCreated', data);

        Messenger.send('model_added', data);
        Editor.modelPopup.hide();
        Editor.modelPanel.show();
        Editor.modelPanel.setData(data.content);
    }

}
