/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>

import {IWidget} from "../contracts/IWidget";
import {EditPropertyForm} from "../model/PropertyForm";
import {AbstractWidget} from "../contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../model/Template";
import {Button} from "../model/Button";
import {Ajax} from "../Ajax";
import {ErrorBlock} from "../model/ErrorBlock";
import {IMessageContentGenerics, Messenger} from "../model/Messenger";
import {EventHandler} from "../../EventHandler";
import {DeleteProperty} from "../actions/DeleteProperty";
import {EventStack} from "../../EventStack";
import {BaseProps, BaseState, IEvent} from "../../event/IEvent";
import {AbstractEvent} from "../../event/AbstractEvent";

export class PropertyPanel extends AbstractWidget implements IWidget{

    selector : string = '#edit_property_panel';
    template : string;
    form : EditPropertyForm;

    confirm_delete_button:Button;
    delete_button:Button;
    create_button:Button;

    private editor : Editor;
    constructor(editor : Editor)
    {
        super();
        this.editor = editor;
        this.type = TypeType.panel;
        this.subject = SubjectType.property;
        this.form = new EditPropertyForm();
        Template.getForm(this);
    }
    bindEvents():this
    {
        return this;
    }
    show(): this
    {
        console.log('PropertyPanel.show()');

        super.show();
        this.bindEvents();
        this.form = new EditPropertyForm();

        console.log('PropertyPanel.show() -> bind-click-confirm-delete');
        this.confirm_delete_button = new Button('#fld_confirm_delete_button', this.confirmDelete, this);

        console.log('PropertyPanel.show() -> bind-click-delete');
        this.delete_button = new Button('#fld_delete_property', this.delete, this);

        console.log('PropertyPanel.show() -> bind-click-save');
        this.create_button = new Button('#fld_save_property', this.save, this);

        return this;
    }
    setData(data:  IMessageContentGenerics): this {
        console.log('PropertyPanel.setData', data);

        if(typeof data.table_name != 'undefined')
        {
            console.log("\t", 'this.form.table_name = ' + data.table_name);
            this.form.table_name = data.table_name;
        }
        super.setData(data);
        return this;
    }
    private save = (event : JQuery.ClickEvent, selfClass : AbstractWidget) => {
        console.log('PropertyPanel.save()');
        event.preventDefault();
        console.log("Use error_block");
        console.log('this', this);
        let error_block : ErrorBlock   = new ErrorBlock(this);
        let oData = {
            '_do': 'CreateProperty',
            'data': selfClass.form.getData()
        };
        $.post(window.location.href, oData, (data) => {

            if (data.errors)
            {
                error_block.show(data);
            }
            else
            {
                Editor.onPropertyCreated({content : data, topic : 'property'});
            }

        }, 'json');

    };

    private confirmDelete = (event : JQuery.ClickEvent, propertyPanel : PropertyPanel) => {

        const deleteEvents  = new EventStack<IEvent<BaseProps, BaseState>, AbstractEvent<BaseProps, BaseState>>();

        deleteEvents.register(new DeleteProperty(), {endpoint : "/"}, {data : propertyPanel.form.getData()});
/*
        const statusState  : StatusState = {
            title : "Are you sure?",
            message : "This will delete the column and all the data in it, are you sure?",
            elementId : "confirm_sure",
            StatusMessageColor: StatusMessageColors.warning,
            buttons : [
                {
                    label : "Yes",
                    title : "Yes i am sure",
                    type : StatusMessageColors.warning,
                    action : deleteEvents
                }
            ]
        };
    */

        // EventHandler.trigger(new StatusMessage(), {endpoint : '/'}, statusState);

        // StatusMessage.warning("Sure?", "Are you sure?", "sure_delete_property");
        console.log('PropertyPanel.delete()');
        event.preventDefault();

        Ajax('DeleteProperty', this, Editor.onPropertyDeleted);
        Messenger.send('property_deleted', this.form.getData());
    };
    private delete = (event : JQuery.ClickEvent, propertyPanel : PropertyPanel) => {
        console.log('PropertyPanel.delete()');
        console.log("\t", "propertyPanel", propertyPanel);
        console.log("\t", "propertyPanel.form", propertyPanel.form);
        console.log("\t", "propertyPanel.form.fields", propertyPanel.form.fields);
        console.log("\t", "propertyPanel.form.syncData()", propertyPanel.form.getData());

        EventHandler.trigger(new DeleteProperty(), {endpoint : ""}, {data : propertyPanel.form.getData(), widget  : propertyPanel});
        event.preventDefault();
    };


}
