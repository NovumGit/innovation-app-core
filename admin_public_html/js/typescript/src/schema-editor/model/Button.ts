/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>

import {AbstractWidget} from "../contracts/AbstractWidget";
import {IWidget} from "../contracts/IWidget";

type Callback = (event:JQuery.ClickEvent, widget : AbstractWidget) => void;

export class Button {

    private button: JQuery;
    private widget: IWidget;

    constructor(selector : string, callback : Callback, thisWidget : AbstractWidget) {

        this.button =  $(selector);
        this.button.on('click', (event:JQuery.ClickEvent) => {
            callback(event, thisWidget);
        });
        this.widget = thisWidget;
    }
}
