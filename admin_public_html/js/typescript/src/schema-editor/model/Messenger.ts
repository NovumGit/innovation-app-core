
export class Message<T> {
    topic: string;
    content: T
}

export interface IMessageContentGenerics {
    [name: string]: string
}

export class MessageContentGenerics implements IMessageContentGenerics
{
    [name: string]: string

}

interface MessageHandler {
    handle(message : Message<IMessageContentGenerics>):void;
}
export type MessageHandlerCallback<T> = (message : Message<T>) => void;

class MessageHandler {

    private callback: MessageHandlerCallback<IMessageContentGenerics>;
    topic : string;

    constructor(topic : string, callback : MessageHandlerCallback<IMessageContentGenerics>)
    {
        this.topic = topic;
        this.callback = callback;

    }
    handle(message : Message<IMessageContentGenerics>):void
    {
        this.callback(message);
    }

}
interface RawMessage {
    topic:string,
    data:IMessageContentGenerics
}

class MessageDispenser {
    private static aHandlers : MessageHandler[] = [];

    public static addListener(handler : MessageHandler)
    {
        MessageDispenser.aHandlers.push(handler);
    }
    public static distribute(rawMessage : RawMessage)
    {
        let message : Message<IMessageContentGenerics>;
        for (let i= 0; i < MessageDispenser.aHandlers.length; i++)
        {
            const handler = MessageDispenser.aHandlers[i];

            message = new Message<IMessageContentGenerics>();
            message.topic = rawMessage.topic;
            message.content = rawMessage.data;

            if(handler.topic == message.topic)
            {
                handler.handle(message);
            }
        }
    }
}

export class Messenger {

    private static isListening: boolean;

    private static startListener() {
        Messenger.isListening = true;
        console.log('Start listening');

        let callback = (messageEvent: MessageEvent) => {

            if (typeof messageEvent.data !== "undefined") {
                try
                {
                    console.log('type', typeof messageEvent.data);
                    if (typeof messageEvent.data == 'string') {

                        let rawMessage : RawMessage = JSON.parse(messageEvent.data);
                        MessageDispenser.distribute(rawMessage);
                    }
                }
                catch (e)
                {
                    console.error("Could not parse message" + messageEvent.data);
                    console.error(e.toString());
                }
            }
        };
        console.log('Adding EventListener');
        window.addEventListener('message', callback);
    }

    static send(topic: string, content: any) {

        console.log('Messenger.send(topic, content)', topic, content);
        let oMessage = {
            topic: topic,
            content: content
        };

        const oErdEditor = document.getElementById('erd_editor_window');
        (<HTMLIFrameElement> oErdEditor).contentWindow.postMessage(JSON.stringify(oMessage), '*');
    }

    static onReceive(topic:string, handler: MessageHandlerCallback<IMessageContentGenerics>) {

        if (!Messenger.isListening) {
            console.log('Startig EventListener');
            Messenger.startListener();
            console.log('EventListener started');
        }

        console.log('Add event listener for: ' + topic);
        const messageHandler = new MessageHandler(topic, handler);
        MessageDispenser.addListener(messageHandler);

    }
}
