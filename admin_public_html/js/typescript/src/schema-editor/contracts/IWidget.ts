import {IForm} from "./Form";
import {SubjectType, TypeType} from "../model/Template";
import {IMessageContentGenerics} from "../model/Messenger";

export interface IWidget {

    form : IForm;
    selector : string;
    template : string;
    type : TypeType;
    subject : SubjectType;
    bindEvents():this;
    hide(immediate : boolean):this;
    show():this;

    getPanel():JQuery;

    setData(data:  IMessageContentGenerics): this;
}
