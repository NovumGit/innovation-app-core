$('.sortable_nav').sortable({
    stop : function(event, ui)
    {

        var tabOrder = [];
        $('li', this).each(function(i, e){
            if(typeof $(e).data('tab_id') !== 'undefined')
            {
                tabOrder.push($(e).data('tab_id'));
            }
        });

        $.post(window.location, {_do : 'ChangeTabOrder', tab_order: tabOrder});
    }
});


