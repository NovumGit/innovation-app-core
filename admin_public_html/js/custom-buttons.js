let oTableForm = $('#frm_table_form');
let oCustomButtons = $('.custom_button');

oCustomButtons.click(function(e)
{
    let _doField = $('#fld_do');

    $('#fld_button_id').val($(this).val());
    e.preventDefault();
    _doField.val('TriggerButtonEvent');
    oTableForm.submit();

    // Terug zetten, bij download van een betstand vind geen redirect plaats.
    _doField.val('');
});

let oTriggerForm = $('.trigger_form');

oTriggerForm.click(function () {

    let set_do = $(this).data('set_do');
    if(set_do)
    {
        $('#fld_do').val(set_do);
    }
    $('#' + $(this).data('form_id')).submit();
});

let oCheckboxes = $('.multifunctional_checkbox');
oCheckboxes.change(function(e)
{
    e.preventDefault();

    if($('.multifunctional_checkbox:checked').length > 0)
    {
        oCustomButtons.show(500);
    }
    else
    {
        oCustomButtons.hide(500);
    }
});

let oToggleCheckboxButton = $('.toggle_check');
oToggleCheckboxButton.click(function(e){
    e.preventDefault();
    let aCheckboxes = $('.multifunctional_checkbox');
    aCheckboxes.each(function(i, e){
        $(e).prop('checked', !$(e).is(':checked'));
    });

    if($('.multifunctional_checkbox:checked').length > 0)
    {
        oCustomButtons.show(500);
    }
    else
    {
        oCustomButtons.hide(500);
    }

});
