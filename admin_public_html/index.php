<?php

use Core\ControllerFactory;
use Core\Layout;
use Core\StatusMessage;
use Core\StatusModal;
use Core\User;
use Exception\NuiCartException;
use Model\Setting\MasterTable\LanguageQuery;
use Core\Translate;
use Core\Config;
use Propel\Runtime\ActiveQuery\Criteria;
use Model\Setting\MasterTable\RoleQuery;
use Model\Logging\Except_log;
use Core\UserRegistry;
use Core\Utils;

ini_set('display_errors', 1);
error_reporting(E_ALL);

$sAutoLoadFile = 'vendor/autoload.php';
while (true)
{
    if(!file_exists($sAutoLoadFile))
    {
        $sAutoLoadFile = '../' . $sAutoLoadFile;
        continue;
    }
    break;
}
$oLoader =  require $sAutoLoadFile;

$aPossibleEnvFiles = [
    '.' . $_SERVER['HTTP_HOST'],
    '.' . $_SERVER['SYSTEM_ID']
];
$dotenv = Dotenv\Dotenv::createImmutable('../env/', $aPossibleEnvFiles);
$dotenv->load();

if(!Utils::requestIsSSL() && !isset($_SERVER['IS_DEVEL']))
{
	// Always force SSL
	header('Location: https://' .$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	exit();
}

$aHeaders = [
    '#\.js$#' => 'text/javascript',
    '#\.css$#' => 'text/css',
    '#\.ts$#' => 'application/x-typescript',
    '#\.tsx$#' => 'application/x-typescript',
];
foreach ($aHeaders as $sRegex => $sHeader)
{
    if(preg_match($sRegex, $_SERVER['REQUEST_URI']))
    {
        header("Content-type: $sHeader", true);
        readfile('..'.$_SERVER['REQUEST_URI']);
        exit();
    }

}


// For some reason _GET variables where gone, perhaps overwritten by htaccess?
$aGetVars = [];
if(strpos($_SERVER['REQUEST_URI'], '?'))
{
    $sGetVarString = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?') + 1, strlen($_SERVER['REQUEST_URI']) - strpos($_SERVER['REQUEST_URI'], '?'));
    parse_str($sGetVarString, $aGetVars);
}

$_GET = array_merge($aGetVars, $_GET);
$_REQUEST = array_merge($aGetVars, $_REQUEST);


// De Language wordt opgeslagen in een sessie en moet daarom geinclude worden voordat de sessie start.
// IVM incomplete object error.
$oLanguageInclude = new \Model\Setting\MasterTable\Language();
session_start();

if(isset($_GET['page']) && $_GET['page'] == 'tamtam')
{
    // Session keepalive feature / heartbeat.
    exit(json_encode(['ok' => true, 'status' => 'session still alive!']));
}

$sCodeRoot = dirname($_SERVER['DOCUMENT_ROOT']);



\Core\Cfg::set(require '../config/' . Config::getCustom() . '/config.php');

if(isset($_SERVER['IS_DEVEL']))
{
    ini_set('display_errors', 1);
}
else if($_SERVER['REMOTE_ADDR'] == '86.91.142.93')
{
    ini_set('display_errors', 1);
}
else
{
    ini_set('display_errors', 0);

    // A user-defined error handler function
    function error_handler($iErrorNumber, $sErrorString, $sErrorFile, $sErrorLine) {
        Except_log::registerError($iErrorNumber, $sErrorString, $sErrorFile, $sErrorLine);
    }
    // Set user-defined errr handler function
    set_error_handler("error_handler");
}
$aConfigFiles = [Utils::makePath($sCodeRoot, 'config', Config::getCustom(), 'propel', 'config.php')];
foreach($aConfigFiles as $sConfigFile)
{
    if(file_exists($sConfigFile))
    {
        require_once $sConfigFile;
    }
    else
    {
        throw new LogicException("Configfile $sConfigFile bestaat niet.");
    }
}
try
{
    $oControllerFactory = new ControllerFactory();
    $oController = $oControllerFactory->getController($_GET, $_POST);

    if(isset($_REQUEST['_do']) && !empty($_REQUEST['_do']))
    {
        $sDoBeforeMethod = 'do'.$_REQUEST['_do'];

        if(method_exists($oController, $sDoBeforeMethod) || method_exists($oController, '__call'))
        {
            $oController->$sDoBeforeMethod();
            if(preg_match('/^doNoLogin/', $_REQUEST['_do']))
            {
                // This method may be runned without a signed in user but we prevent anything else from happening after that.
                exit();
            }
        }
        else
        {
            if($oController instanceof AdminModules\Login\Controller)
            {
                $oController->redirect('/');
            }
            throw new LogicException("Attempt to do _do method . ".get_class($oController)." -> $sDoBeforeMethod but does not exist.");
        }
    }

    $aViewData = $oController->run();

    $aViewData['status_messages'] = null;
    if(Core\StatusMessage::hasStatusMessages())
    {
        $aViewData['status_messages'] = StatusMessage::getClearStatusMessages();
    }

    $aViewData['status_modals'] = null;
    if(Core\StatusModal::hasStatusMessages())
    {
        $aViewData['status_modals'] = StatusModal::getClearStatusMessages();
    }

    $aViewData['is_signed_in'] = User::isSignedIn();
    $aViewData['current_locale_flag']  = Translate::getCurrentLocale('FlagIconPath');
    $aViewData['languages']  = LanguageQuery::create()->filterByIsEnabledCms(true)->orderByDescription()->find();

    if(isset($_SESSION['event_javascript_to_run']))
    {
        $aViewData['event_javascript_to_run'] = $_SESSION['event_javascript_to_run'];
        $_SESSION['event_javascript_to_run'] = null;
    }

	$aViewData['menu_size'] = UserRegistry::get('menu_size') == 'minimal' ? 'sb-l-m' : null;
    $aViewData['current_role'] = User::getRole();

    if(User::isSignedIn() && User::getOriginalRole()->getName() == 'Admin')
    {
        $aViewData['all_roles'] = RoleQuery::create()->orderByName()->find();
    }
    else if(User::isSignedIn() && User::getMember()->getCanChangeRoles())
    {
        $oRoleQuery = RoleQuery::create();
        $oRoleQuery->filterByName('Admin', Criteria::NOT_EQUAL);
        $oRoleQuery->orderByName();
        $aViewData['all_roles'] = $oRoleQuery->find();
    }

//    /custom/{{ magic_arguments.custom }}/logo-left-top.png


    $aViewData['show_navbar_top'] = Layout::isNavTopVisible();
    $aViewData['show_sidebar_left'] = Layout::isSidebarLeftVisible();


    echo $oController->parse('layout.twig', $aViewData);


}catch (NuiCartException $e)
{
    Except_log::register($e, true);
}


