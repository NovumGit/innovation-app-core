<?php

use Model\Crm\CustomerAddressType;
use Model\Crm\CustomerAddressTypeQuery;
use Model\Crm\CustomerType;
use Model\Crm\CustomerTypeQuery;
use Model\Module\Module;
use Model\Module\ModuleQuery;
use Model\Module\ModuleRole;
use Model\Module\ModuleRoleQuery;
use Model\Setting\CrudManager\FilterDatatype;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\FilterOperator;
use Model\Setting\CrudManager\FilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\LegalForm;
use Model\Setting\MasterTable\LegalFormQuery;
use Model\Setting\MasterTable\LegalFormTranslation;
use Model\Setting\MasterTable\LegalFormTranslationQuery;
use Model\Setting\MasterTable\PayTerm;
use Model\Setting\MasterTable\PayTermQuery;
use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\ShippingZone;
use Model\Setting\MasterTable\ShippingZoneQuery;
use Model\Setting\MasterTable\UsaState;
use Model\Setting\MasterTable\UsaStateQuery;
use Model\Setting\MasterTable\Vat;
use Model\Setting\MasterTable\VatQuery;
use Model\Setting\MasterTable\VatTranslation;
use Model\Setting\MasterTable\VatTranslationQuery;
use Propel\Runtime\Propel;

if (isset($argv[0]) && $argv[0] == '1_initial-data-loader.php') {
    exit('Je kan dit script niet rechtstreeks aanroepen, ga naar de custom map en roep daar de custom initial dataloader aan.' . PHP_EOL);
}

$oCon = Propel::getConnection();
$oCon->beginTransaction();

$aSaleOrderStatusses = [
    [
        'name'         => 'New',
        'code'         => 'new',
        'is_deletable' => 0,
    ],
    [
        'name'         => 'On hold',
        'code'         => 'on_hold',
        'is_deletable' => 0,
    ],
    [
        'name'         => 'Completed',
        'code'         => 'completed',
        'is_deletable' => 0,
    ],
    [
        'name'         => 'Wait payment',
        'code'         => 'wait_payment',
        'is_deletable' => 0,
    ],
];

foreach ($aSaleOrderStatusses as $aStatus) {
    $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneByCode($aStatus['code']);
    if (!$oSaleOrderStatus instanceof SaleOrderStatus) {
        $oSaleOrderStatus = new SaleOrderStatus();
        $oSaleOrderStatus->setCode($aStatus['code']);
    }
    $oSaleOrderStatus->setName($aStatus['name']);
    $oSaleOrderStatus->setIsDeletable(0);
    $oSaleOrderStatus->save();
}

$aLanguages = [];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Nederlands',
    'locale_code'        => 'nl_NL',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 1,
    'flag_icon'          => 'NL',
    'shop_url_prefix'    => 'nl',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'English',
    'locale_code'        => 'en_US',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 1,
    'is_default_webshop' => 0,
    'flag_icon'          => 'GB',
    'shop_url_prefix'    => 'en',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Français',
    'locale_code'        => 'fr_FR',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 0,
    'flag_icon'          => 'FR',
    'shop_url_prefix'    => 'fr',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Deutsch',
    'locale_code'        => 'de_DE',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 0,
    'flag_icon'          => 'DE',
    'shop_url_prefix'    => 'de',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Español',
    'locale_code'        => 'es_ES',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 0,
    'flag_icon'          => 'ES',
    'shop_url_prefix'    => 'es',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Português',
    'locale_code'        => 'pt_PT',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 0,
    'flag_icon'          => 'PT',
    'shop_url_prefix'    => 'pt',
];
$aLanguages[] = [
    'is_deleted'         => 0,
    'description'        => 'Italiano',
    'locale_code'        => 'it_IT',
    'is_enabled_cms'     => 1,
    'is_enabled_webshop' => 1,
    'is_default_cms'     => 0,
    'is_default_webshop' => 0,
    'flag_icon'          => 'IT',
    'shop_url_prefix'    => 'it',
];

foreach ($aLanguages as $aLanguage) {
    $oLanguage = LanguageQuery::create()->findOneByLocaleCode($aLanguage['locale_code']);
    if (!$oLanguage instanceof Language) {
        $oLanguage = new Language();
        $oLanguage->setLocaleCode($aLanguage['locale_code']);
    }
    $oLanguage->setItemDeleted($aLanguage['is_deleted']);
    $oLanguage->setDescription($aLanguage['description']);
    $oLanguage->setIsEnabledCms($aLanguage['is_enabled_cms']);
    $oLanguage->setIsEnabledWebshop($aLanguage['is_enabled_webshop']);
    $oLanguage->setIsDefaultCms($aLanguage['is_default_cms']);
    $oLanguage->setIsDefaultWebshop($aLanguage['is_default_webshop']);
    $oLanguage->setFlagIcon($aLanguage['flag_icon']);
    $oLanguage->setShopUrlPrefix($aLanguage['shop_url_prefix']);
    $oLanguage->save();
}

$aVatItems = [
    [
        'txt'         => '0% tarief',
        'perc'        => 0,
        'translation' => [
            'nl' => '0% tarief',
            'en' => '0% rate',
            'fr' => '0% taux',
            'de' => '0% rate',
            'es' => '0% tarifa',
            'pt' => '0% tarifa',
            'it' => '0% tasso',
        ],
    ],
    [
        'txt'         => '9% tarief',
        'perc'        => 9,
        'translation' => [
            'nl' => '9% tarief',
            'en' => '9% rate',
            'fr' => '9% taux',
            'de' => '9% rate',
            'es' => '9% tarifa',
            'pt' => '9% tarifa',
            'it' => '9% tasso',
        ],
    ],
    [
        'txt'         => '21% tarief',
        'perc'        => 21,
        'translation' => [
            'nl' => '21% tarief',
            'en' => '21% rate',
            'fr' => '21% taux',
            'de' => '21% rate',
            'es' => '21% tarifa',
            'pt' => '21% tarifa',
            'it' => '21% tasso',
        ],
    ],
];

foreach ($aVatItems as $aVatItem) {
    $oVat = VatQuery::create()->findOneByName($aVatItem['txt']);
    if (!$oVat instanceof Vat) {
        $oVat = new Vat();
        $oVat->setName($aVatItem['txt']);
        $oVat->setPercentage($aVatItem['perc']);
        $oVat->save();
    }
    foreach ($aVatItem['translation'] as $sShopUrlPrefix => $sLabel) {
        $oLanguage = LanguageQuery::create()->findOneByShopUrlPrefix($sShopUrlPrefix);
        $oVatTranslation = VatTranslationQuery::create()->filterByLanguageId($oLanguage->getId())->filterByVatId($oVat->getId())->findOne();
        if (!$oVatTranslation instanceof VatTranslation) {
            $oVatTranslation = new VatTranslation();
            $oVatTranslation->setLanguageId($oLanguage->getId());
            $oVatTranslation->setVatId($oVat->getId());
        }
        $oVatTranslation->setName($sLabel);
        $oVatTranslation->save();
    }
}

$aCustomerTypes = [
    [
        'description' => 'Persoon',
        'code'        => 'person',
    ],
    [
        'description' => 'Bedrijf',
        'code'        => 'company',
    ],
];

foreach ($aCustomerTypes as $aCustomerType) {
    $oCustomerTypeQuery = CustomerTypeQuery::create();
    $oCustomerType = $oCustomerTypeQuery->findOneByCode($aCustomerType['code']);
    if (!$oCustomerType instanceof CustomerType) {
        $oCustomerType = new CustomerType();
        $oCustomerType->setCode($aCustomerType['code']);
    }
    $oCustomerType->setDescription($aCustomerType['description']);
    $oCustomerType->save();
}
$aRoles = [
    1 => [
        'name'         => 'Gebruiker',
        'is_deletable' => true,
    ],
    2 => [
        'name'         => 'Admin',
        'is_deletable' => false,
    ],
];

foreach ($aRoles as $aRole) {
    $oRole = RoleQuery::create()->findOneByName($aRole['name']);

    if (!$oRole instanceof Role) {
        $oRole = new Role();
        $oRole->setName($aRole['name']);
    }
    $oRole->setIsDeletable($aRole['is_deletable']);
    $oRole->save();
}

$oHurahRole = RoleQuery::create()->findOneByName('Hurah beheerder');
$oSettingModule = ModuleQuery::create()->findOneByName('AdminModules\Setting\Config');

if ($oHurahRole instanceof Role && $oSettingModule instanceof Module) {
    $oModuleRole = ModuleRoleQuery::create()->filterByRoleId($oHurahRole->getId())->filterByModuleId($oSettingModule->getId())->findOne();

    if (!$oModuleRole instanceof ModuleRole) {
        $oModuleRole = new ModuleRole();
        $oModuleRole->setModuleId($oSettingModule->getId());
        $oModuleRole->setRoleId($oHurahRole->getId());
        $oModuleRole->setSorting(1);
        $oModuleRole->save();
    }
}

$aCountries = [
    [
        'NL',
        '123112',
        'NL',
        'NED',
        'The Netherlands',
        ' 0',
        '0',
        '0',
    ],

    [
        'EUR1',
        '7779',
        'WA',
        'WAL',
        'Wales',
        '0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '7780',
        'IR',
        'IRE',
        'Ireland',
        '0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '7781',
        'SC',
        'SCO',
        'Scotland',
        '0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '7782',
        'EN',
        'ENG',
        'England',
        '0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '40',
        'AT',
        'AUT',
        'Austria',
        ' 1',
        '1 ',
        '0',
    ],
    [
        'EUR1',
        '56',
        'BE',
        'BEL',
        'Belgium',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '208',
        'DK',
        'DNK',
        'Denmark',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '234',
        'FO',
        'FRO',
        'Faroe Islands',
        ' 0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '250',
        'FR',
        'FRA',
        'France',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '276',
        'DE',
        'DEU',
        'Germany',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '442',
        'LU',
        'LUX',
        'Luxembourg',
        ' 0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '304',
        'GL',
        'GRL',
        'Greenland',
        ' 0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '380',
        'IT',
        'ITA',
        'Italy',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '492',
        'MC',
        'MCO',
        'Monaco',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '752',
        'SE',
        'SWE',
        'Sweden',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '724',
        'ES',
        'ESP',
        'Spain',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '352',
        'IS',
        'ISL',
        'Iceland',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '428',
        'LV',
        'LVA',
        'Latvia',
        ' 0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '440',
        'LT',
        'LTU',
        'Lithuania',
        ' 0',
        '0',
        '0',
    ],
    [
        'EUR1',
        '438',
        'LI',
        'LIE',
        'Liechtenstein',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '470',
        'MT',
        'MLT',
        'Malta',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '499',
        'ME',
        'MNE',
        'Montenegro',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '616',
        'PL',
        'POL',
        'Poland',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '620',
        'PT',
        'PRT',
        'Portugal',
        ' 1',
        '0',
        '0',
    ],
    [
        'EUR1',
        '642',
        'RO',
        'ROU',
        'Romania',
        ' 1',
        '0',
        '0',
    ],

    [
        'EUR2',
        '688',
        'RS',
        'SRB',
        'Serbia',
        ' 0',
        '0',
        '1',
    ],
    [
        'EUR2',
        '703',
        'SK',
        'SVK',
        'Slovakia',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '705',
        'SI',
        'SVN',
        'Slovenia',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '756',
        'CH',
        'CHE',
        'Switzerland',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '6666',
        'TR',
        'TUR',
        'Turkey',
        '0',
        '0',
        '1',
    ],
    [
        'EUR2',
        '348',
        'HU',
        'HUN',
        'Hungary',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '300',
        'GR',
        'GRC',
        'Greece',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '246',
        'FI',
        'FIN',
        'Finland',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '203',
        'CZ',
        'CZE',
        'Czech Republic',
        ' 0',
        '0',
        '1',
    ],
    [
        'EUR2',
        '191',
        'HR',
        'HRV',
        'Croatia',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '0',
        'ES',
        'EST',
        'Estonia',
        ' 1',
        '0',
        '1',
    ],
    [
        'EUR2',
        '8',
        'AL',
        'ALB',
        'Albania',
        ' 0',
        '0',
        '1',
    ],
    [
        'EUR2',
        '70',
        'BA',
        'BIH',
        'Bosnia and Herzegovina',
        ' 0',
        '0',
        '1',
    ],
    [
        'EUR2',
        '100',
        'BG',
        'BGR',
        'Bulgaria',
        ' 1',
        '0',
        '1',
    ],

    [
        'WORLD',
        '12',
        'DZ',
        'DZA',
        'Algeria',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '20',
        'AD',
        'AND',
        'Andorra',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '24',
        'AO',
        'AGO',
        'Angola',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '31',
        'AZ',
        'AZE',
        'Azerbaijan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '32',
        'AR',
        'ARG',
        'Argentina',
        ' 1',
        '0 ',
        '1',
    ],
    [
        'WORLD',
        '44',
        'BS',
        'BHS',
        'Bahamas',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '48',
        'BH',
        'BHR',
        'Bahrain',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '51',
        'AM',
        'ARM',
        'Armenia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '52',
        'BB',
        'BRB',
        'Barbados',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '68',
        'BO',
        'BOL',
        'Bolivia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '72',
        'BW',
        'BWA',
        'Botswana',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '74',
        'BV',
        'BVT',
        'Bouvet Island',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '76',
        'BR',
        'BRA',
        'Brazil',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '84',
        'BZ',
        'BLZ',
        'Belize',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '90',
        'SB',
        'SLB',
        'Solomon Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '92',
        'VG',
        'VGB',
        'Virgin Islands, British',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '96',
        'BN',
        'BRN',
        'Brunei Darussalam',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '104',
        'MM',
        'MMR',
        'Myanmar',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '108',
        'BI',
        'BDI',
        'Burundi',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '112',
        'BY',
        'BLR',
        'Belarus',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '116',
        'KH',
        'KHM',
        'Cambodia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '120',
        'CM',
        'CMR',
        'Cameroon',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '122',
        'SL',
        'SCH',
        'Scotland',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '124',
        'CA',
        'CAN',
        'Canada',
        ' 0',
        '1',
        '1',
    ],
    [
        'WORLD',
        '132',
        'CV',
        'CPV',
        'Cape Verde',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '136',
        'KY',
        'CYM',
        'Cayman Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '148',
        'TD',
        'TCD',
        'Chad',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '152',
        'CL',
        'CHL',
        'Chile',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '162',
        'CX',
        'CXR',
        'Christmas Island',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '170',
        'CO',
        'COL',
        'Colombia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '174',
        'KM',
        'COM',
        'Comoros',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '175',
        'YT',
        'MYT',
        'Mayotte',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '178',
        'CG',
        'COG',
        'Congo',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '180',
        'CD',
        'COD',
        'Congo, Democratic Republic of the',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '184',
        'CK',
        'COK',
        'Cook Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '188',
        'CR',
        'CRI',
        'Costa Rica',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '192',
        'CU',
        'CUB',
        'Cuba',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '196',
        'CY',
        'CYP',
        'Cyprus',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '212',
        'DM',
        'DMA',
        'Dominica',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '218',
        'EC',
        'ECU',
        'Ecuador',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '222',
        'SV',
        'SLV',
        'El Salvador',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '238',
        'FK',
        'FLK',
        'Falkland Islands (Malvinas)',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '239',
        'GS',
        'SGS',
        'South Georgia and the South Sandwich Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '242',
        'FJ',
        'FJI',
        'Fiji',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '248',
        'AX',
        'ALA',
        'Aland Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '268',
        'GE',
        'GEO',
        'Georgia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '270',
        'GM',
        'GMB',
        'Gambia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '288',
        'GH',
        'GHA',
        'Ghana',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '292',
        'GI',
        'GIB',
        'Gibraltar',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '296',
        'KI',
        'KIR',
        'Kiribati',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '308',
        'GD',
        'GRD',
        'Grenada',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '312',
        'GP',
        'GLP',
        'Guadeloupe',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '316',
        'GU',
        'GUM',
        'Guam',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '320',
        'GT',
        'GTM',
        'Guatemala',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '324',
        'GN',
        'GIN',
        'Guinea',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '328',
        'GY',
        'GUY',
        'Guyana',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '332',
        'HT',
        'HTI',
        'Haiti',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '334',
        'HM',
        'HMD',
        'Heard Island and McDonald Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '336',
        'VA',
        'VAT',
        'Holy See (Vatican City State)',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '340',
        'HN',
        'HND',
        'Honduras',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '344',
        'HK',
        'HKG',
        'Hong Kong',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '356',
        'IN',
        'IND',
        'India',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '360',
        'ID',
        'IDN',
        'Indonesia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '372',
        'IE',
        'IRL',
        'Ireland',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '376',
        'IL',
        'ISR',
        'Israel',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '388',
        'JM',
        'JAM',
        'Jamaica',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '392',
        'JP',
        'JPN',
        'Japan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '398',
        'KZ',
        'KAZ',
        'Kazakhstan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '400',
        'JO',
        'JOR',
        'Jordan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '404',
        'KE',
        'KEN',
        'Kenya',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '410',
        'KR',
        'KOR',
        'Korea, Republic of',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '414',
        'KW',
        'KWT',
        'Kuwait',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '422',
        'LB',
        'LBN',
        'Lebanon',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '426',
        'LS',
        'LSO',
        'Lesotho',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '430',
        'LR',
        'LBR',
        'Liberia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '434',
        'LY',
        'LBY',
        'Libyan Arab Jamahiriya',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '446',
        'MO',
        'MAC',
        'Macao',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '450',
        'MG',
        'MDG',
        'Madagascar',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '454',
        'MW',
        'MWI',
        'Malawi',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '458',
        'MY',
        'MYS',
        'Malaysia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '462',
        'MV',
        'MDV',
        'Maldives',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '466',
        'ML',
        'MLI',
        'Mali',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '474',
        'MQ',
        'MTQ',
        'Martinique',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '478',
        'MR',
        'MRT',
        'Mauritania',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '480',
        'MU',
        'MUS',
        'Mauritius',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '484',
        'MX',
        'MEX',
        'Mexico',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '498',
        'MD',
        'MDA',
        'Moldova',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '500',
        'MS',
        'MSR',
        'Montserrat',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '504',
        'MA',
        'MAR',
        'Morocco',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '520',
        'NR',
        'NRU',
        'Nauru',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '524',
        'NP',
        'NPL',
        'Nepal',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '533',
        'AW',
        'ABW',
        'Aruba',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '540',
        'NC',
        'NCL',
        'New Caledonia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '548',
        'VU',
        'VUT',
        'Vanuatu',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '554',
        'NZ',
        'NZL',
        'New Zealand',
        ' 0',
        '1',
        '1',
    ],
    [
        'WORLD',
        '558',
        'NI',
        'NIC',
        'Nicaragua',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '562',
        'NE',
        'NER',
        'Niger',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '566',
        'NG',
        'NGA',
        'Nigeria',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '570',
        'NU',
        'NIU',
        'Niue',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '574',
        'NF',
        'NFK',
        'Norfolk Island',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '578',
        'NO',
        'NOR',
        'Norway',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '584',
        'MH',
        'MHL',
        'Marshall Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '585',
        'PW',
        'PLW',
        'Palau',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '586',
        'PK',
        'PAK',
        'Pakistan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '591',
        'PA',
        'PAN',
        'Panama',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '598',
        'PG',
        'PNG',
        'Papua New Guinea',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '600',
        'PY',
        'PRY',
        'Paraguay',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '604',
        'PE',
        'PER',
        'Peru',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '612',
        'PN',
        'PCN',
        'Pitcairn',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '624',
        'GW',
        'GNB',
        'Guinea-Bissau',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '626',
        'TL',
        'TLS',
        'Timor-Leste',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '630',
        'PR',
        'PRI',
        'Puerto Rico',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '643',
        'RU',
        'RUS',
        'Russian Federation',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '646',
        'RW',
        'RWA',
        'Rwanda',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '654',
        'SH',
        'SHN',
        'Saint Helena',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '659',
        'KN',
        'KNA',
        'Saint Kitts and Nevis',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '660',
        'AI',
        'AIA',
        'Anguilla',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '662',
        'LC',
        'LCA',
        'Saint Lucia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '670',
        'VC',
        'VCT',
        'Saint Vincent and the Grenadines',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '674',
        'SM',
        'SMR',
        'San Marino',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '678',
        'ST',
        'STP',
        'Sao Tome and Principe',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '686',
        'SN',
        'SEN',
        'Senegal',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '690',
        'SC',
        'SYC',
        'Seychelles',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '702',
        'SG',
        'SGP',
        'Singapore',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '706',
        'SO',
        'SOM',
        'Somalia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '716',
        'ZW',
        'ZWE',
        'Zimbabwe',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '732',
        'EH',
        'ESH',
        'Western Sahara',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '740',
        'SR',
        'SUR',
        'Suriname',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '744',
        'SJ',
        'SJM',
        'Svalbard and Jan Mayen',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '748',
        'SZ',
        'SWZ',
        'Swaziland',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '762',
        'TJ',
        'TJK',
        'Tajikistan',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '772',
        'TK',
        'TKL',
        'Tokelau',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '776',
        'TO',
        'TON',
        'Tonga',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '780',
        'TT',
        'TTO',
        'Trinidad and Tobago',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '788',
        'TN',
        'TUN',
        'Tunisia',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '796',
        'TC',
        'TCA',
        'Turks and Caicos Islands',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '798',
        'TV',
        'TUV',
        'Tuvalu',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '800',
        'UG',
        'UGA',
        'Uganda',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '804',
        'UA',
        'UKR',
        'Ukraine',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '807',
        'MK',
        'MKD',
        'Macedonia, the former Yugoslav Republic of',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '818',
        'EG',
        'EGY',
        'Egypt',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '826',
        'GB',
        'GBR',
        'England',
        ' 1',
        '0',
        '1',
    ],
    [
        'WORLD',
        '831',
        'GG',
        'GGY',
        'Guernsey',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '832',
        'JE',
        'JEY',
        'Jersey',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '833',
        'IM',
        'IMN',
        'Isle of Man',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '834',
        'TZ',
        'TZA',
        'Tanzania, United Republic of',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '850',
        'VI',
        'VIR',
        'Virgin Islands, U.S.',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '854',
        'BF',
        'BFA',
        'Burkina Faso',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '858',
        'UY',
        'URY',
        'Uruguay',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '860',
        'UZ',
        'UZB',
        'Uzbekistan',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '862',
        'VE',
        'VEN',
        'Venezuela',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '876',
        'WF',
        'WLF',
        'Wallis and Futuna',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '882',
        'WS',
        'WSM',
        'Samoa',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '887',
        'YE',
        'YEM',
        'Yemen',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '894',
        'ZM',
        'ZMB',
        'Zambia',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '895',
        'US',
        'USA',
        'United States of America',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '896',
        'AU',
        'AUS',
        'Australia',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '5555',
        'TH',
        'THA',
        'Thailand',
        '0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '7777',
        'SA',
        'SOU',
        'South Africa',
        ' 0',
        '0',
        '1',
    ],
    [
        'WORLD',
        '7778',
        'IR',
        'IRA',
        'Iran',
        '0',
        '0',
        '1',
    ],

];
$oCountry = CountryQuery::create()->findOneByName('Philippines');
if ($oCountry instanceof Country) {
    $oCountry->delete();
}

foreach ($aCountries as $aCountry) {
    [
        $sZone,
        $sCode,
        $sIso2,
        $sIso3,
        $sName,
        $sIsEu,
        $sStealthMandatory,
        $sStealthAdvised,
    ] = $aCountry;

    $oShippingZone = ShippingZoneQuery::create()->findOneByName($sZone);

    if (!$oShippingZone instanceof ShippingZone) {
        $oShippingZone = new ShippingZone();
        $oShippingZone->setName($sZone);
        $oShippingZone->save();
    }

    $oCountry = CountryQuery::create()->findOneByIso3($sIso3);

    if (!$oCountry instanceof Country) {
        $oCountry = new Country();
        $oCountry->setIso3($sIso3);
    }
    $oCountry->setIso2($sIso2);
    $oCountry->setName($sName);
    $oCountry->setShippingZoneId($oShippingZone->getId());
    $oCountry->setIsEu($sIsEu == '1' ? true : false);
    $oCountry->setStealthMandatory($sStealthMandatory == '1' ? true : false);
    $oCountry->setStealthAdvised($sStealthAdvised == '1' ? true : false);
    $oCountry->save();
}

$aLatLongData = [
    [
        "AD",
        "42.546245",
        "1.601554",
        "Andorra",
    ],
    [
        "AE",
        "23.424076",
        "53.847818",
        "United Arab Emirates",
    ],
    [
        "AF",
        "33.93911",
        "67.709953",
        "Afghanistan",
    ],
    [
        "AG",
        "17.060816",
        "-61.796428",
        "Antigua and Barbuda",
    ],
    [
        "AI",
        "18.220554",
        "-63.068615",
        "Anguilla",
    ],
    [
        "AL",
        "41.153332",
        "20.168331",
        "Albania",
    ],
    [
        "AM",
        "40.069099",
        "45.038189",
        "Armenia",
    ],
    [
        "AN",
        "12.226079",
        "-69.060087",
        "Netherlands Antilles",
    ],
    [
        "AO",
        "-11.202692",
        "17.873887",
        "Angola",
    ],
    [
        "AQ",
        "-75.250973",
        "-0.071389",
        "Antarctica",
    ],
    [
        "AR",
        "-38.416097",
        "-63.616672",
        "Argentina",
    ],
    [
        "AS",
        "-14.270972",
        "-170.132217",
        "American Samoa",
    ],
    [
        "AT",
        "47.516231",
        "14.550072",
        "Austria",
    ],
    [
        "AU",
        "-25.274398",
        "133.775136",
        "Australia",
    ],
    [
        "AW",
        "12.52111",
        "-69.968338",
        "Aruba",
    ],
    [
        "AZ",
        "40.143105",
        "47.576927",
        "Azerbaijan",
    ],
    [
        "BA",
        "43.915886",
        "17.679076",
        "Bosnia and Herzegovina",
    ],
    [
        "BB",
        "13.193887",
        "-59.543198",
        "Barbados",
    ],
    [
        "BD",
        "23.684994",
        "90.356331",
        "Bangladesh",
    ],
    [
        "BE",
        "50.503887",
        "4.469936",
        "Belgium",
    ],
    [
        "BF",
        "12.238333",
        "-1.561593",
        "Burkina Faso",
    ],
    [
        "BG",
        "42.733883",
        "25.48583",
        "Bulgaria",
    ],
    [
        "BH",
        "25.930414",
        "50.637772",
        "Bahrain",
    ],
    [
        "BI",
        "-3.373056",
        "29.918886",
        "Burundi",
    ],
    [
        "BJ",
        "9.30769",
        "2.315834",
        "Benin",
    ],
    [
        "BM",
        "32.321384",
        "-64.75737",
        "Bermuda",
    ],
    [
        "BN",
        "4.535277",
        "114.727669",
        "Brunei",
    ],
    [
        "BO",
        "-16.290154",
        "-63.588653",
        "Bolivia",
    ],
    [
        "BR",
        "-14.235004",
        "-51.92528",
        "Brazil",
    ],
    [
        "BS",
        "25.03428",
        "-77.39628",
        "Bahamas",
    ],
    [
        "BT",
        "27.514162",
        "90.433601",
        "Bhutan",
    ],
    [
        "BV",
        "-54.423199",
        "3.413194",
        "Bouvet Island",
    ],
    [
        "BW",
        "-22.328474",
        "24.684866",
        "Botswana",
    ],
    [
        "BY",
        "53.709807",
        "27.953389",
        "Belarus",
    ],
    [
        "BZ",
        "17.189877",
        "-88.49765",
        "Belize",
    ],
    [
        "CA",
        "56.130366",
        "-106.346771",
        "Canada",
    ],
    [
        "CC",
        "-12.164165",
        "96.870956",
        "Cocos [Keeling] Islands",
    ],
    [
        "CD",
        "-4.038333",
        "21.758664",
        "Congo [DRC]",
    ],
    [
        "CF",
        "6.611111",
        "20.939444",
        "Central African Republic",
    ],
    [
        "CG",
        "-0.228021",
        "15.827659",
        "Congo [Republic]",
    ],
    [
        "CH",
        "46.818188",
        "8.227512",
        "Switzerland",
    ],
    [
        "CI",
        "7.539989",
        "-5.54708",
        "Côte d'Ivoire",
    ],
    [
        "CK",
        "-21.236736",
        "-159.777671",
        "Cook Islands",
    ],
    [
        "CL",
        "-35.675147",
        "-71.542969",
        "Chile",
    ],
    [
        "CM",
        "7.369722",
        "12.354722",
        "Cameroon",
    ],
    [
        "CN",
        "35.86166",
        "104.195397",
        "China",
    ],
    [
        "CO",
        "4.570868",
        "-74.297333",
        "Colombia",
    ],
    [
        "CR",
        "9.748917",
        "-83.753428",
        "Costa Rica",
    ],
    [
        "CU",
        "21.521757",
        "-77.781167",
        "Cuba",
    ],
    [
        "CV",
        "16.002082",
        "-24.013197",
        "Cape Verde",
    ],
    [
        "CX",
        "-10.447525",
        "105.690449",
        "Christmas Island",
    ],
    [
        "CY",
        "35.126413",
        "33.429859",
        "Cyprus",
    ],
    [
        "CZ",
        "49.817492",
        "15.472962",
        "Czech Republic",
    ],
    [
        "DE",
        "51.165691",
        "10.451526",
        "Germany",
    ],
    [
        "DJ",
        "11.825138",
        "42.590275",
        "Djibouti",
    ],
    [
        "DK",
        "56.26392",
        "9.501785",
        "Denmark",
    ],
    [
        "DM",
        "15.414999",
        "-61.370976",
        "Dominica",
    ],
    [
        "DO",
        "18.735693",
        "-70.162651",
        "Dominican Republic",
    ],
    [
        "DZ",
        "28.033886",
        "1.659626",
        "Algeria",
    ],
    [
        "EC",
        "-1.831239",
        "-78.183406",
        "Ecuador",
    ],
    [
        "EE",
        "58.595272",
        "25.013607",
        "Estonia",
    ],
    [
        "EG",
        "26.820553",
        "30.802498",
        "Egypt",
    ],
    [
        "EH",
        "24.215527",
        "-12.885834",
        "Western Sahara",
    ],
    [
        "ER",
        "15.179384",
        "39.782334",
        "Eritrea",
    ],
    [
        "ES",
        "40.463667",
        "-3.74922",
        "Spain",
    ],
    [
        "ET",
        "9.145",
        "40.489673",
        "Ethiopia",
    ],
    [
        "FI",
        "61.92411",
        "25.748151",
        "Finland",
    ],
    [
        "FJ",
        "-16.578193",
        "179.414413",
        "Fiji",
    ],
    [
        "FK",
        "-51.796253",
        "-59.523613",
        "Falkland Islands [Islas Malvinas]",
    ],
    [
        "FM",
        "7.425554",
        "150.550812",
        "Micronesia",
    ],
    [
        "FO",
        "61.892635",
        "-6.911806",
        "Faroe Islands",
    ],
    [
        "FR",
        "46.227638",
        "2.213749",
        "France",
    ],
    [
        "GA",
        "-0.803689",
        "11.609444",
        "Gabon",
    ],
    [
        "GB",
        "55.378051",
        "-3.435973",
        "United Kingdom",
    ],
    [
        "GD",
        "12.262776",
        "-61.604171",
        "Grenada",
    ],
    [
        "GE",
        "42.315407",
        "43.356892",
        "Georgia",
    ],
    [
        "GF",
        "3.933889",
        "-53.125782",
        "French Guiana",
    ],
    [
        "GG",
        "49.465691",
        "-2.585278",
        "Guernsey",
    ],
    [
        "GH",
        "7.946527",
        "-1.023194",
        "Ghana",
    ],
    [
        "GI",
        "36.137741",
        "-5.345374",
        "Gibraltar",
    ],
    [
        "GL",
        "71.706936",
        "-42.604303",
        "Greenland",
    ],
    [
        "GM",
        "13.443182",
        "-15.310139",
        "Gambia",
    ],
    [
        "GN",
        "9.945587",
        "-9.696645",
        "Guinea",
    ],
    [
        "GP",
        "16.995971",
        "-62.067641",
        "Guadeloupe",
    ],
    [
        "GQ",
        "1.650801",
        "10.267895",
        "Equatorial Guinea",
    ],
    [
        "GR",
        "39.074208",
        "21.824312",
        "Greece",
    ],
    [
        "GS",
        "-54.429579",
        "-36.587909",
        "South Georgia and the South Sandwich Islands",
    ],
    [
        "GT",
        "15.783471",
        "-90.230759",
        "Guatemala",
    ],
    [
        "GU",
        "13.444304",
        "144.793731",
        "Guam",
    ],
    [
        "GW",
        "11.803749",
        "-15.180413",
        "Guinea-Bissau",
    ],
    [
        "GY",
        "4.860416",
        "-58.93018",
        "Guyana",
    ],
    [
        "GZ",
        "31.354676",
        "34.308825",
        "Gaza Strip",
    ],
    [
        "HK",
        "22.396428",
        "114.109497",
        "Hong Kong",
    ],
    [
        "HM",
        "-53.08181",
        "73.504158",
        "Heard Island and McDonald Islands",
    ],
    [
        "HN",
        "15.199999",
        "-86.241905",
        "Honduras",
    ],
    [
        "HR",
        "45.1",
        "15.2",
        "Croatia",
    ],
    [
        "HT",
        "18.971187",
        "-72.285215",
        "Haiti",
    ],
    [
        "HU",
        "47.162494",
        "19.503304",
        "Hungary",
    ],
    [
        "ID",
        "-0.789275",
        "113.921327",
        "Indonesia",
    ],
    [
        "IE",
        "53.41291",
        "-8.24389",
        "Ireland",
    ],
    [
        "IL",
        "31.046051",
        "34.851612",
        "Israel",
    ],
    [
        "IM",
        "54.236107",
        "-4.548056",
        "Isle of Man",
    ],
    [
        "IN",
        "20.593684",
        "78.96288",
        "India",
    ],
    [
        "IO",
        "-6.343194",
        "71.876519",
        "British Indian Ocean Territory",
    ],
    [
        "IQ",
        "33.223191",
        "43.679291",
        "Iraq",
    ],
    [
        "IR",
        "32.427908",
        "53.688046",
        "Iran",
    ],
    [
        "IS",
        "64.963051",
        "-19.020835",
        "Iceland",
    ],
    [
        "IT",
        "41.87194",
        "12.56738",
        "Italy",
    ],
    [
        "JE",
        "49.214439",
        "-2.13125",
        "Jersey",
    ],
    [
        "JM",
        "18.109581",
        "-77.297508",
        "Jamaica",
    ],
    [
        "JO",
        "30.585164",
        "36.238414",
        "Jordan",
    ],
    [
        "JP",
        "36.204824",
        "138.252924",
        "Japan",
    ],
    [
        "KE",
        "-0.023559",
        "37.906193",
        "Kenya",
    ],
    [
        "KG",
        "41.20438",
        "74.766098",
        "Kyrgyzstan",
    ],
    [
        "KH",
        "12.565679",
        "104.990963",
        "Cambodia",
    ],
    [
        "KI",
        "-3.370417",
        "-168.734039",
        "Kiribati",
    ],
    [
        "KM",
        "-11.875001",
        "43.872219",
        "Comoros",
    ],
    [
        "KN",
        "17.357822",
        "-62.782998",
        "Saint Kitts and Nevis",
    ],
    [
        "KP",
        "40.339852",
        "127.510093",
        "North Korea",
    ],
    [
        "KR",
        "35.907757",
        "127.766922",
        "South Korea",
    ],
    [
        "KW",
        "29.31166",
        "47.481766",
        "Kuwait",
    ],
    [
        "KY",
        "19.513469",
        "-80.566956",
        "Cayman Islands",
    ],
    [
        "KZ",
        "48.019573",
        "66.923684",
        "Kazakhstan",
    ],
    [
        "LA",
        "19.85627",
        "102.495496",
        "Laos",
    ],
    [
        "LB",
        "33.854721",
        "35.862285",
        "Lebanon",
    ],
    [
        "LC",
        "13.909444",
        "-60.978893",
        "Saint Lucia",
    ],
    [
        "LI",
        "47.166",
        "9.555373",
        "Liechtenstein",
    ],
    [
        "LK",
        "7.873054",
        "80.771797",
        "Sri Lanka",
    ],
    [
        "LR",
        "6.428055",
        "-9.429499",
        "Liberia",
    ],
    [
        "LS",
        "-29.609988",
        "28.233608",
        "Lesotho",
    ],
    [
        "LT",
        "55.169438",
        "23.881275",
        "Lithuania",
    ],
    [
        "LU",
        "49.815273",
        "6.129583",
        "Luxembourg",
    ],
    [
        "LV",
        "56.879635",
        "24.603189",
        "Latvia",
    ],
    [
        "LY",
        "26.3351",
        "17.228331",
        "Libya",
    ],
    [
        "MA",
        "31.791702",
        "-7.09262",
        "Morocco",
    ],
    [
        "MC",
        "43.750298",
        "7.412841",
        "Monaco",
    ],
    [
        "MD",
        "47.411631",
        "28.369885",
        "Moldova",
    ],
    [
        "ME",
        "42.708678",
        "19.37439",
        "Montenegro",
    ],
    [
        "MG",
        "-18.766947",
        "46.869107",
        "Madagascar",
    ],
    [
        "MH",
        "7.131474",
        "171.184478",
        "Marshall Islands",
    ],
    [
        "MK",
        "41.608635",
        "21.745275",
        "Macedonia [FYROM]",
    ],
    [
        "ML",
        "17.570692",
        "-3.996166",
        "Mali",
    ],
    [
        "MM",
        "21.913965",
        "95.956223",
        "Myanmar [Burma]",
    ],
    [
        "MN",
        "46.862496",
        "103.846656",
        "Mongolia",
    ],
    [
        "MO",
        "22.198745",
        "113.543873",
        "Macau",
    ],
    [
        "MP",
        "17.33083",
        "145.38469",
        "Northern Mariana Islands",
    ],
    [
        "MQ",
        "14.641528",
        "-61.024174",
        "Martinique",
    ],
    [
        "MR",
        "21.00789",
        "-10.940835",
        "Mauritania",
    ],
    [
        "MS",
        "16.742498",
        "-62.187366",
        "Montserrat",
    ],
    [
        "MT",
        "35.937496",
        "14.375416",
        "Malta",
    ],
    [
        "MU",
        "-20.348404",
        "57.552152",
        "Mauritius",
    ],
    [
        "MV",
        "3.202778",
        "73.22068",
        "Maldives",
    ],
    [
        "MW",
        "-13.254308",
        "34.301525",
        "Malawi",
    ],
    [
        "MX",
        "23.634501",
        "-102.552784",
        "Mexico",
    ],
    [
        "MY",
        "4.210484",
        "101.975766",
        "Malaysia",
    ],
    [
        "MZ",
        "-18.665695",
        "35.529562",
        "Mozambique",
    ],
    [
        "NA",
        "-22.95764",
        "18.49041",
        "Namibia",
    ],
    [
        "NC",
        "-20.904305",
        "165.618042",
        "New Caledonia",
    ],
    [
        "NE",
        "17.607789",
        "8.081666",
        "Niger",
    ],
    [
        "NF",
        "-29.040835",
        "167.954712",
        "Norfolk Island",
    ],
    [
        "NG",
        "9.081999",
        "8.675277",
        "Nigeria",
    ],
    [
        "NI",
        "12.865416",
        "-85.207229",
        "Nicaragua",
    ],
    [
        "NL",
        "52.132633",
        "5.291266",
        "Netherlands",
    ],
    [
        "NO",
        "60.472024",
        "8.468946",
        "Norway",
    ],
    [
        "NP",
        "28.394857",
        "84.124008",
        "Nepal",
    ],
    [
        "NR",
        "-0.522778",
        "166.931503",
        "Nauru",
    ],
    [
        "NU",
        "-19.054445",
        "-169.867233",
        "Niue",
    ],
    [
        "NZ",
        "-40.900557",
        "174.885971",
        "New Zealand",
    ],
    [
        "OM",
        "21.512583",
        "55.923255",
        "Oman",
    ],
    [
        "PA",
        "8.537981",
        "-80.782127",
        "Panama",
    ],
    [
        "PE",
        "-9.189967",
        "-75.015152",
        "Peru",
    ],
    [
        "PF",
        "-17.679742",
        "-149.406843",
        "French Polynesia",
    ],
    [
        "PG",
        "-6.314993",
        "143.95555",
        "Papua New Guinea",
    ],
    [
        "PH",
        "12.879721",
        "121.774017",
        "Philippines",
    ],
    [
        "PK",
        "30.375321",
        "69.345116",
        "Pakistan",
    ],
    [
        "PL",
        "51.919438",
        "19.145136",
        "Poland",
    ],
    [
        "PM",
        "46.941936",
        "-56.27111",
        "Saint Pierre and Miquelon",
    ],
    [
        "PN",
        "-24.703615",
        "-127.439308",
        "Pitcairn Islands",
    ],
    [
        "PR",
        "18.220833",
        "-66.590149",
        "Puerto Rico",
    ],
    [
        "PS",
        "31.952162",
        "35.233154",
        "Palestinian Territories",
    ],
    [
        "PT",
        "39.399872",
        "-8.224454",
        "Portugal",
    ],
    [
        "PW",
        "7.51498",
        "134.58252",
        "Palau",
    ],
    [
        "PY",
        "-23.442503",
        "-58.443832",
        "Paraguay",
    ],
    [
        "QA",
        "25.354826",
        "51.183884",
        "Qatar",
    ],
    [
        "RE",
        "-21.115141",
        "55.536384",
        "Réunion",
    ],
    [
        "RO",
        "45.943161",
        "24.96676",
        "Romania",
    ],
    [
        "RS",
        "44.016521",
        "21.005859",
        "Serbia",
    ],
    [
        "RU",
        "61.52401",
        "105.318756",
        "Russia",
    ],
    [
        "RW",
        "-1.940278",
        "29.873888",
        "Rwanda",
    ],
    [
        "SA",
        "23.885942",
        "45.079162",
        "Saudi Arabia",
    ],
    [
        "SB",
        "-9.64571",
        "160.156194",
        "Solomon Islands",
    ],
    [
        "SC",
        "-4.679574",
        "55.491977",
        "Seychelles",
    ],
    [
        "SD",
        "12.862807",
        "30.217636",
        "Sudan",
    ],
    [
        "SE",
        "60.128161",
        "18.643501",
        "Sweden",
    ],
    [
        "SG",
        "1.352083",
        "103.819836",
        "Singapore",
    ],
    [
        "SH",
        "-24.143474",
        "-10.030696",
        "Saint Helena",
    ],
    [
        "SI",
        "46.151241",
        "14.995463",
        "Slovenia",
    ],
    [
        "SJ",
        "77.553604",
        "23.670272",
        "Svalbard and Jan Mayen",
    ],
    [
        "SK",
        "48.669026",
        "19.699024",
        "Slovakia",
    ],
    [
        "SL",
        "8.460555",
        "-11.779889",
        "Sierra Leone",
    ],
    [
        "SM",
        "43.94236",
        "12.457777",
        "San Marino",
    ],
    [
        "SN",
        "14.497401",
        "-14.452362",
        "Senegal",
    ],
    [
        "SO",
        "5.152149",
        "46.199616",
        "Somalia",
    ],
    [
        "SR",
        "3.919305",
        "-56.027783",
        "Suriname",
    ],
    [
        "ST",
        "0.18636",
        "6.613081",
        "São Tomé and Príncipe",
    ],
    [
        "SV",
        "13.794185",
        "-88.89653",
        "El Salvador",
    ],
    [
        "SY",
        "34.802075",
        "38.996815",
        "Syria",
    ],
    [
        "SZ",
        "-26.522503",
        "31.465866",
        "Swaziland",
    ],
    [
        "TC",
        "21.694025",
        "-71.797928",
        "Turks and Caicos Islands",
    ],
    [
        "TD",
        "15.454166",
        "18.732207",
        "Chad",
    ],
    [
        "TF",
        "-49.280366",
        "69.348557",
        "French Southern Territories",
    ],
    [
        "TG",
        "8.619543",
        "0.824782",
        "Togo",
    ],
    [
        "TH",
        "15.870032",
        "100.992541",
        "Thailand",
    ],
    [
        "TJ",
        "38.861034",
        "71.276093",
        "Tajikistan",
    ],
    [
        "TK",
        "-8.967363",
        "-171.855881",
        "Tokelau",
    ],
    [
        "TL",
        "-8.874217",
        "125.727539",
        "Timor-Leste",
    ],
    [
        "TM",
        "38.969719",
        "59.556278",
        "Turkmenistan",
    ],
    [
        "TN",
        "33.886917",
        "9.537499",
        "Tunisia",
    ],
    [
        "TO",
        "-21.178986",
        "-175.198242",
        "Tonga",
    ],
    [
        "TR",
        "38.963745",
        "35.243322",
        "Turkey",
    ],
    [
        "TT",
        "10.691803",
        "-61.222503",
        "Trinidad and Tobago",
    ],
    [
        "TV",
        "-7.109535",
        "177.64933",
        "Tuvalu",
    ],
    [
        "TW",
        "23.69781",
        "120.960515",
        "Taiwan",
    ],
    [
        "TZ",
        "-6.369028",
        "34.888822",
        "Tanzania",
    ],
    [
        "UA",
        "48.379433",
        "31.16558",
        "Ukraine",
    ],
    [
        "UG",
        "1.373333",
        "32.290275",
        "Uganda",
    ],
    [
        "UM",
        "",
        "",
        "U.S. Minor Outlying Islands",
    ],
    [
        "US",
        "37.09024",
        "-95.712891",
        "United States",
    ],
    [
        "UY",
        "-32.522779",
        "-55.765835",
        "Uruguay",
    ],
    [
        "UZ",
        "41.377491",
        "64.585262",
        "Uzbekistan",
    ],
    [
        "VA",
        "41.902916",
        "12.453389",
        "Vatican City",
    ],
    [
        "VC",
        "12.984305",
        "-61.287228",
        "Saint Vincent and the Grenadines",
    ],
    [
        "VE",
        "6.42375",
        "-66.58973",
        "Venezuela",
    ],
    [
        "VG",
        "18.420695",
        "-64.639968",
        "British Virgin Islands",
    ],
    [
        "VI",
        "18.335765",
        "-64.896335",
        "U.S. Virgin Islands",
    ],
    [
        "VN",
        "14.058324",
        "108.277199",
        "Vietnam",
    ],
    [
        "VU",
        "-15.376706",
        "166.959158",
        "Vanuatu",
    ],
    [
        "WF",
        "-13.768752",
        "-177.156097",
        "Wallis and Futuna",
    ],
    [
        "WS",
        "-13.759029",
        "-172.104629",
        "Samoa",
    ],
    [
        "XK",
        "42.602636",
        "20.902977",
        "Kosovo",
    ],
    [
        "YE",
        "15.552727",
        "48.516388",
        "Yemen",
    ],
    [
        "YT",
        "-12.8275",
        "45.166244",
        "Mayotte",
    ],
    [
        "ZA",
        "-30.559482",
        "22.937506",
        "South Africa",
    ],
    [
        "ZM",
        "-13.133897",
        "27.849332",
        "Zambia",
    ],
    [
        "ZW",
        "-19.015438",
        "29.154857",
        "Zimbabwe",
    ],
];

foreach ($aLatLongData as $aRow) {
    $sIso2 = $aRow[0];
    $sLat = $aRow[1];
    $sLong = $aRow[2];

    $oCountry = CountryQuery::create()->findOneByIso2($sIso2);
    if ($oCountry instanceof Country) {
        $oCountry->setLatitude($sLat);
        $oCountry->setLongitude($sLong);
        $oCountry->save();
    }
}

$aCustomerAddressTypes = [
    [
        'description' => 'Afleveradres',
        'code'        => 'delivery',
    ],
    [
        'description' => 'Factuuradres',
        'code'        => 'invoice',
    ],
    [
        'description' => 'Algemeen adres',
        'code'        => 'general',
    ],
];

foreach ($aCustomerAddressTypes as $aAddressType) {
    $oCustomerAddressTypeQuery = CustomerAddressTypeQuery::create();
    $oCustomerAddressType = $oCustomerAddressTypeQuery->findOneByCode($aAddressType['code']);

    if (!$oCustomerAddressType instanceof CustomerAddressType) {
        $oCustomerAddressType = new CustomerAddressType();
        $oCustomerAddressType->setCode($aAddressType['code']);
    }
    $oCustomerAddressType->setDescription($aAddressType['description']);
    $oCustomerAddressType->save();
}

$aUsaStates = [
    [
        "Alaska",
        "AK",
    ],
    [
        "Alabama",
        "AL",
    ],
    [
        "American Samoa",
        "AS",
    ],
    [
        "Arizona",
        "AZ",
    ],
    [
        "Arkansas",
        "AR",
    ],
    [
        "California",
        "CA",
    ],
    [
        "Colorado",
        "CO",
    ],
    [
        "Connecticut",
        "CT",
    ],
    [
        "Delaware",
        "DE",
    ],
    [
        "District of Columbia",
        "DC",
    ],
    [
        "Federated usa_states of Micronesia",
        "FM",
    ],
    [
        "Florida",
        "FL",
    ],
    [
        "Georgia",
        "GA",
    ],
    [
        "Guam",
        "GU",
    ],
    [
        "Hawaii",
        "HI",
    ],
    [
        "Idaho",
        "ID",
    ],
    [
        "Illinois",
        "IL",
    ],
    [
        "Indiana",
        "IN",
    ],
    [
        "Iowa",
        "IA",
    ],
    [
        "Kansas",
        "KS",
    ],
    [
        "Kentucky",
        "KY",
    ],
    [
        "Louisiana",
        "LA",
    ],
    [
        "Maine",
        "ME",
    ],
    [
        "Marshall Islands",
        "MH",
    ],
    [
        "Maryland",
        "MD",
    ],
    [
        "Massachusetts",
        "MA",
    ],
    [
        "Michigan",
        "MI",
    ],
    [
        "Minnesota",
        "MN",
    ],
    [
        "Mississippi",
        "MS",
    ],
    [
        "Missouri",
        "MO",
    ],
    [
        "Montana",
        "MT",
    ],
    [
        "Nebraska",
        "NE",
    ],
    [
        "Nevada",
        "NV",
    ],
    [
        "New Hampshire",
        "NH",
    ],
    [
        "New Jersey",
        "NJ",
    ],
    [
        "New Mexico",
        "NM",
    ],
    [
        "New York",
        "NY",
    ],
    [
        "North Carolina",
        "NC",
    ],
    [
        "North Dakota",
        "ND",
    ],
    [
        "Northern Mariana Islands",
        "MP",
    ],
    [
        "Ohio",
        "OH",
    ],
    [
        "Oklahoma",
        "OK",
    ],
    [
        "Oregon",
        "OR",
    ],
    [
        "Palau",
        "PW",
    ],
    [
        "Pennsylvania",
        "PA",
    ],
    [
        "Puerto Rico",
        "PR",
    ],
    [
        "Rhode Island",
        "RI",
    ],
    [
        "South Carolina",
        "SC",
    ],
    [
        "South Dakota",
        "SD",
    ],
    [
        "Tennessee",
        "TN",
    ],
    [
        "Texas",
        "TX",
    ],
    [
        "Utah",
        "UT",
    ],
    [
        "Vermont",
        "VT",
    ],
    [
        "Virgin Islands",
        "VI",
    ],
    [
        "Virginia",
        "VA",
    ],
    [
        "Washington",
        "WA",
    ],
    [
        "West Virginia",
        "WV",
    ],
    [
        "Wisconsin",
        "WI",
    ],
    [
        "Wyoming",
        "WY",
    ],
    [
        "Armed Forces Africa",
        "AE",
    ],
    [
        "Armed Forces Americas except Canada",
        "AA",
    ],
    [
        "Armed Forces Canada",
        "AE",
    ],
    [
        "Armed Forces Europe",
        "AE",
    ],
    [
        "Armed Forces Middle East",
        "AE",
    ],
    [
        "Armed Forces Pacific",
        "AP",
    ],
];

foreach ($aUsaStates as $aState) {
    $oUsaState = UsaStateQuery::create()->findOneByAbrev($aState[1]);
    if (!$oUsaState instanceof UsaState) {
        $oUsaState = new UsaState();
        $oUsaState->setAbrev($aState[1]);
    }
    $oUsaState->setName($aState[0]);
    $oUsaState->save();
}

$aPayterms = [];
$aPayterms[] = [
    'name'       => '14 dagen',
    'code'       => '14DAYS',
    'total_days' => '14',
    'period'     => 'DAY',
];

foreach ($aPayterms as $aPayterm) {
    $oPayTerm = PayTermQuery::create()->findOneByCode($aPayterm['code']);
    if (!$oPayTerm instanceof PayTerm) {
        $oPayTerm = new PayTerm();
        $oPayTerm->setCode($aPayterm['code']);
    }
    $oPayTerm->setName($aPayterm['name']);
    $oPayTerm->setTotalDays($aPayterm['total_days']);
    $oPayTerm->setPeriod('DAY');
    $oPayTerm->save();
}

$aVatLevels = [];
foreach ($aVatLevels as $aVatLevel) {
    $oVat = VatQuery::create()->findOneByName($aVatLevel['label']);
    if (!$oVat instanceof Vat) {
        $oVat = new Vat();
    }

    $oVat->setName($aVatLevel['label']);
    $oVat->setPercentage($aVatLevel['percentage']);
    $oVat->save();

    foreach ($aVatLevel['translations'] as $sLocaleCode => $sLocaleLabel) {
        $oLanguage = LanguageQuery::create()->findOneByLocaleCode($sLocaleCode);

        $oVatTranslation = VatTranslationQuery::create()->filterByVatId($oVat->getId())->filterByLanguageId($oLanguage->getId())->findOne();

        if (!$oVatTranslation instanceof VatTranslation) {
            $oVatTranslation = new VatTranslation();
            $oVatTranslation->setVatId($oVat->getId());
            $oVatTranslation->setLanguageId($oLanguage->getId());
        }
        $oVatTranslation->setName($sLocaleLabel);
        $oVatTranslation->save();
    }
}

$aDataTypes = [
    'string',
    'number',
    'boolean',
    'lookup',
    'date',
];

$aFilterOperators = [
    [
        "field"                => "equals",
        "description"          => "Is gelijk aan",
        "description_sentence" => "gelijk is aan",
        'datatypes'            => [
            'string',
            'number',
            'lookup',
            'date',
            'boolean',
        ],
    ],
    [
        "field"                => "not_equals",
        "description"          => "Is ongelijk aan",
        "description_sentence" => "ongelijk is aan",
        'datatypes'            => [
            'string',
            'number',
            'lookup',
            'date',
        ],
    ],
    [
        "field"                => "larger_then",
        "description"          => "Is groter dan",
        "description_sentence" => "groter is dan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "larger_then_or_equal",
        "description"          => "Is groter dan of gelijk aan",
        "description_sentence" => "groter dan of gelijk is aan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then",
        "description"          => "Is kleiner dan",
        "description_sentence" => "kleiner is dan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then_or_equal",
        "description"          => "Is kleiner dan of gelijk aan",
        "description_sentence" => "kleiner dan of gelijk is aan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then_or_equal_or_null",
        "description"          => "Is kleiner dan of gelijk aan of null",
        "description_sentence" => "kleiner dan of gelijk is aan",
        'datatypes'            => ['date'],
    ],
    [
        "field"                => "in",
        "description"          => "Komt voor in opsomming",
        "description_sentence" => "voorkomt in opsomming",
        'datatypes'            => [
            'string',
            'number',
        ],
    ],
    [
        "field"                => "not_in",
        "description"          => "Komt niet voor opsomming",
        "description_sentence" => "niet voorkomt in opsomming",
        'datatypes'            => [
            'string',
            'number',
        ],
    ],
    [
        "field"                => "starts_with",
        "description"          => "Begind met",
        "description_sentence" => "begind met",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "ends_on",
        "description"          => "Eindigt op",
        "description_sentence" => "eindigd op",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "contains_text",
        "description"          => "Bevat de tekst",
        "description_sentence" => "het volgende tekst fragment bevat",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "not_contains_text",
        "description"          => "Bevat niet tekst",
        "description_sentence" => "niet het volgende tekst fragment bevat",
        'datatypes'            => ['string'],
    ],
    [
        "field"       => "yes_no",
        "description" => "Is gelijk aan",
        'datatypes'   => ['boolean'],
    ],
    [
        "field"       => "is_null",
        "description" => "Is null",
        'datatypes'   => [
            'date',
            'string',
        ],
    ],
    [
        "field"       => "is_not_null",
        "description" => "Is niet null",
        'datatypes'   => [
            'date',
            'string',
        ],
    ],
];
foreach ($aDataTypes as $sDataType) {
    $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($sDataType);
    if (!$oFilterDataType instanceof FilterDatatype) {
        $oFilterDataType = new FilterDatatype();
        $oFilterDataType->setName($sDataType);
        $oFilterDataType->save();
    }
}
foreach ($aFilterOperators as $aFilterOperator) {
    $oFilterOperatorQuery = new FilterOperatorQuery();
    $oFilterOperatorQuery->filterByName($aFilterOperator['field']);
    $oFilterOperator = $oFilterOperatorQuery->findOne();
    if (!$oFilterOperator instanceof FilterOperator) {
        $oFilterOperator = new FilterOperator();
    }
    $oFilterOperator->setName($aFilterOperator['field']);
    $oFilterOperator->setDescription($aFilterOperator['description']);

    if (isset($aFilterOperator['description_sentence'])) {
        $oFilterOperator->setDescriptionSentence($aFilterOperator['description_sentence']);
    } else {
        $oFilterOperator->setDescriptionSentence('');
    }
    $oFilterOperator->save();
}

foreach ($aFilterOperators as $aFilterOperator) {
    $oFilterOperator = FilterOperatorQuery::create()->findOneByName($aFilterOperator['field']);
    foreach ($aFilterOperator['datatypes'] as $sDataType) {
        $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($sDataType);
        $oFilterOperatorDatatypeQuery = FilterOperatorDatatypeQuery::create();
        $oFilterOperatorDatatypeQuery->filterByFilterDatatypeId($oFilterDataType->getId());
        $oFilterOperatorDatatypeQuery->filterByFilterOperatorId($oFilterOperator->getId());
        $oFilterOperatorDatatype = $oFilterOperatorDatatypeQuery->findOne();
        if (!$oFilterOperatorDatatype instanceof FilterOperatorDatatype) {
            $oFilterOperatorDatatype = new FilterOperatorDatatype();
            $oFilterOperatorDatatype->setFilterDatatypeId($oFilterDataType->getId());
            $oFilterOperatorDatatype->setFilterOperatorId($oFilterOperator->getId());
            $oFilterOperatorDatatype->save();
        }
    }
}

$aJuridicalForms = [];
$aJuridicalForms[] = 'Eenmanszaak';
$aJuridicalForms[] = 'B.V.';
$aJuridicalForms[] = 'N.V.';
$aJuridicalForms[] = 'Maatschap';
$aJuridicalForms[] = 'V.O.F';
$aJuridicalForms[] = 'C.V.';
$aJuridicalForms[] = 'Vereniging';
$aJuridicalForms[] = 'Stichting';

$aLanguageCodes = [
    'nl_NL',
    'en_US',
    'fr_FR',
    'de_DE',
    'es_ES',
    'pt_PT',
    'it_IT',
];

foreach ($aJuridicalForms as $sLegalForm) {
    $oLegalForm = LegalFormQuery::create()->findOneByName($sLegalForm);

    if (!$oLegalForm instanceof LegalForm) {
        $oLegalForm = new LegalForm();
        $oLegalForm->setName($sLegalForm);
        $oLegalForm->save();
    }
    foreach ($aLanguageCodes as $sLocaleCode) {
        $oLanguage = LanguageQuery::create()->findOneByLocaleCode($sLocaleCode);

        if (!$oLanguage instanceof Language) {
            continue;
        }
        $oLegalFormTranslation = new LegalFormTranslation();
        $oLegalFormTranslation = LegalFormTranslationQuery::create()->filterByLanguageId($oLanguage->getId())->filterByLegalFormId($oLegalForm->getId())->findOne();

        if (!$oLegalFormTranslation instanceof LegalFormTranslation) {
            $oLegalFormTranslation = new LegalFormTranslation();
            $oLegalFormTranslation->setLanguageId($oLanguage->getId());
            $oLegalFormTranslation->setLegalFormId($oLegalForm->getId());
            $oLegalFormTranslation->setName($sLegalForm);
            $oLegalFormTranslation->save();
        }
    }
}
echo "cmmmit";
$oCon->commit();
