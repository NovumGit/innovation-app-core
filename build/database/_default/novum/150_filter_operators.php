<?php

use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\FilterOperator;
use Model\Setting\CrudManager\FilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;

$aFilterOperators = [
    ["field"                => "equals",
     "description"          => "Is gelijk aan",
     "description_sentence" => "gelijk is aan",
     'datatypes'            => [
         'string',
         'number',
         'lookup',
         'date',
         'boolean',
     ],
    ],
    [
        "field"                => "not_equals",
        "description"          => "Is ongelijk aan",
        "description_sentence" => "ongelijk is aan",
        'datatypes'            => [
            'string',
            'number',
            'lookup',
            'date',
        ],
    ],
    [
        "field"                => "larger_then",
        "description"          => "Is groter dan",
        "description_sentence" => "groter is dan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "larger_then_or_equal",
        "description"          => "Is groter dan of gelijk aan",
        "description_sentence" => "groter dan of gelijk is aan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then",
        "description"          => "Is kleiner dan",
        "description_sentence" => "kleiner is dan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then_or_equal",
        "description"          => "Is kleiner dan of gelijk aan",
        "description_sentence" => "kleiner dan of gelijk is aan",
        'datatypes'            => [
            'number',
            'date',
        ],
    ],
    [
        "field"                => "smaller_then_or_equal_or_null",
        "description"          => "Is kleiner dan of gelijk aan of null",
        "description_sentence" => "kleiner dan of gelijk is aan",
        'datatypes'            => ['date'],
    ],
    [
        "field"                => "in",
        "description"          => "Komt voor in opsomming",
        "description_sentence" => "voorkomt in opsomming",
        'datatypes'            => [
            'string',
            'number',
        ],
    ],
    [
        "field"                => "not_in",
        "description"          => "Komt niet voor opsomming",
        "description_sentence" => "niet voorkomt in opsomming",
        'datatypes'            => [
            'string',
            'number',
        ],
    ],
    [
        "field"                => "starts_with",
        "description"          => "Begind met",
        "description_sentence" => "begind met",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "ends_on",
        "description"          => "Eindigt op",
        "description_sentence" => "eindigd op",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "contains_text",
        "description"          => "Bevat de tekst",
        "description_sentence" => "het volgende tekst fragment bevat",
        'datatypes'            => ['string'],
    ],
    [
        "field"                => "not_contains_text",
        "description"          => "Bevat niet tekst",
        "description_sentence" => "niet het volgende tekst fragment bevat",
        'datatypes'            => ['string'],
    ],
    [
        "field"       => "yes_no",
        "description" => "Is gelijk aan",
        'datatypes'   => ['boolean'],
    ],
    [
        "field"       => "is_null",
        "description" => "Is null",
        'datatypes'   => [
            'date',
            'string',
        ],
    ],
    [
        "field"       => "is_not_null",
        "description" => "Is niet null",
        'datatypes'   => [
            'date',
            'string',
        ],
    ],
];
foreach ($aFilterOperators as $aFilterOperator) {
    $oFilterOperatorQuery = new FilterOperatorQuery();
    $oFilterOperatorQuery->filterByName($aFilterOperator['field']);
    $oFilterOperator = $oFilterOperatorQuery->findOne();
    if (!$oFilterOperator instanceof FilterOperator) {
        $oFilterOperator = new FilterOperator();
    }
    $oFilterOperator->setName($aFilterOperator['field']);
    $oFilterOperator->setDescription($aFilterOperator['description']);

    if (isset($aFilterOperator['description_sentence'])) {
        $oFilterOperator->setDescriptionSentence($aFilterOperator['description_sentence']);
    } else {
        $oFilterOperator->setDescriptionSentence('');
    }
    $oFilterOperator->save();
}

foreach ($aFilterOperators as $aFilterOperator) {
    $oFilterOperator = FilterOperatorQuery::create()->findOneByName($aFilterOperator['field']);
    foreach ($aFilterOperator['datatypes'] as $sDataType) {
        $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($sDataType);
        $oFilterOperatorDatatypeQuery = FilterOperatorDatatypeQuery::create();
        $oFilterOperatorDatatypeQuery->filterByFilterDatatypeId($oFilterDataType->getId());
        $oFilterOperatorDatatypeQuery->filterByFilterOperatorId($oFilterOperator->getId());
        $oFilterOperatorDatatype = $oFilterOperatorDatatypeQuery->findOne();
        if (!$oFilterOperatorDatatype instanceof FilterOperatorDatatype) {
            $oFilterOperatorDatatype = new FilterOperatorDatatype();
            $oFilterOperatorDatatype->setFilterDatatypeId($oFilterDataType->getId());
            $oFilterOperatorDatatype->setFilterOperatorId($oFilterOperator->getId());
            $oFilterOperatorDatatype->save();
        }
    }
}
