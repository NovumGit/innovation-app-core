<?php

use Model\Setting\CrudManager\FilterDatatype;
use Model\Setting\CrudManager\FilterDatatypeQuery;

$aDataTypes = [
    'string',
    'number',
    'boolean',
    'lookup',
    'date',
];

foreach ($aDataTypes as $sDataType) {
    $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($sDataType);
    if (!$oFilterDataType instanceof FilterDatatype) {
        $oFilterDataType = new FilterDatatype();
        $oFilterDataType->setName($sDataType);
        $oFilterDataType->save();
    }
}
