<?php

use Core\Environment;

echo '10_env_files' . PHP_EOL;
$aFiles = glob('../../../../public_html/*');

foreach ($aFiles as $sFile) {
    $sBaseName = basename($sFile);

    $sCustomDir = basename(dirname($_SERVER['PWD']));

    if (!file_exists($sFile . '/site-settings.php')) {
        continue;
    }
    $sSettingsFileContents = file_get_contents($sFile . '/site-settings.php');

    if (!strpos($sSettingsFileContents, $sCustomDir)) {
        continue;
    }

    if (preg_match('/api\.[a-z0-9.]+\.demo\.novum\.nu/', $sBaseName)) {
        $sBase = str_replace('api.', '', $sBaseName);
        echo "Make env file $sBase" . PHP_EOL;

        if (file_exists('../propel.php')) {
            $aConfig = require_once('../propel.php');

            $aVersions = [
                '',
                '.admin',
                '.api',
            ];
            foreach ($aVersions as $sVersion) {
                $aDb = $aConfig['propel']['database']['connections']['hurah'];

                $aEnvFile = [];
                $aEnvFile[] = 'SYSTEM_ID="' . $sCustomDir . '"';
                if (Environment::isDevel()) {
                    $aEnvFile[] = 'IS_DEVEL="1"';
                }

                $aEnvFile[] = 'SYSTEM_ROOT="/home/' . $aDb['user'] . '/platform/system"';

                $aEnvFile[] = 'DB_USER="' . $aDb['user'] . '"';
                $aEnvFile[] = 'DB_HOST="localhost"';
                $aEnvFile[] = 'DB_PASS="' . $aDb['password'] . '"';

                $sEnvFile = join(PHP_EOL, $aEnvFile);

                // echo $sEnvFile . PHP_EOL;

                $sFile = '../../../../env/' . $sVersion . '.' . $sBase;
                if (Environment::isDevel()) {
                    $sFile = str_replace('.nu', '.nuidev.nl', $sFile);
                } elseif (Environment::isTest()) {
                        $sFile = str_replace('.demo', '.test.demo', $sFile);
                }


                if (!file_exists($sFile)) {
                    echo "Write " . $sFile . PHP_EOL;
                    file_put_contents($sFile, $sEnvFile);
                }
            }
        }
    }
}
