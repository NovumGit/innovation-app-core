<?php

use Model\Setting\CrudManager\FilterDatatype;
use Model\Setting\CrudManager\FilterDatatypeQuery;

$aNewDatatypes = [

    'money',
];
foreach ($aNewDatatypes as $sDataType) {
    $oFilterDatatype = FilterDatatypeQuery::create()->findOneByName($sDataType);

    if (!$oFilterDatatype instanceof FilterDatatype) {
        $oFilterDatatype = new FilterDatatype();
        $oFilterDatatype->setName($sDataType);
        $oFilterDatatype->save();
    }
}
