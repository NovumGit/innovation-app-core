<?php

use Model\System\Event\EventTriggerQuery;

/*
moment
event
input
output
applicable_to_collection
applicable_to_item
*/
$aEventTriggers = [

    [
        'Before get',
        'pre_get',
    ],
    [
        'After get',
        'post_get',
    ],

    [
        'Before put',
        'pre_put',
    ],
    [
        'After put',
        'post_put',
    ],

    [
        'Before post',
        'pre_post',
    ],
    [
        'After post',
        'post_post',
    ],

    [
        'At time',
        'at_time',
    ],
    [
        'On interval',
        'on_interval',
    ],

    [
        'On click',
        'on_click',
    ],
];

foreach ($aEventTriggers as $aEventType) {
    echo "Add " . $aEventType[0] . PHP_EOL;
    $oEventTriggerQuery = EventTriggerQuery::create();
    $oEventTriggerQuery->filterByCode($aEventType[1]);
    $oEventTrigger = $oEventTriggerQuery->findOneOrCreate();
    $oEventTrigger->setName($aEventType[1]);
    $oEventTrigger->save();
}
