<?php

use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

try {
    $aLanguages = [];
    $aLanguages[] = ['is_deleted'         => 0,
                     'description'        => 'Nederlands',
                     'locale_code'        => 'nl_NL',
                     'is_enabled_cms'     => 1,
                     'is_enabled_webshop' => 1,
                     'is_default_cms'     => 0,
                     'is_default_webshop' => 1,
                     'flag_icon'          => 'NL',
                     'shop_url_prefix'    => 'nl',
    ];
    $aLanguages[] = ['is_deleted'         => 0,
                     'description'        => 'English',
                     'locale_code'        => 'en_US',
                     'is_enabled_cms'     => 1,
                     'is_enabled_webshop' => 1,
                     'is_default_cms'     => 1,
                     'is_default_webshop' => 0,
                     'flag_icon'          => 'GB',
                     'shop_url_prefix'    => 'en',
    ];

    foreach ($aLanguages as $aLanguage) {
        $oLanguage = LanguageQuery::create()->findOneByLocaleCode($aLanguage['locale_code']);
        if (!$oLanguage instanceof Language) {
            $oLanguage = new Language();
            $oLanguage->setLocaleCode($aLanguage['locale_code']);
        }
        $oLanguage->setItemDeleted($aLanguage['is_deleted']);
        $oLanguage->setDescription($aLanguage['description']);
        $oLanguage->setIsEnabledCms($aLanguage['is_enabled_cms']);
        $oLanguage->setIsEnabledWebshop($aLanguage['is_enabled_webshop']);
        $oLanguage->setIsDefaultCms($aLanguage['is_default_cms']);
        $oLanguage->setIsDefaultWebshop($aLanguage['is_default_webshop']);
        $oLanguage->setFlagIcon($aLanguage['flag_icon']);
        $oLanguage->setShopUrlPrefix($aLanguage['shop_url_prefix']);
        $oLanguage->save();
    }
} catch (\Exception\LogicException $e) {
    echo $e->getMessage();
}
