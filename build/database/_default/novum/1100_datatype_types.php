<?php

use Model\System\DataModel\DataTypeQuery;

$aDataTypes = [
    'BIT',
    'TINYINT',
    'SMALLINT',
    'INTEGER',
    'BIGINT',
    'FLOAT',
    'REAL',
    'NUMERIC',
    'DECIMAL',
    'CHAR',
    'VARCHAR',
    'LONGVARCHAR',
    'DATE',
    'TIME',
    'TIMESTAMP',
    'BINARY',
    'VARBINARY',
    'LONGVARBINARY',
    'NULL',
    'OTHER',
    'DISTINCT',
    'STRUCT',
    'BLOB',
    'CLOB',
    'REF',
    'BOOLEANINT',
    'BOOLEANCHAR',
    'DOUBLE',
    'BOOLEAN',
    'OBJECT',
    'ARRAY',
    'ENUM',
];

echo "Adding " . count($aDataTypes) . " datatypes." . PHP_EOL;

foreach ($aDataTypes as $sDataType) {
    $oDataTypeQuery = DataTypeQuery::create();
    $oDataTypeQuery->filterByName($sDataType);
    $oDataType = $oDataTypeQuery->findOneOrCreate();
    $oDataType->save();
}
