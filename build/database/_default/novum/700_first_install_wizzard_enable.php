<?php

use Core\Setting;

$bInstallationCompleted = Setting::get('installation_completed');

if (!$bInstallationCompleted) {
    Setting::store('start_installer', false);
    Setting::store('installation_completed', true);

    Setting::store('show_sidebar_left', true);
    Setting::store('show_navbar_top', true);
}
