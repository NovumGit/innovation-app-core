<?php

use Core\Utils;
use Crud\Generic\InterfaceGenericFieldType;
use Crud\IEventField;
use Hi\Helpers\DirectoryStructure;
use Model\System\FormFieldTypeQuery;

$oDirectoryStructure = new DirectoryStructure();
$sClassesDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'classes');
$sGenericFieldDir = Utils::makePath($sClassesDir, 'Crud', 'Generic', 'Field');
$aFiles = glob(((string)$sGenericFieldDir) . '/*');

foreach ($aFiles as $sField) {
    $sField = str_replace('.php', '', $sField);
    $sField = str_replace($sClassesDir, '', $sField);
    $sFqn = str_replace('/', '\\', $sField);

    $oReflector = new \Core\Reflector($sFqn);

    if (!$oReflector->implementsInterface(InterfaceGenericFieldType::class)) {
        continue;
    }
    if ($oReflector->implementsInterface(IEventField::class)) {
        continue;
    }

    $sLabel = $sFqn::getGenericLabel();
    $oDataType = $sFqn::getPropelType();

    $sName = str_replace('Generic', '', $oReflector->getShortName());

    $oFormFieldTypeQuery = FormFieldTypeQuery::create();
    $oFormFieldTypeQuery->filterByName($sName);
    $oFormFieldType = $oFormFieldTypeQuery->findOneOrCreate();
    echo "Added form field type $sName " . PHP_EOL;

    $oFormFieldType->setIsForeignKey(($sName === 'ForeignKey'));

    $oFormFieldType->setLabel($sLabel);
    $oFormFieldType->setDataTypeId($oDataType->getId());
    $oFormFieldType->save();
}
