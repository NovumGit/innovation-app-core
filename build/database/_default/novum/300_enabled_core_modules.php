<?php

use Model\Module\Module;
use Model\Module\ModuleQuery;
use Model\Module\ModuleRole;
use Model\Module\ModuleRoleQuery;
use Model\Setting\MasterTable\RoleQuery;

try {
    $aEnabledModules = [

        'AdminModules\Setting\Config'                  => [
            'Admin',
            'Medewerker',
        ],
        'AdminModules\Services\Config'                 => [
            'Admin',
            'Medewerker',
        ],
        'AdminModules\Apps\Config'                     => ['Developer'],
        'AdminModules\Modeller\Config'                 => ['Developer'],
        'AdminModules\Visual_code\Config'              => ['Developer'],
        'AdminModules\System\Ui_component_type\Config' => ['Developer'],
        'AdminModules\System\Config'                   => ['Developer'],
        'AdminModules\User\Config'                     => [
            'Admin',
            'Medewerker',
            'Developer',
        ],
        'AdminModules\Admin\Config'                    => [
            'Admin',
            'Developer',
        ],
    ];

    $i = 0;
    foreach ($aEnabledModules as $sModule => $aRoles) {
        echo "Module $sModule " . PHP_EOL;
        $oModule = ModuleQuery::create()->findOneByName($sModule);

        if (!$oModule instanceof Module) {
            echo "Creating module " . $sModule . PHP_EOL;
            $oModule = new Module();
            $oModule->setName($sModule);
            $oModule->save();
        }

        foreach ($aRoles as $sRole) {
            $oRole = RoleQuery::create()->findOneByName($sRole);
            $oModuleRoleQuery = ModuleRoleQuery::create();

            echo "Role $sRole " . $oRole->getId() . PHP_EOL;
            $oModuleRoleQuery->filterByRoleId($oRole->getId());
            $oModuleRoleQuery->filterByModuleId($oModule->getId());
            $oModuleRole = $oModuleRoleQuery->findOne();

            if (!$oModuleRole instanceof \Model\Module\ModuleRole) {
                echo "Created module role " . PHP_EOL;
                $oModuleRole = new ModuleRole();
                $oModuleRole->setRoleId($oRole->getId());
                $oModuleRole->setModuleId($oModule->getId());
                $oModuleRole->setSorting($i);
                $oModuleRole->save();
                $i++;
            } else {
                echo "Created module was already created " . PHP_EOL;
            }
        }
    }
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
