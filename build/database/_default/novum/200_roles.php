<?php

use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;

$aRoles = [
    1 => ['name'         => 'Medewerker',
          'is_deletable' => false,
    ],
    2 => [
        'name'         => 'Admin',
        'is_deletable' => false,
    ],
    3 => [
        'name'         => 'Developer',
        'is_deletable' => false,
    ],
];

foreach ($aRoles as $aRole) {
    $oRole = RoleQuery::create()->findOneByName($aRole['name']);

    if (!$oRole instanceof Role) {
        $oRole = new Role();
        $oRole->setName($aRole['name']);
    }
    $oRole->setIsDeletable($aRole['is_deletable']);
    $oRole->save();
}
