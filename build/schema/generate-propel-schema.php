<?php

use Generator\Schema\Merger;

$sRootDir = dirname(explode(DIRECTORY_SEPARATOR . 'build', $_SERVER['PWD'])[0]);

// 3x ../ is nodig
/** @noinspection PhpIncludeInspection */
require_once $sRootDir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

ini_set('display_errors', true);
error_reporting(E_ALL);

echo "Generate propel schema " . PHP_EOL;
$sSchemaUrl = null;

if (isset($argv[1])) {
    $sSchemaUrl = $argv[1] . '/v2/propel-schema.xml';
    echo "Fetching $sSchemaUrl " . PHP_EOL;
    $sUserSchema = file_get_contents($sSchemaUrl);
    echo "Writing ./generated-schema/import/user-schema.xml" . PHP_EOL;
    file_put_contents('./generated-schema/import/user-schema.xml', $sUserSchema);
}

$oConverter = new Merger('schema.xml');
$oConverter->merge();

if (file_exists('./generated-schema/import/user-schema.xml')) {
    $oConverter->addImports(['./generated-schema/import/user-schema.xml']);
}

echo "done" . PHP_EOL;
exit();
