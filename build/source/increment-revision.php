<?php

$sRevisionFile = '../../public_html/' . $argv['1'] . '/revision';

if (!file_exists($sRevisionFile)) {
    touch($sRevisionFile);
}
$sRevision = file_get_contents($sRevisionFile);
$sRevision = (int)$sRevision + 1;
file_put_contents($sRevisionFile, $sRevision);
echo "Revision incremented to $sRevision" . PHP_EOL;
