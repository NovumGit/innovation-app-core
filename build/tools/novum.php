#!/usr/bin/env php
<?php

$sSystemRoot = dirname(dirname(dirname($_SERVER['PHP_SELF'])));
require_once $sSystemRoot . '/vendor/autoload.php';

use Cli\Helper\Application\ApplicationLoader;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PhpNamespace;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Input\ArrayInput;

$oDirectoryStructure = new DirectoryStructure();
$_ENV['SYSTEM_ROOT'] = $oDirectoryStructure->getSystemRoot();

$oApplicationLoader = new ApplicationLoader('Novum Innovation App');
$oPropelPath = new Path($oDirectoryStructure->getSystemRoot() . '/vendor/propel/propel/src/Propel/Generator/Command');
$oPropelNs = new PhpNamespace('\\Propel\\Generator\\Command\\');

$oApplicationLoader->addDirectory($oPropelPath, $oPropelNs, 'propel');
$oApplication = $oApplicationLoader->load();

$cwd = getcwd();

if (isset($argv[1]) && $sFirstSection = explode(':', $argv[1])[0] === 'propel') {
    $sFirstSection = explode(':', $argv[1])[0];
    if ($sFirstSection === 'propel') {
        // Get the domain as it is always required for propel commands so we can import the build config first
        $oCommand = $oApplication->find('db:propel-build-helper');
        $input = new ArrayInput([]);
        $output = new Symfony\Component\Console\Output\ConsoleOutput();
        $oCommand->run($input, $output);

        $oCommand = $oApplication->find($argv[1]);
        $oCommand->run($input, $output);
    }
} else {
    $oApplication->run();
}
chdir($cwd);
