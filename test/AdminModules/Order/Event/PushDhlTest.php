<?php

namespace Test\AdminModules\Event;

use AdminModules\Order\Event\Push_dhl;
use Core\Cfg;
use Dhl\ApiClient;
use Model\Sale\SaleOrderQuery;
use PHPUnit\Framework\TestCase;

class PushDhlTest extends TestCase
{
    public function setUp()
    {
        $aConfigFiles = [];

        $aConfigFiles[] = "../config/vangoolstoffenonline/propel/config.php";
        $aConfigFiles[] = "../config/vangoolstoffenonline/config.php";
        foreach ($aConfigFiles as $sConfigFile) {
            if (file_exists($sConfigFile)) {
                require_once $sConfigFile;
            }
        }
        parent::setUp();
    }

    public function testCapabilities()
    {
        $oRandomSaleOrder = SaleOrderQuery::create()->filterByIsFullyPaid(true)->findOne();
        $options = Push_dhl::getShipmentOptions($oRandomSaleOrder);

        print_r($options);
    }

    public function testGetClient()
    {
        $aConfig['DHL_API_KEY'] = '43f631e3-1b75-4716-ba36-f4b5708b183f';
        $aConfig['DHL_API_USER_ID'] = '462ce683-a943-4e4b-8089-d5d408e5acad';
        $aConfig['DHL_API_ACCOUNT_ID'] = '06080808';
        $aConfig['DHL_API_ORGANIZATION_ID'] = 'b8c1b29b-99d6-4c56-922b-0be9328bd262';
        Cfg::set($aConfig);

        $oClient = Push_dhl::getClient();

        $this->assertTrue($oClient instanceof ApiClient);
    }
}
