<?php

/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24-2-20
 * Time: 13:11
 */

namespace Test\public_html\api_iot_litams_com\modules\Mqtt\Loggers;

use LitamsIotApi\Mqtt\Loggers\AdsLogger;
use PHPUnit\Framework\TestCase;

class AdsLoggerTest extends TestCase
{

    public function testLog()
    {

        $oAdsLogger = new AdsLogger();

        $this->assertFalse($oAdsLogger->filter('46040033/6680/scale/availability'));
        $this->assertFalse($oAdsLogger->filter('46040033/6680/tagreader/availability'));
        $this->assertFalse($oAdsLogger->filter('46040033/6680/monitoring/ping'));

        $this->assertTrue($oAdsLogger->filter('46040033/6680/scale/bla'));
        $this->assertTrue($oAdsLogger->filter('46040033/6680/bla'));
        $this->assertTrue($oAdsLogger->filter('46040033/6680/bla/test'));
    }
}
