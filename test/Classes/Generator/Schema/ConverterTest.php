<?php

namespace Test\Classes\Api\Accounting\MoneyMonk;

use Exception;
use Generator\Schema\Converter;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Model\DataModelQuery;
use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase {

    /**
     * @throws Exception
     */
    public function testChangeXsd() {
        $sInput = '<database name="hurah" custom="NovumOverheid" crudRoot="Custom/NovumOverheid" crudNamespace="Crud\Custom\NovumOverheid" defaultIdMethod="native" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://novumgit.gitlab.io/innovation-app-schema-xsd/v1/schema-plus-crud.xsd" >';
        $sExpectedOutput = '<database name="hurah" custom="NovumOverheid" crudRoot="Custom/NovumOverheid" crudNamespace="Crud\Custom\NovumOverheid" defaultIdMethod="native" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://novumgit.gitlab.io/innovation-app-schema-xsd/v1/schema.xsd" >';

        $oConverter = new Converter(ModuleQuery::create(), DataModelQuery::create(), 'NovumOverheid');
        $oConverter->changeXsd($sInput);

        $this->assertTrue($sInput == $sExpectedOutput, $sInput . ' ==  ' . $sExpectedOutput);

        $sInput = 'xsi:noNamespaceSchemaLocation="../../../schema/schema-plus-crud.xsd"';
        $sExpectedOutput = 'xsi:noNamespaceSchemaLocation="./schema.xsd"';

        $oConverter = new Converter(ModuleQuery::create(), DataModelQuery::create(), 'NovumOverheid');
        $oConverter->changeXsd($sInput);

        $this->assertTrue($sInput == $sExpectedOutput, $sInput . ' ==  ' . $sExpectedOutput);
    }//end testChangeXsd()

    /*
        function testGetExternalSchemas()
        {
            $oConverter = new Converter('./test-schema.xml');

            $sSchema  = file_get_contents('./test-schema.xml');
            $aSchemas = $oConverter->getExternalSchemaFiles($sSchema);
            $this->assertTrue(count($aSchemas) == 2, print_r($aSchemas, true));

        }//end testGetExternalSchemas()
    */

    public function testFixExternalSchemas() {
        $aTestData = [
            0 => [
                'in'  => '<external-schema filename="../../schema/core-schema-extra.xml" />',
                'out' => '<external-schema filename="./core-schema-extra.xml" />',

            ],
            1 => [
                'in'  => '<external-schema filename="./core-schema.xml" />',
                'out' => '<external-schema filename="./core-schema.xml" />',

            ],
        ];
        foreach ($aTestData as $sTestRow) {
            $oConverter = new Converter(ModuleQuery::create(), DataModelQuery::create(),);
            $aSchemas = $oConverter->getExternalSchemaFiles($sTestRow['in']);
            $oConverter->adjustExternalSchemaPaths($sTestRow['in'], $aSchemas);

            $this->assertTrue($sTestRow['in'] == $sTestRow['out'], $sTestRow['in'] . '==' . $sTestRow['out']);
        }
    }//end testFixExternalSchemas()

}//end class
