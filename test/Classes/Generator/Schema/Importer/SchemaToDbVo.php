<?php


namespace Test\Classes\Generator\Schema\Importer;


use Hurah\Types\Type\Path;
use Hurah\Types\Type\PathCollection;
use Hurah\Types\Type\SystemId;

class SchemaToDbVo extends \Generator\Vo\SchemaToDbVo
{
    function __construct()
    {
        parent::__construct(new SystemId('x.y'));
    }

    function getSystemId(): SystemId {
        return new SystemId('x.y');
    }

    function getSchemaFiles(): PathCollection {
        return new PathCollection([
            new Path('./schema.xml')
        ]);
    }
}