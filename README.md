<p align="center"><img src="https://gitlab.com/NovumGit/innovation-app-core/-/raw/master/assets/novum.png"  alt="Novum logo"/></p>

# InnovatieApp

InnovatieApp is an API / UI build tool developed by the Novum Innovation Lab. With this tool you can quickly develop
API's that contain all the required necessities to connect to the [NlX](https://nlx.io/) platform. The system comes 
with a bunch of modules based on other opensource components. If you are looking for a good startingpoint then 
the [demo system](https://gitlab.com/NovumGit/innovatieapp) would be a good place to start.
