<?php
namespace AdminModules\Api\Airbnb;

use AdminModules\Api\TasklistController;
use Api\Supplier\EasyHosting\Task\FetchInvoices;
use Core\Translate;

class OverviewController extends TasklistController
{
    function getTitle()
    {
        return Translate::fromCode('Airbnb related tasks');
    }

    function getTasks(): array
    {
        return [
            FetchInvoices::class
        ];
    }

}