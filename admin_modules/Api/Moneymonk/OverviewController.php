<?php
namespace AdminModules\Api\MoneyMonk;

use AdminModules\Api\TasklistController;
use Api\Accounting\MoneyMonk\Task\LedgerUpdate;
use Api\Accounting\MoneyMonk\Task\NoInvoiceReconcilliation;
use Api\Accounting\MoneyMonk\Task\PurchaseInvoiceReconciliation;
use Api\Accounting\MoneyMonk\Task\FetchContacts;
use Api\Accounting\MoneyMonk\Task\FetchSaleInvoices;
use Api\Supplier\MoneyMonk\Task\DownloadOwnInvoices;
use Core\Translate;

class OverviewController extends TasklistController
{
    function getTitle()
    {
        return Translate::fromCode('Moneymonk related tasks');
    }

    function getTasks(): array
    {
        return [
            FetchContacts::class,
            FetchSaleInvoices::class,
            LedgerUpdate::class,
            DownloadOwnInvoices::class,
            PurchaseInvoiceReconciliation::class,
            NoInvoiceReconcilliation::class
        ];
    }

}