<?php
namespace AdminModules\Api\Fxw;

use AdminModules\Api\TasklistController;
use Api\Supplier\Fxw\Task\FetchInvoices;
use Core\Translate;

class OverviewController extends TasklistController
{
    function getTitle()
    {
        return Translate::fromCode('Moneymonk related tasks');
    }

    function getTasks(): array
    {
        return [
            FetchInvoices::class
        ];
    }

}