<?php
namespace AdminModules\Api\Reeleezee;

use Api\Push\Accounting\Reeleezee\Reeleezee_config;
use Core\MainController;
use Core\Translate;
use Core\Utils;

class OverviewController extends MainController
{
    function run()
    {
        $aDirectories = Reeleezee_config::getDirectories();

        $sLogFile = $aDirectories['log'].'/'.date('Y').'summary.log';
        $iLogLineCount = 50;

        $aLogLines = [];
        if(file_exists($sLogFile))
        {
            $aLogLines = Utils::tail($sLogFile, $iLogLineCount);
            $aLogLines = array_reverse($aLogLines);
        }

        $aCounters = [];
        foreach($aDirectories as $sKey => $sDirectory)
        {
            $aCounters[$sKey] = count(glob($sDirectory.'/*.json'));
        }

        $sTitle = Translate::fromCode('Reeleezee API info');

        $aMainData = [
            'counters' => $aCounters,
            'line_count' => $iLogLineCount,
            'log_lines' => $aLogLines

        ];
        $aTopnavData = [];

        $aOut = [
            'title' => $sTitle,
            'content' => $this->parse('Api/Reeleezee/overview.twig', $aMainData),
            'top_nav' => $this->parse('Api/Reeleezee/overview_top_nav.twig', $aTopnavData)
        ];
        return $aOut;
    }
}