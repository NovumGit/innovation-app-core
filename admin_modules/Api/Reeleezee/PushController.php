<?php
namespace AdminModules\Api\Reeleezee;

use Api\Push\Accounting\Reeleezee\Main;
use Core\MainController;
use Exception;


class PushController extends MainController
{

    function doNoLoginpPush()
    {
        $this->run();
    }

    function run()
    {
        try
        {
            $oReeleezeeMain = new Main();
            $oReeleezeeMain->push();
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            mail('anton@nui-boutkam.nl', 'Push to Reeleezee problemen', $e->getMessage().PHP_EOL.$e->getFile());
        }

        exit('You made it!');
    }
}