<?php
namespace AdminModules\Api;

use Api\TaskList;
use Api\TaskRunner;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Exception;

abstract class TasklistController extends MainController
{
    abstract function getTitle();

    /**
     * @return \Api\AbstractTask[]
     */
    abstract protected function getTasks():array ;

    function doResetCounters()
    {
        try
        {
            $iTaskId = $this->get('task_id');
            (new TaskRunner($iTaskId))->resetCounters();
            StatusMessage::success(Translate::fromCode("Tellers op nul gezet"));
        }
        catch (Exception $exception)
        {
            StatusMessage::warning($exception->getMessage());
        }
        $this->redirect($this->getRequestUri(false));
    }
    function doRunTask()
    {
        try
        {
            ini_set('max_execution_time', 0);

            $iTaskId = $this->get('task_id');
            (new TaskRunner($iTaskId))->run();
            StatusMessage::success(Translate::fromCode("De taak is afgerond"));
        }
        catch (Exception $exception)
        {
            StatusMessage::warning($exception->getMessage());
        }
        $this->redirect($this->getRequestUri(false));
    }
    function run()
    {
        $aMain = [
            'base_uri' => $this->getRequestUri(false),
            'tasks' => new TaskList($this->getTasks())
        ];
        return [
            'title' => Translate::fromCode('Moneymonk API'),
            'content' => $this->parse('Api/task_list_overview.twig', $aMain),
            'top_nav' => $this->parse('Api/task_list_top_nav.twig', [])
        ];
    }
}