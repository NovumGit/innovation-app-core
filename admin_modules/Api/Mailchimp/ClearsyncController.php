<?php
namespace AdminModules\Api\Mailchimp;

use \Core\MainController;
use Core\QueryMapper;
use Core\StatusMessage;
use Core\Translate;
use Helper\MailChimpHelper;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;

class ClearsyncController extends MainController
{
    function run()
    {
        $sListId = $this->get('list_id', null, true);
        $sTableName = MailChimpHelper::getMailChimpListId($sListId);
        QueryMapper::query("DROP TABLE IF EXISTS $sTableName");
        StatusMessage::success("All customers will be resynced in the comming minutes.");
        $this->redirect('/api/mailchimp/overview');


        $sCrudViewName = 'mailchimp_list_'.$sListId;
        $oCrudView = CrudViewQuery::create()->findOneByName($sCrudViewName);

        if($oCrudView instanceof CrudView)
        {
            $oCrudView->delete();
            StatusMessage::success(Translate::fromCode("Sync instellingen verwijderd."));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Sync instellingen niet gevonden, niets gedaan."));
        }
        $this->redirect('/api/mailchimp/overview');
        exit();
    }
}
