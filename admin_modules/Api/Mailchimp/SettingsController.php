<?php
namespace AdminModules\Api\Mailchimp;

use Core\Setting;
use Core\StatusMessage;
use Core\Translate;

class SettingsController extends \Core\MainController
{

    function doStore()
    {
        $aData = $this->post('data');

        Setting::store('mailchimp_api_key', $aData['mailchimp_api_key']);
        StatusMessage::success("Mailchimp API key opgeslagen.");
        $this->redirect('/api/mailchimp/overview');
    }
    function run()
    {
        $sMailchimpApiKey = Setting::get('mailchimp_api_key');

        $aData = [
            'mailchimp_api_key' => $sMailchimpApiKey
        ];

        return [
            'content' => $this->parse('Api/Mailchimp/settings.twig', $aData),
            'top_nav' => $this->parse('Api/Mailchimp/top_nav_overview.twig', []),
            'title' => Translate::fromCode("MailChimp connector"),
        ];

    }
}