<?php
namespace AdminModules\Api\Mailchimp;

use \Core\MainController;
use Core\QueryMapper;
use Core\StatusMessage;
use Core\Translate;
use Helper\MailChimpHelper;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;

class UnlinkController extends MainController
{
    function run()
    {
        $sListId = $this->get('list_id', null, true);
        $sTableName = MailChimpHelper::getMailChimpListId($sListId);
        QueryMapper::query("DROP TABLE IF EXIST $sTableName");
        $oCrudView = CrudViewQuery::create()->findOneByName($sTableName);

        if($oCrudView instanceof CrudView)
        {
            $oCrudView->delete();
            StatusMessage::success(Translate::fromCode("Sync instellingen verwijderd."));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Sync instellingen niet gevonden, niets gedaan."));
        }
        $this->redirect('/api/mailchimp/overview');
        exit();
    }
}
