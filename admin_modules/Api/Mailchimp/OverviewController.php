<?php
namespace AdminModules\Api\Mailchimp;

use Core\Setting;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use DrewM\MailChimp\MailChimp;
use Helper\MailChimpHelper;

class OverviewController extends \Core\MainController
{
    function doConfirmDeleteList()
    {
        $sMessage = Translate::fromCode('Weet u zeker dat u de lijst wilt verwijderen? Dit kan niet ongedaan gemaakt worden.');
        $sTitle = Translate::fromCode('Weet u het zeker?');
        $sListId = $this->get('list_id');
        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Ja verwijder'), '/api/mailchimp/editlist?_do=DeleteList&list_id='.$sListId, Translate::fromCode('Lijst verwijderen'), 'danger'),
            new StatusMessageButton(Translate::fromCode('Annuleren'), '/api/mailchimp/overview', Translate::fromCode('Lijst niet verwijderen'), 'info'),
        ];

        StatusModal::warning($sMessage, $sTitle, $aButtons);
    }
    function run()
    {
        $sMailchimpApiKey = Setting::get('mailchimp_api_key');

        if(!$sMailchimpApiKey)
        {
            StatusMessage::warning("No MailChimp API key is configured yet");
            $this->redirect('/api/mailchimp/settings');
        }
        $oMailChimp = new MailChimp($sMailchimpApiKey);
        $aOverviewVars = $oMailChimp->get('lists');

        if(!empty($aOverviewVars['lists']))
        {
            foreach($aOverviewVars['lists'] as &$aList)
            {
                $aList['syncs_from_nuicart'] = MailChimpHelper::syncsWithNuiCart($aList['id']);
                $aList['local_matching_records'] = MailChimpHelper::countLocalMatchingItems($aList['id']);
            }
        }

        return [
            'content' => $this->parse('Api/Mailchimp/overview.twig', $aOverviewVars),
            'top_nav' => $this->parse('Api/Mailchimp/top_nav_overview.twig', []),
            'title' => Translate::fromCode("MailChimp connector"),
        ];
    }
}