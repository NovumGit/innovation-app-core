<?php
namespace AdminModules\Api\Mailchimp;

use Core\Translate;
use Crud\CrudViewManager;
use Crud\Customer\CrudMailChimpManager;
use Helper\MailChimpHelper;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;

class ConfigureController extends \Core\MainController
{
    function doChangeToWhom()
    {
        // This function needs to be here but actually doesn't do anything.
    }
    function run()
    {
        $sListId = $this->get('list_id');
        // Niet zomaar veranderen, wordt gebruikt in de cronjob om de mailing te syncen.
        $sCrudViewName = 'mailchimp_list_'.$sListId;

        $oCrudView = CrudViewQuery::create()->findOneByName($sCrudViewName);
        $iCrudViewId = null;
        $oCrudMailChimpManager = new CrudMailChimpManager();
        if($oCrudView instanceof CrudView)
        {
            $iCrudViewId = $oCrudView->getId();
        }
        else
        {
            $iCrudViewId = CrudViewManager::createOrEditView(new CrudMailChimpManager(), $sCrudViewName, $oCrudMailChimpManager->getDefaultOverviewFields());
            $oCrudView = CrudViewQuery::create()->findOneById($iCrudViewId);
        }

        $sDo = $this->post('_do');
        $aData = [];
        if($sDo)
        {
            $aData = $this->post('data');
        }
        $iCurrentPage = $this->get('p', 1, false, 'numeric');

        $aOverviewVars = [
            'sample_data' => MailChimpHelper::getSampleData($oCrudView, $oCrudMailChimpManager, $iCurrentPage),
            'data' => $aData,
            'crud_view_id' => $iCrudViewId
        ];
        return [
            'content' => $this->parse('Api/Mailchimp/configure.twig', $aOverviewVars),
            'top_nav' => $this->parse('Api/Mailchimp/top_nav_overview.twig', []),
            'title' => Translate::fromCode("MailChimp connector"),
        ];
    }


}