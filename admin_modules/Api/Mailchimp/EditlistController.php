<?php
namespace AdminModules\Api\Mailchimp;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Model\Company\CompanyQuery;
use Model\Setting\MasterTable\Base\UsaStateQuery;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use DrewM\MailChimp\MailChimp;

class EditlistController extends MainController {

    function doDeleteList()
    {
        $sListId = $this->get('list_id', null, true);

        $sMailchimpApiKey = Setting::get('mailchimp_api_key');
        $oMailChimp = new MailChimp($sMailchimpApiKey);
        $oMailChimp->delete('lists/'.$sListId);

        StatusMessage::success(Translate::fromCode('Mailling lijst verwijderd.'));
        $this->redirect('/api/mailchimp/overview');
        exit();
    }
    function doCreateList()
    {
        $aData = $this->post('data');
        $aErrors = [];
        if(empty($aData['name']))
        {
            $aErrors[] = Translate::fromCode('Geef alstublieft een naam aan de lijst.');
        }
        if(empty($aData['contact']['company']))
        {
            $aErrors[] = Translate::fromCode('Het veld bedrijfsnaam is een verplicht veld.');
        }
        if(empty($aData['contact']['address1']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een adres invullen.');
        }
        if(empty($aData['contact']['city']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een stad invullen.');
        }
        if(empty($aData['contact']['zip']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een postcode invullen.');
        }
        if(empty($aData['contact']['country']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een land invullen.');
        }
        if(empty($aData['campaign_defaults']['from_name']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een afzender naam invullen.');
        }
        if(empty($aData['campaign_defaults']['from_email']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een afzender e-mailadres invullen.');
        }
        if(empty($aData['campaign_defaults']['subject']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een onderwerp invullen.');
        }
        if(empty($aData['campaign_defaults']['language']))
        {
            $aErrors[] = Translate::fromCode('U moet verplicht een taal invullen.');
        }
        $sMailchimpApiKey = Setting::get('mailchimp_api_key');
        if(!empty($aErrors))
        {
            foreach($aErrors as $sError)
            {
                StatusMessage::warning($sError);
            }
        }
        else if(!$sMailchimpApiKey)
        {
            StatusMessage::warning(Translate::fromCode("Er is geen MailChimp API key geconfigureerd. We kunnen je hier verder nite bij helpen."));
        }
        else
        {
            $iListId = $this->get('list_id');

            // Boolean velden van maken
            $aData['email_type_option'] = (bool)$aData['email_type_option'];
            $aData['visibility'] = $aData['visibility'] == '1' ? 'pub' : 'priv';

            $oMailChimp = new MailChimp($sMailchimpApiKey);

            if($iListId)
            {
                StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
                $mResponse = $oMailChimp->patch("lists/$iListId", $aData);
            }
            else
            {
                StatusMessage::success(Translate::fromCode("Yay, je hebt zojuist een nieuwe mailinglijst aangemaakt."));
                $mResponse = $oMailChimp->post('lists', $aData);
            }
            if(isset($mResponse['id']))
            {

                $this->redirect('/api/mailchimp/overview');
            }
        }
    }
    function run()
    {

        $sDo = $this->post('_do');

        $oCurrentUser = User::getMember();
        $oOwnCompany = CompanyQuery::create()->findOneByIsDefaultSupplier(true);

        $sListId = $this->get('list_id');

        if($sDo == 'CreateList')
        {
            $aData = $this->post('data');
        }
        else if($sListId)
        {
            // Edit an existing list
            $sMailchimpApiKey = Setting::get('mailchimp_api_key');
            $oMailChimp = new MailChimp($sMailchimpApiKey);
            $aData = $oMailChimp->get('lists/'.$sListId);
        }
        else
        {
            // Create a new list
            $oCountry = $oOwnCompany->getGeneralCountry();
            if(!$oCountry instanceof Country)
            {
                $oCountry = new Country();
            }

            $aData['name'] = date('Y-m-d').' webshop e-mail adresses';
            $aData['contact']['company'] = $oOwnCompany->getCompanyName();
            $aData['contact']['address1'] = $oOwnCompany->getGeneralStreet().' '.$oOwnCompany->getGeneralNumber().' '.$oOwnCompany->getGeneralNumberAdd();
            $aData['contact']['address2'] = '';
            $aData['contact']['city'] = $oOwnCompany->getGeneralCity();
            $aData['contact']['zip'] = $oOwnCompany->getGeneralPostal();
            $aData['contact']['state'] = '';
            $aData['contact']['country'] =  $oCountry->getIso2();
            $aData['contact']['phone'] = $oOwnCompany->getPhone();
            $aData['permission_reminder'] = 'You gave us permission when you placed an order in our webshop';
            $aData['campaign_defaults']['from_name'] = $oCurrentUser->getFirstName().' '.$oCurrentUser->getLastName();
            $aData['campaign_defaults']['from_email'] = $oOwnCompany->getEmail();
            $aData['campaign_defaults']['subject'] = 'Mailing from '.$oOwnCompany->getCompanyName();
            $aData['campaign_defaults']['language'] = 'English';
            $aData['email_type_option'] =  true;
            $aData['visibility'] =  true;
        }
        $aEditVars['usa_states'] = UsaStateQuery::create()->find();
        $aEditVars['countries'] = CountryQuery::create()->find();
        $aEditVars['data'] = $aData;

        return [
            'content' => $this->parse('Api/Mailchimp/editlist.twig', $aEditVars),
            'top_nav' => $this->parse('Api/Mailchimp/top_nav_overview.twig', []),
            'title' => Translate::fromCode("MailChimp connector"),
        ];
    }
}