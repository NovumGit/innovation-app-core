<?php
namespace AdminModules\Cms\Promoproducts;

use AdminModules\Cms\GeneralController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Product\CrudProductPromoManager;
use Crud\Product\CrudProductPromoSelectorManager;
use Crud\Promo_product\CrudPromo_productManager;
use Helper\FilterHelper;
use Model\CustomerQuery;

class Pick_productController extends GeneralController {


    function run()
    {
        // This one we use to pick a product to build the promo up on.
        $oCrudProductPromoSelectorManager = new CrudProductPromoSelectorManager();

        // This one we use for the new + overview url
        $oCrudPromo_ProductManager = new CrudPromo_productManager();

        $oProductQuery = new CustomerQuery();
        $oProductQuery->filterByDerrivedFromId(null);
        $oCrudview = CrudViewManager::getView($oCrudProductPromoSelectorManager);
        $iTabId = $oCrudview->getId();

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oProductQuery = FilterHelper::applyFilters($oProductQuery, $iTabId);
        $oProductQuery = FilterHelper::generateVisibleFilters($oProductQuery, $aFilters);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aProducts = $oProductQuery->find();
        $oCrudProductPromoSelectorManager->setOverviewData($aProducts);

        $aViewData['overview_table'] = $oCrudProductPromoSelectorManager->getOverviewHtml($aFields);
        $aViewData['title'] = Translate::fromCode("Kies een product om aan de promotie toe te voegen");
        $sOverview = $this->parse('generic_overview.twig', $aViewData);

        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudPromo_ProductManager->getCreateNewUrl(),
            'page_tab_id' => $oCrudview->getId(),
            'new_title' => $oCrudPromo_ProductManager->getNewFormTitle(),
            'fields_config_code' => 'site_nav_overview',
            'manager' => 'ProductPromoSelectorManager',
            'module' => 'Product',
            'site' => $this->get('site', null, true)
        ];

        $sTopNav = $this->parse('Cms/Page/overview_top_nav.twig', $aSitePageViewData);
        $aView = [
            'title' => 'Kies een product',
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}