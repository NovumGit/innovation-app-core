<?php
namespace AdminModules\Cms\Promoproducts;


use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Promo_product_translation\CrudPromo_product_translationManager;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\Product;
use Model\CustomerQuery;
use Model\PromoProduct;
use Model\PromoProductQuery;
use Model\PromoProductTranslation;
use Model\PromoProductTranslationQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

class EditController extends GeneralController  {

    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iPromoProductId = $this->get('id', null, true);
        $sDataDir = \Core\Config::getDataDir()."/img/promo/$iPromoProductId";

        $aFiles = glob($sDataDir.'/*');
        foreach($aFiles as $sFile)
        {
            unlink($sFile);
        }
        rmdir($sDataDir);

        $oPromoProduct = PromoProductQuery::create()->findOneById($iPromoProductId);
        $oPromoProduct->delete();
        StatusMessage::success(Translate::fromCode("Item verwijderd."));
        $this->redirect("/cms/promoproducts/overview?site=$sSite");
    }
    function doCreate()
    {
        $sSite = $this->get('site', null, true);
        $aPromoProduct = $this->post('promo_product');

        if(empty($aPromoProduct['about']))
        {
            StatusMessage::danger(Translate::fromCode("Wijzigingen niet opgeslagen, geef alsjeblieft een \"over deze aanbieding\" tekst op zodat je deze advertentie later kunt herkennen."));
        }
        else
        {
            $oPromoProduct = new PromoProduct();
            $oSite = SiteQuery::create()->findOneByDomain($sSite);
            if(!$oSite instanceof Site)
            {
                throw new LogicException("Could not find the Site to use.");
            }

            $oPromoProduct->setSiteId($oSite->getId());
            $oPromoProduct->setAbout($aPromoProduct['about']);
            $oPromoProduct->save();
            $this->doUpdate($oPromoProduct->getId());
        }
    }

    function doUpdate($iPromoProductId = null)
    {
        $sSite = $this->get('site', null, true);
        if($iPromoProductId == null)
        {
            $iPromoProductId = $this->get('promo_product_id', null, true, 'numeric');
        }
        $aPromoProduct = $this->post('promo_product');
        if(empty($aPromoProduct['about']))
        {
            StatusMessage::danger(Translate::fromCode("Wijzigingen niet opgeslagen, geef alsjeblieft een \"over deze aanbieding\" tekst op zodat je deze advertentie later kunt herkennen."));
        }
        else
        {
            $oPromoProduct = PromoProductQuery::create()->findOneById($iPromoProductId);
            $oPromoProduct->setAbout($aPromoProduct['about']);
            $oPromoProduct->save();

            $aTranslations = $this->post('promo_translation');

            if(!empty($aTranslations))
            {
                foreach($aTranslations as $iLanguageId => $aTranslation)
                {
                    $oPromoProductTranslation = PromoProductTranslationQuery::create()
                                                    ->filterByLanguageId($iLanguageId)
                                                    ->filterByPromoProductId($oPromoProduct->getId())
                                                    ->findOne();

                    if(!$oPromoProductTranslation instanceof PromoProductTranslation)
                    {
                        $oPromoProductTranslation = new PromoProductTranslation();
                        $oPromoProductTranslation->setLanguageId($iLanguageId);
                        $oPromoProductTranslation->setPromoProductId($iPromoProductId);
                    }
                    $oPromoProductTranslation->setTitle($aTranslation['title']);
                    $oPromoProductTranslation->setAltUrl($aTranslation['alt_url']);
                    $oPromoProductTranslation->setDescription($aTranslation['description']);
                    $oPromoProductTranslation->save();

                    if($_FILES['picture']['name'][$iLanguageId] && $_FILES['picture']['error'][$iLanguageId] === 0)
                    {
                        $sExt = $ext = pathinfo($_FILES['picture']['name'][$iLanguageId], PATHINFO_EXTENSION);
                        if(in_array($sExt, ['jpg', 'png', 'jpeg']))
                        {
                            $sDataDir = \Core\Config::getDataDir().'/img';


                            if(!is_writable($sDataDir))
                            {
                                throw new LogicException('Datadir '.$sDataDir.' is not writable.');
                            }
                            if(!is_dir($sDataDir.'/promo/'))
                            {
                                mkdir($sDataDir.'/promo/', 0777,  true);
                            }
                            if(!is_writable($sDataDir.'/promo/'))
                            {
                                throw new LogicException('Datadir '.$sDataDir.'/promo/ is not writable.');
                            }

                            // chmod($sDataDir.'/promo/', 0777);

                            $sPromoImageDir = $sDataDir.'/promo/'.$oPromoProductTranslation->getPromoProductId();

                            if(!is_dir($sPromoImageDir))
                            {
                                if(is_writable(dirname($sPromoImageDir)))
                                {
                                    mkdir($sPromoImageDir, 0777,  true);
                                }
                                else
                                {
                                    throw new LogicException("Could not make dir ".dirname($sPromoImageDir).", directory not writable.");
                                }

                            }

                            $sDestination = $sPromoImageDir.'/'.$oPromoProductTranslation->getId().'.'.$sExt;
                            move_uploaded_file($_FILES['picture']['tmp_name'][$iLanguageId], $sDestination);

                            $oPromoProductTranslation->setHasImage(true);
                            $oPromoProductTranslation->setImageExt($sExt);
                            $oPromoProductTranslation->save();
                        }
                        else
                        {
                            StatusMessage::warning("Image upload failed, only jpg and png images are supported at the moment.");
                        }
                    }
                }
            }
            StatusMessage::success(Translate::fromCode('Wijzigingen opgeslagen.'));
            if($this->post('next_overview') == 1)
            {
                $this->redirect('/cms/promoproducts/overview?site='.$sSite);
            }
            else
            {
                $this->redirect('/cms/promoproducts/edit?site='.$sSite.'&promo_product_id='.$oPromoProduct->getId());
            }
        }
    }

    function run()
    {
        $iPromoProductId = $this->get('promo_product_id');

        $oCrudPromo_product_translationManager = new CrudPromo_product_translationManager();
        // $sSite = $this->get('site', null, true);

        if($iPromoProductId)
        {
            $oPromoProduct = PromoProductQuery::create()->findOneById($iPromoProductId);
        }
        else
        {
            $oPromoProduct = new PromoProduct();
        }

        $aEnabledLanguages = LanguageQuery::create()->findByIsEnabledWebshopOrIsEnabledCms();
        $aPostedTranslation = $this->post('promo_translation');


        $aActionTranslations = [];
        if(!$aEnabledLanguages->isEmpty())
        {
            foreach($aEnabledLanguages as $oLanguage)
            {
                if(!$oLanguage instanceof Language)
                {
                    throw new LogicException("Expected an instance of Language.");
                }
                if(isset($iPromoProductId))
                {
                    $oPromoProductTranslation = PromoProductTranslationQuery::create();
                    $oPromoProductTranslation->filterByLanguageId($oLanguage->getId());
                    $oPromoProductTranslation->filterByPromoProductId($iPromoProductId);
                    $oPromoProductTranslation = $oPromoProductTranslation->findOne();
                }
                else
                {
                    $oPromoProductTranslation = new PromoProductTranslation();
                    $oPromoProductTranslation->setLanguage($oLanguage);
                }
                if(isset($aPostedTranslation[$oLanguage->getId()]))
                {
                    $oPromoProductTranslation->setDescription($aPostedTranslation[$oLanguage->getId()]['description']);
                    $oPromoProductTranslation->setTitle($aPostedTranslation[$oLanguage->getId()]['title']);
                }
                $aActionTranslations[$oLanguage->getId()]  = $oPromoProductTranslation;
            }
        }
        $aEditViewVariables = [
            'next_action' => $iPromoProductId ? 'Update' : 'Create',
            'promo_product' => $oPromoProduct,
            'promo_translations' => $aActionTranslations,
        ];

        $sEditView = $this->parse('Cms/Promoproducts/edit.twig', $aEditViewVariables);

        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudPromo_product_translationManager->getCreateNewUrl(),
            /* 'page_tab_id' => $oCrudview->getId(), */
            'new_title' => $oCrudPromo_product_translationManager->getNewFormTitle(),
            'fields_config_code' => 'site_nav_overview',
            'manager' => 'ProductPromoSelectorManager',
            'module' => 'Product',
            'site' => $this->get('site', null, true)
        ];
        $sTopNav = $this->parse('Cms/Page/edit_top_nav.twig', $aSitePageViewData);
        $aView = [
            'title' => Translate::fromCode("Promo product instellen"),
            'top_nav' => $sTopNav,
            'content' => $sEditView
        ];
        return $aView;
    }
}