<?php
namespace AdminModules\Cms\Promoproducts;

use AdminModules\Cms\GeneralController;
use Crud\CrudViewManager;
use Crud\Promo_product\CrudPromo_productManager;
use Exception\LogicException;
use Helper\FilterHelper;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\PromoProductQuery;


class OverviewController extends GeneralController
{

    function run()
    {
        $sTitle = 'Product in promotion';
        $aGet = $this->get();
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("Site not found in ".__METHOD__.' on '.__LINE__);
        }

        // $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        // $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oQuery = PromoProductQuery::create();
        $oQuery->filterBySite($oSite);

        $oCrudPromo_productManager = new CrudPromo_productManager();

        $oCrudview = CrudViewManager::getView($oCrudPromo_productManager);
        $iTabId = $oCrudview->getId();

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oQuery = FilterHelper::applyFilters($oQuery, $iTabId);

        $oQuery = FilterHelper::generateVisibleFilters($oQuery, $aFilters);


        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aPromo_products = $oQuery->find();
        $oCrudPromo_productManager->setOverviewData($aPromo_products);

        $aViewData['overview_table'] = $oCrudPromo_productManager->getOverviewHtml($aFields);


        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudPromo_productManager->getCreateNewUrl(),
            'page_tab_id' => $oCrudview->getId(),
            'new_title' => $oCrudPromo_productManager->getNewFormTitle(),
            'fields_config_code' => 'promo_products_overview',
            'manager' => 'Promo_productManager',
            'module' => 'Promo_product',
            'site' => $this->get('site', null, true)
        ];

        $sTopNav = $this->parse('Cms/Page/overview_top_nav.twig', $aSitePageViewData);

        $sOverview = $this->parse('generic_overview.twig', $aViewData);

        $aView =
            [
                'top_nav' => $sTopNav,
                'content' => $sOverview,
                'title' => $sTitle
            ];

        return $aView;
    }
}
