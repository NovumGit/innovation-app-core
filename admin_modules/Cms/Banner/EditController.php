<?php
namespace AdminModules\Cms\Banner;

use AdminModules\Cms\GeneralController;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\Site_banner\CrudSite_bannerManager;
use Helper\EditviewHelper;

class EditController extends GeneralController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sSite = $this->get('site', null, true);
        $aUsp = $this->post('data', null);
        $this->oCrudManager = new CrudSite_bannerManager($aUsp);
        $this->oCrudManager->setArgument('site', $sSite);
    }
    function run()
    {
        $sEditConfigKey = 'website_usp_editor';
        $sEditViewTitle = 'Banner bewerken';

        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sEditViewTitle);

        $aBanner = $this->post('data', null);
        $aBanner['id'] = $this->get('id');

        $oBanner = $this->oCrudManager->getModel($aBanner);

        $aMainViewData = [
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oBanner, $sEditConfigKey),
            'banner' => $oBanner
        ];
        $sEdit = $this->parse('Cms/Banner/edit.twig', $aMainViewData);

        DeferredAction::register('return_after_slicing', $this->getRequestUri());

        $aNavViewData = [
            'site' => $this->get('site', null, true),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'fields_config_title' => $sEditViewTitle,
            'fields_config_code' => $sEditConfigKey,
            'enabled_modules' => $this->getEnabledModulesTopnav()
        ];
        $sTopNav = $this->parse('Cms/Banner/edit_top_nav.twig', $aNavViewData);
        $aView =
            [
                'title' => Translate::fromCode("Banner slides configureren"),
                'top_nav' => $sTopNav,
                'content' => $sEdit
        ];
        return $aView;
    }

    function doStore()
    {
        $sNextOverview = $this->post('next_overview');
        $aBanner = $this->post('data', null);

        if($this->oCrudManager->isValid($aBanner))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $oBanner = $this->oCrudManager->save($aBanner);
            $sSite = $oBanner->getSite()->getDomain();

            $iId = $oBanner->getId();
            if($sNextOverview == '1')
            {
                $this->redirect("/cms/banner/overview?site=$sSite&id=$iId");
            }
            else
            {
                $this->redirect("/cms/banner/edit?site=$sSite&id=$iId");
            }
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aBanner);
        }
    }
}
