<?php
namespace AdminModules\Cms\Banner;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_banner\CrudSite_bannerManager;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Model\Cms\SiteBannerQuery;
use LogicException;
use Model\Setting\CrudManager\CrudView;

class OverviewController extends GeneralController
{
    function run()
    {

        $sSite = $this->get('site', null, true);
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oSiteBannerQuery = SiteBannerQuery::create();
        $oSiteBannerQuery->orderBy($sSortField, $sSortDirection);
        $oCrudSite_bannerManager = new CrudSite_bannerManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudSite_bannerManager);
        $oCrudview = current($aCrudViews);
        if (!$oCrudview instanceof CrudView) {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }
        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_bannerManager);

        $aFields = CrudViewManager::getFields($oCrudview);
        $oSiteBannerQuery = FilterHelper::applyFilters($oSiteBannerQuery, $oCrudview->getId());
        $aCompanies = $oSiteBannerQuery->find();
        $oCrudSite_bannerManager->setOverviewData($aCompanies);

        $oCrudSite_bannerManager->setArgument('site', $sSite);
        $aViewData['overview_table'] = $oCrudSite_bannerManager->getOverviewHtml($aFields);

        $aTopNavVars = [
            'usp_tab_id' => $oCrudview->getId(),
            'site' => $sSite,
/*            'overview_url' => $oCrudSite_bannerManager->getOverviewUrl(),*/
            'new_url' => $oCrudSite_bannerManager->getCreateNewUrl(),
            'new_title' => $oCrudSite_bannerManager->getNewFormTitle(),
            'enabled_modules' => $this->getEnabledModulesTopnav()
        ];

        $sTopNav = $this->parse('Cms/Banner/overview_top_nav.twig', $aTopNavVars);
        $sOverview = $this->parse('Cms/Banner/overview.twig', $aViewData);

        $aView =
            [
                'title' => Translate::fromCode("Banner slides overzicht"),
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];

        return $aView;
    }
    function doDelete()
    {
        $oCrudSite_bannerManager = new CrudSite_bannerManager();
        StatusMessage::info("Banner verwijderd.");
        $oCrudSite_bannerManager->delete($this->get('id'));
        $this->redirect($oCrudSite_bannerManager->getOverviewUrl());
    }
}
