<?php
namespace AdminModules\Cms\Page;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Ui\RenderEditConfig;
use Core\Config;
use Crud\Site_page\CrudSite_pageManager;
use Exception\LogicException;
use Helper\EditviewHelper;
use Model\Cms\SiteFooterBlockQuery;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Model\Cms\SiteFooterBlockTranslationSitePage;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery;
use Model\Cms\SitePagePicture;
use Model\Cms\SitePagePictureQuery;
use Model\Cms\Site;
use Model\Cms\SitePageQuery;
use Model\Cms\SiteQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class EditController extends GeneralController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sSite = $this->get('site', null, true);
        $aUsp = $this->post('data', null);
        $this->oCrudManager = new CrudSite_pageManager($aUsp);
        $this->oCrudManager->setArgument('site', $sSite);
    }
    function run()
    {
        $sSite = $this->get('site', null, true);

        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("Kon site niet vinden $sSite.");
        }

        $sEditConfigKey = 'website_page_editor';
        $sEditViewTitle = 'Pagina bewerken';

        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sEditViewTitle);

        $aPage = $this->post('data', null);
        $aPage['id'] = $this->get('id');
        $aPage['site_id'] = $oSite->getId();

        $oUsp = $this->oCrudManager->getModel($aPage);

        $aSitePagePictures = SitePagePictureQuery::create()->orderBySorting()->filterBySitePageId($aPage['id'])->find();
        $oRenderEditConfig = new RenderEditConfig();
        $oRenderEditConfig->setLabelAndFieldWidth(1, 11);

        $iSiteFooterBlockTranslationId = null;
        $oSitePage = SitePageQuery::create()->findOneById($aPage['id']);
        $aFooterTranslationBlocks = null;
        if($aPage['id'] && $oSitePage->getLanguage())
        {
            $aSiteFooterBlocks = SiteFooterBlockQuery::create()->filterByType('cms')->findBySiteId($oSite->getId());
            if(!$aSiteFooterBlocks->isEmpty())
            {
                $aFooterTranslationBlocks = [];
                foreach($aSiteFooterBlocks as $oSiteFooterBlock)
                {
                    $oSiteFooterBlockTranslation = SiteFooterBlockTranslationQuery::create()->filterBySiteFooterBlockId($oSiteFooterBlock->getId())->filterByLanguageId($oSitePage->getLanguageId())->findOne();
                    $aFooterTranslationBlocks[] = ['title' => $oSiteFooterBlockTranslation->getTitle(), 'site_footer_block_translation_id' => $oSiteFooterBlockTranslation->getId()];
                }
            }


            $oSiteFooterBlockTranslationSitePageQuery = SiteFooterBlockTranslationSitePageQuery::create();
            $oSiteFooterBlockTranslationSitePageQuery->filterBySitePageId($oSitePage->getId());
            $oSiteFooterBlockTranslationSitePage = $oSiteFooterBlockTranslationSitePageQuery->findOne();
            $iSiteFooterBlockTranslationId = null;
            if($oSiteFooterBlockTranslationSitePage instanceof SiteFooterBlockTranslationSitePage)
            {
                $iSiteFooterBlockTranslationId = $oSiteFooterBlockTranslationSitePage->getSiteFooterBlockTranslationId();
            }
        }
        $sEditForm = $this->oCrudManager->getRenderedEditHtml($oUsp, $sEditConfigKey, $oRenderEditConfig);

        $sEditForm = str_replace('<textarea', '<textarea style="height:400px;"', $sEditForm);
        $aMainViewData = [
            'site' => $sSite,
            'edit_form' => $sEditForm,
            'site_page_id' => $aPage['id'],
            'site_page_images' => $aSitePagePictures,
            'site_footer_block_translation_id' => $iSiteFooterBlockTranslationId,
            'footer_translation_blocks' => $aFooterTranslationBlocks
        ];
        $sEdit = $this->parse('Cms/Page/edit.twig', $aMainViewData);

        $aSitePageViewData = [
            'site' => $sSite,
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'module' => 'Site_page',
            'manager' => 'Site_pageManager',
            'fields_config_title' => $sEditViewTitle,
            'fields_config_code' => $sEditConfigKey
        ];

        $sTopNav = $this->parse('Cms/Page/edit_top_nav.twig', $aSitePageViewData);
        $aView =
            [
                'title' => Translate::fromCode('Statische pagina bewerken'),
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }

    function doStore()
    {

        $sNextOverview = $this->post('next_overview');
        $aPage = $this->post('data', null);
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if($this->oCrudManager->isValid($aPage))
        {
            StatusMessage::success("Wijzigingen opgeslagen");

            $aPage['site_id'] = $oSite->getId();

            $oPage = $this->oCrudManager->save($aPage);
            $sSite = $oPage->getSite()->getDomain();

            $iId = $oPage->getId();

            if(!empty($_FILES['new_image']) && $_FILES['new_image']['error'] == 0)
            {
                $oSitePictureForSorting = SitePagePictureQuery::create()->orderBySorting(Criteria::DESC)->filterBySitePageId($iId);
                if($oSitePictureForSorting instanceof SitePagePicture)
                {
                    $iSorting = $oSitePictureForSorting->getSorting() + 1;
                }
                else
                {
                    $iSorting = 0;
                }
                $oSitePagePicture = new SitePagePicture();
                $oSitePagePicture->setSorting($iSorting);
                $oSitePagePicture->setSitePageId($iId);
                $oSitePagePicture->setFileMime($_FILES['new_image']['type']);
                $oSitePagePicture->setFileSize($_FILES['new_image']['size']);
                $oSitePagePicture->setOriginalName($_FILES['new_image']['name']);

                $sExt = pathinfo($_FILES['new_image']['name'], PATHINFO_EXTENSION);
                $oSitePagePicture->setFileExt(strtolower($sExt));
                $oSitePagePicture->save();

                $sDir = $oSitePagePicture->getImageDir();

                if(!is_dir($sDir))
                {
                    echo "makedir";
                    mkdir($sDir, 0777, true);
                    chmod($sDir, 0777);
                }

                move_uploaded_file($_FILES['new_image']['tmp_name'], $sDir.$oSitePagePicture->getId().'.'.$oSitePagePicture->getFileExt());
                StatusMessage::success("Afbeelding ".$_FILES['new_image']['name']." toegevoegd.");
            }
            if(isset($aPage['site_footer_block_translation_id']) && $aPage['site_footer_block_translation_id'])
            {
                $oSiteFooterBlockTranslationSitePageQuery = SiteFooterBlockTranslationSitePageQuery::create();
                $oSiteFooterBlockTranslationSitePageQuery->filterBySitePageId($iId);
                $oSiteFooterBlockTranslationSitePage = $oSiteFooterBlockTranslationSitePageQuery->findOne();

                if(!$oSiteFooterBlockTranslationSitePage instanceof SiteFooterBlockTranslationSitePage)
                {
                    $oSiteFooterBlockTranslationSitePage = new SiteFooterBlockTranslationSitePage();
                    $oSiteFooterBlockTranslationSitePage->setSitePageId($iId);
                }
                $oSiteFooterBlockTranslationSitePage->setSiteFooterBlockTranslationId($aPage['site_footer_block_translation_id']);
                $oSiteFooterBlockTranslationSitePage->save();
            }
            else
            {
                $oSiteFooterBlockTranslationSitePageQuery = SiteFooterBlockTranslationSitePageQuery::create();
                $oSiteFooterBlockTranslationSitePageQuery->filterBySitePageId($iId);
                $oSiteFooterBlockTranslationSitePageQuery->delete();
            }

            if($sNextOverview == '1')
            {
                $this->redirect("/cms/page/overview?site=$sSite");
            }
            else
            {
                $this->redirect("/cms/page/edit?site=$sSite&id=$iId");
            }

        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aPage);
        }
    }

    function doSortImages()
    {
        $iSitePageId = $this->get('id', null, true, 'numeric');
        $sNewOrder = $this->get('new_order', null, true);
        $sSite = $this->get('site', null, true);

        $aNewOrder = array_filter(explode(',', $sNewOrder));

        if(empty($aNewOrder))
        {
            throw new LogicException("Nieuwe volgorde is leeg, deze bug is gemeld.");
        }

        $i = 0;
        foreach($aNewOrder as $iSitePagePictureId)
        {
            SitePagePictureQuery::create()->filterBySitePageId($iSitePageId)->findOneById($iSitePagePictureId)->setSorting($i)->save();
            ++$i;
        }
        StatusMessage::success("Volgorde gewijzgd.");

        $this->redirect('/cms/page/edit?site='.$sSite.'&id='.$iSitePageId);
    }

    function doDeletePicture()
    {
        $iSitePageId = $this->get('id', null, true, 'numeric');
        $iImageId = $this->get('image_id', null, true, 'numeric');
        $sSite = $this->get('site', null, true);
        $oSitePage = SitePageQuery::create()->findOneById($iSitePageId);
        $oSitePagePicture = SitePagePictureQuery::create()->findOneById($iImageId);

        if($oSitePage->getId() == $oSitePagePicture->getSitePageId())
        {
            $sImageFile = $oSitePagePicture->getImageDir().$oSitePagePicture->getId().'.'.$oSitePagePicture->getFileExt();
            if(file_exists($sImageFile))
            {
                unlink($sImageFile);
            }
            $oSitePagePicture->delete();
        }
        StatusMessage::success("Afbeelding verwijderd.");
        $this->redirect('/cms/page/edit?site='.$sSite.'&id='.$oSitePage->getId());
    }
}
