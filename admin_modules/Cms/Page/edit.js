
$('.sortable_container' ).sortable({
    connectWith: '.sortable_container',
    stop : function()
    {
        var sIds = '';
        var iProductId = null, sSite = null;

        $('.sortable_container div.sortable_item').each(function(i, e){
            console.log(e);
            sIds += $(e).data('picture_id')+',';
            iProductId = $(e).data('site_page_id');
            sSite = $(e).data('site');

        });
        window.location = '/cms/page/edit?site='+sSite+'&id='+iProductId+'&_do=SortImages&new_order='+sIds;
    }
});
function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    if (!txtarea) { return; }

    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    } else if (br == "ff") {
        strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0, strPos);
    var back = (txtarea.value).substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var ieRange = document.selection.createRange();
        ieRange.moveStart ('character', -txtarea.value.length);
        ieRange.moveStart ('character', strPos);
        ieRange.moveEnd ('character', 0);
        ieRange.select();
    } else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }

    txtarea.scrollTop = scrollPos;
}

$('.dz-remove.dz-imgtag').on('click', function(e)
{
    sImageTag = '<img src="'+$(this).attr('href')+'">';
    insertAtCaret('fld_content', sImageTag);
    e.preventDefault();
    alert("We have inserted the image tag at the cursor position.");
});