<?php
namespace AdminModules\Cms\Page;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_page\CrudSite_pageManager;
use Exception\LogicException;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Helper\PageHelper;
use Model\Cms\SitePagePictureQuery;
use Model\Cms\SiteQuery;
use Model\Cms\SitePageQuery;
use Model\Setting\CrudManager\CrudView;

class OverviewController extends GeneralController
{
    function run()
    {


        // Dit is een GET variabele
        $sSite = $this->get('site', null, true);
        $aGet = $this->get();

        // In de get variabele zit mogelijk een kolom waarop gesorteerd moet worden en een richting.
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        // Crtud manager klasse, heb jij net zelf gemaakt
        $oCrudSite_pageManager = new CrudSite_pageManager();

        // Deze haalt de juiste kolommen op, die ophet scherm moeten komen
        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_pageManager);

        // Voor de IDE en "defensive programming" dat de boel gelijk stuk gaat als het niet klopt.
        if (!$oCrudview instanceof CrudView)
        {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }
        // Propel is een libarry die klassen voor ons maakt (met dat migrate script).
        // Dat ding maakt altijd 2 objecten per tabel, ValueObject (getter/setter) en Query object.
        // Hier haal ik een instantie van het query object op.
        $oSitePageQuery = SitePageQuery::create();
        $oSitePageQuery->orderBy($sSortField, $sSortDirection);

        // Echt een array met velden uit hget object halen om op het scherm te tonen.
        $aFields = CrudViewManager::getFields($oCrudview);

        //Dit ding voegt onzichtbare filters toe.
        $oSitePageQuery = FilterHelper::applyFilters($oSitePageQuery, $oCrudview->getId());

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($oCrudview->getId());
        $oSitePageQuery = FilterHelper::generateVisibleFilters($oSitePageQuery, $aFilters);

        $iP = $this->get('p', 1, false, 'numeric');
        // Voeg array / iterterator toe aan CrudManager klasse om tabel te renderren.
        $oCrudSite_pageManager->setOverviewData($oSitePageQuery, $iP);
        // Dit argument is de GET site variabele, deze hebben we nodig bij het sorteren omdat we bovenin deze klasse hebben
        // gegegven dat site verplicht is.
        $oCrudSite_pageManager->setArgument('site', $sSite);

        // Hier vragen we de html tabel op

        $sViewData = $oCrudSite_pageManager->getOverviewHtml($aFields);

        $aMainViewData = [
            'overview_table' => $sViewData,
            'filter_html' => FilterHelper::renderHtml($oCrudview->getId(), ['extra_hidden_fields' => ['site' => $sSite]])
        ];
        $aSitePageViewData = [
            'tab_id' => $oCrudview->getId(),
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudSite_pageManager->getCreateNewUrl(),
            'page_tab_id' => $oCrudview->getId(),
            'new_title' => $oCrudSite_pageManager->getNewFormTitle(),
            'fields_config_code' => 'site_nav_overview',
            'manager' => 'Site_pageManager',
            'module' => 'Site_page',
            'site' => $this->get('site', null, true)
        ];

        //return $this->runCrud(new CrudProduct_typeManager(), ProductTypeQuery::create());
        $sEdit = $this->parse('Cms/Page/overview.twig', $aMainViewData);

        $sTopNav = $this->parse('Cms/Page/overview_top_nav.twig', $aSitePageViewData);
        $aView = [
            'title' => Translate::fromCode('Content management systeem'),
            'top_nav' => $sTopNav,
            'content' => $sEdit
        ];
        return $aView;
    }

    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iId = $this->get('id', null, true, 'numeric');

        $oSiteQuery = SiteQuery::create();
        $oSiteQuery->filterByDomain($sSite);
        $oSite = $oSiteQuery->findOne();

        $oSitePageQuery = SitePageQuery::create();
        $oSitePageQuery->filterBySiteId($oSite->getId());
        $oSitePageQuery->filterById($iId);
        $oSitePage = $oSitePageQuery->findOne();

        $oSitePagePictureQuery = SitePagePictureQuery::create();
        $oSitePagePictureQuery->filterBySitePageId($oSitePage->getId());
        $aPictures = $oSitePagePictureQuery->find();

        foreach($aPictures as $oPicture)
        {
            $aFiles = glob($oPicture->getImageDir().'/*');

            if(!empty($aFiles))
            {
                foreach($aFiles as $sFile)
                {
                    unlink($sFile);
                }
            }
            $oPicture->delete();
        }

        $oSitePage->delete();
        StatusMessage::success('Pagina verwijderd');
        $this->redirect('/cms/page/overview?site='.$sSite);
    }
}
