<?php
namespace AdminModules\Cms\Holiday_message;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;

class EditController extends MainController
{
    function doStore()
    {
        $aData = $this->post('data');

        Setting::store('display_holiday_message', ($aData['display_holiday_message'] == '1') ? '1' : '0');
        Setting::store('title', $aData['title']);
        Setting::store('content', $aData['content']);
        StatusMessage::info(Translate::fromCode("Changes saved"));
        $this->redirect($this->getRequestUri());
        exit();
    }
    function run()
    {
        $bDisplayHolidayMessage = (Setting::get('display_holiday_message', '0') == 1);
        $sTitle = Setting::get('title', null);
        $sContent = Setting::get('content', null);

        $aTopNavData = [];

        $aData = [
            'display_holiday_message' => $bDisplayHolidayMessage,
            'title' => $sTitle,
            'content' => $sContent
        ];

        $aResult = [
            'content' => $this->parse('Cms/Holiday_message/holiday_message.twig', $aData),
            'top_nav' => $this->parse('Cms/Holiday_message/top_nav.twig', $aTopNavData),
            'title' => Translate::fromCode("Vakantiebericht"),
        ];

        return $aResult;

    }

}
