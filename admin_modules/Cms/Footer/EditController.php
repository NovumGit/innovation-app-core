<?php
namespace AdminModules\Cms\Footer;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Cms\SiteFooterBlock;
use Model\Cms\SiteFooterBlockQuery;
use Model\Cms\SiteFooterBlockTranslation;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Model\Cms\SiteQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

class EditController extends GeneralController  {

    function doStore()
    {
        $aData = $this->post('data', null, true, 'array');
        $sSite = $this->get('site', null, true);

        $oSite = SiteQuery::create()->findOneByDomain($sSite);
        $oSite->setNumFooterBlocks($aData['num_footer_blocks']);
        $oSite->save();

        if(is_numeric($aData['num_footer_blocks']) && $aData['num_footer_blocks'] > 1)
        {
            for($i=1; $i < ($aData['num_footer_blocks']+1); $i++)
            {
                $oSiteFooterBlock = SiteFooterBlockQuery::create()->filterBySiteId($oSite->getId())->filterBySorting($i)->findOne();
                if(!$oSiteFooterBlock instanceof SiteFooterBlock)
                {
                    $oSiteFooterBlock = new SiteFooterBlock();
                }
                $oSiteFooterBlock->setSorting($i);
                $oSiteFooterBlock->setSiteId($oSite->getId());

                if(isset($aData['footer_block'][$i]['type']))
                {
                    $oSiteFooterBlock->setType($aData['footer_block'][$i]['type']);
                }
                if(isset($aData['footer_block'][$i]['width']))
                {
                    $oSiteFooterBlock->setWidth($aData['footer_block'][$i]['width']);
                }
                $oSiteFooterBlock->save();

                if(isset($aData['footer_block_translation'][$i]))
                {
                    foreach($aData['footer_block_translation'][$i] as $iLanguageId => $aDataRow)
                    {
                        $oSiteFooterBlockTranslation = SiteFooterBlockTranslationQuery::create()
                                                        ->filterBySiteFooterBlockId($oSiteFooterBlock->getId())
                                                        ->filterByLanguageId($iLanguageId)
                                                        ->findOne();

                        if(!$oSiteFooterBlockTranslation instanceof SiteFooterBlockTranslation)
                        {
                            $oSiteFooterBlockTranslation = new SiteFooterBlockTranslation();
                            $oSiteFooterBlockTranslation->setSiteFooterBlockId($oSiteFooterBlock->getId());
                            $oSiteFooterBlockTranslation->setLanguageId($iLanguageId);
                        }

                        if(isset($aDataRow['title']))
                        {
                            $oSiteFooterBlockTranslation->setTitle($aDataRow['title']);
                        }
                        if(isset($aDataRow['html_contents']))
                        {
                            $oSiteFooterBlockTranslation->setHtmlContents($aDataRow['html_contents']);
                        }
                        $oSiteFooterBlockTranslation->save();
                    }
                }
            }
        }

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect("/cms/footer/edit?site=$sSite");
    }

    function run()
    {
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        $aSiteFooterBlocks = [];
        $aLanguageObjects = LanguageQuery::create()->findByIsEnabledWebshopOrIsEnabledCms();

        for($i = 1; $i < ($oSite->getNumFooterBlocks() + 1); $i ++)
        {
            $oSiteFooterBlock = SiteFooterBlockQuery::create()->filterBySiteId($oSite->getId())->filterBySorting($i)->findOne();
            if(!$oSiteFooterBlock instanceof SiteFooterBlock)
            {
                $oSiteFooterBlock = new SiteFooterBlock();
                $oSiteFooterBlock->setSorting($i);
                $oSiteFooterBlock->setWidth(3);
            }

            $aLanguages = null;
            foreach($aLanguageObjects as $oLanguage)
            {
                if(!$oLanguage instanceof Language)
                {
                    throw new LogicException("Expected an instance of Language");
                }
                $oSiteFooterBlockTranslation = SiteFooterBlockTranslationQuery::create()
                                                ->filterBySiteFooterBlockId($oSiteFooterBlock->getId())
                                                ->filterByLanguageId($oLanguage->getId())
                                                ->findOne();

                if(!$oSiteFooterBlockTranslation instanceof SiteFooterBlockTranslation)
                {
                    $oSiteFooterBlockTranslation = new SiteFooterBlockTranslation();
                }
                $aLanguages[] = [
                    'object' => $oLanguage,
                    'site_footer_block_translation' => $oSiteFooterBlockTranslation
                ];
            }


            $aSiteFooterBlocks[] = ['object' => $oSiteFooterBlock, 'languages' => $aLanguages];
        }

        $aLanguages = [];
        $aLabels = [
            1 => Translate::fromCode("Eerste blok"),
            2 => Translate::fromCode("Tweede blok"),
            3 => Translate::fromCode("Derde blok"),
            4 => Translate::fromCode("Vierde blok"),
            5 => Translate::fromCode("Vijfde blok"),
            6 => Translate::fromCode("Zesde blok"),
            7 => Translate::fromCode("Zevende blok"),
            8 => Translate::fromCode("Achtste blok"),
        ];

        $aMain = [
            'site' => $oSite,
            'labels' => $aLabels,
            'languages' => $aLanguages,
            'site_footer_blocks' => $aSiteFooterBlocks
        ];

        $aTop = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'site' => $sSite
        ];

        $aOut = [
            'title' => Translate::fromCode('Website footer menu instellen'),
            'content' => $this->parse('Cms/Footer/edit.twig', $aMain),
            'top_nav' => $this->parse('Cms/Footer/top_nav.twig', $aTop),
        ];
        return $aOut;
    }
}