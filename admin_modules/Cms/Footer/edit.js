var oBlockType = $('.block_type');

oBlockType.on('click', function(/*e*/)
{
    var oScope = $(this).parent().parent().parent().parent();
    $('.conditional_display', oScope).css('display', 'none');
    $('.display_on_'+$(this).val(), oScope).css('display', 'block');
});
oBlockType.trigger('click');