<?php
namespace AdminModules\Cms\Faq;

use AdminModules\Cms\GeneralController;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_FAQ\CrudSite_FAQManager;
use Exception\LogicException;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Model\Cms\SiteBlogQuery;
use Model\Cms\SiteFAQQuery;
use Model\Setting\CrudManager\CrudView;

class OverviewController extends GeneralController
{
    function run()
    {
        $sSite = $this->get('site', null, true);
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';
        $oCrudSite_faqManager = new CrudSite_FAQManager();
        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_faqManager);
        $sTitle = Translate::fromCode('Veel gestelde vragen');
        if (!$oCrudview instanceof CrudView)
        {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }
        $oSiteFAQQuery = SiteFAQQuery::create();
        $oSiteFAQQuery->orderBy($sSortField, $sSortDirection);

        $aFields = CrudViewManager::getFields($oCrudview);

        $oSiteFAQQuery = FilterHelper::applyFilters($oSiteFAQQuery, $oCrudview->getId());

        $oSiteFAQQuery = FilterHelper::applyFilters($oSiteFAQQuery, $oCrudview->getId());
        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($oCrudview->getId());

        $oSiteFAQQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oSiteFAQQuery, $aFilters, null);

        $aSitePageItems = $oSiteFAQQuery->find();

        $oCrudSite_faqManager->setOverviewData($aSitePageItems);
        $oCrudSite_faqManager->setArgument('site', $sSite);

        $aFilterArguments = [
            'extra_hidden_fields' => ['site' => $sSite]
        ];

        $aMainViewData = [
            'title' => $sTitle,
            'filter_html' =>  FilterHelper::renderHtml($oCrudview->getId(), $aFilterArguments),
            'overview_table' => $oCrudSite_faqManager->getOverviewHtml($aFields)
        ];
        DeferredAction::register('do_after_save', $this->getRequestUri());

        $aSitePageViewData = [
            'do_after_save' => 'do_after_save',
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudSite_faqManager->getCreateNewUrl(),
            'tab_id' => $oCrudview->getId(),
            'new_title' => $oCrudSite_faqManager->getNewFormTitle(),
            'fields_config_code' => 'site_faq_overview',
            'manager' => 'Site_FAQManager',
            'module' => 'Site_FAQ',
            'site' => $this->get('site', null, true)
        ];
        $aView = [
            'title' => $sTitle,
            'top_nav' => $this->parse('Cms/Faq/overview_top_nav.twig', $aSitePageViewData),
            'content' => $this->parse('generic_overview.twig', $aMainViewData)
        ];
        return $aView;
    }
    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iId = $this->get('id', null, true, 'numeric');

        $oSiteBlogQuery = SiteFAQQuery::create();
        $oSiteBlogQuery->filterById($iId);
        $oBlog = $oSiteBlogQuery->findOne();

        $oBlog->delete();
        StatusMessage::success('FAQ verwijderd');
        $this->redirect('/cms/faq/overview?site='.$sSite);
    }
}
