<?php
namespace AdminModules\Cms\Faq;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Site_FAQ\CrudSite_FAQManager;
use Exception\LogicException;
use Helper\EditviewHelper;
use Model\Cms\Site;
use Model\Cms\SiteQuery;

class EditController extends GeneralController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sSite = $this->get('site', null, true);
        $aUsp = $this->post('data', null);
        $this->oCrudManager = new CrudSite_FAQManager($aUsp);
        $this->oCrudManager->setArgument('site', $sSite);
    }
    function run()
    {
        $sSite = $this->get('site', null, true);

        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("Kon site niet vinden $sSite.");
        }


        $sEditConfigKey = 'website_faq_editor';
        $sEditViewTitle = 'FAQ bewerken';

        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sEditViewTitle);

        $aBlog = $this->post('data', null);
        $aBlog['id'] = $this->get('id');
        $aBlog['site_id'] = $oSite->getId();

        $oUsp = $this->oCrudManager->getModel($aBlog);

        //$aSitePagePictures = SitePagePictureQuery::create()->orderBySorting()->filterBySitePageId($aPage['id'])->find();

        $aMainViewData = [
            'site' => $sSite,
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oUsp, $sEditConfigKey),
            'site_blog_id' => $aBlog['id'],
            // 'site_page_images' => $aSitePagePictures,
        ];
        $sEdit = $this->parse('Cms/Faq/faq_edit.twig', $aMainViewData);


        $aSitePageViewData = [
            'site' => $sSite,
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'module' => 'Site_FAQ',
            'manager' => 'Site_FAQManager',
            'fields_config_title' => $sEditViewTitle,
            'fields_config_code' => $sEditConfigKey
        ];
        $sTopNav = $this->parse('Cms/Faq/edit_top_nav.twig', $aSitePageViewData);
        $aView =
            [
                'title' => Translate::fromCode("Veel gestelde vragen"),
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }

    function doStore()
    {
        $sNextOverview = $this->post('next_overview');
        $aPage = $this->post('data', null);
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if($this->oCrudManager->isValid($aPage))
        {
            StatusMessage::success("Wijzigingen opgeslagen");

            $aPage['site_id'] = $oSite->getId();

            $oPage = $this->oCrudManager->save($aPage);
            $sSite = $oPage->getSite()->getDomain();

            $iId = $oPage->getId();
            if($sNextOverview == 1)
            {
                $this->redirect("/cms/faq/overview?site=$sSite");
            }
            else
            {
                $this->redirect("/cms/faq/edit?site=$sSite&id=$iId");
            }

        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aPage);
        }
    }
}
