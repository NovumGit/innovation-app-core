<?php
namespace Modules\Cms\Navigation;

use Crud\Site_nav\CrudSite_navManager;

use Core\MainController;
use Core\StatusMessage;
use Helper\CrudHelper;
use Helper\EditviewHelper;
use model\Cms\Nav\SiteNavSiteConfigBlog;
use model\Cms\Nav\SiteNavSiteConfigBlogQuery;
use model\Cms\Nav\SiteNavSiteConfigFormQuery;
use model\Cms\SiteBlog;
use model\Cms\SiteBlogQuery;
use model\Cms\SiteQuery;
use model\Cms\Nav\SiteNavSiteConfigCmsPageQuery;
use model\Cms\Nav\SiteNavSiteConfigThumbnailPage;
use model\Cms\Nav\SiteNavSiteConfigThumbnailPageQuery;
use model\Cms\Nav\SiteNav;
use model\Cms\Nav\SiteNavSiteConfigCmsPage;
use model\Cms\Nav\SiteNavSiteConfigForm;
use model\Cms\Nav\SiteNavSiteNavPageType;
use model\Cms\Nav\SiteNavPageTypeQuery;
use model\Cms\Nav\SiteNavSiteNavPageTypeQuery;
use model\Cms\SitePageQuery;
use model\Setting\CrudManager\Base\CrudEditorQuery;
use model\Setting\CrudManager\CrudEditor;

class EditController extends MainController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $aUser = $this->post('data', null);
        $this->oCrudManager = new CrudSite_navManager($aUser);
    }
    function run()
    {
        $sSite = $this->get('site', null, true);
        $sEditConfigKey = 'cms_navigation_item';

        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $this->oCrudManager->getEditFormTitle());

        $aSiteNav = $this->post('data', null);
        $aSiteNav['id'] = $this->get('id');

        $aItemPageConfig = $this->getSiteNavPageTypes($aSiteNav['id']);

        $oSiteNav = $this->oCrudManager->getModel($aSiteNav);

        $aParseData = [
            'product_id' => $aSiteNav['id'],
            'item_page_config' => $aItemPageConfig,
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oSiteNav, $sEditConfigKey)
        ];
        $sEdit = $this->parse('Cms/Navigation/navigation_edit.twig', $aParseData);

        $aTopnavEdit = [
            'site' => $sSite,
            'module' => 'Site_nav',
            'manager' => 'Site_navManager',
            'edit_config_key' => $sEditConfigKey,
            'edit_view_title' => $this->oCrudManager->getEditFormTitle()
        ];

        $sTopNav = $this->parse('Cms/Navigation/navigation_edit_top_nav.twig', $aTopnavEdit);
        $aView =
            [
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }
    function doStore()
    {

        $aSiteNav = $this->post('data', null);
        $sSite = $this->get('site', null, true);

        $aSiteNavPageTypes = $this->post('page_type', null);

        if($this->oCrudManager->isValid($aSiteNav))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $oSite = SiteQuery::create()->findOneByDomain($sSite);
            $aSiteNav['site_id'] = $oSite->getId();
            $oSiteNav = $this->oCrudManager->save($aSiteNav);

            if($oSiteNav instanceof SiteNav)
            {
                if(!empty($aSiteNavPageTypes))
                {
                    foreach($aSiteNavPageTypes as $iSiteNavPageTypeId => $sValue)
                    {
                        if($sValue == 1) {

                            $oSiteNavSiteNavPageTypeQuery = SiteNavSiteNavPageTypeQuery::create();
                            $oSiteNavSiteNavPageTypeQuery->filterBySiteNavPageTypeId($iSiteNavPageTypeId);
                            $oSiteNavSiteNavPageType = $oSiteNavSiteNavPageTypeQuery->filterBySiteNavId($oSiteNav->getId())->findOne();

                            if (!$oSiteNavSiteNavPageType instanceof SiteNavSiteNavPageType) {
                                $oSiteNavSiteNavPageType = new SiteNavSiteNavPageType();
                                $oSiteNavSiteNavPageType->setSiteNavId($oSiteNav->getId());
                                $oSiteNavSiteNavPageType->setSiteNavPageTypeId($iSiteNavPageTypeId);
                                $oSiteNavSiteNavPageType->save();
                            }

                            $oSiteNavPageType = $oSiteNavSiteNavPageType->getSiteNavPageType();
                            //site_nav_site_thumbnail_page

                            if($oSiteNavPageType->getTag() == 'thumbnail_overview')
                            {
                                $aSiteNavSiteConfigThumbnailPage = $this->post('site_nav_site_thumbnail_page');

                                if ($aSiteNavSiteConfigThumbnailPage) {

                                    $oSiteNavSiteConfigCmsPageQuery = SiteNavSiteConfigThumbnailPageQuery::create();
                                    $oSiteNavSiteConfigCmsPageQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());

                                    $oSiteNavSiteConfigThumbnailPage = $oSiteNavSiteConfigCmsPageQuery->findOne();
                                    if (!$oSiteNavSiteConfigThumbnailPage instanceof SiteNavSiteConfigThumbnailPage && $aSiteNavSiteConfigThumbnailPage['thumbnail_source'])
                                    {
                                        $oSiteNavSiteConfigThumbnailPage = new SiteNavSiteConfigThumbnailPage();
                                    }
                                    else if(!$aSiteNavSiteConfigThumbnailPage['thumbnail_source'])
                                    {
                                        $oSiteNavSiteConfigThumbnailPage->delete();
                                        continue;
                                    }
                                    if($oSiteNavSiteConfigThumbnailPage instanceof SiteNavSiteConfigThumbnailPage)
                                    {
                                        $oSiteNavSiteConfigThumbnailPage->setThumbnailSource($aSiteNavSiteConfigThumbnailPage['thumbnail_source']);
                                        $oSiteNavSiteConfigThumbnailPage->setSiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                                        $oSiteNavSiteConfigThumbnailPage->save();
                                    }
                                }
                            }
                            if ($oSiteNavPageType->getTag() == 'cms_page')
                            {
                                $aSiteNavSiteConfigCmsPage = $this->post('site_nav_site_config_cms_page');

                                if ($aSiteNavSiteConfigCmsPage) {

                                    $oSiteNavSiteConfigCmsPageQuery = SiteNavSiteConfigCmsPageQuery::create();
                                    $oSiteNavSiteConfigCmsPageQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());

                                    $oSiteNavSiteConfigCmsPage = $oSiteNavSiteConfigCmsPageQuery->findOne();

                                    if (!$oSiteNavSiteConfigCmsPage instanceof SiteNavSiteConfigCmsPage && $aSiteNavSiteConfigCmsPage['site_page_id'])
                                    {
                                        $oSiteNavSiteConfigCmsPage = new SiteNavSiteConfigCmsPage();
                                    }

                                    else if(!$aSiteNavSiteConfigCmsPage['site_page_id'] && $oSiteNavSiteConfigCmsPage)
                                    {
                                        $oSiteNavSiteConfigCmsPage->delete();
                                        continue;
                                    }
                                    if($oSiteNavSiteConfigCmsPage instanceof SiteNavSiteConfigCmsPage)
                                    {
                                        $oSiteNavSiteConfigCmsPage->setSiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                                        $oSiteNavSiteConfigCmsPage->setSitePageId($aSiteNavSiteConfigCmsPage['site_page_id']);
                                        $oSiteNavSiteConfigCmsPage->save();
                                    }
                                }
                            }
                            if($oSiteNavPageType->getTag() == 'contact_form')
                            {
                                $aForm = $this->post('site_form');

                                if($aForm)
                                {
//                                    var_dump($oSiteNavPageType);
                                    $oSiteNavSiteConfigFormQuery = SiteNavSiteConfigFormQuery::create();
                                    $oSiteNavSiteConfigFormQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                                    $oSiteNavSiteConfigForm = $oSiteNavSiteConfigFormQuery->findOne();

//                                    var_dump($oSiteNavSiteConfigForm);
//                                    exit();

                                    if(!$oSiteNavSiteConfigForm instanceof SiteNavSiteConfigForm && $aForm['form_id'])
                                    {
                                        $oSiteNavSiteConfigForm = new SiteNavSiteConfigForm();
                                    }
                                    else if(!$aForm['form_id'] && $oSiteNavSiteConfigForm instanceof SiteNavSiteConfigForm)
                                    {
                                        $oSiteNavSiteConfigForm->delete();
                                        continue;
                                    }

                                    if($oSiteNavSiteConfigForm instanceof SiteNavSiteConfigForm)
                                    {
                                        $oSiteNavSiteConfigForm->setSiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                                        $oSiteNavSiteConfigForm->setCrudEditorId($aForm['form_id']);
                                        $oSiteNavSiteConfigForm->save();
                                    }
                                }
                            }

                            if($oSiteNavPageType->getTag() == 'blog'){
                                $aBlog = $this->post('blog');

                                $oSiteNavSiteConfigBlogQuery = SiteNavSiteConfigBlogQuery::create();
                                $oSiteNavSiteConfigBlogQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                                $oSiteNavSiteConfigBlog = $oSiteNavSiteConfigBlogQuery->findOne();

                                if(!$oSiteNavSiteConfigBlog instanceof SiteNavSiteConfigBlog && $aBlog['items_pp'])
                                {
                                    $oSiteNavSiteConfigBlog = new SiteNavSiteConfigBlog();
                                }

                                if($oSiteNavSiteConfigBlog instanceof SiteNavSiteConfigBlog)
                                {
                                    $oSiteNavSiteConfigBlog->setItemsPp($aBlog['items_pp']);
                                    $oSiteNavSiteConfigBlog->setSiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());

                                    $oSiteNavSiteConfigBlog->save();
                                }

                            }
                        }
                        else
                        {
                            $oSiteNavSiteNavPageTypeQuery = SiteNavSiteNavPageTypeQuery::create();
                            $oSiteNavSiteNavPageTypeQuery->findBySiteNavId($oSiteNav->getId());
                            $oSiteNavSiteNavPageTypeQuery->filterBySiteNavPageTypeId($iSiteNavPageTypeId); // ->delete();
                            $oSiteNavSiteNavPageType = $oSiteNavSiteNavPageTypeQuery->findOne();
                            if($oSiteNavSiteNavPageType instanceof SiteNavSiteNavPageType)
                            {
                                $oSiteNavSiteNavPageType->delete();
                            }
                        }
                    }
                }
            }

            $this->redirect('/cms/navigation/edit?id='.$oSiteNav->getId().'&site='.$sSite);
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aSiteNav);
        }
    }

    private function getSiteNavPageTypes($iSiteNavId)
    {
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        $aSiteNavPageTypes = SiteNavPageTypeQuery::create()->orderByTitle()->find();
        $aOut = [];
        if(!$aSiteNavPageTypes->isEmpty())
        {
            foreach($aSiteNavPageTypes as $oSiteNavPageType)
            {
                $oSiteNavSiteNavPageTypeQuery = SiteNavSiteNavPageTypeQuery::create();
                $oSiteNavSiteNavPageTypeQuery->filterBySiteNavId($iSiteNavId);
                $oSiteNavSiteNavPageTypeQuery->filterBySiteNavPageTypeId($oSiteNavPageType->getId());

                $oSiteNavSiteNavPageType = $oSiteNavSiteNavPageTypeQuery->findOne();
                $bPageTypeEnabled = false;

                if($oSiteNavSiteNavPageType instanceof SiteNavSiteNavPageType)
                {
                    $bPageTypeEnabled = true;
                }

                $aModuleConfig = [
                    'site_nav_page_type' => $oSiteNavPageType
                ];
                //site_nav_site_thumbnail_page
                if($bPageTypeEnabled && $oSiteNavPageType->getTag() == 'thumbnail_overview')
                {
                    $oSiteNavSiteConfigThumbnailPageQuery = SiteNavSiteConfigThumbnailPageQuery::create();
                    $oSiteNavSiteConfigThumbnailPageQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                    $oSiteNavSiteConfigThumbnailPage = $oSiteNavSiteConfigThumbnailPageQuery->findOne();

                    if($oSiteNavSiteConfigThumbnailPage instanceof SiteNavSiteConfigThumbnailPage)
                    {
                        $aModuleConfig['thumbnail_source'] = $oSiteNavSiteConfigThumbnailPage->getThumbnailSource();
                    }
                }
                else if($bPageTypeEnabled && $oSiteNavPageType->getTag() == 'cms_page')
                {
                    // $oSite
                    $aPages = SitePageQuery::create()->filterBySiteId($oSite->getId())->orderByAbout();

                    $oSiteNavSiteConfigCmsPageQuery = SiteNavSiteConfigCmsPageQuery::create();
                    $oSiteNavSiteConfigCmsPageQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                    $oSiteNavSiteConfigCmsPage = $oSiteNavSiteConfigCmsPageQuery->findOne();

                    $aModuleConfig['pages'] = $aPages;
                    if($oSiteNavSiteConfigCmsPage instanceof SiteNavSiteConfigCmsPage)
                    {
                        $aModuleConfig['selected_page'] = $oSiteNavSiteConfigCmsPage->getSitePageId();
                    }
                }else if($bPageTypeEnabled && $oSiteNavPageType->getTag() == 'contact_form')
                {
                    $aForms = CrudHelper::editorCreateIfNotExists('Crud\Site_form\CrudSite_formManager');

                    $aModuleConfig['forms'] = $aForms;

                    $oSiteNavSiteConfigFormQuery = SiteNavSiteConfigFormQuery::create();
                    $oSiteNavSiteConfigFormQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                    $oSiteNavSiteConfigForm = $oSiteNavSiteConfigFormQuery->findOne();

                    if($oSiteNavSiteConfigForm instanceof SiteNavSiteConfigForm)
                    {
                        $aModuleConfig['selected_form'] = $oSiteNavSiteConfigForm->getCrudEditorId();
                    }

                }else if($bPageTypeEnabled && $oSiteNavPageType->getTag() == 'blog')
                {
                    $oSiteNavSiteConfigBlogQuery = SiteNavSiteConfigBlogQuery::create();
                    $oSiteNavSiteConfigBlogQuery->filterBySiteNavSiteNavPageTypeId($oSiteNavSiteNavPageType->getId());
                    $oSiteNavSiteConfigBlog = $oSiteNavSiteConfigBlogQuery->findOne();

                    if($oSiteNavSiteConfigBlog instanceof SiteNavSiteConfigBlog)
                    {
                        $aModuleConfig['selected_items'] = $oSiteNavSiteConfigBlog->getItemsPp();
                    }
                }



                $sHtmlEditor = $this->parse('Cms/Navigation/navigation_item_config_'.$oSiteNavPageType->getTag().'.twig', $aModuleConfig);

                $aOut[] = [
                    'site_nav_page_type' => $oSiteNavPageType,
                    'enabled' => $bPageTypeEnabled,
                    'html_editor' => $sHtmlEditor
                ];
            }
        }

        return $aOut;
    }
    /*
    function doUndelete()
    {
        StatusMessage::success("Product terug gezet.");
        $oProduct = $this->oCrudManager->getModel(['id' => $this->get('id')]);
        $oProduct->setItemDeleted(0)->save();
        $this->redirect('/product/overview');
    }
    function doDelete()
    {
        StatusMessage::info("Bedrijf verwijderd.");
        $this->oCrudManager->getModel(['id' => $this->get('id')])->setItemDeleted(1)->save();
        $this->redirect($this->oCrudManager->getOverviewUrl());
    }
    */


}
