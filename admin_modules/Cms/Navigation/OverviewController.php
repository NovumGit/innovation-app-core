<?php
namespace Modules\Cms\Navigation;

use Core\StatusMessage;
use Crud\CrudViewManager;
use CrudSiteNav
use Crud\Site_nav\CrudSite_navManager;
use Exception\LogicException;
use Helper\CrudHelper;
use Helper\FilterHelper;

use model\Cms\Nav\Base\SiteNavSiteNavPageTypeQuery;
use model\Cms\Nav\SiteNavQuery;
use model\Cms\SiteQuery;
use model\Setting\CrudManager\CrudView;
use Modules\Setting\OverviewController as ParentOverviewController;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        $sSite = $this->get('site', null, true);
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oCrudSite_navManager = new CrudSite_navManager();
        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_navManager);

        if (!$oCrudview instanceof CrudView) {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }

        $oSiteNavQuery = SiteNavQuery::create();
        $oSiteNavQuery->orderBy($sSortField, $sSortDirection);
        $aFields = CrudViewManager::getFields($oCrudview);
        $oSiteNavQuery = FilterHelper::applyFilters($oSiteNavQuery, $oCrudview->getId());

        $aSiteNavItems = $oSiteNavQuery->find();

        $oCrudSite_navManager->setOverviewData($aSiteNavItems);
        $oCrudSite_navManager->setArgument('site', $sSite);
        $sViewData = $oCrudSite_navManager->getOverviewHtml($aFields);

        $aMainViewData =
        [
            'overview_table' => $sViewData
        ];
        $aNavViewData =
        [
            'new_url' => $oCrudSite_navManager->getCreateNewUrl(),
            'new_title' => $oCrudSite_navManager->getNewFormTitle(),
            'page_tab_id' => $oCrudview->getId(),
            'fields_config_code' => 'site_nav_overview',
            'manager' => 'Site_navManager',
            'module' => 'Site_nav',
            'site' => $this->get('site', null, true)
        ];

        //return $this->runCrud(new CrudProduct_typeManager(), ProductTypeQuery::create());
        $sEdit = $this->parse('Cms/Navigation/navigation_overview.twig', $aMainViewData);
        $sTopNav = $this->parse('Cms/Navigation/navigation_overview_top_nav.twig', $aNavViewData);
        $aView =
        [
            'top_nav' => $sTopNav,
            'content' => $sEdit
        ];
        return $aView;
    }

    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iSiteNavId = $this->get('id', null, true);


        $oSite = SiteQuery::create()->findOneByDomain($sSite);
        $aSiteNav['site_id'] = $oSite->getId();

        $oSiteNav = SiteNavQuery::create()->findOneById($iSiteNavId);

        // Alles gelinkte pagina typen opruimen
        $oSiteNavSiteNavPageTypeQuery = SiteNavSiteNavPageTypeQuery::create()->findBySiteNavId($oSiteNav->getId());

        if($oSiteNavSiteNavPageType instanceof SiteNavSiteNavPageTypeQuery)
        {
            $oSiteNavSiteNavPageTypeQuery->delete();
        }


        // Sitenav item zelf verwijderen
        $oSiteNav->delete();

        $oCrudSite_navManager = new CrudSite_navManager();
        $oCrudSite_navManager->setArgument('site', $sSite);

        StatusMessage::success("Navigatie verwijderd");

        $this->redirect($oCrudSite_navManager->getOverviewUrl());

    }
}
