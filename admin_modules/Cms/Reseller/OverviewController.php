<?php
namespace AdminModules\Cms\Reseller;

use AdminModules\GenericOverviewController;
use Core\Translate;
use Crud\Link_block_cms_module\CrudLink_block_cms_moduleManager;
use Crud\Reseller\CrudResellerManager;
use Crud\FormManager;
use Model\Reseller\ResellerQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    protected function getEnabledModulesTopnav()
    {
        $sSite = $this->get('site', null, true);
        $oCrudLeftLinkBlockDetails = new CrudLink_block_cms_moduleManager();
        $aViewData = [
            'link_items' => $aLeftMenuLinkItems = $oCrudLeftLinkBlockDetails->getLinkArray('crm_enabled_modules'),
            'site' => $sSite,
            'current_url' => $this->getRequestUri()
        ];
        $sLeftPanel = $this->parse('Cms/left_menu_part.twig', $aViewData);
        return $sLeftPanel;
    }
    function getTopNavTemplate()
    {
        return 'Cms/Reseller/overview_top_nav.twig';
    }
    function getModule():string
    {
        return 'Reseller';
    }
    function getQueryObject():ModelCriteria
    {
        return ResellerQuery::create();
    }
    function getManager():FormManager
    {
        return new CrudResellerManager();
    }
    function getTitle():string
    {
        return Translate::fromCode('Sites, shops, verkoop kanalen');
    }
    function doDelete()
    {
        $iResellerId = $this->get('id', null, true, 'numeric');
        $oReseller = ResellerQuery::create()->findOneById($iResellerId);
        $oReseller->delete();
    }
}

