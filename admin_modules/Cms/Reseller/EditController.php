<?php
namespace AdminModules\Cms\Reseller;

use AdminModules\GenericEditController;
use Core\DeferredAction;
use Crud\Reseller\CrudResellerManager;
use Crud\FormManager;
use Core\Translate;

class EditController extends GenericEditController
{
    function getTopNav()
    {

        DeferredAction::register('back_reseller', $this->getRequestUri());
        $aVars = [
            'layout_key' => $this->getFormLayoutKey(),
            'site' => $this->get('site')
        ];
        return $this->parse('Cms/Reseller/top_nav_edit.twig', $aVars);
    }
    function getCrudManager():FormManager
    {
        return new CrudResellerManager();
    }
    function getPageTitle()
    {
        return Translate::fromCode('Sites, shops, verkoop kanalen');
    }

}
