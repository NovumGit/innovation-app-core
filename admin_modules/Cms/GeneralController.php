<?php
namespace AdminModules\Cms;

use Core\StatusMessage;
use Crud\Link_block_cms_module\CrudLink_block_cms_moduleManager;
use Helper\EditviewHelper;
use Core\MainController;
use Crud\Site\CrudSiteManager;
use Model\Cms\Site;
use Model\Cms\SiteQuery;

class GeneralController extends MainController
{
    private $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);

        $sDomain = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sDomain);

        if(!$oSite instanceof Site)
        {
            $oSite = new Site();
            $oSite->setDomain($sDomain);
            $oSite->save();
        }

        $aUser = $this->post('data', null);
        $this->oCrudManager = new CrudSiteManager($aUser);
    }
    protected function getEnabledModulesTopnav($bAddSaveButton = false)
    {
        $sSite = $this->get('site', null, true);
        $oCrudLeftLinkBlockDetails = new CrudLink_block_cms_moduleManager();

        $aViewData = [
            'add_save_button' => $bAddSaveButton ,
            'link_items' => $oCrudLeftLinkBlockDetails->getLinkArray('crm_enabled_modules'),
            'site' => $sSite,
            'current_url' => $this->getRequestUri()
        ];



        $sLeftPanel = $this->parse('Cms/left_menu_part.twig', $aViewData);
        return $sLeftPanel;
    }
    function run()
    {
        $sSite = $this->get('site', null, true);
        $aSite = $this->post('data', null);
        $aSite['site'] = $sSite;

        $sSiteTitle = 'Algemene instellingen';
        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, 'general_site_settings', $sSiteTitle);

        $oSite = $this->oCrudManager->getModel($aSite);

        $aViewArguments =
        [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'site_title' => $sSiteTitle,
            'edit_form' => $this->oCrudManager->getEditHtml($oSite),
            'site' => $sSite
        ];
        $aView = [
            'top_nav' => $this->parse('Cms/general_top_nav.twig', $aViewArguments),
            'content' => $this->parse('Cms/site.twig', $aViewArguments),
            'title' => $sSiteTitle,
            'module' => '',
            'manager' => ''
        ];

        return $aView;
    }

    function doStore()
    {
        $sSite = $this->get('site', null, true);
        $aData = $this->post('data', null, true, 'array');
        $aData['site'] = $sSite;

        $this->oCrudManager->save($aData);

        StatusMessage::success("Wijzigingen opgeslagen");
        
        $aArguments = ['site' => $sSite];
        $sUrl = $this->makeUrl('/cms/general', $aArguments);

        $this->redirect($sUrl);

    }

}