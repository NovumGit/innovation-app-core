<?php
namespace AdminModules\Cms\Site_navigation;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Helper\Site_navigationTreeEditorHelper;
use Model\Cms\Site_navigationQuery;

class OverviewController extends GeneralController
{
    function doStoreTreeInternal($aData = null, $iParentId = null)
    {
        $iSorting = 0;
        if(!empty($aData))
        {
            foreach($aData as $aRow)
            {
                $iSite_NavigationId = preg_replace('/^c_/', '', $aRow['id']);

                $oCategory = Site_navigationQuery::create()->findOneById($iSite_NavigationId);
                $oCategory->setParentId($iParentId);
                $oCategory->setSorting($iSorting);
                $oCategory->save();
                $iSorting++;
                if(isset($aRow['children']) && !empty($aRow['children']))
                {
                    $this->doStoreTreeInternal($aRow['children'], $iSite_NavigationId);
                }
            }
        }
    }
    function doStoreTree()
    {
        $sData = $this->post('tree');
        $aData = json_decode($sData, true);
        $this->doStoreTreeInternal($aData, null);
        StatusMessage::success("Wijzigingen opgeslagen");
        Utils::jsonOk();
    }
    function run()
    {

        if(!isset($_SESSION['WARNING_SHOWN']))
        {
            $_SESSION['WARNING_SHOWN'] = false;
        }

        if(!$_SESSION['WARNING_SHOWN'])
        {
            StatusMessage::info("Vergeet niet uw wijzigingen op te slaan. Sorteren kan met drag & drop.");
            $_SESSION['WARNING_SHOWN'] = true;
        }


        $sSite = $this->get('site', null, true);
        $this->addJsFile('/assets/plugins/jstree/dist/jstree.js');
        $this->addCssFile('/assets/plugins/jstree/dist/themes/default/style.min.css');

        $aContent = [];
        $aContent[] = "        <div id=\"jstree\">";
        $aContent[] = '            <ul>';
        $aContent[] =                   Site_navigationTreeEditorHelper::getTree(null);
        $aContent[] = '            </ul>';
        $aContent[] = '        </div>';
        $sContent = join(PHP_EOL, $aContent);

        $aOverview = [
            'site' => $sSite,
            'content' => $sContent
        ];

        $aOut = [
            'top_nav' => $this->getEnabledModulesTopnav(true),
            'content' =>  $this->parse('Cms/Site_navigation/overview.twig', $aOverview),
            'title' => Translate::fromCode('Website navigatie bewerken')
        ];
        return $aOut;
    }
}