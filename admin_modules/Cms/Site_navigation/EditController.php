<?php
namespace AdminModules\Cms\Site_navigation;

use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use AdminModules\Setting\EditController as ParentEditController;
use Crud\Site_navigation\CrudSite_navigationManager;


class EditController extends ParentEditController
{
    function __construct($aGet, $aPost)
    {
        if(isset($aGet['parent_id']))
        {
            $aPost['parent_id'] = $aGet['parent_id'];
        }

        parent::__construct($aGet, $aPost);
    }
    function doConfirmDelete()
    {
        $sSite = $this->get('site', null, true);

        $iCategoryId = $this->get('id', null, true, 'numeric');


        $sMessage = Translate::fromCode("Weet je zeker dat je dit navigatie item wilt verwijderen?");
        $sTitle = Translate::fromCode("Zeker weten?");
        $sOkUrl = '/cms/site_navigation/edit?id='.$iCategoryId.'&_do=Delete&site='.$sSite;
        $sNOUrl = '/cms/site_navigation/overview?site='.$sSite;
        $sYes = Translate::fromCode("Ja");
        $sCancel = Translate::fromCode("Annuleren");
        $aButtons  = [
            new StatusMessageButton($sYes, $sOkUrl, $sYes, 'warning'),
            new StatusMessageButton($sCancel, $sNOUrl, $sCancel, 'info'),
        ];
        StatusModal::warning($sMessage, $sTitle, $aButtons);
    }

    function getCrudManager()
    {
        return new CrudSite_navigationManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }
}
