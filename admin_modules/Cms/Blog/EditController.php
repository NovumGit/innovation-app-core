<?php
namespace AdminModules\Cms\Blog;

use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\Site_blog\CrudSite_blogManager;
use Exception\LogicException;
use Helper\EditviewHelper;
use Core\MainController;
use Model\Cms\Site;
use Model\Cms\SiteQuery;


class EditController extends MainController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sSite = $this->get('site', null, true);
        $aUsp = $this->post('data', null);
        $this->oCrudManager = new CrudSite_blogManager($aUsp);
        $this->oCrudManager->setArgument('site', $sSite);
    }
    function doEditWysiwtg()
    {
        // Does nothing but needs to be here to toggle.
    }
    function doEditPlain()
    {
        // Does nothing but needs to be here to toggle.
    }
    function run()
    {
        $sSite = $this->get('site', null, true);

        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if(!$oSite instanceof Site)
        {
            throw new LogicException("Kon site niet vinden $sSite.");
        }

        $sEditConfigKey = 'website_blog_editor';
        $sEditViewTitle = 'Blog bewerken';

        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sEditViewTitle);

        $aBlog = $this->post('data', null);
        $aBlog['id'] = $this->get('id');
        $aBlog['site_id'] = $oSite->getId();

        $oUsp = $this->oCrudManager->getModel($aBlog);

        //$aSitePagePictures = SitePagePictureQuery::create()->orderBySorting()->filterBySitePageId($aPage['id'])->find();

        $aMainViewData = [
            'site' => $sSite,
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oUsp, $sEditConfigKey),
            'site_blog_id' => $aBlog['id'],
           // 'site_page_images' => $aSitePagePictures,
        ];
        $sEdit = $this->parse('Cms/Blog/edit.twig', $aMainViewData);

        DeferredAction::register('return_blog', $this->getRequestUri());
        $aSitePageViewData = [
            'site' => $sSite,
            'do_after_save' => 'return_blog',
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'module' => 'Site_blog',
            'manager' => 'Site_blogManager',
            'fields_config_title' => $sEditViewTitle,
            'fields_config_code' => $sEditConfigKey
        ];

        $sTopNav = $this->parse('Cms/Blog/edit_top_nav.twig', $aSitePageViewData);
        $aView =
            [
                'title' => is_numeric($aBlog['id']) ? Translate::fromCode("Edit blog item") : Translate::fromCode("Create blog item"),
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }

    function doStore()
    {
        $aPage = $this->post('data', null);
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if($this->oCrudManager->isValid($aPage))
        {
            StatusMessage::success("Wijzigingen opgeslagen");

            $aPage['site_id'] = $oSite->getId();

            $oSiteBlog = $this->oCrudManager->getModel($aPage);

            if(!empty($_FILES['data']) && $_FILES['data']['error'][0] == 0)
            {

                $oSiteBlog->setFileMime($_FILES['data']['type'][0]);
                $oSiteBlog->setFileSize($_FILES['data']['size'][0]);
                $oSiteBlog->setFileOriginalName($_FILES['data']['name'][0]);
                $oSiteBlog->setFileExt(strtolower(pathinfo($_FILES['data']['name'][0], PATHINFO_EXTENSION)));

                $oSiteBlog->save();

                if(!is_dir($oSiteBlog->getImageDir()))
                {
                    mkdir($oSiteBlog->getImageDir());

                    exit('not a dir '.$oSiteBlog->getImageDir());
                }
                chmod($oSiteBlog->getImageDir(), 0777);
                move_uploaded_file($_FILES['data']['tmp_name'][0], $oSiteBlog->getImageDir().$oSiteBlog->getId().'.'.$oSiteBlog->getFileExt());
            }

            $aPage['created_by_user_id'] = User::getMember()->getId();
            if(!isset($aPage['created_on']))
            {
                $aPage['created_on'] = date("Y-m-d H:i:s");
            }

            $sSite = $oSiteBlog->getSite()->getDomain();

            $iId = $oPage->getId();

            if($this->post('next_overview') == '1')
            {
                $this->redirect($this->oCrudManager->getOverviewUrl());
            }
            else
            {
                $this->redirect("/cms/blog/edit?site=$sSite&id=$iId");
            }
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aPage);
        }
    }
}
