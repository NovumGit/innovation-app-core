<?php
namespace AdminModules\Cms\Blog;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_blog\CrudSite_blogManager;
use Exception\LogicException;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Model\Cms\SiteBlogQuery;
use Model\Setting\CrudManager\CrudView;

class OverviewController extends GeneralController
{

    function run()
    {
        $sSite = $this->get('site', null, true);
        $aGet = $this->get();

        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oCrudSite_blogManager = new CrudSite_blogManager();

        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_blogManager);

        if (!$oCrudview instanceof CrudView)
        {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }
        $oSiteBlogQuery = SiteBlogQuery::create();
        $oSiteBlogQuery->orderBy($sSortField, $sSortDirection);

        $aFields = CrudViewManager::getFields($oCrudview);

        $oSiteBlogQuery = FilterHelper::applyFilters($oSiteBlogQuery, $oCrudview->getId());

        $aSitePageItems = $oSiteBlogQuery->find();

        $oCrudSite_blogManager->setOverviewData($aSitePageItems);
        $oCrudSite_blogManager->setArgument('site', $sSite);

        $sViewData = $oCrudSite_blogManager->getOverviewHtml($aFields);

        $aMainViewData =
            [
                'overview_table' => $sViewData
            ];
        $aSitePageViewData =
            [
                'new_url' => $oCrudSite_blogManager->getCreateNewUrl(),
                'page_tab_id' => $oCrudview->getId(),
                'new_title' => $oCrudSite_blogManager->getNewFormTitle(),
                'enabled_modules' => $this->getEnabledModulesTopnav(),
                'fields_config_code' => 'site_nav_overview',
                'manager' => 'Site_blogManager',
                'module' => 'Site_blog',
                'site' => $this->get('site', null, true)
            ];

        $sEdit = $this->parse('Cms/Blog/overview.twig', $aMainViewData);
        $sTopNav = $this->parse('Cms/Blog/overview_top_nav.twig', $aSitePageViewData);
        $aView =
            [
                'title' => Translate::fromCode("Blog posts"),
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }

    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iId = $this->get('id', null, true, 'numeric');

        $oSiteBlogQuery = SiteBlogQuery::create();
        $oSiteBlogQuery->filterById($iId);
        $oBlog = $oSiteBlogQuery->findOne();

        $oBlog->delete();
        StatusMessage::success('Blog verwijderd');
        $this->redirect('/cms/blog/overview?site='.$sSite);

    }
}
