var oNodeTypeSelector = $('#fld_node_type_id');
var oFileUploader = $('#fld_file_uploader');
var oFileName = $('#fld_file_name');

oNodeTypeSelector.change(function(){
    if($('option:selected', $(this)).data('type') == 'folder')
    {
        oFileUploader.hide();
        oFileName.show();

    }
    else if($('option:selected', $(this)).data('type') == 'file')
    {
        oFileUploader.show();
        oFileName.hide();
    }

})
