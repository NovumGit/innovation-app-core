<?php
namespace AdminModules\Cms\Media;

use AdminModules\Cms\GeneralController;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Cms\CustomMedia;
use Model\Cms\CustomMediaQuery;
use Model\Cms\SiteQuery;

class StructureController extends GeneralController
{
    function doDelete()
    {
        $sSite = $this->get('site', null, true);
        $iItemId = $this->get('id', null, true, 'numeric');

        $oCustomMedia = CustomMediaQuery::create()->findOneById($iItemId);
        $oCustomMedia->deleteWithFiles();
        $iParent = $oCustomMedia->getParentId();
        $sFileType = $oCustomMedia->getCustomMediaNodeType()->getType();
        StatusMessage::success(Translate::fromCode($sFileType).' '.Translate::fromCode('verwijderd.'));

        $this->redirect("/cms/media/structure?site=$sSite&parent_id=$iParent");
    }
    function run()
    {
        $this->addCssFile('/assets/js/plugins/fancytree/skin-win8/ui.fancytree.min.css');

        $iParentId = $this->get('parent_id', 'null', false);
        $sSite = $this->get('site', null, true);
        $oSite = SiteQuery::create()->findOneByDomain($sSite);

        if($iParentId == 'null' || empty($iParentId))
        {
            $iParentId = null;
        }

        $oCustomMedia = CustomMediaQuery::create()->findOneById($iParentId);

        if($oCustomMedia instanceof CustomMedia && $oCustomMedia->getParentId()) // Item has parent
        {
            $sOverviewUrl = '/cms/media/structure?site=' . $sSite . '&parent_id=' . $oCustomMedia->getParentId();
        }
        else
        {
            $sOverviewUrl = '/cms/media/structure?site=' . $sSite;
        }

        DeferredAction::register('do_after_delete', $sOverviewUrl);

        $sSite = $this->get('site', null, true);
        $aNavViewData = [
            'parent_id' => $iParentId,
            'site' => $sSite,
            'overview_url' => $sOverviewUrl,
        ];

        $oCustomMediaQuery = CustomMediaQuery::create();
        $oCustomMediaQuery->filterBySite($oSite);
        $oCustomMediaQuery->filterByParentId($iParentId);
        $aCustomMedia = $oCustomMediaQuery->find();

        $oCurrentRoot = CustomMediaQuery::create()->findOneById($iParentId);
        if(!$oCurrentRoot instanceof CustomMedia)
        {
            $oCurrentRoot = new CustomMedia();
        }

        $sTitle = Translate::fromCode('Media en overige bestanden');
        $aData = [
            'title' => $sTitle,
            'parent_id' => $iParentId,
            'site' => $sSite,
            'current_root' => $oCurrentRoot,
            'custom_media' => $aCustomMedia
        ];
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Cms/Media/view_top_nav.twig', $aNavViewData),
            'content' => $this->parse('Cms/Media/structure.twig', $aData)
        ];
    }

}