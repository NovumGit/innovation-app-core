<?php
namespace AdminModules\Cms\Media;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Cms\CustomMediaQuery;
use Core\Cfg;

class ViewController extends MainController
{
    static $iOriginalDeleteId = null;
    function doDelete($iId = null)
    {
        if($iId == null)
        {
            $iId = $this->get('parent_id', null, false,  'numeric');
            if(!$iId)
            {
                $iId = $this->get('id', null, true,  'numeric');
            }
            self::$iOriginalDeleteId = $iId;
        }

        $aMediaItems = CustomMediaQuery::create()->findByParentId($iId);
        // Recursiveness
        if(!$aMediaItems->isEmpty())
        {
            foreach($aMediaItems as $oMediaItem)
            {
                echo $oMediaItem->getId()."<Br>";
                $this->doDelete($oMediaItem->getId());
            }
        }
        $oCustomMedia = CustomMediaQuery::create()->findOneById($iId);
        $oCustomMedia->deleteWithFiles();
        StatusMessage::info(Translate::fromCode('Media verwijderd.'));

        if(self::$iOriginalDeleteId == $iId)
        {
            // Done with recursion
            $this->redirect(DeferredAction::get('do_after_delete'));
        }
    }
    function run()
    {
        $iId = $this->get('id', null, true,  'numeric');
        $sSite = $this->get('site', null, true);

        $oCustomMedia = CustomMediaQuery::create()->findOneById($iId);

        $sOverviewUrl = '/cms/media/structure?site='.$sSite.'&parent_id='.$oCustomMedia->getParentId();
        DeferredAction::register('do_after_delete', $sOverviewUrl);

        $aNavViewData = [
            'site' => $sSite,
            'id' => $iId,
            'parent_id' => $oCustomMedia->getParentId()
        ];

        $sTitle = Translate::fromCode('Bestand bekijken');
        $aData = [
            'title' => $sTitle,
            'site' => Cfg::get('DOMAIN'),
            'site_protocol' => Cfg::get('PROTOCOL'),
            'custom_media' => $oCustomMedia
        ];
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Cms/Media/view_top_nav.twig', $aNavViewData),
            'content' => $this->parse('Cms/Media/view.twig', $aData)
        ];
    }
}