<?php
namespace AdminModules\Cms\Media;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Cms\CustomMedia;
use Model\Cms\CustomMediaNodeType;
use Model\Cms\CustomMediaNodeTypeQuery;
use Model\Cms\SiteQuery;


class AddController extends GeneralController
{
    function __construct($aGet, $aPost)
    {
        $aCustomMediaNodeTypes = ['folder', 'file'];

        foreach($aCustomMediaNodeTypes as $sNodeType)
        {
            $oCustomMediaNodeType = CustomMediaNodeTypeQuery::create()->findOneByType($sNodeType);
            if(!$oCustomMediaNodeType instanceof CustomMediaNodeType)
            {
                $oCustomMediaNodeType = new CustomMediaNodeType();
                $oCustomMediaNodeType->setType($sNodeType);
                $oCustomMediaNodeType->save();
            }
        }
        parent::__construct($aGet, $aPost);
    }

    function doStore()
    {
        $sSite = $this->get('site', null, true);
        $iParentId = $this->get('parent_id', null, true);
        $aData = $this->post('data');


        $bHasErrors = false;
        if(!$aData['node_type_id'])
        {
            StatusMessage::warning(Translate::fromCode("Geef alsjeblieft aan of het om een map of een bestand gaat"));
        }
        else
        {
            $oNodeType = CustomMediaNodeTypeQuery::create()->findOneById($aData['node_type_id']);
            if($oNodeType->getType() == 'file')
            {
                if($_FILES['file']['error'] == UPLOAD_ERR_INI_SIZE)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Het bestand wat u probeerde te uploaden is te groot, maximaal:").ini_get('upload_max_filesize'));
                }
                else if($_FILES['file']['error'] == UPLOAD_ERR_FORM_SIZE)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Het bestand is groter dan is toegestaan in dit formulier."));
                }
                else if($_FILES['file']['error'] == UPLOAD_ERR_PARTIAL)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Het bestand is maar deelk geupload."));
                }
                else if($_FILES['file']['error'] == UPLOAD_ERR_NO_FILE)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("U heeft geen bestand gekozen / toegevoegd."));
                }
                else if($_FILES['file']['error'] == UPLOAD_ERR_NO_TMP_DIR)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Door een configuratiefout op de server kon het betand niet worden toegevoegd, er is geen tmp map gedefinieerd."));
                }
                else if($_FILES['file']['error'] == UPLOAD_ERR_CANT_WRITE)
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Door een configuratiefout op de server kon het betand niet worden toegevoegd, geen schrijfrechten in de tmp map."));
                }
                if(empty($aData['name']))
                {
                    $aData['name'] = $_FILES['file']['name'];
                }
            }
            else if($oNodeType->getType() == 'folder')
            {
                if(empty($aData['name']))
                {
                    $bHasErrors = true;
                    StatusMessage::warning(Translate::fromCode("Geef alstublieft een naam op voor de map die u probeert aan te maken."));;
                }
            }
            else
            {
                throw new LogicException("Unsupported node type ".$oNodeType->getType());
            }
            if(!$bHasErrors)
            {
                $oCustomMedia = new CustomMedia();
                $oCustomMedia->setNodeTypeId($aData['node_type_id']);
                $oCustomMedia->setParentId($iParentId === 'null' || empty($iParentId) ? null : $iParentId);
                $oCustomMedia->setSite(SiteQuery::create()->findOneByDomain($sSite));
                $oCustomMedia->setName($aData['name']);
                $oCustomMedia->save();

                if($oNodeType->getType() == 'file')
                {
                    $oCustomMedia->saveFile($_FILES['file']['tmp_name']);
                }
                StatusMessage::success(Translate::fromCode($oNodeType->getType()).' '.Translate::fromCode("Opgeslagen"));
                $this->redirect('/cms/media/structure?site='.$sSite.'&parent_id='.$iParentId);
            }
        }
    }

    function run()
    {
        $this->addJsFile('/admin_modules/Cms/Media/add.js');

        $aData = $this->post('data');
        $sSite = $this->get('site', null, true);
        $aData['is_deletable'] = isset($aData['is_deletable']) ? $aData['is_deletable'] : 1;
        $sNodeTypeIsFile = false;

        if(isset($aData['node_type_id']))
        {
            $oCustomMediaNodeType = CustomMediaNodeTypeQuery::create()->findOneById($aData['node_type_id']);
            $sNodeTypeIsFile = ($oCustomMediaNodeType->getType() === 'file');
        }

        $aNavViewData = [
            'site' => $sSite,
            'new_url' => '/cms/media/add?site='.$sSite,
            'new_title' => Translate::fromCode('Nieuw bestand / map'),
            'enabled_modules' => $this->getEnabledModulesTopnav()
        ];

        $sTitle = Translate::fromCode('Media uploader');
        $aData = [
            'title' => $sTitle,
            'data' => $aData,
            'node_types' => CustomMediaNodeTypeQuery::create()->find(),
            'node_type_is_file' => $sNodeTypeIsFile
        ];
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Cms/Banner/edit_top_nav.twig', $aNavViewData),
            'content' => $this->parse('Cms/Media/add.twig', $aData)
        ];
    }



}