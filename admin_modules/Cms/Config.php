<?php
namespace AdminModules\Cms;

use AdminModules\ModuleConfig;
use Crud\Link;
use Crud\Link_block_cms_module\CrudLink_block_cms_moduleManager;
use Model\Cms\SiteQuery;

class Config extends ModuleConfig{

    function getMenuExtraVars(): array
    {
        $oCrudLeftLinkBlockDetails = new CrudLink_block_cms_moduleManager();
        $aLinks = $oCrudLeftLinkBlockDetails->getLinkArray('crm_enabled_modules');

        $oCurrentField = null;

        if(isset($aLinks[0]))
        {
            $oCurrentField = $aLinks[0];
        }

        if($oCurrentField instanceof Link)
        {
            $sUrl = $oCurrentField->getLinkUrl();
        }
        else
        {
            $sUrl =  null; // defaults to '/cms/general';
        }

        return [
            'first_module_enabled' => $sUrl
        ];

    }

    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return 'Content management systeem';
    }
    function getMenuData()
    {
        $aSites = SiteQuery::create()->find();
        return ['sites' => $aSites];
    }
}
