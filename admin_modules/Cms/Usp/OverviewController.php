<?php
namespace AdminModules\Cms\Usp;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_usp\CrudSite_uspManager;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Model\Cms\SiteUspQuery;

class OverviewController extends GeneralController
{
    function run()
    {
        $sSite = $this->get('site', null, true);

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oSiteUspQuery = SiteUspQuery::create();
        $oSiteUspQuery->orderBy($sSortField, $sSortDirection);
        $oCrudSite_uspManager = new CrudSite_uspManager();

        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_uspManager);
        $aFields = CrudViewManager::getFields($oCrudview);
        $oSiteUspQuery = FilterHelper::applyFilters($oSiteUspQuery, $oCrudview->getId());


        $aFilters = (isset($aGet['filter'])) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($oCrudview->getId());
        $oSiteUspQuery = FilterHelper::generateVisibleFilters($oSiteUspQuery, $aFilters);

        $aCompanies = $oSiteUspQuery->find();
        $oCrudSite_uspManager->setOverviewData($aCompanies);
        $oCrudSite_uspManager->setArgument('site', $sSite);
        $aViewData = [
            'overview_table' => $oCrudSite_uspManager->getOverviewHtml($aFields),
            'filter_html' => FilterHelper::renderHtml($oCrudview->getId(), ['extra_hidden_fields' => ['site'=>$sSite]]),
            'site' => $sSite
        ];

        $aTopNavVars = [
            'tab_id' => $oCrudview->getId(),
            'site' => $sSite,
            'new_url' => $oCrudSite_uspManager->getCreateNewUrl(),
            'new_title' => $oCrudSite_uspManager->getNewFormTitle()
        ];


        $sOverview = $this->parse('Cms/Usp/overview.twig', $aViewData);


        $sTopNavCustomPart = $this->parse('Cms/Usp/overview_top_nav.twig', $aTopNavVars);

        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudSite_uspManager->getCreateNewUrl(),

            'new_title' => $oCrudSite_uspManager->getNewFormTitle(),
            'fields_config_code' => 'usp_overview',
            'manager' => 'Site_uspManager',
            'module' => 'Site_usp',
            'custom_items' => $sTopNavCustomPart,
            'site' => $this->get('site', null, true)
        ];

        $sTopNav = $this->parse('Cms/general_top_nav.twig', $aSitePageViewData);

        $aView =
        [
            'title' => Translate::fromCode("Unique sellingpoints"),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
    function doDelete()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oCrudSite_uspManager = new CrudSite_uspManager();
        StatusMessage::info("USP verwijderd.");
        $oCrudSite_uspManager->getModel(['id' => $iId])->delete();
        $this->redirect($oCrudSite_uspManager->getOverviewUrl());
    }
}
