<?php
namespace AdminModules\Cms\Usp;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Site_usp\CrudSite_uspManager;
use Helper\EditviewHelper;

class EditController extends GeneralController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sSite = $this->get('site', null, true);
        $aUsp = $this->post('data', null);
        $this->oCrudManager = new CrudSite_uspManager($aUsp); 
        $this->oCrudManager->setArgument('site', $sSite);
    }
    function run()
    {
        $sEditConfigKey = 'website_usp_editor';
        $sEditViewTitle = 'USP bewerken';

        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sEditViewTitle);

        $aUsp = $this->post('data', null);
        $aUsp['id'] = $this->get('id');

        $oUsp = $this->oCrudManager->getModel($aUsp);

        $aMainViewData = [
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oUsp, $sEditConfigKey)
        ];
        $sEdit = $this->parse('Cms/Usp/edit.twig', $aMainViewData);
        $aNavViewData = [
            'site' => $this->get('site', null, true),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'fields_config_title' => $sEditViewTitle,
            'fields_config_code' => $sEditConfigKey
        ];
        $sTopNavCustomPart = $this->parse('Cms/Usp/edit_top_nav.twig', $aNavViewData);

        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'fields_config_code' => 'usp_overview',
            'manager' => 'Site_uspManager',
            'module' => 'Site_usp',
            'custom_items' => $sTopNavCustomPart,
            'site' => $this->get('site', null, true)
        ];

        $sTopNav = $this->parse('Cms/general_top_nav.twig', $aSitePageViewData);


        $aView =
            [
                'title' => Translate::fromCode("Unique sellingpoints"),
                'top_nav' => $sTopNav,
                'content' => $sEdit
        ];
        return $aView;
    }

    function doStore()
    {
        $aUsp = $this->post('data', null);

        if($this->oCrudManager->isValid($aUsp))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $oUsp = $this->oCrudManager->save($aUsp);
            $sSite = $oUsp->getSite()->getDomain();

            $iId = $oUsp->getId();
            if($this->post('next_overview') == '1')
            {
                $this->redirect($this->oCrudManager->getOverviewUrl());
            }
            else
            {
                $this->redirect("/cms/usp/edit?site=$sSite&id=$iId");
            }

        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aUsp);
        }
    }
}
