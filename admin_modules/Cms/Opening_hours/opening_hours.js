$('.time_change').change(function(e){

    var sScopeTr = $(this).parent().parent();
    var bIsOpen = false;

    if($('select.select_open', sScopeTr).val() == '00:00' && $('select.select_close', sScopeTr).val() == '00:00')
    {
        bIsOpen = false;
    }
    else
    {
        bIsOpen = true;
    }

    $('.open_checkbox', sScopeTr).prop('checked', bIsOpen);
});


$('.open_checkbox').change(function()
{
    var sScopeTr = $(this).parent().parent();
    var bIsChecked = $('.open_checkbox').is(':checked');

    if(!bIsChecked)
    {
        $('select.select_open', sScopeTr).val('00:00');
        $('select.select_close', sScopeTr).val('00:00');
    }
});