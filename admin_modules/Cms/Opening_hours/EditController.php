<?php
namespace AdminModules\Cms\Opening_hours;

use AdminModules\Cms\GeneralController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Model\System\SystemRegistryQuery;

class EditController extends GeneralController
{
    function doStore()
    {
        $aDays = $this->post('days');
        $sSite = $this->get('site');

        foreach($aDays as $iDayNum => $aVars)
        {
            $oQuery = SystemRegistryQuery::create();
            $oQuery->createOrOverwrite('is_open_'.$iDayNum, $aVars['is_open']);
            $oQuery->createOrOverwrite($iDayNum.'_open', $aVars['open']);
            $oQuery->createOrOverwrite($iDayNum.'_close', $aVars['close']);
        }
        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen."));
        $this->redirect('/cms/opening_hours/edit?site='.$sSite);
    }

    function run()
    {

        $aData = [];

        $aData['hour_ranges'] = Utils::generateDropdownHourRanges();
        $aData['days'] = [
            1 => [
                'label' => 'Maandag',
                'is_open' => SystemRegistryQuery::getVal('is_open_1'),
                'open' => SystemRegistryQuery::getVal('1_open'),
                'close' => SystemRegistryQuery::getVal('1_close'),
            ],
            2 => [
                'label' => 'Dinsdag',
                'is_open' => SystemRegistryQuery::getVal('is_open_2'),
                'open' => SystemRegistryQuery::getVal('2_open'),
                'close' => SystemRegistryQuery::getVal('2_close'),
            ],
            3 => [
                'label' => 'Woensdag',
                'is_open' => SystemRegistryQuery::getVal('is_open_3'),
                'open' => SystemRegistryQuery::getVal('3_open'),
                'close' => SystemRegistryQuery::getVal('3_close'),
            ],
            4 => [
                'label' => 'Donderdag',
                'is_open' => SystemRegistryQuery::getVal('is_open_4'),
                'open' => SystemRegistryQuery::getVal('4_open'),
                'close' => SystemRegistryQuery::getVal('4_close'),
            ],
            5 => [
                'label' => 'Vrijdag',
                'is_open' => SystemRegistryQuery::getVal('is_open_5'),
                'open' => SystemRegistryQuery::getVal('5_open'),
                'close' => SystemRegistryQuery::getVal('5_close'),
            ],
            6 => [
                'label' => 'Zaterdag',
                'is_open' => SystemRegistryQuery::getVal('is_open_6'),
                'open' => SystemRegistryQuery::getVal('6_open'),
                'close' => SystemRegistryQuery::getVal('6_close'),
            ],
            7 => [
                'label' => 'Zondag',
                'is_open' => SystemRegistryQuery::getVal('is_open_7'),
                'open' => SystemRegistryQuery::getVal('7_open'),
                'close' => SystemRegistryQuery::getVal('7_close'),
            ],
        ];

        $aTop = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            /*'custom_items' => $sTopNavCustomPart, */
            'site' => $this->get('site', null, true)
        ];

        $aView = [
            'top_nav' =>  $this->parse('Cms/general_top_nav.twig', $aTop),
            'content' => $this->parse('Cms/Opening_hours/opening_hours.twig', $aData),
            'title' => Translate::fromCode("Openingstijden")
        ];
        return $aView;
    }

}
