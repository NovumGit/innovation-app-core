<?php
namespace Modules\Cms\Form;

use Core\DeferredAction;
use Core\MainController;
use Crud\CrudViewManager;
use Modules\Generic\Crud\Editor_editController;

class OverviewController extends MainController
{

    function run()
    {

        $sSite = $this->get('site', null, true);

        $aTopViewVariables =
            [
                'site'  => $sSite
            ];

        $aOut['top_nav'] = $this->parse('Cms/editor_top_nav.twig', $aTopViewVariables);
        $aOut['content'] = $this->parse('Cms/IForm/overview.twig', $aTopViewVariables);


        return $aOut;
    }
}
