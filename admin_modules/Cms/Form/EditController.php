<?php
namespace Modules\Cms\Form;

use Core\DeferredAction;
use Crud\CrudViewManager;
use Modules\Generic\Crud\Editor_editController;

class EditController extends Editor_editController
{

    function __construct($aGet, $aPost)
    {

        $sName = $aGet['name'];
        $sTitle = $aGet['title'];

        $aGet['module'] = 'Customer';
        $aGet['manager'] = 'WebshopManager';
        $aGet['name'] = $sName;
        $aGet['title'] = $sTitle;
        $aGet['do_after_save'] = 'after_save_form_editor';
        DeferredAction::register($aGet['do_after_save'], $this->getRequestUri(true));


        $this->addJsFile('Modules/Generic/Crud/editor_edit.js');
        parent::__construct($aGet, $aPost);
    }

    function run()
    {
        $sSite = $this->get('site', null, true);
        $aOut = parent::run();

        $aTopNavVariables =
        [
            'site' =>  $sSite
        ];
        $aOut['top_nav'] = $this->parse('Cms/editor_top_nav.twig', $aTopNavVariables);

        return $aOut;
    }
}
