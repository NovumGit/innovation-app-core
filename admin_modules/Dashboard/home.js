$('.donut').each(function(i, e)
{
    c3.generate({
        bindto: e,
        color: {
            pattern: [bgDangerLr, bgSuccessLr, bgPrimaryLr]
        },
        data: {
            columns: $(e).data('chartdata'),
            type : 'donut',
            onclick: function (d, i) {
                console.log($(e).data('chartdata'));
                console.log('onclick', d, i);
            },
            onmouseover: function (d, i) { console.log('onmouseover', d, i); },
            onmouseout: function (d, i) { console.log('onmouseout', d, i); }
        },
        donut: {
            title: ''
        }
    });
});


var highColors = [bgWarning, bgPrimary, bgInfo, bgSuccess, bgDanger, bgSystem, bgDark, bgDark];
$('.line_chart').each(function(i , e)
{
    console.log($(e).data('legend'));
    var sChartId = $(e).attr('id');
    var line3 = $('#' + sChartId);
    if (line3.length) {
        $(e).highcharts({
            credits: false,
            colors: highColors,
            chart: {
                backgroundColor: '#f9f9f9',
                className: 'br-r',
                type: 'line',
                zoomType: 'x',
                panning: true,
                panKey: 'shift',
                marginTop: 25,
                marginRight: 1
            },
            title: {
                text: null
            },
            xAxis: {
                gridLineColor: '#EEE',
                lineColor: '#EEE',
                tickColor: '#EEE',
                categories: $(e).data('legend')
            },
            yAxis: {
                min: 0,
                tickInterval: 5,
                gridLineColor: '#EEE',
                title: {
                    text: null
                }
            },
            plotOptions: {
                spline: {
                    lineWidth: 3
                },
                area: {
                    fillOpacity: 0.2
                }
            },
            legend: { enabled: false },
            series: $(e).data('data')
        });
    }
});

