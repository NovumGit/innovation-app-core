<?php
namespace AdminModules\Dashboard;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Core\User;
use Crud\Desktop_widgets\AbstractDestopWidgetField;
use Crud\Desktop_widgets\CrudDesktop_widgetsManager;

class HomeController extends MainController
{
    function run()
    {

        $this->addCssFile('/assets/js/plugins/c3charts/c3.min.css');
        $this->addJsFile('/assets/js/plugins/c3charts/d3.min.js');
        $this->addJsFile('/assets/js/plugins/c3charts/c3.min.js');
        $this->addJsFile('/assets/js/plugins/c3charts/c3.min.js');

        DeferredAction::register('return_desktop', $this->getRequestUri());
        $sLayoutKey = 'desktop_widget_'.User::getRole()->getName();

        $oCrudDesktop_widgetsManager = new CrudDesktop_widgetsManager();

        $aLinkArray = $oCrudDesktop_widgetsManager->getLinkArray($sLayoutKey);
        $aWidgetHtml = [];

        if(!empty($aLinkArray))
        {
            foreach($aLinkArray as $oField)
            {
                if($oField instanceof AbstractDestopWidgetField)
                {
                    $aWidgetHtml[] = $oField->getHtml();
                }
            }
        }
        $aViewData = [
            'widget_html' => join(PHP_EOL, $aWidgetHtml)
        ];


        $aTopNavVars = [
            'layout_key' => $sLayoutKey
        ];

        $sTopNav = $this->parse('Dashboard/top_nav_overview.twig', $aTopNavVars);

        $sOverview = $this->parse('Dashboard/home.twig', $aViewData);

        $aView = [
            'title' => Translate::fromCode('Dashboard'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}
