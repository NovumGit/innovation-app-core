<?php
namespace AdminModules\Reporting\Sales\Download;

use Core\MainController;
use AdminModules\Reporting\Sales\SalesDataHelper;
use Core\Translate;

class SummaryController extends MainController
{
    function run()
    {
        $aSalesReport = SalesDataHelper::getYearTransactions();

        $sTitle = Translate::fromCode('Sales samenvatting afgelopen 12 maanden, geprint op ').date('Y-m-d');

        $iRow = 1;
        $oExcel = new \Excel($sTitle);
        $oExcel->merge(0, $iRow, 5);
        $oExcel->setValue(0,$iRow, $sTitle);
        $oExcel->backgroundCell("#fff000", 1, $iRow);
        $iRow ++;

        $oExcel->setValue(0, $iRow, Translate::fromCode('Categorie'));
        $oExcel->bold();
        $oExcel->setValue(1, $iRow, Translate::fromCode('Jaar'));
        $oExcel->bold();
        $oExcel->setValue(2, $iRow, Translate::fromCode('Maand'));
        $oExcel->bold();
        $oExcel->setValue(3, $iRow, Translate::fromCode('Bedrag'));
        $oExcel->bold();
        $iRow ++;

        $fGrandTotal = 0;
        foreach($aSalesReport as $aRowData)
        {
            $oExcel->setValue(0, $iRow, $aRowData['category_name']);
            $oExcel->setValue(1, $iRow, $aRowData['yp']);
            $oExcel->setValue(2, $iRow, $aRowData['mp']);
            $oExcel->setValue(3, $iRow, $aRowData['sum_total']);
            $fGrandTotal = $fGrandTotal + $aRowData['sum_total'];
            $iRow ++;
        }

        $oExcel->setValue(0, $iRow, Translate::fromCode('Totaal'));
        $oExcel->bold();
        $oExcel->setValue(3, $iRow, $fGrandTotal);
        $oExcel->bold();

        $oExcel->download('Sales-report-summary-'.date('Y-m-d'));
        exit();
    }

}
