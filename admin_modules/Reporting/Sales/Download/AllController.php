<?php
namespace AdminModules\Reporting\Sales\Download;

use AdminModules\Reporting\Sales\SalesDataHelper;
use Core\MainController;
use Core\Translate;

class AllController extends MainController
{
    function run()
    {
        $aSalesReport = SalesDataHelper::getYearTransactions();
        $sTitle = Translate::fromCode('Sales transacties afgelopen 12 maanden, geprint op ').date('Y-m-d');

        $iRow = 1;
        $oExcel = new \Excel($sTitle);
        $oExcel->merge(0, $iRow, 5);
        $oExcel->setValue(0,$iRow, $sTitle);
        $oExcel->backgroundCell("#fff000", 1, $iRow);
        $iRow ++;

        $oExcel->setValue(0, $iRow, Translate::fromCode('Categorie'));
        $oExcel->bold();
        $oExcel->setValue(1, $iRow, Translate::fromCode('Artikel'));
        $oExcel->bold();
        $oExcel->setValue(2, $iRow, Translate::fromCode('Voornaam'));
        $oExcel->bold();
        $oExcel->setValue(3, $iRow, Translate::fromCode('Achternaam'));
        $oExcel->bold();
        $oExcel->setValue(4, $iRow, Translate::fromCode('Land'));
        $oExcel->bold();
        $oExcel->setValue(5, $iRow, Translate::fromCode('Aangemaakt'));
        $oExcel->bold();
        $oExcel->setValue(6, $iRow, Translate::fromCode('Betaald'));
        $oExcel->bold();

        $oExcel->setValue(6, $iRow, Translate::fromCode('Betaalmethode'));
        $oExcel->bold();

        $oExcel->setValue(7, $iRow, Translate::fromCode('Verzonden'));
        $oExcel->bold();
        $oExcel->setValue(8, $iRow, Translate::fromCode('Aantal'));
        $oExcel->bold();
        $oExcel->setValue(9, $iRow, Translate::fromCode('Stukprijs'));
        $oExcel->bold();
        $oExcel->setValue(10, $iRow, Translate::fromCode('Totaal'));
        $oExcel->bold();

        $iRow ++;

        $fGrandTotal = 0;
        $aColumns  = ['category_name', 'number', 'cp_first_name', 'cp_last_name', 'customer_delivery_country', 'created_date', 'paymethod_name', 'pay_date', 'ship_date', 'quantity', 'price',  'total_price'];
        foreach($aSalesReport as $aRowData)
        {
            $iCel = 0;
            foreach($aColumns as $sColumn)
            {
                $oExcel->setValue($iCel, $iRow, $aRowData[$sColumn]);
                $iCel++;
            }
            $fGrandTotal = $fGrandTotal + $aRowData['total_price'];
            $iRow ++;
        }
        $oExcel->download('Sales-report-summary-'.date('Y-m-d'));
        exit();
    }

}
