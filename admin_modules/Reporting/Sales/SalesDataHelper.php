<?php
namespace AdminModules\Reporting\Sales;

use Core\QueryMapper;

class SalesDataHelper
{
    static function getYearTransactions()
    {
        $iStart = strtotime('-12 month');
        $iEnd = time();
        $sQuery = "SELECT
                      p.number,
                      cp_first_name,
                      cp_last_name,
                      customer_delivery_country,
                      DATE(so.created_on) created_date,
                      DATE(so.pay_date) pay_date,                      
                      DATE(so.ship_date) ship_date,
                      so.paymethod_name,
                      YEAR(pay_date) yp, 
                      MONTH(pay_date) mp,
                       category_name,
                       quantity,
                       price,
                       total_price,
                       total_price sum_total
                    FROM
                      sale_order so,
                      (SELECT *, (ROUND(soi.price * soi.quantity, 2)) total_price FROM sale_order_item soi) as soi,
                      sale_order_item_product soip,
                      product p
                      LEFT JOIN product p_parent ON p.derrived_from_id = p_parent.id                      
                    WHERE
                      soi.sale_order_id = so.id
                    AND soip.sale_order_item_id = soi.id
                    AND soip.product_id = p.id
                    AND so.is_fully_paid = 1
                    AND `pay_date` BETWEEN FROM_UNIXTIME($iStart) AND FROM_UNIXTIME($iEnd)
                     ORDER BY  
                     yp, 
                     mp
                     ";

        $aQueryResult = QueryMapper::fetchArray($sQuery);
        return $aQueryResult;
    }
    static function getYearSummaryData()
    {
        $iStart = strtotime('-12 month');
        $iEnd = time();
        $sQuery = "SELECT
                      YEAR(pay_date) yp, 
                      MONTH(pay_date) mp,
                       category_name,
                       SUM(total_price) as sum_total
                    FROM
                      sale_order so,
                      (SELECT *, (ROUND(soi.price * soi.quantity, 2)) total_price FROM sale_order_item soi) as soi,
                      sale_order_item_product soip,
                      product p
                      LEFT JOIN product p_parent ON p.derrived_from_id = p_parent.id
                    WHERE
                      soi.sale_order_id = so.id
                    AND soip.sale_order_item_id = soi.id
                    AND soip.product_id = p.id
                    AND so.is_fully_paid = 1
                    AND `pay_date` BETWEEN FROM_UNIXTIME($iStart) AND FROM_UNIXTIME($iEnd)
                    GROUP BY 
                        category_name,
                        yp, 
                        mp 
                     ORDER BY  
                     yp, 
                     mp
                     ";

        $aQueryResult = QueryMapper::fetchArray($sQuery);
        return $aQueryResult;
    }

}