<?php
namespace AdminModules\Reporting\Sales;

use Core\MainController;
use Core\Translate;
use Core\User;

class OverviewController extends MainController
{
    function run()
    {
        $this->addCssFile('/assets/js/plugins/c3charts/c3.min.css');
        $this->addJsFile('/assets/js/plugins/c3charts/d3.min.js');
        $this->addJsFile('/assets/js/plugins/c3charts/c3.min.js');


        $aMonths = [
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mrt',
            4 => 'Apr',
            5 => 'Mei',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Okt',
            11 => 'Nov',
            12 => 'Dec'
        ];


        $aPastMonths = [];
        for($i=0; $i<12; $i++)
        {
            $iCurrentMonth = strtotime("-$i month");
            $aPastMonths[$i] = [
                'name' => $aMonths[date('n', $iCurrentMonth)],
                'num' => date('n', $iCurrentMonth),
                'year' => date('Y', $iCurrentMonth)
            ];
        }
        $aPastMonths = array_reverse($aPastMonths);

        $aSalesLegendArray = [];
        foreach($aPastMonths as $aMonth)
        {
            $aSalesLegendArray[] = $aMonth['name'];
        }

        $aSalesLegend = json_encode($aSalesLegendArray);
        $aSalesLegend = htmlentities($aSalesLegend, ENT_QUOTES, 'UTF-8');

        $aQueryResult = SalesDataHelper::getYearSummaryData();
        $aTransposed = [];
        if(!empty($aQueryResult))
        {
            foreach($aQueryResult as $aRow)
            {
                $aTransposed[$aRow['category_name']][$aRow['yp']][$aRow['mp']] = $aRow['sum_total'];
            }
        }
        $aCategories = array_keys($aTransposed);

        $aSalesReport = [];
        foreach($aCategories as $sCategory)
        {
            $aReportRow = [];
            $aReportRow['name'] = $sCategory;
            $aReportRow['data'] = [];
            foreach($aPastMonths as $iRow => $sMonth)
            {

                if(!isset($aTransposed[$sCategory]))
                {
                    $aReportRow['data'][] = 0;
                    continue;
                }
                if(!isset($aTransposed[$sCategory][$sMonth['year']]))
                {
                    $aReportRow['data'][] = 0;
                    continue;
                }
                if(!isset($aTransposed[$sCategory][$sMonth['year']][$sMonth['num']]))
                {
                    $aReportRow['data'][] = 0;
                    continue;
                }
                $aReportRow['data'][] = $aTransposed[$sCategory][$sMonth['year']][$sMonth['num']];
            }
            $aSalesReport[] = $aReportRow;
        }

        $sSalesReport = json_encode($aSalesReport);
        $sSalesReport = htmlentities($sSalesReport, ENT_QUOTES, 'UTF-8');

        $sTitle = Translate::fromCode('Verkoop rapportage');

        $aCategoriesAndLabels = [];
        $i = 0;
        $aLabelColors = ['warning', 'primary', 'info', 'success', 'danger', 'system', 'dark', 'dark'] ;
        foreach($aCategories as $sCategory)
        {
            $aCategoriesAndLabels[] = ['label' => $sCategory, 'color' => $aLabelColors[$i]];
            $i++;
            if(count($aLabelColors) == $i)
            {
                $i = 0;
            }
        }

        $aTopNavData = [];
        $aData = [
            'language_code' => User::getMember()->getLanguage()->getShopUrlPrefix(),
            'sale_legend' => $aSalesLegend,
            'sale_categories' => $aCategoriesAndLabels,
            'sales_report' => $sSalesReport
        ];
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Reporting/Sales/overview_top_nav.twig', $aTopNavData),
            'content' => $this->parse('Reporting/Sales/overview.twig', $aData)
        ];
    }
}


