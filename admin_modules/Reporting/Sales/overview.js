$('.donut').each(function(i, e)
{
    c3.generate({
        bindto: e,
        color: {
            pattern: [bgDangerLr, bgSuccessLr, bgPrimaryLr]
        },
        data: {
            columns: $(e).data('chartdata'),
            type : 'donut',
            onclick: function (d, i) {
                console.log($(e).data('chartdata'));
                console.log('onclick', d, i);
            },
            onmouseover: function (d, i) { console.log('onmouseover', d, i); },
            onmouseout: function (d, i) { console.log('onmouseout', d, i); }
        },
        donut: {
            title: ''
        }
    });
});

var highColors = [bgWarning, bgPrimary, bgInfo, bgSuccess, bgDanger, bgSystem, bgDark, bgDark];

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$('.line_chart').each(function(i , e)
{
    var sChartId = $(e).attr('id');
    var line3 = $('#' + sChartId);
    var sLanguage = $(e).data('language');


    if (line3.length) {
        $(e).highcharts({
            credits: false,
            colors: highColors,
            chart: {
                backgroundColor: '#f9f9f9',
                className: 'br-r',
                type: 'line',
                zoomType: 'x',
                panning: true,
                panKey: 'shift',
                marginTop: 25,
                marginRight: 1
            },
            tooltip: {
                formatter: function() {

                    if(sLanguage == 'nl')
                    {
                        var aMAp = {
                            Jan : 'Januari',
                            Feb : 'Februari',
                            Mrt : 'Maart',
                            Apr : 'April',
                            Mei : 'Mei',
                            Jun : 'Juni',
                            Jul : 'Juli',
                            Aug : 'Augustus',
                            Sep : 'September',
                            Okt : 'October',
                            Nov : 'November',
                            Dec : 'December'
                        };
                        return 'In de productcategorie <b>' + this.series.name.toLocaleLowerCase() + '</b> heeft u in <b>' + aMAp[this.x].toLocaleLowerCase() + ' </b> <b>€' + number_format(this.y, 2, ',', '.') + '</b> omgezet.';
                    }
                    else
                    {
                        var aMAp = {
                            Jan : 'January',
                            Feb : 'February',
                            Mrt : 'March',
                            Apr : 'April',
                            Mei : 'May',
                            Jun : 'June',
                            Jul : 'July',
                            Aug : 'August',
                            Sep : 'September',
                            Okt : 'October',
                            Nov : 'November',
                            Dec : 'December'
                        };
                        return 'The turnover of <b>' + aMAp[this.x].toLocaleLowerCase() + '</b> for the category <b>' + this.series.name.toLocaleLowerCase() + '</b> was <b>€' + number_format(this.y, 2, ',', '.') + '</b>.';
                    }
                }
            },
            title: {
                text: null
            },
            xAxis: {
                gridLineColor: '#EEE',
                lineColor: '#EEE',
                tickColor: '#EEE',
                categories: $(e).data('legend')
            },
            yAxis: {
                min: 0,
                tickInterval: 500,
                gridLineColor: '#EEE',
                title: {
                    text: null
                }
            },
            plotOptions: {
                spline: {
                    lineWidth: 3
                },
                area: {
                    fillOpacity: 0.2
                }
            },
            legend: { enabled: false },
            series: $(e).data('data')
        });
    }
});

