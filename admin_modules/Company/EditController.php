<?php
namespace AdminModules\Company;

use Core\Config;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Own_company\CrudOwn_companyManager;
use Model\Company\CompanyLogo;
use Model\Company\CompanyQuery;

class EditController extends MainController
{
    private $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);

        $aUser = $this->post('data', null);
        $this->oCrudManager = new CrudOwn_companyManager($aUser);
    }
    function run()
    {
        $aCompany = $this->post('data', null);
        $aCompany['id'] = $this->get('id');
        $oCompany = $this->oCrudManager->getModel($aCompany);
        $sRenderKey = 'own_company_form';

        $aParseData =
        [
            'product_id' => $aCompany['id'],
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oCompany, $sRenderKey)
        ];
        $sEdit = $this->parse('Company/edit.twig', $aParseData);

        $aTopNavData =
        [
            'overview_url' => '/company/overview',
            'module' => 'Own_company',
            'manager' => 'Own_companyManager',
            'edit_config_key' => $sRenderKey,
            'edit_view_title' => 'Eigen bedrijf'
        ];
        $sTopNav = $this->parse('generic_top_nav_edit.twig', $aTopNavData);

        $aView =
            [
                'title' => Translate::fromCode('Over ons bedrijf'),
                'top_nav' => $sTopNav,
                'content' => $sEdit
        ];
        return $aView;
    }
    function doUndelete()
    {
        StatusMessage::success("Product terug gezet.");
        $oProduct = $this->oCrudManager->getModel(['id' => $this->get('id')]);
        $oProduct->setItemDeleted(0)->save();
        $this->redirect('/product/overview');
    }

    function doDelete()
    {
        StatusMessage::info("Bedrijf verwijderd.");
        $this->oCrudManager->getModel(['id' => $this->get('id')])->setItemDeleted(1)->save();
        $this->redirect($this->oCrudManager->getOverviewUrl());
    }
    function doStore()
    {
        $aCompany = $this->post('data', null);
        $iId = $this->get('id', null);
        if($iId)
        {
            $oCompany = CompanyQuery::create()->findOneById($iId);
        }

        if(isset($_FILES['data']) && !empty($_FILES['data']) && $_FILES['data']['error'][0] == 0 && isset($oCompany)){
            $sLogoDir = Config::getDataDir().'/pub/company_logo/';
            if(!is_dir($sLogoDir)) mkdir($sLogoDir);

            if($oCompany->getCompanyLogoRelatedByCompanyLogoId()){
                $sPossiblePreviousFile = $sLogoDir.$oCompany->getId().'.'.$oCompany->getCompanyLogoRelatedByCompanyLogoId()->getExt();
                if(file_exists($sPossiblePreviousFile)) unlink($sPossiblePreviousFile);
            }

            $sExt = pathinfo($_FILES['data']['name'][0], PATHINFO_EXTENSION);

            $oCompanyLogo = new CompanyLogo();
            $oCompanyLogo->setOwnCompanyId($oCompany->getId());
            $oCompanyLogo->setOriginalName($_FILES['data']['name'][0]);
            $oCompanyLogo->setExt($sExt);
            $oCompanyLogo->setSize($_FILES['data']['size'][0]);
            $oCompanyLogo->setMime($_FILES['data']['type'][0]);

            move_uploaded_file($_FILES['data']['tmp_name'][0], $sLogoDir.$oCompany->getId().'.'.$oCompanyLogo->getExt());

            $oCompanyLogo->save();
            $aCompany['company_logo_id'] = $oCompanyLogo->getId();
        }
        else if(isset($_FILES['data']) && !empty($_FILES['data']) && $_FILES['data']['error'][0] == 0 && !isset($oCompany))
        {
            StatusMessage::alert("U moet eerst het bedrijf toevoegen voordat u een afbeelding toe kunt voegen");
        }

        if($this->oCrudManager->isValid($aCompany))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $oCompany = $this->oCrudManager->save($aCompany);

            $sNextOverview = $this->post('next_overview');

            if($sNextOverview === '1')
            {
                $this->redirect('/company/overview');
            }
            $this->redirect('/company/edit?id='.$oCompany->getId());


        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aCompany);
        }
    }
}
