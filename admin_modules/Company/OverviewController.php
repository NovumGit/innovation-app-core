<?php
namespace AdminModules\Company;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Own_company\CrudOwn_companyManager;
use Helper\FilterHelper;
use Model\Company\Company;
use Model\Company\CompanyQuery;

use LogicException;
use model\Setting\CrudManager\CrudView;

class OverviewController extends MainController{

    function doSetDefaultSupplyingCompany()
    {
        $iCompanyId = $this->get('company_id', null, true, 'numeric');

        $aCompanies = CompanyQuery::create()->find();
        $sCompany =  null;
        $oDefaultCompany = new Company();
        foreach($aCompanies as $oCompany)
        {
            $bIsDefault = false;
            if($oCompany->getId() == $iCompanyId)
            {
                $oDefaultCompany = $oCompany;
                $bIsDefault = true;
            }
            $oCompany->setIsDefaultSupplier($bIsDefault);
            $oCompany->save();
        }

        StatusMessage::success("Standaard leverancier / bedrijf aangepast naar \"{$oDefaultCompany->getCompanyName()}\".");

        $this->redirect($this->getRequestUri(false));
    }

    function run(){

        $iTabId = $this->get('tab');

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oCompanyQuery = CompanyQuery::create();
        $oCompanyQuery->orderBy($sSortField, $sSortDirection);
        $oCrudOwn_companyManager = new CrudOwn_companyManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudOwn_companyManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }
        $aFields = CrudViewManager::getFields($oCrudview);
        $oCompanyQuery = FilterHelper::applyFilters($oCompanyQuery, $iTabId);
        $aCompanies = $oCompanyQuery->find();
        $oCrudOwn_companyManager->setOverviewData($aCompanies);

        $aViewData['overview_table'] = $oCrudOwn_companyManager->getOverviewHtml($aFields);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Own_company',
            'manager' => 'Own_companyManager',
            'overview_url' => $oCrudOwn_companyManager->getOverviewUrl(),
            'new_url' => $oCrudOwn_companyManager->getCreateNewUrl(),
            'new_title' => $oCrudOwn_companyManager->getNewFormTitle()
        ];

        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('Company/overview.twig', $aViewData);

        $aView =
        [
            'title' => Translate::fromCode("Bedrijven / handelsnamen"),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];

        return $aView;
    }
}
