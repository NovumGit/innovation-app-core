<?php
namespace AdminModules\Company;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Own_company_setting\CrudOwn_company_settingManager;

class EditsettingsController extends MainController
{
	private $oCrudManager;

	function __construct($aGet, $aPost)
	{
		parent::__construct($aGet, $aPost);

		$aUser = $this->post('data', null);
		$this->oCrudManager = new CrudOwn_company_settingManager($aUser);
	}
	function run()
	{
	    $sTitle = Translate::fromCode('Eigen bedrijf instellingen');
		$aCompany = $this->post('data', null);
		$aCompany['id'] = $this->get('company_id');
		$oCompany = $this->oCrudManager->getModel($aCompany);
		$sRenderKey = 'own_company_form';

		$aParseData = [
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oCompany, $sRenderKey)
        ];
		$sEdit = $this->parse('Company/editsettings.twig', $aParseData);

		DeferredAction::register('return_company_settings_edit', $this->getRequestUri());
		$aTopNavData = [
            'overview_url' => '/company/overview',
            'module' => 'Own_company_setting',
            'manager' => 'Own_company_settingManager',
            'edit_config_key' => $sRenderKey,
            'do_after_save' => 'return_company_settings_edit',
            'title' => $sTitle
        ];

		$sTopNav = $this->parse('generic_top_nav_edit.twig', $aTopNavData);

		$aView = [
            'title' => $sTitle,
            'top_nav' => $sTopNav,
            'content' => $sEdit
        ];
		return $aView;
	}

	function doStore()
	{
		$aCompanySettings = $this->post('data', null);

		if($this->oCrudManager->isValid($aCompanySettings))
		{
			StatusMessage::success("Wijzigingen opgeslagen");
			$oCompany = $this->oCrudManager->save($aCompanySettings);
			$this->redirect('/company/editsettings?company_id='.$oCompany->getId());
		}
		else
		{
			$this->oCrudManager->registerValidationErrorStatusMessages($aCompanySettings);
		}
	}
}
