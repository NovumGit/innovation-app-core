<?php
namespace AdminModules\Product;

use Helper\AutoTranslationHelper;
use Model\Product;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Connection\ConnectionWrapper;
use Propel\Runtime\Propel;

class AutoTranslateHelper
{
    /**
     * @param Product $oProduct
     * @return int aantal vertalingen
     */
    static function autoTranslate(Product $oProduct)
    {
        $oConnection = Propel::getWriteConnection('hurah');
        if($oConnection instanceof ConnectionWrapper)
        {

        }

        // Propel::getConnection()

        $aLanguages = LanguageQuery::create()->find();
        $iCount = 0;
        foreach($aLanguages as $oLanguage)
        {
            $iCount++;
            $oProductTranslationQuery = ProductTranslationQuery::create();
            $oProductTranslationQuery->filterByLanguage($oLanguage);
            $oProductTranslationQuery->filterByProduct($oProduct);
            $oProductTranslation = $oProductTranslationQuery->findOne();

            if(!$oProductTranslation instanceof ProductTranslation)
            {
                $oProductTranslation = new ProductTranslation();
                $oProductTranslation->setLanguage($oLanguage);
                $oProductTranslation->setProduct($oProduct);

            }

            $sTitleEn = AutoTranslationHelper::doTranslate($oProduct->getTitle(), 'nl_NL', 'en_US');
            $sDescriptionEn = AutoTranslationHelper::doTranslate($oProduct->getDescription(), 'nl_NL', 'en_US');

            if($oLanguage->getLocaleCode() == 'en_US')
            {
                $oProductTranslation->setTitle($sTitleEn);
                $oProductTranslation->setDescription($sDescriptionEn);
            }
            else if($oLanguage->getLocaleCode() == 'nl_NL')
            {
                $oProductTranslation->setTitle($oProduct->getTitle());
                $oProductTranslation->setDescription($oProduct->getDescription());
            }
            else
            {
                $sTitleEn = AutoTranslationHelper::doTranslate($oProduct->getTitle(), 'en_US', $oLanguage->getLocaleCode());
                $sDescriptionEn = AutoTranslationHelper::doTranslate($oProduct->getDescription(), 'en_US', $oLanguage->getLocaleCode());

                $oProductTranslation->setTitle($sTitleEn);
                $oProductTranslation->setDescription($sDescriptionEn);
            }

            /*
             * @todo later eventueel doen
                $oProductTranslation->setShortDescription();
                $oProductTranslation->setPageTitle();
                $oProductTranslation->setMetaKeywords();
                $oProductTranslation->setMetaDescription();
                $oProductTranslation->setMetaDescription();
            */
            $oProductTranslation->save();
        }
        return $iCount;
    }
}