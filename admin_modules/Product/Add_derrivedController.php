<?php
namespace AdminModules\Product;

use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\CustomerQuery;
use Model\Product;
use Exception\LogicException;

class Add_derrivedController extends MainController
{

    function doAddProduct()
    {
        $aData = $this->post('data');

        $oNumberExistsCheckProduct = CustomerQuery::create()->findOneByNumber($aData['number']);
        if($oNumberExistsCheckProduct instanceof Product)
        {
            StatusMessage::danger(Translate::fromCode("Het opgegeven artikelnummer bestaat al."));
            $this->redirect($this->getRequestUri());
        }

        $iSourceProductId = $this->get('from_product_id', null, true, 'numeric');
        $oSourceProduct = CustomerQuery::create()->findOneById($iSourceProductId);

        $oNewProduct = new Product();
        $oNewProduct->setCategoryId($oSourceProduct->getCategoryId());
        $oNewProduct->setDerrivedFromId($oSourceProduct->getId());
        $oNewProduct->setNumber($aData['number']);
        $oNewProduct->save();

        LogActivity::register('Product', 'Derrive', 'Heeft een variant op '.$oSourceProduct->getId().' gemaakt, variant id: '.$oNewProduct->getId());

        if($aData['after_copy'] == 'go_new')
        {
            StatusMessage::success(Translate::fromCode("De product variatie is aangemaakt, geef nu instellingen op."));
            $this->redirect('/product/edit?id='.$oNewProduct->getId());
        }
        else if($aData['after_copy'] == 'go_original')
        {
            StatusMessage::success(Translate::fromCode("De variatie is toegevoegd, u kunt nu nog meer variaties toevoegen of de toegevoegde variatie instellen."));
            $this->redirect('/product/edit?id='.$oSourceProduct->getId());
        }
        else if($aData['after_copy'] == 'stay')
        {
            StatusMessage::success(Translate::fromCode("Het product is gekopieerd"));
            $this->redirect($this->getRequestUri());
        }
        else
        {
            throw new LogicException("Unsupported action after_copy.");
        }
    }
    function run()
    {
        $sPageTitle = 'Product variatie toevoegen';
        $iFromProductId = $this->get('from_product_id', true, null, 'numeric');
        $oParentProduct = CustomerQuery::create()->findOneById($iFromProductId);

        if(!$oParentProduct instanceof Product)
        {
            throw new LogicException("Expected an instance of product but coult not find any product with product id $iFromProductId");
        }
        if($oParentProduct->getDerrivedFromId())
        {
            throw new LogicException("You cannot derrive a product from a product that is already derrived from another product.");
        }

        $aTopNavVars = [
            'from_product_id' => $iFromProductId
        ];
        $aViewData = [
            'source_product' => $oParentProduct
        ];

        $sTopNavHtml = $this->parse('Product/add_derrived_top_nav.twig', $aTopNavVars);
        $sMainWindowHtml = $this->parse('Product/add_derrived.twig', $aViewData);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];
        return $aView;
    }
}
