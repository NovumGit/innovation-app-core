<?php
namespace AdminModules\Product\Event;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\DeferredAction;
use Core\StatusMessage;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;

class Redirect extends AbstractEvent implements AbstractConfigurableEvent{

    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        DeferredAction::register('configurator_url', $this->getRequestUri());
        $iId = $this->get('event_id', null, true);
        $sValue = $this->getFieldSetting($iId.$sEnvironment, 'redirect_value');
        // $sField = $this->getFieldSetting($iId.$sEnvironment, 'redirect_field');
        $sNote = $this->getFieldSetting($iId.$sEnvironment, 'notes');
        $bAddProductIds = $this->getFieldSetting($iId.$sEnvironment, 'add_product_ids');

        $aVariables['add_product_ids'] = $bAddProductIds;
        $aVariables['current_value'] = $sValue;
        $aVariables['current_note'] = $sNote;
        $aVariables['event_id'] = $iCrudEditorButtonEventId;
        $aVariables['origin'] = 'products';
        StatusMessage::success("Wijzigingen opgeslagen");
        return $this->parse('Generic/Event/redirect.twig', $aVariables);
    }
    function saveConfiguration($sEnvironment)
    {
        $iId = $this->get('event_id', null, true);

        $this->storeFieldSetting($iId.$sEnvironment, 'add_product_ids', $this->get('add_product_ids'));
        $this->storeFieldSetting($iId.$sEnvironment, 'redirect_value', $this->get('redirect_value'));
        $this->storeFieldSetting($iId.$sEnvironment, 'redirect_field', $this->get('redirect_field'));
        $this->storeFieldSetting($iId.$sEnvironment, 'notes', $this->get('notes'));

        $sConfiguratorUrl = DeferredAction::get('configurator_url');
        $this->redirect($sConfiguratorUrl);
    }
    function getFieldSetting($iButtonEventId, $sSetting)
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey('redirect_'.$iButtonEventId.'_crud_'.$sSetting);

        if($oSystemRegistry)
        {
            return $oSystemRegistry->getItemValue();
        }
        return null;
    }
    function storeFieldSetting($iButtonEventId, $sSetting, $sValue)
    {
        $sKey = 'redirect_'.$iButtonEventId.'_crud_'.$sSetting;
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sValue);
        $oSystemRegistry->save();
    }
    function getDescription(): string
    {
        return "Redirect";
    }
    function outputType(): \core\Mime\Mime
    {
        return null;
    }
    function isConfigurable(): bool
    {
        return true;
    }
    function trigger(string $sEnvironment): void
    {
        $bAddProductIds = $this->getFieldSetting($this->getButtonEventId().$sEnvironment, 'add_product_ids');
        $sUrl = $this->getFieldSetting($this->getButtonEventId().$sEnvironment, 'redirect_value');

        if($bAddProductIds === '1')
        {
            $aProductIds = array_keys($this->get('items'));
            $sQuery = http_build_query(['product_ids' => $aProductIds]);
            $sSeg = strpos($sUrl, '?') ? '&' : '?';

            $sUrl = $sUrl.$sSeg.$sQuery;
        }
        $this->redirect($sUrl);
    }
}

