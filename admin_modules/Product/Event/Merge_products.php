<?php
namespace AdminModules\Product\Event;

use Cassandra\Exception\LogicException;
use Core\AbstractEvent;
use Core\Cfg;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\Product\IMerge;

class Merge_products extends AbstractEvent{

	function getDescription(): string
    {
		if(Cfg::get('PRODUCT_MERGE_FUNCTION') instanceof IMerge)
		{
			return "Merge products into one";
		}
		else
		{
			return "Merge products into one, implement IMerge top make this work.";
		}
	}
	function outputType(): \core\Mime\Mime
    {
		return null;
	}
	function isConfigurable(): bool
    {
		return false;
	}
	function trigger(string $sEnvironment): void
    {
		$aProductsArray = $this->get('items');
		$aProducts = array_keys($aProductsArray);
		$iProductTo = array_shift($aProducts);

		$iProductsMerged = 1;
		foreach($aProducts as $iProductFrom)
		{
			$iProductsMerged++;
			$oMerger = Cfg::get('PRODUCT_MERGE_FUNCTION');

			if(!$oMerger instanceof IMerge)
			{
				throw new LogicException("In order to use the Merge event you have to write your own merge code, this is not implemented");
			}
			$oMerger->merge($iProductFrom, $iProductTo);
		}

		$aParts = [
			$iProductsMerged,
			" " . Translate::fromCode("products merged") . " ",
			($iProductsMerged - 1),
			" " . Translate::fromCode("products deleted") . ".",
		];

		StatusMessage::success(join('', $aParts), 'Yay');


		$sBackUrl = DeferredAction::get('back_overview_url');
		$this->redirect($sBackUrl);

	}
}

