<?php
namespace AdminModules\Product\Event;

use Core\AbstractEvent;
use Core\DeferredAction;

class PickProducts extends AbstractEvent{

    function getDescription(): string
    {
        return "Redirect to deferred action (after_pick_products)";
    }
    function outputType(): \core\Mime\Mime
    {
        return null;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function trigger(string $sEnvironment): void
    {
        $sDeferredAction = DeferredAction::get('after_pick_products');
        $aProductIds = array_keys($this->get('items'));
        $this->redirect($sDeferredAction . '&' . http_build_query(['product_ids' => $aProductIds]));
    }
}

