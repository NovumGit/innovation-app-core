<?php
namespace AdminModules\Product\Event;


use Core\AbstractEvent;
use Core\Config;
use Core\Mime\ZipDirMime;
use Core\Translate;
use Core\ZipDir;
use Exception\LogicException;
use Model\Product\Image\ProductImage;
use Model\CustomerQuery;

class Download_images extends AbstractEvent
{

    function getDescription(): string
    {
        return Translate::fromCode("Download afbeeldingen");
    }
    function outputType(): \core\Mime\Mime
    {
        return new ZipDirMime();
    }
    function isConfigurable(): bool
    {
        return false;
    }

    /**
     * @param string $sEnvironment
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     */
    function trigger(string $sEnvironment): void
    {
        $iProductId = $this->get('item_id');
        $oProduct = CustomerQuery::create()->findOneById($iProductId);
        $aImages = $oProduct->getImages();

        $sDataDir = Config::getDataDir();

        $i = 1;
        if(!$aImages->isEmpty())
        {
            foreach($aImages as $oImage)
            {
                if(!$oImage instanceof ProductImage)
                {
                    throw new LogicException("Expected a ProductImage");
                }
                $sImageSource = $sDataDir.'/img/product/'.$oImage->getId().'.'.$oImage->getFileExt();
                if(file_exists($sImageSource))
                {
                    ZipDir::addFile($sImageSource, $oProduct->getId().'/fotos/'.$i.'.'.$oImage->getFileExt());
                    $i++;
                }
            }
        }
        ZipDir::addFromString($oProduct->getId().'/info.txt', 'bnlablabla');
    }
}

