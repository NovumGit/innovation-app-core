<?php
namespace AdminModules\Product\Event;

use Core\AbstractEvent;
use Core\DeferredAction;

class RelateProducts extends AbstractEvent {

    function getDescription(): string
    {
        return "Relate products (works only from product picker screen)";
    }
    function outputType(): \core\Mime\Mime
    {
        return null;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function trigger(string $sEnvironment): void
    {
        $sBaseUrl = DeferredAction::get('after_pick_multi_product');
        $aProductIds = $this->get('items');

        $aRelateProducts = [];
        foreach($aProductIds as $iProductId => $sCheckboxValue)
        {
            if($sCheckboxValue === '1')
            {
                $aRelateProducts[] = $iProductId;
            }
        }
        $sQuery = http_build_query(['product_ids' => $aRelateProducts]);
        $sFullUrl = $sBaseUrl . '&' . $sQuery;

        $this->redirect($sFullUrl);
    }
}

