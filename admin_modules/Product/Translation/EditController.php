<?php
namespace AdminModules\Product\Translation;

use Core\DeferredAction;
use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Product_translation\CrudProduct_translationManager;
use Model\CustomerQuery;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

class EditController extends MainController
{
    private $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $aProduct_translation = $this->post('data', null);
        $this->oCrudManager = new CrudProduct_translationManager($aProduct_translation);
    }
    protected  function getTopNavTemplate()
    {
        return 'Product/top_nav_edit.twig';
    }

    function run()
    {
        $iLanguageId = $this->get('language_id', true, null, 'numeric');
        $iProductId = $this->get('product_id', true, null, 'numeric');

        $oProduct = CustomerQuery::create()->findOneById($iProductId);

        $sFormLayoutKey = 'product_translation_category_'.$oProduct->getCategoryId();

        $oProductTranslation = ProductTranslationQuery::create()->filterByLanguageId($iLanguageId)->filterByProductId($iProductId)->findOne();

        if(!$oProductTranslation instanceof ProductTranslation)
        {
            $oProductTranslation = new ProductTranslation();
            $oProductTranslation->setLanguageId($iLanguageId);
            $oProductTranslation->setProductId($iLanguageId);
        }

        $sPageTitle = 'Product toevoegen';;
        $sCrudReturnUrl = "/product/translation/edit?product_id={$iProductId}&language_id={$iLanguageId}";

        $sDoAfterSaveOption = 'return_product_translation_edit';
        DeferredAction::register($sDoAfterSaveOption, $this->getRequestUri());
        $sEditForm = $this->oCrudManager->getRenderedEditHtml($oProductTranslation, $sFormLayoutKey);

        $oCurrentLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        $aTopNavVars = [
            'module' => 'Product_translation',
            /* 'from_page' => $sFromPage, */
            'manager' => 'Product_translationManager',
            'crud_return_url' => $sCrudReturnUrl,
            'category' => $oProduct->getCategory(),
            'product_id' => $iProductId,
            'webshop_languages' => LanguageQuery::create()->findByIsEnabledWebshop(true),
            'current_language' => $oCurrentLanguage,
            'name' => $sFormLayoutKey,
            'title' => $sPageTitle,
            'section' => 'languages'
        ];
        $aViewData = [
            'product_id' => $iProductId,
            'edit_form' => $sEditForm,
            'product' => $oProduct,
            'category' => $oProduct->getCategory(),
            'do_after_save' => $sDoAfterSaveOption,
            'title' => $sPageTitle
        ];

        $sTopNavHtml = $this->parse($this->getTopNavTemplate(), $aTopNavVars);
        $sMainWindowHtml = $this->parse('Product/edit.twig', $aViewData);
        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];
        return $aView;
    }
    function doStore()
    {
        StatusMessage::success(Translate::fromCode("Wijzingen opgeslagen."));
        $iProductId = $this->get('product_id', null, true, 'numeric');
        $iLanguageId = $this->get('language_id', null, true, 'numeric');

        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        if($oLanguage instanceof Language)
        {
            LogActivity::register('Product_Translation', 'Save', "Saved product translation " . $oLanguage->getDescription() . " of product " . $iProductId);
        }

        $aData = $this->post('data');
        $aData['language_id'] = $iLanguageId;
        $aData['product_id'] = $iProductId;
        $this->oCrudManager->save($aData);

        if($this->post('next_overview') == 1)
        {
            $this->redirect("/product/edit?product_id=$iProductId");
        }
        else
        {
            $this->redirect("/product/translation/edit?product_id=$iProductId&language_id=$iLanguageId");
        }
    }
}
