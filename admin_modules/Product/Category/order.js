
$('ul#products' ).sortable({
    connectWith: '.sortable_container',
    helper: 'clone',

    stop : function()
    {
    }
});

$('#fld_save').click(function(e){
    e.preventDefault();
    var aIds = [];

    $('ul#products li').each(function(i, e)
    {
        aIds.push($(e).data('product_id'));
    });

    var iProductId = $('#fld_product_id').val();
    var iCategoryId = $('#fld_category_id').val();

    $.ajax({
        url : '/product/category/order?menu_item='+iCategoryId+'&product_id='+iProductId+'&_do=ChangeSorting',
        async : true,
        method : 'POST',
        data: {'sorting' : aIds },
        beforeSend : null,
        cache : false,
        success : function(data)
        {
            window.location = window.location;
        },
        dataType : 'json',
        error : function(xhr,status,error)
        {
            // alert('1 Something went wrong, we got an error from the server: ' + error);
        }
    });

})