<?php
namespace AdminModules\Product\Category;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Model\Category\CategoryQuery;
use Model\Category\ProductMultiCategoryQuery;

class OrderController extends MainController
{

    function doChangeSorting()
    {
        $aSorting = $this->post('sorting', null, true);
        $iProductId = $this->get('product_id', null, true, 'numeric');
        $iCategoryId = $this->get('menu_item', null, true, 'numeric');

        $i=0;
        foreach($aSorting as $iProductId)
        {
            $oProductMultiCategoryQuery = ProductMultiCategoryQuery::create();
            $oProductMultiCategoryQuery->filterByProductId($iProductId);
            $oProductMultiCategoryQuery->filterByCategoryId($iCategoryId);
            $oProductMultiCategory = $oProductMultiCategoryQuery->findOne();

            $oProductMultiCategory->setSortingOrder($i);
            $oProductMultiCategory->save();
            $i++;
        }
        StatusMessage::success(Translate::fromCode('Wijzigingen opgeslagen.'));

        Utils::jsonOk();

    }

    function run()
    {
        $iCategoryId = $this->get('menu_item', null, true, 'numeric');
        $iProductId = $this->get('product_id', null, true, 'numeric');


        $oCategory = CategoryQuery::create()->findOneById($iCategoryId);

        $oProductMultiCategoryQuery = ProductMultiCategoryQuery::create();
        $oProductMultiCategoryQuery->filterByCategoryId($iCategoryId);
        $oProductMultiCategoryQuery->orderBySortingOrder();
        // $oProductMultiCategoryQuery->limit(60);
        $aProductCategories = $oProductMultiCategoryQuery->find();

        $aTopNav =
        [
            'product_id' => $iProductId
        ];

        $aData =
        [
            'product_id' => $iProductId,
            'category' => $oCategory,
            'product_categories' => $aProductCategories
        ];

        $aOut['title'] = Translate::fromCode("Fournituren");
        $aOut['top_nav'] = $this->parse('Product/Category/order_top_nav.twig', $aTopNav);
        $aOut['content'] = $this->parse('Product/Category/order.twig', $aData);
        return $aOut;
    }

}
