<?php
namespace AdminModules\Product;

use Core\Cfg;
use Core\DeferredAction;
use Core\MainController;
use Core\Paginate;
use Core\Translate;
use Core\UserRegistry;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\Product\CrudProductManager;
use Helper\FilterHelper;
use Helper\OverviewButtonHelper;
use Model\Category\CategoryQuery;
use Model\CustomerQuery;
use Model\ProductQuery;
use Model\Setting\CrudManager\CrudView;
use Exception\LogicException;
use Propel\Runtime\ActiveQuery\Criteria;

class OverviewController extends MainController{

    use TriggerButtonEvenTrait;

    function getAfterSaveButtonEventUrl()
    {
        $iCategoryId = $this->get('category_id', null, false, 'numeric');
        $iCategoryUrl = '';

        if($iCategoryId)
        {
            $iCategoryUrl = "?category_id=$iCategoryId";
        }
        return '/product/overview'.$iCategoryUrl;
    }

    function doShowDeleted()
    {
        $sShowDeletedProducts = UserRegistry::get('show_deleted_products');
        if($sShowDeletedProducts == '1')
        {
            $sShowDeletedProducts = '0';
        }
        else
        {
            $sShowDeletedProducts = '1';
        }
        UserRegistry::register('show_deleted_products', $sShowDeletedProducts);

        $sRequestUri = $this->getRequestUri();

        $this->redirect(str_replace('&_do=ShowDeleted', '',$sRequestUri));
    }
    function doToggleView()
    {
        $sNewView = $this->post('new_view', 'list', true);
        if(!in_array($sNewView, ['list', 'grid']))
        {
            throw new LogicException("Only grid and listview are supported at the moment.");
        }
        UserRegistry::register('customer_overview_view', $sNewView);
        Utils::jsonOk();
    }
    function getQueryObject():ProductQuery
    {
        return CustomerQuery::create();
    }

    function getManager():CrudProductManager
    {
        return new CrudProductManager();
    }
    protected function getTopNavTemplate()
    {
        return 'Product/top_nav_overview_custom.twig';
    }
    protected  function getMainTemplate()
    {
        return 'Product/overview.twig';
    }
    function addCustomFilters(ProductQuery $oProductQuery)
    {
        return $oProductQuery;
    }
    protected function getGridLayoutTemplate()
    {
        return 'Product/overview-grid-layout.twig';
    }

    /**
     * @return array
     * @throws \Exception
     */
    function run()
    {
        DeferredAction::register('after_delete', $this->getRequestUri());
        DeferredAction::register('back_overview_url', $this->getRequestUri());
        DeferredAction::register('product_overview', $this->getRequestUri());
        DeferredAction::register('after_crud_change', $this->getRequestUri());
        $sTitle = Translate::fromCode('Producten overzicht');
        $iTabId = $this->get('tab');
        $iCurrentPage = $this->get('p', 1, false, 'numeric');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oCategoryQuery = CategoryQuery::create();
        $aCategories = $oCategoryQuery->orderByName()->filterByParentId(null)->find();

        $iCategoryId = ($this->get('category_id'))? $this->get('category_id') : $aCategories->getFirst()->getId();
        $oProductQuery = $this->getQueryObject();
        $oProductQuery->where('derrived_from_id IS NULL');


        $aCustomSortFields = Cfg::get('CUSTOM_PRODUCT_SORT_FIELDS');

        if(!empty($aCustomSortFields) && in_array($sSortField, $aCustomSortFields))
        {
            $aCustomSupplierSortFunction =  Cfg::get('CUSTOM_PRODUCT_SORT_FUNCTION');
            if(is_callable($aCustomSupplierSortFunction))
            {
                $oProductQuery = $aCustomSupplierSortFunction($oProductQuery, $sSortField, $sSortDirection);
            }
        }
        else
        {
            $oProductQuery->orderBy($sSortField, $sSortDirection);
        }
        $oProductQuery->filterByCategoryId($iCategoryId);

        $oProductQuery = $this->addCustomFilters($oProductQuery);

        if(UserRegistry::get('show_deleted_products') == '1')
        {
            $oProductQuery->filterByDeletedByUserId(null, Criteria::NOT_EQUAL);
        }
        else
        {
            $oProductQuery->filterByDeletedByUserId(null);
        }

        $oCrudProductManager = $this->getManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudProductManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);


        $oProductQuery = FilterHelper::applyFilters($oProductQuery, $iTabId);


        $sCustomFilterFields = Cfg::get('CUSTOM_PRODUCT_SEARCH_FIELDS');
        $sCustomFilterFunction = Cfg::get('CUSTOM_PRODUCT_SEARCH_FUNCTION');
        $oProductQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oProductQuery, $aFilters, $sCustomFilterFunction, $sCustomFilterFields);

	    $a = [
	    	'show_thumb_list_buttons' => true,
	        'show_tumb_scaler' => true,
			'thumb_current_size' => 6,
			'thumb_sizes_available' => [2, 3, 4, 6],
		    'extra_hidden_fields' => ['category_id' => $iCategoryId]
	    ];

        $sNewView = UserRegistry::get('customer_overview_view');
        $sNewView = ($sNewView) ? $sNewView : 'list';
        $a['show_thumb_list_view'] = $sNewView;

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId, $a);
        $aButtons = OverviewButtonHelper::getButtons($iTabId);

        if($sNewView == 'list')
        {
            $oCrudProductManager->setOverviewData($oProductQuery, $iCurrentPage);
            $aViewData['overview_table'] = $oCrudProductManager->getOverviewHtml($oCrudview->getFields());
        }
        else if($sNewView == 'grid')
        {
	        // Popup, large small view thumbnails
	        $this->addCssFile('/assets/js/plugins/magnific/magnific-popup.css');
	        $this->addJsFile('/assets/js/plugins/magnific/jquery.magnific-popup.js');
	        $this->addJsFile('/js/thumbs-overview.js');

	        $oPaginate =  new Paginate($oProductQuery, $iCurrentPage, 48);
            $aOverviewData = $oPaginate->getDataIterator();

            $aData = [
                'has_buttons' => !$aButtons->isEmpty(),
                'products' => $aOverviewData,
                'paginate_html' => $oPaginate->getPaginateHtml(10),
	            'thumb_scale' => $_SESSION['thumb_size'] ?? 3
            ];

            $aViewData['overview_table'] = $this->parse($this->getGridLayoutTemplate(), $aData);
        }
        else
        {
            throw new LogicException("Only grid or list are valid options for the productiew");
        }

        $sManagerClass = str_replace('Crud\Product\Crud', '', get_class($this->getManager()));

        $aTopNavVars = [
            'has_create_custom_buttons' => true,
            'show_deleted_products' => UserRegistry::get('show_deleted_products'),
            'crud_views' => $aCrudViews,
            'r' => 'after_crud_change',
            'category_id' => $iCategoryId,
            'buttons' => $aButtons,
            'tab_id' => $iTabId,
            'module' => 'Product',
            'title' => $sTitle,
            'manager' => $sManagerClass,
            'overview_url' => $oCrudProductManager->getOverviewUrl(),
            'new_url' => $oCrudProductManager->getCreateNewUrl(),
            'new_title' => $oCrudProductManager->getNewFormTitle(),
            'all_categories' => $aCategories,
            'selected_category' => CategoryQuery::create()->findOneById($iCategoryId),
        ];

        $sTopNav = $this->parse($this->getTopNavTemplate(), $aTopNavVars);

        $sOverview = $this->parse($this->getMainTemplate(), $aViewData);

        $aView =
        [
            'top_nav' => $sTopNav,
            'content' => $sOverview,
            'title' => $sTitle
        ];
        return $aView;
    }
}
/*
ipad
bestverkocht
coiu
*/
