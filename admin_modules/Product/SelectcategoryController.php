<?php
namespace AdminModules\Product;

use Core\MainController;
use Model\Category\CategoryQuery;


class SelectcategoryController extends MainController{

    function run(){

        $aViewData = [];
        $aViewData['categories'] = CategoryQuery::create()->orderByName()->filterByParentId(null)->find();

        $sTopNav = $this->parse('Product/selectcategory_topnav.twig', $aViewData);
        $sOverview = $this->parse('Product/selectcategory.twig', $aViewData);
        $sTitle = 'Selecteer een categorie';
        $aView =
            [
                'top_nav' => $sTopNav,
                'content' => $sOverview,
                'title' => $sTitle
            ];

        return $aView;
    }
}
