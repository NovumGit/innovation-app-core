<?php
namespace AdminModules\Product;

use Core\ButtonEventDispatcher;

trait TriggerButtonEvenTrait{

    abstract function getAfterSaveButtonEventUrl();

    /**
     * @throws \Exception
     */
    function doTriggerButtonEvent()
    {
        $iButtonId = $this->get('button_id', null, true, 'numeric');
        $aItems = $this->get('items', null, true, 'array');
        $aItems = array_keys($aItems);

        $bOneHasOutput = false;
        if(!empty($aItems))
        {
            $bIsLast = false;
            $iCurrent = 1;
            $iNumItems = count($aItems);
            foreach($aItems as $iItemId)
            {
                if($iCurrent == $iNumItems)
                {
                    $bIsLast = true;
                }

                $bEventHasOutput = ButtonEventDispatcher::dispatch($iButtonId, ['item_id' => $iItemId], 'overview', $bIsLast);
                if($bEventHasOutput)
                {
                    $bOneHasOutput = true;
                }
                $iCurrent++;
            }
        }
        if(!$bOneHasOutput)
        {
            $this->redirect($this->getAfterSaveButtonEventUrl());
        }
    }

}