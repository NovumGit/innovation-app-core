<?php
namespace AdminModules\Product;

use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Product\CrudProductManager;
use Exception\LogicException;
use Model\Base\ProductTranslationQuery;
use Model\Product\Image\ProductImageQuery;
use Model\Product;
use Model\CustomerQuery;
use Model\Product\Image\ProductImage;

class CopyController extends MainController
{
    private $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $aProduct = $this->post('data', null);
        $this->oCrudManager = new CrudProductManager($aProduct);
    }
    function doCopyProduct()
    {
        $aData = $this->post('data');


        $oNumberExistsCheckProduct = CustomerQuery::create()->findOneByNumber($aData['number']);
        if($oNumberExistsCheckProduct instanceof Product)
        {
            StatusMessage::danger(Translate::fromCode("Het opgegeven artikelnummer bestaat al."));
            $this->redirect($this->getRequestUri());
        }

        $iSourceProductId = $this->get('product_id', null, true, 'numeric');
        $oSourceProduct = CustomerQuery::create()->findOneById($iSourceProductId);

        if(!$oSourceProduct instanceof Product)
        {
            throw new LogicException("Could not find the product to copy.");
        }

        $oNewProduct = $oSourceProduct->copy(true);
        $oNewProduct->setNumber($aData['number']);
        $oNewProduct->setOldId(null);
        $oNewProduct->save();

        if(isset($aData['copy_images']) && $aData['copy_images'] == 1)
        {
            if(!is_writable('../data/img/product/'))
            {
                throw new LogicException("Folder ../data/img/product/ is not writable, please fix!!");
            }

            $aOldProductImages = ProductImageQuery::create()->findByProductId($oSourceProduct->getId());
            if(!$aOldProductImages->isEmpty()) {
                foreach ($aOldProductImages as $oOldProductImage) {
                    if (!$oOldProductImage instanceof ProductImage) {
                        throw new LogicException("Old ProductImage should be an intance of ProductImage.");
                    }
                    $oNewProductImage = ProductImageQuery::create()
                        ->filterByProductId($oNewProduct->getId())
                        ->filterBySorting($oOldProductImage->getSorting())
                        ->findOne();

                    if (!$oNewProductImage instanceof ProductImage) {
                        throw new LogicException("New ProductImage should be an intance of ProductImage.");
                    }
                    $sOldFileEXt = $oOldProductImage->getFileExt();
                    $sNewFileExt = $oNewProductImage->getFileExt();
                    $sOldFile = "../data/img/product/{$oOldProductImage->getId()}.$sOldFileEXt";
                    $sNewFile = "../data/img/product/{$oNewProductImage->getId()}.$sNewFileExt";
                    copy($sOldFile, $sNewFile);
                }
            }
        }
        else
        {
            ProductImageQuery::create()->filterByProductId($oNewProduct->getId())->delete();
        }

        if($aData['copy_translations'] == 0)
        {
            ProductTranslationQuery::create()->filterByProductId($oNewProduct->getId())->delete();
        }

        if($aData['product_link'] == 'new_derrived_from_old')
        {
            $oNewProduct->setDerrivedFromId($oSourceProduct->getId());
            $oNewProduct->save();
        }
        else if($aData['product_link'] == 'old_derrived_from_new')
        {
            $oSourceProduct->setDerrivedFromId($oNewProduct->getId());
            $oSourceProduct->save();
        }
        else if(!in_array($aData['product_link'], ['siblings', 'not_linked']))
        {
            // When the value contains silings or not_linked, the system should not do anything.
            // Other options should never occur so we throw an exception
            throw new LogicException("Unsupported product_link option {$aData['product_link']}");
        }

        LogActivity::register('Product', 'Copy', 'Heeft product '.$oSourceProduct->getId().' gekopieerd naar '.$oNewProduct->getId());
        if($aData['after_copy'] == 'go_new')
        {
            StatusMessage::success(Translate::fromCode("Het product is gekopieerd, u kijkt nu naar het nieuwe product."));
            $this->redirect('/product/edit?id='.$oNewProduct->getId());
        }
        else if($aData['after_copy'] == 'go_original')
        {
            StatusMessage::success(Translate::fromCode("Het product is gekopieerd, u kijkt nu naar het originele product."));
            $this->redirect('/product/edit?id='.$oSourceProduct->getId());
        }
        else if($aData['after_copy'] == 'stay')
        {
            StatusMessage::success(Translate::fromCode("Het product is gekopieerd"));
            $this->redirect($this->getRequestUri());

        }
        else
        {
            throw new LogicException("Unsupported action after_copy.");
        }
    }

    function run()
    {
        $sPageTitle = 'Product kopiëren';
        $iProductId = $this->get('product_id', true, null, 'numeric');
        $oSourceProduct = CustomerQuery::create()->findOneById($iProductId);

        if(!$oSourceProduct instanceof Product)
        {
            throw new LogicException("Expected an instance of product but coult not find any product with product id $iProductId");
        }

        $aTopNavVars = [
            'product_id' => $iProductId
        ];
        $aViewData = [
            'source_product' => $oSourceProduct
        ];

        $sTopNavHtml = $this->parse('Product/copy_top_nav.twig', $aTopNavVars);
        $sMainWindowHtml = $this->parse('Product/copy.twig', $aViewData);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];
        return $aView;
    }
    function doSortImages()
    {
        $iProductId = $this->get('id', null, true, 'numeric');
        $sNewOrder = $this->get('new_order', null, true);

        $aNewOrder = array_filter(explode(',', $sNewOrder));

        if(empty($aNewOrder))
        {
            throw new LogicException("Nieuwe volgorde is leeg, deze bug is gemeld.");
        }

        $i = 0;
        foreach($aNewOrder as $iProductImageId)
        {
            ProductImageQuery::create()->filterByProductId($iProductId)->findOneById($iProductImageId)->setSorting($i)->save();
            $i++;
        }
        StatusMessage::success("Volgorde gewijzgd.");
        $this->redirect('/product/edit?id='.$iProductId);
    }

    function doStore()
    {
        $aProduct = $this->post('data', null);
        $aProduct['category_id'] = $this->get('category_id');

        if($this->oCrudManager->isValid($aProduct))
        {
            $oProduct = $this->oCrudManager->save($aProduct);
            StatusMessage::success("Wijzigingen opgeslagen");
            /*
            $sNewTag = $this->post('new_tag', null);
            $iNewRelated= $this->post('new_related', null);


            if($sNewTag)
            {
                ProductHelper::addTagIfNotExists($oProduct, $sNewTag);
            }

            if($iNewRelated)
            {
                ProductHelper::addRelatedIfNotExists($oProduct, $iNewRelated);
            }

            if(isset($_FILES['new_image']) && !empty($_FILES['new_image']['name']))
            {
                if($_FILES['new_image']['error'] != 0)
                {
                    StatusMessage::warning("Kun het bestand niet uploaden, het systeem gaf foutcode ".$_FILES['new_image']['error']." bij het uploaden.");
                }
                $oProductImageForSorting = ProductImageQuery::create()->orderBySorting(Criteria::DESC)->filterByProductId($oProduct->getId());
                if($oProductImageForSorting instanceof ProductImage)
                {
                    $iSorting = $oProductImageForSorting->getSorting() + 1;
                }
                else
                {
                    $iSorting = 0;
                }
                $oProductImage = new ProductImage();
                $oProductImage->setSorting($iSorting);
                $oProductImage->setProductId($oProduct->getId());
                $oProductImage->setFileType($_FILES['new_image']['type']);
                $oProductImage->setFileSize($_FILES['new_image']['size']);
                $oProductImage->setOriginalName($_FILES['new_image']['name']);

                $sExt = pathinfo($_FILES['new_image']['name'], PATHINFO_EXTENSION);
                $oProductImage->setFileExt(strtolower($sExt));
                $oProductImage->save();

                move_uploaded_file($_FILES['new_image']['tmp_name'], $oProduct->getImageDir().'/'.$oProductImage->getId().'.'.$oProductImage->getFileExt());

                StatusMessage::success("Afbeelding ".$_FILES['new_image']['name']." toegevoegd.");
            }

            if($oProduct instanceof Product)
            {
                if(!empty($aProductOptions))
                {
                    foreach($aProductOptions as $iProductOptionId => $aProductOptionData)
                    {
                        $oPOPPQuery =  ProductOptionProductParameterQuery::create();
                        $iProductId = $oProduct->getId();
                        $oPOPP = $oPOPPQuery->filterByProductId($iProductId)->filterByProductOptionId($iProductOptionId)->findOne();
                        if(!$oPOPP instanceof ProductOptionProductParameter)
                        {
                            $oPOPP = new ProductOptionProductParameter();
                        }
                        $oPOPP->setProductId($oProduct->getId());
                        $oPOPP->setProductOptionId($iProductOptionId);
                        if(isset($aProductOptionData['override_sale_price']))
                        {
                            if(empty($aProductOptionData['override_sale_price']))
                            {
                                $aProductOptionData['override_sale_price'] = NULL;
                            }

                            $oPOPP->setOverrideSalePrice($aProductOptionData['override_sale_price'] );
                        }
                        if(isset($aProductOptionData['increment_decrement_sale_price']))
                        {
                            $oPOPP->setIncrementDecrementSalePrice($aProductOptionData['increment_decrement_sale_price']);
                        }
                        $oPOPP->save();
                    }
                }
            }
            else
            {
                throw new LogicException("Er is iets fout gegaan bij het opslaan van het product");
            }
            */
            $this->redirect('/product/edit?id='.$oProduct->getId().'&category_id='.$oProduct->getCategoryId());
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aProduct);
        }
    }

    /*
        function doDeleteRelated()
        {
            $iRelatedId = $this->get('related_product_id', null, true);
            $iProductId = $this->get('id', null, true, 'numeric');

            RelatedProductQuery::create()->findOneById($iRelatedId)->delete();
            StatusMessage::success("Het gerelateerde product is verwijderd.");
            $this->redirect('/product/edit?id='.$iProductId);

        }

        function doDeleteTag()
        {
            $iProductId = $this->get('id', null, true, 'numeric');
            $iProductTagId = $this->get('product_tag_id', null, true, 'numeric');
            $oProduct = ProductQuery::create()->findOneById($iProductId);

            $oProductTagQuery = ProductTagQuery::create();
            $oProductTagQuery->filterByProductId($oProduct->getId());
            $oProductTag = $oProductTagQuery->findOneById($iProductTagId);

            $iTagId = null;
            if($oProductTag instanceof ProductTag)
            {
                $iTagId = $oProductTag->getTagId();
                $oProductTag->delete();
                StatusMessage::success("De tag is verwijderd.");
            }

            if($iTagId)
            {
                $oProductTagQuery = ProductTagQuery::create();
                $aProductTags = $oProductTagQuery->filterByTagId($iTagId)->find();

                if($aProductTags->isEmpty())
                {
                    // Er zijn geen producten meer met deze tag, ruim de tag dus ook maar op.
                    TagQuery::create()->findOneById($iTagId)->delete();
                }
            }
            $this->redirect('/product/edit?id='.$iProductId);
        }


        function doDeletePicture()
        {
            $iProductId = $this->get('id', null, true, 'numeric');
            $iImageId = $this->get('image_id', null, true, 'numeric');

            $oProduct = ProductQuery::create()->findOneById($iProductId);
            $oProductImage = ProductImageQuery::create()->findOneById($iImageId);

            if($oProduct->getId() == $oProductImage->getProductId())
            {
                $sImageFile = $oProduct->getImageDir().'/'.$oProductImage->getId().'.'.$oProductImage->getFileExt();
                if(file_exists($sImageFile))
                {
                    unlink($sImageFile);
                }
                $oProductImage->delete();
            }
            StatusMessage::success("Afbeelding verwijderd.");
            $this->redirect('/product/edit?id='.$oProduct->getId());
        }

        function doUndelete()
        {
            StatusMessage::success("Product terug gezet.");
            $oProduct = $this->oCrudManager->getModel(['id' => $this->get('id')]);
            $oProduct->setItemDeleted(0)->save();
            $this->redirect('/product/overview');
        }


    */
    function doDeleteImage()
    {
        $iImageId =  $this->get('image_id', null, true, 'numeric');
        $oProductImage = ProductImageQuery::create()->findOneById($iImageId);

        if(file_exists("../data/img/product/{$oProductImage->getId()}.{$oProductImage->getFileExt()}"))
        {
            unlink("../data/img/product/{$oProductImage->getId()}.{$oProductImage->getFileExt()}");
        }

        $iProductId = $oProductImage->getProductId();
        $oProduct = CustomerQuery::create()->findOneById($iProductId);

        if(!$oProduct instanceof Product)
        {
            throw new LogicException("Could not find the product related to this image, was the image already deleted maybe?");
        }
        StatusMessage::success(Translate::fromCode("Afbeelding verwijderd. "));
        $iCategoryId = $oProduct->getCategoryId();
        $oProductImage->delete();
        $this->redirect("/product/edit?category_id=$iCategoryId&id=$iProductId");

    }

}
