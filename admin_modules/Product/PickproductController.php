<?php
namespace AdminModules\Product;

use Core\DeferredAction;
use Core\MainController;
use Core\Paginate;
use Core\Translate;
use Core\UserRegistry;
use Crud\CrudViewManager;
use Crud\Product\CrudPickproductManager;
use Crud\Product\CrudProductManager;
use Helper\FilterHelper;
use Helper\OverviewButtonHelper;
use Model\Category\CategoryQuery;
use Model\CustomerQuery;
use Model\Setting\CrudManager\CrudView;
use LogicException;

class PickproductController extends MainController
{
    use TriggerButtonEvenTrait;

    function getAfterSaveButtonEventUrl()
    {

    }

    function getModuleName()
    {
        return 'Product';
    }
    function getManagerName()
    {
        return 'PickproductManager';
    }
    function getManager():CrudProductManager
    {
        return new CrudPickproductManager();
    }
    function run(){

        $this->addJsFile('/admin_modules/Product/overview.js');

        $sTitle = Translate::fromCode('Product kiezen');

        $iTabId = $this->get('tab');
        $iCurrentPage = $this->get('p', 1, false, 'numeric');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oCategoryQuery = CategoryQuery::create();
        $aCategories = $oCategoryQuery->orderByName()->filterByParentId(null)->find();


        $iDefaultcat = isset($_SESSION['PROD_LAST_CAT']) ? $_SESSION['PROD_LAST_CAT'] : $aCategories->getFirst()->getId();
        $iCategoryId = ($this->get('category_id'))? $this->get('category_id') : $iDefaultcat;

        $_SESSION['PROD_LAST_CAT'] = $iCategoryId;

        $oProductQuery = CustomerQuery::create();
        $oProductQuery->where('derrived_from_id IS NULL');
        $oProductQuery->orderBy($sSortField, $sSortDirection);
        $oProductQuery->filterByCategoryId($iCategoryId);

        $oCrudProductManager = $this->getManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudProductManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aButtons = OverviewButtonHelper::getButtons($iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);
        $oProductQuery = FilterHelper::applyFilters($oProductQuery, $iTabId);
        $oProductQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oProductQuery, $aFilters);

        $a['extra_hidden_fields']['category_id'] = $iCategoryId;

        $sNewView = UserRegistry::get('customer_overview_view');
        $sNewView = ($sNewView) ? $sNewView : 'list';
        $a['show_thumb_list_buttons'] = true;
        $a['show_thumb_list_view'] = $sNewView;
        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId, $a);

        $oCrudProductManager->setOverviewData($oProductQuery, $iCurrentPage);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'category_id' => $iCategoryId,
            'buttons' => $aButtons,
            'tab_id' => $iTabId,
            'module' => $this->getModuleName(),
            'title' => $sTitle,
            'manager' => $this->getManagerName(),
            'overview_url' => $oCrudProductManager->getOverviewUrl(),
            'new_url' => $oCrudProductManager->getCreateNewUrl(),
            'new_title' => $oCrudProductManager->getNewFormTitle(),
            'all_categories' => $aCategories,
            'selected_category' => CategoryQuery::create()->findOneById($iCategoryId),
        ];

        $sTopNav = $this->parse('Product/top_nav_overview_custom.twig', $aTopNavVars);


        if($sNewView == 'grid')
        {
            $oPaginate =  new Paginate($oProductQuery, $iCurrentPage, 48);
            $aOverviewData = $oPaginate->getDataIterator();

            $sReturnUrl = DeferredAction::get('after_pick_product');

            $aViewData['has_pick_button'] = true;
            $aViewData['url_separator'] = strpos($sReturnUrl, '?') ? '&' : '?';
            $aViewData['pick_button_base_url'] = $sReturnUrl;
            $aViewData['thumb_scale'] = 3;
            $aViewData['has_buttons'] = !$aButtons->isEmpty();
            $aViewData['products'] = $aOverviewData;
            $aViewData['paginate_html'] = $oPaginate->getPaginateHtml(10);
            $aViewData['overview_table'] = $this->parse('Product/overview-grid-layout.twig', $aViewData);
        }
        else
        {
            $aViewData['title'] = $sTitle;
            $aViewData['overview_table'] = $oCrudProductManager->getOverviewHtml($aFields);
        }
        $sOverview = $this->parse('Product/overview.twig', $aViewData);
        return [
            'top_nav' => $sTopNav,
            'content' => $sOverview,
            'title' => $sTitle
        ];
    }
}