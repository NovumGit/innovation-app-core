$('.toggle_view').click(function(e){
    e.preventDefault();
    $.ajax({
        url : '/product/overview?_do=ToggleView',
        async : true,
        method : 'POST',
        data: {new_view : $(this).attr('id')},
        beforeSend : null,
        cache : false,
        success : function()
        {
            location.reload();
        },
        dataType : 'json',
        error : function(xhr,status,error) {}
    });
});

var oTableForm = $('#frm_table_form');
var oProductCheckboxes = $('.product_checkbox');
var oCustomButtons = $('.custom_button');
oProductCheckboxes.change(function(e)
{
    e.preventDefault();

    if($('.product_checkbox:checked').length > 0)
    {
        oCustomButtons.show(500);
    }
    else
    {
        oCustomButtons.hide(500);
    }
});

oCustomButtons.click(function(e)
{
    var _doField = $('#fld_do');

    $('#fld_button_id').val($(this).val());
    e.preventDefault();
    _doField.val('TriggerButtonEvent');
    oTableForm.submit();

    // Terug zetten, bij download van een betstand vind geen redirect plaats.
    _doField.val('');
});

var oToggleCheckboxButton = $('.toggle_check');
oToggleCheckboxButton.click(function(e){
    e.preventDefault();
    var aCheckboxes = $('.sale_order_checkbox');
    aCheckboxes.each(function(i, e){
        $(e).prop('checked', !$(e).is(':checked'));
    });
});
