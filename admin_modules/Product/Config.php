<?php
namespace AdminModules\Product;

use AdminModules\ModuleConfig;
use Core\Translate;
use Crud\Link_block_menu_products\CrudLink_block_menu_productsManager;

class Config extends ModuleConfig{

    function getMenuExtraVars(): array
    {
        return [
            'link_array' => (new CrudLink_block_menu_productsManager())->getLinkArray('products')
        ];
    }
    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return Translate::fromCode('Producten');
    }

}
