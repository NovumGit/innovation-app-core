<?php
namespace AdminModules\Product\Lists;

use Core\Cfg;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\IListableField;
use Exception\LogicException;

class EditorController extends MainController
{
    function doAddProduct()
    {
        $sListName = $this->get('list_name', null, true);
        $oList = $this->getList();
        $iProductId = $this->get('product_id', null, true,  'numeric');
        $oList->addToList($iProductId);
        StatusMessage::success(Translate::fromCode("Het product is toegevoegd aan dit lijstje."), Translate::fromCode('Yay!'));
        $this->redirect('/product/lists/editor?list_name=' . $sListName);
    }
    function doRemoveProduct()
    {
        $sListName = $this->get('list_name', null, true);
        $oList = $this->getList();
        $iProductId = $this->get('product_id', null, true,  'numeric');

        $oList->removeFromList($iProductId);

        StatusMessage::success(Translate::fromCode("Het product is van de lijst verwijderd."), Translate::fromCode('Yay!'));
        $this->redirect('/product/lists/editor?list_name=' . $sListName);
    }
    private function getList():IListableField
    {
        $sListName = $this->get('list_name', null, true);
        $sListClass = Utils::camelCase($sListName);
        $sListClassName = "Crud\\Link_block_menu_products\\Field\\".Cfg::get('CUSTOM_NAMESPACE')."\\$sListClass";

        $oListClass = new $sListClassName;

        if(!$oListClass instanceof IListableField)
        {
            throw new LogicException("$sListClassName must implement IListableField, but it does not");
        }
        return $oListClass;

    }
    function doSortList()
    {
        $aProductIds = $this->post('product_ids', null, true ,'array');
        $oListClass = $this->getList();
        $oListClass->storeListOrder($aProductIds);
        Utils::jsonOk();
    }
    function run()
    {
        DeferredAction::register('after_pick_product', $this->getRequestUri() .  '&_do=AddProduct');
        $sListName = $this->get('list_name', null, true);
        $oListClass = $this->getList();

        $aMainData = [
            'products' => $oListClass->getList(),
            'list_title' => $oListClass->getListTitle(),
            'list_name' => $sListName
        ];


        return [
            'title' => Translate::fromCode("Product lijstjes bewerken"),
            'top_nav' => $this->parse('Product/Lists/editor_top_nav.twig', []),
            'content' => $this->parse('Product/Lists/editor_overview.twig', $aMainData)
        ];
    }
}
