$('ul.sortable_container' ).each(function(i, e)
{
    $(e).sortable({
        connectWith: '.sortable_container',
        containment: "parent",
        stop : function(event, ui)
        {
            var sListName = $(this).data('list_name');
            var aProductIds = [];

            $('li', $(this)).each(function(i, e){
                aProductIds.push($(e).data('product_id'));
            });
            $.post('/product/lists/editor?list_name=' + sListName + '&_do=SortList', { product_ids : aProductIds});
        }
    });
});