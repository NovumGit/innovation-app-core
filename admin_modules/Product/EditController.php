<?php
namespace AdminModules\Product;

use Core\DeferredAction;
use Core\LogActivity;
use Core\Logger;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Core\Utils;
use Crud\Product\CrudDerrivedProductManager;
use Crud\Product\CrudProductManager;
use Exception\LogicException;
use Model\Category\Base\CategoryQuery;
use Model\Category\ProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery;
use Model\Product\RelatedProductQuery;
use Model\Product\Image\ProductImageQuery;
use Model\Product\Image\ProductImage;
use Model\Product;
use Model\ProductColor;
use Model\ProductColorQuery;
use Model\CustomerQuery;
use Model\ProductUsage;
use Model\ProductUsageQuery;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Product\RelatedProduct;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Propel;
use Exception;

class EditController extends MainController
{
    private CrudProductManager $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);

        $iProductId = $this->get('id', null, false, 'numeric');
        $this->oCrudManager = $this->getManager($iProductId);
    }

    function doStoreSelectedNodes()
    {
        $oCon = Propel::getWriteConnection('hurah');
        $oCon->beginTransaction();

        try{
            $iProductID = $this->get('id', null, true, 'numeric');
            $aSelectedItems = $this->post('selected_items', null);

            ProductMultiCategoryQuery::create()->findByProductId($iProductID)->delete();

            if(!empty($aSelectedItems))
            {
                foreach($aSelectedItems as $iCategoryId)
                {
                    $oProductMultiCategory = new ProductMultiCategory();
                    $oProductMultiCategory->setCategoryId($iCategoryId);
                    $oProductMultiCategory->setProductId($iProductID);
                    $oProductMultiCategory->save();
                }
            }
            $oCon->commit();
        }
        catch (Exception $e)
        {
            $oCon->rollBack();
            throw $e;
        }
        Utils::jsonOk();
    }


    function run()
    {
        // Nodig voor de Category tree editor
        $this->addJsFile('/assets/plugins/jstree/dist/jstree.js');
        $this->addCssFile('/assets/plugins/jstree/dist/themes/default/style.min.css');

        $aProduct = $this->post('data', null);
        $iProductId = $this->get('id');
        $iCategoryId = $this->get('category_id');

        $sDo = $this->post('_do');

        $sFromPage = $this->get('from');

        if(!$iCategoryId && !$iProductId)
        {
            $this->redirect('/product/selectcategory');
        }
        $oProduct = $this->oCrudManager->getModel(['id' => $iProductId]);

        if($sDo == 'Store')
        {
            $oProduct = $this->oCrudManager->fillVo($oProduct, $aProduct);
        }
        if(!$iCategoryId && $oProduct instanceof Product)
        {
            $iCategoryId = $oProduct->getCategoryId();
        }

        $oCategory = CategoryQuery::create()->findOneById($iCategoryId);
        $sFormLayoutKey = $this->getFormLayoutKey();

        if($iProductId)
        {

            $sPageTitle = ($this->oCrudManager instanceof CrudDerrivedProductManager) ? 'Product variatie bewerken' : 'Product bewerken';
            $sCrudReturnUrl = "/product/edit?category_id={$oCategory->getId()}&id={$iProductId}";
        }
        else
        {
            $sPageTitle = ($this->oCrudManager instanceof CrudDerrivedProductManager) ? 'Product variatie toevoegen' : 'Product toevoegen' ;
            $sCrudReturnUrl = "/product/edit?category_id={$oCategory->getId()}";
        }


        $sDoAfterSaveOption = 'return_product_edit';
        DeferredAction::register($sDoAfterSaveOption, $this->getRequestUri());

        $sEditForm = $this->oCrudManager->getRenderedEditHtml($oProduct, $sFormLayoutKey);

        DeferredAction::register("return_after_slicing", $this->getRequestUri());
        $bAutoTranslate = (Setting::get('product_auto_translate') == '1');

        $aTopNavVars = [
            'auto_translate' => $bAutoTranslate,
            'module' => 'Product',
            'from_page' => $sFromPage,
            'manager' => (new Reflector($this->oCrudManager))->getShortName(),
            'derrived_from_id' => $oProduct->getDerrivedFromId(),
            'crud_return_url' => $sCrudReturnUrl,
            'category' => $oCategory,
            'name' => $sFormLayoutKey,
            'title' => $sPageTitle,
            'product_id' => $iProductId,
            'webshop_languages' => LanguageQuery::create()->findByIsEnabledWebshop(true)
        ];

        $aRelatedProductIds = RelatedProductQuery::create()->findByProductId($iProductId);
        $aRelatedProducts = [];

        foreach($aRelatedProductIds as $oRelatedProduct)
        {
            $aRelatedProducts[] = CustomerQuery::create()->findOneById($oRelatedProduct->getOtherProductId());
        }

        $aViewData = [
            'product_id' => $iProductId,
            'edit_form' => $sEditForm,
            'product' => $oProduct,
            'category' => $oCategory,
            /* 'product_images' => $aProductImages, */
            'related_products' => $aRelatedProducts,
            'related_products_id' => $aRelatedProductIds,
            'do_after_save' => $sDoAfterSaveOption,
            'title' => $sPageTitle
        ];

        $sTopNavHtml = $this->parse($this->getTopNavTemplate(), $aTopNavVars);
        $sMainWindowHtml = $this->parse('Product/edit.twig', $aViewData);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];

        return $aView;
    }
    function doSortRelatedProducts()
    {
        $iProductId = $this->get('id', null, true, 'numeric');
        $aSortedProducts = $this->post('data');

        foreach($aSortedProducts as $iSorting => $sJson)
        {
            $aProduct = json_decode($sJson, true);
            $oRelatedProductQuery = RelatedProductQuery::create();
            $oRelatedProductQuery->filterByProductId($iProductId);
            $oRelatedProductQuery->filterByOtherProductId($aProduct['product_id']);
            $oRelatedProduct = $oRelatedProductQuery->findOne();

            if($oRelatedProduct instanceof RelatedProduct)
            {
                $oRelatedProduct->setSorting($iSorting);
                $oRelatedProduct->save();
            }
        }
        Utils::jsonOk();
    }
    function doSortImages()
    {
        $iProductId = $this->get('id', null, true, 'numeric');
        $aSortedImages = $this->post('data');

        if(!empty($aSortedImages))
        {
            foreach($aSortedImages as $iSorting => $sJson);
            {
                if (!empty($sJson)) {
                    $aImageInfo = json_decode($sJson, true);

                    $oProductImageQuery = ProductImageQuery::create();
                    $oProductImageQuery->filterByProductId($iProductId);
                    $oProductImageQuery->filterById($aImageInfo['image_id']);
                    $oProductImage = $oProductImageQuery->findOne();

                    if($oProductImage instanceof ProductImage)
                    {
                        if (isset($iSorting)) {
                            $oProductImage->setSorting($iSorting);
                        }
                        $oProductImage->save();
                    }
                }
            }
        }
        Utils::jsonOk();
    }
    function doStore()
    {
        $aProduct = $this->post('data', null);
        $iGetCategoryId = $this->get('category_id');

        if($iGetCategoryId)
        {
            $aProduct['category_id'] = $iGetCategoryId;
        }
        $sFormLayoutKey = $this->getFormLayoutKey();

        if($this->oCrudManager->isValid($aProduct, $sFormLayoutKey))
        {
            if(isset($aProduct['id']))
            {
                LogActivity::register('Product', 'store', 'Changed / saved product ' . $aProduct['id']);
            }

            $oProduct = $this->oCrudManager->save($aProduct);

            LogActivity::register('Product', 'store', 'Created product ' . $oProduct->getId());

            $bAutoTranslate = (Setting::get('product_auto_translate') == '1');

            $bSkipAutoTranslate = $this->post('skip_auto_translate') == '1';

            if($bAutoTranslate && !$bSkipAutoTranslate)
            {
                AutoTranslateHelper::autoTranslate($oProduct);
                StatusMessage::success(Translate::fromCode("Product teksten automatisch vertaald."));
            }

            StatusMessage::success("Wijzigingen opgeslagen");
            if($this->post('next_overview') == 1)
            {
                $sOverviewUrl = DeferredAction::get('back_overview_url');
                if($sOverviewUrl)
                {
                    $this->redirect($sOverviewUrl);
                    exit();
                }
            }

            $this->redirect('/product/edit?id='.$oProduct->getId().'&category_id='.$oProduct->getCategoryId());
            exit();
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aProduct, $sFormLayoutKey);
        }
    }
    function doDeleteUsage()
    {
        $iUsageId = $this->get('usage_id', null, true, 'numeric');
        $iProductId = $this->get('id', null, true, 'numeric');
        $oProductUsageQuery = ProductUsageQuery::create();
        $oProductUsageQuery->filterByUsageId($iUsageId);
        $oProductUsageQuery->filterByProductId($iProductId);
        $oProductUsage = $oProductUsageQuery->findOne();

        if($oProductUsage instanceof ProductUsage)
        {
            $oProductUsage->delete();
        }
        StatusMessage::success(Translate::fromCode("Kleur weg gehaald."));
        $this->redirect('/product/edit?id='.$iProductId);
    }
    function doDeleteColor()
    {
        $iColorId = $this->get('color_id', null, true, 'numeric');
        $iProductId = $this->get('id', null, true, 'numeric');
        $oProductColorQuery = ProductColorQuery::create();
        $oProductColorQuery->filterByColorId($iColorId);
        $oProductColorQuery->filterByProductId($iProductId);
        $oProductColor = $oProductColorQuery->findOne();

        if($oProductColor instanceof ProductColor)
        {
            $oProductColor->delete();
        }
        StatusMessage::success(Translate::fromCode("Kleur weg gehaald."));
        $this->redirect('/product/edit?id='.$iProductId);
    }
    function doAddColor()
    {
        $iNewColor = $this->post('new_color');
        if($iNewColor)
        {
            $iProductId = $this->get('id', null, true, 'numeric');

            $oProductColor = new ProductColor();
            $oProductColor->setColorId($iNewColor);
            $oProductColor->setProductId($iProductId);
            $oProductColor->save();
        }
        StatusMessage::success(Translate::fromCode("Kleur weg toegevoegd."));
        $this->redirect($this->getRequestUri());
    }
    function doAddUsage()
    {

        $iNewUsage = $this->post('new_usage');
        if($iNewUsage)
        {
            $iProductId = $this->get('id', null, true, 'numeric');

            $oProductColor = new ProductUsage();
            $oProductColor->setUsageId($iNewUsage);
            $oProductColor->setProductId($iProductId);
            $oProductColor->save();
        }
        StatusMessage::success(Translate::fromCode("Gebruik weg toegevoegd."));
        $this->redirect($this->getRequestUri());
    }
    function doDeleteRelation()
    {
        $iCurrentProduct = $this->get('id', null, true, 'numeric');
        $iRelatedProduct = $this->get('related_id', null, true, 'numeric');

        $oRelatedProductQuery = RelatedProductQuery::create();
        $oRelatedProductQuery->filterByProductId($iCurrentProduct);
        $oRelatedProductQuery->filterByOtherProductId($iRelatedProduct);
        $oRelatedProduct = $oRelatedProductQuery->findOne();

        if($oRelatedProduct instanceof RelatedProduct)
        {
            StatusMessage::success(Translate::fromCode("De relatie is verwijderd."));
            $oRelatedProduct->delete();
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Het lukte niet om de relatie te verwijderen."));
        }
        $this->redirect("/product/edit?id=$iCurrentProduct");
    }
    function doRelateProducts()
    {
        $aProductIds = $this->get('product_ids');

        foreach($aProductIds as $iProductId)
        {
            $this->doRelateProduct($iProductId, false);
        }

        $iCurrentProduct = $this->get('id', null, true, 'numeric');
        $this->redirect("/product/edit?id=$iCurrentProduct");
    }
    function doRelateProduct($iRelatedProduct = null, $bRedirectAfterRelate = true)
    {
        $iCurrentProduct = $this->get('id', null, true, 'numeric');
        if($iRelatedProduct === null)
        {
            $iRelatedProduct = $this->get('product_id', null, true, 'numeric');
        }

        $oRelatedProductQuery = RelatedProductQuery::create();
        $oRelatedProductQuery->filterByProductId($iCurrentProduct);
        $oRelatedProductQuery->filterByOtherProductId($iRelatedProduct);
        $oRelatedProduct = $oRelatedProductQuery->findOne();

        if(!$oRelatedProduct instanceof Product\RelatedProduct)
        {
            $oRelatedProductQuery = RelatedProductQuery::create();
            $oRelatedProductQuery->filterByProductId($iCurrentProduct);
            $oRelatedProductQuery->orderBySorting(Criteria::DESC)->findOne();
            $oRelatedProduct = $oRelatedProductQuery->findOne();

            if($oRelatedProduct instanceof RelatedProduct)
            {
                $iSorting = $oRelatedProduct->getSorting() + 1;
            }
            else
            {
                $iSorting = 0;
            }

            $oRelatedProduct = new RelatedProduct();
            $oRelatedProduct->setProductId($iCurrentProduct);
            $oRelatedProduct->setOtherProductId($iRelatedProduct);
            $oRelatedProduct->setSorting($iSorting);
            $oRelatedProduct->save();
            StatusMessage::success(Translate::fromCode("De producten zijn nu gerelateerd."));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Kon de producten niet relateren."));
        }

        if($bRedirectAfterRelate)
        {
            $this->redirect("/product/edit?id=$iCurrentProduct");
        }

    }
    function doDeleteImage()
    {
        $iImageId =  $this->get('image_id', null, true, 'numeric');
        $oProductImage = ProductImageQuery::create()->findOneById($iImageId);

        $iProductId = $oProductImage->getProductId();
        $oProduct = CustomerQuery::create()->findOneById($iProductId);
        $oProductImage->deleteWithFile();

        if(!$oProduct instanceof Product)
        {
            throw new LogicException("Could not find the product related to this image, was the image already deleted maybe?");
        }
        StatusMessage::success(Translate::fromCode("Afbeelding verwijderd. "));
        $iCategoryId = $oProduct->getCategoryId();

        $this->redirect("/product/edit?category_id=$iCategoryId&id=$iProductId");
    }
    function doUndelete()
    {
        $iProductId = $this->get('id', null, true, 'numeric');
        $oCurrentUser = User::getMember();
        Logger::info($oCurrentUser->getFullName()." restored product $iProductId from the recyclebin.");
        $oProduct = CustomerQuery::create()->findOneById($iProductId);
        $oProduct->setDeletedByUserId(null);
        $oProduct->setDeletedOn(null);
        $oProduct->save();
        StatusMessage::success(Translate::fromCode("Product uit de prullenbak gehaald."));
        $this->redirect(DeferredAction::get('after_delete'));;
        exit();
    }
    function doMarkDeleted()
    {
        $iProductId = $this->get('id', null, true, 'numeric');
        $oCurrentUser = User::getMember();
        Logger::info($oCurrentUser->getFullName()." placed $iProductId in the recyclebin.");
        $oProduct = CustomerQuery::create()->findOneById($iProductId);
        $oProduct->setDeletedByUserId($oCurrentUser->getId());
        $oProduct->setDeletedOn(time());
        $oProduct->save();
        StatusMessage::success(Translate::fromCode("Product als verwijderd gemarkeerd."));
        $this->redirect(DeferredAction::get('after_delete'));;
        exit();
    }
    /**
     * Permanent verwijderen.
     * @param null $iProductId
     * @param bool $bSkipRedirect
     * @return null
     */
    function doDelete($iProductId = null, $bSkipRedirect = false)
    {

        if($iProductId === null)
        {
            $bLastInChain = true;
            $iProductId = $this->get('id', null, true, 'numeric');
        }
        else
        {
            $bLastInChain = false;
        }
        $aProductImages = ProductImageQuery::create()->findByProductId($iProductId);

        if(!$aProductImages->isEmpty())
        {
            foreach($aProductImages as $oProductImage)
            {
                if(!$oProductImage instanceof ProductImage)
                {
                    throw new LogicException("Expected a ProductImage when deleting the images of products but got something else.");
                }
                $oProductImage->deleteWithFile();
            }
        }

        // Some recursion for the derrived products, if they are present.
        $aDerrivedProducts = CustomerQuery::create()->findByDerrivedFromId($iProductId);
        if(!$aDerrivedProducts->isEmpty())
        {
            foreach($aDerrivedProducts as $oDerrivedProduct)
            {
                if(!$oDerrivedProduct instanceof Product)
                {
                    throw new LogicException("Expected a Product when deleting derrived products.");
                }
                $this->doDelete($oDerrivedProduct->getId());
            }
        }
        $oModel = $this->oCrudManager->getModel(['id' => $iProductId]);
        $oModel->delete();
        if($bLastInChain)
        {
            if($bSkipRedirect)
            {
                return null;
            }
            if($oModel->getDerrivedFromId())
            {
                StatusMessage::info(Translate::fromCode("De productvariatie is verwijderd."));
                $this->redirect('/product/edit?id='.$oModel->getDerrivedFromId());
            }
            else
            {
                StatusMessage::info(Translate::fromCode("Het product en alle variaties er van zijn permanent verwijderd."));
                $sUrl = DeferredAction::get('back_overview_url');
                $this->redirect($sUrl);
            }
        }
        return null;
    }

    protected function getManager(int $iProductId = null):CrudProductManager
    {
        $bIsDerrivedProductId = false;
        if($iProductId)
        {
            $oProduct = CustomerQuery::create()->findOneById($iProductId);
            if($oProduct instanceof Product && $oProduct->getDerrivedFromId())
            {
                $bIsDerrivedProductId = true;
            }
        }
        // Een derrived product is een variatie op een gewoon product.
        // Deze heeft dus andere velden etc.
        if($bIsDerrivedProductId)
        {
            return new CrudDerrivedProductManager();
        }
        else
        {
            return new CrudProductManager();
        }
    }
    protected function getFormLayoutKey()
    {
        $iProductId = $this->get('id');
        $iCategoryId = $this->get('category_id');

        if($iProductId)
        {
            $oProductQuery = CustomerQuery::create();
            $oProduct = $oProductQuery->findOneById($iProductId);
            $iCategoryId = $oProduct->getCategoryId();
            $oCategory = CategoryQuery::create()->findOneById($iCategoryId);
        }
        else if($iCategoryId)
        {
            $oCategory = CategoryQuery::create()->findOneById($iCategoryId);
        }
        else
        {
            throw new \LogicException("Could not detect product category id.");
        }

        $sFormLayoutKey = 'product_editor_category_'.$oCategory->getId();
        return $sFormLayoutKey;
    }
    protected function getTopNavTemplate()
    {
        return 'Product/top_nav_edit.twig';
    }

}
