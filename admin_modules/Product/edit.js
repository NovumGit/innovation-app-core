$('#quick_save').click(function(e){
    $('#fld_skip_auto_translate').val('1');
    $('#main_form').submit();
});
$('#add_color').click(function()
{
    $('#fld_do').val('AddColor');

});
$('#add_usage').click(function()
{
    $('#fld_do').val('AddUsage');
});
$('ul.sortable_container' ).sortable({
    connectWith: '.sortable_container',
    axis: 'x',
    stop : function()
    {
        $('ul.sortable_container li.sortable_item').each(function(i, e){
            console.log(e);
            var sMethod = $(e).data('method');
            var sIds = null;

            if($(e).data('image_id') != 'undefined')
            {
                sIds += $(e).data('image_id')+',';
                var iProductId = $(e).data('product_id');
            }
            var aData = [];

            $('li', $(this)).each(function(i, e){
                aData.push($(e).data('data'));
            });
            $.post('/product/edit?id='+iProductId+'&_do=' + sMethod, { data : aData});
        });
    }

});


var oTree = $('#jstree');


oTree.on('changed.jstree', function (e, data) {

    if(data.action === 'select_node' || data.action === 'deselect_node')
    {
        var iProductId = $('#fld_product_id').val();

        $.ajax({
            url : '/product/edit?_do=StoreSelectedNodes&id='+iProductId,
            async : true,
            method : 'POST',
            data: {selected_items : data.selected},
            beforeSend : null,
            cache : false,
            success : function(data)
            {
                // window.location = window.location;
            },
            dataType : 'json',
            error : function(xhr,status,error)
            {
                // alert('1 Something went wrong, we got an error from the server: ' + error);
            }
        });
    }

    /*
    var oTreeData = $('#jstree').jstree("get_json");

    console.log(oTreeData);
    var sTreeData = JSON.stringify(oTreeData);
    */

});


if(typeof oTree.jstree == 'function')
{
    oTree.jstree({
    plugins : [
        "checkbox",
        "contextmenu"
    ],
    contextmenu : {
        items: {
            order: {
                separator_before: false,
                separator_after: false,
                label: 'Volgorde wijzigen',
                title: 'Wijzig de volgorde van producten in deze categorie',
                action: function (env) {
                    var iId = $(env.reference[0]).attr('id').toString().replace(/^c_/, '').replace(/_anchor$/, '');
                    iProductId = $('#fld_product_id').val();
                    window.location = '/product/category/order?menu_item=' + iId + '&product_id='+iProductId;
                },
                icon: 'fa fa-sort'
            }
        }
    },
    types : {
        "#": {
            "max_children": 1, /* Hoeveel root nodes? */
            "max_depth": 2,
            "valid_children": ["root"]}
    },
    core : {
        check_callback : true
    },
    checkbox : {
        two_state : true,
        three_state : false,
        cascade : 'undetermined'
    }
});
}
