<?php
namespace AdminModules\Visual_code;


use Core\MainController;
use Core\Translate;

class EditController extends MainController
{
    function run()
    {
        return [
            'title' => Translate::fromCode('Program your boards using drag and drop'),
            'top_nav' => $this->parse('Visual_code/edit_top_nav.twig', []),
            'content' => $this->parse('Visual_code/edit.twig', [])
        ];
    }
}
