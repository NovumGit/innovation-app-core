<?php
namespace AdminModules\Crm\Export;

use Core\MainController;
use Core\QueryMapper;
use Core\StatusMessage;
use Core\Translate;
use Core\Config;

class OverviewController extends MainController
{
	function doCustomers()
	{
		$aKeys = [
			'id' => 'Id',
			'import_source' => 'Import source',
			'ip' => 'IP',
			'created_on' => 'Created on',
			'is_deleted' => 'Is deleted',
			'is_newsletter_subscriber' => 'Is newsletter subscriber',
			'language_id' => 'Language id',
			'is_intracommunautair' => 'Is intra communautarry',
			'customer_type_id' => 'Customer type id',
			'payterm_id' => 'Payterm id',
			'branche_id' => 'Branche id',
			'sector_id' => 'Sector id',
			'debitor' => 'Debitor',
			'gender' => 'Gender',
			'first_name' => 'First name',
			'insertion' => 'Insertion',
			'last_name' => 'Last name',
			'email' => 'E-mailaddress',
			'can_buy_on_credit' => 'Can buy on credit',
			'iban' => 'IBAN',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'fax' => 'Fax',
			'website' => 'Website',
			'tag' => 'Tag',
			'is_verified' => 'Is verified',
			'verification_key' => 'Verification key'
		];


		$sQuery = "SELECT
                    ".join(',', $aKeys)."
                    FROM
                    customer
                    ";
		$this->download($sQuery, $aKeys, 'Customers.csv');
	}
	function doAddresses()
	{
		$aKeys = [
			'c.id' => 'Customer id',
			'ca.id customer_address_id' => 'Customer address id',
			'created_on' => 'Created on',
			'debitor' => 'Debitor',
			'gender' => 'Gender',
			'first_name' => 'First name',
			'insertion' => 'Insertion',
			'last_name' => 'Last name',
			'email' => 'E-mail addresss',
			'iban' => 'IBAN',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'company_name' => 'Company name',
			'attn_name' => 'Attention to name',
			'postal' => 'Postal',
			'city' => 'City',
			'mc.name country_name' => 'Country name',
			'usa_state_id' => 'USA state id',
			'address_l1' => 'Address line 1',
			'address_l2' => 'Address line 2',
		];

		$sQuery = "
		SELECT
			".join(', ', array_keys($aKeys))."
		FROM
			customer c,
			customer_address ca,
			mt_country mc
		WHERE
			c.id = ca.customer_id
		AND mc.id = ca.country_id
		GROUP BY
			c.id
		ORDER BY 
			ca.is_default DESC";


		$this->download($sQuery, $aKeys, 'Addresses.csv');
	}

    private function download($sQuery, $aKeys, $sFileName = 'export.csv')
    {
        $sExportDir = Config::getDataDir()."/export";

        if(!is_writable(Config::getDataDir()))
        {
            StatusMessage::warning(Translate::fromCode("De data directory is niet beschrijfbaar, kan geen tijdelijke export opslaan."), Translate::fromCode("Bummer"));
            $this->redirect('/crm/export/customers');
        }
        if(!is_dir($sExportDir))
        {
            mkdir($sExportDir);
        }
        if(!is_writable($sExportDir))
        {
            StatusMessage::warning(Translate::fromCode("De tijdelijke export map is niet beschrijfbaar, kan geen tijdelijk bestand aanmaken."), Translate::fromCode("Bummer"));
            $this->redirect('/crm/export/customers');
        }

	    $aCustomers = QueryMapper::fetchArray($sQuery);


        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . $sFileName);
        header('Pragma: no-cache');
        $sFile = "$sExportDir/export.csv";


        $fp = fopen($sFile, 'w');

        fputcsv($fp, $aKeys);

        foreach ($aCustomers as $aFields) {
            fputcsv($fp, $aFields);
        }

        fclose($fp);
        readfile($sFile);
        unlink($sFile);
        exit();
    }
    function run()
    {
        return [
            'top_nav' => $this->parse('Crm/top_nav_overview.twig', []),
            'title' => Translate::fromCode('Klanten importeren'),
            'content' => $this->parse('Crm/Export/overview.twig', [])
        ];

    }
}
