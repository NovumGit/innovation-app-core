<?php
namespace AdminModules\Crm;

use Helper\StatusEmailHelper;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\Customer\CrudCustomerManager;
use Ui\RenderEditConfig;
use Model\Crm\Base\CustomerQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerCompany;
use Model\Sale\SaleOrderQuery;

class Customer_editController extends Generic_customerController
{
    const sEditConfigKey = 'base';

    private $oCrudCustomerManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $oCustomer = $this->post('data', null);
        $this->oCrudCustomerManager = new CrudCustomerManager($oCustomer);
    }
    function doApproveWholesale()
    {
        $iTab = $this->get('tab');
        $sAddUrl = '';
        if($iTab)
        {
            $sAddUrl = '?tab='.$iTab;
        }
        $iCustomerId = $this->get('id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $oCustomer->setIsWholesaleApproved(true);
        $oCustomer->save();
        StatusMessage::success(Translate::fromCode('Customer has been approved and their account has been opened up. We have informed the customer trough an automated e-mail message.'));
        StatusEmailHelper::sendCustomerStatusEmail($iCustomerId, 'subscription_approved');
        $this->redirect("/crm/customer_overview$sAddUrl");


        /*
        $oLanguage = LanguageQuery::create()->findOneById($oSaleOrder->getLanguageId());
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_subject_'.$oLanguage->getId());
        $sDomain = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN');
        $sUrl = $sDomain."/".$oLanguage->getShopUrlPrefix().'/mail?order_id='.$iOrderId.'&notification_type_id='.$iNoticationMailId;
        $sContents = file_get_contents($sUrl);
*/

    }
    function doDisapproveWholesale()
    {
        $sAddUrl = '';
        $iTab = $this->get('tab');
        if($iTab)
        {
            $sAddUrl = '?tab='.$iTab;
        }
        $iCustomerId = $this->get('id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $oCustomer->setIsWholesaleApproved(true);
        $oCustomer->setItemDeleted(true);
        $oCustomer->save();
        StatusEmailHelper::sendCustomerStatusEmail($iCustomerId, 'subscription_not_approved');
        StatusMessage::success(Translate::fromCode('Customer has been disapproved and their account is marked as deleted. We have informed the customer trough an automated e-mail message.'));
        $this->redirect("/crm/customer_overview$sAddUrl");
    }
    function run()
    {

        $sDoAfterSaveOption = 'return_customer_edit';
        DeferredAction::register($sDoAfterSaveOption, $this->getRequestUri());

        $sEditViewTitle = Translate::fromCode('Relatie bewerken');
        $sCreateViewTitle = Translate::fromCode('Relatie toevoegen');
        $iCustomerId = $this->get('id');
        $sPageTitle = is_numeric($iCustomerId) ? $sEditViewTitle : $sCreateViewTitle;

        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        if($iCustomerId && !$oCustomer instanceof Customer)
        {
            StatusMessage::warning(Translate::fromCode("Klant $iCustomerId niet gevonden."));
            $this->redirect('/crm/customer_overview');
        }

        if($this->post('_do') == 'Store')
        {
            // Wanneer het script hier komt dan is er gestored maar waren er validatie errors.
            // We willen datn het $oCustomer object opnieuw opbouwen met de gepostte data.
            $aPostedData = $this->post('data');
            $oCustomer = $this->oCrudCustomerManager->fillVo($aPostedData, new Customer());

        }
        else
        {
            $oCustomer = $this->oCrudCustomerManager->getModel(['id' => $iCustomerId]);
        }


        if($oCustomer->getIsIntracommunautair() && $oCustomer->getCompany() instanceof CustomerCompany)
        {
            $sVat = $oCustomer->getCompany()->getVatNumber();
            if(empty($sVat))
            {
                StatusMessage::warning(Translate::fromCode("Bij intracommunautaire leveringen moet verplicht het BTW nummer van de klant op de factuur staan, voer het BTW nummer van de klant in."));
            }
        }

        $oRenderEditConfig = new RenderEditConfig($sPageTitle, 'Style2');
        $oRenderEditConfig->setEditorHeaderMenuHtml($this->getCustomerHeaderMenu($iCustomerId, 'edit'));
        $aParseData['edit_form'] = $this->oCrudCustomerManager->getRenderedEditHtml($oCustomer, self::sEditConfigKey, $oRenderEditConfig);
        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);
        $aParseData['edit_form'] = str_replace('<!-- before-inside-tab-1 -->', $sLeftPanel, $aParseData['edit_form']);

        $aTopnavEdit = [
            'overview_url' => '/crm/customer_overview',
            'edit_config_key' => self::sEditConfigKey,
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => $sPageTitle,
            'do_after_save' => $sDoAfterSaveOption
        ];

        $aView['content'] = $this->parse('Crm/customer_edit.twig', $aParseData);
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = $sPageTitle;

        return $aView;
    }
    function doDelete()
    {
        $iTabId = $this->get('tab');
        $iId = $this->get('id');
        $iCustomerOrderCount = SaleOrderQuery::create()->findByCustomerId($iId)->count();

        $oCustomer = $this->oCrudCustomerManager->getModel(['id' => $iId]);
        if($iCustomerOrderCount > 0)
        {
            $oCustomer->setItemDeleted(1);
            $oCustomer->save();
        }
        else
        {
            $oCustomer->delete();
        }
        StatusMessage::success(Translate::fromCode('Het record van')." ".$oCustomer->getCompanyName()." ".Translate::fromCode('is naar de prullenmand verplaatst'));
        $sAddTabUrl = '';
        if($iTabId)
        {
            $sAddTabUrl = '?tab='.$iTabId;
        }
        $this->redirect('/crm/customer_overview'.$sAddTabUrl);
    }
    function doStore()
    {
        $aCustomer = $this->post('data', null);
        $aCustomer['id'] = $this->get('id');

        if($this->oCrudCustomerManager->isValid($aCustomer, self::sEditConfigKey))
        {
            StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
            $oCustomer = $this->oCrudCustomerManager->save($aCustomer);
            if($this->post('next_overview') == 1)
            {
                $this->redirect($this->oCrudCustomerManager->getOverviewUrl());
            }
            else
            {
                $this->redirect('/crm/customer_edit?id='.$oCustomer->getId());
            }
        }
        else
        {
            $this->oCrudCustomerManager->registerValidationErrorStatusMessages($aCustomer, self::sEditConfigKey);
        }
    }
}
