<?php
namespace AdminModules\Crm\Address;

use AdminModules\Crm\Generic_customerController;
use Core\DeferredAction;
use Core\Logger;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\CustomerAddress\CrudCustomerAddressManager;
use Ui\RenderEditConfig;
use Exception\LogicException;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\CustomerAddressType;
use Model\Crm\CustomerAddressTypeQuery;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;

class EditController extends Generic_customerController
{
    const sEditConfigKey = 'base';

    private $oCrudCustomerAddressManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $oCustomer = $this->post('data', null);
        $this->oCrudCustomerAddressManager = new CrudCustomerAddressManager($oCustomer);
    }

    function doDelete()
    {
        $iCustomerId = $this->get('id');
        $oCustomerAddress = CustomerAddressQuery::create()->findOneById($iCustomerId);
        $iCustomerId = $oCustomerAddress->getCustomerId();
        $oCustomerAddress->delete();
        StatusMessage::success(Translate::fromCode("Klantadres verwijderd."));
        $this->redirect("/crm/address/overview?customer_id=$iCustomerId");

        exit();
    }
    function run()
    {

        $sAddressType = $this->get('address_type');
        $sR =  $this->get('r');

        $sDoAfterSaveOption = 'return_customer_address_edit';
        DeferredAction::register($sDoAfterSaveOption, $this->getRequestUri());

        $sEditViewTitle = Translate::fromCode('Adres bewerken');
        $sCreateViewTitle = Translate::fromCode('Adres toevoegen');

        $aCustomerAddress = $this->post('data', null);
        $aCustomerAddress['id'] = $this->get('id');
        $oCustomerAddress = CustomerAddressQuery::create()->findOneById($aCustomerAddress['id']);
        $sTitle = ($oCustomerAddress instanceof CustomerAddress) ? $sEditViewTitle : $sCreateViewTitle;

        $oRenderEditConfig = new RenderEditConfig($sTitle, 'Style2');
        if($sR == 'after_create_delivery_address')
        {
            $oRenderEditConfig->setSaveOverviewButtonVisibile(false);
            StatusMessage::warning(Translate::fromCode("Deze klant heeft nog geen afleveradres, maak nu een \"afleveradres\" of een \"algemeen adres\" voor de klant aan. Daarna kunt u verder gaan met het aanmaken van de order "));
        }
        if($sR == 'after_create_invoice_address')
        {
            $oRenderEditConfig->setSaveOverviewButtonVisibile(false);
            StatusMessage::warning(Translate::fromCode("Deze klant heeft nog geen factuuradres, maak nu een \"factuuradres\" of een \"algemeen adres\" voor de klant aan. Daarna kunt u verder gaan met het aanmaken van de order "));
        }

        if($oCustomerAddress instanceof CustomerAddress)
        {
            $iCustomerId = $oCustomerAddress->getCustomerId();
        }
        else
        {
            $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        }

        $oCustomer = $this->oCrudCustomerAddressManager->getModel($aCustomerAddress);

        $oRenderEditConfig->setEditorHeaderMenuHtml($this->getCustomerHeaderMenu($iCustomerId, 'addresses'));

        if($sAddressType && !$oCustomer->getId())
        {
            $oCustomerAddressType = CustomerAddressTypeQuery::create()->findOneByCode($sAddressType);
            if($oCustomerAddressType instanceof CustomerAddressType)
            {
                $oCustomer->setCustomerAddressTypeId($oCustomerAddressType->getId());
            }
            else
            {
                Logger::warning("When ".User::getMember()->getFirstName().' '.User::getMember()->getLastName().' tried to add an address, a default address type was specified but this address type did not exist.');
            }

            $oCountry = CountryQuery::create()->findOneByName('The Netherlands');
            if($oCountry instanceof Country)
            {
                $oCustomer->setCountryId($oCountry->getId());
            }
        }

        $aParseData['edit_form'] = $this->oCrudCustomerAddressManager->getRenderedEditHtml($oCustomer, self::sEditConfigKey, $oRenderEditConfig);

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);
        $aParseData['edit_form'] = str_replace('<!-- before-inside-tab-1 -->', $sLeftPanel, $aParseData['edit_form']);


        $aTopnavEdit = [
            'module' => 'CustomerAddress',
            'manager' => 'CustomerAddressManager',
            'edit_config_key' => self::sEditConfigKey,
            'edit_view_title' => $sTitle,
            'do_after_save' => $sDoAfterSaveOption
        ];

        $aView['content'] = $this->parse('Crm/Address/edit.twig', $aParseData);
        $aView['top_nav'] = $this->parse('generic_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = Translate::fromCode('Klant adres bewerken');

        return $aView;
    }

    function doStore()
    {
        $aCustomerAddress = $this->post('data', null);

        $aCustomerAddress['id'] = $this->get('id', null);

        $iCustomerId = $this->get('customer_id', null);
        if($iCustomerId)
        {
            $aCustomerAddress['customer_id'] = $iCustomerId;
        }


        if($this->oCrudCustomerAddressManager->isValid($aCustomerAddress, self::sEditConfigKey))
        {
            StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
            $oCustomerAddress = $this->oCrudCustomerAddressManager->save($aCustomerAddress);

            if(!$oCustomerAddress instanceof CustomerAddress)
            {
                throw new LogicException("\$oCustomerAddress should be an instance of CustomerAddress");
            }

            if($this->get('r'))
            {
                $this->redirect(DeferredAction::get($this->get('r')));
            }
            if($this->post('next_overview') == 1)
            {
                $this->redirect('/crm/address/overview?customer_id='.$oCustomerAddress->getCustomerId());
            }
            else
            {
                $this->redirect('/crm/address/edit?id='.$oCustomerAddress->getId());
            }
        }
        else
        {
            $this->oCrudCustomerAddressManager->registerValidationErrorStatusMessages($aCustomerAddress, self::sEditConfigKey);
        }
    }

    /*
    function doDelete()
    {
        $iId = $this->get('id');

        $oCustomer = $this->oCrudCustomerManager->getModel(['id' => $iId]);
        $oCustomer->setItemDeleted(1);
        $oCustomer->save();
        StatusMessage::success(Translate::fromCode('Het record van')." ".$oCustomer->getCompanyName()." ".Translate::fromCode('is naar de prullenmand verplaatst'));
        $this->redirect('/crm/overview');
    }

    */
}
