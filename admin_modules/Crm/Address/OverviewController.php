<?php
namespace AdminModules\Crm\Address;

use AdminModules\Crm\Generic_customerController;
use Core\MainController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\CustomerAddress\CrudCustomerAddressManager;
use Helper\FilterHelper;
use Model\Crm\CustomerAddressQuery;
use LogicException;
use model\Setting\CrudManager\CrudView;

class OverviewController extends Generic_customerController{
    function run(){

        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'company_name';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';
        $iCurrentPage = $this->get('p', 1, false);

        $oCustomerAddressQuery = CustomerAddressQuery::create();
        $oCustomerAddressQuery->orderBy($sSortField, $sSortDirection);
        $oCustomerAddressQuery->filterByCustomerId($iCustomerId);

        $oCrudCustomerAddressManager = new CrudCustomerAddressManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudCustomerAddressManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $oCustomerAddressQuery = FilterHelper::applyFilters($oCustomerAddressQuery, $iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $oCustomerAddressQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oCustomerAddressQuery, $aFilters);
        $oCrudCustomerAddressManager->setOverviewData($oCustomerAddressQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'customer_id' => $iCustomerId,
            'module' => 'CustomerAddress',
            'manager' => 'CustomerAddressManager',
            'overview_url' => $oCrudCustomerAddressManager->getOverviewUrl(),
            'new_url' => $oCrudCustomerAddressManager->getCreateNewUrl().'?customer_id='.$iCustomerId,
            'new_title' =>  'Terug naar de klant'
        ];

        $aMainViewData = [
            'filter_html' => FilterHelper::renderHtml($iTabId),
            'overview_table' => $oCrudCustomerAddressManager->getOverviewHtml($aFields),
            'title' => Translate::fromCode('Klant adressen'),
            'editor_header_menu' => $this->getCustomerHeaderMenu($iCustomerId, 'addresses')
        ];

        $aView =
            [
                'title' => Translate::fromCode('Klant adressen'),
                'top_nav' =>  $this->parse('Crm/Address/top_nav_overview.twig', $aTopNavVars),
                'content' => $this->parse('Crm/Address/overview.twig', $aMainViewData)
            ];
        return $aView;
    }
}
