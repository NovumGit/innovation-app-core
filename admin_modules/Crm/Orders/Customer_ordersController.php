<?php
namespace AdminModules\Crm;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Customer\CrudCustomerManager;
use Crud\CustomerAddress\CrudCustomerAddressManager;
use Ui\RenderEditConfig;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;

class Customer_ordersController extends MainController
{
    const sEditConfigKey = 'base';

    private $oCrudCustomerManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $oCustomer = $this->post('data', null);
        $this->oCrudCustomerManager = new CrudCustomerManager($oCustomer);
    }

    function run()
    {


        /*
        $sDoAfterSaveOption = 'return_customer_edit';
        DeferredAction::register($sDoAfterSaveOption, $this->getRequestUri());

        $sEditViewTitle = Translate::fromCode('Relatie bewerken');
        $sCreateViewTitle = Translate::fromCode('Relatie toevoegen');

        $aCustomer = $this->post('data', null);
        $aCustomer['id'] = $this->get('id');

        $oCustomer = $this->oCrudCustomerManager->getModel($aCustomer);
        $oRenderEditConfig = new RenderEditConfig($sEditViewTitle, 'Style2');
        $iCustomerAddressCount = CustomerAddressQuery::create()->filterByCustomerId($oCustomer->getId())->count();
        */
        $aArguments = [
            'customer_address_count' => $iCustomerAddressCount,
            'customer_id' => $aCustomer['id'],
            'orders_active' => 'active'
        ];
        $oRenderEditConfig->setEditorHeaderMenuHtml($this->parse('Crm/editor_header_menu.twig', $aArguments));

        /*
        $aParseData['edit_form'] = $this->oCrudCustomerManager->getRenderedEditHtml($oCustomer, self::sEditConfigKey, $oRenderEditConfig);


        $sEditImage = $this->parse('Crm/customer_edit_menu.twig', []);
        $aParseData['edit_form'] = str_replace('<!-- before-inside-tab-1 -->', $sEditImage, $aParseData['edit_form']);

        $aTopnavEdit = [
            'module' => 'Customer',
            'manager' => 'CustomerManager',
            'edit_config_key' => self::sEditConfigKey,
            'edit_view_title' => is_numeric($aCustomer['id']) ? $sEditViewTitle : $sCreateViewTitle,
            'do_after_save' => $sDoAfterSaveOption
        ];


        */
        $aView['content'] = $this->parse('Crm/customer_edit.twig', $aParseData);
        $aView['top_nav'] = $this->parse('generic_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = Translate::fromCode('Klant bewerken');

        return $aView;
    }
    function doDelete()
    {
        $iId = $this->get('id');

        $oCustomer = $this->oCrudCustomerManager->getModel(['id' => $iId]);
        $oCustomer->setItemDeleted(1);
        $oCustomer->save();
        StatusMessage::success(Translate::fromCode('Het record van')." ".$oCustomer->getCompanyName()." ".Translate::fromCode('is naar de prullenmand verplaatst'));
        $this->redirect('/crm/overview');
    }
    function doStore()
    {
        $oCustomer = $this->post('data', null);

        if($this->oCrudCustomerManager->isValid($oCustomer, self::sEditConfigKey))
        {
            StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));

            $oCustomer = $this->oCrudCustomerManager->save($oCustomer);

            if($this->post('next_overview') == 1)
            {
                $this->redirect($this->oCrudCustomerManager->getOverviewUrl());
            }
            else
            {
                $this->redirect('/crm/company_edit?company_id='.$oCustomer->getId());
            }
        }
        else
        {
            $this->oCrudCustomerManager->registerValidationErrorStatusMessages($oCustomer, self::sEditConfigKey);
        }
    }
}
