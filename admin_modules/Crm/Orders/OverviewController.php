<?php
namespace AdminModules\Crm\Orders;

use AdminModules\Crm\Generic_customerController;
use Core\DeferredAction;
use Crud\CrudViewManager;
use Crud\Sale_order\CrudCustomer_ordersManager;
use Helper\FilterHelper;
use Model\Sale\SaleOrderQuery;
use Core\Translate;

class OverviewController extends Generic_customerController   {

    function run()
    {

        $iCustomerId = $this->get('customer_id');
        // $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'invoice_number';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';
        $iCurrentPage = $this->get('p');
        $iCurrentPage = is_numeric($iCurrentPage) ? $iCurrentPage : 1;
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrderQuery->orderBy($sSortField, $sSortDirection);
        $oSaleOrderQuery->filterByCustomerId($iCustomerId);
        $oCrudCustomer_ordersManager = new CrudCustomer_ordersManager();

        $oCrudview = CrudViewManager::getView($oCrudCustomer_ordersManager);
        $oSaleOrderQuery = FilterHelper::applyFilters($oSaleOrderQuery, $oCrudview->getId());

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($oCrudview->getId());

        $oSaleOrderQuery = FilterHelper::generateVisibleFilters($oSaleOrderQuery, $aFilters);
        $oCrudCustomer_ordersManager->setOverviewData($oSaleOrderQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        // $aViewData['filter_html'] = FilterHelper::renderHtml($oCrudview->getId());
        $sOverviewHtml = $oCrudCustomer_ordersManager->getOverviewHtml($aFields);
        $aViewData['title'] = Translate::fromCode('Klant bestellingen');

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);
        $sTopPanel = $this->getCustomerHeaderMenu($iCustomerId);

        DeferredAction::register('customer_orders', $this->getRequestUri());

        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'top_panel' => $sTopPanel,
            'data_table' => $sOverviewHtml
        ];
        $aTopNavRightExtraOptions = [
            'tab_id' => $oCrudview->getId(),
            'module' => 'Sale_order',
            'manager' => 'Customer_ordersManager',
        ];
        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => 'xxx',
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
            'top_menu_right_extra_options' =>  $this->parse('Crm/Orders/top_menu_right_extra_options.twig', $aTopNavRightExtraOptions)
        ];
        $aView = [
            'title' => Translate::fromCode('Bestellingen van deze klant.'),
            'content' => $this->parse('Crm/Orders/overview.twig', $aMainPanelData),
            'top_nav' => $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit)
        ];

        return $aView;

    }
}