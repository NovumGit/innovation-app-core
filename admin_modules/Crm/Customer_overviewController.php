<?php
namespace AdminModules\Crm;

use Core\MainController;
use Core\QueryMapper;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\Customer\CrudCustomerManager;
use Helper\FilterHelper;
use Model\Crm\CustomerQuery;
use LogicException;
use model\Setting\CrudManager\CrudView;
use Propel\Runtime\ActiveQuery\Criteria;

;

class Customer_overviewController extends MainController{

    function run(){
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'debitor';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';
        $iCurrentPage = $this->get('p');
        $iCurrentPage = is_numeric($iCurrentPage) ? $iCurrentPage : 1;
        $oCustomerQuery = CustomerQuery::create();

	    if($sSortField == 'customer_company_company_name')
	    {
	    	$sort = $sSortDirection == 'asc' ? Criteria::ASC : Criteria::DESC;
		    $oCustomerQuery->useCustomerCompanyQuery()
			    ->orderByCompanyName($sort)
			    ->endUse();

		    $oCustomerQuery->orderBy($sSortField, $sSortDirection);
	    }
	    else
        {
	        $oCustomerQuery->orderBy($sSortField, $sSortDirection);
        }






        $oCrudCustomerManager = new CrudCustomerManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudCustomerManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }
        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oCountCustomerQuery = CustomerQuery::create();
                $oCountCustomerQuery = FilterHelper::applyFilters($oCountCustomerQuery, $o->getId());
                $o->_sz = $oCountCustomerQuery->count();
            }
        }

        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $oCustomerQuery = FilterHelper::generateVisibleFilters($oCustomerQuery, $aFilters,
            function($oQueryObject, $sFilterValue)
            {
                if(!$oQueryObject instanceof CustomerQuery)
                {
                    throw new \Exception\LogicException("Oh no, we got the wrong object type. I was expecting a CustomerQuery object but i got ".get_class($oQueryObject).".");
                }
                $sFilterValue = QueryMapper::quote('%'.$sFilterValue.'%');
                $sQuery = "SELECT customer_id FROM customer_company WHERE company_name LIKE $sFilterValue AND customer_id IS NOT NULL";
                $aRecords = QueryMapper::fetchArray($sQuery);
                $aCustomerIds1 = Utils::flattenArray($aRecords, 'customer_id');

                $sQuery = "SELECT id FROM customer WHERE first_name LIKE $sFilterValue OR last_name LIKE $sFilterValue";
                $aRecords = QueryMapper::fetchArray($sQuery);

                $aCustomerIds2 = Utils::flattenArray($aRecords, 'id');

                $aCustomerIds = array_merge($aCustomerIds1, $aCustomerIds2);

                $oQueryObject->filterById($aCustomerIds);

                return $oQueryObject;
            }
        );
        $oCrudCustomerManager->setOverviewData($oCustomerQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aViewData['overview_table'] = $oCrudCustomerManager->getOverviewHtml($aFields);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Customer',
            'manager' => 'CustomerManager',
            'overview_url' => $oCrudCustomerManager->getOverviewUrl(),
            'new_url' => $oCrudCustomerManager->getCreateNewUrl(),
            'new_title' => $oCrudCustomerManager->getNewFormTitle()
        ];
        $sTopNav = $this->parse('Crm\top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView =
            [
                'title' => Translate::fromCode('Klanten'),
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];
        return $aView;
    }
}
