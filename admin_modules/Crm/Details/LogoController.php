<?php
namespace AdminModules\Crm\Details;

use AdminModules\Crm\Generic_customerController;
use Core\StatusMessage;
use Model\Crm\CustomerLogo;
use Model\Crm\CustomerLogoQuery;
use Model\Crm\CustomerQuery;
use Model\Crm\Customer;

class LogoController extends Generic_customerController {

    function doStore()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        if(isset($_FILES['company_logo']) && $_FILES['company_logo']['size'] && $oCustomer instanceof Customer)
        {

            $oCustomer->addCompanyLogo();
            StatusMessage::success('Wijzigingen opgeslagen.');
        }
        $this->redirect($this->getRequestUri());
    }

    function doDelete()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomerLogo = CustomerLogoQuery::create()->findOneByCustomerId($iCustomerId);
        $bSuccess = false;
        if($oCustomerLogo instanceof CustomerLogo)
        {
            $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
            $bSuccess = $oCustomer->deleteLogo();
            if($bSuccess)
            {
                StatusMessage::success("Logo verwijderd");
            }
        }
        if(!$bSuccess)
        {
            StatusMessage::danger("Logo niet verwijderd, kon het logo niet vinden, was hij misschien al verwijderd?");
        }
        $this->redirect($this->getRequestUri());
    }

    function run()
    {
        StatusMessage::warning('The image size should be 250x100 or 500x200 or something in the same ratio bigger then 250x100');

        // I use $this->get and $this->post to get variables
        // If a variable is mandatory it ensures that it is available and you can enforce integer/numeric
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
        ];


        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);

        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'logo' => CustomerLogoQuery::create()->findOneByCustomerId($iCustomerId)

        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Details/logo.twig', $aMainPanelData);


        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = 'Bedrijfs logo';
        return $aView;

    }
}
