<?php
namespace AdminModules\Crm\Details;

use AdminModules\Crm\Generic_customerController;
use Core\StatusMessage;
use Model\Crm\CustomerPropertyQuery;


class Parking_public_transportController extends Generic_customerController {

    function doStore()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aProperties = $this->post('customer_property');

        CustomerPropertyQuery::storeSet($iCustomerId, $aProperties);
        StatusMessage::success("Wijzigingen opgeslagen");

        $this->redirect($this->getRequestUri());
    }
    function run()
    {
        // I use $this->get and $this->post to get variables
        // If a variable is mandatory it ensures that it is available and you can enforce integer/numeric
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $AllProperties = CustomerPropertyQuery::getSet($iCustomerId);

        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
        ];

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);

        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'customer_property' => $AllProperties

        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Details/parking_and_transport.twig', $aMainPanelData);


        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = 'Bedrijfs logo';
        return $aView;

    }
}