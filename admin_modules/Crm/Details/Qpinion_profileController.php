<?php
namespace AdminModules\Crm\Details;

use AdminModules\Crm\Generic_customerController;
use Model\Crm\CustomerPropertyQuery;
use Core\StatusMessage;


class Qpinion_profileController extends Generic_customerController
{
    function doStore()
    {
        $aData = $this->post('customer_property');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        CustomerPropertyQuery::storeSet($iCustomerId, $aData);
        StatusMessage::success("Wijzigingen opgeslagen");
        $this->redirect($this->getRequestUri());
    }

    function run()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
        ];

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);
        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'customer_property' => CustomerPropertyQuery::getSet($iCustomerId),
        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Details/qpinion_profile.twig', $aMainPanelData);

        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = 'Social Media';
        return $aView;

    }



}