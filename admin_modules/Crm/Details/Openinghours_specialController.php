<?php
namespace AdminModules\Crm\Details;

use AdminModules\Crm\Generic_customerController;
use Core\StatusMessage;
use Core\Utils;
use Model\Crm\CustomerPropertyQuery;

class Openinghours_specialController extends Generic_customerController {

    function doStore()
    {
        $aData = $this->post('customer_property');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        CustomerPropertyQuery::storeSet($iCustomerId, $aData);
        StatusMessage::success("Wijzigingen opgeslagen");
        $this->redirect($this->getRequestUri());
    }
    function run()
    {

        // I use $this->get and $this->post to get variables
        // If a variable is mandatory it ensures that it is available and you can enforce integer/numeric
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
        ];


        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);

        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'dutch_holidays' => Utils::getDutchHolidayDays(),
            'customer_property' => CustomerPropertyQuery::getSet($iCustomerId),
            'hour_ranges' => Utils::generateDropdownHourRanges()
        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Details/openinghours_special.twig', $aMainPanelData);

        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = 'Openingstijden';
          return $aView;

    }
}