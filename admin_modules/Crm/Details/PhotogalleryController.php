<?php
namespace AdminModules\Crm\Details;

use AdminModules\Crm\Generic_customerController;
use Core\StatusMessage;
use Model\Crm\CustomerPhotoQuery;
use Model\Crm\CustomerQuery;
use Model\Crm\Customer;

class PhotogalleryController extends Generic_customerController {

    function doStore()
    {
        $iCustomerPhotoId = $this->get('customer_photo_id');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        if($iCustomerPhotoId)
        {
            $aData = $this->post('data');

            // Bestaande foto bewerken
            $oCustomerPhoto = CustomerPhotoQuery::create()->findOneById($iCustomerPhotoId);
            $oCustomerPhoto->setDescription($aData['description']);
            $oCustomerPhoto->setTags($aData['tags']);
            $oCustomerPhoto->setTitle($aData['title']);
            $oCustomerPhoto->save();
            StatusMessage::success("Wijzigingen opgeslagen");
            $this->redirect("/crm/details/photogallery?customer_id=$iCustomerId");
        }
        else
        {
            // Nieuwe foto toevoegen

            $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

            if(isset($_FILES['company_photo']) && $_FILES['company_photo']['size'] && $oCustomer instanceof Customer)
            {
                $oCustomer->addCompanyPhoto();
            }

            StatusMessage::success("Afbeelding toegevoegd.");
            $this->redirect($this->getRequestUri());
        }
    }
    function doDeleteCustomerPhoto()
    {
        $iCustomerPhotoId = $this->get('customer_photo_id', true, null, 'numeric');
        $oCustomerPhoto = CustomerPhotoQuery::create()->findOneById($iCustomerPhotoId);
        $iCustomerId = $oCustomerPhoto->getCustomerId();
        $oCustomerPhoto->deleteWithFile();
        StatusMessage::success("Afbeelding verwijderd.");
        $this->redirect("/crm/details/photogallery?customer_id=$iCustomerId");
    }
    function run()
    {
        // I use $this->get and $this->post to get variables
        // If a variable is mandatory it ensures that it is available and you can enforce integer/numeric
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aTopnavEdit = [
            'edit_config_key' => 'xxx',
            'left_form_edit_config_key' => $this->sLeftFormEditConfigKey,
            'edit_view_title' => 'xxx',
            'do_after_save' => 'xxx',
        ];

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);

        $oCurrentPhoto = null;

        $iCustomerPhotoId = $this->get('customer_photo_id');
        $oCurrentPhoto = null;
        if($iCustomerPhotoId)
        {
            $oCurrentPhoto = CustomerPhotoQuery::create()->findOneById($iCustomerPhotoId);
        }
        $aMainPanelData = [
            'left_panel' => $sLeftPanel,
            'customer_photo_id' => $this->get('customer_photo_id'),
            'edit' => $this->get('edit'),
            'current_photo' => $oCurrentPhoto,
            'customer_photo' => CustomerPhotoQuery::create()->orderBySort()->filterByCustomerId($iCustomerId)
        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Details/photogallery.twig', $aMainPanelData);

        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
        $aView['title'] = 'Afbeeldingen';
        return $aView;
    }
}