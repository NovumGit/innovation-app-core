<?php
namespace AdminModules\Crm;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\CustomerNote;
use Model\Crm\CustomerNoteQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Crud\Link_block_customer_details\CrudLink_block_customer_detailsManager;

abstract class Generic_customerController extends MainController{

    protected $sLeftFormEditConfigKey = 'link_block_left';

    public function doAddNote()
    {
        $iCustomerId = $this->get('customer_id', null);
        if(!$iCustomerId)
        {
            $iCustomerId = $this->get('id', null, true, 'numeric');
        }
        $oCurrentUser = User::getMember();
        $sDescription = $this->post('description');

        if(empty($sDescription))
        {
            StatusMessage::warning(Translate::fromCode("Uw notitie is niet opgelsagen want de notitie heeft geen inhoud."));
        }
        else
        {
            $oCustomerNote = new CustomerNote();
            $oCustomerNote->setCustomerId($iCustomerId);
            $oCustomerNote->setCreatedByUserId($oCurrentUser->getId());
            $oCustomerNote->setDescription($sDescription);
            $oCustomerNote->save();
            StatusMessage::success("Uw notitie is opgeslagen.");
            $this->redirect($this->getRequestUri());
            exit();
        }
    }
    protected function getCustomerLeftPanel($iCustomerId, $sLeftFormEditConfigKey)
    {

        $oCrudLeftLinkBlockDetails = new CrudLink_block_customer_detailsManager();
        $aLeftMenuLinkItems = $oCrudLeftLinkBlockDetails->getLinkArray($sLeftFormEditConfigKey);

        $sTitle = $this->post('title');
        $sDescription = $this->post('description');


        if(!$iCustomerId)
        {
            $sLeftPanel = $this->parse('Crm/inc/customer_edit_menu_new.twig', []);
        }
        else
        {
            $aCustomerNotes = CustomerNoteQuery::create()->filterByCustomerId($iCustomerId)->orderByCreatedOn(Criteria::DESC);
            $aCustomerAddresses = CustomerAddressQuery::create()->filterByCustomerId($iCustomerId)->find();

            $aArguments = [
                'customer_addresses' => $aCustomerAddresses,
                'customer_notes' => $aCustomerNotes,
                'customer_id' => $iCustomerId,
                'left_menu_link_items' => $aLeftMenuLinkItems,
                'posted_title' => $sTitle,
                'posted_description' => $sDescription
            ];

            $sLeftPanel = $this->parse('Crm/inc/customer_edit_menu_edit.twig', $aArguments);
        }
        return $sLeftPanel;
    }
    protected function getCustomerHeaderMenu($iCustomerId, $sActive = '')
    {
        $oCrudLeftLinkBlockDetails = new CrudLink_block_customer_detailsManager();
        $aMenuLinkItems = $oCrudLeftLinkBlockDetails->getLinkArray('editor_header_menu');

        $iCustomerAddressCount = CustomerAddressQuery::create()->filterByCustomerId($iCustomerId)->count();
        $aArguments = [
            'top_menu_link_items' => $aMenuLinkItems,
            'customer_address_count' => $iCustomerAddressCount,
            'customer_id' => $iCustomerId,
            $sActive.'_active' => 'active'
        ];
        return $this->parse('Crm/inc/editor_header_menu.twig', $aArguments);
    }
}