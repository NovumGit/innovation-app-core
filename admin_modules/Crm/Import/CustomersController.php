<?php
namespace AdminModules\Crm\Import;

use Core\MainController;
use Core\Translate;

class CustomersController extends MainController
{
    function doDownloadCsv()
    {
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=template.csv');
        header('Pragma: no-cache');

        readfile('../admin_modules/Crm/Import/template.csv');
        exit();
    }
    function run()
    {

        return [
            'top_nav' => $this->parse('Crm/top_nav_overview.twig', []),
            'title' => Translate::fromCode('Klanten importeren'),
            'content' => $this->parse('Crm/Import/customers.twig', [])
        ];

    }
}
