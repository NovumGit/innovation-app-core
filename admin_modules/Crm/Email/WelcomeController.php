<?php
namespace AdminModules\Crm\Email;

use AdminModules\Generic\Email\GenericEmailController;
use Core\Translate;

class WelcomeController extends GenericEmailController {

    use CrmEmailTrait;

    function getActivityDescription()
    {
        return 'Welkomt e-mailbericht verzonden';
    }
    function getTitle()
    {
        return Translate::fromCode('Welkomst e-mailbericht');
    }
    function getTemplateConfigKey()
    {
        return 'welcome';
    }
}