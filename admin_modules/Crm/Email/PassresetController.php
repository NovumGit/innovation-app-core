<?php
namespace AdminModules\Crm\Email;

use AdminModules\Generic\Email\GenericEmailController;
use Core\Customer;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\CustomerQuery;

class PassresetController extends GenericEmailController {
    use CrmEmailTrait;

    function lastChangesOnContents($sContents)
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        if(!$oCustomer instanceof \Model\Crm\Customer)
        {
            throw new LogicException("Customer not found");
        }
        if(strpos($sContents, '[[new_pass]]') !== false)
        {
            // generateNewPass
            $sUnencryptedPass = Customer::generateNewPass($oCustomer);
            $sContents = str_replace('[[new_pass]]', $sUnencryptedPass, $sContents);
        }
        return $sContents;
    }

    function getActivityDescription()
    {
        return 'Wachtwoord reset mail verstuurd';
    }
    function getTitle()
    {
        return Translate::fromCode('Wachtwoord mailen');
    }
    function getTemplateConfigKey()
    {
        return 'password_reset';
    }
}