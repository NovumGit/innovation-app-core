<?php
namespace AdminModules\Crm\Email;

use Core\User;
use Model\Company\CompanyQuery;
use Model\Crm\CustomerQuery;

trait CrmEmailTrait
{
    function getMailingTemplateVars():array
    {
        $iCustomerId = $this->get('customer_id');
        return [
            'Customer' => CustomerQuery::create()->findOneById($iCustomerId),
            'Own_company' => CompanyQuery::getDefaultCompany(),
            'User' => User::getMember()
        ];
    }
    function getToEmail()
    {
        $iCustomerId = $this->get('customer_id');
        return CustomerQuery::create()->findOneById($iCustomerId)->getEmail();
    }
    function getFromEmail()
    {
        return User::getMember()->getEmail();
    }
    function getTopNav()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aTopnavEdit = [
            'customer_id' => $iCustomerId,
            'template_config_key' => $this->getTemplateConfigKey()
        ];
        return $this->parse('Crm/customer_edit_top_nav_edit.twig', $aTopnavEdit);
    }
    function getMailingLanguageId()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $iLanguageId = $oCustomer->detectLanguageId();
        return $iLanguageId;
    }
}