<?php
namespace AdminModules\Crm\Api_connect;

use AdminModules\Crm\Generic_customerController;
use Api\Push\Customer\AbstractPush;
use Api\Push\Customer\PushFactory;
use Core\Cfg;
use Core\StatusModal;
use Exception\LogicException;
use Model\Crm\CustomerPropertyQuery;
use Model\Crm\CustomerQuery;

class Push_statusController extends Generic_customerController {

    function dotest()
    {
        var_dump($_POST);
        exit();
    }
    function doActivate()
    {
        $iIndex = $this->get('index');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $aPushers = PushFactory::getClasses($oCustomer);
        $oPusher = $aPushers[$iIndex];

        if($oPusher instanceof AbstractPush)
        {
            $oPusher->activate();
            exit('Activationscript done.');
        }
        else
        {
            throw new LogicException("Expected an instance of AbstractPush.");
        }
    }
    function doUpdateProfile()
    {
        $iIndex = $this->get('index');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $aPushers = PushFactory::getClasses($oCustomer);
        $oPusher = $aPushers[$iIndex];

        if($oPusher instanceof AbstractPush)
        {
            $oPusher->push();
            exit('Push profile script done.');
        }
        else
        {
            throw new LogicException("Expected an instance of AbstractPush.");
        }
    }
    function doShowHelp()
    {
        $sTxt = $this->get('txt');
        $iCustomerId = $this->get('customer_id');
        if($sTxt == 'confirmation')
        {
            StatusModal::info('Nadat het account op de site is geregistreerd volgt er vaak een registratie e-mailbericht. Wanneer u in de mailbox op de activatie link heeft gedrukt dan kunt u dat aan het systeem doorgeven door op deze knop te klikken. Pas als een account is geactiveerd zal deze aanpasbaar zijn. De knop wordt alleen weergegeven bij sites die gebruik maken van een activatie link.','Handmatig activatie bevestigen');
        }
        else if($sTxt == 'registration')
        {
            StatusModal::info('Wanneer u op deze knop drukt dan zal het systeem gaan proberen om het bedrijf aan melden bij de site, soms door het bedrijf te claimen en soms door een nieuw bedrijf toe te voegen. Indien er informatie ontbreekt of niet juist is ingevuld dan zal dit soms niet lukken, u ziet op het scherm de achterliggende oorzaak. Pas indien nodig het profiel aan en probeer het daarna opnieuw.','Bedrijf registreren');
        }
        else if($sTxt == 'updateprofile')
        {
            StatusModal::info('Wanneer u op deze knop druk zal het profiel worden doorgezet naar de website.','Profiel pushen');
        }
        $this->redirect("/crm/api_connect/push_status?customer_id=$iCustomerId");

    }
    function doRegister()
    {
        $iIndex = $this->get('index');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        $aPushers = PushFactory::getClasses($oCustomer);
        $oPusher = $aPushers[$iIndex];

        if($oPusher instanceof AbstractPush)
        {
            $oPusher->register();
        }
        else
        {
            throw new LogicException("Expected an instance of AbstractPush.");
        }
    }
    function doPush()
    {
        var_dump($_POST);
        exit();
    }

    function run()
    {
        $sDomain = Cfg::get('DOMAIN');
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        $aPushers = PushFactory::getClasses($oCustomer);

        /*
        foreach($aPushers as $oPusher)
        {

        }
        */

        // $oAllebedrijveninPush = new AllebedrijveninPush($oCustomer);
        // $oAllebedrijveninPush->push();

        // $oOpendiPush = new OpendiPush($oCustomer);
        // $oOpendiPush->push();

        // $oCylex = new CylexPush($oCustomer);
        // $oCylex->push();

        // $oDrimblePush = new DrimblePush();
        // $oDrimblePush->push($oCustomer);

        // I use $this->get and $this->post to get variables
        // If a variable is mandatory it ensures that it is available and you can enforce integer/numeric
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $aProperties = CustomerPropertyQuery::getSet($iCustomerId);

        $aTopnavEdit = [
            'customer_id' => $iCustomerId
        ];

        $sLeftPanel = $this->getCustomerLeftPanel($iCustomerId, $this->sLeftFormEditConfigKey);

        $aMainPanelData = [
            'domain' => $sDomain,
            'customer' => $oCustomer,
            'pushers' => $aPushers,
            'left_panel' => $sLeftPanel,
            'customer_property' => $aProperties
        ];

        // Content is what you see on the main panel..
        $aView['content'] = $this->parse('Crm/Api_connect/push_status.twig', $aMainPanelData);

        // Top_nav is the navigation bar where that little toolbox is also
        $aView['top_nav'] = $this->parse('Crm/Api_connect/push_status_top.twig', $aTopnavEdit);
        $aView['title'] = 'Bedrijfsomschrijving';
        return $aView;

    }
}