<?php
namespace AdminModules\Newsletter\Edit;

use AdminModules\GenericEditController;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\FormManager;
use Crud\NewsletterSubscriber\CrudNewsletterSubscriberManager;
use Model\Crm\NewsletterSubscriberQuery;

class SubscriberController extends GenericEditController
{
    function doDelete()
    {
        $iId = $this->get('id', null, 'numeric');
        $oNewsletterSubscriberQuery = NewsletterSubscriberQuery::create();
        $oNewsletterSubscriberQuery->findOneById($iId);
        $oNewsletterSubscriberQuery->delete();
        StatusMessage::info(Translate::fromCode('Subscriber removed.'));
        $this->redirect(DeferredAction::get('after_delete_url'));
    }
    function getCrudManager(): FormManager
    {
        return new CrudNewsletterSubscriberManager();
    }
    function getPageTitle()
    {
        return Translate::fromCode('Nieuwbrief abbonee');
    }
}