<?php
namespace AdminModules\Newsletter\Overview;

use AdminModules\GenericOverviewController;
use Core\Translate;
use Crud\FormManager;
use Crud\NewsletterSubscriber\CrudNewsletterSubscriberManager;
use Model\Crm\NewsletterSubscriberQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class SubscribersController extends GenericOverviewController
{
    function getModule(): string
    {
        return 'NewsletterSubscriber';
    }

    function getQueryObject(): ModelCriteria
    {
        return NewsletterSubscriberQuery::create();
    }

    function getTitle(): string
    {
        return Translate::fromCode('Nieuwbrief abbonees');
    }

    function getManager(): FormManager
    {
        return new CrudNewsletterSubscriberManager();
    }
}