<?php
namespace AdminModules\Tax\Bookings;

use Core\MainController;
use Core\Translate;

class AutomagicController extends MainController
{
    function run()
    {
        return [

            'title' => Translate::fromCode('Automatische taken'),
            'top_nav' => '',
            'content' => '',

        ];
    }

}