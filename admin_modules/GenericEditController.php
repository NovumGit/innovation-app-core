<?php
namespace AdminModules;

use Core\DeferredAction;
use Core\MainController;
use Core\Reflector;
use Core\StatusMessage;
use Core\Translate;
use Crud\FormManager;
use Ui\RenderEditConfig;
use Exception\LogicException;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

abstract class GenericEditController extends MainController
{

    private $oRenderEditConfig;
    abstract function getCrudManager():FormManager;
    abstract function getPageTitle();


    function __construct($aGet, $aPost)
    {

        $sLayoutKey = $this->getFormLayoutKey();
        $sDefaultTitle = $this->getCrudManager()->getEntityTitle();
        $this->oRenderEditConfig = new RenderEditConfig($sDefaultTitle, $sLayoutKey);

        $this->getCrudManager()->getEntityTitle();
        $this->addJsFile('/js/custom-buttons.js');
        DeferredAction::register('crud_editor_return_url', $this->getRequestUri());
        parent::__construct($aGet, $aPost);
    }

    function getRenderEditConfig():RenderEditConfig
    {
        return $this->oRenderEditConfig;
    }
    /*
     * Indien je meerdere layouts nodig hebt kun je deze overwriten zodat de key telkens uniek is.
     */

    function getSavedStatusMessage()
    {
        return Translate::fromCode("Wijzigingen opgeslagen");
    }

    function getFormLayoutKey()
    {
        return str_replace('\\','_', get_class($this));
    }
    function getEditTemplate()
    {
        return 'edit.twig';
    }
    function getOverviewUrl():?string
    {
        $sOverviewUrl = DeferredAction::get('overview_url');
        return $sOverviewUrl ?? null;
    }
    protected function getTopNavVars()
    {
        $oCrudManager = $this->getCrudManager();


        $sManagerName = str_replace( 'Crud', '', (new Reflector($oCrudManager))->getName());

        $aTopNavVars = [
            'id' => $this->get('id'),
            'overview_url' => $this->getOverviewUrl(),
            'module' => str_replace('Manager', '', $sManagerName),
            'manager' => $sManagerName,
            'crud_return_url' => $this->getRequestUri(),
            'edit_config_key' => $this->getFormLayoutKey(),
            'title' => $this->getPageTitle(),
            'get' => $this->get()
        ];
        return $aTopNavVars;
    }

    function run()
    {

        $sR = $this->get('r');
        $aData = $this->post('data');
        $aData['id'] = $this->get('id');
        $oCrudManager = $this->getCrudManager();

        $oDataObject = $oCrudManager->getModel($aData);
        $sFormLayoutKey = $this->getFormLayoutKey();
        DeferredAction::register($sFormLayoutKey, $this->getRequestUri());

        $oRenderEditConfig = $this->getRenderEditConfig();
        if(!empty($sR))
        {
            // Het systeem wil ons na het opslaan ergens anders heen sturen. Om verwarring te voorkomen geen overviewbutton tonen.
            $oRenderEditConfig->setSaveOverviewButtonVisibile(false);
        }
        $oRenderEditConfig->setLayout('Style1');

        $sEditForm = $oCrudManager->getRenderedEditHtml($oDataObject, $sFormLayoutKey, $oRenderEditConfig);

        $aViewData = [
            'edit_form' => $sEditForm,
            'title' => $this->getPageTitle(),
            'extra_fields' => $this->getExtraFields()
        ];

        $sTopNavTemplate = $this->getTopNavTemplate();
        if(method_exists($this, 'getTopNav'))
        {
            $sTopNavHtml = $this->getTopNav();
        }
        else
        {
            $aTopNavVars = $this->getTopNavVars();
            $sTopNavHtml = $this->parse($sTopNavTemplate, $aTopNavVars);
        }

        $sMainWindowHtml = $this->parse($this->getEditTemplate(), $aViewData);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $this->getPageTitle()
        ];
        return $aView;
    }
    function getExtraFields():array
    {
        return [];
    }
    function getAfterSaveUrl($iId)
    {
        return null;
    }
    function postSave(ActiveRecordInterface $oData, array $aData):ActiveRecordInterface
    {
        return $oData;
    }
    function doStore()
    {
        $oCrudManager = $this->getCrudManager();
        $aData = $this->post('data', null);

        if($oCrudManager->isValid($aData, $this->getFormLayoutKey()))
        {

            $oData = $oCrudManager->save($aData);
            $oData = $this->postSave($oData, $aData);

            if($oData instanceof ActiveRecordInterface && method_exists($oData, 'getId'))
            {
                if($this->getSavedStatusMessage() != 'custom')
                {
                    StatusMessage::success($this->getSavedStatusMessage());
                }

                $sRequestUri = $this->getRequestUri(false);

                if($this->getAfterSaveUrl($oData->getId()))
                {
                    $this->redirect($this->getAfterSaveUrl($oData->getId()));
                    exit();
                }
                else if($this->get('r'))
                {
                    $this->redirect(DeferredAction::get($this->get('r')));
                    exit();
                }
                else if($this->post('next_overview') == '1')
                {
                    $this->redirect($oCrudManager->getOverviewUrl());
                    exit();
                }

                $this->redirect($sRequestUri.'?id='.$oData->getId());
                exit();
            }
            else
            {
                throw new LogicException("Something went wrong, validation went ok but it seems we could not safe.");
            }
        }
        else
        {
            $oCrudManager->registerValidationErrorStatusMessages($aData, $this->getFormLayoutKey());
        }
    }

    public function getTopNavTemplate()
    {
        return  'generic_top_nav_edit.twig';
    }
}
