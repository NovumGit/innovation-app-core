<?php
namespace AdminModules\Services\Wet\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\Wet\CrudWetManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Services\Wet instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudWetManager();
	}


	public function getPageTitle(): string
	{
		return "Wetten";
	}
}
