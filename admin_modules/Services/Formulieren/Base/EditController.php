<?php
namespace AdminModules\Services\Formulieren\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\Formulier\CrudFormulierManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Services\Formulieren instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudFormulierManager();
	}


	public function getPageTitle(): string
	{
		return "Formulieren";
	}
}
