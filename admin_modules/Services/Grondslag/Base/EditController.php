<?php
namespace AdminModules\Services\Grondslag\Base;

use AdminModules\GenericEditController;
use Crud\Eenoverheid\Grondslag\CrudGrondslagManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Services\Grondslag instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudGrondslagManager();
	}


	public function getPageTitle(): string
	{
		return "Grondslagen";
	}
}
