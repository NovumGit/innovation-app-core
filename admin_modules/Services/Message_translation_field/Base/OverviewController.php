<?php
namespace AdminModules\Services\Message_translation_field\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FormManager;
use Crud\MessageTranslationField\CrudMessageTranslationFieldManager;
use Model\Custom\NovumOverheid\MessageTranslationField;
use Model\Custom\NovumOverheid\MessageTranslationFieldQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Services\Message_translation_field instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Veld vertaling";
	}


	public function getModule(): string
	{
		return "MessageTranslationField";
	}


	public function getManager(): FormManager
	{
		return new CrudMessageTranslationFieldManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return MessageTranslationFieldQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof MessageTranslationField){
		    LogActivity::register("Services", "Veld vertaling verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Veld vertaling verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Veld vertaling niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Veld vertaling item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
