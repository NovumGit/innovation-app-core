<?php
namespace AdminModules\Services\Message_translation_field\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\MessageTranslationField\CrudMessageTranslationFieldManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Services\Message_translation_field instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudMessageTranslationFieldManager();
	}


	public function getPageTitle(): string
	{
		return "Veld vertaling";
	}
}
