<?php
namespace AdminModules\Purchase\Invoice;

use Core\MainController;
use Model\Finance\PurchaseInvoiceQuery;

class Pdf_viewerController extends MainController
{
    function run()
    {

        $iInvoiceId =  $this->get('invloice_id');
        $oPurchaseInvoiceQuery = PurchaseInvoiceQuery::create();
        $oPurchaseInvoice = $oPurchaseInvoiceQuery->findOneById($iInvoiceId);

        header("Content-type:application/pdf");

        // It will be called downloaded.pdf
        header("Content-Disposition:attachment;filename='" . $oPurchaseInvoice->getRemoteInvoiceId() . ".pdf'");

        readfile($oPurchaseInvoice->getFilePath());
        exit();
        // TODO: Implement run() method.
    }

}