<?php
namespace AdminModules\Purchase\Invoice\Event;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\Mime\VoidMime;
use Core\Translate;
use Crud\Purchase_invoice\CrudPurchase_invoiceManager;
use Helper\CrudManagerHelper;
use Model\Finance\PurchaseInvoiceQuery;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;
use DateTime;
use DateInterval;

class Change_field extends AbstractEvent implements AbstractConfigurableEvent{

    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        $iEventId = $this->get('event_id', null, true, 'numeric');

        $sValue = $this->getFieldSetting($iEventId.$sEnvironment, 'overwrite_value');
        $sField = $this->getFieldSetting($iEventId.$sEnvironment, 'crud_field');
        $sNote = $this->getFieldSetting($iEventId.$sEnvironment, 'notes');
        $sSelectedField = $this->get('crud_field');
        if(!$sSelectedField)
        {
            $sSelectedField = $sField;
        }

        $oCrudManager = new CrudPurchase_invoiceManager();
        $aCrudFields = CrudManagerHelper::getAllFieldObjects($oCrudManager, 'Purchase_invoice', $sSelectedField);

        if(!empty($aCrudFields['selected_field_lookups']))
        {
            foreach($aCrudFields['selected_field_lookups'] as &$aField)
            {
                $aField['selected'] =  ($sValue == $aField['id']) ? 'selected="selected"' : '';
            }
        }

        $aVariables['current_value'] = $sValue;
        $aVariables['current_note'] = $sNote;
        $aVariables['crud_fields'] = $aCrudFields;
        $aVariables['event_id'] = $iEventId;

        return $this->parse('Generic/Event/order_event_change_field.twig', $aVariables);
    }

    /**
     * @param $sEnvironment
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function saveConfiguration($sEnvironment)
    {
        $iEventId = $this->get('event_id', null, true);

        $this->storeFieldSetting($iEventId.$sEnvironment, 'overwrite_value', $this->get('overwrite_value'));
        $this->storeFieldSetting($iEventId.$sEnvironment, 'crud_field', $this->get('crud_field'));
        $this->storeFieldSetting($iEventId.$sEnvironment, 'notes', $this->get('notes'));
    }

    function getFieldSetting($iButtonEventId, $sSetting)
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey('overwrite_'.$iButtonEventId.'_crud_'.$sSetting);

        if($oSystemRegistry)
        {
            return $oSystemRegistry->getItemValue();
        }
        return null;
    }

    /**
     * @param $iButtonEventId
     * @param $sSetting
     * @param $sValue
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function storeFieldSetting($iButtonEventId, $sSetting, $sValue)
    {
        $sKey = 'overwrite_'.$iButtonEventId.'_crud_'.$sSetting;
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sValue);
        $oSystemRegistry->save();
    }

    function getDescription(): string
    {
        return Translate::fromCode("Wijzig een veld");
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function isConfigurable(): bool
    {
        return true;
    }

    /**
     * @event_if veld wordt gewijzigd
     * @event_then wijzigd veld
     * @param string $sEnvironment
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Exception
     */
    function trigger(string $sEnvironment): void
    {

        $aItems = array_keys($this->get('items'));

        foreach ($aItems as $iPurchaseInvoiceId)
        {
            $sTmpField = $this->get('field');

            if($sTmpField)
            {
                $sValue = $this->get('value');
                $sField = $this->get('field');
            }
            else
            {
                $sValue = $this->getFieldSetting($this->getButtonEventId().$sEnvironment, 'overwrite_value');
                $sField = $this->getFieldSetting($this->getButtonEventId().$sEnvironment, 'crud_field');
            }
            $oPurchase_invoice = PurchaseInvoiceQuery::create()->findOneById($iPurchaseInvoiceId);
            $sSetter = 'set'.ucfirst($sField);

            if(preg_match('/^(now )?(\+)? ([0-9]+) day$/', $sValue, $aMatches))
            {

                $sGetter = str_replace('set', 'get', $sSetter);

                $oDate  = $oPurchase_invoice->$sGetter();
                if($oDate == null || trim($aMatches[1]) == 'now')
                {
                    $oDate = new DateTime();
                }

                $oDate->add(new DateInterval('P'.$aMatches[3].'D'));
                $oPurchase_invoice->$sSetter($oDate);
            }
            else
            {
                $oPurchase_invoice->$sSetter($sValue);
            }
            $oPurchase_invoice->save();
        }
    }
}

