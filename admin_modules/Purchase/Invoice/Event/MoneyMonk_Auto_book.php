<?php
namespace AdminModules\Purchase\Invoice\Event;

use Api\Accounting\MoneyMonk\Task\PurchaseInvoiceReconciliation;
use Core\AbstractEvent;
use Core\Mime\HtmlMime;
use Core\Translate;
use Model\Finance\PurchaseInvoiceQuery;
use Psr\Log\AbstractLogger;

class MoneyMonk_Auto_book extends AbstractEvent{

    function getDescription(): string
    {
        return Translate::fromCode("Probeer facturen automatisch te boeken");
    }

    /**
     * @param string $sEnvironment
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function trigger(string $sEnvironment): void
    {

        $oDisplayLogger = new class extends AbstractLogger
        {
            function log($level, $message, array $context = array())
            {
                echo $message . '<br>' . PHP_EOL;
            }
        };
        $aPurchaseInvoices = array_keys($this->get('items'));

        if(!empty($aPurchaseInvoices))
        {
            foreach ($aPurchaseInvoices as $iPurchaseInvoiceId)
            {
                $oPurchaseInvoice = PurchaseInvoiceQuery::create()->findOneById($iPurchaseInvoiceId);

                $oPurchaseInvoiceReconciliation = new PurchaseInvoiceReconciliation($oDisplayLogger);
                $oPurchaseInvoiceReconciliation->reconcileOne($oPurchaseInvoice);
            }
        }

        // $_GET['items
        var_dump($aPurchaseInvoices);

        exit('autobook');
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new HtmlMime();
    }

}
