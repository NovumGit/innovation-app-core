<?php
namespace AdminModules\Purchase\Invoice;

use AdminModules\GenericOverviewController;
use AdminModules\Product\TriggerButtonEvenTrait;
use Crud\FormManager;
use Crud\Purchase_invoice\CrudPurchase_invoiceManager;
use Model\Finance\PurchaseInvoiceQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    use TriggerButtonEvenTrait;


    function getAfterSaveButtonEventUrl()
    {
        $iTabId = $this->get('tab', null, false, 'numeric');
        $sQuery = '';

        if($iTabId)
        {
            $sQuery = '?' . http_build_query(['tab' => $iTabId]);
        }

        return '/purchase/invoice/overview' . $sQuery;
    }


    function __construct($aGet, $aPost)
    {
        $this->setEnablePaginate(50);
        $this->setEnableCreateButtons('Purchase/Invoice');
        parent::__construct($aGet, $aPost);
    }

    function getModule(): string
    {
        return 'Purchase_invoice';
    }

    function getManager(): FormManager
    {
        return new CrudPurchase_invoiceManager();
    }

    function getQueryObject(): ModelCriteria
    {
        return PurchaseInvoiceQuery::create();
    }
    function getTitle(): string
    {
        if(!empty($this->sCrudViewName))
        {
            return $this->sCrudViewName;
        }
        return $this->getManager()->getEntityTitle();
    }
}