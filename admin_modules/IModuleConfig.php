<?php
namespace AdminModules;

interface IModuleConfig{

    function setDatabaseId($iId);
    function getDatabaseId();
    function getMenuExtraVars(): array;
    function isEnabelable(): bool;
    function getModuleTitle(): string;
}
