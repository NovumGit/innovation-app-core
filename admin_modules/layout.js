$('.dropdown-toggle').click(function(){

    oToggleForm = $('.panel-menu.p12.allcp-form.mtn');
    if(oToggleForm.is(':visible'))
    {
        oToggleForm.hide(200);
    }
    else
    {
        oToggleForm.show(200);
    }
});

$('input[type=radio]', $('#user-role').closest('form')).change(function(){
    $('#user-role').val($(this).val());
    $('#frm_role_selector').submit();
});
