<?php
namespace AdminModules\Contact;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Contact_message\CrudContact_messageManager;
use Model\ContactMessage;
use Model\ContactMessageQuery;

class EditController extends MainController
{
    const sFormLayoutKey = 'contact_form_editor';

    private $oCrudUserManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->oCrudUserManager = new CrudContact_messageManager();
    }
    function doDelete()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oContactMessage = ContactMessageQuery::create()->findOneById($iId);

        if($oContactMessage instanceof ContactMessage)
        {
            $oContactMessage->delete();
            $sMsg = Translate::fromCode("Contactbericht verwijderd.");
            StatusMessage::success($sMsg);
        }
        else
        {
            $sMsg = Translate::fromCode("Contactbericht niet gevonden, kon hem niet verwijderen.");
            StatusMessage::warning($sMsg);
        }

        $sReturnUrl = DeferredAction::get('return_after_delete_contact');
        $this->redirect($sReturnUrl);

    }
    function doStore()
    {
        $aContactMessage = $this->post('data', null);
        if($this->oCrudUserManager->isValid($aContactMessage))
        {
            $sAddUrl = '';
            if(isset($_GET['tab']))
            {
                $sAddUrl = '&tab='.$_GET['tab'];
            }
            StatusMessage::success("Wijzigingen opgeslagen");
            $oProductReview = $this->oCrudUserManager->save($aContactMessage);
            $bNext = $this->post('next_overview', null, false);
            if($bNext == '1')
            {
                $this->redirect('/contact/overview');
            }
            else
            {
                $this->redirect('/contact/edit?id='.$oProductReview->getId().$sAddUrl);
            }
        }
        else
        {
            $this->oCrudUserManager->registerValidationErrorStatusMessages($aContactMessage);
        }
    }
    function run()
    {
        $aContactMessage = $this->post('data', null);
        $aContactMessage['id'] = $this->get('id');

        $oContactMessage = $this->oCrudUserManager->getModel($aContactMessage);
        $oContactMessage->setIsNew(false);
        $oContactMessage->save();
        $aParseData['edit_form'] = $this->oCrudUserManager->getRenderedEditHtml($oContactMessage, self::sFormLayoutKey);
        $aTopnavEdit = [
            'module' => 'Contact_message',
            'manager' => 'Contact_messageManager',
            'overview_url' => $this->oCrudUserManager->getOverviewUrl(),
            'edit_config_key' => self::sFormLayoutKey,
            'name' => self::sFormLayoutKey,
            'title' => 'Contact bericht scherm instellen'
        ];
        $aView['top_nav'] = $this->parse('top_nav_edit.twig', $aTopnavEdit);
        $aView['content'] = $this->parse('edit.twig', $aParseData);
        $aView['title'] = Translate::fromCode('Contact bericht').' '.($aContactMessage['id'] ? Translate::fromCode('bewerken') : Translate::fromCode('toevoegen'));

        return $aView;
    }
}