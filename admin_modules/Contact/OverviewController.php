<?php
namespace AdminModules\Contact;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Core\ButtonEventDispatcher;
use Crud\CrudViewManager;
use Crud\Contact_message\CrudContact_messageManager;
use Helper\FilterHelper;
use Helper\OverviewButtonHelper;
use Model\ContactMessageQuery;
use LogicException;
use model\Setting\CrudManager\CrudView;;

class OverviewController extends MainController{

    function doTriggerButtonEvent()
    {
        $iButtonId = $this->get('button_id', null, true, 'numeric');

        $aSaleOrders = $this->get('sale_order', null, true, 'array');
        $aSaleOrders = array_keys($aSaleOrders);

        $bOneHasOutput = false;
        if(!empty($aSaleOrders))
        {
            $bIsLast = false;
            $iCurrent = 1;
            $iNumItems = count($aSaleOrders);
            foreach($aSaleOrders as $iSaleOrderId)
            {
                if($iCurrent == $iNumItems)
                {
                    $bIsLast = true;
                }

                $bEventHasOutput = ButtonEventDispatcher::dispatch($iButtonId, ['order_id' => $iSaleOrderId], 'overview', $bIsLast);
                if($bEventHasOutput)
                {
                    $bOneHasOutput = true;
                }
                $iCurrent++;
            }
        }
        $sAfterEvent = DeferredAction::get('after_event');

        if(!$bOneHasOutput)
        {
            $this->redirect($sAfterEvent);
        }
    }
    function run(){

        DeferredAction::register('after_event', $this->getRequestUri());

        $this->addJsFile('/admin_modules/Contact/overview.js');
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'created_date';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';
        $iCurrentPage = $this->get('p');
        $iCurrentPage = is_numeric($iCurrentPage) ? $iCurrentPage : 1;

        $oContactMessageQuery = ContactMessageQuery::create();
        $oContactMessageQuery->orderBy($sSortField, $sSortDirection);

        $oCrudContact_messageManager = new CrudContact_messageManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudContact_messageManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oCountMessageQuery = ContactMessageQuery::create();
                $oCountMessageQuery = FilterHelper::applyFilters($oCountMessageQuery, $o->getId());
                $o->_sz = $oCountMessageQuery->count();
            }
        }

        $oContactMessageQuery = FilterHelper::applyFilters($oContactMessageQuery, $iTabId);
        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $oContactMessageQuery = FilterHelper::generateVisibleFilters($oContactMessageQuery, $aFilters
            /*,
            function($oQueryObject, $sFilterValue)
            {
                if(!$oQueryObject instanceof CustomerQuery)
                {
                    throw new \Exception\LogicException("Oh no, we got the wrong object type. I was expecting a CustomerQuery object but i got ".get_class($oQueryObject).".");
                }
                $sFilterValue = QueryMapper::quote('%'.$sFilterValue.'%');
                $sQuery = "SELECT customer_id FROM customer_company WHERE company_name LIKE $sFilterValue AND customer_id IS NOT NULL";
                $aRecords = QueryMapper::fetchArray($sQuery);
                $aCustomerIds1 = Utils::flattenArray($aRecords, 'customer_id');

                $sQuery = "SELECT id FROM customer WHERE first_name LIKE $sFilterValue OR last_name LIKE $sFilterValue";
                $aRecords = QueryMapper::fetchArray($sQuery);
                $aCustomerIds2 = Utils::flattenArray($aRecords, 'id');

                $aCustomerIds = array_merge($aCustomerIds1, $aCustomerIds2);

                $oQueryObject->filterById($aCustomerIds);

                return $oQueryObject;
            }
            */
        );

        $oCrudContact_messageManager->setOverviewData($oContactMessageQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aViewData['overview_table'] = $oCrudContact_messageManager->getOverviewHtml($aFields);

        DeferredAction::register('return_after_delete_contact', $this->getRequestUri());
        DeferredAction::register('return_contact', $this->getRequestUri());

        $aTopNavVars = [
            'buttons' => OverviewButtonHelper::getButtons($iTabId),
            'r' => 'return_contact',
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Contact_message',
            'manager' => 'Contact_messageManager',
            'overview_url' => $oCrudContact_messageManager->getOverviewUrl(),
            'new_url' => $oCrudContact_messageManager->getCreateNewUrl(),
            'new_title' => $oCrudContact_messageManager->getNewFormTitle()
        ];
        $sTopNav = $this->parse('Contact/top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView =
            [
                'title' => Translate::fromCode('Contact aanvragen'),
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];
        return $aView;
    }
}
