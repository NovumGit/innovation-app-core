<?php
namespace AdminModules\Contact\Event;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\Mime\VoidMime;
use Core\Translate;
use Model\ContactMessage;
use Model\ContactMessageQuery;

class Delete_message extends AbstractEvent implements AbstractConfigurableEvent{

    function saveConfiguration($sEnvironment)
    {
        return null;
    }

    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        return null;
    }

    function getDescription(): string
    {
        return Translate::fromCode("Verwijder");
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function isConfigurable(): bool
    {
        return false;
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id');

        $oContactMessage = ContactMessageQuery::create()->findOneById($iOrderId);
        if($oContactMessage instanceof ContactMessage)
        {
            $oContactMessage->delete();
        }
    }
}

