<?php
namespace AdminModules\Contact;

use AdminModules\ModuleConfig;
use Core\Translate;
use Model\ContactMessageQuery;

class Config extends ModuleConfig
{
    function getMenuExtraVars(): array
    {
        $oContactMessageQuery = ContactMessageQuery::create();
        $oContactMessageQuery->filterByIsNew(true);
        return [
            'new_message_count' => $oContactMessageQuery->count()
        ];
    }

    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return Translate::fromCode('Contact');
    }
}
