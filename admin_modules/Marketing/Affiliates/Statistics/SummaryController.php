<?php
namespace AdminModules\Marketing\Affiliates\Statistics;

use AdminModules\Marketing\Affiliates\TopNavTrait;
use Core\MainController;
use Core\Translate;

class SummaryController extends MainController
{
    use TopNavTrait;

    function run()
    {
        $sTitle = Translate::fromCode('Statistics');

        $aMain = [];
        return [
            'title' => $sTitle,
            'content' => $this->parse('Marketing/Affiliates/overview.twig', $aMain),
            'top_nav' => $this->parse('Marketing/Affiliates/top_nav_overview.twig', $this->getTopNavVars('statistics'))
        ];
    }
}