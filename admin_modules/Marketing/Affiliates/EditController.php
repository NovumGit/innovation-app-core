<?php
namespace AdminModules\Marketing\Affiliates;

use Core\MainController;
use Core\Translate;

class EditController extends MainController
{

    use TopNavTrait;
    function run()
    {
        $sTitle = Translate::fromCode('Affiliates');

        $aMain = [
            'title' => $sTitle
        ];
        return [
            'title' => $sTitle,
            'content' => $this->parse('Marketing/Affiliates/edit.twig', $aMain),
            'top_nav' => $this->parse('Marketing/Affiliates/top_nav_overview.twig', $this->getTopNavVars('affiliates'))
        ];
    }
}