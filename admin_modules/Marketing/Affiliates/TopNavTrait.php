<?php
namespace AdminModules\Marketing\Affiliates;

use Model\Marketing\Base\MailingTemplateQuery;
use Model\Marketing\MailingListQuery;
use Model\Marketing\MailingMessageQuery;
use Model\Marketing\MailingQuery;

trait TopNavTrait{


    function getTopNavVars($sActivePage)
    {
        return [
            $sActivePage => 'active',
        ];
    }

}