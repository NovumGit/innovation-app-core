<?php
namespace AdminModules\Marketing\Mailing\Cron;

use Core\Mailer;
use Core\MailMessage;
use Core\MainController;
use Exception\LogicException;
use Model\Marketing\Mailing;
use Model\Marketing\MailingMessageQuery;
use Model\Marketing\MailingQuery;

class MailController extends MainController
{
    function doNoLoginRun()
    {
        $this->run();
    }
    function run()
    {
        $iLimit = 10;
        $oMailingQuery = MailingQuery::create();
        $oMailingQuery->filterByIsPlaying(true);
        $oMailingQuery->filterByStatus('Sending');

        $oMailing = $oMailingQuery->findOne();


        if(!$oMailing instanceof Mailing)
        {
            $oMailingQuery = MailingQuery::create();
            $oMailingQuery->filterByStatus('Ready for sending');
            $oMailingQuery->filterByIsPlaying(true);
            $oMailing = $oMailingQuery->findOne();

            if($oMailing instanceof Mailing)
            {
                $oMailing->setStatus('Sending');
                $oMailing->save();
            }
        }
        if(!$oMailing instanceof Mailing)
        {
            exit('all done');
        }
        $iMessageCount = MailingMessageQuery::create()->filterByMailingId($oMailing->getId())->count();


        if($iMessageCount == 0)
        {
            $oMailing->setStatus('Completed');
            $oMailing->save();
        }
        else
        {
            for($iMailCount =0; $iMailCount < $iLimit; $iMailCount++)
            {
                $oMailingMessage = MailingMessageQuery::create()->findOneByMailingId($oMailing->getId());
                $oMailMessage = new MailMessage();
                $oMailMessage->setSubject($oMailingMessage->getSubject());
                $oMailMessage->setFrom($oMailing->getFromAddress());
                $oMailMessage->setBody($oMailingMessage->getContents());
                $oMailMessage->setTo($oMailingMessage->getToAddress());
                Mailer::send($oMailMessage);
                $oMailingMessage->delete();

                $oMailing->setSendCount($oMailing->getSendCount() + 1);
                $oMailing->save();
                sleep(1);
            }
            exit('Limit reached, send '.$iMailCount.' messages.');
        }
        throw new LogicException("Mailing reached the end of the script, this should never happen.");
    }
}
