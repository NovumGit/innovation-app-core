<?php
namespace AdminModules\Marketing\Mailing\Cron;

use AdminModules\Marketing\Mailing\Mailing_list\FilterHelper;
use Core\InlineTemplate;
use Core\MainController;
use Core\Utils;
use Exception\LogicException;
use Model\Crm\CustomerQuery;
use Model\Marketing\Mailing;
use Model\Marketing\MailingMessage;
use Model\Marketing\MailingQuery;

class GeneratorController extends MainController
{
    function doNoLoginRun()
    {
        $this->run();
    }
    function run()
    {
        $oMailingQuery = MailingQuery::create();
        $oMailingQuery->filterByIsPlaying(true);
        $oMailingQuery->filterByStatus('starting');
        $oMailing = $oMailingQuery->findOne();

        if($oMailing instanceof Mailing)
        {
            $oMailing->getMailingList();

            //@todo wnneer er in de toekomst andere bronnen komen voor het verzenden van mailings naast CustomerQuery andere query objecten hier toevoegen

            $oCustomerQuery = CustomerQuery::create();
            $oCustomerQuery->filterByIsNewsletterSubscriber(true);
            $oCustomerQuery->filterByItemDeleted(false);
            $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $oMailing->getMailingListId());
            $oMailing->setStatus('Preparing messages');
            $oMailing->save();

            if($oCustomerQuery instanceof CustomerQuery)
            {
                $i = 0;
                foreach($oCustomerQuery->find() as $oCustomer)
                {
                    $i++;

                    if(!filter_var($oCustomer->getEmail(), FILTER_VALIDATE_EMAIL))
                    {
                        $oMailing->setErrorCount($oMailing->getErrorCount() + 1);
                        $oMailing->save();
                        continue;
                    }
                    $oMailingMessage = new MailingMessage();
                    $oMailingMessage->setMailingId($oMailing->getId());
                    $oMailingMessage->setToAddress($oCustomer->getEmail());

                    echo "$i {$oCustomer->getId()} {$oCustomer->getEmail()} {$oCustomer->getFullName()}. <br>";

                    $bEncoding = Utils::checkEncoding($oCustomer->getFullName(), 'UTF-8');
                    if(!$bEncoding)
                    {
                        throw new LogicException("Bad encoding ".$oCustomer->getFullName());
                    }

                    if(strpos($oCustomer->getFullName(), '\xC4\\'))
                    {
                        throw new LogicException("Bad encoding ".$oCustomer->getFullName());
                    }

                    $oMailingMessage->setToFullName($oCustomer->getFullName());

                    $aViewData['Customer'] = $oCustomer;

                    $sSubject = InlineTemplate::parse($oMailing->getSubject(), $aViewData);
                    $sContents  = InlineTemplate::parse($oMailing->getContents(), $aViewData);

                    $oMailingMessage->setSubject($sSubject);
                    $oMailingMessage->setContents($sContents);
                    $oMailingMessage->save();

                }
            }
            $oMailing->setStatus('Ready for sending');
            $oMailing->save();
        }
        exit('ok');
    }
}
