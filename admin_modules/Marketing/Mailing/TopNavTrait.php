<?php
namespace AdminModules\Marketing\Mailing;

use Model\Marketing\Base\MailingTemplateQuery;
use Model\Marketing\MailingListQuery;
use Model\Marketing\MailingMessageQuery;
use Model\Marketing\MailingQuery;

trait TopNavTrait{

    abstract function getNewUrl();
    abstract function getNewTitle();

    function getTopNavVars($sActivePage)
    {

        return [
            $sActivePage => 'active',
            'new_url' => $this->getNewUrl(),
            'new_title' => $this->getNewTitle(),
            'list_count' => MailingListQuery::create()->count(),
            'mailing_count' => MailingQuery::create()->count(),
            'template_count' => MailingTemplateQuery::create()->count(),
            'queue_count' => MailingMessageQuery::create()->count()
        ];
    }

}