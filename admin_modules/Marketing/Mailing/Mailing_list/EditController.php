<?php
namespace AdminModules\Marketing\Mailing\Mailing_list;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\Customer\CrudCustomerManager;
use Crud\FieldUtilsTrait;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Model\Marketing\Base\MailingListsCriteriaQuery;
use Model\Marketing\MailingList;
use Model\Marketing\MailingListQuery;
use Model\Marketing\MailingListsCriteria;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;

class EditController extends MainController
{
    use FieldUtilsTrait;
    use TopNavTrait;

    function getNewTitle()
    {
        return Translate::fromCode('Lijst toevoegen');
    }
    function getNewUrl()
    {
        return '/marketing/mailing/mailing_list/edit';
    }
    function doDeleteFilter()
    {
        $iFilterId = $this->get('id', null, true, 'numeric');
        $oMailingListsCriteria = MailingListsCriteriaQuery::create()->findOneById($iFilterId);

        $iListId = $oMailingListsCriteria->getMailingListId();
        StatusMessage::success("Criterium verwijderd.", 'Success');
        $oMailingListsCriteria->delete();
        $this->redirect('/marketing/mailing/mailing_list/edit?list_id='.$iListId);
    }
    function doSaveFilter()
    {
        $sField = $this->post('field');
        $sOperator = $this->post('operator');
        $sValue = $this->post('value');
        $iListId = $this->post('list_id', null, false);
        if($iListId)
        {
            $oMailingListQuery = MailingListQuery::create();
            $oMailingList = $oMailingListQuery->findOneById($iListId);
        }
        else
        {
            $oMailingList = new MailingList();
        }
        $sName = $this->post('name');
        $oMailingList->setName($sName);
        $oMailingList->save();

        if($sField && $sOperator && $sValue)
        {
            $oMailingListsCriteria = new MailingListsCriteria();
            $oMailingListsCriteria->setMailingListId($oMailingList->getId());
            $oMailingListsCriteria->setFilterOperatorId($sOperator);
            $oMailingListsCriteria->setFilterName($sField);
            $oMailingListsCriteria->setFilterValue($sValue);
            $oMailingListsCriteria->save();
        }
        $this->redirect("/marketing/mailing/mailing_list/edit?list_id={$oMailingList->getId()}  ");
    }
    function run()
    {
        $this->addJsFile('/admin_modules/Marketing/Mailing/Mailing_list/edit.js');

        $oCrudManager = new CrudCustomerManager();
        $iListId = $this->get('list_id', null, false);
        $sFilterField = $this->post('field');
        $iOperator = $this->post('operator');
        $sFilterValue = $this->post('value');

        $sName = $this->post('name');

        if($iListId)
        {
            $oMailingList = MailingListQuery::create()->findOneById($iListId);
        }
        else
        {
            $oMailingList = new MailingList();
        }

        if($sName)
        {
            $oMailingList->setName($sName);
        }

        $aFields = $this->getFilterableFieldAsLookup($oCrudManager, $sFilterField);
        $oSelectedField = null;

        if($sFilterField)
        {
            $oSelectedField = new $sFilterField;
        }
        $aSelectedFieldLookups = null;
        if($oSelectedField instanceof IFilterableLookupField)
        {
            $aSelectedFieldLookups = $oSelectedField->getLookups();
        }
        Utils::usort_strcmp($aFields, 'title');
        $aCurrentFilters = [];
        if($iListId)
        {
            $aCurrentFilters = FilterHelper::getCurrentListFilters($iListId);
        }

        $oAllFilterOperatorQuery = FilterOperatorQuery::create();
        $oFilterDataType = null;
        $sSelectedFieldDataType = '';

        if($oSelectedField instanceof IFilterableField)
        {
            $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($oSelectedField->getDataType());

            $oFilterOperatorDatatype = FilterOperatorDatatypeQuery::create();
            $oFilterOperatorDatatype->filterByFilterDatatypeId($oFilterDataType->getId());
            $aOperators = $oFilterOperatorDatatype->find();

            $oAllFilterOperatorQuery->filterByFilterOperatorDatatype($aOperators);
            $sSelectedFieldDataType = $oSelectedField->getDataType();
        }

        $aAllFilterOperators = $oAllFilterOperatorQuery->find();
        $aOperatorFilterSelectItems = Utils::makeSelectOptions($aAllFilterOperators, 'getDescription', $iOperator);
        Utils::usort_strcmp($aOperatorFilterSelectItems, 'label');

        $oCurrentlySelectedOperator = null;
        if($iOperator)
        {
            $oCurrentlySelectedOperator = FilterOperatorQuery::create()->findOneById($iOperator);
        }

        $sTitle = Translate::fromCode('Mail lijsten');

        $aData = [
            'mailing_list' => $oMailingList,
            'fields' => $aFields,
            'title' => $sTitle,
            'value' => $sFilterValue,
            'selected_field_lookups' => $aSelectedFieldLookups,
            'selected_field_datatype' => $sSelectedFieldDataType,
            'selected_field_operator' => $oCurrentlySelectedOperator,
            'current_filters' => $aCurrentFilters,
            'operator_filter_select_items' => $aOperatorFilterSelectItems
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('lists')),
            'content' => $this->parse('Marketing\Mailing\Mailing_list\edit.twig', $aData),
        ];
    }
}
