<?php
namespace AdminModules\Marketing\Mailing\Mailing_list;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Model\Marketing\MailingListQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class OverviewController extends MainController
{
    use TopNavTrait;

    function getNewUrl()
    {
        return '/marketing/mailing/mailing_list/edit';
    }
    function getNewTitle()
    {
        return Translate::fromCode('Lijst toevoegen');
    }
    function doDelete()
    {
        $iMessageId = $this->get('id', null, true, 'numeric');
        MailingListQuery::create()->findOneById($iMessageId)->delete();
        $sCancelUrl = "/marketing/mailing/mailing_list/overview";
        StatusMessage::success("Lijst permanent verwijderd.", 'Verwijderd');
        $this->redirect($sCancelUrl);
    }
    function doConfirmDelete()
    {
        $iMessageId = $this->get('id', null, true, 'numeric');
        $sDeleteUrl = "/marketing/mailing/mailing_list/overview?_do=Delete&id=$iMessageId";
        $sCancelUrl = "/marketing/mailing/mailing_list/overview";

        $sDltLabel = Translate::fromCode("Permanent verwijderen");
        $sCancelLabel = Translate::fromCode("Niet verijderen");

        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Annuleren'), $sCancelUrl, $sCancelLabel, 'default'),
            new StatusMessageButton(Translate::fromCode('Ja verwijder'), $sDeleteUrl, $sDltLabel, 'danger'),
        ];
        StatusModal::warning(
            Translate::fromCode("Weet je zeker dat je deze lijst wilt verwijderen?"),
            Translate::fromCode('Zeker weten?'),
            $aButtons);
    }
    function run()
    {
        $sTitle = Translate::fromCode('Mail lijsten');

        $aMailingLists = MailingListQuery::create()->orderByCreatedDate(Criteria::DESC)->find();

        $aData = [
            'title' => $sTitle,
            'mailing_lists' => $aMailingLists,
            'list_count' => MailingListQuery::create()->count()
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('lists')),
            'content' => $this->parse('Marketing\Mailing\Mailing_list\overview.twig', $aData),
        ];
    }
}