<?php
namespace AdminModules\Marketing\Mailing\Mailing_list;

use Crud\AbstractCrudFilter;
use Crud\Field;
use Crud\FilterUtilsTrait;
use Crud\IFilterableField;
use Exception\LogicException;
use Model\Marketing\MailingListsCriteriaQuery;

class FilterHelper extends AbstractCrudFilter
{
    static function getCurrentListFilters($iListId)
    {
        if (!is_numeric($iListId)) {
            throw new LogicException("Het lijst id moet een nummerieke waarde bevatten.");
        }
        $aCurrentListFilters = MailingListsCriteriaQuery::create()->findByMailingListId($iListId);
        $aFilters = [];
        if (!$aCurrentListFilters->isEmpty()) {
            foreach ($aCurrentListFilters as $oCurrentFilter) {

                $sFieldClassName = $oCurrentFilter->getFilterName();
                $oField = new $sFieldClassName();
                if (!$oField instanceof Field) {
                    throw new LogicException("Expected an instance of field but got " . get_class($oField) . ".");
                }
                if (!$oField instanceof IFilterableField) {
                    throw new LogicException("Field " . $oField->getFieldTitle() . " should implement IFilterableField but it does not.");
                }
                $sVisibleValue = self::getVisibleFilterValue($oField, $oCurrentFilter->getFilterValue(), $oCurrentFilter);

                $sActualValue = $oCurrentFilter->getFilterValue();
                if ($sActualValue == 'today') {
                    $sActualValue = date("Y-m-d");
                } else if ($sActualValue == 'now') {
                    $sActualValue = date("Y-m-d H:i:s");
                }
                $aFilters[] = [
                    'object' => $oCurrentFilter,
                    'filter_field' =>
                        [
                            'namespaced_classname' => $sFieldClassName,
                            'classname' => basename(str_replace('\\', '/', $sFieldClassName)),
                            'field_object' => $oField
                        ],
                    'visible_value' => $sVisibleValue,
                    'actual_value' => $sActualValue,
                ];
            }

        }
        return $aFilters;
    }

    /**
     * @param null $iListId (in de get variabele heet deze vaak tabid
     * @return array
     */
    static function getFilters($iListId = null):array
    {
        if(!is_numeric($iListId))
        {
            throw new LogicException("Het lijst moet een nummerieke waarde bevatten.");
        }
        $oMailingListsCriteriaQuery = MailingListsCriteriaQuery::create();
        $aMailingListCriteria = $oMailingListsCriteriaQuery->findByMailingListId($iListId);
        $aFilters = self::buildFilterArray($aMailingListCriteria);

        return $aFilters;
    }
}
