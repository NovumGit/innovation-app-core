<?php
namespace AdminModules\Marketing\Mailing\Compose;

use AdminModules\Marketing\Mailing\Mailing_list\FilterHelper;
use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\CustomerQuery;
use Model\Marketing\Mailing;
use Model\Marketing\MailingListQuery;
use Model\Marketing\MailingProperty;
use Model\Marketing\MailingQuery;
use Model\Marketing\MailingTemplateQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class GenerateController extends MainController
{
    use TopNavTrait;

    function doSave()
    {
        $iId = $this->get('id');
        $aData = $this->post('data');

        if($iId)
        {
            $oMailing = MailingQuery::create()->findOneById($iId);
        }
        else
        {
            $oMailing = new Mailing();
        }

        $oMailing->setFromAddress($aData['from']);
        $oMailing->setSubject($aData['subject']);
        $oMailing->setMailingListId($aData['mailing_list_id']);
        $oMailing->setMailingTemplateId($aData['mailing_template_id']);
        $oMailing->setContents($aData['contents']);
        $oMailing->setStatus('new');
        $oMailing->save();

        $oCustomerQuery = CustomerQuery::create();
        $oCustomerQuery->filterByIsNewsletterSubscriber(true);
        $oCustomerQuery->filterByItemDeleted(false);
        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $oMailing->getMailingListId());

        $oMailing->setTotalCount($oCustomerQuery->count());
        $oMailing->save();

        unset($aData['from'],  $aData['subject'], $aData['mailing_list_id'], $aData['mailing_template_id']);

        if(!empty($aData))
        {
            foreach($aData as $sKey => $sValue)
            {
                MailingProperty::store($oMailing->getId(), $sKey, $sValue);
            }
        }
        StatusMessage::success("Mailing data saved", 'Data saved');
        $this->redirect('/marketing/mailing/compose/generate?id='.$oMailing->getId());
    }

    function getNewUrl()
    {
        return '/marketing/mailing/compose/generate';
    }
    function getNewTitle()
    {
        return Translate::fromCode('Mailing genereren');
    }
    function run()
    {
        $iId = $this->get('id');
        $this->addJsFile('/admin_modules/Marketing/Mailing/Compose/generate.js');

        $sTitle = Translate::fromCode('Mail lijsten');
        $aData = $this->post('data');

        $oMailing = new Mailing();
        if($iId && !$aData)
        {
            $oMailing = MailingQuery::create()->findOneById($iId);
            $aData = MailingProperty::getAllPropertiesAssoc($iId);
        }
        if(isset($aData['from']))
        {
            Setting::store('last_used_mailing_email', $aData['from']);
        }
        $aData['from'] = Setting::get('last_used_mailing_email');

        if(isset($aData['subject']))
        {
            Setting::store('last_used_mailing_subject', $aData['subject']);
        }
        $aData['subject'] = Setting::get('last_used_mailing_subject');

        $oMailingTemplate = null;
        $bShowPreview = false;
        $aFields = null;

        if(!isset($aData['mailing_template_id']))
        {
            $aData['mailing_template_id'] = $oMailing->getMailingTemplateId();
        }
        if(!isset($aData['mailing_list_id']))
        {
            $aData['mailing_list_id'] = $oMailing->getMailingListId();
        }
        if(isset($aData['mailing_template_id']) && isset($aData['mailing_list_id']) && $aData['mailing_template_id'] && $aData['mailing_list_id'])
        {
            $bShowPreview = true;
            $oMailingTemplate = MailingTemplateQuery::create()->findOneById($aData['mailing_template_id']);
            $aFields = MailTemplateHelper::getEditableFields($oMailingTemplate, $aData);
        }

        $aData = [
            'title' => $sTitle,
            'data' =>  $aData,
            'show_preview' => $bShowPreview,
            'custom_fields' => $aFields,
            'current_mailing_template' => $oMailingTemplate,
            'mailing_lists' =>  MailingListQuery::create()->orderByCreatedDate(Criteria::DESC)->find(),
            'mailing_templates' => MailingTemplateQuery::create()->find(),
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('generate')),
            'content' => $this->parse('Marketing\Mailing\Compose\generate.twig', $aData),
        ];
    }
}