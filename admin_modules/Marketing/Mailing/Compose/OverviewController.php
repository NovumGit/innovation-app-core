<?php
namespace AdminModules\Marketing\Mailing\Compose;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Model\Marketing\Mailing;
use Model\Marketing\MailingListQuery;
use Model\Marketing\MailingQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class OverviewController extends MainController
{
    use TopNavTrait;

    function doPlay()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oMailing = MailingQuery::create()->findOneById($iId);
        $oMailing->setIsPlaying(true);
        if($oMailing->getStatus() == 'new')
        {
            $oMailing->setStatus('starting');
        }
        $oMailing->save();
        $this->redirect('/marketing/mailing/compose/overview?id='.$iId);
    }
    function doPauze()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oMailing = MailingQuery::create()->findOneById($iId);
        $oMailing->setIsPlaying(false);
        $oMailing->save();
        $this->redirect('/marketing/mailing/compose/overview?id='.$iId);
    }
    function getNewUrl()
    {
        return '/marketing/mailing/compose/generate';
    }
    function getNewTitle()
    {
        return Translate::fromCode('Mailing toevoegen');
    }
    function doDelete()
    {
        $iMessageId = $this->get('id', null, true, 'numeric');
        MailingQuery::create()->findOneById($iMessageId)->delete();
        $sCancelUrl = "/marketing/mailing/compose/overview";
        StatusMessage::success("Mailing permanent verwijderd.", 'Verwijderd');
        $this->redirect($sCancelUrl);
    }
    function doConfirmDelete()
    {
        $iMessageId = $this->get('id', null, true, 'numeric');
        $sDeleteUrl = "/marketing/mailing/compose/overview?_do=Delete&id=$iMessageId";
        $sCancelUrl = "/marketing/mailing/compose/overview";

        $sDltLabel = Translate::fromCode("Permanent verwijderen");
        $sCancelLabel = Translate::fromCode("Niet verijderen");

        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Annuleren'), $sCancelUrl, $sCancelLabel, 'default'),
            new StatusMessageButton(Translate::fromCode('Ja verwijder'), $sDeleteUrl, $sDltLabel, 'danger'),
        ];
        StatusModal::warning(
            Translate::fromCode("Weet je zeker dat je deze mailing wilt verwijderen? "),
            Translate::fromCode('Zeker weten?'),
            $aButtons);
    }

    function run()
    {
        $this->addJsFile('/admin_modules/Marketing/Mailing/Compose/overview.js');
        $sTitle = Translate::fromCode('Mailing');

        $aMailings = MailingQuery::create()->orderByCreatedDate(Criteria::DESC)->find();

        $aData = [
            'title' => $sTitle,
            'mailings' => $aMailings,
            'list_count' => MailingQuery::create()->count()
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('generate')),
            'content' => $this->parse('Marketing\Mailing\Compose\overview.twig', $aData),
        ];
    }
}