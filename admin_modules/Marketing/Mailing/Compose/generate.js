var oNodeTypeId = $('#fld_node_type_id')
var oMailingListId = $('#fld_mailing_list_id');

oNodeTypeId.change(function(){
    $('#main_form').submit();
});
oMailingListId.change(function(){
    $('#main_form').submit();
});


var aInputFields = $('input, textarea');



var oOrignalHtml = $('#original_html');
console.log(oOrignalHtml);
function updatePreview()
{
    aFields = {};
    aInputFields.each(function(i,e){
        sName = $(e).attr('name');
        console.log(sName);
        if(typeof sName != 'undefined')
        {
            aFields[sName.replace(/data\[/, '').replace(']', '')] = $(e).val()
        }
    });
    // update iframe contents
    sHtml = oOrignalHtml.val();
    aMatches = sHtml.match(/{{[a-zA-Z0-9\s]+\|[a-zA-Z0-9]+\|[a-z:\/.A-Z0-9\s\'-]+}}/g);
    console.log(aMatches);
    if(aMatches !== null)
    {
        for (i = 0; i < aMatches.length; i++)
        {
            sOriginal = aMatches[i];
            sNoHooks = sOriginal.replace('{{', '').replace('}}', '');
            sFieldName = sNoHooks.split('|')[0];
            sHtml = sHtml.replace(sOriginal, aFields[sFieldName]);
        }
    }
    document.getElementById('html_result').src = "data:text/html;charset=utf-8," + escape(sHtml);
    $('#final_contents').val(sHtml);
}
updatePreview();

aInputFields.keyup(function(){
    updatePreview();
});


$('#fld_save').click(function(e){
    e.preventDefault();
    $('#fld_do').val('Save');
    $('#main_form').trigger('submit');
})