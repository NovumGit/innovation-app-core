<?php
namespace AdminModules\Marketing\Mailing\Compose;

use Model\Marketing\MailingTemplate;

class MailTemplateHelper
{
    static function getEditableFields(MailingTemplate $oMailingTemplate, $aPostedData)
    {
        $sContents = $oMailingTemplate->getContents();

        preg_match_all('/{{([a-zA-Z0-9\s]+)\|([a-zA-Z0-9]+)\|([a-z:\/.A-Z0-9\s\' -]+)}}/', $sContents, $aMatches);

        $aOut = [];

        if($aMatches[0])
        {
            foreach($aMatches[0] as $iKey => $sOriginalString)
            {
                $aOut[] = [
                    'original' => $sOriginalString,
                    'field_name' => $aMatches[1][$iKey],
                    'field_type' => $aMatches[2][$iKey],
                    'default_value' => $aMatches[3][$iKey],
                    'current_value' => isset($aPostedData[$aMatches[1][$iKey]]) ? $aPostedData[$aMatches[1][$iKey]] : $aMatches[3][$iKey]
                ];
            }
        }
        return $aOut;
    }
}