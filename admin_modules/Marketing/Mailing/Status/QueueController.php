<?php
namespace AdminModules\Marketing\Mailing\Status;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\Paginate;
use Core\StatusMessage;
use Core\Translate;
use Model\Marketing\MailingMessageQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class QueueController extends MainController
{
    use TopNavTrait;

    function doDelete()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oMailingMessage = MailingMessageQuery::create()->findOneById($iId);
        $oMailingMessage->delete();
        StatusMessage::success(Translate::fromCode('Bericht verwijderd.'));
        $this->redirect('/marketing/mailing/status/queue');
    }
    function doDeleteAll()
    {
        MailingMessageQuery::create()->deleteAll();
        StatusMessage::success(Translate::fromCode('Alle berichten verwijderd.'));
        $this->redirect('/marketing/mailing/status/queue');
    }
    function getNewTitle()
    {
        return null;
    }
    function getNewUrl()
    {
        return null;
    }

    function run()
    {
        $this->addJsFile('/admin_modules/Marketing/Mailing/Status/queue.js');

        $sTitle = Translate::fromCode('Mail wachtrij');
        $iCurrentPage = $this->get('p', 1, false, 'numeric');

        $aMailingMessages = MailingMessageQuery::create();

        $oPaginate =  new Paginate($aMailingMessages, $iCurrentPage, 48);
        $aMailingMessages = $oPaginate->getDataIterator();

        $aData = [
            'title' => $sTitle,
            'mailing_messages' => $aMailingMessages,
            'paginate_html' => $oPaginate->getPaginateHtml(10)
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('queue')),
            'content' => $this->parse('Marketing\Mailing\Status\queue.twig', $aData),
        ];
    }
}