<?php
namespace AdminModules\Marketing\Mailing\Status;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\Translate;
use Model\Marketing\MailingMessageQuery;

class ViewController extends MainController
{



    function doLoadIframe()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oMailingMessage = MailingMessageQuery::create()->findOneById($iId);
        echo $oMailingMessage->getContents();
        exit();
    }
    function run()
    {
        $iId = $this->get('id', null, true, 'numeric');

        $aData = ['id' => $iId];

        $sTitle = Translate::fromCode('Title');
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing/Mailing/Status/top_nav_view.twig', []),
            'content' => $this->parse('Marketing/Mailing/Status/view.twig', $aData),
        ];
    }
}