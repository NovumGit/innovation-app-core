<?php
namespace AdminModules\Marketing\Mailing\Template;

use Core\MainController;
use Model\Marketing\MailingTemplateQuery;

class PreviewController extends MainController
{
    function run()
    {

        $iTemplateId = $this->get('id');

        $oMailingTemplate = MailingTemplateQuery::create()->findOneById($iTemplateId);

        echo $oMailingTemplate->getContents();

        exit();

    }
}