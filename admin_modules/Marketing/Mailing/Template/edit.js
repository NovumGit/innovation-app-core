$('#toggle_view').click(function(e){

    e.preventDefault();

    var  sCurrentAlt = $(this).data('alt');
    var  sCurrentHtml = $(this).html();
    $(this).data('alt', sCurrentHtml);
    $(this).html(sCurrentAlt);

    var oEditor = $('#editor');
    var oPreview = $('#preview');

    if(oPreview.is(':visible'))
    {
        oEditor.css('display', 'block');
        oPreview.css('display', 'none');
    }
    else
    {

      //  http://femaleseeds.nuidev.nl/img/media/7-720x480-png


        // update iframe contents
        sHtml = oEditor.val();
        aMatches = sHtml.match(/{{[a-zA-Z0-9\s]+\|[a-zA-Z0-9]+\|[a-z:\/.A-Z0-9\s\'-]+}}/g);
        console.log(aMatches);
        if(aMatches !== null)
        {
            for (i = 0; i < aMatches.length; i++)
            {
                sOriginal = aMatches[i];
                sNoHooks = sOriginal.replace('{{', '').replace('}}', '');
                sReplacement = sNoHooks.split('|')[2];
                sHtml = sHtml.replace(sOriginal, sReplacement);
            }
        }
        document.getElementById('preview').src = "data:text/html;charset=utf-8," + escape(sHtml);
        oEditor.css('display', 'none');
        oPreview.css('display', 'block');
    }
});

