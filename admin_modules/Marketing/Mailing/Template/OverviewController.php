<?php
namespace AdminModules\Marketing\Mailing\Template;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Model\Marketing\MailingTemplateQuery;

class OverviewController extends MainController
{
    use TopNavTrait;

    function getNewUrl()
    {
        return '/marketing/mailing/template/edit';
    }
    function getNewTitle()
    {
        return Translate::fromCode('Template toevoegen');
    }
    function doDelete()
    {
        $iTemplateId = $this->get('id', null, true, 'numeric');
        MailingTemplateQuery::create()->findOneById($iTemplateId)->delete();
        $sCancelUrl = "/marketing/mailing/template/overview";
        StatusMessage::success("Template permanent verwijderd.", 'Verwijderd');
        $this->redirect($sCancelUrl);
    }
    function doConfirmDelete()
    {
        $iMessageId = $this->get('id', null, true, 'numeric');
        $sDeleteUrl = "/marketing/mailing/template/overview?_do=Delete&id=$iMessageId";
        $sCancelUrl = "/marketing/mailing/template/overview";

        $sDltLabel = Translate::fromCode("Permanent verwijderen");
        $sCancelLabel = Translate::fromCode("Niet verijderen");

        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Annuleren'), $sCancelUrl, $sCancelLabel, 'default'),
            new StatusMessageButton(Translate::fromCode('Ja verwijder'), $sDeleteUrl, $sDltLabel, 'danger'),
        ];
        StatusModal::warning(
            Translate::fromCode("Weet je zeker dat je deze template wilt verwijderen?"),
            Translate::fromCode('Zeker weten?'),
            $aButtons);
    }
    function run()
    {
        $sTitle = Translate::fromCode('Mailing templates');

        $aData = [
            'title' => $sTitle,
            'mailing_templates' => MailingTemplateQuery::create()->find(),
            'template_count' => MailingTemplateQuery::create()->count()
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('template')),
            'content' => $this->parse('Marketing\Mailing\Template\overview.twig', $aData),
        ];
    }

}