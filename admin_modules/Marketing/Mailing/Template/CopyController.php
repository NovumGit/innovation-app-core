<?php
namespace AdminModules\Marketing\Mailing\Template;

use AdminModules\Marketing\Mailing\TopNavTrait;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Marketing\MailingTemplate;
use Model\Marketing\MailingTemplateQuery;

class CopyController extends MainController
{
    use TopNavTrait;

    function run()
    {
        $this->addJsFile('/admin_modules/Marketing/Mailing/Template/edit.js');

        $iMailingTemplateId = $this->get('id');
        $sTitle = Translate::fromCode('Mailing template kopieren');

        $aData = [
            'title' => $sTitle,
            'mailing_template' => MailingTemplateQuery::create()->findOneById($iMailingTemplateId)
        ];
        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Marketing\Mailing\top_nav.twig', $this->getTopNavVars('template')),
            'content' => $this->parse('Marketing\Mailing\Template\copy.twig', $aData),
        ];
    }

    function doCopy()
    {
        $aMailingTemplate = $this->post('mailing_template');
        $iId = $this->post('id');
        $oMailingTemplate = MailingTemplateQuery::create()->findOneById($iId)->copy();
        $oMailingTemplate->setName($aMailingTemplate['name']);
        $oMailingTemplate->save();
        $this->redirect('/marketing/mailing/template/overview');

        exit();
        /*
        $aMailingTemplate = $this->post('mailing_template');


        if($iId)
        {
            $oMailingTemplate = MailingTemplateQuery::create()->findOneById($iId);
        }
        else
        {
            $oMailingTemplate = new MailingTemplate();
        }

        $oMailingTemplate->setName($aMailingTemplate['name']);
        $oMailingTemplate->setContents($aMailingTemplate['contents']);
        $oMailingTemplate->save();
        StatusMessage::success("Template opgeslagen", 'Wijzigingen opgeslagen');

        $this->redirect('/marketing/mailing/template/edit?id='.$oMailingTemplate->getId());
        */
    }
    function getNewUrl()
    {
        return '/marketing/mailing/template/edit';
    }
    function getNewTitle()
    {
        return Translate::fromCode('Template toevoegen');
    }

}