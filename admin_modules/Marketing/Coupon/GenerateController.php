<?php
namespace AdminModules\Marketing\Coupon;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Marketing\Coupon;
use Model\Marketing\CouponQuery;

class GenerateController extends MainController
{

    function doGenerate()
    {
        $iQuantity = $this->post('quantity');
        $iCouponTypeId = $this->get('id', null, true, 'numeric');
        if(!is_numeric($iQuantity))
        {
            StatusMessage::warning(Translate::fromCode("Geen couponnen gegenereerd, geef alstublieft een geldig aantal te genererern couponen op."));
            $this->redirect("/marketing/coupon/generate?id=$iCouponTypeId");
        }

        for($i = 0; $i < $iQuantity; $i++)
        {
            // Coupon codes must be unique at all times.
            $sCouponKey = null;

            while(true)
            {
                $sCouponKey = substr(sha1($i.time().rand(0,999)), 5, 5);
                $oCoupon = CouponQuery::create()->findOneByRedeemCode($sCouponKey);
                if(!$oCoupon instanceof Coupon)
                {
                    // yes we have a unique Coupon code.
                    break;
                }
            }

            $oCoupon = new Coupon();
            $oCoupon->setCouponTypeId($iCouponTypeId);
            $oCoupon->setRedeemCode($sCouponKey);
            $oCoupon->save();

        }
        StatusMessage::success($iQuantity.Translate::fromCode(" couponnen zijn aangemaakt."));
        $this->redirect("/marketing/coupon/edit?id=$iCouponTypeId");
    }
    function run()
    {
        $iCouponTypeId = $this->get('id', null, true, 'numeric');

        $sTitle = Translate::fromCode("Coupon codes genereren.");

        $aTopNavVars =
        [
            'coupon_type_id' => $iCouponTypeId
        ];

        $aViewData = [

        ];

        $aView = [
            'top_nav' => $this->parse('Marketing/Coupon/top_nav_generate.twig', $aTopNavVars),
            'content' => $this->parse('Marketing/Coupon/generate.twig', $aViewData),
            'title' => $sTitle,
            'module' => '',
            'manager' => ''
        ];
        return $aView;
    }
}