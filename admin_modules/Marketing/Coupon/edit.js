var oCouponTypeField = $('#fld_coupon_type');


var oAutosafeRedeemCodes =  $('.autosafe_redeem');

oAutosafeRedeemCodes.keyup(function()
{
    var aData = {
        _do : 'SetRedeemCode',
        id : $(this).data('id'),
        value : $(this).val()
    };
    $.post(window.location, aData, function(aResponseData)
    {

        console.log(aResponseData);

    }, 'json');
});


if(oCouponTypeField.length == 1)
{
    showAppropriateField = function()
    {
        var oAmountField = $('#fld_discount_amount');
        var oPercentageField = $('#fld_discount_percentage');

        if(oCouponTypeField.val() == 'percentage')
        {
            oAmountField.closest('.form-group').hide();
            oPercentageField.closest('.form-group').show();
        }
        else if(oCouponTypeField.val() == 'amount')
        {
            oAmountField.closest('.form-group').show();
            oPercentageField.closest('.form-group').hide();
        }
        else
        {
            oAmountField.closest('.form-group').hide();
            oPercentageField.closest('.form-group').hide();
        }
    };
    oCouponTypeField.change(function()
    {
        showAppropriateField();
    });
    showAppropriateField();
}