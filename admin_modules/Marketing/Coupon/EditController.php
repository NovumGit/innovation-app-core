<?php
namespace AdminModules\Marketing\Coupon;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\Coupon_type\CrudCoupon_typeManager;
use Model\Marketing\Coupon;
use Model\Marketing\CouponQuery;
use Model\Marketing\CouponTypeQuery;

class EditController extends MainController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->oCrudManager = new CrudCoupon_typeManager();
    }
    function doSetRedeemCode()
    {
        $iId = $this->post('id', null, true, 'numeric');
        $sValue = $this->post('value', null, true, 'string');

        $oCoupon = CouponQuery::create()->findOneById($iId);

        if($oCoupon instanceof Coupon)
        {
            $oCoupon->setRedeemCode($sValue);
            $oCoupon->save();
        }
        Utils::jsonOk();
    }
    function doMarkAllGivenOut()
    {
        $iCouponTypeId = $this->get('coupon_type_id', null, true, 'numeric');

        CouponQuery::create()->filterByCouponTypeId($iCouponTypeId)->update(['GivenOut'=>true]);
        StatusMessage::success(Translate::fromCode("Alle couponnen zijn gemarkeerd als uitgegeven."));
        $this->redirect("/marketing/coupon/edit?id=$iCouponTypeId");
    }
    function doMarkGivenOut()
    {
        $iCouponId = $this->get('coupon_id', null, true, 'numeric');
        $iCouponTypeId = $this->get('coupon_type_id', null, true, 'numeric');

        CouponQuery::create()->filterByCouponTypeId($iCouponTypeId)->filterById($iCouponId)->findOne()->setGivenOut(true)->save();
        StatusMessage::success(Translate::fromCode("De coupon is gemarkeerd als uitgegeven."));
        $this->redirect("/marketing/coupon/edit?id=$iCouponTypeId");

    }
    function doDeleteUniqueCoupon()
    {
        $iCouponId = $this->get('coupon_id', null, true, 'numeric');
        $iCouponTypeId = $this->get('coupon_type_id', null, true, 'numeric');
        CouponQuery::create()->filterByCouponTypeId($iCouponTypeId)->filterById($iCouponId)->findOne()->delete();
        StatusMessage::success(Translate::fromCode("De coupon code is verwijderd."));
        $this->redirect("/marketing/coupon/edit?id=$iCouponTypeId");
    }
    function doDelete()
    {
        $iCouponTypeId = $this->get('id', null, true, 'numeric');

        CouponQuery::create()->findByCouponTypeId($iCouponTypeId)->delete();
        CouponTypeQuery::create()->findById($iCouponTypeId)->delete();

        StatusMessage::success(Translate::fromCode("De coupon en de bijbehorende inwisselcodes zijn gewist."));
        $this->redirect('/marketing/coupon/overview');
    }
    function run()
    {
        $iItemId = $this->get('id');
        $oDataObject = $this->oCrudManager->getModel(['id' => $iItemId]);

        $sFormLayoutKey = 'coupon_type_edit_form';
        DeferredAction::register($sFormLayoutKey, $this->getRequestUri());

        $sEditForm = $this->oCrudManager->getRenderedEditHtml($oDataObject, $sFormLayoutKey);
        $sTitle = Translate::fromCode("Coupon aanmaken");

        $aTopNavVars = [
            'module' => 'Coupon_type',
            'manager' => 'Coupon_typeManager',
            'crud_return_url' => $this->getRequestUri(),
            'name' => $sFormLayoutKey,
            'title' => $sTitle
        ];
        $aCoupons = null;
        if(!$oDataObject->isNew())
        {
            $aCoupons = CouponQuery::create()->findByCouponTypeId($oDataObject->getId());
        }

        $aViewData = [
            'edit_form' => $sEditForm,
            'coupon_type' => $oDataObject,
            'coupons' => $aCoupons
        ];
        $aView = [
            'top_nav' => $this->parse('Marketing/Coupon/top_nav_edit.twig', $aTopNavVars),
            'content' => $this->parse('Marketing/Coupon/edit.twig', $aViewData),
            'title' => $sTitle,
            'module' => '',
            'manager' => ''
        ];
        return $aView;
    }
    function doStore()
    {

        $aCoupon_type = $this->post('data', null);
        if($this->oCrudManager->isValid($aCoupon_type))
        {
            $oCoupon_type = $this->oCrudManager->save($aCoupon_type);
            StatusMessage::success("Wijzigingen opgeslagen");
            $this->redirect('/marketing/coupon/edit?id='.$oCoupon_type->getId());
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aCoupon_type);
        }
    }
}


