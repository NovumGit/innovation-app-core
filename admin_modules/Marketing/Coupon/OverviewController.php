<?php
namespace AdminModules\Marketing\Coupon;

use AdminModules\GenericOverviewController;
use Core\Translate;
use Crud\Coupon_type\CrudCoupon_typeManager;
use Crud\FormManager;
use Model\Marketing\CouponTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    function getTopNavTemplate()
    {
        return 'Marketing/Coupon/top_nav_overview.twig';
    }
    function getModule():string
    {
        return 'Coupon_type';
    }
    function getQueryObject():ModelCriteria
    {
        return CouponTypeQuery::create();
    }
    function getManager():FormManager
    {
        return new CrudCoupon_typeManager();
    }
    function getTitle():string
    {
        return Translate::fromCode("Couponnen en vouchers");
    }
}