<?php
namespace AdminModules\Setting\Sale_order\Status_email;

use Crud\Sale_order_notification_type\CrudSale_order_notification_typeManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        return '/setting/sale_order/status_email/overview';
    }
}
