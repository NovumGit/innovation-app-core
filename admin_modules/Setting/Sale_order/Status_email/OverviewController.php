<?php
namespace AdminModules\Setting\Sale_order\Status_email;
use AdminModules\Setting\OverviewController as ParentOverviewController;

use Crud\Sale_order_notification_type\CrudSale_order_notification_typeManager;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;

class OverviewController extends ParentOverviewController
{

    function getLeftTopMenu()
    {
        return $this->parse('Setting/Sale_order/top_nav_left.twig', []);
    }
    function getTopMenuTemplate()
    {
        return 'Setting/top_nav_overview.twig';
    }

    function run()
    {
        return $this->runCrud(new CrudSale_order_notification_typeManager(), Sale_order_notification_typeQuery::create());
    }
}
