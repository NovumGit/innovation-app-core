<?php
namespace AdminModules\Setting\Sale_order;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Product;

class ConfigController extends MainController
{
    function doStore()
    {
        $aData = $this->post('data');

        foreach(['order', 'invoice'] as $sField)
        {
            if(isset($aData[$sField.'_auto_numbering']) && $aData[$sField.'_auto_numbering'] == '1')
            {
                $sNextNumber = trim($aData[$sField.'_next_number']);
                if(!preg_match('/[0-9]+$/', $sNextNumber, $aParts))
                {
                    $sNextNumber = $sNextNumber.'1';
                }
            }
            else if(!isset($aData[$sField.'_auto_numbering']))
            {
                $sNextNumber = null;
            }
            else
            {
                throw new LogicException("The value for order_auto_numbering isset but is not 1, which should never happen. This inidcates a bug.");
            }

            Setting::store($sField.'_auto_numbering', isset($aData[$sField.'_auto_numbering']) ? 1 : 0);
            Setting::store($sField.'_next_number', $sNextNumber);
        }
        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect($this->getRequestUri(false));
    }

    function doNoLoginGetNextDebitorNumber()
    {
        $sNextProductNumberWithPrefix = Product::generateNexProductNumber();
        echo $sNextProductNumberWithPrefix;
        exit();
    }

    function run()
    {
        $sTitle = 'Product instellingen';

        $aMainViewData = [
            'title' => $sTitle,
            'data' => [
                'order_auto_numbering' => Setting::get('order_auto_numbering'),
                'order_next_number' => Setting::get('order_next_number'),
                'invoice_auto_numbering' => Setting::get('invoice_auto_numbering'),
                'invoice_next_number' => Setting::get('invoice_next_number')
            ]
        ];

        $aTopNav = [
            'left_top_menu' => $this->parse('Setting/Sale_order/top_nav_left.twig', [])
        ];

        return [
            'title' => Translate::fromCode($sTitle),
            'top_nav' => $this->parse('Setting/top_nav_overview.twig', $aTopNav),
            'content' => $this->parse('Setting/Sale_order/config.twig', $aMainViewData),
        ];
    }
}
