<?php
namespace AdminModules\Setting\Invoice;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\Customer;
use Model\Setting\MasterTable\Base\Language;
use Model\Setting\MasterTable\Base\LanguageQuery;

class ConfigController extends MainController
{

    function doStore()
    {
        $aData = $this->post('invoice');

        foreach($aData['footer_txt'] as $iLanguageId => $sFooterTxt)
        {
            Setting::store('translated_invoice_footer_txt_'.$iLanguageId, $sFooterTxt);
        }

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect($this->getRequestUri(false));
    }
/*
    function doNoLoginGetNextDebitorNumber()
    {
        $sNextDebitorNumberWithPrefix = Customer::generateNexDebitorNumber();

        echo $sNextDebitorNumberWithPrefix;
        exit();
    }
    */
    function run()
    {
        $sTitle = 'Factuur instellingen';

        $aLanguages = LanguageQuery::create()->findByIsEnabledWebshopOrIsEnabledCms();

        $aFooterTxtTranslations = [];

        foreach($aLanguages as $oLanguage)
        {
            if(!$oLanguage instanceof Language)
            {
                throw new LogicException("Expected an instance of Language");
            }
            $aFooterTxtTranslations[$oLanguage->getId()] = Setting::get('translated_invoice_footer_txt_'.$oLanguage->getId());
        }

        $aMainViewData = [
            'languages' => $aLanguages,
            'translated_footer_txt' => $aFooterTxtTranslations
        ];
        /*
        $aMainViewData = [
            'title' => $sTitle,
            'data' => [
                'customer_auto_debitor_numbering' => Setting::get('customer_auto_debitor_numbering', 1),
                'customer_next_debitor_number' => Setting::get('customer_next_debitor_number', 1)
            ]
        ];
        */

        return [
            'title' => Translate::fromCode($sTitle),
            'content' => $this->parse('Setting/Invoice/config.twig', $aMainViewData)
        ];
    }
}
