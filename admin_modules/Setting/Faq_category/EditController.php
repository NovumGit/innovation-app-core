<?php
namespace AdminModules\Setting\Faq_category;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Core\StatusMessage;
use Core\Translate;
use AdminModules\Setting\EditController as ParentEditController;
use Crud\Site_FAQ_category\CrudSite_FAQ_categoryManager;
use Model\Cms\SiteFAQCategoryQuery;
use Model\Cms\SiteFAQCategoryTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function doDeleteImage()
    {
        $sSite = $this->get('site', null, true);
        $iFaqCategoryid = $this->get('id', null, true, 'numeric');
        $oFaqCategory = SiteFAQCategoryQuery::create()->findOneById($iFaqCategoryid);
        $sFile = $oFaqCategory->getImageFileLocation();

        if(file_exists($sFile))
        {
            unlink($sFile);
        }
        $oFaqCategory->setHasImage(false);
        $oFaqCategory->setImageExt(null);
        $oFaqCategory->save();
        StatusMessage::success(Translate::fromCode("Afbeelding verwijderd"));
        $this->redirect("/setting/faq_category/edit?site=$sSite&id=$iFaqCategoryid");
    }
    function getCrudManager()
    {
        return new CrudSite_FAQ_categoryManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }
    /**
     * @param $iTranslatedItemId
     * @return ModelCriteria
     */
    function getTranslationModels($iTranslatedItemId): ModelCriteria
    {
        $oCategoryTranslationQuery = SiteFAQCategoryTranslationQuery::create();
        return $oCategoryTranslationQuery->filterBySiteFaqCategoryId($iTranslatedItemId);
    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['category_id'] = $iItemId;
        return $aPostedData;
    }
}
