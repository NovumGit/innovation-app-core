<?php
namespace AdminModules\Setting\Faq_category;

use AdminModules\Cms\GeneralController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Site_FAQ_category\CrudSite_FAQ_categoryManager;
use Exception\LogicException;
use Helper\CrudHelper;
use Helper\FilterHelper;
use Model\Cms\SiteFAQCategoryQuery;
use Model\Setting\CrudManager\CrudView;

class OverviewController extends GeneralController
{

    function getTopMenuTemplate()
    {
        return 'Setting/top_nav_overview.twig';
    }

    function run()
    {
        $sSite = $this->get('site', null, true);

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';
        $oCrudSite_FAQ_categoryManager = new CrudSite_FAQ_categoryManager();

        $oCrudview = CrudHelper::getDefaultCrudView($oCrudSite_FAQ_categoryManager);

        if(!$oCrudview instanceof CrudView)
        {
            throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
        }

        $oSiteFAQCategoryQuery = SiteFAQCategoryQuery::create();
        $oSiteFAQCategoryQuery->orderBy($sSortField, $sSortDirection);

        $aFields = CrudViewManager::getFields($oCrudview);
        $oSiteFAQCategoryQuery = FilterHelper::applyFilters($oSiteFAQCategoryQuery, $oCrudview->getId());
        $aSitePageItems = $oSiteFAQCategoryQuery->find();

        $oCrudSite_FAQ_categoryManager->setOverviewData($aSitePageItems);
        $oCrudSite_FAQ_categoryManager->setArgument('site', $sSite);

        $sViewData = $oCrudSite_FAQ_categoryManager->getOverviewHtml($aFields);

        $aMainViewData = [
            'overview_table' => $sViewData
        ];

        $aSitePageViewData = [
            'enabled_modules' => $this->getEnabledModulesTopnav(),
            'new_url' => $oCrudSite_FAQ_categoryManager->getCreateNewUrl(),
            'new_title' => $oCrudSite_FAQ_categoryManager->getNewFormTitle(),
            'site' => $this->get('site', null, true)
        ];
        $sEdit = $this->parse('Setting/Faq_category/overview.twig', $aMainViewData);

        $sTopNav = $this->parse('Setting/Faq_category/overview_top_nav.twig', $aSitePageViewData);
        $aView =
            [
                'title' => Translate::fromCode('Veel gestelde vragen'),
                'top_nav' => $sTopNav,
                'content' => $sEdit
            ];
        return $aView;
    }
}
