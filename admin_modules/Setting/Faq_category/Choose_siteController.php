<?php
namespace AdminModules\Setting\Faq_category;

use Core\Translate;
use Crud\Site\CrudSiteManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Cms\SiteQuery;

class Choose_siteController extends ParentOverviewController {


    function getTopMenuTemplate()
    {
        return 'Setting/top_nav_overview.twig';
    }

    function run()
    {
        $sTitle = Translate::fromCode("Voor welke site wil je de FAQ categorieen aanpassen?");
        $oCrudSiteManager = new CrudSiteManager();
        $oCrudSiteManager->setArgument('button_configurable_url', '/setting/faq_category/overview?site=[[domain]]');
        return $this->runCrud($oCrudSiteManager, SiteQuery::create(), $sTitle);
    }
}
