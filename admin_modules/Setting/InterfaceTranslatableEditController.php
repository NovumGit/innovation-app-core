<?php
namespace AdminModules\Setting;

use Propel\Runtime\ActiveQuery\ModelCriteria;

interface InterfaceTranslatableEditController{

    /**
     * Return an array of Propel models, one for each enabled language. So also an Object for the languages that are not yet created.
     * @param $iTranslatedItemId
     * @return ModelCriteria - (ModelCriteria = a Propel xxxQuery object instance)
     */
    function getTranslationModels($iTranslatedItemId) : ModelCriteria;
    function addUntranslatedItemId($aPostedData, $iItemId) : array;
}