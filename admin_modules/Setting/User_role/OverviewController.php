<?php
namespace AdminModules\Setting\User_role;

use Crud\Role\CrudRoleManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\RoleQuery;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudRoleManager(), RoleQuery::create());
    }
}
