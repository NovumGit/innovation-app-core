<?php
namespace AdminModules\Setting\Shipping\Cost;

use Core\MainController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\IConfigurableCrud;
use Helper\CrudHelper;
use Model\Setting\MasterTable\CountryQuery;
use Crud\Country\CrudCountryShippingManager;

class SettingsController extends MainController  {

    function run()
    {
        $oCrudManager = new CrudCountryShippingManager();
        CrudViewManager::init($oCrudManager);
        $oCrudView = CrudHelper::getDefaultCrudView($oCrudManager);

        $oQueryObject = CountryQuery::create();

        // $aFilters = [];
       // $oQueryObject = FilterHelper::generateVisibleFilters($oQueryObject, $aFilters);

        $aData = $oQueryObject->find();
        $oCrudManager->setOverviewData($aData);
        $aFields = CrudViewManager::getFields($oCrudView);
        $sCountryListTable = $oCrudManager->getOverviewHtml($aFields);

        $aMainView = [
            'overview_table' => $sCountryListTable
        ];

        $aNavView = [
            'implements_configurable_crud' => (new CrudCountryShippingManager() instanceof IConfigurableCrud),
            'module' => 'Country',
            'manager' => 'CountryShippingManager',
            'tab_id' => $oCrudView->getId()
        ];

        $aNavView['top_nav_overview_right'] = $this->parse('Setting/Shipping/Cost/top_nav_overview_right.twig', $aNavView);
        $aOut['top_nav'] = $this->parse('Setting/Shipping/top_nav.twig', $aNavView);
        $aOut['content'] = $this->parse('generic_overview.twig', $aMainView);
        $aOut['title'] = Translate::fromCode('Verzendmethoden en kosten per land');

        return $aOut;
    }
}


