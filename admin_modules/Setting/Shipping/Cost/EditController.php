<?php
namespace AdminModules\Setting\Shipping\Cost;

use Core\MainController;
use Core\Translate;
use Exception\LogicException;
use Model\Setting\MasterTable\Base\CountryShippingMethodQuery;
use Model\Setting\MasterTable\Base\ShippingMethodQuery;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\CountryShippingMethod;

class EditController extends MainController   {

    function doStore()
    {
        $iCountryId = $this->get('country_id', null, true, 'numeric');
        $aData = $this->post('data');

        foreach($aData as $iShippingMethodId => $aValues)
        {
            $oCountryShippingMethod = CountryShippingMethodQuery::create()
                                        ->filterByCountryId($iCountryId)
                                        ->filterByShippingMethodId($iShippingMethodId)
                                        ->findOne();
            if(!$oCountryShippingMethod instanceof CountryShippingMethod)
            {
                $oCountryShippingMethod = new CountryShippingMethod();
                $oCountryShippingMethod->setCountryId($iCountryId);
                $oCountryShippingMethod->setShippingMethodId($iShippingMethodId);
            }
            $oCountryShippingMethod->setIsEnabledWebsite($aValues['is_enabled_website']);
            $oCountryShippingMethod->setPrice($aValues['price']);
            $oCountryShippingMethod->save();
        }
        if($this->post('next_overview') == '1')
        {
            $this->redirect('/setting/shipping/cost/settings');
        }
        else
        {
            $this->redirect($this->getRequestUri());
        }
    }
    function run()
    {
        $iCountryId = $this->get('country_id', null, true, 'numeric');
        $oCountry = CountryQuery::create()->findOneById($iCountryId);

        if(!$oCountry instanceof Country)
        {
            throw new LogicException("That Country is not available anymore.");
        }
        $aShippingMethods = ShippingMethodQuery::create()->orderByName()->find();
        $aShippingData = [];
        foreach($aShippingMethods as $oShippingMethod)
        {
            $aShipping = [
                'name' => $oShippingMethod->getName(),
                'id' => $oShippingMethod->getId()
            ];
            $oCountryShippingMethod = CountryShippingMethodQuery::create()
                ->filterByCountryId($iCountryId)
                ->filterByShippingMethodId($oShippingMethod->getId())
                ->findOne();
            if($oCountryShippingMethod instanceof CountryShippingMethod)
            {
                $aShipping['price'] = $oCountryShippingMethod->getPrice();
                $aShipping['is_enabled_website'] = $oCountryShippingMethod->getIsEnabledWebsite();
            }
            $aShippingData[] = $aShipping;
        }

        $aViewData = [
            'country_id' => $iCountryId,
            'country' => $oCountry,
            'shipping_methods' => $aShippingData,
            'title' => Translate::fromCode('Verzendmethoden voor').' '.$oCountry->getName()
        ];
        $aTopNavVars = [];
        $sTopNavHtml = $this->parse('Setting/top_nav_overview.twig', $aTopNavVars);
        $sMainWindowHtml = $this->parse('Setting/Shipping/Cost/edit.twig', $aViewData);

        $aOut = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => Translate::fromCode('Verzendmethoden en kosten per land')
        ];
        return $aOut;
    }
}


