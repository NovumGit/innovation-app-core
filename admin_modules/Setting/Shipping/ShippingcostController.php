<?php
namespace AdminModules\Setting\Shipping;

use Core\Currency;
use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;

class ShippingcostController extends MainController
{
    function doStore()
    {
        $aData = $this->post('data');
        $fFreeShippingAbove = Currency::formatForDb($aData['free_shipping_above']);
        Setting::store('free_shipping_above', $fFreeShippingAbove);

        $aData = $this->post('data');
        Setting::store('envelope_weight_grams', (int) $aData['envelope_weight_grams']);

        $fSpecialWrapFee = Currency::formatForDb($aData['special_wrap_fee']);
        Setting::store('special_wrap_fee', $fSpecialWrapFee);

        StatusMessage::success(Translate::fromCode("Wijzigingen opgelagen"));
        $this->redirect('/setting/shipping/shippingcost');

    }

    function run()
    {
        $aView['free_shipping_above'] = Currency::formatForPresentation(Setting::get('free_shipping_above'));
        $aView['envelope_weight_grams'] = (int) Setting::get('envelope_weight_grams');
        $aView['special_wrap_fee'] = Currency::formatForPresentation(Setting::get('special_wrap_fee'));

        $aOut['title'] = Translate::fromCode("Verzendkosten instellen");
        $aOut['top_nav'] = $this->parse('Setting/Shipping/top_nav.twig', $aView);
        $aOut['content'] = $this->parse('Setting/Shipping/shippingcost.twig', $aView);

        return $aOut;
    }
}