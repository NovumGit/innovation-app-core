<?php
namespace AdminModules\Setting;

use Core\MainController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\IConfigurableCrud;
use Crud\IEditableCrud;
use Crud\FormManager;
use Helper\CrudHelper;
use Helper\FilterHelper;
use InvalidArgumentException;
use ReflectionClass;

abstract class OverviewController extends MainController
{

    function run()
    {
        // wordt niet aangeroepen
    }
    function runCrud(FormManager $oCrudManager, $oQueryObject, $sTitleOverride = null)
    {
        if($sTitleOverride)
        {
            $sTitle = $sTitleOverride;
        }
        else
        {
            $sTitle = Translate::fromCode('Instellingen');
        }

        $oReflector = new ReflectionClass($oCrudManager);
        $aParts = explode('\\', $oReflector->getNamespaceName());
        $sModule = $aParts[1];
        $sManager = $aParts[1].'Manager';

        CrudViewManager::init($oCrudManager);

        $oCrudView = CrudHelper::getDefaultCrudView($oCrudManager);

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        if(method_exists($oQueryObject, 'orderBy'))
        {
            $oQueryObject->orderBy($sSortField, $sSortDirection);
        }

        if(!method_exists($oQueryObject, 'find'))
        {
            throw new InvalidArgumentException("Could not find query object.");
        }

        if(!method_exists($oQueryObject, 'find'))
        {
            throw new InvalidArgumentException("Could not find query object.");
        }

        if(!$oCrudManager instanceof IEditableCrud)
        {
            throw new InvalidArgumentException(get_class($oCrudManager )." " .Translate::fromCode('moet IEditableCrud implementeren.'));
        }

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($oCrudView->getId());

        $oQueryObject = $oUserQuery = FilterHelper::generateVisibleFilters($oQueryObject, $aFilters);

        $aData = $oQueryObject->find();

        $oCrudManager->setOverviewData($aData);
        $aFields = CrudViewManager::getFields($oCrudView);
        $aViewData['filter_html'] = FilterHelper::renderHtml($oCrudView->getId());
        $aViewData['title'] = $sTitle;
        $aViewData['overview_table'] = $oCrudManager->getOverviewHtml($aFields);

        $aTopNavVars = [
            'new_url' => $oCrudManager->getCreateNewUrl(),
            'new_title' => $oCrudManager->getNewFormTitle(),
            'implements_configurable_crud' => ($oCrudManager instanceof IConfigurableCrud),
            'module' => $sModule,
            'manager' => $sManager,
            'tab_id' => $oCrudView->getId()
        ];

        if(method_exists($this, 'getLeftTopMenu'))
        {
            $aTopNavVars['left_top_menu'] = $this->getLeftTopMenu();

        }
        if(method_exists($this, 'getTopMenuTemplate'))
        {
            $sTopNavTemplate = $this->getTopMenuTemplate();
            $sTopNav = $this->parse($sTopNavTemplate, $aTopNavVars);
        }

        if(!isset($sTopNav))
        {
            $sTopNav = $this->parse('Setting/top_nav_overview.twig', $aTopNavVars);
        }
        $sParseResult = $this->parse('generic_overview.twig', $aViewData);

        $aView =
            [
                'title' => $sTitle,
                'top_nav' => $sTopNav,
                'content' => $sParseResult
            ];
        return $aView;

    }
}
