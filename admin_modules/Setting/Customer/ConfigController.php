<?php
namespace AdminModules\Setting\Customer;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\Customer;

class ConfigController extends MainController
{
    function doStore()
    {
        $aData = $this->post('data');

        if(isset($aData['customer_auto_debitor_numbering']) && $aData['customer_auto_debitor_numbering'] == '1')
        {
            $sCustomerNextDebitorNumber = trim($aData['customer_next_debitor_number']);
            if(!preg_match('/[0-9]+$/', $sCustomerNextDebitorNumber, $aParts))
            {
                $sCustomerNextDebitorNumber = $sCustomerNextDebitorNumber.'1';
            }
        }
        else if(!isset($aData['customer_auto_debitor_numbering']))
        {
            $sCustomerNextDebitorNumber = null;
        }
        else
        {
            throw new LogicException("The value for customer_auto_debitor_numbering isset but is not 1, which should never happen. This inidcates a bug.");
        }

        Setting::store('customer_auto_debitor_numbering', isset($aData['customer_auto_debitor_numbering']) ? 1 : 0);
        Setting::store('customer_next_debitor_number', $sCustomerNextDebitorNumber);

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect($this->getRequestUri(false));
    }

    function doNoLoginGetNextDebitorNumber()
    {
        $sNextDebitorNumberWithPrefix = Customer::generateNexDebitorNumber();

        echo $sNextDebitorNumberWithPrefix;
        exit();
    }

    function run()
    {
        $sTitle = 'Klant instellingen';

        $aMainViewData = [
            'title' => $sTitle,
            'data' => [
                'customer_auto_debitor_numbering' => Setting::get('customer_auto_debitor_numbering', 1),
                'customer_next_debitor_number' => Setting::get('customer_next_debitor_number', 1)
            ]
        ];


        return [
            'title' => Translate::fromCode($sTitle),
            'content' => $this->parse('Setting/Customer/config.twig', $aMainViewData)
        ];
    }
}
