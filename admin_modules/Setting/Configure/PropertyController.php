<?php
namespace AdminModules\Setting\Configure;


use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\User as CoreUser;
use Exception\LogicException;
use Model\Account\User;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;

class PropertyController extends MainController
{


    function doSetLanguage()
    {

        $sValue = $this->get('value', null, true, 'numeric');

        $oLanguage = LanguageQuery::create()->filterById($sValue)->filterByIsEnabledCms(true)->findOne();

        if(!$oLanguage instanceof Language)
        {
            StatusMessage::warning(Translate::fromCode("Kon de taal niet aanpassen, de gekozen taal lijkt uit te staan of komt niet voor in dit systeem"));
        }
        else
        {
            Translate::storeLanguageIdInCookie($sValue);

            $oUser = CoreUser::getMember();

            if($oUser instanceof User)
            {
                $oUser->setPreferedLanguageId($sValue);
                $oUser->save();
                CoreUser::setMember($oUser);
            }

            StatusMessage::success(Translate::fromCode('Taal gewijzigd naar').' '.$oLanguage->getDescription());
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }


    function run()
    {
        throw new LogicException(Translate::fromCode("Oeps, er is iets fout gegaan. Onze excuses."));
    }

}
