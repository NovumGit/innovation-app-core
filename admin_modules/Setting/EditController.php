<?php
namespace AdminModules\Setting;

use Core\DeferredAction;
use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Crud\IConfigurableCrud;
use Crud\FormManager;
use Crud\RenderCrudTranslationForm;
use Exception\LogicException;
use ReflectionClass;
use Core\Translate;

abstract class EditController extends MainController
{
    /**
     * @return FormManager
     */
    abstract function getCrudManager();
    abstract function getOverviewUrl();

    private $sModule;
    private $sManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $oReflector = new ReflectionClass($this->getCrudManager());
        $aParts = explode('\\', $oReflector->getNamespaceName());
        $this->sModule = $aParts[1];
        $this->sManager = $aParts[1].'Manager';
    }
    function getEditTemplate()
    {
        return 'edit.twig';
    }
    function run()
    {
        $sDoChangeFieldConfig = 'return_edit';
        DeferredAction::register($sDoChangeFieldConfig, $this->getRequestUri());

        $sRenderedEditHtmlKey = 'lookup_editor';
        $oCrudManager = $this->getCrudManager();

        // @todo fixen dat het formulier gevuld wordt.
        // $aPostedData = $this->post('data', null);
        $iCurrentItemId = $this->get('id');

        $oModel = $oCrudManager->getModel(['id' =>  $iCurrentItemId]);

        $aParseData['edit_form'] = $oCrudManager->getRenderedEditHtml($oModel, $sRenderedEditHtmlKey);


        $aTopNavVars = [
            'overview_url' => $this->getOverviewUrl(),
            'do_after_save' => $sDoChangeFieldConfig,
            'module' => $this->sModule,
            'manager' => $this->sManager,
            'name' => $sRenderedEditHtmlKey,
            'title' => Translate::fromCode('eigenschappen'),
            'has_translated_fields' => false
        ];

        if($this instanceof InterfaceTranslatableEditController)
        {
            $aTopNavVars['has_translated_fields'] = true;
            $aTopNavVars['translate_module'] = $this->sModule.'_translation';
            $aTopNavVars['translate_manager'] = $this->sModule.'_translationManager';

            $sNamespacedTranslationManager = '\\Crud\\'.$aTopNavVars['translate_module'].'\\Crud'.$aTopNavVars['translate_manager'];

            $aTopNavVars['translate_name'] = $sRenderedEditHtmlKey.'_translation';
            $aTopNavVars['translate_title'] = Translate::fromCode('Vertalingen');

            $oTranslatedCrudManager = new $sNamespacedTranslationManager;
            if(!$oTranslatedCrudManager instanceof FormManager)
            {
                throw new LogicException('Expected an instance of Manager but class '.$aTopNavVars['translate_manager'].' is not.');
            }

            $sTranslationForm = RenderCrudTranslationForm::renderForm($this->getTranslationModels($iCurrentItemId), $oTranslatedCrudManager, $aTopNavVars['translate_name']);
            $aParseData['edit_form'] .= $sTranslationForm;
        }

        $sContent = $this->parse($this->getEditTemplate(), $aParseData);
        $sTopNav = $this->parse('Setting/top_nav_edit.twig', $aTopNavVars);

        $aView = [
            'top_nav' => $sTopNav,
            'content' => $sContent,
            'title' => Translate::fromCode('Instellingen')
        ];

        return $aView;
    }
    function doStore()
    {

        $aDataGet = $this->get('data');
        $aData = $this->post('data');

        if(is_array($aDataGet))
        {
            $aData = array_merge($aData, $aDataGet);
        }

        $oCrudmanager = $this->getCrudManager();

        if($oCrudmanager->isValid($aData))
        {
            $sAction = empty($aData['id']) ?  Translate::fromCode('toegevoegd') : Translate::fromCode('gewijzigd');

            $oObject = $oCrudmanager->save($aData);

            $iItemId = null;

            if(method_exists($oObject, 'getId'))
            {
                $iItemId = $oObject->getId();
            }
            else
            {
                throw new LogicException("I tried to get the id of an object of type ".get_class($oObject)." but it does not exist.");
            }
            $sEntityTitle = $oCrudmanager->getEntityTitle();

            if(method_exists($oObject, 'getName'))
            {
                $sName = $oObject->getName();
                LogActivity::register('Setting', 'doStore', "Heeft \"$sName\" als \"$sEntityTitle\" $sAction.");
            }
            else
            {
                LogActivity::register('Setting', 'doStore', "Heeft een \"$sEntityTitle\" $sAction.");
            }

            if($this instanceof InterfaceTranslatableEditController)
            {

                $sNamespacedTranslationManager = '\\Crud\\' . $this->sModule . '_translation\\Crud' . $this->sModule . '_translationManager';
                $oNamespacedTranslationManager = new $sNamespacedTranslationManager;

                if($oNamespacedTranslationManager instanceof FormManager)
                {
                    if(isset($aData['translation']) && !empty($aData['translation']))
                    {
                        foreach ($aData['translation'] as $iLanguageId => $aTranslatedData)
                        {
                            $aTranslatedData['language_id'] = $iLanguageId;
                            $aTranslatedData = $this->addUntranslatedItemId($aTranslatedData, $iItemId);
                            $oNamespacedTranslationManager->save($aTranslatedData);
                        }
                    }
                }
                else
                {
                    throw new LogicException("\$oNamespacedTranslationManager must be an instance of Manager");
                }
            }

            StatusMessage::success(ucfirst($sEntityTitle )." $sAction.");
            $sReturn = $this->get('return');
            if($sReturn)
            {
                $sUrl = DeferredAction::get($sReturn);
            }
            else
            {
                $sUrl = $this->makeUrl($this->getRequestUri(true), ['id' => $oObject->getId()]);
            }

            if($this->post('next_overview') == '1' && $oCrudmanager instanceof IConfigurableCrud)
            {
                $this->redirect($oCrudmanager->getOverviewUrl());
            }
            else
            {
                $this->redirect($sUrl);
            }
        }
        else
        {
            $this->getCrudManager()->registerValidationErrorStatusMessages($aData);
        }

    }

    function doDelete()
    {
        $iId = $this->get('id');
        $oCrudManager = $this->getCrudManager();
        $oModel = $oCrudManager->getModel(['id' => $iId]);

        $sEntityTitle = $oCrudManager->getEntityTitle();

        if(method_exists($oModel, 'getName'))
        {
            $sName = $oModel->getName();
            LogActivity::register('Setting', 'doDelete', "Heeft \"$sName\" als \"$sEntityTitle\" verwijderd.");
        }
        else
        {
            LogActivity::register('Setting', 'doDelete', "Heeft een \"$sEntityTitle\" verwijderd.");
        }


        if(!method_exists($oModel, 'delete'))
        {
            throw new LogicException("I tried to delete a record but the object does not support this action in ".get_class($oModel));
        }


        StatusMessage::info("Item verwijderd.");
        $oModel->delete();

        $this->redirect($this->getOverviewUrl());
    }

}
