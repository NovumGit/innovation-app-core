<?php
namespace AdminModules\Setting;

use AdminModules\ModuleConfig;
use Core\Translate;
use Crud\Link_block_menu_setting\CrudLink_block_menu_settingManager;

class Config extends ModuleConfig
{
    function getMenuExtraVars(): array
    {
        return [
            'link_array' => (new CrudLink_block_menu_settingManager())->getLinkArray('settings')
        ];
    }
    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return  Translate::fromCode('Instellingen');
    }
}
