<?php
namespace AdminModules\Setting\Category;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Helper\CategoryTreeEditorHelper;
use Model\Category\CategoryQuery;

class OverviewController extends MainController
{
    function doStoreTreeInternal($aData = null, $iParentId = null)
    {
        $iSorting = 0;

        if(!empty($aData))
        {
            foreach($aData as $aRow)
            {
                $iCategoryId = preg_replace('/^c_/', '', $aRow['id']);
                $oCategory = CategoryQuery::create()->findOneById($iCategoryId);
                $oCategory->setParentId($iParentId);
                $oCategory->setSorting($iSorting);
                $oCategory->save();
                $iSorting++;
                if(isset($aRow['children']) && !empty($aRow['children']))
                {
                    $this->doStoreTreeInternal($aRow['children'], $iCategoryId);
                }
            }
        }
    }
    function doStoreTree()
    {
        $sData = $this->post('tree');
        $aData = json_decode($sData, true);

        // Utils::jsonOk would break recursion if we would place that exit think on the end of doStoreTreeInternal hence two methods
        $this->doStoreTreeInternal($aData, null);

        StatusMessage::success("Wijzigingen opgeslagen");
        Utils::jsonOk();
    }
    function run()
    {

        $this->addJsFile('/assets/plugins/jstree/dist/jstree.js');
        $this->addCssFile('/assets/plugins/jstree/dist/themes/default/style.min.css');

        $aContent = [];
        $aContent[] = "        <div id=\"jstree\">";
        $aContent[] = '            <ul>';
        $aContent[] =                   CategoryTreeEditorHelper::getTree(null);
        $aContent[] = '            </ul>';
        $aContent[] = '        </div>';
        $sContent = join(PHP_EOL, $aContent);

        $aOverview = [
            'content' => $sContent
        ];

        $aOut = [
            'top_nav' => $this->parse('Setting/Category/top_nav_overview.twig', []),
            'content' =>  $this->parse('Setting/Category/overview.twig', $aOverview),
            'title' => Translate::fromCode('Categorieën bewerken')
        ];
        return $aOut;
    }
}
