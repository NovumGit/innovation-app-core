<?php
namespace AdminModules\Setting\Category;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Category\CrudCategoryManager;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Category\CategoryQuery;
use Model\Category\CategoryTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function __construct($aGet, $aPost)
    {
        if(isset($aGet['parent_id']))
        {

            $aPost['parent_id'] = $aGet['parent_id'];
        }

        parent::__construct($aGet, $aPost);
    }
    function doConfirmDelete()
    {
        $iCategoryId = $this->get('id', null, true, 'numeric');

        $sMessage = Translate::fromCode("Weet je zeker dat je deze categorie wilt verwijderen?");
        $sTitle = Translate::fromCode("Zeker weten?");
        $sOkUrl = '/setting/category/edit?id='.$iCategoryId.'&_do=Delete';
        $sNOUrl = '/setting/category/overview';
        $sYes = Translate::fromCode("Ja");
        $sCancel = Translate::fromCode("Annuleren");
        $aButtons  = [
            new StatusMessageButton($sYes, $sOkUrl, $sYes, 'warning'),
            new StatusMessageButton($sCancel, $sNOUrl, $sCancel, 'info'),
        ];
        StatusModal::warning($sMessage, $sTitle, $aButtons);
    }


    function doDeleteImage()
    {
        $iCategoryid = $this->get('id', null, true, 'numeric');
        $oCategory = CategoryQuery::create()->findOneById($iCategoryid);
        $sFile = $oCategory->getImagePath();
        if(file_exists($sFile))
        {
            unlink($sFile);
        }
        $oCategory->setHasImage(false);
        $oCategory->setImageExt(null);
        $oCategory->save();
        StatusMessage::success(Translate::fromCode("Afbeelding verwijderd"));
        $this->redirect('/setting/category/edit?id='.$iCategoryid);
    }

    function getCrudManager()
    {
        return new CrudCategoryManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }

    /**
     * @param $iTranslatedItemId
     *
     * @return ModelCriteria
     */
    function getTranslationModels($iTranslatedItemId): ModelCriteria
    {
        $oCategoryTranslationQuery = CategoryTranslationQuery::create();
        return $oCategoryTranslationQuery->filterByCategoryId($iTranslatedItemId);

    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['category_id'] = $iItemId;
        return $aPostedData;
    }
}
