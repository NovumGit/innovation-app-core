var oTree = $('#jstree');
oTree.jstree({
    plugins : [
        "contextmenu",
        "dnd",
        "state"
    ],
    types : {
        "#": {
            "max_children": 1, /* Hoeveel root nodes? */
            "max_depth": 2,
            "valid_children": ["root"]}
    },
    core : {
        check_callback : true,
        multiple : false,
        strings :{
            'New node' : $('#new_node').val(),
            'Loading ...' : $('#loadingnodes').val(),
            'nodes': $('#nodes').val()
        }
    },
    contextmenu : {
        items : {
            create: {
                separator_before : false,
                separator_after : false,
                label : 'Toevoegen',
                title : 'Vertaal dit menu item voor de webshop',
                action : function (env)
                {
                    var iId = $(env.reference[0]).attr('id').toString().replace(/^c_/, '').replace(/_anchor$/, '');
                    window.location = '/setting/category/edit?data[parent_id]='+iId;
                },
                icon : 'fa fa-magic'
            },
            edit: {
                separator_before : false,
                separator_after : false,
                label : 'Bewerken',
                title : 'Vertaal dit menu item voor de webshop',
                icon : 'fa fa-edit',
                action : function (env)
                {
                    var iId = $(env.reference[0]).attr('id').toString().replace(/^c_/, '').replace(/_anchor$/, '');
                    window.location = '/setting/category/edit?id='+iId;
                }

            },
            delete: {
                separator_before : false,
                separator_after : false,
                label : 'Verwijderen',
                title : 'Vertaal dit menu item voor de webshop',
                icon : 'fa fa-trash-o',
                action : function (env)
                {
                    var iId = $(env.reference[0]).attr('id').toString().replace(/^c_/, '').replace(/_anchor$/, '');
                    window.location = '/setting/category/edit?id='+iId+'&_do=ConfirmDelete';
                }
            }
        }
    }
});

$('#fld_save_changes').on('click', function(e)
{
    e.preventDefault();
    var oTreeData = $('#jstree').jstree("get_json");
    var sTreeData = JSON.stringify(oTreeData);

    $.ajax({
        url : '/setting/category/overview?_do=StoreTree',
        async : true,
        method : 'POST',
        data: {tree : sTreeData},
        beforeSend : null,
        cache : false,
        success : function(data)
        {
            console.log(data);
            window.location = window.location;
        },
        dataType : 'json',
        error : function(xhr,status,error)
        {
            // alert('1 Something went wrong, we got an error from the server: ' + error);
        }
    });

});
