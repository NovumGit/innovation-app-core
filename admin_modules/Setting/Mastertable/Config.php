<?php
namespace Modules\Setting\MasterTable;

use Modules\ModuleConfig;

class Config extends ModuleConfig{

    function getModuleTitle()
    {
        return 'Stam tabellen';
    }

    function getMenu()
    {
        return [];
    }
}