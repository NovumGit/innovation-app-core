<?php
namespace AdminModules\Setting\Mastertable\Material;

use Crud\Material\CrudMaterialManager;

use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\ProductMaterialQuery;

class OverviewController extends ParentOverviewController
{

    function run()
    {


        return $this->runCrud(new CrudMaterialManager(), ProductMaterialQuery::create());

    }
}
