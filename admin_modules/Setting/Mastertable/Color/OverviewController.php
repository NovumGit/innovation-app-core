<?php
namespace AdminModules\Setting\Mastertable\Color;

use Crud\Color\CrudColorManager as CrudInstanceManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\ColorQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
