<?php
namespace AdminModules\Setting\Mastertable\Color;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Crud\Color\CrudColorManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Setting\MasterTable\ColorTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        $oCrudInstanceManager = new CrudInstanceManager();
        return $oCrudInstanceManager->getOverviewUrl();
    }

    /**
     * @param $UsageId
     *
     * @return ModelCriteria|string
     */
    function getTranslationModels($UsageId): ModelCriteria
    {
        $oColorTranslationQuery = new ColorTranslationQuery();
        return $oColorTranslationQuery->filterByColorId($UsageId);

    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['color_id'] = $iItemId;
        return $aPostedData;
    }
}
