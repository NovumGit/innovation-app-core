<?php
namespace AdminModules\Setting\Mastertable\Contact_message_status;
use Crud\Contact_message_status\CrudContact_message_statusManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\ContactMessageStatusQuery;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudContact_message_statusManager(), ContactMessageStatusQuery::create());
    }
}
