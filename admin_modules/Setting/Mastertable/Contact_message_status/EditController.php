<?php
namespace AdminModules\Setting\Mastertable\Contact_message_status;

use Crud\Contact_message_status\CrudContact_message_statusManager;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudContact_message_statusManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }
}
