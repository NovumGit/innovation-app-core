<?php
namespace AdminModules\Setting\Mastertable\Unit;

use Crud\Unit\CrudUnitManager;

use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\ProductUnitQuery;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudUnitManager(), ProductUnitQuery::create());
    }
}
