<?php
namespace AdminModules\Setting\Mastertable\Vat;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Core\StatusMessage;
use Crud\Vat\CrudVatManager;
use Model\CustomerQuery;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Setting\MasterTable\VatTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{

    function doDelete()
    {
        $iId = $this->get('id');

        $aProducts = CustomerQuery::create()->filterByItemDeleted(0)->findByVatId($iId);

        if(!$aProducts->isEmpty())
        {
            StatusMessage::warning("Er zijn ".$aProducts->count()." producten die in dit BTW tarief vallen. Het BTW tarief kan daarom niet verwijderd worden.");
        }
        else
        {
            parent::doDelete();
        }
    }


    function getCrudManager()
    {
        return new CrudVatManager();
    }
    function getOverviewUrl()
    {
        return '/setting/mastertable/vat/overview';
    }

    /**
     * @param $iTranslatedItemId
     *
     * @return ModelCriteria
     */
    function getTranslationModels($iTranslatedItemId): ModelCriteria
    {
        $oVatTranslationQuery = VatTranslationQuery::create();
        return $oVatTranslationQuery->filterByVatId($iTranslatedItemId);
    }

    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['vat_id'] = $iItemId;
        return $aPostedData;
    }
}
