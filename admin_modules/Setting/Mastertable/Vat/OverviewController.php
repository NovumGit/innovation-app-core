<?php
namespace AdminModules\Setting\Mastertable\Vat;

use Core\Translate;
use Crud\Vat\CrudVatManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\VatQuery;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudVatManager(), VatQuery::create());
    }
}
