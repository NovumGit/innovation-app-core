<?php
namespace AdminModules\Setting\Mastertable\Brand;

use Crud\Brand\CrudBrandManager as CrudInstanceManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\BrandQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
