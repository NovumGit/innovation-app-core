<?php
namespace AdminModules\Setting\Mastertable\Brand;

use Crud\Brand\CrudBrandManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        $oCrudInstanceManager = new CrudInstanceManager();
        return $oCrudInstanceManager->getOverviewUrl();
    }

}
