<?php
namespace AdminModules\Setting\Mastertable\Legal_form;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Crud\Legal_form\CrudLegal_formManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Setting\MasterTable\LegalFormTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        return '/setting/mastertable/legal_form/overview';
    }

    /**
     * @param $iTranslatedItemId
     *
     * @return ModelCriteria|string
     */
    function getTranslationModels($iTranslatedItemId): ModelCriteria
    {
        return LegalFormTranslationQuery::create()->filterByLegalFormId($iTranslatedItemId);
    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['legal_form_id'] = $iItemId;
        return $aPostedData;
    }
}
