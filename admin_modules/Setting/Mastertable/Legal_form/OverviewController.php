<?php
namespace AdminModules\Setting\Mastertable\Legal_form;

use Crud\Legal_form\CrudLegal_formManager as CrudInstanceManager;

use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\LegalFormQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
