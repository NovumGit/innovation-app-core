<?php
namespace AdminModules\Setting\Mastertable\Contact_message_type;

use Crud\Contact_message_type\CrudContact_message_typeManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\ContactMessageTypeQuery;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudContact_message_typeManager(), ContactMessageTypeQuery::create());
    }
}
