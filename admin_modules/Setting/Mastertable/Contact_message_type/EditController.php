<?php
namespace AdminModules\Setting\Mastertable\Contact_message_type;

use Crud\Contact_message_type\CrudContact_message_typeManager;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudContact_message_typeManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }
}
