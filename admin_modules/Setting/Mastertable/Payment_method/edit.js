var oPspField =  $('#fld_psp');
var oCodeField = $('#fld_code');
oPspField.change(function()
{
    var aArguments = {
        _do: 'GetPaymentMethods',
        Psp : $(this).val()

    };
    $.post('/setting/mastertable/payment_method/edit', aArguments, function(data)
    {
        oCodeField.html('');
        $.each(data['psp_paymethods'], function(key, item)
        {

            console.log(key);
            console.log(item);
            oCodeField.append($('<option>', {
                value: key,
                text : key
            }));
        });

    }, 'json');

});

