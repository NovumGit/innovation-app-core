<?php
namespace AdminModules\Setting\Mastertable\Payment_method;

use Crud\Payment_method\CrudPayment_methodManager as CrudInstanceManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\PaymentMethodQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
