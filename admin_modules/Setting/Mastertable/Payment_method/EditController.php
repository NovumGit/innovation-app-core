<?php
namespace AdminModules\Setting\Mastertable\Payment_method;

use Crud\Payment_method\CrudPayment_methodManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;
use Exception\LogicException;
use Model\Sale\SaleOrder;
use Psp\AbstractPsp;


class EditController extends ParentEditController
{
    function doGetPaymentMethods()
    {
        $sPsP = $this->post('Psp',null, true);
        $sClassName = '\\Psp\\Psp'.$sPsP;


        $oPsp = new $sClassName;

        if(!$oPsp instanceof AbstractPsp)
        {
            throw new LogicException("Expected an instance of AbstractPsp");
        }
        $aResponse = ['psp_paymethods' => $oPsp->getPaymethods('nl', 'EUR', 100, new SaleOrder())];
        echo json_encode($aResponse);
        exit();
    }

    function getEditTemplate()
    {
        return 'Setting/Mastertable/Payment_method/edit.twig';
    }
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        return '/setting/mastertable/payment_method/overview';
    }
}
