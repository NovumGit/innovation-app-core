<?php
namespace AdminModules\Setting\Mastertable\Product_delivery_time;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Crud\Product_delivery_time\CrudProduct_delivery_timeManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        $oCrudInstanceManager = new CrudInstanceManager();
        return $oCrudInstanceManager->getOverviewUrl();
    }

    /**
     * @param $iTranslatedItemId
     *
     * @return ModelCriteria|string
     */
    function getTranslationModels($iTranslatedItemId): ModelCriteria
    {
        $oProductDeliveryTimeTranslationQuery = new ProductDeliveryTimeTranslationQuery();
        return $oProductDeliveryTimeTranslationQuery->filterByDeliveryTimeId($iTranslatedItemId);

    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['delivery_time_id'] = $iItemId;
        return $aPostedData;
    }
}
