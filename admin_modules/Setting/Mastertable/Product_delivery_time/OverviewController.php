<?php
namespace AdminModules\Setting\Mastertable\Product_delivery_time;

use Crud\Product_delivery_time\CrudProduct_delivery_timeManager as CrudInstanceManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\ProductDeliveryTimeQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
