<?php
namespace AdminModules\Setting\Mastertable\Country;

use Crud\Country\CrudCountryManager;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudCountryManager();
    }
    function getOverviewUrl()
    {
        return $this->getCrudManager()->getOverviewUrl();
    }
}
