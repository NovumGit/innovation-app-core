<?php
namespace AdminModules\Setting\Mastertable\Country;

use Crud\Country\CrudCountryManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\CountryQuery;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudCountryManager(), CountryQuery::create());
    }
}
