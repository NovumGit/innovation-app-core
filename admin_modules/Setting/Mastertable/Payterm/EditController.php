<?php
namespace AdminModules\Setting\Mastertable\Payterm;

use Core\StatusMessage;
use Crud\Payterm\CrudPaytermManager as CrudInstanceManager;

use Model\Crm\CustomerQuery;
use Model\Setting\MasterTable\PayTermQuery;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        return '/setting/mastertable/payterm/overview';
    }

    function doDelete()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oCustomerQuery = CustomerQuery::create()->filterByPaytermId($iId);
        $aCustomers = $oCustomerQuery->find();

        if(!$aCustomers->isEmpty()){
            StatusMessage::warning("Het item kon niet verwijderd worden omdat er nog een klant aan verbonden is!");
            $this->redirect($this->getOverviewUrl());
        }else{
            parent::doDelete();
        }
    }

}
