<?php
namespace AdminModules\Setting\Mastertable\Payterm;

use Crud\Payterm\CrudPaytermManager as CrudInstanceManager;

use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\PayTermQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
