<?php
namespace AdminModules\Setting\Mastertable\Usage;

use Crud\Usage\CrudUsageManager as CrudInstanceManager;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\UsageQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
