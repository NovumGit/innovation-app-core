<?php
namespace AdminModules\Setting\Mastertable\Usage;

use AdminModules\Setting\InterfaceTranslatableEditController;
use Crud\Usage\CrudUsageManager as CrudInstanceManager;
use AdminModules\Setting\EditController as ParentEditController;
use Model\Setting\MasterTable\UsageTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends ParentEditController implements InterfaceTranslatableEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        $oCrudInstanceManager = new CrudInstanceManager();
        return $oCrudInstanceManager->getOverviewUrl();
    }

    /**
     * @param $UsageId
     *
     * @return ModelCriteria|string
     */
    function getTranslationModels($UsageId): ModelCriteria
    {
        $oUsageTranslationQuery = new UsageTranslationQuery();
        return $oUsageTranslationQuery->filterByUsageId($UsageId);

    }
    function addUntranslatedItemId($aPostedData, $iItemId) : array
    {
        $aPostedData['usage_id'] = $iItemId;
        return $aPostedData;
    }
}
