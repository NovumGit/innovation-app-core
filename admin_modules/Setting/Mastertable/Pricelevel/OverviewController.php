<?php
namespace AdminModules\Setting\Mastertable\Pricelevel;

use Crud\Pricelevel\CrudPricelevelManager as CrudInstanceManager;

use Modules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\PriceLevelQuery as QueryModelObject;

class OverviewController extends ParentOverviewController
{

    function run()
    {
        return $this->runCrud(new CrudInstanceManager(), QueryModelObject::create());
    }
}
