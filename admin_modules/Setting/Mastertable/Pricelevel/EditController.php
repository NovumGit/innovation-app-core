<?php
namespace AdminModules\Setting\Mastertable\Pricelevel;

use Crud\Pricelevel\CrudPricelevelManager as CrudInstanceManager;

use Modules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        return '/setting/mastertable/price_level/overview';
    }

}
