<?php
namespace AdminModules\Setting;

use Exception\LogicException;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;

class TranslatableModel{
    private $oLanguage;
    private $oPropelModel;

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->oLanguage;
    }

    /**
     * @param mixed $oLanguage
     */
    public function setLanguage(Language $oLanguage)
    {
        $this->oLanguage = $oLanguage;
    }

    /**
     * @return mixed
     */
    public function getPropelModel()
    {
        return $this->oPropelModel;
    }

    /**
     * @param mixed $oPropelModel
     */
    public function setPropelModel($oPropelModel)
    {
        if(!$oPropelModel instanceof ActiveRecordInterface)
        {
            throw new LogicException("Expected an instance of ActiveRecordInterface got ".get_class($oPropelModel));
        }
        $this->oPropelModel = $oPropelModel;
    }
}