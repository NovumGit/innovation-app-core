<?php
namespace AdminModules\Setting\Product;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\Customer;
use Model\Product;
use Model\Setting\MasterTable\VatQuery;

class ConfigController extends MainController
{
    function doStore()
    {
        $aData = $this->post('data');

        if(isset($aData['customer_auto_product_numbering']) && $aData['customer_auto_product_numbering'] == '1')
        {
            $sCustomerNextDebitorNumber = trim($aData['customer_next_product_number']);
            if(!preg_match('/[0-9]+$/', $sCustomerNextDebitorNumber, $aParts))
            {
                $sCustomerNextDebitorNumber = $sCustomerNextDebitorNumber.'1';
            }
        }
        else if(!isset($aData['customer_auto_product_numbering']))
        {
            $sCustomerNextDebitorNumber = null;
        }
        else
        {
            throw new LogicException("The value for customer_auto_product_numbering isset but is not 1, which should never happen. This inidcates a bug.");
        }


        Setting::store('customer_auto_product_numbering', isset($aData['customer_auto_product_numbering']) ? 1 : 0);
        Setting::store('customer_next_product_number', $sCustomerNextDebitorNumber);
        Setting::store('product_auto_translate', $aData['product_auto_translate']);
        Setting::store('product_default_vat_level', $aData['product_default_vat_level']);
        


        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect($this->getRequestUri(false));
    }

    function doNoLoginGetNextDebitorNumber()
    {
        $sNextProductNumberWithPrefix = Product::generateNexProductNumber();
        echo $sNextProductNumberWithPrefix;
        exit();
    }

    function run()
    {
        $sTitle = 'Product instellingen';

        $aVatLevels = $aVatLevels = VatQuery::create()->find();
        $aMainViewData = [
            'title' => $sTitle,
            'vat_levels' => $aVatLevels,
            'data' => [
                'customer_auto_product_numbering' => Setting::get('customer_auto_product_numbering', 1),
                'customer_next_product_number' => Setting::get('customer_next_product_number', 1),
                'product_auto_translate' => Setting::get('product_auto_translate', 0),
                'product_default_vat_level' =>  Setting::get('product_default_vat_level', null),
            ]
        ];
        return [
            'title' => Translate::fromCode($sTitle),
            'content' => $this->parse('Setting/Product/config.twig', $aMainViewData)
        ];
    }
}
