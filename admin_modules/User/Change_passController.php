<?php
namespace AdminModules\User;


use Core\StatusMessage;
use Core\Translate;
use Core\User;

class Change_passController extends \Core\MainController
{
    function doChangePass()
    {
        $sNewPassword =  $this->post('new_password');
        $sNewPasswordVerify =  $this->post('new_password_verify');

        if($sNewPassword != $sNewPasswordVerify)
        {
            StatusMessage::warning("De nieuwe wachtwwoorden komen niet overeen");
            $this->redirect('/user/change_pass');
        }
        else if(strlen($sNewPassword) < 8)
        {
            StatusMessage::warning("Het nieuwe wachtwoord moet minimaal 8 tekens lang zijn");
            $this->redirect('/user/change_pass');
        }

        $sSalt = User::makeSalt();
        $sEncryptedPass = User::makeEncryptedPass($sNewPassword, $sSalt);

        $oUser = User::getMember();
        $oUser->setPassword($sEncryptedPass);
        $oUser->setSalt($sSalt);
        $oUser->setMustChangePassword(false);
        $oUser->save();
        StatusMessage::info('Wachtwoord gewijzigd');
        $this->redirect('/');
    }

    function run()
    {
        $aTopNav = [];
        $aData = [];

        return [
            'title' => Translate::fromCode("Wachtwoord wijzigen"),
            'top_nav' =>  $this->parse('empty_top_nav.twig', $aTopNav),
            'content' => $this->parse('User/change_password.twig', $aData)
        ];
    }

}
