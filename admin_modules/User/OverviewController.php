<?php
namespace AdminModules\User;

use Core\MainController;
use Crud\CrudViewManager;
use Crud\IConfigurableCrud;
use Crud\User\CrudUserManager;
use Exception\LogicException;
use Helper\FilterHelper;
use Model\Setting\CrudManager\CrudView;
use Model\Account\UserQuery;
use Model\Setting\MasterTable\RoleQuery;

class OverviewController extends MainController{

    function run(){
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oUserQuery = UserQuery::create();
        $oUserQuery->orderBy($sSortField, $sSortDirection);

        $oRoleQuery = RoleQuery::create();
        $aViewData['roles'] = $oRoleQuery->find();
        $oCrudUserManager = new CrudUserManager();
        $aCrudViews = CrudViewManager::getViews($oCrudUserManager);

        if(!$iTabId)
        {
            $oCrudView = current($aCrudViews);

            if(!$oCrudView instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudView));
            }
            $iTabId = $oCrudView->getId();
        }
        else
        {
            $oCrudView = $aCrudViews[$iTabId];
        }

        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oUserQuantityQuery = UserQuery::create();
                $oUserQuantityQuery = FilterHelper::applyFilters($oUserQuantityQuery, $o->getId());
                $o->_sz = $oUserQuantityQuery->count();
            }
        }


        $oUserQuery = FilterHelper::applyFilters($oUserQuery, $iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $oUserQuery = FilterHelper::generateVisibleFilters($oUserQuery, $aFilters);

        $aUsers = $oUserQuery->find();

        $oCrudUserManager->setOverviewData($aUsers);
        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);

        $aFields = CrudViewManager::getFields($oCrudView);
        $aViewData['overview_table'] = $oCrudUserManager->getOverviewHtml($aFields);

        $aTopNavVars =
        [
            'new_url' => $oCrudUserManager->getCreateNewUrl(),
            'new_title' => $oCrudUserManager->getNewFormTitle(),
            'implements_configurable_crud' => ($oCrudUserManager instanceof IConfigurableCrud),
            'module' => 'User',
            'crud_views' => $aCrudViews,
            'manager' => 'UserManager',
            'tab_id' => $iTabId
        ];

        $sParseResult = $this->parse('generic_overview.twig', $aViewData);
        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);

        $aView =
        [
            'title' => 'Gebruikers',
            'top_nav' => $sTopNav,
            'content' => $sParseResult
        ];
        return $aView;

    }
}
