<?php
namespace AdminModules\User;

use Core\Config;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\UserRegistry;
use Core\Utils;
use LinkedIn\LinkedIn;
use core\User;
use Model\Crm\UserLinkedIn;
use Model\Crm\UserLinkedInQuery;
use Propel\Runtime\Exception\LogicException;
use Model\User\UserActivityQuery;

class About_meController extends MainController
{
    private $olinkedIn;

    function __construct($aGet, $aPost)
    {
        $sProtocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';

        $this->olinkedIn = new LinkedIn(
            [
                'api_key' => '78cm4tipnnexq3',
                'api_secret' => 'A47qL4tcCHoFl03f',
                'callback_url' => $sProtocol.'://'.$_SERVER['HTTP_HOST'].'/user/about_me?_do=HandleLinkedInResponse'
            ]
        );
        parent::__construct($aGet, $aPost);
    }
    function run()
    {
        $oUser = User::getMember();
        $oUserLinkedIn = UserLinkedInQuery::create()->findOneByUserId($oUser->getId());
        $sSelectedTab = strtolower($this->get('tab', 'home'));
        $aAllowedTabs = [
            'home' => Translate::fromCode('Over mij'),
            'activity' => Translate::fromCode('Mijn activiteiten'),
            'change' => Translate::fromCode('Wijzigen')
        ];

        if(!in_array($sSelectedTab, array_keys($aAllowedTabs)))
        {
            throw new LogicException("De gekozen tab bestaat (nog) niet, kies uit: ".join(",", array_keys($aAllowedTabs)));
        }


        $aTwigData =
            [
                'current_tab_'.$sSelectedTab => 'active',
                'linked_in_user' => $oUserLinkedIn,
                'linked_in_connected' => ($oUserLinkedIn instanceof UserLinkedIn),
                'answered_never_copy_linked_in_photo' => UserRegistry::get('never_copy_linked_in_photo')
            ];

        if($sSelectedTab == 'activity')
        {
            $oUserActivityQuery = UserActivityQuery::create();
            $oUserActivityQuery->orderByActivityDone('DESC');
            $oUserActivityQuery->findByUserId($oUser->getId());
            $oUserActivityQuery->limit(200);
            $aLatestActivities = $oUserActivityQuery->find();

            $aTwigData['activities'] = $aLatestActivities;
        }


        $aView = [
            'content' => $this->parse('User/about_me_'.$sSelectedTab.'.twig', $aTwigData),
            'title' => $aAllowedTabs[$sSelectedTab]
        ];
        return $aView;
    }
    function doSave()
    {

        $aData = $this->post('data', null);

        $bValidationErrors = false;

        if(!empty($aData))
        {
            $oUser = User::getMember();
            $aSupportedMimes = Utils::getSupportedImageMimeTypes();
            if($_FILES['profile_pic'] && $_FILES['profile_pic']['error'] == 0 && in_array($_FILES['profile_pic']['type'], $aSupportedMimes))
            {
                $sExt = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
                $oUser->setPicExt($sExt);
                $oUser->setHasProfilePic(true);


                if(!is_dir(Config::getDataDir()."/img/user/{$oUser->getId()}"))
                {
                    mkdir(Config::getDataDir()."/img/user/{$oUser->getId()}", 0777, true);
                }

                move_uploaded_file( $_FILES['profile_pic']['tmp_name'], Config::getDataDir()."/img/user/{$oUser->getId()}/profile_pic.jpg");
            }

            $oUser->setPhone($aData['phone']);
            $oUser->setPhoneMobile($aData['phone_mobile']);
            $oUser->setWebsite($aData['website']);
            $oUser->setTwitter($aData['twitter']);
            $oUser->setAboutMe($aData['about_me']);

            $oUser->save();
            StatusMessage::success("Wijzigingen opgeslagen");
            if(!$bValidationErrors)
            {
                $this->redirect('/user/about_me');
            }
            else
            {
                $this->redirect('/user/about_me?tab=Change');
            }
        }
    }
    function doCopyLinkedInPhoto()
    {
        $oUser = User::getMember();
        $oUserLinkedIn = UserLinkedInQuery::create()->findOneByUserId($oUser->getId());
        UserRegistry::register('never_copy_linked_in_photo', true);

        $sProfilePicUrl = $oUserLinkedIn->getPictureUrl();

        if(empty($sProfilePicUrl))
        {
            StatusMessage::warning("We probeerden je LinkedIn profielfoto over te nemen maar blijbaar heb je die helemaal niet want we kregen een lege waarde door van LinkedIn. Sorry!");
            return;
        }

        $oUser->setPicExt('jpg');
        $oUser->setHasProfilePic(true);
        $oUser->save();
        $sPicData = file_get_contents($sProfilePicUrl);

        $sProfilePicDir = Config::getDataDir()."/img/user/{$oUser->getId()}";

        if(!is_dir($sProfilePicDir))
        {
            mkdir($sProfilePicDir, 0777, true);
            chmod($sProfilePicDir, 0777);
        }
        file_put_contents("$sProfilePicDir/profile_pic.jpg", $sPicData);

        StatusMessage::success("Profielfoto van LinkedIn overgenomen.");
        $this->redirect('/user/about_me');

    }

    function doNeverCopyLinkedInPhoto()
    {
        UserRegistry::register('never_copy_linked_in_photo', true);
        $this->redirect('/user/about_me');
    }
    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doHandleLinkedInResponse()
    {
        $oUser = User::getMember();
        $sCode = $this->get('code');
        if($sCode)
        {
            $oUserLinkedIn = UserLinkedInQuery::create()->findOneByUserId($oUser->getId());

            if(!$oUserLinkedIn instanceof UserLinkedIn)
            {
                $oUserLinkedIn = new UserLinkedIn();
            }

            $oUserLinkedIn->setUserId($oUser->getId());

            $sToken = $this->olinkedIn->getAccessToken($_GET['code']);
            $oUserLinkedIn->setToken($sToken);

            $sTokenExpires = $this->olinkedIn->getAccessTokenExpiration();
            $oUserLinkedIn->setTokenExpires($sTokenExpires);
            $oUserLinkedIn->save();
            $this->redirect('/user/about_me?_do=RefreshLinkedInData');
        }
        else
        {
            StatusMessage::warning("Het is helaas niet gelukt om je profiel te koppelen aan LinkedIn, sorry! Je zou het nog eens kunnen proberen...");
            $this->redirect('/user/about_me');
        }
    }

    function doRefreshLinkedInData()
    {
        $oUser = User::getMember();
        $oUserLinkedIn = UserLinkedInQuery::create()->findOneByUserId($oUser->getId());

        if($oUserLinkedIn instanceof UserLinkedIn)
        {

            $this->olinkedIn->setAccessToken($oUserLinkedIn->getToken());

            $aResponse = $this->olinkedIn->get('/people/~:(first-name,last-name,picture-url,picture-urls::(original),email-address,headline,public-profile-url)');

            if(isset($aResponse['emailAddress']))
            {
                $oUserLinkedIn->setEmail($aResponse['emailAddress']);
            }
            if(isset($aResponse['headline']))
            {
                $oUserLinkedIn->setHeadline($aResponse['headline']);
            }
            if(isset($aResponse['pictureUrls']) && $aResponse['pictureUrls']['values'])
            {
                $oUserLinkedIn->setPictureUrl(current($aResponse['pictureUrls']['values']));
            }
            if(isset($aResponse['publicProfileUrl']))
            {
                $oUserLinkedIn->setPublicProfileUrl($aResponse['publicProfileUrl']);
            }
            $oUserLinkedIn->save();
            StatusMessage::success("Linked in is gekoppeld en we hebben wat basis info van je overgenomen, bedankt! ");

            $this->redirect('/user/about_me');
        }
        else
        {
            StatusMessage::warning("We wilden wat info over je ophalen uit LinkedIn maar dat is helaas niet gelukt en we hadden eerlijk gezegd geen idee waarom. Profiel verwijderd misschien?");
            $this->redirect('/user/about_me');
        }

    }

    function doHookupLinkedIn()
    {
        $sAuthenticationUrl = $this->olinkedIn->getLoginUrl([

            LinkedIn::SCOPE_BASIC_PROFILE,
            LinkedIn::SCOPE_EMAIL_ADDRESS

        ]);
        $this->redirect($sAuthenticationUrl);
    }
}
