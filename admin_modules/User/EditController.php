<?php
namespace AdminModules\User;

use Core\MainController;
use Core\StatusMessage;
use Crud\User\CrudUserManager;

class EditController extends MainController
{
    const sFormLayoutKey = 'user_editor';

    private $oCrudUserManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->oCrudUserManager = new CrudUserManager(self::sFormLayoutKey);
    }
    function run()
    {
        $aUser = $this->post('data', null);
        $aUser['id'] = $this->get('id');

        $oUser = $this->oCrudUserManager->getModel($aUser);
        $aParseData['edit_form'] = $this->oCrudUserManager->getRenderedEditHtml($oUser, self::sFormLayoutKey);
        $aTopnavEdit = [
            'module' => 'User',
            'manager' => 'UserManager',
            'overview_url' => $this->oCrudUserManager->getOverviewUrl(),
            'edit_config_key' => self::sFormLayoutKey,
            'name' => self::sFormLayoutKey,
            'title' => 'Gebruiker instellen'
        ];
        $aView['top_nav'] = $this->parse('top_nav_edit.twig', $aTopnavEdit);
        $aView['content'] = $this->parse('edit.twig', $aParseData);
        $aView['title'] = 'Gebruiker '.($aUser['id'] ? 'bewerken' : 'toevoegen');

        return $aView;
    }
    function doDelete()
    {
        $sTab = $this->get('tab');
        $iId = $this->get('id');
        StatusMessage::Warning("Gebruiker als verwijderd gemarkeerd.");
        $oUser = $this->oCrudUserManager->getModel(['id' => $iId]);
        $oUser->setItemDeleted(1);
        $oUser->save();

        $sAddUrl = '';
        if($sTab)
        {
            $sAddUrl = '?tab='.$sTab;
        }
        $this->redirect('/user/overview'.$sAddUrl);
    }
    function doUndelete()
    {
        $sTab = $this->get('tab');
        $iId = $this->get('id');
        StatusMessage::success("Gebruiker uit de prullenbak gehaald. (-:");

        $oUser = $this->oCrudUserManager->getModel(['id' => $iId]);
        $oUser->setItemDeleted(0);
        $oUser->save();

        $sAddUrl = '';
        if($sTab)
        {
            $sAddUrl = '?tab='.$sTab;
        }

        $this->redirect('/user/overview'.$sAddUrl);
    }

    function doStore()
    {
        $aUser = $this->post('data', null);

        if($this->oCrudUserManager->isValid($aUser, self::sFormLayoutKey))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $oUser = $this->oCrudUserManager->save($aUser);
            $this->redirect('/user/edit?id='.$oUser->getId());
        }
        else
        {
            $this->oCrudUserManager->registerValidationErrorStatusMessages($aUser, self::sFormLayoutKey);
        }
    }
}
