<?php
namespace AdminModules\Sale\Invoice;

use AdminModules\GenericOverviewController;
use Crud\FormManager;
use Crud\Sale_invoice\CrudSale_invoiceManager;
use Model\Finance\SaleInvoiceQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    function __construct($aGet, $aPost)
    {
        $this->setEnablePaginate(50);
        parent::__construct($aGet, $aPost);
    }

    function getModule(): string
    {
        return 'Sale_invoice';
    }

    function getManager(): FormManager
    {
        return new CrudSale_invoiceManager();
    }

    function getQueryObject(): ModelCriteria
    {
        return SaleInvoiceQuery::create();
    }
    function getTitle(): string
    {
        return $this->getManager()->getEntityTitle();
    }
}