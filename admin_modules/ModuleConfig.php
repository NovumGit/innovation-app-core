<?php
namespace AdminModules;

abstract class ModuleConfig implements IModuleConfig {

    protected $iDatabaseId;
    function setDatabaseId($iId)
    {
        $this->iDatabaseId = $iId;
    }
    function getDatabaseId()
    {
        return $this->iDatabaseId;
    }
    function getMenuExtraVars(): array
    {
        return [];
    }

    /*
     * Bepaald of dit menu item zichtbaar is in het modules instellen venster.
     */
    abstract function isEnabelable(): bool;

    /**
     * @return string
     */
    abstract function getModuleTitle(): string;


}
