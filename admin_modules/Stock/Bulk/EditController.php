<?php
namespace AdminModules\Stock\Bulk;

use AdminModules\GenericEditController;
use Core\StatusMessage;
use Core\Translate;
use Crud\BulkStock\CrudBulkStockManager;
use Crud\FormManager;
use Model\BulkStock;
use Model\BulkStockQuery;


class EditController extends GenericEditController  {

    function doDelete()
    {
        $iId = $this->get('id', null, true, 'numeric');
        $oBulkStock = BulkStockQuery::create()->findOneById($iId);
        if($oBulkStock instanceof BulkStock)
        {
            $oBulkStock->delete();
        }
        StatusMessage::success("Bulk stock item verwijderd.");
        $this->redirect('/stock/bulk/overview');
    }
    function getPageTitle()
    {
        return Translate::fromCode('Bulk voorraad wijzigen');
    }
    function getCrudManager():FormManager
    {
        $aBulkStock = $this->post('data', null);
        return new CrudBulkStockManager($aBulkStock);
    }
}
