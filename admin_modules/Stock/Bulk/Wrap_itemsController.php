<?php
namespace AdminModules\Stock\Bulk;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Model\BulkStockQuery;
use Model\CustomerQuery;

class Wrap_itemsController extends MainController {


    function doWrapStock()
    {
        $iBulkStockId = $this->get('bulk_stock_id', null, true, 'numeric');
        $iProductId = $this->post('product_id', null, true, 'numeric');
        $iPacks = $this->post('packs', null, true, 'numeric');
        $oProduct = CustomerQuery::create()->findOneById($iProductId);

        $iTotalQuantity = $oProduct->getQuantityPerPack() * $iPacks;

        $oBulkStock = BulkStockQuery::create()->findOneById($iBulkStockId);
        $oBulkStock->setQuantity($oBulkStock->getQuantity() - $iTotalQuantity);
        $oBulkStock->save();

        $oProduct->setStockQuantity($oProduct->getStockQuantity() + $iPacks);
        $oProduct->save();
        $this->redirect("/stock/bulk/wrap_items?bulk_stock_id=$iBulkStockId");
    }
    function doUnwrapStock()
    {
        $iBulkStockId = $this->get('bulk_stock_id', null, true, 'numeric');
        $iProductId = $this->post('product_id', null, true, 'numeric');
        $iPacks = $this->post('packs', null, true, 'numeric');
        $oProduct = CustomerQuery::create()->findOneById($iProductId);

        $iTotalQuantity = $oProduct->getQuantityPerPack() * $iPacks;

        $oBulkStock = BulkStockQuery::create()->findOneById($iBulkStockId);
        $oBulkStock->setQuantity($oBulkStock->getQuantity() + $iTotalQuantity);
        $oBulkStock->save();

        $oProduct->setStockQuantity($oProduct->getStockQuantity() - $iPacks);
        $oProduct->save();
        $this->redirect("/stock/bulk/wrap_items?bulk_stock_id=$iBulkStockId");
    }
    function run()
    {
        DeferredAction::register('bulk_stock', $this->getRequestUri());

        $iBulkStockId = $this->get('bulk_stock_id', null, true, 'numeric');
        $sPageTitle = Translate::fromCode('Producten inpakken');

        $sTopNavHtml = $this->parse('Stock/Bulk/top_nav.twig', []);

        $aMainWindowData = [
            'products' => CustomerQuery::create()->findByBulkStockId($iBulkStockId),
            'bulk_stock' => BulkStockQuery::create()->findOneById($iBulkStockId)
        ];
        $sMainWindowHtml = $this->parse('Stock/Bulk/wrap_items.twig', $aMainWindowData);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];
        return $aView;
    }
}
