<?php
namespace AdminModules\Stock\Bulk;

use Crud\BulkStock\CrudBulkStockManager;
use Crud\FormManager;
use Model\BulkStockQuery;

use AdminModules\GenericOverviewController;
use Model\Setting\CrudManager\CrudView;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController {

    /**
     * @return string
     */
    function getTitle():string
    {
        return 'Bulk voorraad producten';
    }

    function getManager():FormManager
    {
        return new CrudBulkStockManager();
    }
    function getQueryObject():ModelCriteria
    {

        return BulkStockQuery::create();
    }

    function getModule():string
    {
        return 'BulkStock';
    }

    function getDefaultSortingColumn(CrudView $oCrudView = null):string
    {
        return 'name';
    }
}
