<?php
namespace AdminModules\Stock\Bulk;

use Core\MainController;
use Core\Translate;
use Core\Utils;
use Model\Product;
use Model\CustomerQuery;

class ApiController extends MainController
{
    private function sendNotification($sProductNumber, $iQuantity, $aMessages)
    {
        $aHeaders = [];
        $aHeaders[] = 'From: system@wholesale.femaleseeds.nl';
        $aHeaders[] = "MIME-Version: 1.0";;
        $aHeaders[] = 'Content-type: text/html; charset=UTF-8';
        $aReportHtml[] = "<strong>There was an incomming API call to lower the stock for product $sProductNumber with $iQuantity items.</strong>";
        $aReportHtml[] = 'Url called: '.$this->getRequestUri();
        $aReportHtml[] = 'The response was: ';
        foreach($aMessages as $sKey => $mMessage)
        {
            $aReportHtml[] = $mMessage;
        }
        if(!isset($_SERVER['IS_DEVEL']))
        {
            mail('gennaro@femaleseeds.nl', 'Wholesale stock mutation report', join('<br>', $aReportHtml), join("\r\n", $aHeaders));
        }
        mail('anton@nui-boutkam.nl', 'Wholesale stock mutation report', join('<br>', $aReportHtml), join("\r\n", $aHeaders));
    }
    function doNoLoginLowerStock()
    {
        $sProductNumber = $this->get('product_number', null);
        $iQuantity = $this->get('quantity', null);

        $aErrors = [];
        if(!$sProductNumber)
        {
            $aErrors['no_product_number'] = Translate::fromCode("Geef alstublieft een productnummer op.");
        }
        if(!$iQuantity)
        {
            $aErrors['no_quantity'] = Translate::fromCode("Geef alstublieft een aantal op.");
        }
        if(!empty($aErrors))
        {
            $this->sendNotification($sProductNumber, $iQuantity, $aErrors);
            Utils::jsonExit($aErrors);
        }

        $oProduct = CustomerQuery::create()->findOneByNumber($sProductNumber);
        if($oProduct instanceof Product)
        {
            $iCurrentQuantity = $oProduct->getStockQuantity();
            $iNewQuantity = $iCurrentQuantity - $iQuantity;
            $oProduct->setStockQuantity($iNewQuantity);
            $this->sendNotification($sProductNumber, $iQuantity, ['Stock changed']);
            Utils::jsonOk();
        }
        $aErrors['product_not_found'] = Translate::fromCode("Artikel niet gevonden.");
        $this->sendNotification($sProductNumber, $iQuantity, $aErrors);
        Utils::jsonExit($aErrors);
        exit();
    }
    function doNoLoginGetProductStock()
    {
        $sProductNumber = $this->get('product_number', null, true);
        $oProductQuery = CustomerQuery::create();
        $oProduct = $oProductQuery->findOneByNumber($sProductNumber);

        $iStockQuantity = 0;
        if($oProduct instanceof Product)
        {
            $iStockQuantity = $oProduct->getStockQuantity();
        }
        Utils::jsonExit(['stock_quantity' => $iStockQuantity]);
    }
    function doNoLoginGetProducts()
    {
        $oProductQuery =  CustomerQuery::create();
        $oProductQuery->where('derrived_from_id IS NOT NULL');
        $aProducts = $oProductQuery->find();
        $aOut = [];
        foreach($aProducts as $oProduct)
        {
            if($oProduct->getNumber() != '')
            {
                $aOut[] = [
                    'id' => $oProduct->getNumber(),
                    'name' => $oProduct->getNumber()
                ];
            }
        }
        Utils::jsonExit($aOut);
    }
    function run(){ exit('No intended to be used like this.');}
}