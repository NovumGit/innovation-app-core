
$('.substract_stock').click(function(e)
{
    $('#fld_do').val('UnwrapStock');
    $('#fld_product_id').val($(this).data('product_id'));
    $('#fld_packs').val($(this).data('packs'));
    $('#add_substract_form').submit();

    e.preventDefault();
});


$('.add_stock').click(function(e)
{
    $('#fld_do').val('WrapStock');
    $('#fld_product_id').val($(this).data('product_id'));
    $('#fld_packs').val($(this).data('packs'));
    $('#add_substract_form').submit();

    e.preventDefault();
});
