<?php
namespace AdminModules\Stock;

use AdminModules\ModuleConfig;
use Core\Translate;

class Config extends ModuleConfig
{
    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return  Translate::fromCode('Voorraadbeheer');
    }

}
