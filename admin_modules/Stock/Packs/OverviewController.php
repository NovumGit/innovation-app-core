<?php
namespace AdminModules\Stock\Packs;

use Core\MainController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Product\CrudProductManager;
use Helper\FilterHelper;
use Model\CustomerQuery;

class OverviewController extends MainController{

    function run()
    {
        $sTitle = Translate::fromCode('Voorraad overzicht');

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'Number';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oProductQuery = CustomerQuery::create();
        $oProductQuery->where('derrived_from_id IS NOT NULL');
        $oProductQuery->orderBy($sSortField, $sSortDirection);

        $oCrudProductManager = new CrudProductManager();

        $aFields = ['TranslatedTitle', 'Number', 'QuantityPerPack', 'StockQuantity'];
        $iTabId = CrudViewManager::createOrEditView($oCrudProductManager, 'Voorraad', $aFields);

        $oCrudview = CrudViewManager::getView($oCrudProductManager, $iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oProductQuery = FilterHelper::applyFilters($oProductQuery, $iTabId);
        $oProductQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oProductQuery, $aFilters);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aProducts = $oProductQuery->find();
        $oCrudProductManager->setOverviewData($aProducts);

        $aViewData['overview_table'] = $oCrudProductManager->getOverviewHtml($aFields);

        /*
        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'category_id' => $iCategoryId,
            'tab_id' => $iTabId,
            'module' => 'Product',
            'title' => $sTitle,
            'manager' => 'ProductManager',
            'overview_url' => $oCrudProductManager->getOverviewUrl(),
            'new_url' => $oCrudProductManager->getCreateNewUrl(),
            'new_title' => $oCrudProductManager->getNewFormTitle(),
            'all_categories' => $aCategories,
            'selected_category' => CategoryQuery::create()->findOneById($iCategoryId),
        ];
        */

//        exit(var_dump($aCategories));
/*
        $sTopNav = $this->parse('Product/top_nav_overview_custom.twig', $aTopNavVars);
  */
        $sTopNav = '';
        $sOverview = $this->parse('generic_overview.twig', $aViewData);

        $aView =
            [
                'top_nav' => $sTopNav,
                'content' => $sOverview,
                'title' => $sTitle
            ];

        return $aView;

    }
}