<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\VoidMime;
use Core\Setting;
use Core\Translate;
use Helper\SaleOrderHelper;
use Model\Company\CompanyQuery;
use Model\Sale\SaleOrderQuery;

class Generate_invoice_number extends AbstractEvent  {

    function getDescription(): string
    {
        return Translate::fromCode("Genereer factuurnummer");
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function trigger(string $sEnvironment): void
    {

        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrder = $oSaleOrderQuery->findOneById($iOrderId);
        $sInvoiceNumber = $oSaleOrder->getInvoiceNumber();

        if(empty($sInvoiceNumber) || $sInvoiceNumber == 'proforma')
        {
            $oCompany = CompanyQuery::create()->findOneByIsDefaultSupplier(1);
            $oSaleOrder = SaleOrderHelper::generateInvoiceNumber($oCompany, $oSaleOrder);
            $oSaleOrder->save();
        }
    }
}

