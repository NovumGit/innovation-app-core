<?php
namespace AdminModules\Order\Event;

use Core\DeferredAction;
use Core\Environment;
use Core\InlineTemplate;
use Core\Logger;
use Core\Mime\VoidMime;
use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\QueryMapper;
use Core\StatusMessage;
use Core\Translate;
use Crud\Field;
use Crud\IDisplayableField;
use Crud\Product\CrudProductManager;
use Crud\Sale_order_item\CrudSale_order_itemManager;
use Model\Product;
use Model\Sale\SaleOrderItemQuery;
use Model\System\SystemRegistryQuery;

class CallUrlPerOrderItem extends AbstractEvent implements AbstractConfigurableEvent {

    function getDescription(): string
    {
        return Translate::fromCode("Roep voor ieder product in de bestelling een url aan.");
    }
    function hasOutput()
    {
        return false;
    }
    function isConfigurable(): bool
    {
        return true;
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        DeferredAction::register('call_per_order_item_return_url', $this->getRequestUri());
        $aProductPropertyFields = QueryMapper::fetchArray('SELECT field FROM product_property GROUP BY field');

        // Velden toevoegen? Zorg dat ze IDisplayableField implementeren.
        $filter = function(Field $oField){
            return $oField instanceof IDisplayableField;
        };

        $aAllProductFields = (new CrudProductManager())->getFieldIterator($filter);
        $aAllSaleOrderItemFields = (new CrudSale_order_itemManager())->getFieldIterator($filter);

        $sUrlTobeCalled = SystemRegistryQuery::create()->getVal('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.call_url');
        $sDevUrlTobeCalled = SystemRegistryQuery::create()->getVal('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.call_url_dev');
        $bLogCals = SystemRegistryQuery::create()->getVal('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.log_calls');

        $aViewVariables = [
            'all_product_fields' => $aAllProductFields,
            'all_sale_order_item_fields' => $aAllSaleOrderItemFields,
            'all_product_property_fields' => $aProductPropertyFields,
            'event_id' => $iCrudEditorButtonEventId,
            'data' => [
                'call_url' => $sUrlTobeCalled,
                'call_url_dev' => $sDevUrlTobeCalled,
                'log_calls' =>  $bLogCals
            ]
        ];
        return $this->parse('Generic/Event/call_url_per_product_event.twig', $aViewVariables);
    }
    function saveConfiguration($sEnvironment)
    {

        $iCrudEditorButtonEventId = $this->get('event_id');

        $aData = $this->post('data');

        SystemRegistryQuery::create()->createOrOverwrite('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.call_url', $aData['call_url']);
        SystemRegistryQuery::create()->createOrOverwrite('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.call_url_dev', $aData['call_url_dev']);
        SystemRegistryQuery::create()->createOrOverwrite('event.sale_order.'.$sEnvironment.'.'.$iCrudEditorButtonEventId.'.log_calls', $aData['log_calls']);

        StatusMessage::success(Translate::fromCode('Wijzigingen opgeslagen.'));
        $this->redirect(DeferredAction::get('call_per_order_item_return_url'));
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {

        $iOrderId = $this->get('order_id');
        $aSaleOrderItems = SaleOrderItemQuery::create()->findBySaleOrderId($iOrderId);
        $aResult = [];
        $aReportHtml = [];
        foreach($aSaleOrderItems as $oSaleOrderItem)
        {
            $oProduct = $oSaleOrderItem->getFirstProduct();
            if(!$oProduct instanceof Product)
            {
                continue;
            }

            $sUrl = SystemRegistryQuery::create()->getNewOrEmpty('event.sale_order.'.$sEnvironment.'.'.$this->getButtonEventId().'.call_url');

            if(Environment::isDevel())
            {
                $sUrl = SystemRegistryQuery::create()->getNewOrEmpty('event.sale_order.'.$sEnvironment.'.'.$this->getButtonEventId().'.call_url_dev');
            }


            $sUrl .= '{{Product_property.blabla}}';
            $sUrl = preg_replace(['/{{\s/', '/\s}}/'], ['{{', '}}'], $sUrl);

            preg_match_all('/{{Product_property.([a-zA-Z0-9_-]+)}}/', $sUrl, $aMatches);

            if(isset($aMatches[1]) && !empty($aMatches[1]))
            {
                foreach($aMatches[1] as $iId => $sPropertyKey)
                {
                    $sValue = $oProduct->getPropertyByKey($sPropertyKey);
                    $sUrl = str_replace($aMatches[0][$iId], urlencode($sValue), $sUrl);
                }
            }

            $bLogCals = SystemRegistryQuery::create()->getNewOrEmpty('event.sale_order.'.$sEnvironment.'.'.$this->getButtonEventId().'.log_calls');

            if($bLogCals === '1')
            {
                Logger::info('Called event url '.$sUrl);
            }

            $sUrl = InlineTemplate::parse($sUrl, ['Product' => $oProduct, 'Sale_order_item' => $oSaleOrderItem]);

            $aPostFields = []; // in the future perhaps add array('field1'=>'value1', 'field2'=>'value2');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            // Edit: prior variable $postFields should be $postfields;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $aPostFields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $sResult = curl_exec($ch);

            // @todo, dit later weghalen of alleen voor femaleseeds laten gelden

            if($bLogCals === '1')
            {
                Logger::info('Event url reply ' . $sResult);
            }
        }
    }
}

