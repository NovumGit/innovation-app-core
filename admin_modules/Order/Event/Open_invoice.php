<?php
namespace AdminModules\Order\Event;

use AdminModules\Order\Invoice\InvoiceController;
use Core\AbstractEvent;
use Core\Mime\PdfMime;
use Core\Translate;

class Open_invoice extends AbstractEvent  {


    function getDescription(): string
    {
        return Translate::fromCode("Open de factuur");
    }
    function isConfigurable(): bool
    {
        return false;
    }


    function outputType(): \core\Mime\Mime
    {
        return new PdfMime();
    }


    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id', null, true);

        $aGet = ['order_id' => $iOrderId, 'format' => 'pdf_return'];
        $oInvoiceController = new InvoiceController($aGet, []);
        return $oInvoiceController->run();
    }
}

