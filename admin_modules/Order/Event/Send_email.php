<?php
namespace AdminModules\Order\Event;

use Core\InlineTemplate;
use Core\Mailer;
use Core\MailMessage;
use Core\Mime\VoidMime;
use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\Utils;
use Crud\Field;
use Crud\Sale_order\CrudSale_orderManager;
use Crud\Sale_order_item\CrudSale_order_itemManager;
use Crud\FormManager;
use Document\Invoice\Invoice;
use Document\PackingSlip\PackingSlip;
use Exception\LogicException;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderEmail;
use Model\Sale\SaleOrderQuery;
use Model\System\SystemRegistryQuery;

class Send_email extends AbstractEvent implements AbstractConfigurableEvent {

    function hasOutput()
    {
        return true;
    }
    function isConfigurable(): bool
    {
        return true;
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    private function getCrudFields(FormManager $oCrudManager, $sPrefix)
    {
        $aAllFields = $oCrudManager->getAllAvailableFields();
        $aAllCrudFields = $oCrudManager->loadCrudFields($aAllFields);
        $aUsableVariables = [];
        foreach($aAllCrudFields as $oCrudField)
        {
            $oClass = new \ReflectionClass($oCrudField);
            if($oCrudField instanceof Field)
            {
                $aUsableVariables[] = [
                    'title' => $oCrudField->getFieldTitle(),
                    'getter' => '{{'.$sPrefix.'.get'.$oClass->getShortName().'}}'
                ];
            }
        }
        Utils::usort_strcmp($aUsableVariables, 'title');
        return $aUsableVariables;
    }
    static function generateTxt($sType, $iOrderId, $iCrudEditorButtonEventId)
    {
        if(!in_array($sType, ['subject' , 'message', 'from', 'to', 'bcc']))
        {
            throw new LogicException("Verkeerd bericht type, alleen subject en message worden momenteel ondesteund.");
        }
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $aViewData = [];
        $aViewData['order'] = $oSaleOrder;

        $sMessage = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEventId.'_crud_'.$sType);

        $sTemplate = InlineTemplate::parse($sMessage, $aViewData);

        return $sTemplate;
    }
    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        //$iId = $this->get('crud_editor_button_event_id', null, true);
        $aViewVariables = [];
        $aViewVariables['usable_order_fields'] = $this->getCrudFields(new CrudSale_orderManager(), 'order');
        $aViewVariables['usable_order_item_fields'] = $this->getCrudFields(new CrudSale_order_itemManager(), 'order_item');
        $iCrudEditorButtonEvent = $this->get('crud_editor_button_event_id');

        $aViewVariables['subject'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_subject');
        $aViewVariables['message'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_message');
        $aViewVariables['from'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_from');
        $aViewVariables['to'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_to');
        $aViewVariables['bcc'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_bcc');

        $aViewVariables['attachment_invoice'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_attachment_invoice');
        $aViewVariables['attachment_packingslip'] = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_attachment_packingslip');

        $oSaleOrder = SaleOrderQuery::getOneRandom();

        if(!$oSaleOrder instanceof SaleOrder)
        {
            throw new LogicException("Expected an instance of SaleOrder but got ".get_class($oSaleOrder));
        }

        $sPreviewSubject = self::generateTxt('subject', $oSaleOrder->getId(), $iCrudEditorButtonEvent);
        $sPreviewMessage = self::generateTxt('message', $oSaleOrder->getId(), $iCrudEditorButtonEvent);
        $sPreviewFrom = self::generateTxt('from', $oSaleOrder->getId(), $iCrudEditorButtonEvent);
        $sPreviewTo = self::generateTxt('to', $oSaleOrder->getId(), $iCrudEditorButtonEvent);
        $sPreviewBcc = self::generateTxt('bcc', $oSaleOrder->getId(), $iCrudEditorButtonEvent);

        $aViewVariables['from_preview'] = $sPreviewFrom;
        $aViewVariables['to_preview'] = $sPreviewTo;
        $aViewVariables['bcc_preview'] = $sPreviewBcc;
        $aViewVariables['subject_preview'] = $sPreviewSubject;
        $aViewVariables['message_preview'] = $sPreviewMessage;

        return $this->parse('Generic/Event/order_event_send_email.twig', $aViewVariables);
    }

    function saveConfiguration($sEnvironment)
    {
        $iCrudEditorButtonEvent = $this->get('crud_editor_button_event_id');
        $sSubject = $this->post('subject');
        $sMessage = $this->post('message');

        $sFrom = $this->post('from');
        $sTo = $this->post('to');
        $sBcc = $this->post('bcc');

        $sAttachmentInvoice = $this->post('attachment_invoice');
        $sAttachmentPackingslip = $this->post('attachment_packingslip');

        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_subject', $sSubject);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_message', $sMessage);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_from', $sFrom);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_to', $sTo);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_bcc', $sBcc);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_attachment_invoice', $sAttachmentInvoice);
        SystemRegistryQuery::create()->createOrOverwrite('email_'.$iCrudEditorButtonEvent.'_'.$sEnvironment.'_crud_attachment_packingslip', $sAttachmentPackingslip);
    }

    function getDescription(): string
    {
        return "Verstuur een e-mailbericht";
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);

        if(!$oSaleOrder instanceof SaleOrder)
        {
            throw new LogicException("\$oSaleOrder should be an instance of SaleOrder, got ".get_class($oSaleOrder));
        }

        $sSubject = self::generateTxt('subject', $oSaleOrder->getId(), $this->getButtonEventId());
        $sMessage = self::generateTxt('message', $oSaleOrder->getId(), $this->getButtonEventId());
        $sFrom = self::generateTxt('from', $oSaleOrder->getId(), $this->getButtonEventId());
        $sTo = self::generateTxt('to', $oSaleOrder->getId(), $this->getButtonEventId());
        $sBcc = self::generateTxt('bcc', $oSaleOrder->getId(), $this->getButtonEventId());

        $bAddAttachmentInvoice = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$this->getButtonEventId().'_crud_attachment_invoice');
        $bAddAttachmentPackingslip = SystemRegistryQuery::create()->getNewOrEmpty('email_'.$this->getButtonEventId().'_crud_attachment_packingslip');

        $aAttachments = [];
        if($bAddAttachmentInvoice)
        {
            $sInvoicePdf = Invoice::create($oSaleOrder, 'return');
            $aAttachments[] = \Swift_Attachment::newInstance($sInvoicePdf, 'factuur - '.$oSaleOrder->getInvoiceNumber().'.pdf');
        }

        if($bAddAttachmentPackingslip)
        {
            $sPackingslipPdf = PackingSlip::create($oSaleOrder, 'return');
            $aAttachments[] = \Swift_Attachment::newInstance($sPackingslipPdf, 'pakbon.pdf');
        }

        if(empty($sFrom))
        {
            throw new LogicException("Afzender e-mailadres is leeg / niet ingevuld.");
        }
        if(!filter_var($sFrom, FILTER_VALIDATE_EMAIL))
        {
            throw new LogicException("Afzender e-mailadres ($sFrom) is geen geldig e-mailadres.");
        }

        $aToEmailAdresses = explode(',', $sTo);
        $bSomeEmailsSend = false;
        if(!empty($aToEmailAdresses))
        {
            foreach($aToEmailAdresses as $sToEmailAddress)
            {
                if(filter_var($sToEmailAddress, FILTER_VALIDATE_EMAIL))
                {
                    $bSomeEmailsSend = true;
                    $oMailMessage = new MailMessage();
                    $oMailMessage->setFrom($sFrom);
                    $oMailMessage->setTo($sToEmailAddress);
                    $oMailMessage->setSubject($sSubject);
                    $oMailMessage->setBody($sMessage);

                    Mailer::send($oMailMessage, $aAttachments);

                    SaleOrderEmail::register($oSaleOrder->getId(), $oMailMessage, $aAttachments);
                }
            }
        }
        $aBccEmailAdresses = explode(',', $sBcc);

        if(!empty($aBccEmailAdresses))
        {
            foreach($aBccEmailAdresses as $sBccEmailAddress)
            {
                if(filter_var($sBccEmailAddress, FILTER_VALIDATE_EMAIL))
                {
                    $bSomeEmailsSend = true;
                    $oMailMessage = new MailMessage();
                    $oMailMessage->setFrom($sFrom);
                    $oMailMessage->setTo($sBccEmailAddress);
                    $oMailMessage->setSubject($sSubject);
                    $oMailMessage->setBody($sMessage);
                    Mailer::send($oMailMessage, $aAttachments);

                    SaleOrderEmail::register($oSaleOrder->getId(), $oMailMessage, $aAttachments);
                }
            }
        }

        if(!$bSomeEmailsSend)
        {
            throw new LogicException("Er zijn geen factuur berichten verzonden want er was geen ontvanger e-mailadres geconfigureerd en .");
        }
    }
}

