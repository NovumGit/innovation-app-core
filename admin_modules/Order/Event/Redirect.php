<?php
namespace AdminModules\Order\Event;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;

use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;


class Redirect extends AbstractEvent implements AbstractConfigurableEvent{


    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        $iId = $this->get('event_id', null, true);
        $sValue = $this->getFieldSetting($iId.$sEnvironment, 'redirect_value');
       // $sField = $this->getFieldSetting($iId.$sEnvironment, 'redirect_field');
        $sNote = $this->getFieldSetting($iId.$sEnvironment, 'notes');

        $aVariables['current_value'] = $sValue;
        $aVariables['current_note'] = $sNote;
        $aVariables['event_id'] = $iCrudEditorButtonEventId;
        return $this->parse('Generic/Event/redirect.twig', $aVariables);
    }
    function saveConfiguration($sEnvironment)
    {
        $iId = $this->get('event_id', null, true);

        $this->storeFieldSetting($iId.$sEnvironment, 'redirect_value', $this->get('redirect_value'));
        $this->storeFieldSetting($iId.$sEnvironment, 'redirect_field', $this->get('redirect_field'));
        $this->storeFieldSetting($iId.$sEnvironment, 'notes', $this->get('notes'));
    }

    function getFieldSetting($iButtonEventId, $sSetting)
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey('redirect_'.$iButtonEventId.'_crud_'.$sSetting);

        if($oSystemRegistry)
        {
            return $oSystemRegistry->getItemValue();
        }
        return null;
    }
    function storeFieldSetting($iButtonEventId, $sSetting, $sValue)
    {
        $sKey = 'redirect_'.$iButtonEventId.'_crud_'.$sSetting;
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sValue);
        $oSystemRegistry->save();
    }

    function getDescription(): string
    {
        return "Redirect";
    }

    function outputType(): \core\Mime\Mime
    {
        return null;
    }

    function isConfigurable(): bool
    {
        return true;
    }

    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id');
        $this->redirect(str_replace('#order_id#', $iOrderId, $this->getFieldSetting($this->getButtonEventId().$sEnvironment, 'redirect_value')));
    }
}

