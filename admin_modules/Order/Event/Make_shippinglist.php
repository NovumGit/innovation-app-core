<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\ExcelMime;
use Model\Sale\SaleOrderQuery;

class Make_shippinglist extends AbstractEvent{

    private static $iRow = 1;
    static $bIsFirstCall = true;
    private static $oExcel;

    function outputType(): \core\Mime\Mime
    {
        return new ExcelMime();
    }
    function getDescription(): string
    {
        return "Make shippinglist";
    }
    function trigger(string $sEnvironment): void
    {
        if(!self::$oExcel  instanceof \Excel)
        {
            $sTitle = 'Shippinglist';
            self::$oExcel = new \Excel($sTitle);
            self::$oExcel->merge(1, self::$iRow, 5);
            self::$oExcel->setValue(1, self::$iRow, $sTitle);
            self::$oExcel->backgroundCell("#fff000", 1, self::$iRow);
            self::$iRow++;
            self::$oExcel->setValue(1, self::$iRow, 'Customer');
            self::$oExcel->bold();
            self::$oExcel->setValue(6, self::$iRow, 'Product');
            self::$oExcel->bold();
            self::$oExcel->setValue(7, self::$iRow, 'Quantity');
            self::$oExcel->bold();

            self::$iRow++;
        }

        self::$iRow++;
        $iCustomerFirstLine = self::$iRow;
        $iOrderId = $this->get('order_id');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);


        self::$oExcel->setValue(1, self::$iRow, "#".$oSaleOrder->getOrderNumber());
        if($oSaleOrder->getCompanyName())
        {
            self::$iRow++;
            self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCompanyName());
        }
        if(trim($oSaleOrder->getCpFirstName().' '.$oSaleOrder->getCpLastName()))
        {
            self::$iRow++;
            self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCpFirstName().' '.$oSaleOrder->getCpLastName());
        }
        self::$iRow++;
        self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCustomerDeliveryAddressL1());
        if($oSaleOrder->getCustomerDeliveryAddressL2())
        {
            self::$iRow++;
            self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCustomerDeliveryAddressL2());
        }

        self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCustomerDeliveryCity());
        self::$oExcel->setValue(2, self::$iRow, $oSaleOrder->getCustomerDeliveryPostal());
        self::$iRow++;

        self::$oExcel->setValue(1, self::$iRow, $oSaleOrder->getCustomerDeliveryCountry());
        self::$iRow++;

        self::$oExcel->setValue(1, self::$iRow, 'Shipping price: €'.number_format($oSaleOrder->getShippingPrice(), 2, ',', '.'));
        self::$iRow++;

        self::$oExcel->setValue(1, self::$iRow, 'Total price: €'.number_format($oSaleOrder->getTotalPrice(), 2, ',', '.'));
        self::$iRow++;

        if($oSaleOrder->getPaymethodName())
        {
            self::$oExcel->setValue(1, self::$iRow, 'Pay method: '.$oSaleOrder->getPaymethodName());
            self::$iRow++;
        }
        if($oSaleOrder->getCustomerOrderNote())
        {
            self::$oExcel->setValue(1, self::$iRow, 'Note: '.$oSaleOrder->getCustomerOrderNote());
            self::$iRow++;
        }

        if($oSaleOrder->getHasWrap())
        {
            self::$oExcel->setValue(1, self::$iRow, 'STEALTH');
            self::$oExcel->bold();
            self::$iRow++;
        }

        $iCurrentProductLine = $iCustomerFirstLine;
        $aOrderItems = $oSaleOrder->getSaleOrderItems();


        foreach($aOrderItems as $orderItem)
        {
            self::$oExcel->setValue(6, $iCurrentProductLine, $orderItem->getDescription());
            self::$oExcel->setValue(7, $iCurrentProductLine, $orderItem->getQuantity());
            $iCurrentProductLine++;
        }

        if($iCurrentProductLine > self::$iRow)
        {
            self::$iRow = $iCurrentProductLine;
        }

        self::$iRow++;

        /*
        $oSaleOrder->getOrderNumber();
        $oSaleOrder->getOrderNumber()
        $oSaleOrder->getCompanyName()
        */

        return self::$oExcel;
/*
        $excel->setValue(0, 2, "Days", true);
        $excel->setValue(1, 2, "Monday", true);
        $excel->setValue(2, 2, "Tuesday", true);
        $excel->setValue(3, 2, "Wednesday", true);
        $excel->setValue(0, 3, "Hackaton", true)
            ->setValue(1, 3, "12", true)
            ->setValue(2, 3, "34", true)
            ->setValue(3, 3, "15", true);
        $excel->setValue(0,4, "Networking", true)
            ->setValue(1,4, "10", true)
            ->setValue(2,4, "12", true)
            ->setValue(3,4, "43", true);
        $excel->download('juan');
*/


        /*
        if(self::$bIsFirstCall)
        {

            echo file_get_contents('../admin_modules/Order/inc/Dymo/print.js');
            echo PHP_EOL;
            self::$bIsFirstCall = false;
        }

        $iOrderId = $this->get('order_id');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $aVars = ['sale_order' => $oSaleOrder];
        echo $this->parse('Order/inc/Dymo/single-job.twig.js', $aVars);
        */
    }
    function isConfigurable(): bool
    {
        return false;
    }
}
