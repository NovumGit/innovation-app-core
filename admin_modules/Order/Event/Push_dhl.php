<?php
namespace AdminModules\Order\Event;

use Dhl\ApiClient;
use Core\Cfg;
use Core\Mime\JavascriptMime;
use Core\AbstractEvent;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\CountryQuery;

class Push_dhl extends AbstractEvent
{
	private $sandbox = false;

    function hasOutput()
    {
        return true;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new JavascriptMime();
    }
    function getDescription(): string
    {
        return "Push naar DHL";
    }

    public static function getClient():ApiClient
    {
	    $sDhlApiUserId = Cfg::get('DHL_API_USER_ID');
	    $sDhlApiKey= Cfg::get('DHL_API_KEY');

	    $iAccountId = Cfg::get('DHL_API_ACCOUNT_ID');
	    $iOrganisationId = Cfg::get('DHL_API_ORGANIZATION_ID');

	    return new ApiClient([
		    'apiUser' => $sDhlApiUserId,
		    'apiKey' => $sDhlApiKey,
		    'accountId' => $iAccountId,
		    'organisationId' => $iOrganisationId,
		    'senderType' => ''
	    ]);
    }
	/**
	 * Returns the shipment options (pick up at pick up point, deliver to my home etc)
	 * @param SaleOrder $oSaleOrder
	 * @return array
	 */
    static function getShipmentOptions(SaleOrder $oSaleOrder):array
    {
		$client = self::getClient();

	    $oCountryQuery = CountryQuery::create();
		$oCountryFrom = $oCountryQuery->findOneByName($oSaleOrder->getOurGeneralCountry());

	    $oCountryQuery = CountryQuery::create();
	    $oCountryTo = $oCountryQuery->findOneByName($oSaleOrder->getCustomerDeliveryCountry());

	    $result = $client->capabilities($args = [
		    'fromCountry' => $oCountryFrom->getIso2(),
		    'fromPostalCode' => $oSaleOrder->getOurGeneralPostal(),
		    'toCountry' => $oCountryTo->getIso2(),
		    'toPostalCode' => $oSaleOrder->getCustomerDeliveryPostal(),
		    'senderType' => 'business'
	    ]);

	    echo "-------------------------- " . PHP_EOL;
	    print_r($args);
	    echo "-------------------------- " . PHP_EOL;
	    print_r($result);

	    return $result;
    }

    function trigger(string $sEnvironment): void
    {


	    /*
		$iOrderId = $this->get('order_id', null, true,'numeric');
		$oCitymailShipment = CitymailShipmentQuery::create()->findOneBySaleOrderId($iOrderId);

		$fWeight = 0;
		$sShippingOption = 0;
		if($oCitymailShipment instanceof CitymailShipment)
		{
			$fWeight = $oCitymailShipment->getWeight();
			$sShippingOption = $oCitymailShipment->getShipmentOption();
		}

		$bHasErrors = false;
		if(empty($fWeight))
		{
			$bHasErrors = true;
			StatusMessage::warning(Translate::fromCode("Gewicht is een verplicht veld."));
		}
		if(empty($sShippingOption))
		{
			$bHasErrors = true;
			StatusMessage::warning(Translate::fromCode("Verzendmethode is een verplicht veld"));
		}

		if(!$bHasErrors)
		{
			StatusMessage::success(Translate::fromCode("Citymail label wordt geopend in een nieuw tab."));
		}
		$aVars = ['order_id' => $iOrderId, 'has_errors' => (int)$bHasErrors];
		echo $this->parse('Order/inc/Citymail/open-label.twig.js', $aVars);
		*/
    }
}
