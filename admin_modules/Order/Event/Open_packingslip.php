<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\PdfMime;
use Core\User;
use Document\PackingSlip\PackingSlip;
use Model\Sale\SaleOrderQuery;

class Open_packingslip extends AbstractEvent  {

    function getDescription(): string
    {
        return "Open de pakbon";
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new PdfMime();
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrder = $oSaleOrderQuery->findOneById($iOrderId);

        PackingSlip::create($oSaleOrder);
    }
}

