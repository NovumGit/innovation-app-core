<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Logger;
use Core\Mime\VoidMime;
use Core\Translate;
use Model\Product;
use Model\Sale\SaleOrderItemQuery;
use Model\Sale\SaleOrderItemStockClaimed;

class Deduct_stock extends AbstractEvent
{

    function getDescription(): string
    {
        return Translate::fromCode("Voorraad aanpassen");
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function isConfigurable(): bool
    {
        return false;
    }

    /**
     * @param string $sEnvironment
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id');
        $aSaleOrderItemQuery = SaleOrderItemQuery::create();
        $aSaleOrderItemQuery->filterBySaleOrderId($iOrderId);
        $aSaleOrderItems = $aSaleOrderItemQuery->find();

        if(!$aSaleOrderItems->isEmpty())
        {
            foreach($aSaleOrderItems as $oSaleOrderItem)
            {
                $oProduct = $oSaleOrderItem->getFirstProduct();

                if($oProduct instanceof Product)
                {
                    $iNewQuantity = $oProduct->getStockQuantity() - $oSaleOrderItem->getQuantity();
                    Logger::info('Lowered the stock of '.$oProduct->getId().' for sale order item '.$oSaleOrderItem->getId().' old value was '.$oProduct->getStockQuantity().' new is '.$iNewQuantity);
                    $oProduct->setStockQuantity($iNewQuantity);
                    $oProduct->save();

                    $oSaleOrderItemStockClaimed = new SaleOrderItemStockClaimed();
                    $oSaleOrderItemStockClaimed->setSaleOrderItemId($oSaleOrderItem->getId());
                    $oSaleOrderItemStockClaimed->setProductId($oProduct->getId());
                    $oSaleOrderItemStockClaimed->setCreatedOn(time());
                    $oSaleOrderItemStockClaimed->setQuantity($oSaleOrderItem->getQuantity());
                    $oSaleOrderItemStockClaimed->save();

                }
            }
        }
    }
}

