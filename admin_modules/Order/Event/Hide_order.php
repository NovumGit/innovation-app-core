<?php
namespace AdminModules\Order\Event;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\Mime\VoidMime;
use Core\Translate;
use Crud\Sale_order\CrudSale_orderManager;
use Helper\CrudManagerHelper;
use Model\Sale\SaleOrderQuery;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;

class Hide_order extends AbstractEvent implements AbstractConfigurableEvent{

    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment)
    {
        /*
        $iEventId = $this->get('event_id', null, true, 'numeric');

        $sValue = $this->getFieldSetting($iEventId.$sEnvironment, 'overwrite_value');



        $sSelectedField = $this->get('crud_field');
        if(!$sSelectedField)
        {
            $sSelectedField = $sField;
        }

        $oCrudManager = new CrudSale_orderManager();
        $aCrudFields = CrudManagerHelper::getAllFieldObjects($oCrudManager, 'Sale_order', $sSelectedField);

        if(!empty($aCrudFields['selected_field_lookups']))
        {
            foreach($aCrudFields['selected_field_lookups'] as &$aField)
            {
                $aField['selected'] =  ($sValue == $aField['id']) ? 'selected="selected"' : '';
            }
        }

        $aVariables['current_value'] = $sValue;
        $aVariables['current_note'] = $sNote;
        $aVariables['crud_fields'] = $aCrudFields;

        $aVariables['event_id'] = $iEventId;

        return $this->parse('Generic/Event/order_event_change_field.twig', $aVariables);
        */
    }
    function saveConfiguration($sEnvironment)
    {
        /*
        $iEventId = $this->get('event_id', null, true);

        $this->storeFieldSetting($iEventId.$sEnvironment, 'overwrite_value', $this->get('overwrite_value'));
        $this->storeFieldSetting($iEventId.$sEnvironment, 'crud_field', $this->get('crud_field'));
        $this->storeFieldSetting($iEventId.$sEnvironment, 'notes', $this->get('notes'));
        */
    }

    function getFieldSetting($iButtonEventId, $sSetting)
    {
        /*
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey('overwrite_'.$iButtonEventId.'_crud_'.$sSetting);

        if($oSystemRegistry)
        {
            return $oSystemRegistry->getItemValue();
        }
        return null;
        */
    }
    function storeFieldSetting($iButtonEventId, $sSetting, $sValue)
    {
        /*
        $sKey = 'overwrite_'.$iButtonEventId.'_crud_'.$sSetting;
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sValue);
        $oSystemRegistry->save();
        */
    }

    function getDescription(): string
    {
        return Translate::fromCode("Verberg order tijdelijk");
    }
    function outputType(): \core\Mime\Mime
    {
        /*
        return new VoidMime();
        */
    }
    function isConfigurable(): bool
    {
        return true;
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id');

        $sValue = $this->getFieldSetting($this->getButtonEventId(), 'overwrite_value');
        $sField = $this->getFieldSetting($this->getButtonEventId(), 'crud_field');

        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $sSetter = 'set'.ucfirst($sField);

        $oSaleOrder->$sSetter($sValue);
        $oSaleOrder->save();
    }
}

