<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\JavascriptMime;
use Model\Sale\SaleOrderItemQuery;
use Model\Sale\SaleOrderQuery;

class Dymo_print extends AbstractEvent{

    static $bIsFirstCall = true;

    function outputType(): \core\Mime\Mime
    {
        return new JavascriptMime();
    }
    function getDescription(): string
    {
        return "Dymo print delivery address";
    }
    function trigger(string $sEnvironment): void
    {
        if(self::$bIsFirstCall)
        {
            echo file_get_contents('../admin_modules/Order/inc/Dymo/print.js');
            echo PHP_EOL;
            self::$bIsFirstCall = false;
        }

        $iOrderId = $this->get('order_id');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $aVars = ['sale_order' => $oSaleOrder];
        echo $this->parse('Order/inc/Dymo/single-job.twig.js', $aVars);
    }
    function isConfigurable(): bool
    {
        return false;
    }
}
