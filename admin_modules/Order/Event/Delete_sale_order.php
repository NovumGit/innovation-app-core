<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\VoidMime;
use Core\StatusMessage;
use Core\Translate;
use Crud\Sale_order\CrudSale_orderManager;
use Exception\LogicException;

class Delete_sale_order extends AbstractEvent  {

    function getDescription(): string
    {
        return Translate::fromCode("Verwijder de order");
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function trigger(string $sEnvironment): void
    {
        StatusMessage::success(Translate::fromCode("Order verwijderd."));
        $iOrderId = $this->get('order_id');
        if(!$iOrderId)
        {
            throw new LogicException(Translate::fromCode("Order delete event triggered maar het order id kon niet achterhaald worden"));
        }
        (new CrudSale_orderManager())->getModel(['id' => $iOrderId])->setItemDeleted(1)->save();
    }
}

