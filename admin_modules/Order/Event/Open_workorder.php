<?php
namespace AdminModules\Order\Event;

use Core\AbstractEvent;
use Core\Mime\PdfMime;
use Core\User;
use Document\WorkOrder\WorkOrder;
use Model\Sale\SaleOrderQuery;

class Open_workorder extends AbstractEvent  {

    function hasOutput()
    {
        return true;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new PdfMime();
    }

    function getDescription(): string
    {
        return "Open de werkbon";
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrder = $oSaleOrderQuery->findOneById($iOrderId);

        if(!$oSaleOrder->getWorkOrderPrintedBy())
        {
            $oUser = User::getMember();
            $oSaleOrder->setWorkOrderPrintedBy($oUser->getFirstName().' '.$oUser->getLastName());
            $oSaleOrder->setWorkOrderPrintedOn(time());
            $oSaleOrder->save();
        }

        WorkOrder::create($oSaleOrder);
    }
}

