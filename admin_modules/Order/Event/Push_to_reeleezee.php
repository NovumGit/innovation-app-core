<?php
namespace AdminModules\Order\Event;

use AdminModules\Api\Reeleezee\PushController;
use Api\Push\Accounting\Reeleezee\Main;
use Api\Push\Accounting\Reeleezee\Reeleezee_config;
use Core\Mime\VoidMime;
use Core\AbstractEvent;
use Model\Sale\SaleOrderQuery;

class Push_to_reeleezee extends AbstractEvent
{
    function hasOutput()
    {
        return false;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new VoidMime();
    }

    function getDescription(): string
    {
        return "Push naar Reeleezee";
    }

    /**
     * @event_if de pakbon wordt geopend
     * @event_then open de pakbon
     * @param string $sEnvironment
     */
    function trigger(string $sEnvironment): void
    {

        $iOrderId = $this->get('order_id', null, true, 'numeric');

        // Dit systeem maakt alleen maar een json bestandje aan met het juiste ordernummer.
        // De order staat vanaf dat moment in de queue.

        $bSkipFilter = $this->get('skip_filter', false, false);

        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);

        $aDirs = Reeleezee_config::getDirectories();
        // Filter
        if(Reeleezee_config::filter($oSaleOrder) || $bSkipFilter)
        {
            $oSaleOrder->setIsPushedToAccounting(true);
            $oSaleOrder->save();

            touch($aDirs['outbox'].'/'.$iOrderId.'.json');
            chmod($aDirs['outbox'].'/'.$iOrderId.'.json', 0777);
        }

        if(isset($_GET['immediately']))
        {
            try
            {
                $oReeleezeeMain = new Main();
                $oReeleezeeMain->push(true);
            }
            catch(\Exception $e)
            {
                echo $e->getMessage();
                mail('anton@nui-boutkam.nl', 'Push to Reeleezee problemen', $e->getMessage().PHP_EOL.$e->getFile());
            }
        }
    }
}
