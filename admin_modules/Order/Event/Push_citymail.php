<?php
namespace AdminModules\Order\Event;

use Core\Mime\JavascriptMime;
use Core\AbstractEvent;
use Core\StatusMessage;
use Core\Translate;
use Model\Sale\CitymailShipment;
use Model\Sale\CitymailShipmentQuery;

class Push_citymail extends AbstractEvent
{
    function hasOutput()
    {
        return true;
    }
    function isConfigurable(): bool
    {
        return false;
    }
    function outputType(): \core\Mime\Mime
    {
        return new JavascriptMime();
    }
    function getDescription(): string
    {
        return "Push naar Citymail";
    }
    function trigger(string $sEnvironment): void
    {
        $iOrderId = $this->get('order_id', null, true,'numeric');
        $oCitymailShipment = CitymailShipmentQuery::create()->findOneBySaleOrderId($iOrderId);

        $fWeight = 0;
        $sShippingOption = 0;
        if($oCitymailShipment instanceof CitymailShipment)
        {
            $fWeight = $oCitymailShipment->getWeight();
            $sShippingOption = $oCitymailShipment->getShipmentOption();
        }

        $bHasErrors = false;
        if(empty($fWeight))
        {
            $bHasErrors = true;
            StatusMessage::warning(Translate::fromCode("Gewicht is een verplicht veld."));
        }
        if(empty($sShippingOption))
        {
            $bHasErrors = true;
            StatusMessage::warning(Translate::fromCode("Verzendmethode is een verplicht veld"));
        }

        if(!$bHasErrors)
        {
            StatusMessage::success(Translate::fromCode("Citymail label wordt geopend in een nieuw tab."));
        }
        $aVars = ['order_id' => $iOrderId, 'has_errors' => (int)$bHasErrors];
        echo $this->parse('Order/inc/Citymail/open-label.twig.js', $aVars);
    }
}
