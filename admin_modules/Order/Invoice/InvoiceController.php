<?php
namespace AdminModules\Order\Invoice;

use Core\Cfg;
use Core\Utils;
use Exception\LogicException;
use Core\MainController;
use Core\Setting;
use Knp\Snappy\Pdf;
use Model\Sale\SaleOrderQuery;

class InvoiceController extends MainController
{
    function getImgBaseUrl()
    {
        $sCustomFolder = Cfg::get('CUSTOM_FOLDER');
        $sProtocol = Cfg::get('ADMIN_PROTOCOL');
        $sDomain = Cfg::get('DOMAIN');

        // live.domeinnaam admin.live.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'live.') === 0 || strpos($_SERVER['HTTP_HOST'],'live.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        // test.domeinnaam admin.test.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'test.') === 0 || strpos($_SERVER['HTTP_HOST'],'test.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        $sDomain = str_replace('admin.', '', $sDomain);
        $sImagePath = "$sProtocol://admin.$sDomain/custom/$sCustomFolder/logo-left-top.png";
        return $sImagePath;
    }

    function doNoLoginPrintInvoice()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $sFormat = $this->get('format', 'html', false);
        $sSecretKey = $this->get('secret_key');
        if($sSecretKey != '12312387123798b21eifweifcvlkjwqn')
        {
            throw new LogicException("Sorry cannot do that, wrong key.");
        }
        $this->printInvoice($iOrderId, $sFormat);
    }
    function run()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $sFormat = $this->get('format', 'html', false);

        return $this->printInvoice($iOrderId, $sFormat);
    }
    private function printInvoice($iOrderId, $sFormat)
    {
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $sFooterTxt = Setting::get('translated_invoice_footer_txt_'.$oSaleOrder->getLanguageId());



        if(!$oSaleOrder->getCreatedOn())
        {
            $oSaleOrder->getCreatedOn(time());
            $oSaleOrder->save();
        }

        // @todo, deze kan en moet een keer weg, is voor vangool
        $sFabricsUnit =  date('YmdG') >= '2017051716' ? ' mtr' : 'cm';

        $aData = [
            'fabrics_unit' => $sFabricsUnit,
            'img_base_url' => $this->getImgBaseUrl(),
            'sale_order' => $oSaleOrder,
            'footer_txt' => $sFooterTxt,
            'dutch_address_format' => Cfg::get('INVOICE_USE_DUTCH_ADDRESS_FORMAT')
        ];



        $sInvoice = $this->parse('Order/Invoice/invoice.twig', $aData);

        if($sFormat == 'pdf' || $sFormat == 'pdf_return')
        {
            $oSnappy = new Pdf();

            $oSnappy->setBinary(Utils::getSnappyBinaryPath());
            $oSnappy->generateFromHtml($sInvoice, 'invoice.pdf', [], true);

            if($sFormat == 'pdf')
            {
                header('Content-Type: application/pdf');
                echo file_get_contents('invoice.pdf');
                unlink('invoice.pdf');
                exit($sInvoice);
            }
            else if($sFormat == 'pdf_return')
            {
                return file_get_contents('invoice.pdf');
            }
        }
        else if($sFormat == 'html')
        {
            return $sInvoice;
        }
        else if($sFormat == 'html_display')
        {
            exit($sInvoice);
        }
        throw new LogicException("Unsupported output format for pdf generator class $sFormat.");

    }
}