<?php
namespace AdminModules\Order;
use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Model\Crm\CustomerAddressQuery;

class AddressesController extends MainController
{
    function run(){
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');

        $iDeliveryAddressId = $this->get('delivery_address_id');
        $iInvoiceAddressId = $this->get('invoice_address_id');

        if(is_numeric($iDeliveryAddressId) && is_numeric($iInvoiceAddressId))
        {
            // Er is al een afleveradres en een factuuradres gekozen, move on.
            $this->redirect("/order/new?_do=CreateOrderFromCustomer&customer_id=$iCustomerId&delivery_address_id=$iDeliveryAddressId&invoice_address_id=$iInvoiceAddressId");
            exit();
        }

        if($iDeliveryAddressId == 'create_one')
        {
            $sR = 'after_create_delivery_address';
            DeferredAction::register($sR, "/order/new?_do=SelectCustomerAddresses&customer_id=$iCustomerId");
            $this->redirect("/crm/address/edit?customer_id=$iCustomerId&r=$sR");
        }
        if($iInvoiceAddressId == 'create_one')
        {
            $sR = 'after_create_invoice_address';
            DeferredAction::register($sR, "/order/new?_do=SelectCustomerAddresses&customer_id=$iCustomerId");
            $this->redirect("/crm/address/edit?customer_id=$iCustomerId&r=$sR");
        }

        DeferredAction::register('after_create', $this->getRequestUri());
        $sCreateNewDeliveryAddressUrl = "/crm/address/edit?customer_id=$iCustomerId&r=after_create&address_type=delivery";

        if($iDeliveryAddressId == 'select_one')
        {
            $sCurrentType = 'delivery';
            $aAddresses = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'delivery');
            $sQuestionTitle = Translate::fromCode('Naar welk adres moet de order verzonden worden?');
        }
        else if($iInvoiceAddressId == 'select_one')
        {
            $sCurrentType = 'invoice';
            $aAddresses = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'invoice');
            $sQuestionTitle = Translate::fromCode('Wat is het post/factuuradres?');
        }

        $aAddressCollections = [];
        $aAddressCollections[] = $aAddresses;
        $aAddressCollections[] = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'general');


        $aViewData = [
            'customer_id' =>  $iCustomerId,
            'question_title' => $sQuestionTitle,
            'current_type' => $sCurrentType,
            'delivery_address_id' => $this->get('delivery_address_id'),
            'invoice_address_id' => $this->get('invoice_address_id'),
            'address_collections' => $aAddressCollections,
            'new_delivery_address_url' => $sCreateNewDeliveryAddressUrl
        ];

        $aTopNavVars = [
        ];

        $sTopNav = $this->parse('Order/top_nav_addresses.twig', $aTopNavVars);
        $sOverview = $this->parse('Order/addresses.twig', $aViewData);

        $aView = [
            'top_nav' => $sTopNav,
            'content' => $sOverview,
            'title' => 'Nieuw adres toevoegen'
        ];

        return $aView;
    }

}
