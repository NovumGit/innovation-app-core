<?php
namespace AdminModules\Order\Citymail;

use Api\Push\Shipping\Citymail\GetlabelVo;
use Api\Push\Shipping\Citymail\TransferVo;
use Api\Push\Shipping\Citymail\PutOrder;
use Core\Logger;
use Core\MainController;
use Exception\LogicException;
use Model\Sale\CitymailShipment;
use Model\Sale\CitymailShipmentQuery;

class PdfController extends MainController
{
    function run()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');

        $oPutOrder = new PutOrder($iOrderId);
        $mResponse = TransferVo::call($oPutOrder);


        $oCitymailShipment = CitymailShipmentQuery::create()->findOneBySaleOrderId($iOrderId);

        if(!$oCitymailShipment instanceof CitymailShipment)
        {
            throw new LogicException("Could not find the shipment record, this indicates a bug.");
        }

        if(!isset($mResponse['success']))
        {
            var_dump($mResponse);
            Logger::warning('Pushing this order to Citymail went wrong');
            Logger::warning(print_r($mResponse, true));

            throw new LogicException("Somethig went wrong while sending this order to Citymail.");
            exit();
        }
        $oCitymailShipment->setShipmentId($mResponse['success']['shipment_id']);
        $oCitymailShipment->setStatusCode($mResponse['success']['status']);
        $oCitymailShipment->save();

        $oGetLabelVo = new GetlabelVo($iOrderId);
        $mResponse = TransferVo::call($oGetLabelVo);

        if(!isset($mResponse['labels_file']))
        {
            var_dump($mResponse);
            exit();
        }

        header("Content-type: application/pdf");
        echo file_get_contents($mResponse['labels_file']);

        exit();
    }
}