<?php
namespace AdminModules\Order;
use Core\DeferredAction;
use Core\Logger;
use Core\MainController;
use Crud\CrudViewManager;
use Crud\Sale_order\CrudSale_orderManager;
use Helper\FilterHelper;
use Helper\OverviewButtonHelper;
use Model\Sale\SaleOrderItem;
use Model\Sale\SaleOrderQuery;
use LogicException;
use model\Setting\CrudManager\CrudView;
use Core\ButtonEventDispatcher;

class OverviewController extends MainController{

    function doTriggerButtonEvent()
    {
        $iButtonId = $this->post('button_id', null, true, 'numeric');
        $aSaleOrders = $this->post('sale_order', null, true, 'array');
        $aSaleOrders = array_keys($aSaleOrders);
        $bOneHasOutput = false;
        if(!empty($aSaleOrders))
        {
            $bIsLast = false;
            $iCurrent = 1;
            $iNumItems = count($aSaleOrders);
            foreach($aSaleOrders as $iSaleOrderId)
            {
                if($iCurrent == $iNumItems)
                {
                    $bIsLast = true;
                }
                $bEventHasOutput = ButtonEventDispatcher::dispatch($iButtonId, ['order_id' => $iSaleOrderId], 'overview', $bIsLast);
                if($bEventHasOutput)
                {
                    $bOneHasOutput = true;
                }
                $iCurrent++;
            }
        }
        if(!$bOneHasOutput)
        {
            $sTab = $this->get('tab');
            if($sTab)
            {
                $sTab = "?tab=$sTab";
            }
            $this->redirect('/order/overview'.$sTab);
        }
    }
    function run(){
        $iCurrentPage = $this->get('p');
        $iCurrentPage = is_numeric($iCurrentPage) ? $iCurrentPage : 1;

        $sCurrentUrl = $this->getRequestUri(true);
        $sR = 'order_overview_back_url';
        DeferredAction::register($sR, $sCurrentUrl);
        
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $aPost = $this->post();

        $oSaleOrderQuery = SaleOrderQuery::create();
        $oCrudSaleOrderManager = new CrudSale_orderManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudSaleOrderManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }


        $sDefaultOrderBy = $oCrudview->getDefaultOrderBy() ? $oCrudview->getDefaultOrderBy() : 'id';
        $sDefaultOrderDir = $oCrudview->getDefaultOrderDir() ? $oCrudview->getDefaultOrderDir() : 'asc';

        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : $sDefaultOrderBy;
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : $sDefaultOrderDir;
        if($sSortField == 'IsDeleted')
        {
            $sSortField = 'ItemDeleted';
        }
        $oSaleOrderQuery->orderBy($sSortField, $sSortDirection);

        $aFields = CrudViewManager::getFields($oCrudview);
        $oSaleOrderQuery = FilterHelper::applyFilters($oSaleOrderQuery, $iTabId);
        $aFilters = isset($aPost['filter']) ? $aPost['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $oSaleOrderQuery = FilterHelper::generateVisibleFilters($oSaleOrderQuery, $aFilters, function(SaleOrderQuery $oSaleOrderQuery, $sString)
        {
            if(!empty($sString))
            {
                $oSaleOrderQuery = SaleOrderQuery::create();

                $oSaleOrderQuery->condition('cond1', 'order_number LIKE "%'.$sString.'%"');
                $oSaleOrderQuery->condition('cond2', 'invoice_number LIKE "%'.$sString.'%"');
                $oSaleOrderQuery->condition('cond3', 'company_name LIKE "%'.$sString.'%"');
                $oSaleOrderQuery->condition('cond4', 'shipping_note LIKE "%'.$sString.'%"');
                $oSaleOrderQuery->where(array('cond1', 'cond2', 'cond3', 'cond4'), 'or');
            }
            return $oSaleOrderQuery;
        });

        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oCountSaleOrderQuery = SaleOrderQuery::create();
                $oCountSaleOrderQuery = FilterHelper::applyFilters($oCountSaleOrderQuery, $o->getId());
                $o->_sz = $oCountSaleOrderQuery->count();
            }
        }
        $oCrudSaleOrderManager->setOverviewData($oSaleOrderQuery, $iCurrentPage);

        $aButtons = OverviewButtonHelper::getButtons($iTabId);

		$filter_html = FilterHelper::renderHtml($iTabId);
		$overview_html = $oCrudSaleOrderManager->getOverviewHtml($aFields);

        $aViewData = [
            'filter_html' => $filter_html,
            'overview_table' => $overview_html,
            'title' => $oCrudview->getName(),
            'buttons' => $aButtons,
        ];

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Sale_order',
            'manager' => 'Sale_orderManager',
            'r' => $sR,
            'overview_url' => $oCrudSaleOrderManager->getOverviewUrl(),
            'new_url' => $oCrudSaleOrderManager->getCreateNewUrl(),
            'new_title' => $oCrudSaleOrderManager->getNewFormTitle()
        ];

        $sTopNav = $this->parse('Order/top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('Order/overview.twig', $aViewData);

        $aView =
        [
            'top_nav' => $sTopNav,
            'title' => $oCrudview->getName(),
            'content' => $sOverview
        ];

        return $aView;
    }
}
