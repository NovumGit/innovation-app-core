
$('#fld_create_shipping').on('click', function(e)
{
    e.preventDefault();
    $('[name=_do]').val('CreateShippingCitymail');

    $('#shipping_form').trigger('submit');

});
// Voorkom problemen met back button
$('[name=_do]').val('Store');

$('.edit_sale_order_line').on('click', function(e)
{
    e.preventDefault();
    oParent = $(this).parent().parent();
    oParent.fadeOut();
    iOrderItemId = sDescription = $('.description', oParent).data('id');
    $('#fld_delete').val(iOrderItemId);
    sDescription = $('.description', oParent).html();
    fQuantity = $('.quantity', oParent).val();
    fPiecePrice = $('.pieceprice', oParent).html().replace('€', '');

    $('#fld_order_item_description').val(sDescription);
    $('#fld_order_item_quantity').val(fQuantity);
    $('#fld_order_item_price').val(fPiecePrice);

    console.log(oParent);
});

$('#fld_add_manual').on('click', function(e)
{
    /*
     <td><input type="text" id="fld_order_item_description" name="order_item_description"></td>
     <td><input type="text" id="fld_order_item_quantity" name="order_item_quantity"></td>
     <td><input type="text" id="fld_order_item_price" name="order_item_price"></td>
     <td><input type="text" id="fld_order_item_vat" name="order_item_vat"></td>
     */
    e.preventDefault();
    //$('#fld_add_manual').href($('#fld_add_manual').href + '');
    window.location.href = $('#fld_add_manual').attr('href') + '&description=' + $('#fld_order_item_description').val() +
                                                        '&quantity=' + $('#fld_order_item_quantity').val() +
                                                        '&price=' + $('#fld_order_item_price').val() +
                                                        '&vat=' + $('#fld_order_item_vat').val() +
                                                        '&delete_item=' +$('#fld_delete').val();
});

var onkeyupFunction = function(){
    var quantity = parseInt($('#fld_order_item_quantity').val());
    var price = parseInt($('#fld_order_item_price').val());
    var vat = parseInt($('#fld_order_item_vat').val());

    //!! = http://stackoverflow.com/questions/784929/what-is-the-not-not-operator-in-javascript
    if(!!quantity && !!price && !!vat){
        $('#fld_order_item_price_total').html('&euro; ' + ((price * quantity) / 100 * (100 + vat)).toFixed(2));
        console.log('&euro; ' + price * vat * quantity);
    }
};

$('#fld_order_item_quantity').on('keyup', onkeyupFunction);
$('#fld_order_item_price').on('keyup', onkeyupFunction);
$('#fld_order_item_vat').on('keyup', onkeyupFunction);

