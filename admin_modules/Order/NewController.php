<?php
namespace AdminModules\Order;

use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\CrudViewManager;
use Crud\Customer\CrudCustomerOrderManager;
use Helper\FilterHelper;
use Helper\SaleOrderHelper;
use Model\Company\Company;
use Model\Company\CompanyQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerAddressQuery;
// use Model\Crm\CustomerAddress;
// use Model\Crm\CustomerAddressTypeQuery;
use Model\Crm\CustomerQuery;
use Model\Cms\SiteQuery;
use Model\Sale\SaleOrder;
use Model\Setting\CrudManager\CrudView;
use Exception\LogicException;
use Model\Setting\MasterTable\Base\SaleOrderStatusQuery;

class NewController extends MainController
{
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
    }
    function run()
    {
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $aPost = $this->post();

        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';
        $iCurrentPage = $this->get('p') ? $this->get('p') : 1;

        $oCustomerQuery = CustomerQuery::create();
        $oCustomerQuery->orderBy($sSortField, $sSortDirection);
        $oCrudUserManager = new CrudCustomerOrderManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudUserManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        // $oCustomerQuery = FilterHelper::generateTabFilters($oCustomerQuery, $iTabId);

        $oCustomerQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oCustomerQuery, $aFilters);
        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $iTabId);

        $oCrudUserManager->setOverviewData($oCustomerQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aViewData['overview_table'] = $oCrudUserManager->getOverviewHtml($aFields);
        $aViewData['title'] = Translate::fromCode('Voor welke klant wilt u een order aanmaken?');
        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Customer',
            'manager' => 'CustomerOrderManager',
            'overview_url' => $oCrudUserManager->getOverviewUrl(),
            'new_url' => $oCrudUserManager->getCreateNewUrl(),
            'new_title' => $oCrudUserManager->getNewFormTitle()
        ];
        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView =
            [
                'title' => 'Klant kiezen',
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];
        return $aView;
    }
    function doSelectCustomerAddresses()
    {
        /*
         * 1. Als de klant maar een adres heeft van gebruiken we die voor zowel shipping als billing.
         * 2. Als de klant twee adressen heeft dan pakken we de shipping voor de shipping en de billing voor de billing.
         * 3. Als de klant meerdere adressen heeft dan moet er gekozen worden.
         * 4. Als de klant geen adressen heeft dan moeten die eerst ingevuld worden.
         */
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        // $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);

        $oCustomerAddressDelivery = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'delivery');
        $oCustomerAddressInvoice = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'invoice');
        $oCustomerAddressGeneral = CustomerAddressQuery::create()->getByCustomerIdTypeCode($iCustomerId, 'general');


        $aUrlParts = [];
        $aUrlParts['customer_id'] = $iCustomerId;
        // Afleveradres bepalen
        if($oCustomerAddressDelivery->count() >= 2)
        {
            $aUrlParts['delivery_address_id'] = 'select_one';
        }
        else if($oCustomerAddressDelivery->count() == 1)
        {
            $aUrlParts['delivery_address_id'] = $oCustomerAddressDelivery->findOne()->getId();
        }
        else if($oCustomerAddressDelivery->count() == 0 && $oCustomerAddressGeneral->count() == 1)
        {
            $aUrlParts['delivery_address_id'] = $oCustomerAddressGeneral->findOne()->getId();
        }
        else if($oCustomerAddressGeneral->count() >= 2)
        {
            $aUrlParts['delivery_address_id'] = 'select_one';
        }
        else if($oCustomerAddressDelivery->count() == 0)
        {
            $aUrlParts['delivery_address_id'] = 'create_one';
        }
        else
        {
            // Ok, ik dacht dat we alles gedekt hadden. Als het script hier komt dan zit er een lek in het universum of ik heb iets over het hoofd gezien.
            throw new LogicException("For some kind of reason we where unable to select a billing address when creating a new order, please contact the administrator, please tell him it was customer $iCustomerId.");
        }

        // Factuuradres bepalen
        if($oCustomerAddressInvoice->count() >= 2)
        {
            $aUrlParts['invoice_address_id'] = 'select_one';
        }
        else if($oCustomerAddressInvoice->count() == 1)
        {
            $aUrlParts['invoice_address_id'] = $oCustomerAddressInvoice->findOne()->getId();
        }
        else if($oCustomerAddressInvoice->count() == 0 && $oCustomerAddressGeneral->count() == 1)
        {
            $aUrlParts['invoice_address_id'] = $oCustomerAddressGeneral->findOne()->getId();
        }
        else if($oCustomerAddressGeneral->count() >= 2)
        {
            $aUrlParts['invoice_address_id'] = 'select_one';
        }
        else if($oCustomerAddressInvoice->count() == 0)
        {
            $aUrlParts['invoice_address_id'] = 'create_one';
        }
        else
        {
            // Ok, ik dacht dat we alles gedekt hadden. Als het script hier komt dan zit er een lek in het universum of ik heb iets over het hoofd gezien.
            throw new LogicException("For some kind of reason we where unable to select a shipping address when creating a new order, please contact the administrator, please tell him it was customer $iCustomerId.");
        }

        $this->redirect('/order/addresses?'.http_build_query($aUrlParts));
    }
    function doCreateOrderFromCustomer()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');

        $iDeliveryAddressId = $this->get('delivery_address_id', null, true, 'numeric');
        $iInvoiceAddressId = $this->get('invoice_address_id', null, true, 'numeric');

        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        if(!$oCustomer instanceof Customer)
        {
            throw new LogicException("Klant niet gevonden");
        }

        $oSaleOrder = new SaleOrder();
        $oSaleOrder = SaleOrderHelper::addCustomerToSaleOrder($oSaleOrder, $iCustomerId);

        $oSaleOrder = SaleOrderHelper::addDeliveryAddressToSaleOrder($oSaleOrder, $iDeliveryAddressId);
        $oSaleOrder = SaleOrderHelper::addInvoiceAddressToSaleOrder($oSaleOrder, $iInvoiceAddressId);

        $oSaleOrder->setEnvelopeWeightGrams(Setting::get('envelope_weight_grams'));
        $oSaleOrder->setInvoiceNumber('proforma');
        $oSaleOrder->setOrderNumber('proforma');

        $oPayterm = $oCustomer->getPayTerm();
        if($oPayterm)
        {
            $oSaleOrder->setPaytermOriginalId($oPayterm->getId());
            $oSaleOrder->setPaytermString($oPayterm->getName());
        }

        $oCurrentUser = User::getMember();
        $oSaleOrder->setHandledBy($oCurrentUser->getFirstName().' '.$oCurrentUser->getLastName());

        //@todo er moet een is_default kolom worden toegevoegd aan de site tabel en deze moet de default site ophalen.
        $oSite = SiteQuery::create()->findOneByDomain('partners.subb4.nl');

        if($oSite instanceof \model\Cms\Site)
        {
            $oSaleOrder->setSiteId($oSite->getId());
        }

        $oCurrentUser = User::getMember();
        $oSaleOrder->setCustomerOrderPlacedBy($oCurrentUser->getFirstName().' '.$oCurrentUser->getLastName());

        $oCompany = CompanyQuery::create()->findOneByIsDefaultSupplier(true);
        if($oCompany instanceof Company)
        {
            $oSaleOrder = SaleOrderHelper::addOwnCompanyToSaleOrder($oSaleOrder, $oCompany);
        }

        $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneByCode('new');
        if($oSaleOrderStatus)
        {
            $oSaleOrder->setSaleOrderStatusId($oSaleOrderStatus->getId());
        }
        if($oSaleOrder->getOrderNumber() == 'proforma'){
            $oCompany = CompanyQuery::create()->findOneByIsDefaultSupplier(1);
            $oSaleOrder = SaleOrderHelper::generateOrderNumber($oCompany, $oSaleOrder);
        }

        $oSaleOrder->save();

        StatusMessage::success('Nieuwe blanco order aangemaakt voor '.$oCompany->getCompanyName().'.');
        $this->redirect('/order/summary?order_id='.$oSaleOrder->getId());
    }
}