<?php
namespace AdminModules\Order\Cash_and_delivery;

use Core\Currency;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\PaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery;

class EditController extends MainController
{

    function doStore()
    {
        $sR = $this->get('r', null, false);
        $aData = $this->post('data');

        $fShippingPrice = Currency::formatForDb($aData['shipping_price']);
        $iShippingMethodId = $aData['shipping_method'];
        $iPayMethodId = $aData['pay_method'];
        $iSaleOrderId = $this->get('order_id', null, true, 'numeric');

        $oShippingMethod = ShippingMethodQuery::create()->findOneById($iShippingMethodId);
        $oPayMethod = PaymentMethodQuery::create()->findOneById($iPayMethodId);
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iSaleOrderId);

        if($oShippingMethod instanceof ShippingMethod && $iShippingMethodId)
        {
            $oShippingMethod = ShippingMethodQuery::create()->findOneById($iShippingMethodId);
            $oSaleOrder->setShippingPrice($fShippingPrice);
            $oSaleOrder->setShippingHasTrackTrace($oShippingMethod->getIsTrackTrace());
            $oSaleOrder->setShippingCarrier($oShippingMethod->getName());
            $oSaleOrder->setShippingMethodId($oShippingMethod->getId());
            $oSaleOrder->save();
        }
        if($oPayMethod instanceof PaymentMethod)
        {
            $oSaleOrder->setPaymethodCode($oPayMethod->getCode());
            $oSaleOrder->setPaymethodName($oPayMethod->getName());
            $oSaleOrder->setPaymethodId($oPayMethod->getId());
            $oSaleOrder->save();
        }
        StatusMessage::success(Translate::fromCode("Verzend en betaalmethode toegevoegd."));
        if($sR)
        {
            $sUrl = DeferredAction::get($sR);
        }
        else
        {
            $sUrl = "/order/completed/status?order_id=$iSaleOrderId";
        }
        $this->redirect($sUrl);
    }
    function run()
    {
        $iSaleOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iSaleOrderId);
        $sNote = $oSaleOrder->getCustomerOrderNote();
        if($sNote)
        {
            StatusMessage::warning($sNote, Translate::fromCode('Opmerking klant'));
        }
        if($oSaleOrder->getItemDeleted())
        {
            StatusMessage::danger(Translate::fromCode("Deze bestelling is geannuleerd."));
        }
        else if($oSaleOrder->getHasWrap())
        {
            StatusMessage::warning(Translate::fromCode("De klant wil een kado verpakking."));
        }
        if(!$oSaleOrder->getCustomerDeliveryCountry())
        {
            $this->redirect("/order/cash_and_delivery/whoops?order_id=$iSaleOrderId");
        }
        $aShippingMethods = CountryShippingMethodQuery::getShippingMethodsByCountryName($oSaleOrder->getCustomerDeliveryCountry(), null);
        $aPaymentMethods = PaymentMethodQuery::create()->find();

        $sOverviewBackUrl = DeferredAction::get('order_overview_back_url');
        $aTopNavVars = [
            'overview_back_url' => $sOverviewBackUrl,
            'shipping_active' => 'active',
            'order_id' => $iSaleOrderId
        ];
        $aContentVars = [
            'shipping_methods' => $aShippingMethods,
            'payment_methods' => $aPaymentMethods,
            'sale_order' => $oSaleOrder,
            'id' => $iSaleOrderId
        ];
        $aVars = [
            'title' => Translate::fromCode('Paymethod and shipping'),
            'content' => $this->parse('Order/Cash_and_delivery/edit.twig', $aContentVars),
            'top_nav' => $this->parse('Order/top_nav_edit.twig', $aTopNavVars),
        ];
        return $aVars;
    }
}