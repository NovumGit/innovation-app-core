<?php
namespace AdminModules\Order\Cash_and_delivery;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Exception\LogicException;
use Model\Sale\SaleOrderQuery;

class WhoopsController extends MainController
{
    function run()
    {
        $iSaleOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iSaleOrderId);

        $aTopNavVars = [
            'shipping_active' => 'active',
            'order_id' => $iSaleOrderId
        ];

        DeferredAction::register('cash_and_delivery', "/order/cash_and_delivery/edit?order_id=$iSaleOrderId");
        $sUrl = "/order/edit/edit?order_id=$iSaleOrderId&view=delivery_address&r=cash_and_delivery";
        $sTitle = Translate::fromCode("Geen afleverland gespecificeerd.");
        $sText = Translate::fromCode("De verzendkosten worden berekend op basis van het land waar de goederen moeten worden afgeleverd. Omdat het land niet is ingevuld kan het systeem nu geen berekening maken.");
        $aStatusMessageButtons = [];
        $aStatusMessageButtons[] = new StatusMessageButton(Translate::fromCode("Afleverland instellen"), $sUrl, "instellen", 'info');
        StatusModal::danger($sText, $sTitle, $aStatusMessageButtons);

        if(!$oSaleOrder->getCustomerDeliveryCountry())
        {
            $aVars = [
                'title' => Translate::fromCode('No delivery country specified'),
                'content' => '', //$this->parse('Order/Cash_and_delivery/whoops.twig', []),
                'top_nav' => $this->parse('Order/top_nav_edit.twig', $aTopNavVars),
            ];
            return $aVars;
        }
        throw new LogicException("This error is not supported yet.");

    }
}