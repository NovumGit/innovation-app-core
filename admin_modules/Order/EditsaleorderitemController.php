<?php
namespace AdminModules\Order;
use Core\MainController;
use Core\StatusMessage;
use Crud\Sale_order_item_product\CrudSale_order_item_productManager;
use Helper\EditviewHelper;
use Model\Sale\OrderItemProductQuery;

class EditsaleorderitemController extends MainController
{
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->oCrudManager = new CrudSale_order_item_productManager();
    }

    function run(){

        $sTitle = "Product bewerken";
        $sEditConfigKey = 'sale_order_item_product_editor';
        $mData = $this->post('data', null);
        $mData['order_product_id'] = $this->get('order_product_id');
        $oSaleOrderItem = $this->oCrudManager->getModel(['id' => $this->get('order_product_id')]);

        $oSaleOrder = OrderItemProductQuery::create()->findOneById($mData['order_product_id']);

        // Maak iets aan als er nog helemaal geen weergave is.
        EditviewHelper::loadEditorByName($this->oCrudManager, $sEditConfigKey, $sTitle);

        $aViewData = [
            'order_product_id' => $oSaleOrder->getId(),
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oSaleOrderItem, $sEditConfigKey)
        ];

        $aTopNavVars = [
            'module' => 'Sale_order_item_product',
            'manager' => 'Sale_order_item_productManager',
            'name' => $sEditConfigKey,
            'overview_url' => $this->oCrudManager->getOverviewUrl(),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle(),
            'id' => $this->get('order_id', null, true)
        ];

        $sTopNav = $this->parse('top_nav_edit.twig', $aTopNavVars);
        $sOverview = $this->parse('Order/editsaleorderitem.twig', $aViewData);

        $aView = [
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];

        return $aView;
    }

    function doStore()
    {
        $aSaleOrderItemProduct = $this->post('data', null);

        if(!empty($aSaleOrderItemProduct))
        {
            foreach($aSaleOrderItemProduct as $sFieldName => $sFieldValue)
            {
                if(strpos($sFieldName, 'opt_') === 0)
                {

                }
            }
        }


        if($this->oCrudManager->isValid($aSaleOrderItemProduct))
        {
            StatusMessage::success("Wijzigingen opgeslagen");
            $this->oCrudManager->save($aSaleOrderItemProduct);
            $this->redirect('/order/summary?order_id='.$this->get('order_id', null, true));
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aSaleOrderItemProduct);
        }
    }
}
