<?php
namespace AdminModules\Order\Completed;

class TimelineItem{

    private $sType;
    private $sPos;
    private $sIcon;
    private $iTimestamp;
    private $sTitle;
    private $sMessage;

    /**
     * TimelineItem constructor.
     * @param string $sType [info, success, alert, warning, danger]
     * @param string $sPos [left, right]
     * @param integer $sIcon
     * @param string $iTimestamp
     * @param string $sTitle
     * @param string $sMessage
     */
    function __construct(string $sType, string $sPos, string $sIcon, $iTimestamp, string $sTitle, string $sMessage = null)
    {
        $this->sType = $sType;
        $this->sPos = $sPos;
        $this->sIcon = $sIcon;
        $this->iTimestamp = $iTimestamp;
        $this->sTitle = $sTitle;
        $this->sMessage = $sMessage;
    }
    function getTitle()
    {
        return $this->sTitle;
    }
    function getType()
    {
        return $this->sType;
    }
    function getPos()
    {
        return $this->sPos;
    }
    function getIcon()
    {
        return $this->sIcon;
    }
    function getDateTime()
    {
        $oDate = new \DateTime();
        $oDate->setTimestamp($this->iTimestamp);
        return $oDate;
    }
    function getTimeAgo()
    {
        return $this->getTimeAgoInt($this->iTimestamp);
    }
    private function getTimeAgoInt($tm,$rcs = 0)
    {
        $cur_tm = time();
        $dif = $cur_tm-$tm;
        $pds = array('second','minute','hour','day','week','month','year','decade');
        $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);

        for($v = sizeof($lngh)-1; ($v >= 0)&& (($no = $dif/$lngh[$v])<=1); $v--);
            if($v < 0)
            {
                $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);
            }

        $no = floor($no);
        if($no <> 1)
            $pds[$v] .='s';
        $x = sprintf("%d %s ",$no,$pds[$v]);
        if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0))
            $x .= $this->getTimeAgoInt($_tm);
        return $x;
    }
}

