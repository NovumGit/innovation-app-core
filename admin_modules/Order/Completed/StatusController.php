<?php
namespace AdminModules\Order\Completed;

use AdminModules\Order\Event\Push_to_reeleezee;
use Core\AbstractEvent;
use Core\Currency;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\Sale_order_step\AbstractSale_order_step_field;
use Crud\Sale_order_step\CrudSale_order_stepManager;
use Exception\LogicException;
use Model\Crm\CustomerNote;
use Model\Crm\CustomerNoteQuery;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Model\Setting\MasterTable\Sale_order_notification_type;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class StatusController extends MainController
{
    function doPushToReeleezee()
    {
        StatusMessage::success(Translate::fromCode("Order is naar Reeleezee gepushed"));
        $iOrderId = $this->get('order_id', null, true, 'numeric');

        $aGet = [
            'order_id' => $iOrderId,
            'skip_filter' => false
        ];

        (new Push_to_reeleezee($aGet, []))->trigger('edit');
        $this->redirect('/order/completed/status?order_id='.$iOrderId);
    }
    function doStoreNote()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $oCurrentUser = User::getMember();

        $sDescription = $this->post('description');

        if(empty($sDescription))
        {
            StatusMessage::warning(Translate::fromCode("Uw notitie is niet opgelsagen want de notitie heeft geen inhoud."));
        }
        else
        {
            $oCustomerNote = new CustomerNote();
            $oCustomerNote->setCustomerId($oSaleOrder->getCustomerId());
            $oCustomerNote->setCreatedByUserId($oCurrentUser->getId());
            $oCustomerNote->setDescription($sDescription);
            $oCustomerNote->save();
            StatusMessage::success("Uw notitie is opgeslagen.");
            $this->redirect($this->getRequestUri());
            exit();
        }
        StatusMessage::success(Translate::fromCode("Klant notitie toegevoegd."));
        $this->redirect('/order/completed/status?order_id='.$iOrderId);
    }
    /*
    function doRoundUp()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $oSaleOrder->setIs
    }
    */
    function doCancel(bool $bStatus = true)
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $oSaleOrder->setItemDeleted($bStatus);
        $oSaleOrder->save();
        $this->redirect('/order/completed/status?order_id='.$iOrderId);
    }
    function doTriggerEvent()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $sEvent = $this->get('event');
        $aArguments = $this->get('event_argument');
        $sFqEventName ='AdminModules\\Order\\Event\\'.$sEvent;


        $oEvent = new $sFqEventName($aArguments, []);

        if(!$oEvent instanceof AbstractEvent)
        {
            throw new LogicException("Expected an instance of Abstract event");
        }
        $oEvent->trigger('edit');
        $this->redirect("/order/completed/status?order_id=$iOrderId");
    }
    function doUnCancel()
    {
        $this->doCancel(false);
    }
    function run()
    {
        // $this->addCssFile('/admin_modules/Order/Completed/status.css');

        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $sNote = $oSaleOrder->getCustomerOrderNote();
        if($sNote)
        {
            StatusMessage::warning($sNote, Translate::fromCode('Opmerking klant'));
        }
        // wordt ook vanuit Crud velden gebruikt
        DeferredAction::register('return_order_summary', $this->getRequestUri());

        $aTodolistItems =  $this->getTodoList($oSaleOrder, 'Checklist');
        $aSaleOrderTimeline = $this->getSaleOrderTimeline($oSaleOrder);

        $bAllDone = true;
        if(!empty($aTodolistItems))
        {
            foreach($aTodolistItems as $oAbstractTodoItem)
            {
                if($oAbstractTodoItem instanceof AbstractSale_order_step_field)
                {
                    if(!$oAbstractTodoItem->isDone())
                    {
                        $bAllDone = false;
                    }
                }
            }
        }

        $aCustomerNotes = CustomerNoteQuery::create()->filterByCustomerId($oSaleOrder->getCustomerId());
        $aCustomerNotes->orderByCreatedOn(Criteria::DESC);

        if($oSaleOrder->getItemDeleted())
        {
            StatusMessage::danger(Translate::fromCode("Deze bestel  ling is geannuleerd."));
        }
        else if($oSaleOrder->getHasWrap())
        {
            StatusMessage::warning(Translate::fromCode("De klant wil een kado verpakking."));
        }

        $aContentVars =
        [
            'sale_order_timeline' => $aSaleOrderTimeline,
            'customer_notes' => $aCustomerNotes->find(),
            'all_done' => $bAllDone,
            'sale_order' => $oSaleOrder,
            'todo_items' => $aTodolistItems
        ];
        $aTopNavVars = [
            'order_id' => $iOrderId,
            'controller' => 'status_controller',
            'edit_config_key' => 'order_statusses',

            'todo_module' => 'Sale_order_step',
            'todo_manager' => 'Sale_order_stepManager',
            'todo_edit_config_key' => 'Checklist',

            'title' => Translate::fromCode('Te doorlopen orderstatussen'),
            'manager' => 'Sale_orderManager',
            'module' => 'Sale_order',
            'status_active' => 'active',
            'overview_back_url' => DeferredAction::get('order_overview_back_url')
        ];
        $aVars = [
            'title' => Translate::fromCode('Samenvatting'),
            'content' => $this->parse('Order/Completed/status.twig', $aContentVars),
            'top_nav' => $this->parse('Order/top_nav_edit.twig', $aTopNavVars),
        ];
        return $aVars;
    }

    protected function getSaleOrderTimeline(SaleOrder $oSaleOrder)
    {
        $aStatusTimeline = [];
        $iTs = $oSaleOrder->getCreatedOn('U');

        if($oSaleOrder->getSource() == 'webshop')
        {
            $sSourceTxt = 'The customer created the order';
            $sSide = 'left';
            $sBy = 'customer';
        }
        else if($oSaleOrder->getSource() == 'intern')
        {
            $sSourceTxt = 'We created this order using the admin panel';
            $sSide = 'right';
            $sBy = 'us';
        }
        else
        {
            throw new LogicException("We don't know that source.");
        }
        $aStatusTimeline[$iTs.'_order_created'] = new TimelineItem('warning', $sSide, 'file-o', $iTs, 'Order created by '.$sBy, $sSourceTxt);

        $aSaleOrderNotifications = SaleOrderNotificationQuery::create()->filterBySaleOrderId($oSaleOrder->getId());

        foreach($aSaleOrderNotifications as $oSaleOrderNotification)
        {
            if(!$oSaleOrderNotification instanceof SaleOrderNotification)
            {
                throw new LogicException("\$oSaleOrderNotification  should be an instance SaleOrderNotification");
            }
            $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneById($oSaleOrderNotification->getSaleOrderNotificationTypeId());

            if(!$oSale_order_notification_type instanceof Sale_order_notification_type)
            {
                throw new LogicException("\$oSale_order_notification_type  should be an instance Sale_order_notification_type");
            }
            $iTs = $oSaleOrderNotification->getDateSent('U');
            $aStatusTimeline[$iTs.$oSale_order_notification_type->getCode()] = new TimelineItem('info', 'right', 'envelope', $iTs, $oSale_order_notification_type->getName(), null);
        }

        if($oSaleOrder->isFullyPaid())
        {
            $iTs = $oSaleOrder->getPayDate('U');
            $fPrice = Currency::formatForPresentation($oSaleOrder->getTotalPrice());
            $aStatusTimeline[$iTs.'_pay_date'] = new TimelineItem('success', 'left', 'money', $iTs, 'Order paid &euro;'.$fPrice, 'Customer paid for the order');
        }
        if($oSaleOrder->isFullyShipped())
        {
            $iTs = $oSaleOrder->getShipDate('U');
            $aStatusTimeline[$iTs.'_pay_date'] = new TimelineItem('warning', 'right', 'truck', $iTs, 'Order shipped', 'The order is on the way');
        }


        ksort($aStatusTimeline);
        $aStatusTimeline = array_reverse($aStatusTimeline);
        return $aStatusTimeline;

    }
    protected function getTodoList(SaleOrder $oSaleOrder, $sConfigKey)
    {

        $oCrudSale_order_stepManager = new CrudSale_order_stepManager();
        $oCrudSale_order_stepManager->setArgument('sale_order', $oSaleOrder);
        $aTodoList = $oCrudSale_order_stepManager->getLinkArray($sConfigKey);

        return $aTodoList;
    }
}