var oTableForm = $('#frm_table_form');
var oSaleOrderCheckboxes = $('.sale_order_checkbox');
var oCustomButtons = $('.custom_bottom');
var oCustomButtonContainer = $('.custom_bottom_container');

oSaleOrderCheckboxes.change(function(e)
{
    e.preventDefault();

    if($('.sale_order_checkbox:checked').length > 0)
    {
        oCustomButtonContainer.show(500);
    }
    else
    {
        oCustomButtonContainer.hide(500);
    }
});

oCustomButtons.click(function(e)
{
    $('#fld_button_id').val($(this).val());
    e.preventDefault();
    $('#fld_do').val('TriggerButtonEvent');
    oTableForm.submit();
});

var oToggleCheckboxButton = $('.toggle_check');

oToggleCheckboxButton.click(function(e){
    e.preventDefault();
    var aCheckboxes = $('.sale_order_checkbox');
    console.log('a');
    aCheckboxes.each(function(i, e){
        console.log($(e));

        $(e).prop('checked', !$(e).is(':checked'));
    });
});
console.log('loaded');
