<?php
namespace AdminModules\Order\Packing_slip;

use Core\Config;
use Core\MainController;
use Knp\Snappy\Pdf;
use Model\Sale\SaleOrderQuery;

class Packing_slipController extends MainController
{
    function getImgBaseUrl(){
        return (isset($_SERVER['IS_DEVEL'])) ? 'http://admin.femaleseeds.nuidev.nl/' : 'https://admin.femaleseeds.nl/';
    }

    function run()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $sFormat = $this->get('format', 'html', false);

        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);

        $aData = [
            'sale_order' => $oSaleOrder
        ];

        $sInvoice = $this->parse('Order/Packing_slip/packing_slip.twig', $aData);

        if($sFormat == 'pdf')
        {
            $oSnappy = new Pdf();
            $oSnappy->setBinary(Config::getSnappyBinaryPath());
            $oSnappy->generateFromHtml($sInvoice, 'invoice.pdf', [], true);
            header('Content-Type: application/pdf');
            echo file_get_contents('invoice.pdf');
        }

        exit($sInvoice);
    }
}