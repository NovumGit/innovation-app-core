<?php
namespace AdminModules\Order;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Product\CrudProductOrderSelectorManager;
use Helper\FilterHelper;
use Helper\SaleHelper;
use Model\CustomerQuery;
use Model\Sale\OrderItemProductQuery;
use Model\Sale\SaleOrderItemQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\CrudManager\CrudView;
use Exception\LogicException;
use Model\Setting\MasterTable\Base\LanguageQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class Add_productController extends MainController
{
    function run()
    {
        $iCurrentPage = $this->get('p', 1, null, 'numeric');
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';
        $aFilters  = $this->get('filter', null);

        $oProductQuery = CustomerQuery::create();
        $oProductQuery->orderBy($sSortField, $sSortDirection);
        $oCrudProductOrderSelectorManager = new CrudProductOrderSelectorManager();

	    $aCrudViews = CrudViewManager::getViews($oCrudProductOrderSelectorManager);

        if(!$iTabId)
        {
            $oCrudView = current($aCrudViews);
            if(!$oCrudView instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudView));
            }
            $iTabId = $oCrudView->getId();
        }
        else
        {
            $oCrudView = $aCrudViews[$iTabId];
        }

        $oProductQuery = FilterHelper::applyFilters($oProductQuery, $iTabId);
        $oProductQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oProductQuery, $aFilters);

        if(!empty($aFilters))
       {
            foreach($aFilters as $sFilterName => $sFilterValue)
            {
                if(!is_numeric($sFilterName))
                {
                    $oProductQuery->filterBy($sFilterName, $sFilterValue, Criteria::EQUAL);
                }
            }
        }

        $sCrudReturnAction = 'return_after_crud_edit';
        DeferredAction::register($sCrudReturnAction, $this->getRequestUri());

        $oCrudProductOrderSelectorManager->setOverviewData($oProductQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudView);
        $aViewData['overview_table'] = $oCrudProductOrderSelectorManager->getOverviewHtml($aFields);

        $a['extra_hidden_fields']['order_id'] = $this->get('order_id');
        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId, $a);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId, // <- niet wijzigen, anders werkt de overview link niet meer goed.
            'module' => 'Product',
            'r' => $sCrudReturnAction,
            'manager' => 'ProductOrderSelectorManager',
            'overview_url' => $oCrudProductOrderSelectorManager->getOverviewUrl(),
            'new_url' => $oCrudProductOrderSelectorManager->getCreateNewUrl(),
            'new_title' => $oCrudProductOrderSelectorManager->getNewFormTitle()
        ];
        $sTopNav = $this->parse('Order/add_product_top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView =
            [
                'title' => Translate::fromCode("Add product to order"),
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];
        return $aView;
    }

	function doAddProduct()
	{
		$iProductId = $this->get('product_id', null, true);
		$iOrderId = $this->get('order_id', null, true);
		$oProduct = CustomerQuery::create()->findOneById($iProductId);
        $iQuantity =  $this->post('quantity', 1, false, 'numeric');

        $fSaleprice = $oProduct->getSalePrice();

        if($oProduct)
        {
            $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
            $oLanguage = LanguageQuery::create()->findOneById($oSaleOrder->getLanguageId());
		    SaleHelper::addProductToOrder($iProductId, $iOrderId, $fSaleprice, $iQuantity, $oLanguage);
        }

		$this->redirect('/order/summary?order_id='.$iOrderId);
	}

	function doRemoveProduct()
	{
		$iOrderId = $this->get('order_id', null, true);
		$iOrderItemId = $this->get('order_item_id', null, true);
		$oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($iOrderItemId);

        if(!$oSaleOrderItem->getManualEntry())
        {
		    OrderItemProductQuery::create()->findOneBySaleOrderItemId($oSaleOrderItem->getId())->delete();
        }
		$oSaleOrderItem->delete();
		$this->redirect('/order/summary?order_id='.$iOrderId);
	}
}