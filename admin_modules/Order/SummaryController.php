<?php
namespace AdminModules\Order;

use Core\ButtonEventDispatcher;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Core\Utils;
use Model\Sale\SaleOrderItem;
use Model\Sale\SaleOrderItemQuery;
use Model\Sale\SaleOrderQuery;

class SummaryController extends MainController {

    function doShowNote()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);

        $sReturnUrl = DeferredAction::get('after_show_note');
        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Sluiten'), $sReturnUrl, Translate::fromCode('Sluiten'), 'info')
        ];

        StatusModal::info($oSaleOrder->getCustomerOrderNote(), Translate::fromCode("Opmerking klant"), $aButtons);
        $this->redirect($sReturnUrl);
    }
    function doTriggerButtonEvent()
    {
        $iSaleOrderId = $this->get('order_id', null, true,  'numeric');
        $iButtonId = $this->get('button_id', null, true,  'numeric');

        $bEventHasOutput = ButtonEventDispatcher::dispatch($iButtonId, ['order_id' => $iSaleOrderId], 'edit');

        if(!$bEventHasOutput)
        {
            $sReturnUrl = DeferredAction::get('after_button_click');
            $this->redirect($sReturnUrl);
        }
    }
    function doAddManualProduct()
    {
        $iId = $this->get('order_id', null, true);

        $iDeleteId = $this->get('delete_item', null, true);
        if($iDeleteId)
        {
            SaleOrderItemQuery::create()->findOneById($iDeleteId)->delete();
        }

        $sDescription = $this->get('description', null, true);
        $iQuantity = $this->get('quantity', null, true);
        $iPrice = $this->get('price', null, true);
        $iVat = $this->get('vat', null, true);

        if(!$sDescription || !$iQuantity || !$iPrice || !$iVat){
            StatusMessage::warning("Vul alstublieft alle velden in voordat u het product toevoegd.");
            $this->redirect('/order/summary?order_id='.$iId);
            exit();
        }

        $oSaleOrderItem = new SaleOrderItem();
        $oSaleOrderItem->setCreatedOn(new \DateTime());
        $oSaleOrderItem->setSaleOrderId($iId);
        $oSaleOrderItem->setDescription($sDescription);
        $oSaleOrderItem->setQuantity(str_replace(',', '.', $iQuantity));
        $oSaleOrderItem->setPrice(str_replace(',', '.', $iPrice));
        $oSaleOrderItem->setVatPercentage($iVat);
        $oSaleOrderItem->setManualEntry(true);
        $oSaleOrderItem->save();

        StatusMessage::success("Product toegevoegd aan de order.");
        $this->redirect('/order/summary?order_id='.$iId);
    }

    function doDymoPrint()
    {
        $iOrderId = $this->get('order_id', null, true, 'numeric');

        $iOrderItemId =  $this->get('order_item_id', null, true, 'numeric');
        // $sJavascript = file_get_contents('../admin_modules/Order/inc/Dymo/print.js');

        $oSaleOrderItem = SaleOrderItemQuery::create()->findOneById($iOrderItemId);

        $sDescription = $oSaleOrderItem->getDescription();
        $aLines = explode( "\n", wordwrap( $sDescription, 25));


        $aVars = ['sale_order_item' => $oSaleOrderItem, 'desription_lines' => $aLines];
        $sJavascript = $this->parse('Order/inc/Dymo/product-job.twig.js', $aVars);


        echo '<script>';
        echo $sJavascript;
        echo "window.location = '/order/summary?order_id=".$iOrderId."'";
        echo '</script>';
        exit();
    }

    function run()
    {
        $iSaleOrderId = $this->get('order_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iSaleOrderId);

        $sNote = $oSaleOrder->getCustomerOrderNote();
        if($sNote)
        {
            StatusMessage::warning($sNote, Translate::fromCode('Opmerking klant'));
        }
        if($oSaleOrder->getItemDeleted())
        {
            StatusMessage::danger(Translate::fromCode("Deze bestelling is geannuleerd."));
        }
        else if($oSaleOrder->getHasWrap())
        {
            StatusMessage::warning(Translate::fromCode("De klant wil een kado verpakking."));
        }
        DeferredAction::register('summary', $this->getRequestUri());

        if(!$oSaleOrder->getCustomerDeliveryCountry())
        {
           StatusMessage::warning(Translate::fromCode("Bij het afleveradres is geen land opgegeven, geeft u alstublieft eerst een land op."));
        }
        if(!$oSaleOrder->getCustomerInvoiceCountry())
        {
            StatusMessage::warning(Translate::fromCode("Bij het factuuradres is geen land opgegeven, geeft u alstublieft eerst een land op."));
        }
        // $oSaleOrder->getCustome
        $sOverviewUrl = DeferredAction::get('order_overview_back_url');

        if(!$sOverviewUrl)
        {
            $sOverviewUrl = '/order/overview';
        }

        $aTopNavVars = [
            'summary_active' => 'active',
            'order_id' => $iSaleOrderId,
            'overview_back_url' => $sOverviewUrl
        ];

        $aContentVars = [
            'sale_order' => $oSaleOrder,
            'id' => $iSaleOrderId
        ];

        $aVars = [
            'title' => Translate::fromCode('Samenvatting'),
            'content' => $this->parse('Order/summary.twig', $aContentVars),
            'top_nav' => $this->parse('Order/top_nav_edit.twig', $aTopNavVars),
        ];
        return $aVars;
    }
}