<?php
namespace AdminModules\Order\Edit;
use AdminModules\Order\Event\Send_status_email;
use Core\Cfg;
use Core\DeferredAction;
use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\Sale_order\CrudSale_orderManager;
use Exception\LogicException;
use Helper\EditViewButtonHelper;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use AdminModules\Order\Event\GenericDelete_item;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\User\UserActivity;
use Model\User\UserActivityQuery;

class EditController extends MainController
{
    const sFormLayoutKey = 'base';
    private $oCrudManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        //$aOrder = $this->post('data', null);
        $this->oCrudManager = new CrudSale_orderManager();
    }

    function run()
    {
        // $_GET[r] is een verplicht veld.
        $this->get('r', null, true);
        $sView = $this->get('view', null, true);

        // Een view staat gelijk aan een veld configuratie, je kan per view dus andere velden op het scherm zetten.
        // Om wildgroei te voorkomen moet je de view hier wel even aan de array toevoegen.
        $aAllowedViews = [
            'invoice_address' => ['title' => Translate::fromCode('Factuuradres'), 'active' => 'active_summary'],
            'delivery_address' => ['title' => Translate::fromCode('Afleveradres'), 'active' => 'active_summary'],
            'track_trace' => ['title' => Translate::fromCode('Track en trace invoeren'), 'active' => 'active_summary'],
        ];

        $aOrderEditAllowdViews = Cfg::get('ORDER_EDIT_ALLOWED_VIEWS');

	    $aAllowedViews = array_merge($aAllowedViews, $aOrderEditAllowdViews);


        if(!in_array($sView, array_keys($aAllowedViews)))
        {
            throw new LogicException("This view is not allowed, to prevent proliferation of all kinds of views please make this view allowed programattically.");
        }
		else if(isset($aAllowedViews[$sView]['status_func']))
		{
			$aAllowedViews[$sView]['status_func']();
		}

        $sOverviewUrl = DeferredAction::get('order_overview_back_url');
        $aSaleOrder = $this->post('data', null);
        $aSaleOrder['id'] = $this->get('order_id');

        $oSaleOrder = SaleOrderQuery::create()->findOneById($aSaleOrder['id']);

        if($oSaleOrder->getItemDeleted())
        {
            StatusMessage::danger(Translate::fromCode("This order has been cancelled"));
        }
        if(!$oSaleOrder instanceof SaleOrder)
        {
            StatusMessage::warning(Translate::fromCode("Kon geen order met id ").$aSaleOrder['id'].Translate::fromCode(" vinden, heeft u een oude url ingetypt?"));
            $this->redirect('/order/overview');
        }

        $oSaleOrder = $this->oCrudManager->getModel($aSaleOrder);

        $aParseData = [
            $aAllowedViews[$sView]['active'] => 'active',
            'edit_form' => $this->oCrudManager->getRenderedEditHtml($oSaleOrder, $sView),
            'title' => $aAllowedViews[$sView]['title'],
            'summary' => ""
        ];
        $sEdit = $this->parse('edit.twig', $aParseData);

        DeferredAction::register('after_button_click', $this->getRequestUri());
        $aButtons = EditViewButtonHelper::getViewButtons('Crud\\Sale_order\\CrudSale_orderManager', $sView);

        $aTopnavEdit = [
            'buttons' => $aButtons,
            'module' => 'Sale_order',
            'manager' => 'Sale_orderManager',
            'overview_back_url' => $sOverviewUrl,
            'edit_config_key' => $sView,
            'edit_view_title' => $aAllowedViews[$sView]['title'],
            'order_id' => $aSaleOrder['id']
        ];

        $sTopNav = $this->parse('Order/top_nav_edit.twig', $aTopnavEdit);
        $aView = [
            'top_nav' => $sTopNav,
            'title' => $aAllowedViews[$sView]['title'],
            'content' => $sEdit
        ];
        return $aView;
    }
    /**
     * @action de order wordt uit de prullenbak gehaald
     */
    function doUndelete()
    {
        StatusMessage::success("Product terug gezet.");
        $oProduct = $this->oCrudManager->getModel(['id' => $this->get('id')]);
        $oProduct->setItemDeleted(0)->save();
        $this->redirect('/product/overview');
    }
    /**
     * @action de order wordt verwijderd
     */
    function doDelete()
    {
        $iOrderId = $this->get('order_id');
        $oUser = User::getMember();
        LogActivity::register('Order', 'delete', $oUser->getFirstName().' heeft order ' . $iOrderId . ' verwijderd.');

        StatusMessage::info(Translate::fromCode("Bestelling verwijderd."));

        // Trigger een delete event

        (new GenericDelete_item($_GET, $_POST))->trigger('edit');

        $this->oCrudManager->getModel(['id' => $iOrderId])->setItemDeleted(1)->save();

        $iTabId = $this->get('tab');

        $sAddUrl = '';
        if($iTabId)
        {
            $sAddUrl = '?tab='.$iTabId;
        }

        $this->redirect($this->oCrudManager->getOverviewUrl().$sAddUrl);
    }

    /**
     * @action de order wordt opgeslagen
     */
    function doStore()
    {
        $sView = $this->get('view', null, true);
        $aSaleOrder = $this->post('data');

        if(preg_match('/^(invoice|delivery)_address$/', $sView))
        {
            // Ask the user is we should store the address changes also in the customer_addressa table.
            $aSaleOrder = (new CustomerAddressHelper($_GET, $_POST))->dialogShouldWeStoreAddress();
        }

        if($this->oCrudManager->isValid($aSaleOrder, $sView))
        {

            $sR = $this->get('r', null, true);
            StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
            $this->oCrudManager->save($aSaleOrder);

            if($sView == 'track_trace')
            {
                $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneByCode('track_and_trace');
                $aGet['order_id'] = $this->get('order_id', null, true);
                $aGet['notification_type_id'] = $oSale_order_notification_type->getId();
                (new Send_status_email($aGet, []))->trigger('edit');
            }
            $iTabId = $this->get('tab');
            $sAddUrl = '';
            if($iTabId)
            {
                $sAddUrl = '&tab='.$iTabId;
            }
            if($this->post('next_overview') == 1)
            {
                $sOverviewUrl = DeferredAction::get('order_overview_back_url').$sAddUrl;
                $this->redirect($sOverviewUrl);
            }
            else
            {
                $sRedirect = DeferredAction::get($sR);
                $this->redirect($sRedirect);
            }
        }
        else
        {
            $this->oCrudManager->registerValidationErrorStatusMessages($aSaleOrder);
        }
    }
}
