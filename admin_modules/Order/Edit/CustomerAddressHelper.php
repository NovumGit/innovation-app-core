<?php
namespace AdminModules\Order\Edit;

use Core\Base;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Exception\LogicException;
use Model\Setting\MasterTable\Country;
use Model\Crm\CustomerAddressQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\UsaState;
use Model\Setting\MasterTable\UsaStateQuery;

class CustomerAddressHelper extends Base
{
    function dialogShouldWeStoreAddress()
    {
        $sView = $this->get('view');
        $iSaleOrderId = $this->get('order_id');
        $sCopyAddressConfirm = $this->get('copy_address_confirm');

        if(isset($_GET['copy_address_confirm']))
        {
            $aSaleOrder = $_SESSION['tmp_sale_order'];
        }
        else
        {
            $aSaleOrder = $this->post('data');
        }

        $bCurrentViewIsAddress = preg_match('/^(invoice|delivery)_address$/', $sView, $aMatches);

        $sAddressIdGetter = 'getCustomer'.ucfirst($aMatches[1]).'AddressId';
        $oSaleOrder = SaleOrderQuery::create()->findOneById($aSaleOrder['id']);


        if(!isset($_GET['copy_address_confirm']) && $bCurrentViewIsAddress && $oSaleOrder->$sAddressIdGetter())
        {
            $_SESSION['tmp_sale_order'] = $aSaleOrder;

            $aButtons = [];
            $sBaseUrl = "/order/edit/edit?order_id={$iSaleOrderId}&view=$sView&r=summary&_do=Store";
            $sAcceptUrl = "{$sBaseUrl}&copy_address_confirm=1";
            $sDeclineUrl = "{$sBaseUrl}&copy_address_confirm=0";

            DeferredAction::register('decline_url', $sDeclineUrl);

            $aButtons[] = new StatusMessageButton(Translate::fromCode("Ja graag!"), $sAcceptUrl, '','success');
            $aButtons[] = new StatusMessageButton(Translate::fromCode("Nee"), $sDeclineUrl, '','warning');

            $sAddressType = ($aMatches[1] == 'invoice') ? Translate::fromCode("factuur") : Translate::fromCode("aflever");
            $sModalTxt = Translate::fromCode("U heeft zojuist het")." ".$sAddressType." ".Translate::fromCode("adres van de klant gewijzigd, wilt u deze wijziging ook in het adresboek van de klant doorvoeren?");
            $sModalTitle = Translate::fromCode("Zullen we dit gelijk ook regelen?");
            StatusModal::info($sModalTxt, $sModalTitle, $aButtons);

            $this->redirect('/order/edit/pleaseconfirm');
            exit();
        }
        else if ($sCopyAddressConfirm == '1')
        {

            if($aMatches[1] == 'invoice')
            {
                $oCustomerAddress = CustomerAddressQuery::create()->findOneById($oSaleOrder->getCustomerInvoiceAddressId());

                if(isset($aSaleOrder['customer_invoice_company_name'])) { $oCustomerAddress->setCompanyName($aSaleOrder['customer_invoice_company_name']); }
                if(isset($aSaleOrder['customer_invoice_attn_name'])) { $oCustomerAddress->setAttnName($aSaleOrder['customer_invoice_attn_name']); }
                if(isset($aSaleOrder['customer_invoice_address_l1'])) { $oCustomerAddress->setAddressL1($aSaleOrder['customer_invoice_address_l1']); }
                if(isset($aSaleOrder['customer_invoice_address_l2'])) { $oCustomerAddress->setAddressL2($aSaleOrder['customer_invoice_address_l2']); }
                if(isset($aSaleOrder['customer_invoice_street'])) { $oCustomerAddress->setStreet($aSaleOrder['customer_invoice_street']); }
                if(isset($aSaleOrder['customer_invoice_number'])) { $oCustomerAddress->setNumber($aSaleOrder['customer_invoice_number']); }
                if(isset($aSaleOrder['customer_invoice_number_add'])) { $oCustomerAddress->setNumberAdd($aSaleOrder['customer_invoice_number_add']); }
                if(isset($aSaleOrder['customer_invoice_postal'])) { $oCustomerAddress->setPostal($aSaleOrder['customer_invoice_postal']); }
                if(isset($aSaleOrder['customer_invoice_city'])) { $oCustomerAddress->setCity($aSaleOrder['customer_invoice_city']); }

                if(isset($aSaleOrder['customer_invoice_country']))
                {
                    $oCountry = CountryQuery::create()->findOneByName($aSaleOrder['customer_invoice_country']);
                    if($oCountry instanceof Country)
                    {
                        $oCustomerAddress->setCountryId($oCountry->getId());
                    }
                    else
                    {
                        $oCustomerAddress->setCountryId(null);
                    }
                }
                if(isset($aSaleOrder['customer_invoice_usa_state']))
                {
                    $oUsaState = UsaStateQuery::create()->findOneByName($aSaleOrder['customer_invoice_usa_state']);
                    if($oUsaState instanceof UsaState)
                    {
                        $oCustomerAddress->setUsaStateId($oUsaState->getId());
                    }
                    else
                    {
                        $oCustomerAddress->setUsaStateId(null);
                    }
                }
                $oCustomerAddress->save();
                StatusMessage::success(Translate::fromCode("Factuuradres van de klant gewijzigd."));
            }
            else if($aMatches[1] == 'delivery')
            {
                $oCustomerAddress = CustomerAddressQuery::create()->findOneById($oSaleOrder->getCustomerDeliveryAddressId());

                if(isset($aSaleOrder['customer_delivery_company_name'])) { $oCustomerAddress->setCompanyName($aSaleOrder['customer_delivery_company_name']); }
                if(isset($aSaleOrder['customer_delivery_attn_name'])) { $oCustomerAddress->setAttnName($aSaleOrder['customer_delivery_attn_name']); }
                if(isset($aSaleOrder['customer_delivery_address_l1'])) { $oCustomerAddress->setAddressL1($aSaleOrder['customer_delivery_address_l1']); }
                if(isset($aSaleOrder['customer_delivery_address_l2'])) { $oCustomerAddress->setAddressL2($aSaleOrder['customer_delivery_address_l2']); }
                if(isset($aSaleOrder['customer_delivery_street'])) { $oCustomerAddress->setStreet($aSaleOrder['customer_delivery_street']); }
                if(isset($aSaleOrder['customer_delivery_number'])) { $oCustomerAddress->setNumber($aSaleOrder['customer_delivery_number']); }
                if(isset($aSaleOrder['customer_delivery_number_add'])) { $oCustomerAddress->setNumberAdd($aSaleOrder['customer_delivery_number_add']); }
                if(isset($aSaleOrder['customer_delivery_postal'])) { $oCustomerAddress->setPostal($aSaleOrder['customer_delivery_postal']); }
                if(isset($aSaleOrder['customer_delivery_city'])) { $oCustomerAddress->setCity($aSaleOrder['customer_delivery_city']); }
                if(isset($aSaleOrder['customer_delivery_country']))
                {
                    $oCountry = CountryQuery::create()->findOneByName($aSaleOrder['customer_delivery_country']);

                    if($oCountry instanceof \Model\Setting\MasterTable\Country)
                    {
                        $oCustomerAddress->setCountryId($oCountry->getId());
                    }
                    else
                    {
                        $oCustomerAddress->setCountryId(null);
                    }
                }
                if(isset($aSaleOrder['customer_delivery_usa_state']))
                {
                    $oUsaState = UsaStateQuery::create()->findOneByName($aSaleOrder['customer_delivery_usa_state']);
                    if($oUsaState instanceof UsaState)
                    {
                        $oCustomerAddress->setUsaStateId($oUsaState->getId());
                    }
                    else
                    {
                        $oCustomerAddress->setUsaStateId(null);
                    }
                }
                $oCustomerAddress->save();
                StatusMessage::success(Translate::fromCode("Afleveradres van de klant gewijzigd."));
            }
            else
            {
                throw new LogicException("Address type ".$aMatches[1]." is not supported here.");
            }
        }
        return $aSaleOrder;
    }
}
