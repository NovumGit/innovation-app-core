<?php
namespace AdminModules\Order\Edit;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;

class PleaseconfirmController extends MainController
{

    function run()
    {
        $sDeclineUrl = DeferredAction::get('decline_url');
        $aView = [
            'top_nav' => '',
            'title' => Translate::fromCode('Copy changes?'),
            'content' => $this->parse('Order/Edit/pleaseconfirm.twig', ['decline_url' => $sDeclineUrl])
        ];
        return $aView;
    }


}
