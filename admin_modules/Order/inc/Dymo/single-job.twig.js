/**
 * @param dymo Global dymo project
 * @param dymo.label Generic Dymo printer system
 * @param dymo.label.framework contains the Dymo label printer framewor
 * @param dymo.label.framework.getPrinters()  returns a list of available printers
 */
function printLabel{{ sale_order.getId() }}(dymo)
{
    var printers    = dymo.label.framework.getPrinters();

    var printer     = printers[0];

    // create label set to print printers' data
    var labelSetBuilder = new dymo.label.framework.LabelSetBuilder();

    // process each printer info as a separate label
    var record = labelSetBuilder.addRecord();

    // compose text data
    // use framework's text markup feature to set text formatting
    // because text markup is xml you can use any xml tools to compose it
    // here we will use simple text manipulations to avoid cross-browser compatibility.


    var info = "<font family='Calibri' size='14'>";
    info = info + "<font size='{{ sale_order.getCustomerDeliveryAddressL2() ? '19' : '25' }}'>{{ sale_order.getCustomerDeliveryCompanyName()|upper }} {{ sale_order.getCustomerDeliveryAttnName()|upper }}</font><br />";
    info = info + "<font size='{{ sale_order.getCustomerDeliveryAddressL2() ? '19' : '25' }}'>{{ sale_order.getCustomerDeliveryAddressL1()|upper }}</font><br />";
    {% if sale_order.getCustomerDeliveryAddressL2() %}
        info = info + "<font size='19'>{{ sale_order.getCustomerDeliveryAddressL2()|upper }}</font><br />";
    {% endif %}
    info = info + "<font size='{{ sale_order.getCustomerDeliveryAddressL2() ? '19' : '25' }}'>{{ sale_order.getCustomerDeliveryPostal()|upper }} {{ sale_order.getCustomerDeliveryCity()|upper }}</font><br />";


    {% if sale_order.getCustomerDeliveryCountry == 'United States of America' %}
        info = info + "<font size='{{ sale_order.getCustomerDeliveryAddressL2() ? '19' : '25' }}'>{{ sale_order.getCustomerDeliveryUsaState()|upper }} USA</font><br />";
    {% else %}
        info = info + "<font size='{{ sale_order.getCustomerDeliveryAddressL2() ? '19' : '25' }}'>{{ sale_order.getCustomerDeliveryCountry()|upper }}</font><br />";
    {% endif %}

    if (typeof printer.isTwinTurbo != "undefined")
    {
        if (printer.isTwinTurbo)
            info = info + "<i><u><br/></u></i>";
        else
            info = info + "<font size='6'><br/></font>";
    }

    if (typeof printer.isAutoCutSupported != "undefined")
    {
        if (printer.isAutoCutSupported)
            info = info + "<i><u><br/></u></i>";
        else
            info = info + "<font size='6'><br/></font>";
    }

    info = info + "</font>";

    // any label layout is a simple layout with one Text object


    var dieCutLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\
                                    <DieCutLabel Version="8.0" Units="twips">\
                                        <PaperOrientation>Landscape</PaperOrientation>\
                                        <Id>Address</Id>\
                                        <PaperName>30252 Address</PaperName>\
                                        <DrawCommands/>\
                                        <ObjectInfo>\
                                            <TextObject>\
                                                <Name>Text</Name>\
                                                <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                                <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                                <LinkedObjectName></LinkedObjectName>\
                                                <Rotation>Rotation0</Rotation>\
                                                <IsMirrored>False</IsMirrored>\
                                                <IsVariable>True</IsVariable>\
                                                <HorizontalAlignment>Left</HorizontalAlignment>\
                                                <VerticalAlignment>Top</VerticalAlignment>\
                                                <TextFitMode>AlwaysFit</TextFitMode>\
                                                <UseFullFontHeight>True</UseFullFontHeight>\
                                                <Verticalized>False</Verticalized>\
                                                <StyledText/>\
                                            </TextObject>\
                                            <Bounds X="336" Y="1" Width="4000" Height="1600" />\
                                        </ObjectInfo>\
                                    </DieCutLabel>';

    var continuousLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\
                                        <ContinuousLabel Version="8.0" Units="twips">\
                                            <PaperOrientation>Landscape</PaperOrientation>\
                                            <Id>Tape19mm</Id>\
                                            <PaperName>19mm</PaperName>\
                                            <LengthMode>Auto</LengthMode>\
                                            <LabelLength>0</LabelLength>\
                                            <RootCell>\
                                                    <TextObject>\
                                                        <Name>Text</Name>\
                                                        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                                        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\
                                                        <LinkedObjectName></LinkedObjectName>\
                                                        <Rotation>Rotation0</Rotation>\
                                                        <IsMirrored>False</IsMirrored>\
                                                        <IsVariable>True</IsVariable>\
                                                        <HorizontalAlignment>Left</HorizontalAlignment>\
                                                        <VerticalAlignment>Top</VerticalAlignment>\
                                                        <TextFitMode>AlwaysFit</TextFitMode>\
                                                        <UseFullFontHeight>True</UseFullFontHeight>\
                                                        <Verticalized>False</Verticalized>\
                                                        <StyledText/>\
                                                    </TextObject>\
                                                    <ObjectMargin Left="0" Top="0" Right="0" Bottom="0" />\
                                                <Length>0</Length>\
                                                <LengthMode>Auto</LengthMode>\
                                                <BorderWidth>0</BorderWidth>\
                                                <BorderStyle>Solid</BorderStyle>\
                                                <BorderColor Alpha="255" Red="0" Green="0" Blue="0" />\
                                            </RootCell>\
                                        </ContinuousLabel>';



    // select label layout/template based on printer type
    var labelXml;

    if (printer.printerType == "LabelWriterPrinter")
        labelXml = dieCutLabelLayout;
    else if (printer.printerType == "TapePrinter")
        labelXml = continuousLabelLayout;
    else
    {
        alert("Unsupported printer type");
        throw "Unsupported printer type";
    }
    // when printing put info into object with name "Text"
    record.setTextMarkup("Text", info);

    //console.log(printer);
    dymo.label.framework.printLabel(printer.name, "", labelXml, labelSetBuilder);
}
printLabel{{ sale_order.getId() }}(dymo);