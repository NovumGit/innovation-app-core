<?php
namespace AdminModules\Order\Trigger;

use Core\AbstractEvent;
use Core\DeferredAction;
use Core\MainController;
use Exception\LogicException;

class EventController extends MainController
{
    function run()
    {
        $sR = $this->get('r', null, true);
        $sEvent = $this->get('event');
        $aArguments = $this->get('event_argument');
        $sFqEventName = 'AdminModules\\Order\\Event\\' . $sEvent;

        $sUrl = DeferredAction::get($sR);

        $oEvent = new $sFqEventName($aArguments, []);

        if (!$oEvent instanceof AbstractEvent) {
            throw new LogicException("Expected an instance of Abstract event");
        }
        $oEvent->trigger('edit');
        $this->redirect($sUrl);
    }
}