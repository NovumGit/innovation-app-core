$('.is_enabled_webshop').change(function(e){
    $('#fld_do').val('StoreEnabledWebshop');
    $(this).closest('form').submit();
});

$('.is_enabled_cms').change(function(e){
    $('#fld_do').val('StoreEnabledCms');
    $(this).closest('form').submit();
});

$('.is_default_webshop').change(function(e){
    $('#fld_do').val('StoreDefaultWebshop');
    $(this).closest('form').submit();
});

$('.is_default_cms').change(function(e){
    $('#fld_do').val('StoreDefaultCms');
    $(this).closest('form').submit();
});