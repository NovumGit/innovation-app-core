<?php
namespace AdminModules\Admin\Field_translation;

use Core\Cfg;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudElement;
use Crud\Field;
use Exception\LogicException;
use Helper\AutoTranslationHelper;
use Model\Setting\MasterTable\LanguageQuery;

class EditController extends MainController
{
    function doSaveTranslation()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $aData = $this->post('data', null, true, 'array');

        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $sCrudFoldername = $this->get('crud', null, true);
        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        $sLocaleFile = "../classes/Crud/Custom/".Cfg::get('CUSTOM_NAMESPACE')."/$sCrudFoldername/Locales/".$oLanguage->getLocaleCode().".json";
        if(!is_dir("../classes/Crud/Custom/".Cfg::get('CUSTOM_NAMESPACE')."/$sCrudFoldername/Locales/"))
        {
            // Normal crud (not in Custom folder)
            $sLocaleFile = "../classes/Crud/$sCrudFoldername/Locales/".$oLanguage->getLocaleCode().'.json';
        }

        $aLocaleJsonContents = [];
        foreach($aData as $sKey => $sValue)
        {
            $aLocaleJsonContents[$sKey] = $sValue;
        }
        if(!isset($aData['all_translated']))
        {
            $aLocaleJsonContents['all_translated'] = false;
        }

        file_put_contents($sLocaleFile.'.tmp', json_encode($aLocaleJsonContents));
        unlink($sLocaleFile);
        rename($sLocaleFile.'.tmp', $sLocaleFile);

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen."));

        $this->redirect("/admin/field_translation/choose_fieldset?language_id=$iLanguageId");
    }
    function run()
    {
        //$this->addJsFile('/admin_modules/Admin/Field_translation_edit.js');
        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $sCrudFoldername = $this->get('crud', null, true);
        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        $sCrudFieldsPath = "../classes/Crud/Custom/".Cfg::get('CUSTOM_NAMESPACE')."/$sCrudFoldername/Field";

        $bCustomCrud = null;
        if(is_dir($sCrudFieldsPath))
        {
            $bCustomCrud = true;
            $sLocaleFile = "../classes/Crud/Custom/".Cfg::get('CUSTOM_NAMESPACE')."/$sCrudFoldername/Locales/".$oLanguage->getLocaleCode().".json";
            $sEnglishLocaleFile = "../classes/Crud/Custom/".Cfg::get('CUSTOM_NAMESPACE')."/$sCrudFoldername/Locales/en_US.json";;
        }
        else
        {
            $bCustomCrud = false;
            $sCrudFieldsPath = "../classes/Crud/$sCrudFoldername/Field";
            $sLocaleFile = "../classes/Crud/$sCrudFoldername/Locales/".$oLanguage->getLocaleCode().'.json';
            $sEnglishLocaleFile = "../classes/Crud/$sCrudFoldername/Locales/en_US.json";
        }

        $sLocaleJsonContents = file_get_contents($sLocaleFile);
        $aLocaleJsonContents = json_decode($sLocaleJsonContents, true);
        $aFields = glob($sCrudFieldsPath.'/*');

        $aTranslationSuggestions = [];
        if(file_exists($sEnglishLocaleFile))
        {
            $sEnglishLocaleContents = file_get_contents($sEnglishLocaleFile);
            $aEnglishTranslation = json_decode($sEnglishLocaleContents, true);
            if(!empty($aEnglishTranslation))
            {
                foreach($aEnglishTranslation as $sDutchWord => $sEnglishWord)
                {
                    $aTranslationSuggestions[$sDutchWord] = AutoTranslationHelper::doTranslate($sEnglishWord, 'en_US', $oLanguage->getLocaleCode());
                }
            }
        }

        foreach($aFields as $sField)
        {
            $sCrudFieldClassName =  preg_replace('/.php$/', '', basename($sField));

            if($bCustomCrud)
            {
                $sCrudFieldNamespacedClassName = '\\Crud\\Custom\\'.Cfg::get('CUSTOM_NAMESPACE').'\\'.$sCrudFoldername.'\\Field\\'.$sCrudFieldClassName;
            }
            else
            {
                $sCrudFieldNamespacedClassName = '\\Crud\\'.$sCrudFoldername.'\\Field\\'.$sCrudFieldClassName;
            }

            if(!class_exists($sCrudFieldNamespacedClassName))
            {
                continue;
            }

            $oCrudFieldObject = new $sCrudFieldNamespacedClassName;

            if(!$oCrudFieldObject instanceof CrudElement)
            {
                throw new LogicException("Crud field should be an instance of Field");
            }

            if(!isset($aLocaleJsonContents[$oCrudFieldObject->getUntranslatedFieldTitle()]))
            {
                $aLocaleJsonContents['all_translated'] = false;
                $aLocaleJsonContents[$oCrudFieldObject->getUntranslatedFieldTitle()] = $oCrudFieldObject->getFieldTitle();
            }
        }


        $aViewData = [];
        $aViewData['all_translated'] = false;
        if(isset($aLocaleJsonContents['all_translated']))
        {
            $aViewData['all_translated'] = ($aLocaleJsonContents['all_translated'] == '1');
            unset($aLocaleJsonContents['all_translated']);
        }

        $aViewData['crud_fields'] = $aLocaleJsonContents;

        $aViewData['current_language'] = $oLanguage;
        $aViewData['suggestions'] = $aTranslationSuggestions;

        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'field_translation']);
        $sOverview = $this->parse('Admin/Field_translation/edit.twig', $aViewData);
        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}

