<?php
namespace AdminModules\Admin\Field_translation;

use Core\Cfg;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Setting\MasterTable\LanguageQuery;

class OverviewController extends MainController
{
    function run()
    {
        clearstatcache();

        foreach(['../classes/Crud', '../classes/Crud/Custom/'.Cfg::get('CUSTOM_NAMESPACE')] as $sCrudRootFolder)
        {
            $aCrudFolderContents = glob($sCrudRootFolder.'/*');
            $aViewData = [];
            $aViewData['languages'] = LanguageQuery::create()->orderByDescription()->find();
            $aViewData['all_translated'] = [];

            foreach($aCrudFolderContents as $sPossibleCrudFolder)
            {
                if(is_dir($sPossibleCrudFolder) && is_dir($sPossibleCrudFolder.'/Field'))
                {
                    if(!is_writable($sPossibleCrudFolder))
                    {
                        StatusMessage::warning(Translate::fromCode("Kan geen vertaalbestanden aanmaken omdat de map")." $sPossibleCrudFolder ".Translate::fromCode("niet beschrijfbaar is. Mogelijk is de schijf vol of zijn de rechten niet goed geconfigureerd. Neem contact op met de beheerder."));
                    }
                    else
                    {
                        if(!is_dir($sPossibleCrudFolder.'/Locales'))
                        {
                            mkdir($sPossibleCrudFolder.'/Locales');
                        }
                        foreach($aViewData['languages'] as $oLanguage)
                        {
                            // Innocent until proven guilty.
                            $aViewData['all_translated'][$oLanguage->getId()] = true;

                            $sLocaleFile = $sPossibleCrudFolder.'/Locales/'.$oLanguage->getLocaleCode().'.json';

                            if(!file_exists($sLocaleFile))
                            {
                                $aViewData['all_translated'][$oLanguage->getId()] = false;
                                if(!is_writable($sPossibleCrudFolder.'/Locales/'))
                                {
                                    StatusMessage::warning(Translate::fromCode("Kan geen vertaalbestand aanmaken omdat de map").' '.$sLocaleFile.' '.Translate::fromCode("niet beschrijfbaar is. Neem contact op met de beheerder."));
                                }
                                else
                                {
                                    StatusMessage::success(Translate::fromCode("Nieuw vertaalbestand aangemaakt").' '.$sLocaleFile);
                                    touch($sLocaleFile);
                                }
                            }
                            else
                            {
                                $sLocaleFile = file_get_contents($sLocaleFile);
                                $aLocaleVars = json_decode($sLocaleFile, true);

                                if(!isset($aLocaleVars['all_translated']) || !$aLocaleVars['all_translated'])
                                {
                                    $aViewData['all_translated'][$oLanguage->getId()] = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'field_translation']);
        $sOverview = $this->parse('Admin/Field_translation/overview.twig', $aViewData);
        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;

    }
}

