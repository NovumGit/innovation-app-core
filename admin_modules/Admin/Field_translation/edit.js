$('.copy_suggestion').click(function(e)
{
    e.preventDefault();

    oParentRow = $(this).parent().parent().html();

    sNewValue = $('.suggestion:first', oParentRow).html();
    console.log(sNewValue);
   // console.log($('.data_field:first', oParentRow));
    $('input', $(this).parent().parent()).val(sNewValue);
});

$('#copy_all_suggestions').click(function(e)
{
    e.preventDefault();

    aDataRows = $('tr.data_row');

    aDataRows.each(function(i, e)
    {
        sNewValue = $('.suggestion', e).html();
        if($.trim(sNewValue) != '')
        {
            $('input', e).val($('.suggestion', e).html());
        }
    });
});