<?php
namespace AdminModules\Admin\Field_translation;

use Core\Cfg;
use Core\MainController;
use Core\Translate;
use Exception\LogicException;
use Model\Setting\MasterTable\LanguageQuery;

class Choose_fieldsetController extends MainController
{
    function run()
    {
        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $oSelectedLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        $aAvailableCruds = [];

        foreach(['../classes/Crud', '../classes/Crud/Custom/'.Cfg::get('CUSTOM_NAMESPACE')] as $sCrudRootFolder)
        {
            $aCrudFolderContents = glob($sCrudRootFolder.'/*');

            foreach($aCrudFolderContents as $aCrudFolderContentsItem)
            {

                if(is_dir($aCrudFolderContentsItem) && is_dir($aCrudFolderContentsItem.'/Field'))
                {

                    $sLocaleCodeJsonFile = $aCrudFolderContentsItem.'/Locales/'.$oSelectedLanguage->getLocaleCode().".json";
                    if(file_exists($sLocaleCodeJsonFile))
                    {
                        $sJsonFileContents = file_get_contents($sLocaleCodeJsonFile);
                        $aJsonFile = json_decode($sJsonFileContents, true);

                        $bFullyTranslated = false;
                        if(isset($aJsonFile['all_translated']) && $aJsonFile['all_translated'] == '1')
                        {
                            $bFullyTranslated = true;
                        }

                    }
                    else
                    {
                        throw new LogicException("Expected a language file $sLocaleCodeJsonFile");
                    }
                    $aAvailableCruds[] = [
                        'name' => basename($aCrudFolderContentsItem),
                        'is_translated' => $bFullyTranslated
                    ];
                }
            }
        }
        $aViewData = [];
        $aViewData['available_cruds'] = $aAvailableCruds;
        $aViewData['language_id'] = $iLanguageId;
        $aViewData['current_language'] = $oSelectedLanguage;

        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'field_translation']);
        $sOverview = $this->parse('Admin/Field_translation/choose_fieldset.twig', $aViewData);
        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;

    }
}

