<?php
namespace AdminModules\Admin\Mastertable\Shipping_method;

use Core\StatusMessage;
use Crud\Shipping_method\CrudShipping_methodManager;

use Model\Sale\SaleOrderQuery;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudShipping_methodManager();
    }
    function doDelete()
    {
        $iShippingMethodId =  $this->get('id');
        $aSaleOrders = SaleOrderQuery::create()->filterByItemDeleted(0)->filterByShippingMethodId($iShippingMethodId)->find();

        if(!$aSaleOrders->isEmpty())
        {
            StatusMessage::warning("Kan deze verzendmethode niet verwijderen want er zijn ".$aSaleOrders->count()." orders die er gebruik van maken, de methode wijzigen kan invloed hebben op de workflow van het systeem.");
            $this->redirect('/admin/mastertable/shipping_method/overview');
        }
        else
        {
            parent::doDelete();
        }
    }

    function run()
    {
        $iShippingMethodId =  $this->get('id');
        $aSaleOrders = SaleOrderQuery::create()->filterByItemDeleted(0)->filterByShippingMethodId($iShippingMethodId)->find();
        if(!$aSaleOrders->isEmpty() && $iShippingMethodId)
        {
            StatusMessage::warning("Door deze verzendmethode te bewerken wijzigd u ook de verzendmethode van ".$aSaleOrders->count()." orders.");
        }

        return parent::run();
    }
    function getOverviewUrl()
    {
        return '/admin/mastertable/shipping_method/overview';
    }
}
