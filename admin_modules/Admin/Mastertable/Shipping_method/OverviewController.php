<?php
namespace AdminModules\Admin\Mastertable\Shipping_method;

use Crud\Shipping_method\CrudShipping_methodManager;
use Model\Setting\MasterTable\ShippingMethodQuery;
use AdminModules\Setting\OverviewController as ParentOverviewController;

class OverviewController extends ParentOverviewController
{
    function run()
    {
        return $this->runCrud(new CrudShipping_methodManager(), ShippingMethodQuery::create());
    }
}
