<?php
namespace AdminModules\Admin\Mastertable\Sale_order_status;

use Core\StatusMessage;
use Crud\Sale_order_status\CrudSale_order_statusManager;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use AdminModules\Setting\OverviewController as ParentOverviewController;

class OverviewController extends ParentOverviewController
{

    function doDelete()
    {
        $iSaleOrderStatusId = $this->get('id', null, true, 'numeric');
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrderQuery->filterBySaleOrderStatusId($iSaleOrderStatusId);
        $oSaleOrderQuery->filterByItemDeleted(0);

        $aSalesOrders =  $oSaleOrderQuery->find();

        if(!$aSalesOrders->isEmpty())
        {
            StatusMessage::warning("Er zijn nog orders met deze status, door de status te verwijderen zouden deze orders status loos worden en buiten de boot kunnen vallen.");
            StatusMessage::warning("Verwijderen mislukt.");
            $this->redirect('/setting/mastertable/sale_order_status/overview');
        }

        $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneById($iSaleOrderStatusId);

        if(!$oSaleOrderStatus instanceof SaleOrderStatus)
        {
            StatusMessage::warning("Orderstatus niet gevonden, probeer je dezelfde status misschien 2x te verwijderen?");
            $this->redirect('/setting/mastertable/sale_order_status/overview');
        }
        else if(!$oSaleOrderStatus->getIsDeletable())
        {
            StatusMessage::success("Sorry, het veld is verwijderbaar staat uit bij deze orderstatus, je kunt hem daarom niet verwijderen.");
            $this->redirect('/setting/mastertable/sale_order_status/overview');
        }
        else
        {
            StatusMessage::success("De orderstatus is verwijderd.");
            $oSaleOrderStatus->delete();
            $this->redirect('/setting/mastertable/sale_order_status/overview');
        }
    }
    function run()
    {
        return $this->runCrud(new CrudSale_order_statusManager(), SaleOrderStatusQuery::create());
    }
}
