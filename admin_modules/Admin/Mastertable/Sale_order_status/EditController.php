<?php
namespace AdminModules\Admin\Mastertable\Sale_order_status;

use Core\StatusMessage;
use Crud\Sale_order_status\CrudSale_order_statusManager as CrudInstanceManager;

use Model\Sale\SaleOrderQuery;
use AdminModules\Setting\EditController as ParentEditController;

class EditController extends ParentEditController
{
    function getCrudManager()
    {
        return new CrudInstanceManager();
    }
    function getOverviewUrl()
    {
        $iOrderStatusId = $this->get('id');

        if($iOrderStatusId)
        {
            $iOrderQuantity = SaleOrderQuery::create()->filterBySaleOrderStatusId($iOrderStatusId)->filterByItemDeleted(0)->count();
            if($iOrderQuantity > 0)
            {
                StatusMessage::warning("Als je deze orderstatus wijzigd dan wijzig je de orderstatus de $iOrderQuantity orders die momenteel deze status hebben ook, dit kan invloed hebben op de workflow.");
            }
        }
        return '/admin/mastertable/sale_order_status/overview';
    }

}
