<?php
namespace AdminModules\Admin;

use AdminModules\ModuleConfig;
use Core\MainController;
use Core\Module;
use Core\Translate;
use Exception\LogicException;
use Model\Setting\MasterTable\LanguageQuery;

class Translation_overviewController extends MainController
{
    function run()
    {
        $aViewData = [];
        $aViewData['modules'] = Module::getAllModules();
        $aViewData['languages'] = LanguageQuery::create()->orderByDescription()->find();
        $aViewData['translated_items'] = [];

        $aGeneralColumnAllTranslated = [];

        foreach($aViewData['modules'] as $oModule)
        {
            foreach($aViewData['languages'] as $oLanguage)
            {
                // For the 1st column link  (Algemeen) in the list
                $sJsonFile = '../admin_modules/Locales/'.$oLanguage->getLocaleCode().'.json';
                if(file_exists($sJsonFile))
                {
                    $sGeneralJsonContents = file_get_contents($sJsonFile);
                    $aGeneralJsonContents = json_decode($sGeneralJsonContents, true);
                    $aGeneralColumnAllTranslated[$oLanguage->getId()] = (isset($aGeneralJsonContents['all_translated']) && $aGeneralJsonContents['all_translated']== true) ? true : false;

                }

                // End for the 1st column link

                if(!$oModule instanceof ModuleConfig)
                {
                    throw new LogicException("Module should be an instance of ModuleConfig");
                }
                $sJsonFile = str_replace('AdminModules', '../admin_modules', get_class($oModule));
                $sJsonFile = str_replace('\\', '/', $sJsonFile);
                $sJsonFile = dirname($sJsonFile).'/Locales/'.$oLanguage->getLocaleCode().'.json';

                if(file_exists($sJsonFile))
                {
                    $sJsonData = file_get_contents($sJsonFile);

                    $aJsonData = json_decode($sJsonData, true);
                    $bAllTranslated = (isset($aJsonData['all_translated']) && $aJsonData['all_translated']== true) ? true : false;
                }
                else
                {
                    $bAllTranslated = false;
                }
                if(!isset($aViewData['translated_items'][$oModule->getDatabaseId()]))
                {
                    $aViewData['translated_items'][$oModule->getDatabaseId()] = [];
                }
                $aViewData['translated_items'][$oModule->getDatabaseId()][$oLanguage->getId()] = $bAllTranslated;
            }
        }

        $aViewData['general_column_all_translated'] = $aGeneralColumnAllTranslated;

        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'translation_overview']);
        $sOverview = $this->parse('Admin/translation_overview.twig', $aViewData);
        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}

