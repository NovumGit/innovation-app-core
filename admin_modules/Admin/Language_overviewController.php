<?php
namespace AdminModules\Admin;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\Language\CrudLanguageManager;
use Exception\LogicException;
use Helper\FilterHelper;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class Language_overviewController extends MainController
{
    function doStoreEnabledWebshop()
    {
        $aEnabledWebshop = $this->post('enabled_webshop');
        LanguageQuery::create()->update(['IsEnabledWebshop' => false]);
        if(!empty($aEnabledWebshop))
        {
            LanguageQuery::create()->filterById($aEnabledWebshop)->update(['IsEnabledWebshop' => true]);
        }
        StatusMessage::success(Translate::fromCode('Wijziging aan beschikbare talen in de webshop doorgevoerd.'));
        $sTabId = $this->get('tab');
        $sAddUrl = $sTabId ? '?id='.$sTabId : '';
        $this->redirect('/admin/language_overview'.$sAddUrl);
    }
    function doStoreEnabledCms()
    {
        $aEnabledCms = $this->post('enabled_cms');

        LanguageQuery::create()->update(['IsEnabledCms' => false]);
        if(!empty($aEnabledCms))
        {
            LanguageQuery::create()->filterById($aEnabledCms)->update(['IsEnabledCms' => true]);
        }
        StatusMessage::success(Translate::fromCode('Wijziging aan beschikbare talen in het cms doorgevoerd.'));
        $sTabId = $this->get('tab');
        $sAddUrl = $sTabId ? '?id='.$sTabId : '';
        $this->redirect('/admin/language_overview'.$sAddUrl);
    }
    function doStoreDefaultWebshop()
    {
        $iDefaultWebshopId = $this->post('default_webshop');
        if(!$iDefaultWebshopId)
        {
            $iDefaultWebshopId = $this->get('default_webshop');
        }

        LanguageQuery::create()->update(['IsDefaultWebshop' => false]);
        LanguageQuery::create()->findOneById($iDefaultWebshopId)->setIsDefaultWebshop(true)->save();
        StatusMessage::success(Translate::fromCode('Standaard taal van de webshop aangepast.'));
        $sTabId = $this->get('tab');
        $sAddUrl = $sTabId ? '?id='.$sTabId : '';
        $this->redirect('/admin/language_overview'.$sAddUrl);
    }

    function doStoreDefaultCms()
    {
        $iDefaultCms = $this->post('default_cms');
        LanguageQuery::create()->update(['IsDefaultCms' => false]);
        LanguageQuery::create()->findOneById($iDefaultCms)->setIsDefaultCms(true)->save();
        StatusMessage::success(Translate::fromCode('Standaard taal van het cms aangepast.'));
        $sTabId = $this->get('tab');
        $sAddUrl = $sTabId ? '?id='.$sTabId : '';
        $this->redirect('/admin/language_overview'.$sAddUrl);
    }

    function run()
    {
        $aGet = $this->get();
        $sTitle = Translate::fromCode('Talen');
        $iTabId = $this->get('tab');
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'description';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $this->addJsFile('/admin_modules/Admin/Language_overview.js');
        $oCrudLanguageManager = new CrudLanguageManager();

        $oLanguageQuery = LanguageQuery::create();
        $oLanguageQuery->orderBy($sSortField, $sSortDirection);

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudLanguageManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oLanguageQuery = FilterHelper::applyFilters($oLanguageQuery, $iTabId);
        $oLanguageQuery = $oUserQuery = FilterHelper::generateVisibleFilters($oLanguageQuery, $aFilters);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $oLanguageQuery->orderBy($sSortField, $sSortDirection=='asc' ? Criteria::ASC : Criteria::DESC );

        $aLanguages = $oLanguageQuery->find();
        $oCrudLanguageManager->setOverviewData($aLanguages);

        $aViewData['overview_table'] = $oCrudLanguageManager->getOverviewHtml($aFields);

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Language',
            'title' => $sTitle,
            'manager' => 'LanguageManager',
            'overview_url' => $oCrudLanguageManager->getOverviewUrl(),
            'new_url' => $oCrudLanguageManager->getCreateNewUrl(),
            'new_title' => $oCrudLanguageManager->getNewFormTitle()
        ];

        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView = [
            'title' => $sTitle,
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}
