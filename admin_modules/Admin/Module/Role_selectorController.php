<?php
namespace AdminModules\Admin\Module;

use Crud\CrudViewManager;
use Crud\Role\CrudRoleModuleSelector;
use Exception\LogicException;
use Helper\FilterHelper;
use Model\Setting\CrudManager\CrudView;
use AdminModules\Setting\OverviewController as ParentOverviewController;
use Model\Setting\MasterTable\RoleQuery;

class Role_selectorController extends ParentOverviewController
{
    function run()
    {
        $oCrudManager = new CrudRoleModuleSelector();

        CrudViewManager::init($oCrudManager);

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudManager);


        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oRoleQuery = RoleQuery::create();
        $oRoleQuery->orderBy($sSortField, $sSortDirection);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aFields = CrudViewManager::getFields($oCrudview);
        $oRoleQuery = FilterHelper::applyFilters($oRoleQuery, $iTabId);

        $aRoles = $oRoleQuery->find();
        $oCrudManager->setOverviewData($aRoles);

        $aViewData = [
            'overview_table' => $oCrudManager->getOverviewHtml($aFields),
            'title' => 'Voor welk roltype wilt u de ingeschakelde modules instellen?'
        ];

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Product',
            'manager' => 'ProductManager',
        ];

        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('Admin/Module/role_selector.twig', $aViewData);

        $aView =
            [
                'title' => 'Rolrechten instellen',
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];

        return $aView;
    }
}
