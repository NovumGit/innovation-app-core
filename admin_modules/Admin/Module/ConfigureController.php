<?php
namespace AdminModules\Admin\Module;

use Core\MainController;
use Core\Module;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Exception\LogicException;
use Exception\SecurityException;
use Model\Module\ModuleRole;
use Model\Setting\MasterTable\RoleQuery;

use Model\Module\ModuleRoleQuery;

class ConfigureController extends MainController{

    function doSwitchRole()
    {
        $oOriginalRole = User::getOriginalRole();

        $iNewRoleId = $this->get('role_id', null, true, 'numeric');
        $oNewRequestedRole = RoleQuery::create()->findOneById($iNewRoleId);


        if($oOriginalRole->getName() != 'Admin' && !User::getMember()->getCanChangeRoles())
        {
            throw new SecurityException("Only admins and users with \"can change role\" privileges can change role.");
        }

        if(User::getMember()->getCanChangeRoles() && User::getOriginalRole()->getName() != 'Admin' && $oNewRequestedRole->getName() == 'Admin')
        {
            throw new SecurityException("Only users that originally have Admin privileges can switch to Admin.");
        }


        User::switchRole($oNewRequestedRole);
        StatusMessage::success(Translate::fromCode("Rol tijdelijk gewijzigd naar {$oNewRequestedRole->getName()}"));
        $this->redirect("/admin/module/configure?role_id=$iNewRoleId");
    }
    function doStoreModules()
    {
        $iRoleId = $this->get('role_id', null, true, 'numeric');
        $aEnabledModules = explode(',', $this->post('enabled_modules'));
        $iSorting = 1;

        $oModuleRoleQuery = ModuleRoleQuery::create();
        $oModuleRoleQuery->findByRoleId($iRoleId)->delete();

        if(!empty($aEnabledModules))
        {
            foreach($aEnabledModules as $iModuleId)
            {
                $oModulerole = new ModuleRole();
                $oModulerole->setModuleId($iModuleId);
                $oModulerole->setRoleId($iRoleId);
                $oModulerole->setSorting($iSorting);
                $oModulerole->save();
                $iSorting++;
            }
        }
    }
    function run()
    {
        $iRoleId = $this->get('role_id', null, true, 'numeric');
        $oRole = RoleQuery::create()->findOneById($iRoleId);

        // Deze functie registreert alle modules in de database, nieuwe modules worden hiermee ook toegevoegd.
        $aAllModules = Module::registerAll();



        foreach($aAllModules as $oModule)
        {
            if(method_exists($oModule, 'getMenu'))
            {
                throw new LogicException(get_class($oModule)." heeft nog een getMenu");
            }
        }

        $aEnabledModules = Module::getEnabledModules($iRoleId);
        $aDisabledModules = Module::getDisabledModules($iRoleId);



        $aView = [
            'role' => $oRole,
            'enabled_modules' => $aEnabledModules,
            'disabled_modules' => $aDisabledModules,
        ];

        $aOut = [
            'title' => 'Rolrechten instellen',
            'top_nav' => $this->parse('Admin/Module/top_nav_configure.twig', []),
            'content' => $this->parse('Admin/Module/configure.twig', $aView)
        ];
        return $aOut;
    }
}