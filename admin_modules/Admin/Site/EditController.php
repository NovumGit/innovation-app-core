<?php
namespace AdminModules\Admin\Site;

use AdminModules\GenericEditController;
use Crud\Site\CrudSiteManager;
use Crud\FormManager;
use Core\Translate;

class EditController extends GenericEditController
{
    function getCrudManager():FormManager
    {
        return new CrudSiteManager();
    }
    function getPageTitle()
    {
        return Translate::fromCode('Sites, shops, verkoop kanalen');
    }
}

