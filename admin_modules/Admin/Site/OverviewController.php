<?php
namespace AdminModules\Admin\Site;

use AdminModules\GenericOverviewController;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\Site\CrudSiteManager;
use Crud\FormManager;
use Model\Cms\Base\SiteQuery;
use Model\Cms\Site;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    function doDelete()
    {
        $iSiteId = $this->get('id', null, true, 'numeric');
        $oSite = SiteQuery::create()->findOneById($iSiteId);

        if(!$oSite instanceof Site)
        {
            StatusMessage::warning(Translate::fromCode("Site niet gevonden, was hij al verwijderd."));
        }
        else
        {
            $oSite->delete();
            StatusMessage::success(Translate::fromCode("Site verwijderd."));
        }

        $this->redirect(DeferredAction::get('after_delete_url'));
    }
    function getModule():string
    {
        return 'Site';
    }

    function getQueryObject():ModelCriteria
    {
        return SiteQuery::create();
    }

    function getManager():FormManager
    {
        return new CrudSiteManager();
    }
    function getTitle():string
    {
        return Translate::fromCode('Sites, shops, verkoop kanalen');
    }
}

