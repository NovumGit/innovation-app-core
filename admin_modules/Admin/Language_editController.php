<?php
namespace AdminModules\Admin;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Language\CrudLanguageManager;

class Language_editController extends MainController
{
    function run()
    {
        $sFormLayoutKey = 'language_editor';
        $aLanguage = $this->post('data', null);
        $aLanguage['language_id'] = $this->get('language_id');
        $sPageTitle = $aLanguage['language_id'] ? Translate::fromCode('Taal bewerken') : Translate::fromCode('Taal toevoegen');
        $sAddRetunUrl = $aLanguage['language_id']? '?language_id='.$aLanguage['language_id'] : '';

        $oCrudManager = new CrudLanguageManager();

        $oLanguage = $oCrudManager->getModel(['id' => $aLanguage['language_id']]);
        $sOverviewUrl = $oCrudManager->getOverviewUrl();
        $sDoAfterSave = $oCrudManager->getCreateNewUrl().$sAddRetunUrl;

        $aTopNavVars = [
            'module' => 'Language',
            'from_page' => $oCrudManager->getOverviewUrl(),
            'manager' => 'LanguageManager',
            'overview_url' => $sOverviewUrl,
            'name' => $sFormLayoutKey,
        ];

        $sTopNavHtml = $this->parse('generic_top_nav_edit.twig', $aTopNavVars);

        $sEditForm = $oCrudManager->getRenderedEditHtml($oLanguage, $sFormLayoutKey);
        $aMainWindowVars = [
            'language_id' => $aLanguage['language_id'],
            'edit_form' => $sEditForm,
            'language' => $oLanguage,
            'do_after_save' => $sDoAfterSave,
        ];
        $sMainWindowHtml = $this->parse('edit.twig', $aMainWindowVars);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sPageTitle
        ];
        return $aView;
    }

    function doUndelete()
    {
        StatusMessage::success(Translate::fromCode("Taal uit de prullenmand gehaald."));

        $oCrudManager = new CrudLanguageManager();
        $oLanguage = $oCrudManager->getModel(['id' => $this->get('language_id')]);
        $oLanguage->setItemDeleted(0)->save();
        $this->redirect($oCrudManager->getOverviewUrl());
    }

    function doDelete()
    {

        StatusMessage::info(Translate::fromCode("Taal in de prullenmand gelpaatst."));

        $oCrudManager = new CrudLanguageManager();
        $oLanguage = $oCrudManager->getModel(['id' => $this->get('language_id')]);
        $oLanguage->setIsDefaultCms(0);
        $oLanguage->setIsDefaultWebshop(0);
        $oLanguage->setIsEnabledCms(0);
        $oLanguage->setIsEnabledWebshop(0);
        $oLanguage->setItemDeleted(0)->save();
        $this->redirect($oCrudManager->getOverviewUrl());
    }

    function doStore()
    {
        $oCrudManager = new CrudLanguageManager();
        $aLanguage = $this->post('data', null);

        if($oCrudManager->isValid($aLanguage))
        {
            StatusMessage::info(Translate::fromCode("Taal opgeslagen."));
            $oLanguage = $oCrudManager->save($aLanguage);

            if($this->post('next_overview') == 1)
            {
                $this->redirect($oCrudManager->getOverviewUrl());
            }
            else
            {
                $this->redirect($oCrudManager->getCreateNewUrl().'?language_id='.$oLanguage->getId());
            }
        }
        else
        {
            $oCrudManager->registerValidationErrorStatusMessages($aLanguage);
        }
    }
}
