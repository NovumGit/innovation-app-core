<?php
namespace AdminModules\Admin\Mail;
use Core\MainController;

class Translation_overviewController extends MainController
{

    function run()
    {

        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'email_translation']);

        $aView = [
            'content' => 'x',
            'top_nav' => $sTopNav,
            'title' => 'Templates vertalen'
        ];
        return $aView;
    }
}

