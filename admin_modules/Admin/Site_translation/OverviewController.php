<?php
namespace AdminModules\Admin\Site_translation;

use Core\MainController;
use Core\Translate;
use Model\Cms\SiteQuery;

class OverviewController extends MainController {

    function run()
    {
        $aContentVars = [
            'sites' => SiteQuery::create()->find()
        ];
        $aTopNavVars = [
            'active' => 'site_translation'
        ];



        return [
            'title' => Translate::fromCode("Websites vertalen"),
            "content" => $this->parse('Admin/Site_translation/overview.twig', $aContentVars),
            "top_nav" => $this->parse('Admin/top_nav_overview.twig', $aTopNavVars)
        ];
    }

}