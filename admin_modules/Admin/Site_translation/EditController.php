<?php
namespace AdminModules\Admin\Site_translation;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Helper\AutoTranslationHelper;
use Model\Setting\MasterTable\LanguageQuery;

class EditController extends MainController
{

    function doSaveTranslation()
    {
        $aData = $this->post('data');

        $sSite = $this->get('site', null, true);
        $aData['all_translated'] = (isset($aData['all_translated']) && $aData['all_translated'] == 1) ? true : false;
        $sNewContents = json_encode($aData);
        $sJonFile = $this->getJsonFileName();

        file_put_contents($sJonFile.'.tmp', $sNewContents);
        rename($sJonFile.'.tmp', $sJonFile);

        StatusMessage::success(Translate::fromCode('Vertaling opgeslagen'));
        $this->redirect("/admin/site_translation/choose_module?site=$sSite");
    }
    private function getJsonFileName():string
    {
        $sSite = $this->get('site', null, true);
        $sModuleName = $this->get('module_name', null, true);
        if($sModuleName == 'General')
        {
            $sModuleFolder = '/Locales';
        }
        else if($sModuleName == 'Home')
        {
            $sModuleFolder = '/Locales';
        }
        else
        {
            $sModuleFolder =  "/$sModuleName/Locales";
        }
        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);
        $sJonFile = '../public_html/'.$sSite.'/modules'.$sModuleFolder.'/'.$oLanguage->getLocaleCode().'.json';
        return $sJonFile;

    }
    function run()
    {
        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $aViewData['current_language'] = LanguageQuery::create()->findOneById($iLanguageId);

        $sJonFile = $this->getJsonFileName();
        $sJonFileContents = file_get_contents($sJonFile);
        $aViewData['translation_data'] = json_decode($sJonFileContents, true);

        unset($aViewData['translation_data']['all_translated']);
        $aViewData['suggestions'] = [];
        if(!empty($aViewData['translation_data']))
        {
            foreach($aViewData['translation_data'] as $sEnglishWord => $sTranslation)
            {
                $aViewData['suggestions'][$sEnglishWord] = AutoTranslationHelper::doTranslate($sEnglishWord, 'en_US', $aViewData['current_language']->getLocaleCode());
            }
        }
        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'site_translation']);
        $sOverview = $this->parse('Admin/Site_translation/edit.twig', $aViewData);

        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}

