var copySuggestionLink = $('.copy_suggestion');
copySuggestionLink.click(function(e){
    e.preventDefault();

    oParentRow = $(this).parent().parent().html();

    sNewValue = $('.suggestion:first', oParentRow).html();
    console.log(sNewValue);
    // console.log($('.data_field:first', oParentRow));
    $('input', $(this).parent().parent()).val(sNewValue);
});

$('.copy_all_suggestion').click(function(e)
{
    e.preventDefault();
    copySuggestionLink.click();
});