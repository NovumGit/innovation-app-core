<?php
namespace AdminModules\Admin\Site_translation;

use Core\MainController;
use Core\Translate;
use Model\Setting\MasterTable\LanguageQuery;

class Choose_moduleController extends MainController
{
    function run()
    {
        $sSite = $this->get('site', null, true);

        $sModuleRoot = '../public_html/'.$sSite.'/modules';

        $aLocaleDirs = [];
        $aLocaleDirs[] = $sModuleRoot.'/Locales';

        $aRootFiles = glob($sModuleRoot.'/*');

        foreach($aRootFiles as $sModuleDir) {

            if (is_dir($sModuleDir) && is_dir($sModuleDir . '/Locales'))
            {
                $aLocaleDirs[] = $sModuleDir.'/Locales';
            }
        }

        $aViewData = [];

        $aViewData['languages'] = LanguageQuery::create()->orderByDescription()->find();
        $aViewData['site'] = $sSite;
        $aViewData['translated_items'] = [];

        foreach($aLocaleDirs as $sLocaleDir)
        {
            foreach($aViewData['languages'] as $oLanguage)
            {
                $sJsonFile = $sLocaleDir.'/'.$oLanguage->getLocaleCode().'.json';
                $sEnglishFile = $sLocaleDir.'/en_US.json';

                if(!file_exists($sJsonFile))
                {
                    $aEnglishFile = json_decode(file_get_contents($sEnglishFile), true);
                    $aEnglishFile['all_translated'] = false;
                    file_put_contents($sJsonFile, json_encode($aEnglishFile));
                }
                $sJsonData = file_get_contents($sJsonFile);
                $aJsonData = json_decode($sJsonData, true);


                $bAllTranslated = (isset($aJsonData['all_translated']) && $aJsonData['all_translated']== true) ? true : false;

                // Immediately install new translatable words if they are present
                $sEnglishFile = file_get_contents($sEnglishFile);
                $aEnglishData = json_decode($sEnglishFile, true);
                foreach ($aEnglishData as $sKey => $sValue)
                {
                    if(!isset($aJsonData[$sKey]))
                    {
                        $aJsonData[$sKey] = $sValue;
                        $aJsonData['all_translated'] = false;
                    }
                }
                file_put_contents($sJsonFile, json_encode($aJsonData));

                $sModuleName = basename(dirname($sLocaleDir));
                if($sModuleName == 'modules')
                {
                    $sModuleName = 'General';
                }

                if(!isset($aViewData['translated_items'][$sModuleName]))
                {
                    $aViewData['translated_items'][$sModuleName] = [];
                }
                $aViewData['translated_items'][$sModuleName][$oLanguage->getId()] = $bAllTranslated;
            }
        }

        $sTopNav = $this->parse('Admin/top_nav_overview.twig', ['active' => 'site_translation']);
        $sOverview = $this->parse('Admin/Site_translation/choose_module.twig', $aViewData);

        $aView = [
            'title' => Translate::fromCode('Vertalingen'),
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;

    }
}

