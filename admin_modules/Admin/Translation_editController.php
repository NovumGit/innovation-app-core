<?php
namespace AdminModules\Admin;

use AdminModules\Config;
use AdminModules\ModuleConfig;
use Core\Cfg;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Helper\AutoTranslationHelper;
use Model\Module\Base\ModuleQuery;
use Model\Setting\MasterTable\LanguageQuery;

class Translation_editController extends MainController
{
    private $sTranslationJsonLocale;
    private $aUsTranslation = [];
    private $oModuleConfigClass;
    private $oLanguage;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);

        $sModule = $this->get('module', null, false);

        if(!$sModule)
        {
            $iModuleId = $this->get('module_id', null, true, 'numeric');
        }

        $iLanguageId = $this->get('language_id', null, true, 'numeric');
        $this->oLanguage = LanguageQuery::create()->findOneById($iLanguageId);

        if($sModule == 'general')
        {
            $this->sTranslationJsonLocale = '../admin_modules/Locales/'.$this->oLanguage->getLocaleCode().'.json';
            $sTranslationJsonEnUs = '../admin_modules/Locales/en_US.json';
            $this->oModuleConfigClass = new Config();
        }
        else if(isset($iModuleId) && $iModuleId)
        {
            $oModule = ModuleQuery::create()->findOneById($iModuleId);

            $sNamespacedModuleConfig = $oModule->getName();

            $sTranslationJsonFile = str_replace('AdminModules', '../admin_modules', $sNamespacedModuleConfig);
            $sTranslationJsonFile = str_replace('\\', '/', $sTranslationJsonFile);

            if(strpos($sTranslationJsonFile, '/Custom/'.Cfg::get('CUSTOM_NAMESPACE').'/'))
            {
                $this->sTranslationJsonLocale = dirname(dirname($sTranslationJsonFile)).'/Locales/'.$this->oLanguage->getLocaleCode().'.json';
                $this->sTranslationJsonLocale = str_replace('/'.Cfg::get('CUSTOM_NAMESPACE'), '', $this->sTranslationJsonLocale);
            }
            else
            {
                $this->sTranslationJsonLocale = dirname($sTranslationJsonFile).'/Locales/'.$this->oLanguage->getLocaleCode().'.json';
            }

            $sTranslationJsonEnUs = dirname($sTranslationJsonFile).'/Locales/en_US.json';
            $this->oModuleConfigClass = new $sNamespacedModuleConfig;
        }
        else
        {
            throw new LogicException("Expecteda a parameter module_id or a string module=general, got none");
        }

        if(!file_exists($this->sTranslationJsonLocale) || trim(file_get_contents($this->sTranslationJsonLocale)) == '')
        {
            if(!file_exists($sTranslationJsonEnUs))
            {
                if(!is_dir(dirname($sTranslationJsonEnUs)))
                {
                    mkdir(dirname($sTranslationJsonEnUs), 0777, true);
                }
                touch($sTranslationJsonEnUs);
            }
            $sEn_UsTranslation = file_get_contents($sTranslationJsonEnUs);
            touch($this->sTranslationJsonLocale);
            file_put_contents($this->sTranslationJsonLocale, $sEn_UsTranslation);
        }

        if(file_exists($sTranslationJsonEnUs))
        {
            $sUsJson = file_get_contents($sTranslationJsonEnUs);
            $this->aUsTranslation = json_decode($sUsJson, true);
        }


        if(!$this->oModuleConfigClass instanceof ModuleConfig)
        {
            throw new LogicException("\$oModuleClass must be an instance of ModuleConfig");
        }
    }
    function doSaveTranslation()
    {
        $aData = $this->post('data');
        if(empty($aData))
        {
            $iModuleId = $this->get('module_id', null, true, 'numeric');
            $iLanguageId = $this->get('language_id', null, true, 'numeric');
            StatusMessage::success(Translate::fromCode("Er was geen data, niets opgeslagen."));
            $this->redirect("/admin/translation_edit?language_id=$iLanguageId&module_id=$iModuleId");
        }
        else
        {
            $aData['all_translated'] = (isset($aData['all_translated']) && $aData['all_translated'] == 1) ? true : false;
            $sNewContents = json_encode($aData);

            file_put_contents($this->sTranslationJsonLocale.'.tmp', $sNewContents);
            rename($this->sTranslationJsonLocale.'.tmp', $this->sTranslationJsonLocale);
            StatusMessage::success(Translate::fromCode('Vertaling opgeslagen'));
            $this->redirect("/admin/translation_overview");
        }
    }

    function run()
    {
        $this->addJsFile('/admin_modules/Admin/Translation_edit.js');
        $aViewData['translation_data'] = json_decode(file_get_contents($this->sTranslationJsonLocale), true);
        $aViewData['all_translated'] = isset($aViewData['translation_data']['all_translated']) ? $aViewData['translation_data']['all_translated'] : false;

        unset($aViewData['translation_data']['all_translated']);

        if(!$this->oModuleConfigClass instanceof ModuleConfig)
        {
            throw new LogicException("\$oModuleClass must be an instance of ModuleConfig");
        }

        $sTitle = Translate::fromCode('Vertaling van').' '.$this->oModuleConfigClass->getModuleTitle().' - '.$this->oLanguage->getDescription();

        $aViewData['title'] = $sTitle;
        $aViewData['languages'] = LanguageQuery::create()->orderByDescription();
        $aViewData['current_language'] = $this->oLanguage;

        $aViewData['suggestions'] = [];
        if(!empty($this->aUsTranslation))
        {
            foreach($this->aUsTranslation as $sDutchWord => $sEnglishWord)
            {
                $aViewData['suggestions'][$sDutchWord] = AutoTranslationHelper::doTranslate($sEnglishWord, 'en_US', $this->oLanguage->getLocaleCode());
            }
        }

        $sTopNav = $this->parse('Admin/top_nav_edit.twig', []);
        $sOverview = $this->parse('Admin/translation_edit.twig', $aViewData);
        $aView = [
            'title' => $sTitle,
            'top_nav' => $sTopNav,
            'content' => $sOverview
        ];
        return $aView;
    }
}

