<?php
namespace AdminModules\Admin\Exception;

use AdminModules\GenericOverviewController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Except_log\CrudExcept_logManager;
use Crud\FormManager;
use Model\Logging\Except_logQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GenericOverviewController
{
    function __construct($aGet, $aPost)
    {
        $this->setEnablePaginate(25);
        parent::__construct($aGet, $aPost);
    }

    function doClearExceptionLog()
    {
        Except_logQuery::create()->find()->delete();
        StatusMessage::info("Exceptielog geleegd");
        $this->redirect('/admin/exception/overview');
    }
    function getTopNavTemplate()
    {
        return 'Admin/Exception/top_nav_overview.twig';
    }
    function getManager(): FormManager
    {
        return new CrudExcept_logManager();
    }
    function getQueryObject(): ModelCriteria
    {
        return Except_logQuery::create();
    }
    function getTitle(): string
    {
        return Translate::fromCode("Uitzonderingen log");
    }
    function getModule(): string
    {
        return 'Except_log';
    }


    /*
    function run()
    {
        $sTitle =

        $oExcept_logQuery = Except_logQuery::create();
        $oExcept_logQuery->orderByLastOccurence();
        $aExcept_log = $oExcept_logQuery->find();

        $aData = [
            'except_log' => $aExcept_log
        ];

        return [
            'title' => $sTitle,
            'top_nav' => $this->parse('Admin/Exception/overview_top_nav.twig', $aData),
            'content' => $this->parse('Admin/Exception/overview.twig', $aData),
        ];
    }
    */
}