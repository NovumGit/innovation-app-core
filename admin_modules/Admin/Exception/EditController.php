<?php
namespace AdminModules\Admin\Exception;

use AdminModules\GenericEditController;
use Core\DeferredAction;
use Core\Translate;
use Crud\Except_log\CrudExcept_logManager;
use Crud\FormManager;
use Model\Logging\Except_logQuery;

class EditController extends GenericEditController
{
    function doDelete()
    {
        $iId = $this->get('id');

        $sUrl = DeferredAction::get('after_delete_url');

        $oExcept_logQuery = Except_logQuery::create();
        $oExcept_log = $oExcept_logQuery->findOneById($iId);
        $oExcept_log->delete();

        if($sUrl)
        {
            $this->redirect($sUrl);
        }
        $this->redirect('/admin/exception/overview');
    }
    function getCrudManager(): FormManager
    {
        return new CrudExcept_logManager();
    }
    function getPageTitle()
    {
        return Translate::fromCode('Exceptie bewerken');
    }

}