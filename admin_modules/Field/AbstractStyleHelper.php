<?php
namespace AdminModules\Field;

use Crud\FormManager;
use Ui\RenderEditConfig;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

abstract class AbstractStyleHelper{

    protected $oRenderEditConfig;

    function __construct(RenderEditConfig $oRenderEditConfig)
    {
        $this->oRenderEditConfig = $oRenderEditConfig;
    }
    public function getRenderEditConfig():RenderEditConfig
    {
        return $this->oRenderEditConfig;
    }
    abstract function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null);
    abstract function getEditorHeader();
    abstract function getEditorFooter();
    abstract function getBlockHeader(CrudEditorBlock $oCrudEditorBlock, $iEditorBlockCount);
    abstract function getBlockFooter($iEditorBlockCount);
}
