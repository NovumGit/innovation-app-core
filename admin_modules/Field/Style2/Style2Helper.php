<?php
namespace AdminModules\Field\Style2;

use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\IStyleHelper;
use Crud\FormManager;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

class Style2Helper extends AbstractStyleHelper implements IStyleHelper {

    function getEditorHeader()
    {
        $aEditHtml[] = '<div class="panel-heading">';
        $aEditHtml[] = '    <span class="panel-title hidden-xs">'.$this->oRenderEditConfig->getTitle().'</span>';
        $aEditHtml[] = $this->oRenderEditConfig->getEditorHeaderMenuHtml();
        $aEditHtml[] = '</div>';
        $aEditHtml[] = '<div class="panel-body pn">';
        $aEditHtml[] = '    <div class="tab-content pn br-n allcp-form theme-primary">';

        return join(PHP_EOL, $aEditHtml);
    }
    function getEditorFooter()
    {
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';
        return join(PHP_EOL, $aEditHtml);
    }
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null)
    {
        throw new LogicException("Not implemented");
    }

    function getBlockHeader(CrudEditorBlock $oCrudEditorBlock, $iEditorBlockCount)
    {

        $aEditHtml[] = '<div id="tab_'.$iEditorBlockCount.'" class="tab-pane '.($iEditorBlockCount == 1 ? 'active' : '').'">';
        $aEditHtml[] = '<!-- before-inside-tab-'.$iEditorBlockCount.' -->';
        $aEditHtml[] = '    <div class="col-md-'.$oCrudEditorBlock->getWidth().' ph10">';

        /*
        $iWidth = $oCrudEditorBlock->getWidth();
        $aEditHtml[] = '<div class="col-md-'.$iWidth.' generated">';
        $aEditHtml[] = '    <div class="panel">';
        $aEditHtml[] = '        <div class="panel-heading">';
        $aEditHtml[] = '            <span class="panel-title">' . $oCrudEditorBlock->getTitle() . '</span>';
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '        <div class="panel-body">';
        return join(PHP_EOL, $aEditHtml);
        */
        return join(PHP_EOL, $aEditHtml);
    }

    function getBlockFooter($iEditorBlockCount)
    {
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '<!-- after-inside-tab-'.$iEditorBlockCount.' -->';
        $aEditHtml[] = '</div>';
        /*
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';
        return join(PHP_EOL, $aEditHtml);
        */
        return join(PHP_EOL, $aEditHtml);
    }
}
