<?php
namespace AdminModules\Field\AllCp;

use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\IStyleHelper;
use Core\TemplateFactory;
use Core\Translate;
use Crud\Field;
use Crud\FieldRow;
use Crud\FormManager;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;
use Ui\RenderEditConfig;

class AllCpHelper implements IStyleHelper
{

    public static $iRowWidth = 0;
    private $oRenderEditConfig;
    function __construct(RenderEditConfig $oRenderEditConfig)
    {
        $this->oRenderEditConfig = $oRenderEditConfig;
    }

    /**
     * @param FormManager $oManager
     * @param CrudEditor $oCrudEditor
     * @param null $mPropelModelData
     * @param string|null $sRenderKey
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null)
    {
        $aForm = ['blocks' => []];

        foreach ($oCrudEditor->getCrudEditorBlocksSorted() as $oCrudEditorBlock)
        {
            if($this->oRenderEditConfig->titleLocked())
            {
                $aBlock['title'] = $this->oRenderEditConfig->getTitle();
            }
            else
            {
                $aBlock['title'] = $oCrudEditorBlock->getTitle();

            }

            $aRows = [];
            $iTotalWidth = 0;
            foreach ($oCrudEditorBlock->getFieldsSorted() as $oCrudEditorBlockField)
            {
                $iTotalWidth = $iTotalWidth + $oCrudEditorBlockField->getWidth();
                $iCurrentRow = (int) ceil($iTotalWidth / 12);
                !isset($aRows[$iCurrentRow]) ? $aRows[$iCurrentRow] = ['fields' => []] : null;

                $oField = $oCrudEditorBlockField->getFieldObject($oManager->getAllArguments());
                $oField->setEditHtmlStyle($this->oRenderEditConfig->getLayout());
                $oField->setRenderEditConfig($this->oRenderEditConfig);

                $aRows[$iCurrentRow]['fields'][] = $oField->getEditHtml($mPropelModelData, $oCrudEditorBlockField->isReadonlyField());
            }
            $aBlock['rows'] = $aRows;
            $aForm['blocks'][] = $aBlock;
        }

        $aForm['edit_form_url'] = $oManager->getFormEditUrl($sRenderKey, $this->oRenderEditConfig->getTitle());


        $aForm['id'] = $mPropelModelData->getId();

        $aTemplateGlobals = $this->oRenderEditConfig->getGlobals() ?? [];
        $aForm = array_merge($aTemplateGlobals, $aForm);

        return TemplateFactory::parse('Field/AllCp/container.twig', $aForm);
    }


}
