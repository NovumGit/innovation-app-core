<?php
namespace AdminModules\Field;

use Crud\FormManager;
use Ui\RenderEditConfig;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

interface IStyleHelper{

    function __construct(RenderEditConfig $oRenderEditConfig);
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null);
}
