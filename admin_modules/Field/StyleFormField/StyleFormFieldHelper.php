<?php
namespace AdminModules\Field\StyleFormField;

use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\IStyleHelper;
use Crud\FormManager;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

class StyleFormFieldHelper extends AbstractStyleHelper implements IStyleHelper {

    public static $iRowWidth = 0;
    function getEditorHeader()
    {
        return '<div class="row">';
    }
    function getEditorFooter()
    {
        return '</div>';
    }
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null)
    {
        throw new LogicException("Not implemented");
    }

    function getBlockHeader(CrudEditorBlock $oCrudEditorBlock, $iEditorBlockCount)
    {
        $iWidth = $oCrudEditorBlock->getWidth();
        self::$iRowWidth = self::$iRowWidth + $iWidth;

        $aEditHtml[] = '<div class="col-md-'.$iWidth.' generated">';
        $aEditHtml[] = '    <div class="panel">';
        $aEditHtml[] = '        <div class="panel-heading">';
        $aEditHtml[] = '            <span class="panel-title">' . $oCrudEditorBlock->getTitle() . '</span>';
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '        <div class="panel-body pn">';
        return join(PHP_EOL, $aEditHtml);
    }

    function getBlockFooter($iEditorBlockCount)
    {
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';

        if(self::$iRowWidth >= 12)
        {
            $aEditHtml[] = '</div>';
            $aEditHtml[] = '<div class="row">';
            self::$iRowWidth = 0;
        }
        return join(PHP_EOL, $aEditHtml);
    }
}
