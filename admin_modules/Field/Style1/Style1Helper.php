<?php

namespace AdminModules\Field\Style1;

use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\IStyleHelper;
use Core\Translate;
use Crud\Field;
use Crud\FieldRow;
use Crud\FormManager;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

class Style1Helper extends AbstractStyleHelper implements IStyleHelper
{

    public static $iRowWidth = 0;

    function getEditorHeader()
    {
        return '<div class="row">';
    }

    function getEditorFooter()
    {
        return '</div>';
    }

    /**
     * @param FormManager $oManager
     * @param CrudEditor $oCrudEditor
     * @param null $mPropelModelData
     * @param string|null $sRenderKey
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null)
    {

        $iId = null;

        if (method_exists($mPropelModelData, 'getId')) {
            $iId = $mPropelModelData->getId();
        }

        $aEditHtml[] = '<input type="hidden" id="fld_do" name="_do" value="Store" />';
        $aEditHtml[] = '<input type="hidden" name="data[id]" value="' . $iId . '" />';

        $aEditHtml[] = $this->getEditorHeader();

        $iEditorBlockCount = 0;
        foreach ($oCrudEditor->getCrudEditorBlocksSorted() as $oCrudEditorBlock) {
            $bIsAllReadonly = true;
            $iEditorBlockCount++;
            $aEditHtml[] = $this->getBlockHeader($oCrudEditorBlock, $iEditorBlockCount);
            $aCrudFields = [];
            $aBlockFieldsByClass = [];
            foreach ($oCrudEditorBlock->getFieldsSorted() as $oCrudEditorBlockField) {
                $sShort = $oCrudEditorBlockField->getShortName();
                $oField = $oCrudEditorBlockField->getFieldObject($oManager->getAllArguments());
                $oField->setArguments(['crud_editor_block_field' => $oCrudEditorBlockField]);

                $aCrudFields[] = $oField;
                $aBlockFieldsByClass[$sShort] = $oCrudEditorBlockField;
            }

            $iCurrRowWidth = 0;
            $iCurrRow = 0;
            $aCrudFieldRows = [];
            $oFieldRow = new FieldRow();

            foreach ($oCrudEditorBlock->getFieldsSorted() as $oCrudEditorBlockField) {
                $oField = $oCrudEditorBlockField->getFieldObject();

                if (($iCurrRowWidth + $oCrudEditorBlockField->getWidth()) > 12) {
                    $aCrudFieldRows[$iCurrRow] = clone $oFieldRow;

                    $oFieldRow = new FieldRow();
                    $iCurrRowWidth = $oCrudEditorBlockField->getWidth();
                    $iCurrRow++;
                }
                $oFieldRow->addField($oCrudEditorBlockField, $oCrudEditorBlockField->getWidth());

                $iCurrRowWidth = $iCurrRowWidth + $oCrudEditorBlockField->getWidth();
            }


            $aCrudFieldRows[$iCurrRow] = $oFieldRow;
            $bFirstRow = true;
            foreach ($aCrudFieldRows as $iRow => $oFieldRow) {
                if (!$oFieldRow instanceof FieldRow) {
                    continue;
                }

                if (count($oFieldRow->getFields()) === 1) {
                    $iLabelWidth = $this->getRenderEditConfig()->getLabelWidth();
                    $iFieldsWidth = $this->getRenderEditConfig()->getFieldWidth();;
                } else {
                    // The label gets whatever is left over.
                    $iLabelWidth = 12 - $oFieldRow->getFullRowWidth();
                    $iFieldsWidth = $oFieldRow->getFullRowWidth();
                }

                if ($bFirstRow) {
                    $bFirstRow = false;
                } else {
                    $aEditHtml[] = '</div>';
                }

                $aEditHtml[] = '<div class="form-group">';
                $bLabelCutOff = false;
                $iGiveBackToCollWith = 0;
                if ($iLabelWidth > 2) {
                    $aEditHtml[] = '    <label for="fld_' . $oField->getFieldName() . '" class="col-lg-' . $iLabelWidth . '  control-label">';
                    $aEditHtml[] = '    ' . $oFieldRow->getLabel();

                    if ($oField->hasValidations()) {
                        $aEditHtml[] = '    <i class="required">*</i>';
                    }
                    $aEditHtml[] = '    </label>';
                } else {
                    $bLabelCutOff = true;
                    $iGiveBackToCollWith = $iLabelWidth;
                }

                if ($oFieldRow->countFields() > 1) {
                    $iFieldsWidth = $iFieldsWidth + $iGiveBackToCollWith;
                    $aEditHtml[] = '<div class="col-lg-' . $iFieldsWidth . '">';
                }

                foreach ($oFieldRow->getFields() as $oFieldRowItem) {

                    if (!$oFieldRowItem->getField() instanceof Field) {
                        throw new LogicException("Crudfield must be an instance of Field.");
                    }
                    $oField = $oFieldRowItem->getField();
                    $oRenderEditConfig = $this->getRenderEditConfig();
                    $oField->setRenderEditConfig($oRenderEditConfig);
                    $oRenderEditConfig->setLayout('StyleFormField');
                    $oFieldRowItem->getField()->setEditHtmlStyle($oRenderEditConfig->getLayout());

                    if ($oFieldRow->countFields() > 1) {

                        $iCalcColWidth = 12 / ($iFieldsWidth / $oFieldRowItem->getWidth());
                    } else {
                        $iCalcColWidth = $oFieldRowItem->getWidth() - $iLabelWidth;
                    }

                    $aEditHtml[] = '<div class="col-lg-' . ceil($iCalcColWidth) . '">';
                    if ($bLabelCutOff) {
                        $aEditHtml[] = '        <label>' . $oField->getFieldLabel() . '</label>';
                    }
                    $aEditHtml[] = '        ' . $oField->getEditHtml($mPropelModelData, $oFieldRowItem->getBlockField()->isReadonlyField());
                    $aEditHtml[] = '</div>';

                    if (!$oFieldRowItem->getBlockField()->isReadonlyField()) {
                        $bIsAllReadonly = false;
                    }
                }

                if ($oFieldRow->countFields() > 1) {
                    $aEditHtml[] = '    </div>';
                }
            }

            $aEditHtml[] = '</div>';

            if (!$bIsAllReadonly && $oRenderEditConfig->getSaveButtonLocation() == 'in_form') {
                $aEditHtml[] = '            <button type="submit" class="btn ladda-button btn-dark pull-right btn-sm" data-style="expand-right">';
                $aEditHtml[] = '                <span class="ladda-label"><i class="fa fa-save"></i> ' . Translate::fromCode('Opslaan') . '</span>';
                $aEditHtml[] = '                <span class="ladda-spinner"></span>';
                $aEditHtml[] = '            </button>';
                if ($oRenderEditConfig->getSaveOverviewButtonVisibile()) {
                    $aEditHtml[] = '            <button name="next_overview" value="1" type="submit" class="btn btn-sm ladda-button btn-dark pull-right" data-style="expand-right"  style="margin-right:10px;">';
                    $aEditHtml[] = '                <span class="ladda-label"><i class="fa fa-save"></i> ' . Translate::fromCode('Opslaan + naar overzicht') . '</span>';
                    $aEditHtml[] = '                <span class="ladda-spinner"></span>';
                    $aEditHtml[] = '            </button>';
                }
            }

            $aEditHtml[] = $this->getBlockFooter($iEditorBlockCount);
        }
        return join(PHP_EOL, $aEditHtml);
    }

    function getBlockHeader(CrudEditorBlock $oCrudEditorBlock, $iEditorBlockCount)
    {
        $iWidth = $oCrudEditorBlock->getWidth();
        self::$iRowWidth = self::$iRowWidth + $iWidth;

        $aEditHtml[] = '<div class="col-md-' . $iWidth . ' generated">';
        $aEditHtml[] = '    <div class="panel">';
        $aEditHtml[] = '        <div class="panel-heading">';
        $aEditHtml[] = '            <span class="panel-title">' . $oCrudEditorBlock->getTitle() . '</span>';
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '        <div class="panel-body pn">';
        return join(PHP_EOL, $aEditHtml);
    }

    function getBlockFooter($iEditorBlockCount)
    {
        $aEditHtml[] = '        </div>';
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';

        if (self::$iRowWidth >= 12) {
            $aEditHtml[] = '</div>';
            $aEditHtml[] = '<div class="row">';
            self::$iRowWidth = 0;
        }
        return join(PHP_EOL, $aEditHtml);
    }
}
