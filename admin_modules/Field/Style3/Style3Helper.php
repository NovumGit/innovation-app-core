<?php
namespace AdminModules\Field\Style3;

use AdminModules\Field\AbstractStyleHelper;
use AdminModules\Field\IStyleHelper;
use Core\Debug;
use Core\Utils;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorBlock;

class Style3Helper extends AbstractStyleHelper implements IStyleHelper {


    function getEditorHeader() {
        return '';
    }
    function getEditorFooter()  {
        $aEditHtml[] = '    </div>';
        $aEditHtml[] = '</div>';
        return join(PHP_EOL, $aEditHtml);
    }

    function getBlockHeader(CrudEditorBlock $oCrudEditorBlock, $iEditorBlockCount) {
        $aEditHtml = [
            '<h4 class="wizard-section-title title" id="steps-uid-0-h-1" tabindex="-1">',
            '<i class="fa fa-user-secret pr5"></i> ' . $oCrudEditorBlock->getTitle() . '</h4>',
            '<section class="wizard-section body allcp-form" id="steps-uid-0-p-1" >',
        ];
        return join(PHP_EOL, $aEditHtml);
    }

    /**
     * @param FormManager $oManager
     * @param CrudEditor $oCrudEditor
     * @param null $mPropelModelData
     * @param string|null $sRenderKey
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    function renderForm(FormManager $oManager, CrudEditor $oCrudEditor, $mPropelModelData = null, string $sRenderKey = null) {
        $iId = null;

        if (method_exists($mPropelModelData, 'getId')) {
            $iId = $mPropelModelData->getId();
        }

        if($this->getRenderEditConfig()->hasDoField())
        {
            $aEditHtml[] = '<input type="hidden" id="fld_do" name="_do" value="Store" />';
        }


        $aEditHtml[] = '<input type="hidden" name="' . $this->getRenderEditConfig()->getFieldArrayName() .  '[id]" value="' . $iId . '" />';
        $aEditHtml[] = $this->getEditorHeader();

        $iEditorBlockCount = 0;
        foreach ($oCrudEditor->getCrudEditorBlocksSorted() as $oCrudEditorBlock) {
            $aBlock = [];
            $aBlockOnly = [];
            $aTitles[] = [
                'title' => $oCrudEditorBlock->getTitle(),
                'slug' => Utils::slugify($oCrudEditorBlock->getTitle())
            ];
            $aBlock[] = $this->getBlockHeader($oCrudEditorBlock, $iEditorBlockCount);

            foreach ($oCrudEditorBlock->getFieldsSorted() as $oCrudEditorBlockField)
            {
                $oField = $oCrudEditorBlockField->getFieldObject();
                $oRenderEditConfig = $this->getRenderEditConfig();
                $oField->setRenderEditConfig($oRenderEditConfig);
                $oField->setEditHtmlStyle($oRenderEditConfig->getLayout());
                $oField->setArguments(['crud_editor_block_field' => $oCrudEditorBlockField]);

                $oField->setFieldArrayName($this->getRenderEditConfig()->getFieldArrayName());

                $aBlock[] = $aBlockOnly[] = $oField->getEditHtml($mPropelModelData, $oCrudEditorBlockField->isReadonlyField());
            }

            $aBlock[] = $this->getBlockFooter($iEditorBlockCount);
            $iEditorBlockCount++;
            $sBlock = join(PHP_EOL, $aBlock);
            $aBlocks[] =  join(PHP_EOL,$aBlockOnly);
            $aEditHtml[] = $sBlock;
        }


        $aGlobals = $this->getRenderEditConfig()->getGlobals();

        if(isset($aGlobals['panel_footer_components']))
        {
            $aPanelFooterComponents = $aGlobals['panel_footer_components'];
            // $aBlock[] = $this->getBlockHeader($iEditorBlockCount);
            foreach ($aPanelFooterComponents as $component)
            {
                $aEditHtml[] = $component['result'];
            }

            // $aBlock[] = $this->getBlockFooter($iEditorBlockCount);
        }


        $aEditHtml[] = $this->getEditorFooter();
        return join(PHP_EOL, $aEditHtml);

    }


    function getBlockFooter($iEditorBlockCount)  {
        $aEditHtml[] = '</section>';
        return join(PHP_EOL, $aEditHtml);
    }
}
