<?php
namespace AdminModules;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewSorter;
use Crud\FormManager;
use Crud\CrudViewManager;
use Helper\FilterHelper;
use Helper\OverviewButtonHelper;
use Model\Setting\CrudManager\CrudView;
use LogicException;
use Propel\Runtime\ActiveQuery\ModelCriteria;

abstract class GenericOverviewController extends MainController{

    private $bEnablePaginate = false;
    private $bEnableDraggableTabs = false;

    private $sCreateButtonsEventScope = '';
    private $bHasCreateButtons = false;
    private $iItemsPP;

    protected $sCrudViewName = '';

    abstract function getTitle():string;
    abstract function getModule():string;
    abstract function getManager():FormManager;
    abstract function getQueryObject():ModelCriteria;

    function getViewLayoutKey()
    {
        return null;
    }
    function setEnablePaginate($iItemsPP = 25)
    {
        $this->bEnablePaginate = true;
        $this->iItemsPP = $iItemsPP;
    }
    function setEnableDraggableTabs()
    {
        $this->bEnableDraggableTabs = true;
        $this->addJsFile('/js/draggable-nav.js');
    }
    function doChangeTabOrder():void
    {
        if($this->bEnableDraggableTabs)
        {
            $aNewSorting = $this->post('tab_order');
            CrudViewSorter::changeTabOrder($this->getManager(), $aNewSorting);
            Utils::jsonOk();
        }
    }
    function setEnableCreateButtons($sEventScope = 'Product')
    {
        $this->addJsFile('/js/custom-buttons.js');
        $this->bHasCreateButtons = true;
        $this->sCreateButtonsEventScope = $sEventScope;
    }

    /*
     * Overwrite me when needed.
     */
    function getDefaultSortingColumn(CrudView $oCrudView = null):string
    {
        if($oCrudView && $oCrudView->getDefaultOrderBy())
        {
            return $oCrudView->getDefaultOrderBy();
        }
        return 'id';
    }
    function getDefaultSortingDirection(CrudView $oCrudView = null):string
    {
        if($oCrudView && $oCrudView->getDefaultOrderDir())
        {
            return $oCrudView->getDefaultOrderDir();
        }
        return 'asc';
    }
    function getMainTemplate()
    {
        return 'generic_overview.twig';
    }
    function getCustomFilters()
    {
        return null;
    }
    function getCustomSorting()
    {
        return null;
    }
    function getExtraMainVars()
    {
    	return null;
    }
    function getTableTitle()
    {
        return '';
    }
    function getTopNavTemplate()
    {
        return 'top_nav_overview.twig';
    }
    function getExtraTopNavVars()
    {
        return null;
    }
	protected function getFilterHtmlArguments():array
	{
		return [];
	}
    protected function applyFilters(ModelCriteria $oQueryObject,int $iTabId):ModelCriteria
    {
        $oQueryObject = FilterHelper::applyFilters($oQueryObject, $iTabId);
        return $oQueryObject;
    }
    /**
     * @return array
     * @throws \ReflectionException
     */
    function run(){

        DeferredAction::register('after_delete_url', $this->getRequestUri());

        $iTabId = $this->get('tab');

        $aGet = $this->get();

        $oCrudManager = $this->getManager();


        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                $oCrudview = CrudViewManager::createViewIfNoneExists($oCrudManager);
                // throw new LogicException("Was looking for a CrudView but got " . get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }


        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : $this->getDefaultSortingColumn($oCrudview);
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : $this->getDefaultSortingDirection($oCrudview);

        $oQueryObject = $this->getQueryObject();
        if($this->getCustomSorting() && in_array($sSortField, $this->getCustomSorting()['fields']))
        {
            $oQueryObject = $this->getCustomSorting()['filters']($oQueryObject, $sSortField, $sSortDirection);
        }
        else
        {
            $oQueryObject->orderBy($sSortField, $sSortDirection);
        }


        $this->sCrudViewName  = $oCrudview->getName();
        $sTitle = $this->getTitle();

        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oCountQueryObject = clone $oQueryObject;
                $oCountQueryObject = $this->applyFilters($oCountQueryObject, $o->getId());
                $o->_sz = number_format($oCountQueryObject->count(), 0, ',', '.');
            }
        }
        $aFilters = isset($this->aGet['filter']) ? $this->aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oCustomFilters = $this->getCustomFilters();

        $oQueryObject = $this->applyFilters($oQueryObject, $iTabId);


        $oQueryObject = FilterHelper::generateVisibleFilters($oQueryObject, $aFilters, $oCustomFilters['filters'] ?? null, $oCustomFilters['fields'] ?? null);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId, $this->getFilterHtmlArguments());

        if($this->bEnablePaginate)
        {
            $iCurrentPage = $this->get('p', 1, false, 'numeric');
            $oCrudManager->setOverviewData($oQueryObject, $iCurrentPage, $this->iItemsPP);
        }
        else
        {
            $aOverviewData = $oQueryObject->find();
            $oCrudManager->setOverviewData($aOverviewData);
        }

        $aViewData['overview_table'] = $oCrudManager->getOverviewHtml($aFields);

        $oRelector = new \ReflectionClass($oCrudManager);
        $sManagerName = str_replace( 'Crud', '', $oRelector->getShortName());

        $aButtons = null;
        if($this->bHasCreateButtons)
        {
            $aButtons = OverviewButtonHelper::getViewButtons($iTabId);
        }

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'site' => $this->get('site'),
            'title' => Translate::fromCode($sTitle),
            'module' => $this->getModule(),
            'manager' => $sManagerName,
            'layout_key' => $this->getViewLayoutKey(),
            'has_create_custom_buttons' => $this->bHasCreateButtons,
            'event_scope' => $this->sCreateButtonsEventScope,
            'buttons' => $aButtons,
            'overview_url' => $oCrudManager->getOverviewUrl(),
            'new_url' => $oCrudManager->getCreateNewUrl(),
            'new_title' => $oCrudManager->getNewFormTitle(),
            'extra_top_nav_vars' => $this->getExtraTopNavVars()
        ];

        if(method_exists($this, 'getEnabledModulesTopnav'))
        {
            $aTopNavVars['enabled_modules'] = $this->getEnabledModulesTopnav();
        }

        $aViewData['title'] = $this->getTableTitle();
        $aViewData['extra_data'] = $this->getExtraMainVars();

        $sTopNav = $this->parse($this->getTopNavTemplate(), $aTopNavVars);
        $sOverview = $this->parse($this->getMainTemplate(), $aViewData);

        $aView = [
            'top_nav' => $sTopNav,
            'content' => $sOverview,
            'title' => $sTitle
        ];
        return $aView;
    }
}
