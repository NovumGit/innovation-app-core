<?php
namespace AdminModules\Login;

use AdminModules\ModuleConfig;
use Core\Translate;

class Config extends ModuleConfig {

    function isEnabelable():bool
    {
        return false;
    }
    function getModuleTitle():string
    {
        return Translate::fromCode('Inloggen');
    }
}
