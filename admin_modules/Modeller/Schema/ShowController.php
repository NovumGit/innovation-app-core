<?php
namespace AdminModules\Modeller\Schema;

use AdminModules\Modeller\Schema\Helpers\Model;
use AdminModules\Modeller\Schema\Helpers\Property;
use AdminModules\Modeller\Schema\Helpers\ValidateModel;
use AdminModules\Modeller\Schema\Helpers\ValidateProperty;
use Core\Cfg;
use Core\DeferredAction;
use Core\Json\JsonUtils;
use Core\MainController;
use Core\Utils;
use Crud\CrudFactory;
use Crud\IApiExposable;
use Exception\LogicException;
use Generator\Schema\Converter;
use LowCode\App;
use Model\Module\ModuleQuery;
use Model\System\DataModel\DataTypeQuery;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\FormFieldTypeQuery;
use Model\System\UI\IconQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Map\TableMap;

/**
 * Skeleton subclass for drawing a list of DataModel records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class ShowController extends MainController
{
    function doGetModelFields()
    {
        $iDataModelId = $this->get('model_id', null, true, 'numeric');

        $aDataFields = DataFieldQuery::create()->filterByDataModelId($iDataModelId)->find();

        JsonUtils::finish($aDataFields->toArray(null, false, TableMap::TYPE_FIELDNAME));
        exit();

    }
    function doGetForm()
    {
        $sSubject = $this->get('subject');

        if(!in_array($sSubject, ['model', 'property']))
        {
            Utils::jsonExit(['error' => "Only model and property are valid options for subject"]);
        }

        $sType = $this->get('type');

        if(!in_array($sType, ['popup', 'panel']))
        {
            Utils::jsonExit(['error' => "Only popup and panel are valid options for type"]);
        }
        $aData = [];

        if($sSubject == 'property')
        {
            $aData['data_types'] = DataTypeQuery::create()->orderByName()->find();
            $aData['form_field_types'] = FormFieldTypeQuery::create()->orderByName()->find();
            $aData['icons'] = IconQuery::create()->orderByName()->find();
            $aData['models'] = DataModelQuery::create()->find();
        }
        else if($sSubject == 'model')
        {
            $aData['modules'] = ModuleQuery::create()->orderByName(Criteria::DESC)->find();

        }

        $sForm = $this->parse('Modeller/Schema/Forms/' . $sSubject . '_' . $sType . '.twig', $aData);
        echo JsonUtils::encode(['form' => $sForm]);
        exit();

    }
    function doDeleteProperty()
    {
        $aData = $this->post('data');

      //  DataFieldQuery::create()->findOneById($aData['id'])->delete();
        JsonUtils::finish($aData);
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doCreateProperty()
    {
        $aData = $this->post('data');

        if(ValidateProperty::isValid($aData))
        {
            $aProperty = Property::fill($aData);
            $oDataModel = Property::create($aProperty);
            $aDataModel = $oDataModel->toArray(TableMap::TYPE_FIELDNAME);
            $aDataModel['table_name'] = $oDataModel->getFkDataModel()->getName();
            JsonUtils::finish($aDataModel);
        }
        JsonUtils::finish(['errors' => ValidateModel::getValidationErrors($aData)]);
    }
    function doCreateModel()
    {
        $aData = $this->post('data');
        if(ValidateModel::isValid($aData))
        {
            $aModel = Model::fill($aData);
            $oDataModel = Model::create($aModel);
            JsonUtils::finish($oDataModel->toArray(TableMap::TYPE_FIELDNAME));
        }
        JsonUtils::finish(['errors' => ValidateModel::getValidationErrors($aData)]);
    }
    private function getSchema():string
    {
        $oModuleQuery = ModuleQuery::create();
        $oModuleQuery->filterByName('AdminModules\\System\\Config');

        $oDataModelQuery = DataModelQuery::create();
        $sCustom = Cfg::get('CUSTOM_NAMESPACE');
        $sCrudNamespace = 'UiComponents';

        $oConverter = new Converter($oModuleQuery, $oDataModelQuery, $sCustom);
        return $oConverter->asJson();
    }
    function run()
    {
        $this->addCssFile('/assets/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css');
        $this->addJsFile('/assets/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker-iconset-all.js');

        $this->addJsFile('/assets/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.js');

        $this->addJsFile('/admin_modules/Modeller/Schema/novum-modeller.js');

        DeferredAction::register('back_schema_editor', $this->getRequestUri());

        // $this->addJsFile('/js/schema-editor/dist/main.js');

        $aModules = ModuleQuery::create()->orderByName(Criteria::DESC)->find();

        $aModeller = [
            'modules' => $aModules,
            'json_schema' => $this->getSchema()
        ];

        return [
            'title' => "Modeller",
            'content' => $this->parse('Modeller/Schema/show.twig', $aModeller)
        ];
    }
}




