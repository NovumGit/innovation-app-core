
import {BaseState, AbstractEvent, BaseProps, BasePromiseType, IEvent} from "novum-js-event-handling-lib";

export enum AjaxAction {
    DELETE,
    POST,
    PUT,
    PATCH,
    GET
}
interface IAjaxPropertyProps extends BaseProps{
    endpoint : string;
    action : AjaxAction;
}
interface IAjaxPropertyState extends BaseState{
    data : {}
}
export interface IAjaxPromiseType extends BasePromiseType, PromiseLike<any>{
    onFulfilled(callback : (arg : any) => any):void;
    onRejected(callback : (arg : any) => any):void;

}

export class AjaxProps implements IAjaxPropertyProps{
    endpoint : string;
    action : AjaxAction;
}
export class AjaxState implements IAjaxPropertyState{
    data : {};

    constructor(data: {}) {
        this.data = data;
    }

}
export class Ajax<P extends IAjaxPropertyProps, S extends IAjaxPropertyState, PT extends IAjaxPromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT>
{
    propType : P;
    stateType : S;
    promiseType : PT;


    trigger(props: P, state: S): Promise<PT> {
        return new Promise<PT>((resolve, reject) => {

            console.log('Ajax(action, data)', props.action, state.data);

            let oData = {
                '_do': props.action,
                'data': state.data
            };
            console.log("\t", window.location.href, "POST", oData);
            return $.post(window.location.href, oData, (data) => {

                console.log("\t", "server response", data);

                if (data.errors)
                {
                    reject(data.errors);
                }
                else
                {
                    resolve(data);
                }

            }, 'json')
            .fail(() => {
                reject('Ajax call failed');
            });
        });

    }
}
