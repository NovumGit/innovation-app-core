import {AbstractEvent, BasePromiseType, BaseProps, BaseState, EventHandler, IEvent} from "novum-js-event-handling-lib";
import {Ajax, AjaxAction} from "../Ajax";
import {IWidget} from "../Contracts/IWidget";
import {PropertyValues} from "../Model/PropertyForm";

export class DeletePropertyProps extends BaseProps {

    endpoint : string;
    readonly action : AjaxAction = AjaxAction.DELETE;

    constructor(endpoint : string) {
        super();
        this.endpoint = endpoint;

    }
}

export class DeletePropertyState extends BaseState {
    widget: IWidget;
    propertyValues : PropertyValues;

    constructor(widget : IWidget, propertyValues : PropertyValues) {
        super();
        this.widget = widget;
        this.propertyValues = propertyValues;

    }
}

class DeletePropertyPromiseType implements BasePromiseType {

    then : any;

    resolve()
    {

    }

    onRejected(callback: (arg: any) => any): void {
    }

    onFulfilled(callback: (arg: any) => any): void {
    }


}
export class DeleteProperty<P extends DeletePropertyProps, S extends DeletePropertyState, PT extends DeletePropertyPromiseType> extends AbstractEvent<P, S, PT> implements IEvent<P, S, PT>{
    promiseType: PT;
    propType: P;
    stateType: S;

    trigger(props: P, state: S): Promise<PT> {

        return new Promise<PT>((resolve, reject) => {

            EventHandler.trigger(new Ajax(), {endpoint : props.endpoint, action : props.action}, {data : state.widget.form.getData()})
                .then(value => {
                    resolve(value);
                })
                .catch(value => {
                    reject(value);
                });

/*
            const error_block = new ErrorBlock(widget);
            error_block.clear();
            error_block.show(data);



            Ajax('DeleteProperty', state.widget, Editor.onPropertyDeleted);
            Messenger.send('property_deleted', state.data);

            console.log('DeleteProperty.trigger');
            console.log("\t", state.data);
*/
        });


    }
}

