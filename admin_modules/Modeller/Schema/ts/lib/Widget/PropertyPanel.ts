import {IWidget} from "../Contracts/IWidget";
import {EditPropertyForm, PropertyModel} from "../Model/PropertyForm";
import {AbstractWidget} from "../Contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../Model/Template";
import {Button} from "../Model/Button";
import {Ajax, AjaxAction, AjaxState} from "../Ajax";
import {ErrorBlock} from "../Model/ErrorBlock";
import {Messenger} from "../Model/Messenger";
import {DeleteProperty, DeletePropertyProps, DeletePropertyState} from "../Actions/DeleteProperty";
import {IEvent,EventStack,AbstractEvent, BasePromiseType, BaseProps, BaseState, EventHandler} from "novum-js-event-handling-lib";

export class PropertyPanel extends AbstractWidget implements IWidget{

    selector : string = '#edit_property_panel';
    template : string;
    form : EditPropertyForm;

    confirm_delete_button:Button;
    delete_button:Button;
    create_button:Button;

    private editor : Editor;
    constructor(editor : Editor)
    {
        super();
        this.editor = editor;
        this.type = TypeType.panel;
        this.subject = SubjectType.property;
        this.form = new EditPropertyForm();
        Template.getForm(this);
    }
    bindEvents():this
    {
        console.log("Close panel bind!!!");
        const closeButton = $('.close-panel');
        const panel = this.getPanel();

        closeButton.unbind('click');
        closeButton.click(function(e){
            e.preventDefault();
            panel.hide();
        });

        const options = {
            arrowClass: 'btn-danger',
            arrowPrevIconClass: 'fas fa-angle-left',
            arrowNextIconClass: 'fas fa-angle-right',
            cols: 10,
            footer: true,
            header: true,
            icon: 'fa fa-edit',
            iconset: 'fontawesome4',
            labelHeader: '{0} of {1} pages',
            labelFooter: '{0} - {1} of {2} icons',
            rows: 5,
            search: true,
            placement : 'top',
            searchText: 'Search',
            selectedClass: 'btn-success',
            unselectedClass: ''
        };
        //@ts-ignore
        $('.icon-picker').iconpicker(options, options);



        return this;
    }
    show(): this
    {
        console.log('PropertyPanel.show()');

        super.show();
        this.bindEvents();
        this.form = new EditPropertyForm();

        console.log('PropertyPanel.show() -> bind-click-confirm-delete');
        this.confirm_delete_button = new Button('#fld_confirm_delete_button', this.confirmDelete, this);

        console.log('PropertyPanel.show() -> bind-click-delete');
        this.delete_button = new Button('#fld_delete_property', this.delete, this);

        console.log('PropertyPanel.show() -> bind-click-save');
        this.create_button = new Button('#fld_save_property', this.save, this);

        return this;
    }
    setData(data:  PropertyModel<string>): this {
        console.log('PropertyPanel.setData', data);

        if(typeof data.table_name != 'undefined')
        {
            console.log("\t", 'this.form.table_name = ' + data.table_name);
            this.form.table_name = data.table_name;
        }
        super.setData(data);
        return this;
    }
    private save = (event : JQuery.ClickEvent, selfClass : AbstractWidget) => {
        console.log('PropertyPanel.save()');
        event.preventDefault();
        console.log("Use error_block");
        console.log('this', this);
        let error_block : ErrorBlock   = new ErrorBlock(this);
        let oData = {
            '_do': 'CreateProperty',
            'data': selfClass.form.getData()
        };
        $.post(window.location.href, oData, (data) => {

            if (data.errors)
            {
                error_block.show(data);
            }
            else
            {
                Editor.onPropertyCreated({content : data, topic : 'property'});
            }

        }, 'json');

    };

    private confirmDelete = (event : JQuery.ClickEvent, propertyPanel : PropertyPanel) => {

        const eventStack  = new EventStack<IEvent<BaseProps, BaseState, BasePromiseType>, AbstractEvent<BaseProps, BaseState, BasePromiseType>>();

        eventStack.register(new DeleteProperty(), new DeletePropertyProps("/deleteurlinvullenblabla"), {data : propertyPanel.form.getData()});
/*
        const statusState  : StatusState = {
            title : "Are you sure?",
            message : "This will delete the column and all the data in it, are you sure?",
            elementId : "confirm_sure",
            StatusMessageColor: StatusMessageColors.warning,
            buttons : [
                {
                    label : "Yes",
                    title : "Yes i am sure",
                    type : StatusMessageColors.warning,
                    action : deleteEvents
                }
            ]
        };
    */

        // EventHandler.trigger(new StatusMessage(), {endpoint : '/'}, statusState);

        // StatusMessage.warning("Sure?", "Are you sure?", "sure_delete_property");
        console.log('PropertyPanel.delete()');
        event.preventDefault();

        EventHandler.trigger(new Ajax(), {action : AjaxAction.DELETE, endpoint : '/xxxx'}, new AjaxState(this.form.getData()));

        Messenger.send('property_deleted', this.form.getData());
    };
    private delete = (event : JQuery.ClickEvent, propertyPanel : PropertyPanel) => {
        console.log('PropertyPanel.delete()');
        console.log("\t", "propertyPanel", propertyPanel);
        console.log("\t", "propertyPanel.form", propertyPanel.form);
        console.log("\t", "propertyPanel.form.fields", propertyPanel.form.fields);
        console.log("\t", "propertyPanel.form.syncData()", propertyPanel.form.getData());

        EventHandler.trigger(new DeleteProperty(), new DeletePropertyProps("/aasdfdas"), new DeletePropertyState(propertyPanel, propertyPanel.form.getData()));
        event.preventDefault();
    };


}
