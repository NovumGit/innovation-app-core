import {IWidget} from "../Contracts/IWidget";
import {ErrorBlock} from "../Model/ErrorBlock";
import {Button} from "../Model/Button";
import {NewModelForm} from "../Model/ModelForm";
import {AbstractWidget} from "../Contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../Model/Template";

export class ModelPopup extends AbstractWidget implements IWidget{

    public selector:string = '#add_model_popup';
    public create_button:Button;
    private editor:Editor;

    constructor(editor : Editor)
    {
        super();

        this.type = TypeType.popup;
        this.subject = SubjectType.model;
        this.editor = editor;
        this.form = new NewModelForm();

        Template.getForm(this);
    }
    bindEvents():this
    {
        this.create_button = new Button('#fld_create_model', this.create, this);
        return this;
    }

    show(): this {

        this.getPlaceHolder().replaceWith(this.template);
        this.bindEvents();
        this.form = new NewModelForm();

        return this;
    }

    private create = (event : JQuery.ClickEvent, selfWidget : ModelPopup) : void => {

        let error_block = new ErrorBlock(this);
        error_block.clear();
        event.preventDefault();


        let oData = {
            '_do': 'CreateModel',
            'data': {
                'module' : $('#new_module').val(),
                'title' : $('#fld_title').val(),
                'name' : $('#fld_name').val()
            }
        };
        $.post(window.location.href, oData, (data) => {

            if (data.errors)
            {
                error_block.show(data);
            }
            else
            {
                Editor.onModelCreated(data);
            }

        }, 'json');

    }
}
