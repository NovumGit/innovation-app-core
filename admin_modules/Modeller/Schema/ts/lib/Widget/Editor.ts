import {ModelPanel} from "./ModelPanel";
import {PropertyPanel} from "./PropertyPanel";
import {ModelPopup} from "./ModelPopup";
import {PropertyPopup} from "./PropertyPopup";
import {IMessageContent, IMessageContentGenerics, Message} from "../Model/Messenger";
import {IWidget} from "../Contracts/IWidget";
import {ConsoleLogger, ErdEditor, LogLevel, MessagingSystem} from "erd-tool"
import {PropertyModel} from "../Model/PropertyForm";
import {ModelModel} from "../Model/ModelForm";
import {MessageContentGenerics} from "../../../../../../admin_public_html/js/typescript/src/schema-editor/model/Messenger";
import {AbstractWidget} from "../Contracts/AbstractWidget";

export class Editor {

    private static modelPanel : ModelPanel;
    private static modelPopup : ModelPopup;
    private static propertyPanel : PropertyPanel;
    private static propertyPopup : PropertyPopup;
    private static erd : ErdEditor;
    public static readonly applicationId : string = '#application';

    constructor() {
        Editor.modelPanel = new ModelPanel(this);
        Editor.modelPopup = new ModelPopup(this);
        Editor.propertyPanel = new PropertyPanel(this);
        Editor.propertyPopup = new PropertyPopup(this);

        Editor.modelPanel.hide();
        Editor.modelPopup.hide();

        Editor.propertyPanel.hide();
        Editor.propertyPopup.hide();

        const panelPlaceholderElement = document.getElementById('panel_placeholder');
        const initialModel = JSON.parse(panelPlaceholderElement.innerText);
        panelPlaceholderElement.innerText = '';


        console.log(ErdEditor);
        const erdEditor = new ErdEditor({
            elementId: Editor.applicationId,
            initialModel: initialModel,
            autoZoom : false,
            initialZoom : 30,
            logLevel : LogLevel.debug,
            logger : new ConsoleLogger("warning"),
            messagingSystem: MessagingSystem.Callback,
            graphOptions : {
                rankdir : "TB",
                ranker : "network-simplex",
                compound : false,
                nodesep : 10,
                edgesep : 20,
                ranksep : 5,
                marginx : 4,
                marginy : 4
            }
        });



        erdEditor.onReceive((topic : string, message : any) => {

            if(topic === 'add_model')
            {
                Editor.showAddModelPopup();
            }
            else if(topic === 'edit_model')
            {
                Editor.showEditModelPanel(message);
            }
            else if(topic === 'add_property')
            {
                Editor.showAddPropertyPopup(message);
            }
            else if(topic === 'edit_property')
            {
                Editor.showEditPropertyPanel(message);
            }
        });
    }

    private static getAllPanels():AbstractWidget[] {
        return [Editor.modelPanel, Editor.modelPopup, Editor.propertyPanel, Editor.propertyPopup];
    }
    static activePanel(newActivePanel:AbstractWidget)  {
        const aAllViews = Editor.getAllPanels();
        for(let i = 0; i < aAllViews.length; i++)
        {
            aAllViews[i].hide(true);
        }
        console.log('Editor.activePanel.newActivePanel', newActivePanel);
        newActivePanel.show();
    }
    static showEditPropertyPanel(message : PropertyModel<string>):void  {
        console.log('Editor.showEditPropertyPanel.message', message);
        Editor.activePanel(Editor.propertyPanel);
        console.log('Editor.showEditPropertyPanel -> Editor.propertyPanel.setData', message);

        Editor.propertyPanel.setData(message);

    }
    static showEditModelPanel(message : ModelModel<string>):void  {
        console.log('Editor.showEditModelPanel.message', message);
        Editor.activePanel(Editor.modelPanel);
        console.log('Editor.showEditModelPanel -> Editor.modelPanel.setData', message);
        Editor.modelPanel.setData(message);
    }

    static showAddPropertyPopup(message : ModelModel<string>):void  {
        console.log('Editor.showAddPropertyPopup', message.content);
        Editor.activePanel(Editor.propertyPopup);
        console.log('Editor.showAddPropertyPopup -> Editor.propertyPopup.setData', message);
        Editor.propertyPopup.setData(message);
    }
    static showAddModelPopup():void {
        console.log('Editor.showAddModelPopup');
        Editor.activePanel(Editor.modelPopup);
    }
    static onPropertyDeleted(data:Message<IMessageContentGenerics>):void  {
        console.log('Editor.onPropertyDeleted', data);
    }
    static onPropertySaved(data:Message<IMessageContentGenerics>):void {
        console.log('Editor.onPropertySaved', data);
    }
    static onPropertyCreated(data:any):void {
        console.log('Editor.onPropertyCreated', data);

        console.log('Editor.onPropertyCreated', 'Editor.propertyPopup.hide()');
        Editor.propertyPopup.hide();

        // Editor.erd.send('property_added', data.content)


        console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.show()');
        Editor.propertyPanel.show();

        console.log('Editor.onPropertyCreated', 'Editor.propertyPanel.setData(data.content);', data.content);
        Editor.propertyPanel.setData(data.content);
    }
    static onModelCreated(data:Message<IMessageContentGenerics>):void {
        console.log('Editor.onModelCreated', data);

        // Editor.erd.send('model_added', data);
        Editor.modelPopup.hide();
        Editor.modelPanel.show();
        Editor.modelPanel.setData(data.content);
    }

}
