import {IWidget} from "../Contracts/IWidget";
import {ErrorBlock} from "../Model/ErrorBlock";

import {EditModelForm} from "../Model/ModelForm";
import {AbstractWidget} from "../Contracts/AbstractWidget";
import {Editor} from "./Editor";
import {SubjectType, Template, TypeType} from "../Model/Template";

export class ModelPanel extends AbstractWidget implements IWidget{

    public selector:string = '#edit_model_panel';
    public form:EditModelForm;
    public error_block:ErrorBlock;

    private editor : Editor;

    constructor(editor : Editor)
    {
        super();
        this.type = TypeType.panel;
        this.subject = SubjectType.model;

        this.editor = editor;
        this.form = new EditModelForm();

        Template.getForm(this);
    }
    bindEvents():this
    {
        console.log("Close panel bind!!!");
        const closeButton = $('.close-panel');
        const panel = this.getPanel();

        closeButton.unbind('click');
        closeButton.click(function(e){
            panel.hide();
        });


        const deleteButton = $('#fld_delete_model');

        deleteButton.unbind('click');
        deleteButton.click(function(e){
            alert('delete');
        });
        return this;
    }
}
