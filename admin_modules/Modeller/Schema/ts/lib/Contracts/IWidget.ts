import {IForm} from "./Form";
import {SubjectType, TypeType} from "../Model/Template";
import {IMessageContent} from "../Model/Messenger";

export interface IWidget {

    form : IForm;
    selector : string;
    template : string;
    type : TypeType;
    subject : SubjectType;
    bindEvents():this;
    hide(immediate : boolean):this;
    show():this;

    getPanel():JQuery;

}
