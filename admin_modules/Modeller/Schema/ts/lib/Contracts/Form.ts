import {GenericModel} from "../Model/Model";

export interface IField extends JQuery {

}
export interface IFieldValues {
    [key: string] : string
}
export class FieldValues implements IFieldValues{
    [key: string] : string
}

export interface IForm {

    getFieldManager():GenericModel<IField>;
    syncData<T>(model : T):T;
    getData():IModelValues;
    getFieldNames():string[]
}
export interface IModelValues extends GenericModel<string>{
    [key: string]: string;
}
export abstract class AbstractForm<FieldCollection extends GenericModel<IField>> implements IForm{

    public fields : FieldCollection;

    abstract getData(): IModelValues;

    constructor(fields : FieldCollection)
    {
        this.fields = fields;
    }

    getFieldNames():string[] {
        return Object.keys(this.fields);
    }

    getFields() : IField[] {
        console.log('AbstractForm.getFields()');

        let aOut : IField[] = [];

        for( let sField in this.getFieldNames()) {
            aOut.push(this.fields[sField]);
        }
        return aOut;
    }

    getFieldManager():FieldCollection
    {
        return this.fields;
    }

    syncData<T>(model : T): T {

        const aModel : T = model;

        this.getFieldNames().forEach((fieldName) => {

            if(this.fields.hasOwnProperty(fieldName) && typeof(this.fields[fieldName].val()) != 'undefined')
            {
                console.log('Form.syncData', fieldName, this.fields[fieldName]);
                //@ts-ignore
                aModel[fieldName] = this.fields[fieldName].val().toString();
            }
        });
        return aModel;
    }

}

