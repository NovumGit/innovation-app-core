import {IForm} from "./Form";
import {SubjectType, TypeType} from "../Model/Template";
import {IMessageContent, IMessageContentGenerics} from "../Model/Messenger";
import {EditPropertyForm} from "../Model/PropertyForm";

class KeyVal {
    key : string;
    value : string;
}
export abstract class AbstractWidget {

    selector:string;
    form:IForm;
    template : string;
    type : TypeType;
    subject : SubjectType;
    // The actual form may be added to the dom later.
    private data_keeper : KeyVal[] = [];

    constructor()
    {
        this.form = new EditPropertyForm();
    }
    getPlaceHolder():JQuery
    {
        return $('#' + this.type + '_placeholder');
    }
    abstract bindEvents():this;
    createPlaceholder():HTMLDivElement
    {
        var elem = <HTMLDivElement>(document.createElement('div'));
        elem.setAttribute('id', this.type + '_placeholder');
        elem.style.display = 'none';
        return elem;
    }

    setData(data:  IMessageContentGenerics): this {

        console.log('AbstractWidget.setData', data);

        let aFieldNames = this.form.getFieldNames();
        let oFormData = this.form.getFieldManager();
        let fieldName : string;
        this.data_keeper = [];

        for(let i = 0; i < aFieldNames.length; i++)
        {
            fieldName = aFieldNames[i].toString();

            console.log('Fieeeeeeld ', fieldName);
            if(typeof fieldName === 'undefined')
            {
                console.log('nope 111111111');
                continue;
            }
            else if(typeof data[fieldName] === 'undefined')
            {
                console.log('nope 222222222');
                continue;
            }

            else if(!data.hasOwnProperty(fieldName))
            {
                console.log('nope 33333333333');
                continue;
            }

            console.log('Daaaaataaaaaaaaa ', data[fieldName]);

            const pushData = {key : fieldName, value : data[fieldName]};
            this.data_keeper.push(pushData);

            console.log(oFormData, fieldName);

            oFormData[fieldName].val(data[fieldName]);
            console.info(oFormData[fieldName]);

            console.log(data);

        }
        return this;
    }

    notifySaved():this
    {
        console.log("Placeholder for save notification");
        return this;
    }
    getPanel():JQuery
    {
        console.log('AbstractWidget.getPanel()', this.selector);
        return $(this.selector);
    }
    hide(immediate : boolean = false):this
    {
        let panel:JQuery = this.getPanel();
        if(panel.length)
        {
            if(immediate)
            {
                panel.replaceWith(this.createPlaceholder());
            }
            else
            {
                panel.fadeOut(200, () => {

                    panel.replaceWith(this.createPlaceholder());
                })
            }
        }
        return this;
    }

    show():this
    {
        console.log('AbstractWidget.show()');

        this.getPlaceHolder().replaceWith(this.template);

        this.getPanel().show();

        let aFieldNames = this.form.getFieldNames();
        let oFormFieldManager = this.form.getFieldManager();
        let currentFieldName:string;
        let keeperFieldName:string;
        let field:JQuery;

        for(let i = 0; i < aFieldNames.length; i++)
        {
            currentFieldName = aFieldNames[i];
            for(let y = 0;  y < this.data_keeper.length; y++)
            {
                keeperFieldName = this.data_keeper[y].key;

                if(currentFieldName === keeperFieldName)
                {
                    // console.log(keeperFieldName + ' => ' + this.data_keeper[y].value);

                    field = oFormFieldManager[currentFieldName];

                    if(field.is('select') && $.trim(this.data_keeper[y].value) == '')
                    {
                        continue;
                    }
                    // console.log('AbstractWidget.show.' + field.prop('name') + '.val()', keeperFieldName, this.data_keeper[y].value);

                    field.val(this.data_keeper[y].value);
                }
            }
        }
        this.bindEvents();

        return this;
    }
}
