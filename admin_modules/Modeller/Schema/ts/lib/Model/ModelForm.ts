import {AbstractForm, IField, IForm} from "../Contracts/Form";
import {GenericModel} from "./Model";
import {PropertyFormFields, PropertyValues} from "./PropertyForm";

// What fields do we have
export class ModelModel<T> extends GenericModel<T> {
    [name: string]: T

    id: T = null;
    name: T;
    title: T;
    module_id: T;
    create_new_module: T;
    new_module_name: T;
}

export class ModelFormFields extends ModelModel<IField> {
    constructor()
    {
        super();
        this.id = $('#fld_id');
        this.name = $('#fld_name');
        this.title = $('#fld_title');
        this.module_id = $('#fld_module_id');
        this.create_new_module = $('#fld_create_new_module');
        this.new_module_name = $('#fld_new_module');
        console.log('ModelFormFields', this);
    }
}
export class EditModelFormFields extends ModelFormFields {}
class ModelValues extends ModelModel<string>{}

export class NewModelForm extends AbstractForm<ModelFormFields> implements IForm{

    constructor() {
        super(new ModelFormFields());
        this.fields = new ModelFormFields();

        // first item is char, only numbers, letters and underscores.
        this.fields.name.on('keyup', this.modelNameFilter);

        // toggle create new form field and keep track of create new  module or use existing module
        this.fields.module_id.on('change', this.toggleCreateNewModule);
    }

    getFieldManager(): ModelFormFields {
        console.log('ModelFormFields.getFieldManager');
        return new ModelFormFields();
    }

    getData(): ModelValues {
        console.log('NewPropertyForm.syncData()');
        return super.syncData(new ModelValues());
    }

    private toggleCreateNewModule():false {
        let bCreateNewModule = ($('option:selected', this.fields.module_id).data('action') === '_create_new_') ? 1 : 0;

        const new_module_field_block = $('#new_module_field_block');

        new_module_field_block.css(
            {
                'display' : bCreateNewModule ? 'block' : 'none'
            }
        );
        this.fields.create_new_module.val(bCreateNewModule);
        return false;
    }
    private modelNameFilter(event:JQuery.KeyUpEvent):false {
        event.preventDefault();

        let newVal : string;
        newVal = this.fields.name.val()
            .toString()
            .toLowerCase()
            .replace(/^[^a-z]/, '')
            .replace(/^[^a-z0-9_]+$/, '');

        this.fields.name.val(newVal);
        return false;
    }
}
export class EditModelForm extends NewModelForm implements IForm{}
