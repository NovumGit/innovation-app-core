import {IWidget} from "../Contracts/IWidget";
import {ErrorResponse} from "./ErrorResponse";

export class ErrorBlock {

    private element : JQuery;
    private model : IWidget;
    private error_ul: JQuery;

    constructor(oModel : IWidget)
    {
        this.model = oModel;
        this.element = $('.error_block');
        this.error_ul = $('.error_ul');

    }
    clear()
    {
        this.error_ul.html('');
    }
    hide()
    {
        this.clear();
        this.element.fadeOut(200);
    }
    show(data : {errors: ErrorResponse})
    {

        console.log('ErrorBlock.show', data.errors);
        let aKeys = this.model.form.getFieldNames();
        let aErrorList : string[] = [];

        aKeys.forEach((field_name) => {
            if (typeof (data.errors[field_name]) != 'undefined') {
                this.model.form.getFieldManager()[field_name].addClass('error');
                let element = document.createElement('li');
                element.innerHTML = data.errors[field_name];
                console.log('ErrorBlock.show', 'element.toString()', element.toString());

                aErrorList.push(element.outerHTML);
            }
        });

        console.log('ErrorBlock.show', aErrorList);
        this.error_ul.html(aErrorList.join("\n"));
        this.element.fadeIn(200);

        $('.error').on('focus', ()=> {
            $(this).removeClass('error');
        });
    }
}
