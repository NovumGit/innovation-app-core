import {IWidget} from "../Contracts/IWidget";
import {AbstractWidget} from "../Contracts/AbstractWidget";

interface IForm {
    form : string;
}
export enum TypeType {
    popup = "popup",
    panel = 'panel'
}
export enum SubjectType {
    model = "model",
    property = "property"
}

export class Template{

    static getForm(widget : AbstractWidget)
    {
        const oData = {
            _do : 'GetForm',
            type : widget.type,
            subject : widget.subject
        };

        $.get('/modeller/schema/show', oData, (data : IForm) => {

            widget.template = data.form;

        }, 'json').then(() => {

            console.log("Template loaded " + widget.type + ' ' + widget.subject);
        });
    }

}
