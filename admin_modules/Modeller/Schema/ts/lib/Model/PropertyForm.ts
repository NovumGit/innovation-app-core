import {AbstractForm, IField, IForm} from "../Contracts/Form";
import {GenericModel} from "./Model";
import {IMessageContentGenerics} from "./Messenger";

// What fields do we have
export class PropertyModel<T> extends GenericModel<T>{
    [name: string]: T

    // required
    label : T;
    name : T;
    form_field_type_id : T;

    // optional
    id? : T;
    data_type_id? : T;
    form_field_lookups? : T;
    required? : T;
    is_unique? : T;
    is_primary_key? : T;
    icon? : T;
    fk_model_field? : T;
    fk_model? : T;
}

export class PropertyValues extends PropertyModel<string>{}

export class PropertyFormFields extends PropertyModel<IField>{

    constructor()
    {
        super();
        this.label = $('#fld_label');
        this.name = $('#fld_name');
        this.form_field_type_id = $('#fld_form_field_type_id');

        // nullable items
        this.id = $('#fld_id');
        this.data_type_id = $('#fld_data_type_id');
        this.form_field_lookups = $('#fld_form_field_lookups');
        this.required = $('#fld_required');
        this.is_unique = $('#fld_is_unique');
        this.is_primary_key = $('#fld_is_primary_key');
        this.icon = $('#fld_icon');
        this.fk_model_field = $('#fld_fk_model_field');
        this.fk_model =  $('#fld_fk_model');
        console.log('PropertyFormFields', this);
    }
}
export class EditPropertyFormFields extends PropertyFormFields {
}
export class NewPropertyForm extends AbstractForm<PropertyFormFields> implements IForm{

    table_name : string;

    constructor() {
        super(new PropertyFormFields());
        console.log('NewPropertyForm.constructor');
        this.fields = new PropertyFormFields();
    }

    getFieldManager(): PropertyFormFields {
        console.log('NewPropertyForm.getFieldManager');
        return new PropertyFormFields();
    }

    getData():PropertyValues{

        console.log('NewPropertyForm.syncData()');
        let aData = super.syncData(new PropertyValues());
        aData['table_name'] = this.table_name;
        return aData;
    }
}
export class EditPropertyForm extends NewPropertyForm implements IForm{}
