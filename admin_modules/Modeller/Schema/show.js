function openPopup(selector)
{
    $.magnificPopup.open({
        removalDelay: 500,
        alignTop : true,
        items: {
            src: selector
        },
        callbacks: {
            beforeOpen: function (e) {
                this.st.mainClass = 'mfp-zoomIn';
            }
        },
        midClick: true
    });
}

window.closePopup = () => {

    $.magnificPopup.close();
};
