<?php
namespace AdminModules\Modeller\Schema\Helpers;

use Core\Cfg;
use Core\Logger;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Model\DataModel;

class Model
{

    static function create(array $aData):DataModel
    {
        Logger::console($aData);
        $oDataModel = new DataModel();
        $oDataModel->setName($aData['name']);
        $oDataModel->setNamespaceName($aData['namespace']);
        $oDataModel->setPhpName($aData['php_name']);
        $oDataModel->setModuleId($aData['module_id']);
        $oDataModel->setTitle($aData['title']);
        $oDataModel->setApiExposed($aData['api_exposed']);
        $oDataModel->setApiDescription($aData['api_description']);
        $oDataModel->setIsPersistent($aData['is_persistent']);
        $oDataModel->setIsUiComponent($aData['is_ui_component']);
        $oDataModel->save();
        return $oDataModel;
    }

    public static function fill($aData):array
    {
        $aModel = [];

        $sName = preg_replace('/^[^a-z]/', '', $aData['name']);
        $sName = preg_replace('/^[^a-z0-9_]+$/', '', $sName);

        $sPhpName = ucfirst($sName);

        $aModel['name'] = $sName;
        $aModel['title'] = $aData['title'];

        $aModel['php_name'] = $sPhpName;

        $aModel['api_exposed'] = false;
        $aModel['api_description'] = '';

        $aModel['is_persistent'] = false;
        $aModel['is_ui_component'] = false;

        if($aData['create_new_module'] === '1')
        {
            $aModel['module_id']= Module::create($aData['new_module_name']);
        }
        else if(empty($aData['module_id']))
        {
            // Defaulting to the System module.
            $oModuleQuery = ModuleQuery::create();
            $oModule = $oModuleQuery->findOneByName('AdminModules\\System\\Config');
            $aModel['module_id'] = $oModule->getId();
        }
        else
        {
            $aModel['module_id'] = $aData['module_id'];
        }

        $oModuleQuery = ModuleQuery::create();

        $oModule = $oModuleQuery->findOneById($aModel['module_id']);

        $sModuleTopLevelNamespace = $oModule->getTopLevelNamespace();

        $sPropelNamespace =  'Model\\Custom\\' . $sModuleTopLevelNamespace . '\\' . Cfg::get('CUSTOM_NAMESPACE');
        $aModel['namespace'] = $sPropelNamespace;
        Logger::console($aModel);

        return $aModel;
    }
}
