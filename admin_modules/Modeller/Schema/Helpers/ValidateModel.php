<?php
namespace AdminModules\Modeller\Schema\Helpers;

use Core\Translate;

class ValidateModel
{
    static function isValid(array $aData):bool
    {
        return empty(self::getValidationErrors($aData));
    }
    static function getValidationErrors(array $aData):array
    {
        $aOut = [];
        if(!isset($aData['name']) || empty($aData['name']))
        {
            $aOut['name'] = Translate::fromCode('You have to give your property a name');
        }
        if(!isset($aData['title']) || empty($aData['title']))
        {
            $aOut['title'] = Translate::fromCode('You have to give your model a title');
        }
        if(!isset($aData['form_field_type_id']) || empty($aData['form_field_type_id']))
        {
            $aOut['form_field_type_id'] = Translate::fromCode('You need to specify what kind of data is stored in this field');
        }

        return $aOut;
    }
}
