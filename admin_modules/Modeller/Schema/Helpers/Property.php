<?php
namespace AdminModules\Modeller\Schema\Helpers;

use Core\Cfg;
use Core\Logger;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Model\DataModel;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\FormFieldTypeQuery;
use Model\System\UI\IconQuery;

class Property
{

    static function create(array $aData):DataField
    {
        Logger::console($aData);
        $oDataField = new DataField();
        $oDataField->setDataModelId($aData['data_model_id']);
        $oDataField->setName($aData['name']);
        $oDataField->setPhpName($aData['php_name']);
        $oDataField->setLabel($aData['label']);
        $oDataField->setIconId($aData['icon_id']);
        $oDataField->setFormFieldTypeId($aData['form_field_type_id']);
        $oDataField->setFormFieldLookups($aData['form_field_lookups']);
        $oDataField->setDataTypeId($aData['data_type_id']);
        $oDataField->setRequired($aData['required']);
        $oDataField->setIsUnique($aData['is_unique']);
        $oDataField->setIsPrimaryKey($aData['is_primary_key']);
        $oDataField->setAutoIncrement($aData['auto_increment']);
        $oDataField->save();
        return $oDataField;
    }

    public static function fill($aData):array
    {
        /*
            [label] => Dit is een test
            [name] => test
            [form_field_type_id] => 7
            [] => component_button_size
        */
        $iDataModelId = DataModelQuery::create()->findOneByName($aData['table_name'])->getId();
        $iIconId = IconQuery::create()->findOneByName('tag')->getId();

        $sName = preg_replace('/^[^a-z]/', '', $aData['name']);
        $sName = preg_replace('/^[^a-z0-9_]+$/', '', $sName);

        $sPhpName = ucfirst($sName);

        $oFormFieldType = FormFieldTypeQuery::create()->findOneById($aData['form_field_type_id']);


        return [
            'data_model_id' => $iDataModelId,
            'name' => $sName,
            'php_name' => $sPhpName,
            'label' => $aData['label'],
            'icon_id' => $iIconId,
            'form_field_type_id' => $aData['form_field_type_id'],
            'form_field_lookups' => null,
            'data_type_id' => $oFormFieldType->getDataTypeId(),
            'required' => false,
            'is_unique' => false,
            'is_primary_key' => false,
            'auto_increment' => false
        ];
    }
}
