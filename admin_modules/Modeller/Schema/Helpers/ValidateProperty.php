<?php
namespace AdminModules\Modeller\Schema\Helpers;

use Core\Translate;

class ValidateProperty
{
    static function isValid(array $aData):bool
    {
        return empty(self::getValidationErrors($aData));
    }
    static function getValidationErrors(array $aData):array
    {
        $aOut = [];
        if(!isset($aData['name']) || empty($aData['name']))
        {
            $aOut['name'] = Translate::fromCode('You have to give your model a name');
        }
        if(!isset($aData['label']) || empty($aData['label']))
        {
            $aOut['label'] = Translate::fromCode('You have to give your property a label');
        }

        if(!isset($aData['form_field_type_id']) || empty($aData['form_field_type_id']))
        {
            $aOut['form_field_type_id'] = Translate::fromCode('You have to specify the kind of data you are intending to store.');
        }

        return $aOut;
    }
}
