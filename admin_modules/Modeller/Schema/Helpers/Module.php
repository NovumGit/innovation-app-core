<?php
namespace AdminModules\Modeller\Schema\Helpers;


use Core\Cfg;
use Core\Utils;
use Model\Module\ModuleQuery;

class Module
{

    static function create(string $sTitle):int
    {
        $sModuleFolder = ucfirst(Utils::slugify($sTitle, '_'));
        $sModuleFqn = 'AdminModules\\Custom\\' . Cfg::get('CUSTOM_NAMESPACE') . '\\' . $sModuleFolder . '\\Config';

        $oModule = ModuleQuery::create()->findOneByName($sModuleFqn);

        if(!$oModule instanceof \Model\Module\Module)
        {
            $oModule = new \Model\Module\Module();
            $oModule->setName($sModuleFqn);
            $oModule->setTitle($sTitle);
            $oModule->save();
        }
        return $oModule->getId();
    }


}
