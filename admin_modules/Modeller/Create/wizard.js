
$('a.post').click(function(e){
    e.preventDefault();
    if($(this).hasClass('disabled'))
    {
        return false;
    }
    $('#fld_do').val($(this).data('action'));
    $('#next_step').val($(this).attr('href'));
    const oMainForm = $('#main_form');

    oMainForm.attr('action', $(this).attr('href'));
    oMainForm.trigger('submit');
});
