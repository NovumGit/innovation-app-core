<?php
namespace AdminModules\Modeller\Create;

use Core\MainController;
use Core\Translate;

class WizardController extends MainController
{
    private function getSchema():string {
        return 'xx';
    }
    function run()
    {

        $sStep = $this->get('step', 'definition');
        $aSteps = [
            'definition' => ['title' => 'Nieuw schema aanmaken', 'layout' => 'getSchema']
        ];

        $aViewData = [
            'step' => $aSteps[$sStep],
            'tab_content' => $this->{$aSteps[$sStep]['layout']}()
        ];


        return [
            'title' => Translate::fromCode('Schema creëren'),
            'content' => $this->parse('Modeller/Create/wizard.twig', $aViewData)
        ];


    }
}
