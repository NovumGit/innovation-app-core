<?php
namespace AdminModules\Finance;

use AdminModules\ModuleConfig;
use Core\Cfg;
use Core\User;
use Crud\Link;
use Crud\Link_block_cms_module\CrudLink_block_cms_moduleManager;
use Model\Cms\SiteQuery;

class Config extends ModuleConfig{

    function getMenuExtraVars(): array
    {
        $aAllUrls = [
            ['url' => '/finance/bank/summary', 'label' => 'Saldo overzicht'],
            ['url' => '/finance/report/exploitation', 'label' => 'Rapportages'],
            ['url' => '/finance/money/tree', 'label' => 'Boeking categorieën'],
            ['url' => '/finance/bank/iban/addresses', 'label' => 'IBAN Adressenboek'],
            ['url' => '/finance/bank/iban/planned', 'label' => 'Geplande betalingen'],
            ['url' => '/finance/rules/list', 'label' => 'Definieer regels (If this then that)']
        ];

        $sRoleName = User::getMember()->getRole()->getName();
        $mVisible = Cfg::get('Finance', 'Access', $sRoleName);
        $aOut = [];

        foreach($aAllUrls as $aUrl)
        {
            if($mVisible === 'any')
            {
                $aOut[] = $aUrl;
            }
            else if(is_array($mVisible) && in_array($aUrl['url'], $mVisible))
            {
                $aOut[] = $aUrl;
            }
        }
        return $aOut;
    }

    function isEnabelable(): bool
    {
        return true;
    }
    function getModuleTitle(): string
    {
        return 'Financiën';
    }
    function getMenuData()
    {
        return [];

    }
}
