<?php
namespace AdminModules\Finance\Money;

use Model\Finance\BankTransactionCategoryQuery;

class MoneyTreeHelper
{

    static function getTree()
    {
        $aContent = [];
        $aContent[] = "        <div id=\"jstree\">";
        $aContent[] = '            <ul>';
        $aContent[] =                   MoneyTreeHelper::getSubTree(null);
        $aContent[] = '            </ul>';
        $aContent[] = '        </div>';
        $sContent = join(PHP_EOL, $aContent);
        return $sContent;
    }
    static function getSubTree($iBankTransactionCategoryParentId = null)
    {
        $aOut = [];
        $oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
        $oBankTransactionCategoryQuery->filterByParentId($iBankTransactionCategoryParentId);
        $oBankTransactionCategoryQuery->orderByDescription();
        $oBankTransactionCategories = $oBankTransactionCategoryQuery->find();

        if(!$oBankTransactionCategories->isEmpty())
        {
            foreach($oBankTransactionCategories as $oBankTransactionCategory)
            {
                $aState = [];
                $aState['Opened'] = true;

                $sDataAttr = '';
                if(!empty($aState))
                {
                    $sDataAttr = 'data-jstree="'.json_encode($aState).'"';
                }

                $aOut[] = "    <li id=\"c_{$oBankTransactionCategory->getId()}\" $sDataAttr>";
                $aOut[] = '     '.$oBankTransactionCategory->getDescription();

                $aChildren = BankTransactionCategoryQuery::create()->findByParentId($oBankTransactionCategory->getId());

                if(!$aChildren->isEmpty())
                {
                    $aOut[] = '     <ul>';
                    $aOut[] = '     '.self::getSubTree($oBankTransactionCategory->getId());
                    $aOut[] = '     </ul>';
                }

                $aOut[] = '    </li>';
            }
        }
        return join(PHP_EOL, $aOut);
    }
}