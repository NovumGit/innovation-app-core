<?php
namespace AdminModules\Finance\Money;

use Core\MainController;
use Core\Translate;

class TreeController extends MainController
{
    function run()
    {
        $this->addJsFile('/assets/plugins/jstree/dist/jstree.js');
        $this->addCssFile('/assets/plugins/jstree/dist/themes/default/style.min.css');

        $sTree  = MoneyTreeHelper::getTree();


        $aOverview = [
            'content' => $sTree
        ];

        return [
            'title' => Translate::fromCode('Geldboom'),
            'content' => $this->parse('Finance/Money/tree.twig', $aOverview),
            'top_nav' => $this->parse('Finance/Money/tree_top_nav.twig', []),
        ];
    }
}