<?php
namespace AdminModules\Finance\Money\Tree;

use AdminModules\GenericEditController;
use Crud\BankTransactionCategory\CrudBankTransactionCategoryManager;
use Crud\FormManager;

class Edit_itemController extends GenericEditController
{
	function getCrudManager(): FormManager
	{
		return new CrudBankTransactionCategoryManager();
	}
	function getPageTitle()
	{
		return $this->getCrudManager()->getEditFormTitle();
	}

	function getTopNav()
	{
		$aTopNav = [];
		return $this->parse('Finance/Money/Tree/top_nav.twig', $aTopNav);
	}


}