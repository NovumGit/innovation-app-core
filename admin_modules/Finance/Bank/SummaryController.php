<?php
namespace AdminModules\Finance\Bank;

use Core\MainController;
use Core\QueryMapper;
use Core\Translate;
use Core\UserRegistry;
use Model\Finance\BankAccountQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class SummaryController extends MainController
{

	function doBalanceRecalc()
	{
		$aBankAccounts = BankAccountQuery::create()->find();

		if(!$aBankAccounts->isEmpty())
		{
			foreach ($aBankAccounts as $oBankAccount)
			{

				$sQuery = "SELECT 	
							sum(amount) 
					    	FROM 
					    		bank_transaction 
				            WHERE
				            	bank_account_id = " . $oBankAccount->getId();

				$fAmount = QueryMapper::fetchVal($sQuery);


				$oBankAccount = BankAccountQuery::create()->findOneById($oBankAccount->getId());
				$oBankAccount->setBalance($fAmount);
				$oBankAccount->save();
			}
		}
		Translate::fromCode("Saldo's aangepast");
		$this->redirect('/finance/bank/summary');
	}
	function run()
	{

		$oBankAccountQuery = BankAccountQuery::create();
		$oBankAccountQuery->filterByIsActive(true);
		$oBankAccountQuery->orderByIsSavings(Criteria::DESC);
		$oBankAccountQuery->orderByBalance(Criteria::DESC);
		$aBankAccounts = $oBankAccountQuery->find();

		$fTotals = 0;
		foreach ($aBankAccounts as $oBankAccount)
		{
			$fTotals += $oBankAccount->getBalance();
		}



		$aData = [
			'menu_size' => UserRegistry::get('menu_size') == 'minimal' ? 'col-sm-4' : 'col-sm-6',
			'bank_accounts' => $aBankAccounts,
			'totals' => $fTotals
		];

		return [
			'title' => Translate::fromCode('Saldo overzicht'),
			'content' => $this->parse('Finance/Bank/summary.twig', $aData)
		];

	}
}