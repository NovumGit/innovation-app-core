<?php
namespace AdminModules\Finance\Bank\Iban;

use AdminModules\GenericOverviewController;
use Core\DeferredAction;
use Core\Translate;
use Crud\BankIbanAddressBook\CrudBankIbanAddressBookManager;
use Crud\FormManager;
use Model\Finance\BankIbanAddressBookQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class AddressesController extends GenericOverviewController
{
    function __construct($aGet, $aPost)
    {
        DeferredAction::register('transactions_url', $this->getRequestUri());
        $this->setEnablePaginate(25);
        parent::__construct($aGet, $aPost);
    }

    function getManager(): FormManager
    {
        return new CrudBankIbanAddressBookManager();
    }

    function getModule(): string
    {
        return 'BankIbanAddressBook';
    }

    function getTitle(): string
    {
        return Translate::fromCode("Iban adressen");
    }
    function getQueryObject(): ModelCriteria
    {
        return BankIbanAddressBookQuery::create();
    }


}