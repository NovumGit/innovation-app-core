<?php
namespace AdminModules\Finance\Bank\Iban;

use AdminModules\GenericEditController;
use Helper\FilterHelper;
use Core\DeferredAction;
use Core\Translate;
use Crud\BankIbanAddressBook\CrudBankIbanAddressBookManager;
use Crud\BankTransaction\CrudBankTransactionManager;
use Crud\CrudViewManager;
use Crud\FormManager;
use Exception\LogicException;
use Model\Finance\BankTransactionQuery;
use Model\Setting\CrudManager\CrudView;

class AliasController extends GenericEditController
{
	function getAfterSaveUrl($iId)
	{
		$sTransactionsUrl = DeferredAction::get('transactions_url');
		if($sTransactionsUrl)
		{
			$this->redirect($sTransactionsUrl);
		}
	}
	function getPageTitle()
	{
		Translate::fromCode("IBAN Alias");
	}

	function getTopNav()
	{
		$sTransactionsUrl = DeferredAction::get('transactions_url');
		$aData = [
			'back_url' =>  $sTransactionsUrl
		];
		return $this->parse('Finance/Bank/Iban/top_nav.twig', $aData);
	}


	function getEditTemplate()
    {
        return 'Finance/Bank/Iban/alias.twig';
    }

    function getCrudManager(): FormManager
	{
		return new CrudBankIbanAddressBookManager();
	}

	function getExtraFields():array
    {

        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';

        $oBankTransactionQuery = BankTransactionQuery::create();
        $id = $this->get('id');
        $oBankTransactionQuery->filterByCounterBankIbanId($id);


        $oQueryObject = $oBankTransactionQuery;
        $oQueryObject->orderBy($sSortField, $sSortDirection);

        $oCrudManager = new CrudBankTransactionManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudManager);
        $iTabId = $this->get('tab');
        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);
        $aFields = CrudViewManager::getFields($oCrudview);

        $oQueryObject = \Helper\FilterHelper::applyFilters($oQueryObject, $iTabId);
        $oQueryObject = FilterHelper::generateVisibleFilters($oQueryObject, $aFilters, null, null);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);

        $iCurrentPage = $this->get('p', 1, false, 'numeric');
        $oCrudManager->setOverviewData($oQueryObject, $iCurrentPage, 35);

        $aViewData['overview_table'] = $oCrudManager->getOverviewHtml($aFields);

        return $aViewData;
    }

}