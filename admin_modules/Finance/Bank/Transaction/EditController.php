<?php
namespace AdminModules\Finance\Bank\Transaction;

use AdminModules\GenericEditController;
use Core\DeferredAction;
use Core\Translate;
use Crud\BankTransaction\CrudBankTransactionManager;
use Crud\FormManager;

class EditController extends GenericEditController
{
	function getTopnav()
	{
		$sTransactionsUrl = DeferredAction::get('transactions_url');
		$aView = [
			'overview_url' => $sTransactionsUrl
		];
		return $this->parse('Finance/Bank/Transaction/top_nav.twig', $aView);
	}
    function getCrudManager(): FormManager
    {
        return new CrudBankTransactionManager();
    }

    function getPageTitle()
    {
        Translate::fromCode("Bank transactie bewerken");
    }
}