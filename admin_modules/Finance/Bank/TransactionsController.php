<?php
namespace AdminModules\Finance\Bank;

use AdminModules\GenericOverviewController;
use Core\DeferredAction;
use Core\Translate;
use Crud\BankTransaction\CrudBankTransactionManager;
use Crud\FormManager;
use Model\Finance\BankAccountQuery;
use Model\Finance\BankTransactionQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class TransactionsController extends GenericOverviewController
{

	function __construct($aGet, $aPost)
	{
		DeferredAction::register('transactions_url', $this->getRequestUri());
		parent::__construct($aGet, $aPost);
		$this->setEnablePaginate(50);
	}

	function getModule(): string
	{
		return 'BankTransaction';
	}

	function getExtraTopNavVars()
	{
		$oBankAccountQuery = BankAccountQuery::create();
		$oBankAccountQuery->orderByName();
		$aBankAccounts = $oBankAccountQuery->find();

		$oBankAccount = null;
		$iBackAccountId = $this->get('bank_account_id');
		$iUnBookedOnly = $this->get('unbooked_only');

		if($iBackAccountId)
		{
			$oBankAccount = BankAccountQuery::create()->findOneById($iBackAccountId);
		}
		return [
			'unbooked_only' => $iUnBookedOnly,
			'bank_account_id' => $iBackAccountId,
			'bank_accounts' => $aBankAccounts,
			'bank_account' => $oBankAccount,
		];
	}
	function getTopNavTemplate()
	{
		return 'Finance/Bank/transactions_top_nav.twig';
	}
	function getTitle(): string
	{
		$iBankAccountId = $this->get('bank_account_id');
		if($iBankAccountId)
		{
			return BankAccountQuery::create()->findOneById($iBankAccountId)->getName();
		}
		return Translate::fromCode('Alle rekeningen');
	}
	function getManager(): FormManager
	{
		return new CrudBankTransactionManager();
	}
	function getQueryObject(): ModelCriteria
	{
		$oQueryObject = BankTransactionQuery::create();

		$sSort = $this->get('sort');
        $sDirection = $this->get('dir');
		if($sSort)
        {
            if($sSort == 'transaction_time')
            {
                $iDirection = ($sDirection == 'desc') ? Criteria::DESC : Criteria::ASC;
                $oQueryObject->orderByTransactionTime($iDirection);
            }
            else if($sSort == 'bank_account_id')
            {
                $iDirection = ($sDirection == 'desc') ? Criteria::DESC : Criteria::ASC;
                $oQueryObject->useBankAccountQuery()->orderByName($iDirection)->endUse();
            }
            else if($sSort == 'amount')
            {
                $iDirection = ($sDirection == 'desc') ? Criteria::DESC : Criteria::ASC;
                $oQueryObject->orderByAmount($iDirection);
            }

        }



		$iUnbookedOnly = $this->get('unbooked_only');

		if($iUnbookedOnly == '1')
		{
			$oQueryObject->filterByBankTransactionCategoryId(null, Criteria::ISNULL);
		}


		$iBankAccountId = $this->get('bank_account_id');
		if($iBankAccountId)
		{
			$oQueryObject->filterByBankAccountId($iBankAccountId);
		}
		$oQueryObject->orderByTransactionTime(Criteria::DESC);
		return $oQueryObject;
	}
}
