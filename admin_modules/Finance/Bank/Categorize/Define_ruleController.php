<?php
namespace AdminModules\Finance\Bank\Categorize;

use Core\MainController;
use Core\Translate;
use Model\Finance\BankTransactionCategoryQuery;
use Model\Finance\BankTransactionQuery;
use Model\Finance\Base\BankIbanAddressBookQuery;

class Define_ruleController extends MainController
{
	function run()
	{
		$iTransactionId = $this->get('transaction_id', true, null, 'numeric');
		$oTransaction = BankTransactionQuery::create()->findOneById($iTransactionId);

		$aTree = BankTransactionCategoryQuery::getTree();

		$oBankIbanAddressBookQuery = BankIbanAddressBookQuery::create();
		$oBankIbanAddressBookQuery->orderByDescription();

		$aData = [
			'transaction' => $oTransaction,
			'iban_accounts' => $oBankIbanAddressBookQuery,
			'category_tree' => $aTree
		];

		return [
			'title' => Translate::fromCode('Transacties categorisatie regel instellen'),
			'top_nav' => $this->parse('Finance/Bank/Categorize/top_nav.twig', []),
			'content' => $this->parse('Finance/Bank/Categorize/define_rule.twig', $aData)
		];
	}
}