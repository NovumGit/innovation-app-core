<?php
namespace AdminModules\Finance\Bank\Account;

use AdminModules\GenericEditController;
use Core\StatusMessage;
use Core\Translate;
use Crud\BankAccount\CrudBankAccountManager;
use Crud\FormManager;
use Exception\LogicException;
use Model\Finance\BankAccount;
use Model\Finance\BankAccountQuery;
use Core\Config;

class EditController extends GenericEditController
{
    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doStore()
    {
        $oCrudManager = $this->getCrudManager();

        if(!$oCrudManager instanceof CrudBankAccountManager)
        {
            throw new LogicException("Expected an instance of CrudBankAccountManager.");
        }

        $aData = $this->post('data', null);

        if($oCrudManager->isValid($aData, $this->getFormLayoutKey()))
        {
            $oBankAccount = $oCrudManager->save($aData);

            if(!$oBankAccount instanceof BankAccount)
            {
                throw new LogicException("Expected an instance of BankAccount.");
            }

            if(isset($_FILES['data']['tmp_name']) && $_FILES['data']['error']['balance'] === 0)
            {
                $sBankImageDir = Config::getDataDir() . '/finance/bank-avatars';
                if(!is_dir($sBankImageDir))
                {
                    mkdir($sBankImageDir, 0777, true);
                }
                $sExt = pathinfo($_FILES['data']['name']['balance'], PATHINFO_EXTENSION);

                if(!in_array($sExt, ['png', 'jpg']))
                {
                    StatusMessage::warning(Translate::fromCode("Alleen png en jpg afbeeldingen worden ondersteund."));

                }
                else
                {
                    $oBankAccount->setHasAvatar(true);
                    $oBankAccount->setAvatarFileExt($sExt);
                    $oBankAccount->save();
                    $sDestination = $sBankImageDir . '/' . $oBankAccount->getId() . '.' . $sExt;
                    move_uploaded_file($_FILES['data']['tmp_name']['balance'], $sDestination);
                }
            }

            $this->redirect('/finance/bank/summary');
        }
        else
        {

            $oCrudManager->registerValidationErrorStatusMessages($aData, $this->getFormLayoutKey());
        }
    }

    function getPageTitle()
    {
        $iBankAccountId = $this->get('id',null ,true, 'numeric');
        $oBankAccount = BankAccountQuery::create()->findOneById($iBankAccountId);
        return $oBankAccount->getName();
    }

    function getCrudManager(): FormManager
    {
        return new CrudBankAccountManager();
    }
}