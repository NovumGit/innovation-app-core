<?php

namespace AdminModules\Finance\Rules;

use AdminModules\GenericOverviewController;
use Core\Translate;
use Crud\Rule\CrudRuleManager as ManagerObject;
use Crud\FormManager;
use Model\Rule\RuleQuery as QueryObject;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class ListController extends GenericOverviewController
{
    function getMainTemplate()
    {
        return '/home/anton/Documents/sites/hurah/admin_modules/./overview.twig';
    }

    function getTopNavTemplate()
    {
        return '/home/anton/Documents/sites/hurah/admin_modules/./top_nav_overview.twig';
    }

    function getModule(): string
    {
        return 'Rule';
    }

    function getTitle(): string
    {
        return Translate::fromCode('Rules');
    }

    function getQueryObject(): ModelCriteria
    {
        $oQueryObject = QueryObject::create();
        return $oQueryObject;
    }

    function getManager(): FormManager
    {
        return new ManagerObject();
    }
}