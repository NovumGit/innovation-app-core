<?php
namespace AdminModules\Error;

use Core\MainController;
use Core\Logger;
use Core\Translate;

class PagenotFoundController extends MainController
{

    function run()
    {
        $sReferer = 'not set';
        if(isset($_SERVER['HTTP_REFERER']))
        {
            $sReferer = $_SERVER['HTTP_REFERER'];
        }

        $aData = [
            'controller_wanted' => $_SESSION['CONTROLLER_WANTED']
        ];
        Logger::pageNotFound(Translate::fromCode('Pagina niet gevonden, url').$_SERVER['REQUEST_URI'].' referrer: '.$sReferer);

        $aView['content'] = $this->parse('Error/404.twig', $aData);

        return $aView;
    }

}