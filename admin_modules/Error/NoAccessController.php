<?php
namespace AdminModules\Error;

use Core\MainController;
use Core\Logger;

class NoAccessController extends MainController
{
    function run()
    {
        $sReferer = 'not set';
        if(isset($_SERVER['HTTP_REFERER']))
        {
            $sReferer = $_SERVER['HTTP_REFERER'];
        }
        Logger::warning("Geen toegang tot deze module vanaf referer ".$sReferer);
        $aView['content'] = $this->parse('Error/no_access.twig', []);
        return $aView;
    }
}