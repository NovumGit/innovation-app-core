<?php
namespace AdminModules\Apps\App\Base;

use AdminModules\GenericEditController;
use Crud\App\CrudAppManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Apps\App instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudAppManager();
	}


	public function getPageTitle(): string
	{
		return "Apps beheren";
	}
}
