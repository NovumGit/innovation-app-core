<?php
namespace AdminModules\Apps\App\Creator;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Crud\App\CrudAppManager;
use LowCode\App;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentTypeQuery;

/**
 * Skeleton subclass for drawing a list of App records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class InitializeController extends MainController
{
    function __construct($aGet, $aPost)
    {

        if(isset($aPost['app_creator']))
        {
            $oCrudAppManager = new CrudAppManager();

            if($oCrudAppManager->isValid($aPost['app_creator']))
            {
                $oApp = $oCrudAppManager->store($aPost['app_creator']);
                $sAction = '?section=basics&block=screens&app_id=' . $oApp->getId();
                $aButtons = [
                    new StatusMessageButton("Ga verder", $sAction, 'Yay', 'success')
                ];
                StatusModal::success("wijzigingen opgeslagen", "Yay", $aButtons);
            }
            else
            {
                $sAction = '/';
                $aButtons = [
                    new StatusMessageButton("Ga verder", $sAction, 'Yay', 'danger')
                ];
                StatusModal::danger("wijzigingen opgeslagen", "Yay", $aButtons);
            }
        }

        if(isset($aPost['screen_definition_form']) && $aPost['screen_definition_form']['label'])
        {

            $oCrudUiManager = new UIComponent();
            $oCrudUiManager->setAppId($_GET['app_id']);
            $oUIComponentTypeQuery = UIComponentTypeQuery::create();
            $oUIComponentType = $oUIComponentTypeQuery->findOneByComponentClass(\LowCode\Component\View\Component::class);

            $oCrudUiManager->setUiComponentTypeId($oUIComponentType->getId());
            $oCrudUiManager->setLabel($aPost['screen_definition_form']['label']);
            $oCrudUiManager->save();
        }

        /*
         *         echo __FILE__ . ':' . __LINE__."<br>";
        echo "<pre>" . print_r($aGet , true) . "</pre>";
        echo "<pre>" . print_r($aPost , true) . "</pre>";
        exit();
        echo "<pre>" . print_r($aGet , true) . "</pre>";
        echo "<pre>" . print_r($aPost , true) . "</pre>";

        exit();
        */
        parent::__construct($aGet, $aPost);
    }

    /**
     * @return array
     * @throws \Exception
     */
    function run()
    {
        $sCurrentDir = (new Reflector($this))->getClassDir();

        $sJson = file_get_contents($sCurrentDir . '/app-definition.json');
        $aApp = JsonUtils::decode($sJson);

        $oApp = new App($aApp, array_merge_recursive($_GET, $_POST));
        $aAppData = $oApp->render();

        $aVars = [
            'base_url' => $this->getRequestUri(false),
            'app' => $aAppData
        ];

        $aData  =
        [
            'show_navbar_top' => Setting::get('show_navbar_top'),
            'show_sidebar_left' => Setting::get('show_sidebar_left'),
            'content' => $this->parse('Apps/App/Creator/wizard.twig', $aVars),
            'title' => $aApp['title'],
        ];
        return $aData;

    }
}
