<?php
namespace AdminModules\Apps\App\Designer;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Utils;
use Crud\App\CrudAppManager;
use LowCode\App;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;

/**
 * Skeleton subclass for drawing a list of App records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class ComponentController extends MainController
{

    function doAddComponent()
    {
        $iUiComponentTypeId = $this->post('ui_component_type_id');
        $iParentId = $this->post('parent_id');
        $sLabel = $this->post('label');
        $sParentLocation = $this->post('parent_location');

        $oComponentHelper = new ComponentHelper($iParentId);
        $oApp = $oComponentHelper->getApp();

        $oUIComponent = new UIComponent();
        $oUIComponent->setLabel($sLabel);
        $oUIComponent->setAppId($oApp->getId());
        $oUIComponent->setParentId($iParentId);
        $oUIComponent->setParentLocation($sParentLocation);
        $oUIComponent->setUiComponentTypeId($iUiComponentTypeId);
        $oUIComponent->save();

        Utils::jsonOk();


    }
    function run()
    {
        // TODO: Implement run() method.
    }
}
