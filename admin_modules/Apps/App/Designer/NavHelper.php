<?php
namespace AdminModules\Apps\App\Designer;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Crud\App\CrudAppManager;
use LowCode\App;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;

class NavHelper
{
    static function getVars($iUiComponent):array
    {
        $oComponentHelper = new ComponentHelper($iUiComponent);

        $aSiblings = $oComponentHelper->getSiblings();


        return [
            'app' => $oComponentHelper->getApp(),
            'ui_component' => $oComponentHelper->getCurrent(),
            'siblings' => $oComponentHelper->getSiblings()

        ];

    }

}
