<?php
namespace AdminModules\Apps\App\Designer;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Crud\App\CrudAppManager;
use LowCode\App;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;

/**
 * Skeleton subclass for drawing a list of App records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CanvasController extends MainController
{
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $iAppId = $this->get('app_id', null,false,'numeric');
        AppHelper::redirectFirstComponent($iAppId);
    }

    function run()
    {
        $this->addCssFile('/assets/app_designer/app.css');
        $iUiComponentId = $this->get('ui_component_id', null, true, 'numeric');
        $oComponentHelper = new ComponentHelper($iUiComponentId);

        $oApp = $oComponentHelper->getCurrent()->getApp();


        // This part is for debugging, it does what happens in the template so errors show up in readable format
        $aChildren = $oComponentHelper->getChildren();

        if(!$aChildren->isEmpty())
        {
            foreach($aChildren as $oChild)
            {
                if($oChild instanceof UIComponent)
                {
                    if($oChild->isConfigured())
                    {
                        $oChild->getForm();
                        $oChild->previewRender();
                    }
                    else
                    {
                        $oChild->preview();
                    }
                }
            }

        }
        // End debugging part

        $sTitle = "App designer";
        $aCanvasData = [
            'app' => $oApp,
            'children' => $aChildren,
            'properties_chute' => $this->parse('Apps/App/Designer/properties_chute.twig', PropertiesHelper::getVars($iUiComponentId)),
            'ui_component' => $oComponentHelper->getCurrent()
        ];



        $aData  =
            [
                'top_nav' => $this->parse('Apps/App/Designer/canvas_nav.twig', NavHelper::getVars($iUiComponentId)),
                'content' => $this->parse('Apps/App/Designer/canvas.twig', $aCanvasData),
                'title' => $sTitle,
            ];

        return $aData;

    }
}
