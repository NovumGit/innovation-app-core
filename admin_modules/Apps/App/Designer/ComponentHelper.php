<?php
namespace AdminModules\Apps\App\Designer;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Crud\App\CrudAppManager;
use LowCode\Component\IComponent;
use Exception\LogicException;
use Model\System\App;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;

class ComponentHelper
{
    private $iUiComponent;
    function __construct($iUiComponent)
    {
        $this->iUiComponent = $iUiComponent;
    }
    function getCurrent():UIComponent
    {
        $oUIComponent = UIComponentQuery::create()->findOneById($this->iUiComponent);
        return $oUIComponent;
    }
    function getApp():App
    {
        return $this->getCurrent()->getApp();
    }

    function getChildren():ObjectCollection
    {
        return UIComponentQuery::create()->filterByParentId($this->getCurrent()->getId())->find();
    }

    public function getSiblings():ObjectCollection
    {
        $oComponent = $this->getCurrent();

        $oUIComponentQuery = UIComponentQuery::create();
        $oUIComponentQuery->filterById($oComponent->getId(), Criteria::NOT_EQUAL);

        if($oComponent->getParentId())
        {
            $oUIComponentQuery->findByParentId($oComponent->getParentId());
            $aUiComponents = $oUIComponentQuery->find();
        }
        else
        {
            $oUIComponentQuery->filterByAppId($oComponent->getAppId());
            $oUIComponentQuery->filterByParentId(null, Criteria::ISNULL);
            $aUiComponents = $oUIComponentQuery->find();
        }
        return $aUiComponents;
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getComponentType():IComponent
    {

        $oUiComponent = $this->getCurrent();

        $oUiComponentType = $oUiComponent->getUiComponentType();
        $sComponentFqn = $oUiComponentType->getComponentClass();

        $oComponentType = new $sComponentFqn([], []);

        if(!$oComponentType instanceof IComponent)
        {
            throw new LogicException("Expected an instance of IComponent");
        }
        return $oComponentType;
    }

}
