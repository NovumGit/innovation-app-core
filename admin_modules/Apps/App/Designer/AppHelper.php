<?php
namespace AdminModules\Apps\App\Designer;


use Core\Utils;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use UserModel\View\Component_view;

class AppHelper
{
    static function redirectFirstComponent(int $iAppId = null)
    {
        if($iAppId)
        {
            $oUIComponentQuery = UIComponentQuery::create();
            $oUIComponentQuery->filterByParentId(null, Criteria::ISNULL);
            $oUIComponentQuery->filterByAppId($iAppId);

            $oUIComponent = $oUIComponentQuery->findOneOrCreate();

            if($oUIComponent->isNew())
            {
                $oUIComponentType = UIComponentTypeQuery::create()->findOneByComponentClass('LowCode\\Component\\View\\Component');
                $oUIComponent->setUiComponentTypeId($oUIComponentType->getId());
                $oUIComponent->setLabel('Home page');
                $oUIComponent->save();

                $oComponent_view = new Component_view();
                $oComponent_view->setTitle('Initial view');
                $oComponent_view->setDefaultActive(true);
                $oComponent_view->setUiComponentId($oUIComponent->getId());
                $oComponent_view->save();

            }


            $oComponent = UIComponentQuery::create()->findOneByAppId($iAppId);

            if(!$oComponent instanceof UIComponent)
            {

            }


            Utils::redirect(Utils::getRequestUri(false) . '?ui_component_id=' . $oComponent->getId());
        }

    }
}
