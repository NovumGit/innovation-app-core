<?php
namespace AdminModules\Apps\App\Designer;

use Core\Json\JsonUtils;
use Core\MainController;
use Core\Reflector;
use Core\Setting;
use Core\StatusMessageButton;
use Core\StatusModal;
use Crud\App\CrudAppManager;
use Exception\LogicException;
use LowCode\App;
use LowCode\Component\IComponent;
use LowCode\ComponentFactory;
use LowCode\System\CrudComponentManagerFactory;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Model\System\UI\UIComponentTypeQuery;

class PropertiesHelper
{
    /**
     * @param $iUiComponent
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    static function getVars($iUiComponent):array
    {
        $oComponentHelper = new ComponentHelper($iUiComponent);

        $oComponentType = $oComponentHelper->getComponentType();

        $aProps = [
            "type" => \LowCode\Component\Form\Component::class,
            'title' => $oComponentType->getName(),
            'layout_key' =>  $oComponentType->getComponentXml()->getId(),
            'id' => $oComponentType->getComponentXml()->getId(),
            "render_style" => "PlainForm",
            'manager' => CrudComponentManagerFactory::getFqn($oComponentType)
        ];
        $aState = [];

        $oApp = new App($aProps, $aState);

        // $oFormComponent = ComponentFactory::produce($sFormComponent, $aProps, $aState);

        // $aForm = $oFormComponent->render();


        //$sId = $oComponentType->getComponentXml()->getId()

        return [
            'form' => $oApp->render(),
            'component_type' => $oComponentType,
            'accepts' => $oComponentType->accepts()
        ];


    }

}
