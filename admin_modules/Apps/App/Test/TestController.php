<?php

namespace AdminModules\Apps\App\Test;

use AdminModules\Apps\App\Designer\AppHelper;
use Core\MainController;
use Core\Reflector;
use Model\System\UI\UIComponentQuery;

class TestController extends MainController
{
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $iAppId = $this->get('app_id', null,false,'numeric');
        AppHelper::redirectFirstComponent($iAppId);
    }


    function run()
    {
        $this->addCssFile('/assets/app_designer/app.css');

        $iUiComponentId = $this->get('ui_component_id');
        $oUiComponent = UIComponentQuery::create()->findOneById($iUiComponentId);

        $sType = $oUiComponent->getUIComponentType()->getComponentClass();

        $aProperties = $oUiComponent->getUIComponentType()->getComponent()->getConfig($oUiComponent->getId());
        $aProperties['id'] = 'rendered_' . $aProperties['id'];

        $sManager = $aProperties['ui_manager'];
        $oManager = new $sManager();

        $oQueryClass = $oManager->getQueryObject();
        $aProperties['query_object'] = (new Reflector($oQueryClass))->getName();


        $aProperties['type'] = $sType;

        $aState = [];
        $oApp = new \LowCode\App($aProperties, $aState);

        $aRenderedResult =  $oApp->render();

        return [
            'top_nav' => $this->parse('Apps/App/Test/result_nav.twig', []),
            'content' => $this->parse('Apps/App/Test/result.twig', $aRenderedResult)
        ];

    }

}
