<?php
namespace AdminModules\Review;

use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\IConfigurableCrud;
use Crud\Product_review\CrudProduct_reviewManager;
use Exception\LogicException;
use Helper\FilterHelper;
use Model\ProductReview;
use Model\ProductReviewQuery;
use Model\Setting\CrudManager\CrudView;

class ModerationController extends MainController
{
    function doDelete()
    {
        $iTab = $this->get('tab');
        $iProductReviewId = $this->get('id', null, true, 'numeric');
        $oProductReview = ProductReviewQuery::create()->findOneById($iProductReviewId);
        if($oProductReview  instanceof ProductReview)
        {
            StatusMessage::success("Review verwijderd.");
            $oProductReview->delete();
        }
        $this->redirect('/review/moderation?tab='.$iTab);
    }
    function doConfirmDelete()
    {
        $iProductReviewId = $this->get('id', null, true, 'numeric');
        $iTab = $this->get('tab', null, true, 'numeric');
        $sMessage = Translate::fromCode("Weet je zeker dat je deze review wilt verwijderen?");
        $sTitle = Translate::fromCode("Zeker weten?");
        $aButtons = [
            new StatusMessageButton(Translate::fromCode('Ja verwijderen'), "/review/moderation?tab=$iTab&_do=Delete&id=$iProductReviewId", '', 'success'),
            new StatusMessageButton(Translate::fromCode('Annuleren'), "/review/moderation?tab=$iTab", '', 'warning'),
        ];
        StatusModal::danger($sMessage, $sTitle, $aButtons);
    }
    function doApprove()
    {
        $iReviewId = $this->get('review_id', null, true, 'numeric');
        $oProductReview = ProductReviewQuery::create()->findOneById($iReviewId);
        $oProductReview->setApproved(!$oProductReview->getApproved());
        $oProductReview->save();
        $this->redirect('/review/moderation');
    }
    function run()
    {
        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'id';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'asc';

        $oProductReviewQuery = ProductReviewQuery::create();
        $oProductReviewQuery->orderBy($sSortField, $sSortDirection);

        $oCrudProduct_reviewManager = new CrudProduct_reviewManager();
        $aCrudViews = CrudViewManager::getViews($oCrudProduct_reviewManager);

        if(!$iTabId)
        {
            $oCrudView = current($aCrudViews);

            if(!$oCrudView instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudView));
            }
            $iTabId = $oCrudView->getId();
            $this->redirect('/review/moderation?tab='.$iTabId);
        }
        else
        {
            $oCrudView = $aCrudViews[$iTabId];
        }
        $oProductReviewQuery = FilterHelper::applyFilters($oProductReviewQuery, $iTabId);
        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $oProductReviewQuery = FilterHelper::generateVisibleFilters($oProductReviewQuery, $aFilters);

        $oCrudProduct_reviewManager->setOverviewData($oProductReviewQuery);
        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);

        $aFields = CrudViewManager::getFields($oCrudView);
        $aViewData['overview_table'] = $oCrudProduct_reviewManager->getOverviewHtml($aFields);

        $aTopNavVars =
            [
                'new_url' => $oCrudProduct_reviewManager->getCreateNewUrl(),
                'new_title' => $oCrudProduct_reviewManager->getNewFormTitle(),
                'implements_configurable_crud' => ($oCrudProduct_reviewManager instanceof IConfigurableCrud),
                'module' => 'Product_review',
                'crud_views' => $aCrudViews,
                'manager' => 'Product_reviewManager',
                'tab_id' => $iTabId
            ];
        $sParseResult = $this->parse('generic_overview.twig', $aViewData);
        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);

        $aView =
            [
                'title' => 'Product reviews',
                'top_nav' => $sTopNav,
                'content' => $sParseResult
            ];
        return $aView;
    }
}

