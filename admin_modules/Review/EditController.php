<?php
namespace AdminModules\Review;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\Product_review\CrudProduct_reviewManager;

class EditController extends MainController
{
    const sFormLayoutKey = 'product_review_editor';
    private $oCrudUserManager;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $aReview = $this->post('data', null);
        $this->oCrudUserManager = new CrudProduct_reviewManager($aReview);
    }
    function doStore()
    {
        $aProductReview = $this->post('data', null);
        if($this->oCrudUserManager->isValid($aProductReview))
        {
            $sAddUrl = '';
            if(isset($_GET['tab']))
            {
                $sAddUrl = '&tab='.$_GET['tab'];
            }
            StatusMessage::success("Wijzigingen opgeslagen");
            $oProductReview = $this->oCrudUserManager->save($aProductReview);
            $bNext = $this->post('next_overview', null, false);
            if($bNext == '1')
            {
                $this->redirect('/review/moderation?id='.$oProductReview->getId());
            }
            else
            {
                $this->redirect('/review/edit?id=1'.$oProductReview->getId().$sAddUrl);
            }
        }
        else
        {
            $this->oCrudUserManager->registerValidationErrorStatusMessages($aProductReview);
        }
    }
    function run()
    {
        DeferredAction::register('after_pick_product', $this->getRequestUri());
        $aProduct_review = $this->post('data', null);
        $aProduct_review['id'] = $this->get('id');

        $oUser = $this->oCrudUserManager->getModel($aProduct_review);
        $aParseData['edit_form'] = $this->oCrudUserManager->getRenderedEditHtml($oUser, self::sFormLayoutKey);
        $aTopnavEdit = [
            'module' => 'Product_review',
            'manager' => 'Product_reviewManager',
            'overview_url' => $this->oCrudUserManager->getOverviewUrl(),
            'edit_config_key' => self::sFormLayoutKey,
            'name' => self::sFormLayoutKey,
            'title' => 'Product review scherm instellen'
        ];
        $aView['top_nav'] = $this->parse('top_nav_edit.twig', $aTopnavEdit);
        $aView['content'] = $this->parse('edit.twig', $aParseData);
        $aView['title'] = Translate::fromCode('Product review').' '.($aProduct_review['id'] ? Translate::fromCode('bewerken') : Translate::fromCode('toevoegen'));

        return $aView;
    }
}