<?php
namespace AdminModules\System\Message_translation\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FormManager;
use Crud\MessageTranslation\CrudMessageTranslationManager;
use Model\Custom\NovumOverheid\MessageTranslation;
use Model\Custom\NovumOverheid\MessageTranslationQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Message_translation instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Bericht vertaling";
	}


	public function getModule(): string
	{
		return "MessageTranslation";
	}


	public function getManager(): FormManager
	{
		return new CrudMessageTranslationManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return MessageTranslationQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof MessageTranslation){
		    LogActivity::register("System", "Bericht vertaling verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Bericht vertaling verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Bericht vertaling niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Bericht vertaling item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
