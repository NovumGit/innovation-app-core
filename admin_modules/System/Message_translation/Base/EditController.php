<?php
namespace AdminModules\System\Message_translation\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\MessageTranslation\CrudMessageTranslationManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Message_translation instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudMessageTranslationManager();
	}


	public function getPageTitle(): string
	{
		return "Bericht vertaling";
	}
}
