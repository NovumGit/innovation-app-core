<?php
namespace AdminModules\System\Mime_type\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\MimeType\CrudMimeTypeManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Mime_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudMimeTypeManager();
	}


	public function getPageTitle(): string
	{
		return "Data typen";
	}
}
