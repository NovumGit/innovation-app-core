<?php
namespace AdminModules\System\Ui_component_type_accepts\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\UIComponentTypeAccepts\CrudUIComponentTypeAcceptsManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Ui_component_type_accepts instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudUIComponentTypeAcceptsManager();
	}


	public function getPageTitle(): string
	{
		return "UI component type accepts";
	}
}
