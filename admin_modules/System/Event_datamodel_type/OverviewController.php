<?php
namespace AdminModules\System\Event_datamodel_type;

use AdminModules\Custom\System\Event_datamodel_type\Base;

/**
 * Skeleton subclass for drawing a list of EventDatamodelType records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
