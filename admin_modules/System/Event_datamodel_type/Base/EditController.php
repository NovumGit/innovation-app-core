<?php
namespace AdminModules\System\Event_datamodel_type\Base;

use AdminModules\GenericEditController;
use Crud\EventDatamodelType\CrudEventDatamodelTypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Event_datamodel_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudEventDatamodelTypeManager();
	}


	public function getPageTitle(): string
	{
		return "Event datamodel";
	}
}
