<?php
namespace AdminModules\System\Event_form\Base;

use AdminModules\GenericEditController;
use Crud\EventForm\CrudEventFormManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Event_form instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudEventFormManager();
	}


	public function getPageTitle(): string
	{
		return "Event form";
	}
}
