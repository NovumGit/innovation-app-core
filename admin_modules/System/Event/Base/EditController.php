<?php
namespace AdminModules\System\Event\Base;

use AdminModules\GenericEditController;
use Crud\Event\CrudEventManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Event instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudEventManager();
	}


	public function getPageTitle(): string
	{
		return "Event";
	}
}
