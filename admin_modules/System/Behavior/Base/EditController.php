<?php
namespace AdminModules\System\Behavior\Base;

use AdminModules\GenericEditController;
use Crud\Behavior\CrudBehaviorManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Behavior instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudBehaviorManager();
	}


	public function getPageTitle(): string
	{
		return "Behaviors";
	}
}
