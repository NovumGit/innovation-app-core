<?php
namespace AdminModules\System\Crud_editor_button_visibile_filter\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudEditorButtonVisibileFilter\CrudCrudEditorButtonVisibileFilterManager;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_button_visibile_filter instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Knop filters";
	}


	public function getModule(): string
	{
		return "CrudEditorButtonVisibileFilter";
	}


	public function getManager(): FormManager
	{
		return new CrudCrudEditorButtonVisibileFilterManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonVisibileFilterQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof CrudEditorButtonVisibileFilter){
		    LogActivity::register("System", "Knop filters verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Knop filters verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Knop filters niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Knop filters item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
