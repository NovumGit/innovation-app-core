<?php
namespace AdminModules\System\Crud_editor_block_field\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudEditorBlockField\CrudCrudEditorBlockFieldManager;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_block_field instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Knoppen die op collecties van toepassing zijn";
	}


	public function getModule(): string
	{
		return "CrudEditorBlockField";
	}


	public function getManager(): FormManager
	{
		return new CrudCrudEditorBlockFieldManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorBlockFieldQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof CrudEditorBlockField){
		    LogActivity::register("System", "Knoppen die op collecties van toepassing zijn verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Knoppen die op collecties van toepassing zijn verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Knoppen die op collecties van toepassing zijn niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Knoppen die op collecties van toepassing zijn item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
