<?php
namespace AdminModules\System\Crud_editor_block_field\Base;

use AdminModules\GenericEditController;
use Crud\CrudEditorBlockField\CrudCrudEditorBlockFieldManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_block_field instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudEditorBlockFieldManager();
	}


	public function getPageTitle(): string
	{
		return "Knoppen die op collecties van toepassing zijn";
	}
}
