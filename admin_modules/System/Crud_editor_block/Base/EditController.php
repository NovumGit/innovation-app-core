<?php
namespace AdminModules\System\Crud_editor_block\Base;

use AdminModules\GenericEditController;
use Crud\CrudEditorBlock\CrudCrudEditorBlockManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_block instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudEditorBlockManager();
	}


	public function getPageTitle(): string
	{
		return "Formulier blokken";
	}
}
