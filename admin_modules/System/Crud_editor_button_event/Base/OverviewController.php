<?php
namespace AdminModules\System\Crud_editor_button_event\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudEditorButtonEvent\CrudCrudEditorButtonEventManager;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudEditorButtonEvent;
use Model\Setting\CrudManager\CrudEditorButtonEventQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_button_event instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Knop gebeurtenissen";
	}


	public function getModule(): string
	{
		return "CrudEditorButtonEvent";
	}


	public function getManager(): FormManager
	{
		return new CrudCrudEditorButtonEventManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonEventQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof CrudEditorButtonEvent){
		    LogActivity::register("System", "Knop gebeurtenissen verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Knop gebeurtenissen verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Knop gebeurtenissen niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Knop gebeurtenissen item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
