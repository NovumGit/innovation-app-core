<?php
namespace AdminModules\System\Filter_operator\Base;

use AdminModules\GenericEditController;
use Crud\FilterOperator\CrudFilterOperatorManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Filter_operator instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudFilterOperatorManager();
	}


	public function getPageTitle(): string
	{
		return "Filter operator";
	}
}
