<?php
namespace AdminModules\System\Behavior_type;

/**
 * Skeleton subclass for drawing a list of BehaviorType records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EditController extends Base\EditController
{
}
