<?php
namespace AdminModules\System\Behavior_type\Base;

use AdminModules\GenericEditController;
use Crud\BehaviorType\CrudBehaviorTypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Behavior_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudBehaviorTypeManager();
	}


	public function getPageTitle(): string
	{
		return "Behavior type";
	}
}
