<?php
namespace AdminModules\System\Filter_operator_datatype\Base;

use AdminModules\GenericEditController;
use Crud\FilterOperatorDatatype\CrudFilterOperatorDatatypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Filter_operator_datatype instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudFilterOperatorDatatypeManager();
	}


	public function getPageTitle(): string
	{
		return "Filter operator datatype";
	}
}
