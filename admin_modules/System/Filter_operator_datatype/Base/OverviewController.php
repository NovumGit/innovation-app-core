<?php
namespace AdminModules\System\Filter_operator_datatype\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FilterOperatorDatatype\CrudFilterOperatorDatatypeManager;
use Crud\FormManager;
use Model\Setting\CrudManager\FilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Filter_operator_datatype instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Filter operator datatype";
	}


	public function getModule(): string
	{
		return "FilterOperatorDatatype";
	}


	public function getManager(): FormManager
	{
		return new CrudFilterOperatorDatatypeManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return FilterOperatorDatatypeQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof FilterOperatorDatatype){
		    LogActivity::register("System", "Filter operator datatype verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Filter operator datatype verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Filter operator datatype niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Filter operator datatype item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
