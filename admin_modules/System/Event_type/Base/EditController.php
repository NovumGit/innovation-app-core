<?php
namespace AdminModules\System\Event_type\Base;

use AdminModules\GenericEditController;
use Crud\EventType\CrudEventTypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Event_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudEventTypeManager();
	}


	public function getPageTitle(): string
	{
		return "Event type";
	}
}
