<?php
namespace AdminModules\System\Datasource_endpoint\Base;

use AdminModules\GenericEditController;
use Crud\DataSourceEndpoint\CrudDataSourceEndpointManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Datasource_endpoint instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudDataSourceEndpointManager();
	}


	public function getPageTitle(): string
	{
		return "Databron endpoints";
	}
}
