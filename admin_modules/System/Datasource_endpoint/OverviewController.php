<?php
namespace AdminModules\System\Datasource_endpoint;

use Model\System\DataSourceQuery;

/**
 * Skeleton subclass for drawing a list of DataSource records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
    function __construct($aGet, $aPost)
    {
        $aDataSources = DataSourceQuery::create()->find();
        foreach($aDataSources as $oDataSource)
        {

            $sUrl = 'https://api.overheid.demo.novum.nu/v2/rest/datasource/' . $oDataSource->getId() . '/ReloadEndpoints';
            echo $sUrl . "<br>";
            file_get_contents($sUrl);


        }

        parent::__construct($aGet, $aPost);
    }

}
