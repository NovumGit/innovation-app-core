<?php
namespace AdminModules\System\Datasource_endpoint_method;

/**
 * Skeleton subclass for drawing a list of DataSourceEndpointMethod records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
