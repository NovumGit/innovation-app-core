<?php
namespace AdminModules\System\Datasource_endpoint_method\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\DataSourceEndpointMethod\CrudDataSourceEndpointMethodManager;
use Crud\FormManager;
use Model\System\DataSourceEndpointMethod;
use Model\System\DataSourceEndpointMethodQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Datasource_endpoint_method instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Databron endpoint method";
	}


	public function getModule(): string
	{
		return "DataSourceEndpointMethod";
	}


	public function getManager(): FormManager
	{
		return new CrudDataSourceEndpointMethodManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return DataSourceEndpointMethodQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof DataSourceEndpointMethod){
		    LogActivity::register("System", "Databron endpoint method verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Databron endpoint method verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Databron endpoint method niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Databron endpoint method item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
