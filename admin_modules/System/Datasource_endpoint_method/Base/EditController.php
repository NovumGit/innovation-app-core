<?php
namespace AdminModules\System\Datasource_endpoint_method\Base;

use AdminModules\GenericEditController;
use Crud\DataSourceEndpointMethod\CrudDataSourceEndpointMethodManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Datasource_endpoint_method instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudDataSourceEndpointMethodManager();
	}


	public function getPageTitle(): string
	{
		return "Databron endpoint method";
	}
}
