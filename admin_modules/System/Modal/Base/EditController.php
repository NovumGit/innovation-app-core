<?php
namespace AdminModules\System\Modal\Base;

use AdminModules\GenericEditController;
use Crud\Component_modal\CrudComponent_modalManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Modal instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_modalManager();
	}


	public function getPageTitle(): string
	{
		return "modal";
	}
}
