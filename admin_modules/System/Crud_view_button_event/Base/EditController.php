<?php
namespace AdminModules\System\Crud_view_button_event\Base;

use AdminModules\GenericEditController;
use Crud\CrudViewButtonEvent\CrudCrudViewButtonEventManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_view_button_event instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudViewButtonEventManager();
	}


	public function getPageTitle(): string
	{
		return "Knop gebeurtenissen op collecties";
	}
}
