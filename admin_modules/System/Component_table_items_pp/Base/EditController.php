<?php
namespace AdminModules\System\Component_table_items_pp\Base;

use AdminModules\GenericEditController;
use Crud\Component_table_items_pp\CrudComponent_table_items_ppManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Component_table_items_pp instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_table_items_ppManager();
	}


	public function getPageTitle(): string
	{
		return "component_table_items_pp";
	}
}
