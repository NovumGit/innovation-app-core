<?php
namespace AdminModules\System\Component_table_items_pp;

/**
 * Skeleton subclass for drawing a list of Table_items_pp records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
