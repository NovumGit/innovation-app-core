<?php
namespace AdminModules\System\Crud_view_visible_filter\Base;

use AdminModules\GenericEditController;
use Crud\CrudViewVisibleFilter\CrudCrudViewVisibleFilterManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_view_visible_filter instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudViewVisibleFilterManager();
	}


	public function getPageTitle(): string
	{
		return "Formulier filters";
	}
}
