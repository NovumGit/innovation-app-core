<?php
namespace AdminModules\System\Crud_view_visible_filter\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudViewVisibleFilter\CrudCrudViewVisibleFilterManager;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_view_visible_filter instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Formulier filters";
	}


	public function getModule(): string
	{
		return "CrudViewVisibleFilter";
	}


	public function getManager(): FormManager
	{
		return new CrudCrudViewVisibleFilterManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return CrudViewVisibleFilterQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof CrudViewVisibleFilter){
		    LogActivity::register("System", "Formulier filters verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Formulier filters verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Formulier filters niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Formulier filters item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
