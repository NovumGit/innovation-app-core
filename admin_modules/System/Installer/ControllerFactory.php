<?php
namespace AdminModules\System\Installer;

use AdminModules\Generic\Crud\CreateController;
use AdminModules\Generic\Crud\Editor_editController;
use AdminModules\Generic\Crud\Field\Item\ConfigureController;
use Core\IControllerFactory;
use Core\MainController;
use Core\Utils;
use Exception\ClassNotFoundException;

class ControllerFactory implements IControllerFactory
{
    function getController(array $aGet, array $aPost, string $sNamespace = null): MainController
    {

        if(Utils::getRequestUri(false) === '/generic/crud/editor_edit')
        {
            return (new Editor_editController($_GET, $_POST));
        }
        else if(Utils::getRequestUri(false) === '/generic/crud/create')
        {
            return (new CreateController($_GET, $_POST));
        }
        else if(Utils::getRequestUri(false) === '/generic/crud/field/item/configure')
        {
            return (new ConfigureController($_GET, $_POST));
        }
        throw new ClassNotFoundException("Controller not found");
    }

}
