
$('a.post').click(function(e){
    e.preventDefault();
    if($(this).hasClass('disabled'))
    {
        return false;
    }


    $('#fld_do').val($(this).data('action'));
    $('#next_step').val($(this).attr('href'));
    const oMainForm = $('#main_form');

    oMainForm.attr('action', $(this).attr('href'));
    oMainForm.trigger('submit');
});

$('a.event_button').each(function(i, element) {
    console.log('bind');
    $(element).click(function (e) {
        e.preventDefault();
        alert('click');

        if ($(this).hasClass('disabled')) {
            return false;
        }
        $('#fld_do').val('EventHandler');
        const oMainForm = $('#main_form');

        oMainForm.append(
            $('<input>',
                {
                    name: 'EventStack',
                    type: 'hidden',
                    value: JSON.stringify($(this).data('event'))
                }
            )
        );

        // oMainForm.attr('action', $(this).attr('href'));
        oMainForm.trigger('submit');
    });
});
