<?php
namespace AdminModules\System\Installer;

use LowCode\AppLocation;

class AppLocationAdapter
{
    static function toAppLocation(array $mData = null) : AppLocation
    {
        if(isset($mData['section']))
        {
            $aImplementation['section'] = $mData['section'];
        }
        if(isset($mData['block']))
        {
            $aImplementation['block'] = $mData['block'];
        }

        return new AppLocation($aImplementation);
    }
    static function fromArray(array $aArray = null) : AppLocation
    {
        return self::toAppLocation($aArray);
    }
    static function fromAppLocation(AppLocation $oAppLocation)
    {
        return $oAppLocation->getImplementation();
    }
}
