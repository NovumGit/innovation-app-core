<?php
namespace AdminModules\System\Installer;

use AdminModules\Generic\Crud\CreateController;
use AdminModules\Generic\Crud\Editor_editController;
use Core\Debug;
use Core\DeferredAction;
use Core\MainController;
use Core\Mime\JsonMime;
use Core\Setting;
use LowCode\Properties;
use LowCode\State;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\FormManager;
use Core\Header;
use LowCode\State\HttpState;
use Ui\RenderEditConfig;
use Exception\ClassNotFoundException;
use Exception\InvalidArgumentException;
use Exception\NullPointerException;
use LowCode\App;

/**
 * Skeleton subclass for drawing a list of DataSource records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class InstallationWizard extends MainController
{
    private $oApp = null;

    function doSave()
    {
        $this->oApp = new App($this->getDefinition(), array_merge_recursive($_GET, $_POST));
    }
    private function getDefinition()
    {
        $sDefinition = '../admin_modules/System/Installer/installer.json';
        $sDefinitionJson = file_get_contents($sDefinition);
        return json_decode($sDefinitionJson, true);
    }
    /**
     * InstallationWizard constructor.
     * @param $aGet
     * @param $aPost
     * @throws \Exception
     */
    function __construct($aGet, $aPost) {

        Debug::backtrace();
        $this->oApp = new App($this->getDefinition(), array_merge_recursive($_GET, $_POST));

        parent::__construct($aGet, $aPost);
    }

    private function getApp():App {
        return $this->oApp;
    }


    /**
     * Wrapper to call other controller methods that are behind login normally
     * @param $method
     * @param $arguments
     */
    function __call($method, $arguments) {
        $oController = (new ControllerFactory())->getController($this->get(), $this->post());
        $oController->$method();
    }

    function doEventHandler()
    {
        var_dump($this->post());
        exit();
    }
    /**
     * Takes care that some controllers are accessible without signing in.
     * @return array
     */
    private function handleOpenControllers() :array {

        $oBehindLoginController = (new ControllerFactory())->getController($this->get(), $this->post());
        if($oBehindLoginController instanceof MainController)
        {
            return $oBehindLoginController->run();
        }
    }
    /**
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \ReflectionException
     */
    function run()
    {

        try
        {
            return $this->handleOpenControllers();
        }
        catch (ClassNotFoundException $e)
        {

            if($this->getRequestUri(false) != '/')
            {
                // Alles, bijv favicon.ico komen ook in dit script en zorgt voor redirect issues.
                exit();
            }
            // No controller found that is callable in this situation.
            // Proceed as normal

            // Toon back url in formulier/Crud editor
            DeferredAction::register('crud_editor_return_url', $this->getRequestUri());


            $aAppData = $this->getApp()->render();

            $sBaseUrl = $this->getRequestUri(false);

            $aVars = [
                'app' => $aAppData,
                'base_url' => $sBaseUrl
            ];

            $aData  =
                [
                    'show_navbar_top' => Setting::get('show_navbar_top'),
                    'show_sidebar_left' => Setting::get('show_sidebar_left'),
                    'content' => $this->parse('System/Installer/wizard.twig', $aVars),
                    'title' => Translate::fromCode("Installatie")
                ];
            return $aData;
        }


    }

    /*
    private function renderModal($aActiveBlock)
    {
        if($this->get('skip_dialog'))
        {
            return;
        }
        $sTitle = Translate::fromCode($aActiveBlock['title']);
        $sMessage = $aActiveBlock['message'];
        $aButtons = null;

        if($aActiveBlock['buttons'])
        {
            $aButtons = [];
            foreach ($aActiveBlock['buttons'] as $aButton)
            {
                $sButtonUrl = '?' . http_build_query($aButton['action']);

                $aButtons[] = new StatusMessageButton($aButton['label'], $sButtonUrl, $aButton['label'], 'success');
            }
        }
        StatusModal::info($sMessage, $sTitle, $aButtons);
    }
    */
}
