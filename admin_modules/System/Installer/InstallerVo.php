<?php
namespace AdminModules\System\Installer;

final class InstallerVo
{
    private $sHtml;
    private $sConfigUrl;

    function __construct(string $sHtml, string $sConfigUrl = null)
    {
        $this->sHtml = $sHtml;
        $this->sConfigUrl = $sConfigUrl;
    }
    function getFormHtml():string
    {
        return $this->sHtml;
    }
    function getConfigUrl():?string
    {
        return $this->sConfigUrl;
    }
}
