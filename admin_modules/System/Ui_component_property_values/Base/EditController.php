<?php
namespace AdminModules\System\Ui_component_property_values\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\UIComponentPropertyValues\CrudUIComponentPropertyValuesManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Ui_component_property_values instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudUIComponentPropertyValuesManager();
	}


	public function getPageTitle(): string
	{
		return "UI component property values";
	}
}
