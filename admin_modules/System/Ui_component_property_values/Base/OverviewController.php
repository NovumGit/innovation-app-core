<?php
namespace AdminModules\System\Ui_component_property_values\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FormManager;
use Crud\UIComponentPropertyValues\CrudUIComponentPropertyValuesManager;
use Model\System\UI\UIComponentPropertyValues;
use Model\System\UI\UIComponentPropertyValuesQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Ui_component_property_values instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "UI component property values";
	}


	public function getModule(): string
	{
		return "UIComponentPropertyValues";
	}


	public function getManager(): FormManager
	{
		return new CrudUIComponentPropertyValuesManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return UIComponentPropertyValuesQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof UIComponentPropertyValues){
		    LogActivity::register("System", "UI component property values verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("UI component property values verwijderd.");
		}
		else
		{
		       StatusMessage::warning("UI component property values niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit UI component property values item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
