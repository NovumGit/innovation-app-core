<?php
namespace AdminModules\System\Datasource\Base;

use AdminModules\GenericEditController;
use Crud\DataSource\CrudDataSourceManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Datasource instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudDataSourceManager();
	}


	public function getPageTitle(): string
	{
		return "Databronnen";
	}
}
