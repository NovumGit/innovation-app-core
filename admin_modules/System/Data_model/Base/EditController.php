<?php
namespace AdminModules\System\Data_model\Base;

use AdminModules\GenericEditController;
use Crud\DataModel\CrudDataModelManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Data_model instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudDataModelManager();
	}


	public function getPageTitle(): string
	{
		return "Data modellen";
	}
}
