<?php
namespace AdminModules\System\System_registry\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\SystemRegistry\CrudSystemRegistryManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\System_registry instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudSystemRegistryManager();
	}


	public function getPageTitle(): string
	{
		return "";
	}
}
