<?php
namespace AdminModules\System\System_registry;

use AdminModules\Custom\System\System_registry\Base;

/**
 * Skeleton subclass for drawing a list of SystemRegistry records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
