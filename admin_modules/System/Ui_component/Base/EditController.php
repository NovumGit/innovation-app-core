<?php
namespace AdminModules\System\Ui_component\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\UIComponent\CrudUIComponentManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Ui_component instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudUIComponentManager();
	}


	public function getPageTitle(): string
	{
		return "UI component";
	}
}
