<?php
namespace AdminModules\System\Component_dialog_color_type;

/**
 * Skeleton subclass for drawing a list of Dialog_color_type records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EditController extends Base\EditController
{
}
