<?php
namespace AdminModules\System\Ui_component_type\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\UIComponentType\CrudUIComponentTypeManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Ui_component_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudUIComponentTypeManager();
	}


	public function getPageTitle(): string
	{
		return "UI component type";
	}
}
