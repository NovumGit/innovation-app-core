<?php
namespace AdminModules\System\Crud_editor\Base;

use AdminModules\GenericEditController;
use Crud\CrudEditor\CrudCrudEditorManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudEditorManager();
	}


	public function getPageTitle(): string
	{
		return "Formulieren";
	}
}
