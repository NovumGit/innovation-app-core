<?php
namespace AdminModules\System\Crud_editor;

use AdminModules\Custom\System\Crud_editor\Base;

/**
 * Skeleton subclass for drawing a list of CrudEditor records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
