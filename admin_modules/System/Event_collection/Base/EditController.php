<?php
namespace AdminModules\System\Event_collection\Base;

use AdminModules\GenericEditController;
use Crud\EventCollection\CrudEventCollectionManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Event_collection instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudEventCollectionManager();
	}


	public function getPageTitle(): string
	{
		return "Event collection";
	}
}
