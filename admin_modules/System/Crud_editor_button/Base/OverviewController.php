<?php
namespace AdminModules\System\Crud_editor_button\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\CrudEditorButton\CrudCrudEditorButtonManager;
use Crud\FormManager;
use Model\Setting\CrudManager\CrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_button instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Knoppen";
	}


	public function getModule(): string
	{
		return "CrudEditorButton";
	}


	public function getManager(): FormManager
	{
		return new CrudCrudEditorButtonManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return CrudEditorButtonQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof CrudEditorButton){
		    LogActivity::register("System", "Knoppen verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Knoppen verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Knoppen niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Knoppen item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
