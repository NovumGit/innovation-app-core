<?php
namespace AdminModules\System\Crud_editor_button\Base;

use AdminModules\GenericEditController;
use Crud\CrudEditorButton\CrudCrudEditorButtonManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_editor_button instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudEditorButtonManager();
	}


	public function getPageTitle(): string
	{
		return "Knoppen";
	}
}
