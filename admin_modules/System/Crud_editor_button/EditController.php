<?php
namespace AdminModules\System\Crud_editor_button;

/**
 * Skeleton subclass for drawing a list of CrudEditorButton records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class EditController extends Base\EditController
{
}
