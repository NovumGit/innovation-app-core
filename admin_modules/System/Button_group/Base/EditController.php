<?php
namespace AdminModules\System\Button_group\Base;

use AdminModules\GenericEditController;
use Crud\Component_buttongroup\CrudComponent_buttongroupManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Button_group instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_buttongroupManager();
	}


	public function getPageTitle(): string
	{
		return "button_group";
	}
}
