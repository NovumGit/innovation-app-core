<?php
namespace AdminModules\System\Mind_map\Base;

use AdminModules\GenericEditController;
use Crud\Component_mindmap\CrudComponent_mindmapManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Mind_map instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_mindmapManager();
	}


	public function getPageTitle(): string
	{
		return "mind_map";
	}
}
