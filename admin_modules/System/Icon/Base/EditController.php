<?php
namespace AdminModules\System\Icon\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\Icon\CrudIconManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Icon instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudIconManager();
	}


	public function getPageTitle(): string
	{
		return "Iconen";
	}
}
