<?php
namespace AdminModules\System\Crud_view_hidden_filter\Base;

use AdminModules\GenericEditController;
use Crud\CrudViewHiddenFilter\CrudCrudViewHiddenFilterManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Crud_view_hidden_filter instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudCrudViewHiddenFilterManager();
	}


	public function getPageTitle(): string
	{
		return "Standaard filter velden";
	}
}
