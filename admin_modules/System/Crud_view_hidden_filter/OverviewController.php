<?php
namespace AdminModules\System\Crud_view_hidden_filter;

use AdminModules\Custom\System\Crud_view_hidden_filter\Base;

/**
 * Skeleton subclass for drawing a list of CrudViewHiddenFilter records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
