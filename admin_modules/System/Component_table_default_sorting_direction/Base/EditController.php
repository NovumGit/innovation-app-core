<?php
namespace AdminModules\System\Component_table_default_sorting_direction\Base;

use AdminModules\GenericEditController;
use Crud\Component_table_default_sorting_direction\CrudComponent_table_default_sorting_directionManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Component_table_default_sorting_direction instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_table_default_sorting_directionManager();
	}


	public function getPageTitle(): string
	{
		return "component_table_default_sorting_direction";
	}
}
