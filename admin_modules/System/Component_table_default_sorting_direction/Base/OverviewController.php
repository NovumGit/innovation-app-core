<?php
namespace AdminModules\System\Component_table_default_sorting_direction\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\Component_table_default_sorting_direction\CrudComponent_table_default_sorting_directionManager;
use Crud\FormManager;
use Model\System\LowCode\Table\Component_table_default_sorting_direction;
use Model\System\LowCode\Table\Component_table_default_sorting_directionQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Component_table_default_sorting_direction instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "component_table_default_sorting_direction";
	}


	public function getModule(): string
	{
		return "Component_table_default_sorting_direction";
	}


	public function getManager(): FormManager
	{
		return new CrudComponent_table_default_sorting_directionManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return Component_table_default_sorting_directionQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof Component_table_default_sorting_direction){
		    LogActivity::register("System", "component_table_default_sorting_direction verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("component_table_default_sorting_direction verwijderd.");
		}
		else
		{
		       StatusMessage::warning("component_table_default_sorting_direction niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit component_table_default_sorting_direction item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
