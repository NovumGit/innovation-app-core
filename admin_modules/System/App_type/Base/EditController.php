<?php
namespace AdminModules\System\App_type\Base;

use AdminModules\GenericEditController;
use Crud\AppType\CrudAppTypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\App_type instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudAppTypeManager();
	}


	public function getPageTitle(): string
	{
		return "App type";
	}
}
