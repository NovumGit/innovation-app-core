<?php
namespace AdminModules\System\View\Base;

use AdminModules\GenericEditController;
use Crud\Component_view\CrudComponent_viewManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\View instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudComponent_viewManager();
	}


	public function getPageTitle(): string
	{
		return "view";
	}
}
