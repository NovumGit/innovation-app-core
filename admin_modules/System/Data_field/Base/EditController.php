<?php
namespace AdminModules\System\Data_field\Base;

use AdminModules\GenericEditController;
use Crud\DataField\CrudDataFieldManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Data_field instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudDataFieldManager();
	}


	public function getPageTitle(): string
	{
		return "Data model veld";
	}
}
