<?php
namespace AdminModules\System\Filter_datatype\Base;

use AdminModules\GenericEditController;
use Crud\FilterDatatype\CrudFilterDatatypeManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Filter_datatype instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudFilterDatatypeManager();
	}


	public function getPageTitle(): string
	{
		return "Filter datatype";
	}
}
