<?php
namespace AdminModules\System\Filter_datatype\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FilterDatatype\CrudFilterDatatypeManager;
use Crud\FormManager;
use Model\Setting\CrudManager\FilterDatatype;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Filter_datatype instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Filter datatype";
	}


	public function getModule(): string
	{
		return "FilterDatatype";
	}


	public function getManager(): FormManager
	{
		return new CrudFilterDatatypeManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return FilterDatatypeQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof FilterDatatype){
		    LogActivity::register("System", "Filter datatype verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Filter datatype verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Filter datatype niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Filter datatype item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
