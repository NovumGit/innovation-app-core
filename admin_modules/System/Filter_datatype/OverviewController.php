<?php
namespace AdminModules\System\Filter_datatype;

use AdminModules\Custom\System\Filter_datatype\Base;

/**
 * Skeleton subclass for drawing a list of FilterDatatype records.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class OverviewController extends Base\OverviewController
{
}
