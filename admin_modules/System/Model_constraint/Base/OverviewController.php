<?php
namespace AdminModules\System\Model_constraint\Base;

use AdminModules\GenericOverviewController;
use Core\LogActivity;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Crud\FormManager;
use Crud\ModelConstraint\CrudModelConstraintManager;
use Model\System\DataModel\Model\ModelConstraint;
use Model\System\DataModel\Model\ModelConstraintQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Model_constraint instead if you need to override or add functionality.
 */
abstract class OverviewController extends GenericOverviewController
{
	public function __construct($aGet, $aPost)
	{
		$this->setEnablePaginate(50);parent::__construct($aGet, $aPost);
	}


	public function getTitle(): string
	{
		return "Verwijzende sleutels";
	}


	public function getModule(): string
	{
		return "ModelConstraint";
	}


	public function getManager(): FormManager
	{
		return new CrudModelConstraintManager();
	}


	public function getQueryObject(): ModelCriteria
	{
		return ModelConstraintQuery::create();
	}


	public function doDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$oQueryObject = $this->getQueryObject();
		$oDataObject = $oQueryObject->findOneById($iId);
		if($oDataObject instanceof ModelConstraint){
		    LogActivity::register("System", "Verwijzende sleutels verwijderen", $oDataObject->toArray());
		    $oDataObject->delete();
		    StatusMessage::success("Verwijzende sleutels verwijderd.");
		}
		else
		{
		       StatusMessage::warning("Verwijzende sleutels niet gevonden.");
		}
		$this->redirect($this->getManager()->getOverviewUrl());
	}


	final public function doConfirmDelete(): void
	{
		$iId = $this->get('id', null, true, 'numeric');
		$sMessage = Translate::fromCode("Weet je zeker dat je dit Verwijzende sleutels item wilt verwijderen?");
		$sTitle = Translate::fromCode("Zeker weten?");
		$sOkUrl = $this->getManager()->getOverviewUrl() . "?id=" . $iId . "&_do=Delete";
		$sNOUrl = $this->getRequestUri();
		$sYes = Translate::fromCode("Ja");
		$sCancel = Translate::fromCode("Annuleren");
		$aButtons  = [
		   new StatusMessageButton($sYes, $sOkUrl, $sYes, "warning"),
		   new StatusMessageButton($sCancel, $sNOUrl, $sCancel, "info"),
		];
		StatusModal::warning($sMessage, $sTitle, $aButtons);
	}
}
