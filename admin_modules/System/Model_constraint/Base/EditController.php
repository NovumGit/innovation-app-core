<?php
namespace AdminModules\System\Model_constraint\Base;

use AdminModules\GenericEditController;
use Crud\FormManager;
use Crud\ModelConstraint\CrudModelConstraintManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\System\Model_constraint instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudModelConstraintManager();
	}


	public function getPageTitle(): string
	{
		return "Verwijzende sleutels";
	}
}
