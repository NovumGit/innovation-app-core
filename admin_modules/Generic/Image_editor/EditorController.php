<?php
namespace AdminModules\Generic\Image_editor;

use Core\DeferredAction;
use Core\MainController;
use Core\Translate;
use Exception\LogicException;
use Gregwar\Image\Image;

class EditorController extends MainController
{
    function doCropImage()
    {
        // var_dump($_POST);

        $iRenderedImageWidth = $this->post('rendered_image_width');
        $iRenderedImageHeight = $this->post('rendered_image_height');

        $sCurrentSize = $this->post('current_size', null, true);
        $aCropData = json_decode($this->post('crop_data', null, true), true);

        $sFile = $this->get('file', null, true);
        $sSourceDir = dirname($sFile);
        $sOriginalImage = "../data/img/$sFile";
        $sSlicedImageDir = "../data/img/$sSourceDir/sliced/$sCurrentSize/";
        $sFinalImage = $sSlicedImageDir.basename($sFile);

        if(!is_dir($sSlicedImageDir))
        {
            mkdir($sSlicedImageDir, 0777, true);
        }

        $sOriginalImageSize = getimagesize($sOriginalImage);
        /*
        echo "Rendered size <br>";
        echo $iRenderedImageWidth.' '.$iRenderedImageHeight;

        echo "Original size <br>";
        var_dump($sOriginalImageSize);

        echo "To crop size <br>";
        */
        $sOriginalWitdh = $sOriginalImageSize[0];
        $sOriginalHeight = $sOriginalImageSize[1];

        $sRenderedWidthFactor = $sOriginalWitdh / $iRenderedImageWidth;
        $sRenderedHeightFactor = $sOriginalHeight / $iRenderedImageHeight;
        /*
        echo "Rendered factor width, height <Br>";
        echo $sRenderedWidthFactor.' '.$sRenderedHeightFactor;

        exit();
        */
        Image::open($sOriginalImage)
            ->crop(
                round($aCropData['left']*$sRenderedWidthFactor,0),
                round($aCropData['top']*$sRenderedHeightFactor,0),
                round($aCropData['width']*$sRenderedWidthFactor,0),
                round($aCropData['height']*$sRenderedHeightFactor,0))
            ->save($sFinalImage);


        $sFile = $this->get('file');
        $aSizes = explode(',', $this->get('sizes'));

        $iFound = array_search($sCurrentSize, $aSizes);
        unset($aSizes[$iFound]);

        if($aSizes)
        {
            $sNewUrl = "/generic/image_editor/editor?file=$sFile&sizes=".join(',', $aSizes);
            $this->redirect($sNewUrl);
        }
        else
        {
            $sReturnAfterSlicing = DeferredAction::get('return_after_slicing');
            if(empty($sReturnAfterSlicing))
            {
                throw new LogicException('Return after slicing deferred action is missing');
            }
            $this->redirect($sReturnAfterSlicing);
        }

    }

    function run()
    {
        $aSizes = explode(',', $this->get('sizes'));

        if($aSizes[0] == '')
        {
            unset($aSizes[0]);
        }

        if(count($aSizes) > 1)
        {
            $sItemsToGo = count($aSizes) - 1;
            $sTitle = str_replace('{num_items}', $sItemsToGo, Translate::fromCode('Hierna nog {num_items} items bijsnijden'));
            $sNextSize = array_shift($aSizes);

        }
        else if(count($aSizes) == 1)
        {
            $sTitle = 'Afbeelding bijnsijden';
            $sNextSize = array_shift($aSizes);
        }
        else if(count($aSizes) == 0)
        {
            $sTitle = 'Geen afbeeldingen meer';
            $sUrl = DeferredAction::get('cutting_done');
            $this->redirect($sUrl);
            exit($sTitle);
        }
        else
        {
            throw new LogicException('This part of the code should be unreachable. Contact the developer please.');
        }


        $sSourceImage = '/img/'.$this->get('file');
        $this->addJsFile('/assets/js/plugins/cropper/cropper.min.js');


        list($iNextWidhth, $iNextHeight) = explode('x', $sNextSize);

        $aTopNav = [];
        $aMainWindow = [
            'title' => $sTitle,
            'next_width' => $iNextWidhth,
            'next_heigth' => $iNextHeight,
            'source_image' => $sSourceImage
        ];


        $sTopNavHtml = $this->parse('Generic/Image_editor/top_nav.twig', $aTopNav);
        $sMainWindowHtml = $this->parse('Generic/Image_editor/editor.twig', $aMainWindow);

        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => Translate::fromCode('Afbeelding bijsnijden')
        ];
        return $aView;
    }
}
