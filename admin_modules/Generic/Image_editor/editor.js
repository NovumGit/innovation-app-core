'use strict';
//  Author: ThemeREX.com
//  user-forms-image-tools.html scripts
//
(function($) {


    var aSettings = $('#fld_settings').val().split(',');
    var iWidth = aSettings[0];
    var iHeight = aSettings[1];

    $(document).ready(function() {

        "use strict";

        // Init Theme Core
        Core.init();

        // Init Demo JS
        Demo.init();

        // Cropper config
        (function() {
            var $image = $('.img-container > img'),
                $dataX = $('#dataX'),
                $dataY = $('#dataY'),
                $dataHeight = $('#dataHeight'),
                $dataWidth = $('#dataWidth'),
                $dataRotate = $('#dataRotate'),
                options = {
                    aspectRatio: iWidth / iHeight,
                    preview: '.img-preview',
                    crop: function(data) {

                        $dataX.val(Math.round(data.x));
                        $dataY.val(Math.round(data.y));
                        $dataHeight.val(Math.round(data.height));
                        $dataWidth.val(Math.round(data.width));
                        $dataRotate.val(Math.round(data.rotate));
                    }
                };


            console.log($image);
            $image.on({
                'build.cropper': function(e) {
                    console.log(e.type);
                },
                'built.cropper': function(e) {
                    console.log(e.type);
                },
                'dragstart.cropper': function(e) {
                    console.log(e.type, e.dragType);
                },
                'dragmove.cropper': function(e) {
                    console.log(e.type, e.dragType);
                },
                'dragend.cropper': function(e) {
                    console.log(e.type, e.dragType);
                },
                'zoomin.cropper': function(e) {
                    console.log(e.type);
                },
                'zoomout.cropper': function(e) {
                    console.log(e.type);
                }
            }).cropper(options);

            $(document.body).on('click', '[data-method]', function() {
                var data = $(this).data(),
                    $target,
                    result;

                if (data.method) {
                    data = $.extend({}, data);

                    if (typeof data.target !== 'undefined') {
                        $target = $(data.target);

                        if (typeof data.option === 'undefined') {
                            try {
                                data.option = JSON.parse($target.val());
                            } catch (e) {
                                console.log(e.message);
                            }
                        }
                    }

                    result = $image.cropper(data.method, data.option);
                    console.log(result);

                    if (data.method === 'getCroppedCanvas') {
                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);
                    }

                    if ($.isPlainObject(result) && $target) {
                        try {
                            $target.val(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
                    if (data.method === 'getCropBoxData')
                    {
                        var oImageToBeCropped = $('.cropper-canvas');

                        $('#fld_rendered_image_width').val(oImageToBeCropped.css('width').replace('px', ''));
                        $('#fld_rendered_image_height').val(oImageToBeCropped.css('height').replace('px', ''));
                        $('#little_cropperform').trigger('submit');
                    }
                }
            }).on('keydown', function(e) {

                switch (e.which) {
                    case 37:
                        e.preventDefault();
                        $image.cropper('move', -1, 0);
                        break;

                    case 38:
                        e.preventDefault();
                        $image.cropper('move', 0, -1);
                        break;

                    case 39:
                        e.preventDefault();
                        $image.cropper('move', 1, 0);
                        break;

                    case 40:
                        e.preventDefault();
                        $image.cropper('move', 0, 1);
                        break;
                }

            });

            // Import image
            var $inputImage = $('#inputImage'),
                URL = window.URL || window.webkitURL,
                blobURL;

            if (URL) {
                $inputImage.change(function() {
                    var files = this.files,
                        file;

                    if (files && files.length) {
                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            blobURL = URL.createObjectURL(file);
                            $image.one('built.cropper', function() {
                                URL.revokeObjectURL(blobURL);
                            }).cropper('reset', true).cropper('replace', blobURL);
                            $inputImage.val('');
                        } else {
                            showMessage('Please choose an image file.');
                        }
                    }
                });
            } else {
                $inputImage.parent().remove();
            }

            // Options
            $('.docs-options :checkbox').on('change', function() {
                var $this = $(this);

                options[$this.val()] = $this.prop('checked');
                $image.cropper('destroy').cropper(options);
            });

            // Tooltips
            $('[data-toggle="tooltip"]').tooltip();

        }());
    });

})(jQuery);
