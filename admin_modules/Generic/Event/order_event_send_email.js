
function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? "ff" : (document.selection ? "ie" : false ) );
    var range;

    if (br == "ie") {
        txtarea.focus();
        range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff")
    {
      strPos = txtarea.selectionStart;
    }

    var front = (txtarea.value).substring(0,strPos);
    var back = (txtarea.value).substring(strPos,txtarea.value.length);
    txtarea.value=front+text+back;
    strPos = strPos + text.length;

    if (br == "ie")
    {
        txtarea.focus();
        range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff")
    {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}

var sLastItemFocusId = 'message_contents';

$('.add_where_cursor_is').on('click', function(e)
{
    e.preventDefault();
    insertAtCaret('message_contents', $(this).html());
});

$('#message_subject').focus(function(){
    sLastItemFocusId = $(this).attr('id');
});
$('#message_contents').focus(function(){
    sLastItemFocusId = $(this).attr('id');
});
