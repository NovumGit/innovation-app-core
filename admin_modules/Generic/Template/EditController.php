<?php
namespace AdminModules\Generic\Template;

use Core\Cfg;
use Core\DeferredAction;
use Core\MainController;
use Core\QueryMapper;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Crud\Field;
use Crud\FormManager;
use Crud\IDisplayableField;
use Exception\LogicException;
use Model\Crm\Base\NewsletterSubscriber;
use Model\Crm\NewsletterSubscriberQuery;
use Model\Setting\MasterTable\Base\LanguageQuery;
use Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery;
use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;

class EditController extends MainController {


    function doStore()
    {
        $aPostedData = $this->post('data');
        foreach($aPostedData as $sKey => $sValue)
        {
            $oSystemRegistry = SystemRegistryQuery::create()->findOneByItemKey($sKey);
            if(!$oSystemRegistry instanceof SystemRegistry)
            {
                $oSystemRegistry = new SystemRegistry();
                $oSystemRegistry->setItemKey($sKey);
            }
            $oSystemRegistry->setItemValue($sValue);
            $oSystemRegistry->save();
        }
        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen."));
        if($this->post('back_overview') === '1')
        {
            $sAfterSave = DeferredAction::get('after_template_change');
            if($sAfterSave)
            {
                $this->redirect($sAfterSave);
            }
        }
        $this->redirect($this->getRequestUri());
    }
    function run()
    {
        $sTemplate = $this->get('template', true, null);
        $sR = $this->get('r');
        $sOverviewUrl = ($sR) ? DeferredAction::get($sR) : '/setting/sale_order/status_email/overview';

        $aTopNav = [
            'overview_url' => $sOverviewUrl
        ];

        $sTopNavHtml =  $this->parse("Generic/Template/top_nav.twig", $aTopNav);
        $aAllSettings = SystemRegistryQuery::getAllSettingsAsssoc();
        $aTitles =
        [
            'contact_autoreply' =>  Translate::fromCode('Automatisch antwoord e-mail'),
            'email_verification' =>  Translate::fromCode('E-mail verificatie'),
            'password_reset' => Translate::fromCode('Wachtwoord reset e-mail'),
            'welcome' => Translate::fromCode('Welkomst e-mail'),
            'thanks_for_order' => Translate::fromCode('Bedankt voor je order e-mail'),
            'unfinished_order' => Translate::fromCode('Nog niet afgerond e-mail'),
            'order_shipped' => Translate::fromCode('Order verzonden e-mail'),
            'invoice_email' => Translate::fromCode('Factuur e-mail'),
            'confirm_subscription' => Translate::fromCode('Nieuwsbrief inschrijving e-mail'),
            'subscription_not_approved' => Translate::fromCode('Sorry not approved'),
            'thanks_for_subscription' => Translate::fromCode('Thanks for your subscription'),
            'subscription_approved' => Translate::fromCode('Let\'s do business'),
            'track_and_trace' => Translate::fromCode('Track & trace email'),
            'new_customer_verification' => Translate::fromCode('Verificatie'),
        ];

        $aCustomEmailTemplates = Cfg::get('CUSTOM_EMAIL_TEMPLATES');
        $bHideFromField = false;
        $bHideSubjectField = false;

        $bCustomTemplateUsed = false;
        if(!empty($aCustomEmailTemplates))
        {
            foreach($aCustomEmailTemplates as $sCurrentTemplate => $aSettings)
            {
                $aTitles[ $sCurrentTemplate ] = $aSettings[ 'title' ];

                if($sTemplate == $sCurrentTemplate)
                {
                    $bCustomTemplateUsed = true;
                    $aTemplateFieldCruds = $aSettings['template_field_cruds'];
                    $sMailScript = $aSettings['mail_script'];
                    if(isset($aSettings['hide_from_field']))
                    {
                        $bHideFromField = $aSettings['hide_from_field'];
                    }
                    if(isset($aSettings['hide_subject_field']))
                    {
                        $bHideSubjectField = $aSettings['hide_subject_field'];
                    }
                }
            }
        }
        if(!$bCustomTemplateUsed)
        {
            // Welke velden stellen we beschikbaar
            if($sTemplate == 'welcome' )
            {
                $aTemplateFieldCruds = [
                    'Customer' => Translate::fromCode('Klant info'),
                    'Own_company' => Translate::fromCode('Eigen bedrijf'),
                    'User' => Translate::fromCode('Ingelogde gebruiker'),
                ];
                $sMailScript = 'mail/customer';
            }
            else if(in_array($sTemplate, ['password_reset', 'subscription_approved', 'subscription_not_approved', 'thanks_for_subscription', 'new_customer_verification']))
            {
                $aTemplateFieldCruds = [
                    'Customer' => Translate::fromCode('Klant info'),
                    'Own_company' => Translate::fromCode('Eigen bedrijf')
                ];

                $sMailScript = 'mail/customer';
            }
            else if($sTemplate == 'email_verification')
            {
                $sMailScript = 'mail/emailverification';
                $aTemplateFieldCruds['EmailVerification'] = Translate::fromCode('E-mail verificatie');
            }
            else if(in_array($sTemplate, ['confirm_subscription']))
            {
                $aTemplateFieldCruds = [
                    'NewsletterSubscriber' => Translate::fromCode('Newsletter subscription')
                ];
                $sMailScript = 'mail/newsletterconfirm';
            }
            else if(in_array($sTemplate, ['contact_autoreply']))
            {
                $aTemplateFieldCruds = [
                    'Contact_message' => Translate::fromCode('Contact bericht')
                ];
                $sMailScript = 'mail/autoreply';
            }
            else if(in_array($sTemplate, ['thanks_for_order', 'unfinished_order', 'order_shipped', 'invoice_email', 'track_and_trace'])){
                $aTemplateFieldCruds = [
                    'Customer' => Translate::fromCode('Klant info'),
                    'Own_company' => Translate::fromCode('Eigen bedrijf'),
                    'User' => Translate::fromCode('Ingelogde gebruiker'),
                    'Sale_order' => Translate::fromCode('Verkoop order'),
                    'Sale_order_item' => Translate::fromCode('Verkoop order producten'),
                ];
                $sMailScript = 'mail'; // /mail is the controller we are using in the webshop.
            }
            else
            {
                throw new LogicException($sTemplate.' <- That tag is missing. ');
            }
        }

        $oNotificationType = Sale_order_notification_typeQuery::create()->findOneByCode($sTemplate);
        $aAllCrudFields = [];
        if(isset($aTemplateFieldCruds) && !empty($aTemplateFieldCruds))
        {
            foreach($aTemplateFieldCruds as $sCrudBaseName => $sTranslatedTitle)
            {
                $sNamespacedCrud = "\\Crud\\$sCrudBaseName\\Crud{$sCrudBaseName}Manager";
                if(class_exists($sNamespacedCrud))
                {
                    $oCrudManager = new $sNamespacedCrud;
                }
                else
                {
                    $sCustomNamespace = Cfg::get('CUSTOM_NAMESPACE');
                    $sNamespacedCrud = "\\Crud\\Custom\\$sCustomNamespace\\$sCrudBaseName\\Crud{$sCrudBaseName}Manager";
                    $oCrudManager = new $sNamespacedCrud;
                }

                if($oCrudManager instanceof FormManager)
                {
                    $aAllCrudFields[$sCrudBaseName]['title'] = $sTranslatedTitle;

                    $filter = function(Field $oField){
                        return $oField instanceof IDisplayableField;
                    };

                    $aAllCrudFields[$sCrudBaseName]['fields'] = $oCrudManager->getFieldIterator($filter);
                }
            }
        }
        $oNewsletterSubscriberQuery = NewsletterSubscriberQuery::create();
        $oNewsletterSubscriber = $oNewsletterSubscriberQuery->findOne();
        $iNewsletterSubscriberId = null;
        if($oNewsletterSubscriber instanceof NewsletterSubscriber)
        {
            $iNewsletterSubscriberId = $oNewsletterSubscriber->getId();
        }
        $iRandomSaleOrderId = $this->getRandomSaleOrderId();
        $iRandomCustomerId = $this->getRandomCustomerId();
        $iRandomSupplierId = $this->getRandomSuppierId();
        $iRandomProductId = $this->getRandomProductId();
        $iContactMessageId = $this->getRandomContactMessageId();
        $sDomain = Cfg::get('DOMAIN');

        // live.domeinnaam admin.live.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'live.') === 0 || strpos($_SERVER['HTTP_HOST'],'live.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        // test.domeinnaam admin.test.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'test.') === 0 || strpos($_SERVER['HTTP_HOST'],'test.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        if($sDomain == 'linqxx.nl')
        {
            // Hopelijk in de toekomst weg halen.
            $sDomain = 'mijn.linqxx.nl';
        }

        if(Cfg::get('SHOP_URL'))
        {
            $sUrl = Cfg::get('PROTOCOL') . '://' . Cfg::get('SHOP_URL');
        }
        else
        {
            $sDomain = str_replace('admin.', '', $sDomain);
            $sUrl = Cfg::get('PROTOCOL') . '://' . $sDomain;
        }


        $sTitle = '';
        if(isset($aTitles[$sTemplate]))
        {
            $sTitle = $aTitles[$sTemplate];
        }
        if(!isset($sMailScript))
        {
            throw new LogicException("Variable sMailScript is not defined.");
        }
        $aMainWindowVars = [
            'languages' => LanguageQuery::create()->findByIsEnabledWebshopOrIsEnabledCms(),
            'title' =>  $sTitle,
            'registry' => $aAllSettings,
            'crud_fields' => $aAllCrudFields,
            'hide_from_field' => $bHideFromField,
            'hide_subject_field' => $bHideSubjectField,
            'template' => $sTemplate,
            'mail_script' =>  $sMailScript,
            'shop_url' => $sUrl,
            'ramdom_newsletter_subscriber_id' => $iNewsletterSubscriberId,
            'notification_type' => $oNotificationType,
            'product_id' => $iRandomProductId,
            'random_sale_order_id' => $iRandomSaleOrderId,
            'user_id' => User::getMember()->getId(),
            'random_supplier_id' => $iRandomSupplierId,
            'random_customer_id' => $iRandomCustomerId,
            'has_overview_button' => (boolean) DeferredAction::get('after_template_change'),
            'random_contact_message_id' => $iContactMessageId
        ];
        $sMainWindowHtml = $this->parse("Generic/Template/edit.twig", $aMainWindowVars);
        $aView = [
            'top_nav' => $sTopNavHtml,
            'content' => $sMainWindowHtml,
            'title' => $sTitle
        ];
        return $aView;
    }

    function getRandomContactMessageId()
    {
        $sQuery = "SELECT id FROM contact_message ORDER BY RAND() LIMIT 1";
        $aId = QueryMapper::fetchRow($sQuery);

        if(!isset($aId['id']))
        {
            // StatusMessage::warning("There are no contact_message in the database so we cannot create a preview based on a random contact_message.");
            return null;
        }

        return $aId['id'];
    }
    function getRandomProductId()
    {
        $sQuery = "SELECT id FROM product    ORDER BY RAND() LIMIT 1";
        $aId = QueryMapper::fetchRow($sQuery);

        if(!isset($aId['id']))
        {
            // StatusMessage::warning("There are no customers in the database so we cannot create a preview based on a random customer.");
            return null;
        }

        return $aId['id'];
    }
    function getRandomSuppierId()
    {
        $sQuery = "SELECT id FROM supplier ORDER BY RAND() LIMIT 1";
        $aId = QueryMapper::fetchRow($sQuery);

        if(!isset($aId['id']))
        {
            // StatusMessage::warning("There are no customers in the database so we cannot create a preview based on a random customer.");
            return null;
        }

        return $aId['id'];
    }
    function getRandomCustomerId()
    {
        $sQuery = "SELECT id FROM customer WHERE is_deleted = 0 ORDER BY RAND() LIMIT 1";
        $aId = QueryMapper::fetchRow($sQuery);

        if(!isset($aId['id']))
        {
            // StatusMessage::warning("There are no customers in the database so we cannot create a preview based on a random customer.");
            return null;
        }

        return $aId['id'];
    }
    private function getRandomSaleOrderId()
    {
        $sQuery = "SELECT id FROM sale_order WHERE work_in_progress = 0 ORDER BY RAND() LIMIT 1";
        $aId = QueryMapper::fetchRow($sQuery);

        if(!isset($aId['id']))
        {
            // StatusMessage::warning("There are no completed sale_orders in the database so we cannot create a preview based on a random sale order.");
            return null;
        }

        return $aId['id'];
    }
}
