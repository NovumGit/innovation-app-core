function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
        "ff" : (document.selection ? "ie" : false ) );

    if (br == 'ie') {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);
    var back = (txtarea.value).substring(strPos,txtarea.value.length);
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") {
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}

var sFocussedField = null;
$('.register_focus').focus(function()
{
    sFocussedField = $(this).attr('id');
})

$('#tag_block a').click(function(e)
{
    e.preventDefault();
    var sStringToInject = $(this).data('var');
    var iLanguageId = $(this).parent().data('language_id');
    console.log(iLanguageId);
    var oTitleField = $('#tpl_title_'+iLanguageId);
    var oContentField = $('#tpl_content_'+iLanguageId);
    var oFocussedField = null;

    if(sFocussedField == 'tpl_title_'+iLanguageId)
    {
        oFocussedField = oTitleField;
    }
    else
    {
        oFocussedField = oContentField;
    }
    insertAtCaret(oFocussedField.attr('id'), sStringToInject);

});

var oAtoggleLink = $('.toggle_more_less');

oAtoggleLink.click(function(e)
{
    e.preventDefault();
    var oVarOuterContainer = $('.var_container', $(this).parent().parent());

    if(typeof($(this).data('state')) == 'undefined')
    {
        $(this).data('original_height', oVarOuterContainer.css('height'));
    }

    if(typeof($(this).data('state')) == 'undefined' || $(this).data('state') == 'minimized')
    {
        oVarOuterContainer.css('height', 'auto');
        $(this).data('state', 'maximized');
        $('.fa-angle-double-down', $(this).parent()).removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
    }
    else
    {
        oVarOuterContainer.css('height', $(this).data('original_height'));
        $(this).data('state', 'minimized');
        $('.fa-angle-double-up', $(this).parent()).removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
    }
    var sTmp = $(this).data('alt');
    $(this).data('alt', $(this).html());
    $(this).html(sTmp);
    console.log($(this).data('state'));
});