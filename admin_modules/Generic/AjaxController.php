<?php

namespace AdminModules\Generic;

use Core\MainController;
use Core\UserRegistry;
use Core\Utils;
use LogicException;

class AjaxController extends MainController
{
    function run()
    {
        // Not doing anything as you can see (-:
        exit();
    }

	function doToggleThumbSize()
	{
		$_SESSION['thumb_size'] = $this->post('new_size', null, true);
		Utils::jsonOk();
		exit();
	}

	function doToggleView()
	{
		$_SESSION['current_view'] = $this->post('new_view', null, true);
		Utils::jsonOk();
		exit();
	}

    function dostoreMenuSize()
    {
        $sState = $this->post('state');
        if (!in_array($sState, ['minimal', 'large'])) {
            throw new LogicException("State must be minimal or large");
        }
        UserRegistry::register('menu_size', $sState);
        Utils::jsonOk();
    }
    function dostoreMenuState()
    {
        $sState = $this->post('state') == 'false' ? 'closed' : 'open';
        $sModule = $this->post('module');
        UserRegistry::register('menu_state_' . $sModule, $sState);
        Utils::jsonOk();
    }
}