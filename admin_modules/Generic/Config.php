<?php
namespace AdminModules\Generic;

use AdminModules\ModuleConfig;

class Config extends ModuleConfig{

    function isEnabelable(): bool
    {
        return false;
    }
    function getModuleTitle(): string
    {
        return "Utilities + algemeen";
    }
}
