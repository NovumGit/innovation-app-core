<?php
namespace AdminModules\Generic\Loopback;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage as Status;

class Status_messageController extends MainController
{
    function run()
    {
        $sMessageType = $this->get('type', 'success', false);
        $sMessage = $this->get('message',null, true);
        $sR = $this->get('r',null, true);

        if($sMessageType == 'success')
        {
            Status::success($sMessage);
        }
        else if($sMessageType == 'info')
        {
            Status::info($sMessage);
        }
        else if($sMessageType == 'warning')
        {
            Status::warning($sMessage);
        }
        else if($sMessageType == 'danger')
        {
            Status::danger($sMessage);
        }
        $this->redirect(DeferredAction::get($sR));
    }
}