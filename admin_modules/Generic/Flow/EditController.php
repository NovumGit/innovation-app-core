<?php
namespace AdminModules\Generic\Flow;
use AdminModules\Generic\Crud\LoadManagerTrait;
use Core\MainController;

class EditController extends MainController{

    protected $sCrudNamespace;
    protected $oCrudManager;
    use LoadManagerTrait;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }


    function run()
    {

        $this->addCssFile('/admin_modules/Generic/Flow/editor.css');
        $this->addJsFile('https://yui-s.yahooapis.com/3.17.2/build/yui/yui.js');
       // $this->addJsFile('/vendor/wireit2/build/wireit-all/wireit-all.js');
        $this->addJsFile('/vendor/wireit/build/wireit-all/wireit-all.js');

        $aEditorArguments = [
            'title' => "Flow configureren op " . $this->getManager()->getEntityTitle() . " data"
        ];
        $sEditor = $this->parse('Generic/Flow/editor.twig', $aEditorArguments);

        return [
            'title' => 'Flow editor',
            'body_classes' => 'yui3-skin-sam',
            'top_nav' => $this->parse('Generic/Flow/top_nav.twig', []),
            'content' => $sEditor
        ];
    }
}


