
$('#flow_editor').addClass();

YUI_config.groups.wireit.base = '/vendor/wireit/build/';


function getForm()
{
    return Math.floor(Math.random() * 100000);
}

YUI({
    filter: 'raw' // debug
}).use('wireit-app', 'image-container', 'out-container', 'inout-container', 'textarea-container', function (Y) {

    window.Y = Y; //debug


    var containers_definitions = [

        {
            name: "Trigger",
            description : "roep een andere flow aan",
            icon : "flash",
            config: {
                type: 'OutContainer',
                collapsible: true,
                headerContent: 'Trigger',
                bodyContent: "asdfdsafdsafdsafdsafdsa",

                outputs: [
                    {
                        label: 'Output',
                        name: 'output'
                    },
                    {
                        label: 'Errors',
                        name: 'error'
                    }
                ]
            },
        },
        {
            name: "Trigger 2",
            description : "roep een andere flow aan",
            icon : "flash",
            config: {
                type: 'InOutContainer',
                collapsible: true,
                headerContent: 'Trigger',
                bodyContent: "asdfdsafdsafdsafdsafdsa",
                inputs: [
                    {
                        label: 'Output',
                        name: 'output'
                    },
                    {
                        label: 'Errors',
                        name: 'error'
                    }
                ],
                outputs: [
                    {
                        label: 'Output',
                        name: 'output'
                    },
                    {
                        label: 'Errors',
                        name: 'error'
                    }
                ]
            },
        },

        {
            name: "Get",
            icon : "arrow-down",
            description : "Verzoek gegevens op via een api",
            config: {
                type: 'InOutContainer',
                children: Y.WidgetTerminals.FOUR_EDGES,
                imageUrl: 'http://www.google.fr/images/logos/ps_logo2.png'
            }
        },
        {
            name: "Post",
            icon : "arrow-up",
            description : "Creëer gegevens via een api",
            config: {
                type: 'InOutContainer',
                children: Y.WidgetTerminals.FOUR_EDGES,
                imageUrl: 'http://www.google.fr/images/logos/ps_logo2.png'
            }
        },
        {
            name: "Put",
            icon : "arrow-up",
            description : "Wijzig gegevens via een api",
            config: {
                type: 'InOutContainer',
                children: Y.WidgetTerminals.FOUR_EDGES,
                imageUrl: 'http://www.google.fr/images/logos/ps_logo2.png'
            }
        },
        {
            name: "Delete",
            icon : "trash",
            description : "Verwijder gegevens via een api",
            config: {
                type: 'InOutContainer',
                children: Y.WidgetTerminals.FOUR_EDGES,
                imageUrl: 'http://www.google.fr/images/logos/ps_logo2.png'
            }
        },
        {
            name: "Map",
            icon : "table",

            description : "Converteer gegevens",
            config: {
                type: 'InOutContainer',
                headerContent: 'Trigger',
                bodyContent: 'This is a much longer body content',
                inputs: [
                    {
                        label: 'Input',
                        name: 'input'
                    }
                ],
                outputs: [
                    {
                        label: 'Output',
                        name: 'output'
                    },
                    {
                        label: 'Errors',
                        name: 'error'
                    }
                ]
            },

        },

        {
            name: "Loop",
            config: {
                children: Y.WidgetTerminals.EIGHT_POINTS,
                type: 'Container',
                headerContent: 'Loop',
                bodyContent: 'This is a much longer body content'
            }
        },
        {
            name: "InOutDemo",
            config: {
                type: 'InOutContainer',
                headerContent: 'In/Out',
                inputs: [
                    {
                        label: 'Input A',
                        name: 'A'
                    },
                    {
                        label: 'Input B',
                        name: 'B'
                    }
                ],
                outputs: [
                    {
                        label: 'Output C',
                        name: 'C'
                    },
                    {
                        label: 'Output D',
                        name: 'D'
                    }
                ]
            }
        },
        {
            name: "TextArea Demo",
            config: {
                children: Y.WidgetTerminals.FOUR_EDGES,
                type: 'TextareaContainer',
                headerContent: 'Textarea'
            }
        }
    ];

    var wirings = new Y.WiringModelList();
    wirings.load();

    Y.WireItApp.prototype.onWireIn = function(x,y) {
        console.log('wire in');
    };

    Y.WireItApp.prototype.onWireOut = function(x,y) {
        console.log('wire out');
    };

    Y.WireItApp.prototype.onWireClick = function(x,y) {
        console.log('wire click');
    };

    Y.wireitApp = new Y.WireItApp({
        // We force this to false for this example app because there is no server.
        serverRouting: false,
        container    : '#content',
        viewContainer: '#content',

        containerTypes: new Y.ContainerTypeList({
            items: containers_definitions
        }),
        modelList: wirings,
        skin : '#sam'
    });

    Y.wireitApp.render();


});

