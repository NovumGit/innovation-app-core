<?php
namespace AdminModules\Generic\Email;

use Core\Customer;
use Core\DeferredAction;
use Core\InlineTemplate;
use Core\LogActivity;
use Core\Mailer;
use Core\MailMessage;
use Core\MainController;
use Core\StatusMessage;
use Core\User;
use Exception\LogicException;
use Model\Company\CompanyQuery;
use Model\Crm\Base\CustomerQuery;
use Model\Setting\MasterTable\LanguageQuery;
use Model\System\SystemRegistryQuery;

abstract class GenericEmailController extends MainController{

    abstract function getTitle();
    abstract function getTemplateConfigKey();
    abstract function getActivityDescription();
    abstract function getTopNav();
    abstract function getMailingLanguageId();
    abstract function getMailingTemplateVars():array;
    abstract function getFromEmail();
    abstract function getToEmail();

    function lastChangesOnContents($sContents)
    {
        return $sContents;
    }
    function getAfterSendUrl()
    {
        return  $this->getRequestUri();
    }
    /*
     * @return array of \Swift_Attachment's
     */
    function getAttachments()
    {
        return null;
    }
    function doSendMessage()
    {
        $aData = $this->post('data');
        LogActivity::register('Crm', 'Mail', $this->getActivityDescription(), $aData);

        $aViewData = $this->getMailingTemplateVars();

        $sSubject = InlineTemplate::parse($aData['subject'], $aViewData);
        $sContents = InlineTemplate::parse($aData['contents'], $aViewData);

        $sContents = $this->lastChangesOnContents($sContents);
        $oMailMessage = new MailMessage();
        $oMailMessage->setFrom($aData['from']);
        $oMailMessage->setTo($aData['to']);
        $oMailMessage->setSubject($sSubject);
        $oMailMessage->setBody($sContents);

        Mailer::send($oMailMessage, $this->getAttachments());
        StatusMessage::success("Het bericht is verzonden");
        $this->redirect($this->getAfterSendUrl());
        exit();
    }
    function doChangeLanguage()
    {
        $iCustomerId = $this->get('customer_id');
        $aData = $this->post('data');

        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $oCustomer->setLanguageId($aData['language_id']);
        $oCustomer->save();
        $this->redirect($this->getRequestUri());
    }
    function run()
    {
        DeferredAction::register('after_template_change', $this->getRequestUri());

        $aLanguages = LanguageQuery::create()->findByIsEnabledWebshopOrIsEnabledCms();
        $iLanguageId = $this->getMailingLanguageId();

        $sSender = SystemRegistryQuery::getVal($this->getTemplateConfigKey().'_sender_'.$iLanguageId);
        $sContents = SystemRegistryQuery::getVal($this->getTemplateConfigKey().'_contents_'.$iLanguageId);
        $sSubject = SystemRegistryQuery::getVal($this->getTemplateConfigKey().'_subject_'.$iLanguageId);

        $aViewData = $this->getMailingTemplateVars();

        $sContents = InlineTemplate::parse($sContents, $aViewData);
        $aParseData =
        [
            'title' => $this->getTitle(),
            'default_from' => $sSender,
            'current_user_from' => $this->getFromEmail(),
            'to_address' => $this->getToEmail(),
            'mail_subject' => $sSubject,
            'mail_contents' => $sContents,
            'current_language_id' => $iLanguageId,
            'languages' => $aLanguages
        ];

        return [
            'content' => $this->parse('Generic/Email/email.twig', $aParseData),
            'top_nav' => $this->getTopNav(),
            'title' => $this->getTitle()
        ];
    }
}