<?php
namespace AdminModules\Generic\Crud;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Crud\IConfigurableCrud;
use Crud\FormManager;

class Copy_tabController extends MainController
{
	use LoadManagerTrait;


	private $oCrudManager;
    private $sCrudNamespace;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
	    $this->loadManagerAndNamespace();
	    /*
        $sModule = $this->get('module');
        $sProductManager =  $this->get('manager');
        $this->sCrudNamespace = "\\Crud\\$sModule";

        $sFqClassname = $this->sCrudNamespace."\\Crud$sProductManager";

        $this->oCrudManager = new $sFqClassname();
	    */
    }

    function doCopyCrudView()
    {
        $iTabId = $this->get('tab', null, true, 'numeric');
        $bInvisibleFilters = $this->post('invisible_filters', null, true, 'numeric') === '1';
        $bvisibleFilters = $this->post('visible_filters', null, true, 'numeric') === '1';
        $sModule = $this->get('module', null, true);
        $sManager = $this->get('manager', null, true);
        $sNewTabName = $this->post('tab_name');

        if(strlen($sNewTabName) < 4)
        {
            StatusMessage::warning(Translate::fromCode("De tabblad titel moet minimaal vier tekens lang zijn."));
            $this->redirect('/generic/crud/copy_tab?module='.$sModule.'&manager='.$sManager.'&tab='.$iTabId);
        }

        $oCrudView = CrudViewQuery::create()->findOneById($iTabId);
        $oNewCrudView = $oCrudView->copy(true);

        if(!$bInvisibleFilters)
        {
            $oNewCrudView->getCrudViewVisibleFilters()->delete();
        }
        if(!$bvisibleFilters)
        {
            $oNewCrudView->getCrudViewVisibleFilters()->delete();
        }

        $oNewCrudView->setName($sNewTabName);
        $oNewCrudView->save();

        StatusMessage::success(Translate::fromCode('Hier is uw nieuwe tabblad.'));

        if(!$this->oCrudManager instanceof IConfigurableCrud)
        {
            throw new LogicException("Wrong datatype, expected oCrudManager to implement IConfigurableCrud");
        }
        $sRedirectToOverviewUrl = $this->oCrudManager->getOverviewUrl();

        // Soms komen er uit de crudmanager al get variabelen mee.
        $sConcatSign = (strpos($sRedirectToOverviewUrl, '?')) ? '&' : '?';
        $this->redirect($sRedirectToOverviewUrl.$sConcatSign."tab=".$oNewCrudView->getId());
    }

    function run()
    {
        $iTabId = $this->get('tab', null, true, 'numeric');
        if(!$this->oCrudManager  instanceof FormManager)
        {
            throw new InvalidArgumentException(Translate::fromCode("Probeerde een CRUD te instantieren maar klassenamen klopt niet."));
        }
        if(!$this->oCrudManager instanceof IConfigurableCrud)
        {
            throw new LogicException("Crud must implement IConfigurableCrud for this functionality in ".__METHOD__." ".get_class($this->oCrudManager));
        }

        $sModule = $this->get('module');
        $sManager =  $this->get('manager');

        $oCrudView = new CrudView();
        if($iTabId)
        {
            $oCrudView = CrudViewQuery::create()->findOneById($iTabId);
        }

        $aViewData = [
            'tab_id' => $iTabId,
            'module' => $sModule,
            'manager' => $sManager,
            'crud_view' => $oCrudView
        ];
        $aView = [
            'content'   => $this->parse('Generic/Crud/copy_tab.twig', $aViewData)
        ];

        return $aView;
    }
}
