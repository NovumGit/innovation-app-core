<?php
namespace AdminModules\Generic\Crud;

use Core\Cfg;
use Crud\FormManager;

trait LoadManagerTrait
{

    function loadManagerAndNamespace()
    {
        $sModule = $this->get('module');
        $sManagerName =  $this->get('manager');
        $sLayoutKey =  $this->get('layout_key');

        $this->sCrudNamespace = "\\Crud\\$sModule";

        $sFqClassname = $this->sCrudNamespace."\\Crud$sManagerName";

        if(class_exists($sFqClassname))
        {
            $this->oCrudManager = new $sFqClassname($sLayoutKey);
            return null;
        }

        // Klasse niet gevonden, custom crud in proberen te laden.
        $this->sCrudNamespace = "\\Crud\\Custom\\".Cfg::get('CUSTOM_NAMESPACE')."\\$sModule";

        $sCustomFqClassname = $this->sCrudNamespace."\\Crud$sManagerName";
        $this->oCrudManager = new $sCustomFqClassname($sLayoutKey);
    }
    function getManager():FormManager
    {
        return $this->oCrudManager;
    }
}

