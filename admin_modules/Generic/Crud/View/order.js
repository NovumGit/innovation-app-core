$(document).ready(function(){

    $('.sortable' ).sortable({
        connectWith: '.sortable'
    });

    oExplanation = $('#explanation');
    oToggleExplain = $('#toggle_explain');

    oToggleExplain.on('click', function(e)
    {
        e.preventDefault();
        if(oExplanation.is(':visible'))
        {
            oToggleExplain.html(oToggleExplain.data('show-txt'));
            oExplanation.fadeOut(200);
        }
        else
        {
            oToggleExplain.html(oToggleExplain.data('hide-txt'));
            oExplanation.fadeIn(200);
        }
    });
    $('#fld_submit_button').on('click', function(e){
        e.preventDefault();
        var aFields = [];
        $('#configured_fields span').each(function(i, e){
            aFields.push($(e).attr('id'));
        });
        $('#fld_new_sorting').val(aFields.join(','));
        $('#tab_order_form').submit();
    });
});