<?php
namespace AdminModules\Generic\Crud\View;

use AdminModules\Generic\Crud\LoadManagerTrait;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\CrudViewManager;
use Crud\CrudViewSorter;

class OrderController extends MainController
{
    private $oCrudManager;
    use LoadManagerTrait;
    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }

    function doChangeSorting()
    {
        $sR = $this->get('r', null, true);

        $sNewSorting = $this->post('new_sorting');
        $aNewSorting = explode(',', $sNewSorting);

        CrudViewSorter::changeTabOrder($this->oCrudManager, $aNewSorting);

        StatusMessage::success(Translate::fromCode("De volgorde van de tabbladen is gewijzigd."));
        $sNewUrl = DeferredAction::get($sR);
        $this->redirect($sNewUrl);
    }
    function run()
    {
        $this->get('r', null, true);

        $aCrudViews = CrudViewManager::getViews($this->oCrudManager);

        $aMainNavVars = [
            'crud_views' => $aCrudViews
        ];

        $aOut = [
            'top_nav' => '',
            'content' => $this->parse('Generic/Crud/View/order.twig', $aMainNavVars),
            'title' => Translate::fromCode("Tab volgorde wijzigen")
        ];
        return $aOut;
    }
}