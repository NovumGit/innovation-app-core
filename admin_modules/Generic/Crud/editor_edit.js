storeFieldState = function()
{
    var aFieldContainers = $('.available_fields_container', '#fields_in_use');
    var aDataSet = [];
    var editBlock = {};
    aFieldContainers.each(function(iContainerSorting, eContainerElement)
    {
        editBlock = {};

        editBlock['sorting'] = iContainerSorting;
        editBlock['iEditorBlockId'] = $(eContainerElement).data('editor_block_id');
        editBlock['width'] =  $('#width_'+editBlock['iEditorBlockId']).val();
        editBlock['sEditBlockTitle'] = $('#title_'+editBlock['iEditorBlockId']).val();
        editBlock['fields'] = [];

        $('span.datafield', eContainerElement).each(function(iFieldSorting, eFieldElement){
            const el = $(eFieldElement);
            editBlock['fields'].push(
            {
                sorting :  iFieldSorting,
                iBlockFieldId : el.data('block-field-id'),
                fieldName : el.attr('id'),
                width : $('#'+el.attr('id')+' .width').val(),
                read_only : $('#'+el.attr('id')+' .read_only').val()
            });
        });
        aDataSet.push(editBlock);
    });
    $('#fld_data').val(JSON.stringify(aDataSet));
    $('#crud_editor_form').submit();
};

$(document).ready(function(){

    $('#fld_submit').on('click', function(e)
    {
        e.preventDefault();
        storeFieldState();
    });
    $('.sortable' ).sortable(
    {
        connectWith: '.sortable',
    });
    $('.width').change(function(){
        $(this).parent().removeClass('w1 w2 w3 w4 w5 w6 w7 w8 w9 w10 w11 12');
        var sNewClass = 'w'+$(this).val();
        $(this).parent().addClass(sNewClass);
    });
    $('.sortable_block_container').sortable({
        helper : '.sortable_handle',
        stop : function()
        {
            var aIds = [];
            $('.sortable_block').each(function(i, e)
            {
                aIds.push({id : $(e).data('id'), sorting : i });
            });
            $.post(window.location, {_do : 'StoreBlockSorting', aIds : aIds}, function(data){
                window.location = window.location;
            },
            'json');
        }
    });
});
