<?php
namespace AdminModules\Generic\Crud\Field\Item;

use AdminModules\Generic\Crud\Field\Item\IItemConfigContract;
use AdminModules\Generic\Crud\Field\Item\SettingStore;
use Model\Setting\CrudManager\CrudEditorBlockField;

abstract class AbstractConfig implements IItemConfigContract {
    protected CrudEditorBlockField $oCrudEditorBlockField;
    protected SettingStore $oStore;

    function __construct(CrudEditorBlockField $oCrudEditorBlockField) {
        $this->oCrudEditorBlockField = $oCrudEditorBlockField;
        $this->oStore = new SettingStore($oCrudEditorBlockField);
    }

    abstract function getEditorPageTitle(): string;

    function save($aPostedData):void
    {
        $aEditFlags = $this->getEditFlags();
        foreach ($aEditFlags as $aSetting) {
            if (isset($aPostedData[$aSetting['key']])) {
                $this->setValue($aSetting['key'], $aPostedData[$aSetting['key']]);
            } else {
                $this->clearValue($aSetting['key']);
            }
        }
    }
    function getEditFlags(): array {
        return [
            [
                'key'   => 'hide_on_new',
                'label' => 'Verbergen bij nieuw',
                'value' => $this->getValue('hide_on_new'),
            ],
            [
                'key'   => 'read_only_on_new',
                'label' => 'Alleen lezen bij nieuw',
                'value' => $this->getValue('read_only_on_new'),
            ],
            [
                'key'   => 'write_once_read_many',
                'label' => '1x schrijven, altijd lezen',
                'value' => $this->getValue('read_only_on_edit'),
            ],
            [
                'key'   => 'hide_on_edit',
                'label' => 'Verbergen bij edit',
                'value' => $this->getValue('hide_on_edit'),
            ],
            [
                'key'   => 'read_only_on_edit',
                'label' => 'Alleen lezen bij edit',
                'value' => $this->getValue('read_only_on_edit'),
            ],
        ];
    }


    function getTitle(): ?string {
        return $this->getValue('title');
    }
    function getHideFieldOnNew(): bool {
        return $this->getValue('hide_on_new') === '1';
    }

    function getHideFieldOnEdit(): bool {
        return $this->getValue('hide_on_edit') === '1';
    }

    function getReadOnlyOnNew(): bool {
        return $this->getValue('read_only_on_new') === '1';
    }

    function getReadOnlyOnEdit(): bool {
        return $this->getValue('read_only_on_edit') === '1';
    }

    function getWriteOnceReadMany(): bool {
        return $this->getValue('write_once_read_many') === '1';
    }

    function getDefaultValue() {
        return $this->getValue('default_value');
    }

    protected function getValue(string $sKey): ?string {
        return $this->oStore->getValue($sKey);
    }
    protected function setValue(string $sKey, string $sValue) {
        $this->oStore->setValue($sKey, $sValue);
    }
    protected function clearValue(string $sKey) {
        $this->oStore->clearValue($sKey);
    }
}
