<?php

namespace AdminModules\Generic\Crud\Field\Item;

use Model\Setting\CrudManager\CrudEditorBlockField;

interface IItemConfigContract{

    function __construct(CrudEditorBlockField $oCrudEditorBlockField);
    function save(array $aPostedData);
    function getEditorPageTitle():string;
    function getEditorHtml():string;

}
