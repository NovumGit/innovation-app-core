<?php

namespace AdminModules\Generic\Crud\Field\Item\Configurators\Text;

use AdminModules\Generic\Crud\Field\Item\AbstractConfig;
use AdminModules\Generic\Crud\Field\Item\IItemConfigContract;
use Core\Json\JsonUtils;
use Core\TemplateFactory;
use Crud\Generic\Field\GenericEditTextfield;
use Exception\LogicException;

class Config extends AbstractConfig implements IItemConfigContract {

    function getEditorPageTitle(): string {
        return 'Gedrag van veld instellen';
    }

    function save($aPostedData):void {

        $aKeys = [
            'title',
            'default_value',
            'allowed_values',
            'info_help_text'
        ];
        foreach ($aKeys as $sKey) {
            if (isset($aPostedData[$sKey]) && !empty($aPostedData[$sKey])) {
                $sValue = ($sKey === 'allowed_values') ? JsonUtils::encode($aPostedData[$sKey]) : $aPostedData[$sKey];
                $this->setValue($sKey, $sValue);
            } else {
                $this->clearValue($sKey);
            }
        }
        parent::save($aPostedData);
    }
    function getAllowedOptions(): ?array {
        return JsonUtils::decode($this->getValue('allowed_values'));
    }

    function getInfoHelpText(): ?string {
        return $this->getValue('info_help_text');
    }

    function getEditorHtml(): string {

        $sFieldFqn = $this->oCrudEditorBlockField->getField();
        $oField = new $sFieldFqn;

        if (!$oField instanceof GenericEditTextfield) {
            throw new LogicException("Expected an instance of GenericEditTextfield");
        }
        $aAdvancedSettings = $this->getEditFlags();

        $aData = [
            'edit_flags' => $aAdvancedSettings,
            'default_value'     => $this->getValue('default_value'),
            'title'             => $this->getValue('title'),
            'field'             => $oField,
        ];

        $sTemplate = 'Generic/Crud/Field/Item/Configurators/Text/editor.twig';
        return TemplateFactory::parse($sTemplate, $aData);
    }
}
