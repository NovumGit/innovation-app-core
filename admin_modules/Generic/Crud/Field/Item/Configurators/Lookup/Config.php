<?php

namespace AdminModules\Generic\Crud\Field\Item\Configurators\Lookup;

use AdminModules\Generic\Crud\Field\Item\AbstractConfig;
use AdminModules\Generic\Crud\Field\Item\IItemConfigContract;
use Core\Json\JsonUtils;
use Core\TemplateFactory;
use Crud\Generic\Field\GenericLookup;
use Exception\LogicException;

class Config extends AbstractConfig implements IItemConfigContract {

    function getEditorPageTitle(): string {
        return 'Gedrag van dropdown menu instellen';
    }

    function save($aPostedData):void {

        $aKeys = [
            'title',
            'default_value',
            'allowed_values',
            'info_help_text'
        ];
        foreach ($aKeys as $sKey) {
            if (isset($aPostedData[$sKey]) && !empty($aPostedData[$sKey])) {
                $sValue = ($sKey === 'allowed_values') ? JsonUtils::encode($aPostedData[$sKey]) : $aPostedData[$sKey];
                echo "Set $sKey -> $sValue <br>";
                $this->setValue($sKey, $sValue);
            } else {
                $this->clearValue($sKey);
            }
        }

        $aAdvancedSettings = $this->getEditFlags();
        foreach ($aAdvancedSettings as $aSetting) {
            if (isset($aPostedData[$aSetting['key']])) {
                $this->setValue($aSetting['key'], $aPostedData[$aSetting['key']]);
            } else {
                $this->clearValue($aSetting['key']);
            }
        }
    }

    function getInfoHelpText(): ?string {
        return $this->getValue('info_help_text');
    }

    function getTitle(): ?string {
        return $this->getValue('title');
    }

    function getDefaultValue(): ?int {
        return (int)$this->getValue('default_value');
    }

    function getAllowedOptions(): ?array {
        return JsonUtils::decode($this->getValue('allowed_values'));
    }

    function getHideFieldOnNew(): bool {
        return $this->getValue('hide_on_new') === '1';
    }

    function getHideFieldOnEdit(): bool {
        return $this->getValue('hide_on_edit') === '1';
    }

    function getReadOnlyOnNew(): bool {
        return $this->getValue('read_only_on_new') === '1';
    }

    function getReadOnlyOnEdit(): bool {
        return $this->getValue('read_only_on_edit') === '1';
    }

    function getWriteOnceReadMany(): bool {
        return $this->getValue('write_once_read_many') === '1';
    }

    function getEditorHtml(): string {

        $sFieldFqn = $this->oCrudEditorBlockField->getField();
        $oField = new $sFieldFqn;

        if (!$oField instanceof GenericLookup) {
            throw new LogicException("Expected an instance of GenericLookup");
        }
        $aEditFlags = $this->getEditFlags();

        $aData = [
            'edit_flags'        => $aEditFlags,
            'allowed_values'    => JsonUtils::decode($this->getValue('allowed_values')),
            'default_value'     => $this->getValue('default_value'),
            'title'             => $this->getValue('title'),
            'field'             => $oField,
        ];

        $sTemplate = 'Generic/Crud/Field/Item/Configurators/Lookup/editor.twig';
        return TemplateFactory::parse($sTemplate, $aData);
    }
}
