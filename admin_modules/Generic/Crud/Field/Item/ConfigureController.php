<?php

namespace AdminModules\Generic\Crud\Field\Item;

use Core\DeferredAction;
use Core\MainController;
use Crud\IConfigurableFieldItem;
use Crud\IField;
use Exception\LogicException;
use InvalidArgumentException;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;

class ConfigureController extends MainController {

    function doSaveFieldSettings() {
        $aPostData = $this->post('data');

        $oConfigurator = $this->getConfigurator();
        $oConfigurator->save($aPostData);
        $sAfterFieldConfigurationEdit =  DeferredAction::get('after_field_configuration_edit');

        $this->redirect($sAfterFieldConfigurationEdit);
    }
    function run() {

        $sAfterFieldConfigurationEdit =  DeferredAction::get('after_field_configuration_edit');
        $oConfigurator = $this->getConfigurator();

        return [
            'content' => $oConfigurator->getEditorHtml(),
            'top_nav' => $this->parse('Generic/Crud/Field/Item/top_nav.twig', ['back_url' => $sAfterFieldConfigurationEdit]),
            'title' => $oConfigurator->getEditorPageTitle(),
        ];
    }

    private function getConfigurator():?IItemConfigContract {
        $iCrudEditorBlockFieldId = $this->get('crud_editor_block_field_id', null, true, 'numeric');
        $oCrudEditorBlockField = CrudEditorBlockFieldQuery::create()->findOneById($iCrudEditorBlockFieldId);

        $sFieldClassFqn = $oCrudEditorBlockField->getField();
        $oFieldObject = new $sFieldClassFqn();

        if($oFieldObject instanceof IField)
        {
            $oFieldObject->setArguments(['crud_editor_block_field' => $oCrudEditorBlockField]);
        }
        else
        {
            throw new LogicException("Expected an instance of IField");
        }
        if(!$oFieldObject instanceof IConfigurableFieldItem)
        {
            throw new InvalidArgumentException("Expected an instance of IConfigurableFieldItem");
        }

        return $oFieldObject->getItemConfig();
    }
}
