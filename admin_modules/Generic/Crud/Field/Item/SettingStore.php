<?php

namespace AdminModules\Generic\Crud\Field\Item;

use Core\Setting;
use Model\Setting\CrudManager\CrudEditorBlockField;

class SettingStore {

    private CrudEditorBlockField $oCrudEditorBlockField;

    function __construct(CrudEditorBlockField $oCrudEditorBlockField) {
        $this->oCrudEditorBlockField = $oCrudEditorBlockField;
    }
    private function key(string $sKey):string {
        return 'crud_field_item_config_' . $this->oCrudEditorBlockField->getId() . '_' . $sKey;
    }
    public function clearValue(string $sKey):void
    {
        Setting::clear($this->key($sKey));
    }
    public function setValue(string $sKey, string $sValue):void {
        Setting::store($this->key($sKey), $sValue);
    }
    public function getValue(string $sKey) {
        return Setting::get($this->key($sKey));
    }
}
