var oCurrentField = null;
function bindCriteriaEvents()
{
    var oEventFields = $('.field, .operator');

    oEventFields.unbind('change');
    oEventFields.on('change', function(){

        oCurrentField = $('.field', $(this).parent());

        var oPost = {
            _do :  'GetFieldOperators',
            field : oCurrentField.val()
        };
        $.post(window.location, oPost, function(data) {
            var oDropdownFieldValue = $('.operator', oCurrentField.parent()).val();
            var oDropdownField = $('.operator', oCurrentField.parent());

            oDropdownField.html('');
            var iInItemId = null;
            var iNotInItemId = null;

            if (data.operator_filter_select_items.length > 0) {

                $.each(data.operator_filter_select_items, function (i, e) {
                    if (e.name == 'in') {
                        iInItemId = e.id;
                    }
                    if (e.name == 'not_in') {
                        iNotInItemId = e.id;
                    }
                    oDropdownField.append($("<option></option>").attr("value", e.id).text(e.label));
                });
            }
            if (oDropdownFieldValue)
            {
                oDropdownField.val(oDropdownFieldValue);
            }

            $('.value_field', oCurrentField.parent()).remove();
            $('.save_button', oCurrentField.parent()).remove();

            var bShowTextField = false;
            var iCollectionField = (oDropdownFieldValue == iInItemId || oDropdownFieldValue == iNotInItemId );

            if(typeof(data.selected_field_datatype) == 'undefined')
            {
                console.log("Ajax response not properly formatted ");
            }

            if(data.selected_field_datatype == 'lookup' && iCollectionField)
            {
                bShowTextField = true;
            }


            if(data.selected_field_datatype == 'boolean')
            {
                oDropdownField = $('<select class="value_field"></select>');
                oCurrentField.parent().append(oDropdownField);

                oDropdownField.append($("<option value=\"1\">Ja</option>"));
                oDropdownField.append($("<option value=\"0\">Nee</option>"));
                oDropdownField.append($("<option value=\"null\">Niet ingevuld (toon alleen als de waarde nog leeg is)</option>"));
                oCurrentField.parent().append($("<button class='save_button'>Opslaan</button>"));
            }
            else if(data.selected_field_datatype == 'string' || data.selected_field_datatype == 'number' || bShowTextField)
            {
                oTextField = $('<input type="text" class="value_field">');
                oCurrentField.parent().append(oTextField);
                oCurrentField.parent().append($("<button class='save_button'>Opslaan</button>"));
            }
            else if(data.selected_field_datatype == 'lookup')
            {
                if(data.selected_field_lookups.length > 0)
                {
                    oDropdownField = $('<select class="value_field"></select>');
                    oCurrentField.parent().append(oDropdownField);

                    $.each(data.selected_field_lookups, function(i, e){
                        oDropdownField.append($("<option></option>").attr("value", e.id).text(e.label));
                    });
                }
                else
                {
                    alert('Er zijn nog geen lookup waarden gedefinieerd voor dit veld, als je de admin waarschuwd, geef dan ook door om welk veld het gaat en graag ook de url van het systeem.');
                }
                oCurrentField.parent().append($("<button class='save_button'>Opslaan</button>"));
            }
            else
            {
                alert('Voor het datatype ' + data.selected_field_datatype + ' is nog geen routine, dit is een bugje in het systeem, excuus! Zou je dit willen melden bij de admin? Graag de url er bij doorgeven.');
            }
            bindCriteriaEvents();
        }, 'json');

    });
    var oSaveButtonEvent = $('.save_button');
    oSaveButtonEvent.unbind('click');
    oSaveButtonEvent.on('click', function(e){

        e.preventDefault();
        var oContainerField = $(this).parent();
        var iCrudEditorButtonId = oContainerField.data('id');

        var sOperator =  $('.operator', oContainerField).val();
        var sFieldValue =  $('.value_field', oContainerField).val();
        var sField =  $('.field', oContainerField).val();

        aData = {

            _do : 'SaveButtonVisibilityCriteria',
            crud_editor_button_id : iCrudEditorButtonId,
            operator : sOperator,
            field_value : sFieldValue,
            field : sField
        };

        $.post(window.location, aData, function(data){

            var sSelector = '#panel_body_'+data.crud_editor_button_id;
            var newPanel = $(sSelector, data.content).html();

            $(sSelector).html(newPanel);
            bindCriteriaEvents();
        }, 'json');
    });

    var oDeleteButtonEvent = $('.delete_filter');
    oDeleteButtonEvent.unbind('click');
    oDeleteButtonEvent.on('click', function(e)
    {
        e.preventDefault();
        var iButtonId = $(this).data('id');
        var iFilterId = $(this).data('filter-id');
        var aData = {
            _do : 'DeleteVisibilityCriteria',
            crud_editor_button_id : iButtonId,
            crud_editor_button_visibile_filter_id : iFilterId
        };
        $.post(window.location, aData, function(data){

            var sSelector = '#panel_body_'+data.crud_editor_button_id;
            var newPanel = $(sSelector, data.content).html();

            $(sSelector).html(newPanel);
            bindCriteriaEvents();
        }, 'json');
    });
}
bindCriteriaEvents();