(function ($) {

    function storeSetting(field, value, id)
    {
        var aStoreData  = {
            field : field,
            value : value,
            id : id,
            _do : 'StoreButtonSetting'
        };

        $.post(window.location, aStoreData,
            function(data){

            },
        'json');
    }

    $(document).ready(function () {
        selectColorModal = function()
        {
            $.magnificPopup.open({
                removalDelay: 500,
                items: {
                    src: '#color-picker-dialog'
                },
                callbacks: {
                    beforeOpen: function (e) {
                        //      this.st.mainClass = 'mfp-with-fade';
                    }
                },
                midClick: true
            });
        };

        selectIconModal = function()
        {
            $.magnificPopup.open({
                removalDelay: 500,
                items: {
                    src: '#icon-picker-dialog'
                },
                callbacks: {
                    beforeOpen: function (e) {
                        //      this.st.mainClass = 'mfp-with-fade';
                    }
                },
                midClick: true
            });
        };

        var oIconSelected  = $('.icon_selected');
        var oColorSelected  = $('.color_selected');
        var oChangeTitleField = $('.change_title');
        var oSelectingForIcon = null;
        var oSelectingForColor = null;
        var oToggleReusable = $('.toggle_reusable');

        oToggleReusable.on('change', function(){
            iButtonId = $(this).data('id');
            bIsChecked = $(this).is(':checked');
            storeSetting('is_reusable', bIsChecked, iButtonId);
        });

        oChangeTitleField.on('keyup', function(){
            iButtonId = $(this).data('id');
            sNewTitle = $(this).val();
            storeSetting('title', sNewTitle, iButtonId);

            $('#field_title_'+iButtonId).html(sNewTitle);
        });

        // Hier wordt er op een icon geklikt
        oIconSelected.on('click', function(e)
        {
            $('i', oSelectingForIcon.parent()).attr('class', 'fa fa-lg '+$(this).data('icon'));
            e.preventDefault();
            $.magnificPopup.close();

            storeSetting(oSelectingForIcon.data('field'), $(this).data('icon'), oSelectingForIcon.data('id'));
        });

        oColorSelected.on('click', function(e)
        {
            sNewClasses = 'tm-tag tm-tag-'+$(this).data('color')+' color_selector';
            oSelectingForColor.attr('class', sNewClasses);
            e.preventDefault();
            $.magnificPopup.close();
            storeSetting(oSelectingForColor.data('field'), $(this).data('color'), oSelectingForColor.data('id'));
        });
        
        // Hier wordt de modal geoppend
        $('.color_selector').on('click', function () {
            oSelectingForColor = $(this);
            selectColorModal();
        });
        // Hier wordt de modal geoppend
        $('.icon_selector').on('click', function () {
            oSelectingForIcon = $(this);
            selectIconModal();
        });
    });

    $('.sortable_block_container').sortable({
        handle : '.sortable_handle',
        stop : function()
        {
            var aIds = [];
            $('.sortable_block').each(function(i, e)
            {
                aIds.push({id : $(e).data('id'), sorting : i });
            });

            $.post(window.location, {_do : 'StoreButtonSorting', aIds : aIds}, function(data){
                window.location = window.location;
            },
            'json');
        }
    });



})(jQuery);