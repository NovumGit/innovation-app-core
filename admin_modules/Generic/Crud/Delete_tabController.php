<?php
namespace AdminModules\Generic\Crud;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Setting\CrudManager\CrudViewQuery;

class Delete_tabController extends MainController
{
    private $oCrudManager;
    private $sCrudNamespace;

    use LoadManagerTrait;

    /**
     * @return array|void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function run()
    {
        $this->loadManagerAndNamespace();
        if(!$this->oCrudManager instanceof IConfigurableCrud)
        {
            throw new LogicException("CrudManager must be an instance of Manager");
        }

        $iTabId = $this->get('tab', null, true, 'numeric');

        CrudViewQuery::create()->findOneById($iTabId)->delete();
        StatusMessage::info(Translate::fromCode('Tabblad verwijderd.'));

        $this->redirect($this->oCrudManager->getOverviewUrl());
    }
}
