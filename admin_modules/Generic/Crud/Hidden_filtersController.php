<?php
namespace AdminModules\Generic\Crud;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\FieldUtilsTrait;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\FormManager;
use Helper\FilterHelper;
use InvalidArgumentException;
use Model\Setting\CrudManager\Base\CrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\CrudViewHiddenFilter;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;

class Hidden_filtersController extends MainController
{
    use LoadManagerTrait;
    use FieldUtilsTrait;

    private $oCrudManager;
    private $sCrudNamespace;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }
    function doDeleteFilter()
    {
        $aUrlArguments =
        [
            'module' => $this->get('module', null, true),
            'manager' => $this->get('manager', null, true),
            'tab' => $this->get('tab', null, true, 'numeric'),
            'display_other_crud_views' =>  $this->get('display_other_crud_views', true, false)
        ];

        $iId = $this->get('id', null, true, 'numeric');

        $oCrudViewHiddenFilterQuery = CrudViewHiddenFilterQuery::create();
        $oCrudViewHiddenFilter = $oCrudViewHiddenFilterQuery->findOneById($iId);

        if($oCrudViewHiddenFilter instanceof CrudViewHiddenFilter)
        {
            $oCrudViewHiddenFilter->delete();
            StatusMessage::success(Translate::fromCode("Filter verwijderd"));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Filter niet gevonden, was hij al verwijderd?"));
        }

        $sCurrentUrl = $this->getRequestUri(false);
        $sNextUrl = $this->makeUrl($sCurrentUrl, $aUrlArguments);
        $this->redirect($sNextUrl);
    }
    function doSaveFilter()
    {
        $aUrlArguments =
        [
            'module' => $this->get('module', null, true),
            'manager' => $this->get('manager', null, true),
            'tab' => $this->get('tab', null, true, 'numeric'),
            'display_other_crud_views' =>  $this->get('display_other_crud_views', true, false)
        ];

        $sField = $this->get('field');
        $sOperator = $this->get('operator');

        if(empty($sField) || empty($sOperator))
        {
            StatusMessage::warning(Translate::fromCode("Geef tenminste een veld en een operator op."));
        }
        else
        {
            $oCrudViewHiddenFilter = new CrudViewHiddenFilter();
            $oCrudViewHiddenFilter->setCrudViewId($this->get('tab'));
            $oCrudViewHiddenFilter->setFilterName($sField);
            $oCrudViewHiddenFilter->setFilterOperatorId($sOperator);
            $oCrudViewHiddenFilter->setFilterValue($this->get('value'));
            $oCrudViewHiddenFilter->save();
        }

        $sCurrentUrl = $this->getRequestUri(false);
        $sNextUrl = $this->makeUrl($sCurrentUrl, $aUrlArguments);
        $this->redirect($sNextUrl);
    }
    function run()
    {
        $iTabId = $this->get('tab', null, true, 'numeric');
        if(!$this->oCrudManager  instanceof FormManager)
        {
            throw new InvalidArgumentException(Translate::fromCode("Probeerde een CRUD te instantieren maar klassenamen klopt niet."));
        }
        $oCrudView = new CrudView();
        if($iTabId)
        {
            $oCrudView = CrudViewQuery::create()->findOneById($iTabId);
        }
        $sModule = $this->get('module');
        $sFilterField = $this->get('field');
        $iOperator = $this->get('operator');
        $sManager = $this->get('manager');
        $sFilterValue = $this->get('value');

        $aFields = $this->getFilterableFieldAsLookup($this->oCrudManager, $sFilterField);
        $oSelectedField = null;
        if($sFilterField)
        {
            $oSelectedField = new $sFilterField;
        }
        $aSelectedFieldLookups = null;
        if($oSelectedField instanceof IFilterableLookupField)
        {
            $aSelectedFieldLookups = $oSelectedField->getLookups();
        }
        Utils::usort_strcmp($aFields, 'title');
        $aTabFilters = FilterHelper::getFilters($iTabId);

        $oAllFilterOperatorQuery = FilterOperatorQuery::create();
        $oFilterDataType = null;
        $sSelectedFieldDataType = '';

        if($oSelectedField instanceof IFilterableField)
        {
            $oFilterDataType = FilterDatatypeQuery::create()->findOneByName($oSelectedField->getDataType());

            $oFilterOperatorDatatype = FilterOperatorDatatypeQuery::create();
            $oFilterOperatorDatatype->filterByFilterDatatypeId($oFilterDataType->getId());
            $aOperators = $oFilterOperatorDatatype->find();

            $oAllFilterOperatorQuery->filterByFilterOperatorDatatype($aOperators);
            $sSelectedFieldDataType = $oSelectedField->getDataType();
        }

        $aAllFilterOperators = $oAllFilterOperatorQuery->find();
        $aOperatorFilterSelectItems = Utils::makeSelectOptions($aAllFilterOperators, 'getDescription', $iOperator);
        Utils::usort_strcmp($aOperatorFilterSelectItems, 'label');

        $oCurrentlySelectedOperator = null;
        if($iOperator)
        {
            $oCurrentlySelectedOperator = FilterOperatorQuery::create()->findOneById($iOperator);
        }

        $bDisplayOtherCrudViews = $this->get('display_other_crud_views', true, false);
        $aViewData = [
            'fields' => $aFields,
            'tab_id' => $iTabId,
            'operator' => $iOperator,
            'module' => $sModule,
            'value' => $sFilterValue,
            'manager' => $sManager,
            'tab_filters' => $aTabFilters,
            'selected_field_datatype' => $sSelectedFieldDataType,
            'selected_field_operator' => $oCurrentlySelectedOperator,
            'selected_field_filter_data_type' => $oFilterDataType, // string, number, boolean, lookup
            'selected_field_lookups' => $aSelectedFieldLookups,
            'crud_view' => $oCrudView,
            'display_other_crud_views' => $bDisplayOtherCrudViews,
            'operator_filter_select_items' => $aOperatorFilterSelectItems
        ];
        $sOverviewUrl = $this->oCrudManager->getOverviewUrl();
        $aCrudViews = null;
        if($bDisplayOtherCrudViews)
        {
            $aCrudViews = CrudViewManager::getViews($this->oCrudManager);
        }
        $aTopNavData = [
            'crud_views' => $aCrudViews,
            'overview_url' => $sOverviewUrl,
            'module' => $sModule,
            'manager' => $sManager,
            'tab_id' => $iTabId,
        ];
        $aView = [
            'top_nav' => $this->parse('top_nav_overview.twig', $aTopNavData),
            'content'   => $this->parse('Generic/Crud/hidden_filters.twig', $aViewData),
            'title' => Translate::fromCode('Onzichtbare filters instellen')
        ];
        return $aView;
    }
}
