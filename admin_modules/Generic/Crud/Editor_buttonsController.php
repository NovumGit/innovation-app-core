<?php
namespace AdminModules\Generic\Crud;

use Core\Icons;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\IFilterableField;
use Crud\FormManager;
use Exception\LogicException;
use Helper\CrudManagerHelper;
use Helper\EditViewButtonHelper;
use Helper\EventHelper;
use Model\Setting\CrudManager\CrudEditorButtonEvent;
use Model\Setting\CrudManager\CrudEditorButtonEventQuery;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\CrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class Editor_buttonsController extends MainController{

    private $sCrudEditorFqClassname;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager', null, true);
        $sCrudNamespace = "Crud\\$sModule";
        $this->sCrudEditorFqClassname = $sCrudNamespace."\\Crud$sManager";
    }
    function doDelete()
    {
        $iCrudEditorButtonId = $this->get('crud_editor_button_id', null, true, 'numeric');
        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $sName = $this->get('name');
        $sEventScope = $this->get('event_scope', null, true);

        EditViewButtonHelper::deleteButton($this->sCrudEditorFqClassname, $sName,  $iCrudEditorButtonId);
        $sUrlTpl = '/generic/crud/editor_buttons?module=%s&manager=%s&name=%s&event_scope=%s';
        $sUrl = sprintf($sUrlTpl, $sModule, $sManager, $sName, $sEventScope);
        StatusMessage::success(Translate::fromCode("De knop is verwijderd."));
        $this->redirect($sUrl);
    }
    function doCreateCrudEditorButton()
    {
        $sTitle = $this->post('title', null, false);

        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $sName = $this->get('name');
        $sEventScope = $this->get('event_scope', null, true);

        $sUrlTpl = '/generic/crud/editor_buttons?module=%s&manager=%s&name=%s&event_scope=%s';
        $sUrl = sprintf($sUrlTpl, $sModule, $sManager, $sName, $sEventScope);

        if(empty($sTitle))
        {
            StatusMessage::warning(Translate::fromCode("U moet verplicht een titel opgeven voor de knop"));
            $this->redirect($sUrl);
        }
        else if (strlen($sTitle) < 4 && strlen($sTitle) > 100)
        {
            StatusMessage::warning(Translate::fromCode("De titel van de knop moet minimaal 4 tekens lang zijn en mag maximaal 100 tekens lang zijn."));

        }
        StatusMessage::success(Translate::fromCode("De knop is aangemaakt, u kunt de knop nu instellen."));
        EditViewButtonHelper::createBlankButton($this->sCrudEditorFqClassname, $sName, $sTitle);
        $this->redirect($sUrl);
    }
    function doGetFieldOperators()
    {
        $sSelectedFieldDatatype=null;
        $iOperator = $this->get('operator');
        $sFilterField = $this->post('field');
        $sModule = $this->get('module', null, true);
        $oCrudManager = new $this->sCrudEditorFqClassname();
        $aCrudFields = CrudManagerHelper::getAllFieldObjects($oCrudManager, $sModule, $sFilterField);
        $oAllFilterOperatorQuery = FilterOperatorQuery::create();

        if($aCrudFields['selected_field'] instanceof IFilterableField)
        {
            $oFilterDatatype = FilterDatatypeQuery::create()->findOneByName($aCrudFields['selected_field']->getDataType());
            $oFilterOperatorDatatype = FilterOperatorDatatypeQuery::create();
            $oFilterOperatorDatatype->filterByFilterDatatypeId($oFilterDatatype->getId());
            $aOperators = $oFilterOperatorDatatype->find();

            $oAllFilterOperatorQuery->filterByFilterOperatorDatatype($aOperators);
            $sSelectedFieldDatatype = $aCrudFields['selected_field']->getDataType();
        }
        $aAllFilterOperators = $oAllFilterOperatorQuery->find();
        $aOperatorFilterSelectItems = [];
        if(!$aAllFilterOperators->isEmpty())
        {
            foreach($aAllFilterOperators as $oOperator)
            {
                $sSelected = '';
                if($iOperator == $oOperator->getId())
                {
                    $sSelected = 'selected="selected"';
                }
                $aOperatorFilterSelectItems[] = ['id' => $oOperator->getId(), 'selected' => $sSelected ,'label' => $oOperator->getDescriptionSentence(), 'name' => $oOperator->getName()];
            }
        }

        $aResponse = [
            'selected_field_datatype' => $sSelectedFieldDatatype,
            'selected_field_lookups' => $aCrudFields['selected_field_lookups'],
            'operator_filter_select_items' => $aOperatorFilterSelectItems,
        ];
        exit(json_encode($aResponse));
    }
    function doDeleteVisibilityCriteria()
    {
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');
        $iCriteriaId = $this->post('crud_editor_button_visibile_filter_id', null, true, 'numeric');

        CrudEditorButtonVisibileFilterQuery::create()->filterByCrudEditorButtonId($iCrudEditorButtonId)->filterById($iCriteriaId)->findOne()->delete();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }
    function doSaveButtonVisibilityCriteria()
    {
        $sModule = $this->get('module', null, true);
        $iOperatorId = $this->post('operator', null, true, 'numeric');
        $sFieldValue = $this->post('field_value', null);
        $sField = $this->post('field');
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');

        $sFieldName = '\\Crud\\'.$sModule.'\\Field\\'.$sField;

        $oCrudEditorButtonVisibileFilter = CrudEditorButtonVisibileFilterQuery::create()->filterByCrudEditorButtonId($iCrudEditorButtonId)->orderBySorting(Criteria::DESC)->findOne();
        $iNextSorting = 0;
        if($oCrudEditorButtonVisibileFilter)
        {
            $iNextSorting = $oCrudEditorButtonVisibileFilter->getSorting() + 1;
        }

        $oCrudEditorButtonVisibileFilter = new CrudEditorButtonVisibileFilter();
        $oCrudEditorButtonVisibileFilter->setCrudEditorButtonId($iCrudEditorButtonId);
        $oCrudEditorButtonVisibileFilter->setSorting($iNextSorting);
        $oCrudEditorButtonVisibileFilter->setFilterOperatorId($iOperatorId);
        $oCrudEditorButtonVisibileFilter->setFilterValue($sFieldValue);
        $oCrudEditorButtonVisibileFilter->setFilterName($sFieldName);
        $oCrudEditorButtonVisibileFilter->save();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }

    function doDeleteEvent()
    {
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');
        $iCrudEditorButtonEventId = $this->post('crud_editor_button_event_id', null, true, 'numeric');

        $oCrudEditorButtonEventQuery = CrudEditorButtonEventQuery::create();
        $oCrudEditorButtonEventQuery->filterById($iCrudEditorButtonEventId);
        $oCrudEditorButtonEventQuery->filterByCrudEditorButtonId($iCrudEditorButtonId);
        $oCrudEditorButtonEventQuery->findOne()->delete();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }
    function doAddEvent()
    {
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');
        $sEventClass = $this->post('event_class', null, true);

        $oCrudEditorButtonEventQuery = CrudEditorButtonEventQuery::create();
        $oCrudEditorButtonEventQuery->filterByCrudEditorButtonId($iCrudEditorButtonId);
        $oCrudEditorButtonEventQuery->orderBySorting(Criteria::DESC);
        $oLatestCrudEditorButtonEvent = $oCrudEditorButtonEventQuery->findOne();

        $iSorting = 0;
        if($oLatestCrudEditorButtonEvent instanceof CrudEditorButtonEvent)
        {
            $iSorting = $oLatestCrudEditorButtonEvent->getSorting() + 1;
        }
        $oCrudEditorButtonEvent = new CrudEditorButtonEvent();
        $oCrudEditorButtonEvent->setSorting($iSorting);
        $oCrudEditorButtonEvent->setEventClass($sEventClass);
        $oCrudEditorButtonEvent->setCrudEditorButtonId($iCrudEditorButtonId);
        $oCrudEditorButtonEvent->save();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }

    function run()
    {
        $this->addJsFile('/admin_modules/Generic/Crud/editor_buttons_criteria.js');
        $this->addJsFile('/admin_modules/Generic/Crud/editor_buttons_events.js');
        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $sName = $this->get('name');
        $sEventScope = $this->get('event_scope', null, true);

        $oCrudManager = new $this->sCrudEditorFqClassname();

        if(!$oCrudManager instanceof FormManager)
        {
            throw new LogicException(Translate::fromCode("Crud editor niet gevonden."));
        }

        $aCrudFields = CrudManagerHelper::getAllFieldObjects($oCrudManager, $sModule);
        $aCrudEditorViewButtons = EditViewButtonHelper::getViewButtons($this->sCrudEditorFqClassname, $sName);
        $aIcons = Icons::getAll();
        $aAllEvents = EventHelper::getAllByScope($sEventScope);

        $aContentData = [
            'buttons' => $aCrudEditorViewButtons,
            'event_scope' => $sEventScope,
            'events' => $aAllEvents,
            'icons' => $aIcons,
            'crud_fields' => $aCrudFields,
            'module' => $sModule,
            'manager' => $sManager,
            'name' => $sName,
        ];

        $aView =
        [
            'title' => Translate::fromCode('Knoppen toevoegen aan order scherm'), /* @todo Later een keer generiek maken */
            'content' =>  $this->parse('Generic/Crud/editor_buttons.twig' , $aContentData)
        ];
        return $aView;
    }
    function doStoreButtonSorting()
    {
        $aIds = $this->post('aIds');

        if(!empty($aIds))
        {
            foreach($aIds as $aItem)
            {
                $oCrudEditorButton = CrudEditorButtonQuery::create()->findOneById($aItem['id']);
                $oCrudEditorButton->setSorting($aItem['sorting']);
                $oCrudEditorButton->save();
            }
        }
        StatusMessage::success(Translate::fromCode("Volgorde gewijzigd"));
        Utils::jsonOk();
    }
    function doStoreButtonSetting()
    {
        $iButtonId = $this->post('id', null, true, 'numeric');
        $sFieldName = $this->post('field', null, true);
        $sFieldValue = $this->post('value', null, true);
        $oCrudEditorButton = CrudEditorButtonQuery::create()->findOneById($iButtonId);

        if($oCrudEditorButton instanceof CrudEditorButton)
        {
            if($sFieldName == 'icon_after_click')
            {
                $oCrudEditorButton->setIconAfterClick($sFieldValue);
            }
            else if($sFieldName == 'icon_before_click')
            {
                $oCrudEditorButton->setIconBeforeClick($sFieldValue);
            }
            else if($sFieldName == 'is_reusable')
            {
                $oCrudEditorButton->setIsReusable($sFieldValue);
            }
            else if($sFieldName == 'color_after_click')
            {
                $oCrudEditorButton->setColorAfterClick($sFieldValue);
            }
            else if($sFieldName == 'color_before_click')
            {
                $oCrudEditorButton->setColorBeforeClick($sFieldValue);
            }
            else if($sFieldName == 'title')
            {
                if(strlen($sFieldValue) > 4)
                {
                    $oCrudEditorButton->setTitle($sFieldValue);
                }
            }
            $oCrudEditorButton->save();
        }
        Utils::jsonOk();
    }
}
