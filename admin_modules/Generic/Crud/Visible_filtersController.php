<?php
namespace AdminModules\Generic\Crud;

use Core\Cfg;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\IConfigurableCrud;
use Crud\IFilterableField;
use Crud\IFilterableLookupField;
use Crud\FormManager;
use Crud\Field;
use Helper\FilterHelper;
use InvalidArgumentException;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudViewVisibleFilterQuery;
use Model\Setting\CrudManager\CrudViewVisibleFilter;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;

class Visible_filtersController extends MainController{
    use LoadManagerTrait;

    private $oCrudManager;
    private $sCrudNamespace;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }

    /**
     * @param $sFieldName
     * @return Field
     */
    private function getField($sFieldName)
    {
        if(Cfg::get('CUSTOM_NAMESPACE') && class_exists($this->sCrudNamespace."\\Field\\".Cfg::get('CUSTOM_NAMESPACE')."\\$sFieldName"))
        {
            $sFieldClassName = $this->sCrudNamespace."\\Field\\".Cfg::get('CUSTOM_NAMESPACE')."\\$sFieldName";
        }
        else
        {
            $sFieldClassName = $this->sCrudNamespace."\\Field\\$sFieldName";
        }


        $oField = new $sFieldClassName;
        if(!$oField instanceof Field)
        {
            throw new InvalidArgumentException("Propbeerde de CRUD velden van schijf te laden om de bijbehorende titelatuur op te halen maar bij laden krijg ik een onjuist object terug.");
        }
        return $oField;
    }
    function doDeleteFilter()
    {
        $aUrlArguments =
            [
                'module' => $this->get('module', null, true),
                'manager' => $this->get('manager', null, true),
                'tab' => $this->get('tab', null, true, 'numeric'),
                'site' => $this->get('site', null, false)
            ];

        $iId = $this->get('id', null, true, 'numeric');

        $oCrudViewVisibleFilterQuery = CrudViewVisibleFilterQuery::create();
        $oCrudViewVisibleFilter = $oCrudViewVisibleFilterQuery->findOneById($iId);

        if($oCrudViewVisibleFilter instanceof CrudViewVisibleFilter)
        {
            $oCrudViewVisibleFilter->delete();
            StatusMessage::success(Translate::fromCode("Filter verwijderd"));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Filter niet gevonden, was hij al verwijderd?"));
        }

        $sCurrentUrl = $this->getRequestUri(false);
        $sNextUrl = $this->makeUrl($sCurrentUrl, $aUrlArguments);
        $this->redirect($sNextUrl);
    }

    function doSaveFilter()
    {
        $aUrlArguments = [
            'module' => $this->get('module', null, true),
            'manager' => $this->get('manager', null, true),
            'tab' => $this->get('tab', null, true, 'numeric'),
            'site' => $this->get('site', null, false),
        ];

        $sOperator = $this->get('operator');
        $sField = $this->get('field');

        if(!empty($sOperator) && !empty($sField))
        {
            $oCrudViewVisibleFilter = new CrudViewVisibleFilter();
            $oCrudViewVisibleFilter->setCrudViewId($this->get('tab'));
            $oCrudViewVisibleFilter->setFilterName($sField);
            $oCrudViewVisibleFilter->setFilterOperatorId($sOperator);
            $oCrudViewVisibleFilter->setFilterDefaultValue($this->get('value'));
            $oCrudViewVisibleFilter->save();
        }
        $sCurrentUrl = $this->getRequestUri(false);
        $sNextUrl = $this->makeUrl($sCurrentUrl, $aUrlArguments);
        $this->redirect($sNextUrl);
    }

    /**
     * @return array
     */
    function run()
    {
        $sReturnUrl = $this->get('r');

        $iTabId = $this->get('tab', null, true, 'numeric');
        // Only from the cms needed, please just leave this in.
        $sSite = $this->get('site', null, false);

        if(!$this->oCrudManager  instanceof FormManager)
        {
            throw new InvalidArgumentException("Probeerde een CRUD te instantieren maar klassenamen klopt niet.");
        }
        if(!$this->oCrudManager instanceof IConfigurableCrud)
        {
            throw new LogicException("Crud must implement IConfigurableCrud for this functionality in ".__METHOD__." ".get_class($this->oCrudManager));
        }

        $oCrudView = new CrudView();
        if($iTabId)
        {
            $oCrudView = CrudViewQuery::create()->findOneById($iTabId);
        }
        $sModule = $this->get('module');
        $sFilterField = $this->get('field');
        $iOperator = $this->get('operator');
        $sManager = $this->get('manager');
        $sFilterValue = $this->get('value');
        $sFieldNamespace = "\\Crud\\$sModule\\Field\\";

        $aFields = [];
        $oSelectedField = null;
        $aSelectedFieldLookups = null;

        foreach($this->oCrudManager->getAllAvailableFields() as $sFieldName)
        {

            $oField = $this->getField($sFieldName);

            if($oField instanceof IFilterableField)
            {
                $sSelected = '';


                if($sFilterField == get_class($oField))
                {
                    $sSelected = 'selected="selected"';
                    $oSelectedField = $oField;
                }
                $aField = [
                    'name' => get_class($oField),
                    'selected' => $sSelected ,
                    'title' => $oField->getFieldTitle()
                ];

                if($oField->getDataType() == 'lookup')
                {
                    if($oField instanceof IFilterableLookupField)
                    {

                        $oReflector = new \ReflectionClass($oField);

                        if($sFilterField == $oReflector->getName())
                        {
                            $aSelectedFieldLookups = $oField->getLookups();
                        }
                    }
                    else
                    {
                        throw new LogicException("Filtervelden van het datatype lookup moeten InterfaceFilterableLookupField implementeren, dit is niet gebeurd in ".get_class($oField)."." );
                    }
                }

                $aFields[] = $aField;
            }
        }

        Utils::usort_strcmp($aFields, 'title');

        $aTabFilters = FilterHelper::getVisibleTabFilters($iTabId);

        $aCurrentFilterQuery = CrudViewVisibleFilterQuery::create();
        $aCurrentFilterQuery->filterByCrudViewId($iTabId);

        $oAllFilterOperatorQuery = FilterOperatorQuery::create();
        $oFilterDatatype = null;
        $sSelectedFieldDatatype = '';
        if($oSelectedField instanceof IFilterableField)
        {
            $oFilterDatatype = FilterDatatypeQuery::create()->findOneByName($oSelectedField->getDataType());
            if(!$oFilterDatatype)
            {
                throw new LogicException("Datatype not found: " . $oSelectedField->getDataType());
            }
            $oFilterOperatorDatatype = FilterOperatorDatatypeQuery::create();
            $oFilterOperatorDatatype->filterByFilterDatatypeId($oFilterDatatype->getId());
            $aOperators = $oFilterOperatorDatatype->find();
            $oAllFilterOperatorQuery->filterByFilterOperatorDatatype($aOperators);
            $sSelectedFieldDatatype = $oSelectedField->getDataType();
        }
        $aAllFilterOperators = $oAllFilterOperatorQuery->find();
        $aOperatorFilterSelectItems = Utils::makeSelectOptions($aAllFilterOperators, 'getDescription', $iOperator);
        Utils::usort_strcmp($aOperatorFilterSelectItems, 'label');

        $aViewData = [
            'site' => $sSite,
            'fields' => $aFields,
            'tab_id' => $iTabId,
            'operator' => $iOperator,
            'module' => $sModule,
            'value' => $sFilterValue,
            'manager' => $sManager,
            'tab_filters' => $aTabFilters,
            'field_namespace' => $sFieldNamespace,
            'selected_field_datatype' => $sSelectedFieldDatatype,
            'selected_field_filter_data_type' => $oFilterDatatype, // string, number, boolean, lookup
            'selected_field_lookups' => $aSelectedFieldLookups,
            'crud_view' => $oCrudView,
            'operator_filter_select_items' => $aOperatorFilterSelectItems
        ];


        if(!$this->oCrudManager  instanceof FormManager && !$this->oCrudManager  instanceof IConfigurableCrud)
        {
            throw new InvalidArgumentException("Probeerde een CRUD te instantieren maar klassenamen klopt niet.");
        }

        $sOverviewUrl = $this->oCrudManager->getOverviewUrl();
        if($sReturnUrl)
        {
           $sOverviewUrl = DeferredAction::get($sReturnUrl);
        }



        $aTopNavData = [
            'crud_views' => CrudViewManager::getViews($this->oCrudManager),
            'overview_url' => $sOverviewUrl,
            'module' => $sModule,
            'manager' => $sManager,
            'tab_id' => $iTabId,
        ];

        $aView = [
            'title' => Translate::fromCode('Filters instellen'),
            'top_nav' => $this->parse('top_nav_overview.twig', $aTopNavData),
            'content'   => $this->parse('Generic/Crud/visible_filters.twig', $aViewData)
        ];
        return $aView;
    }
}
