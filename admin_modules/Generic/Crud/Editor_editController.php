<?php

namespace AdminModules\Generic\Crud;

use Core\Cfg;
use Core\DeferredAction;
use Core\Json\JsonUtils;
use Core\MainController;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\BaseManager;
use Crud\CrudElement;
use Crud\CrudViewManager;
use Crud\IAppOnlyField;
use Crud\IEventField;
use Crud\IInlineEditor;
use Crud\IRecurringField;
use Exception\ClassNotFoundException;
use Exception\LogicException;
use Helper\EditviewHelper;
use Model\Setting\CrudManager\CrudEditorBlockQuery;
use Propel\Runtime\Exception\PropelException;
use ReflectionClass;
use ReflectionException;

class Editor_editController extends MainController {
    use LoadManagerTrait;

    private $oCrudManager;
    private $sCrudNamespace;

    function __construct($aGet, $aPost) {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }

    /**
     * @throws PropelException
     */
    function doStoreBlockSorting() {
        $aIds = $this->post('aIds');

        if (!empty($aIds)) {
            foreach ($aIds as $aItem) {
                CrudEditorBlockQuery::create()->findOneById($aItem['id'])->setSorting($aItem['sorting'])->save();
            }
        }
        StatusMessage::success(Translate::fromCode("Volgorde gewijzigd"));
        Utils::jsonOk();
    }

    function doSaveCrudEditor() {
        $sModule = $this->get('module', null, true);
        $sManager = $this->get('manager', null, true);
        $sName = $this->get('name', null, true);
        $sTitle = $this->get('title', null, true);
        $sDoAfterSave = $this->post('do_after_save', null, false);

        $sJsonData = $this->post('data', null, true);
        $iCrudEditorId = $this->post('crud_editor_id', null, true, 'numeric');


        $this->track($iCrudEditorId, $sJsonData);
        $aJsonData = JsonUtils::decode($sJsonData, true);
        $aJsonDataAfter = EditviewHelper::storeLayout($iCrudEditorId, $aJsonData, $this->sCrudNamespace);
        $this->track($iCrudEditorId, $aJsonDataAfter);

        StatusMessage::success(Translate::fromCode('Wijzigingen opgeslagen'));

        if ($sDoAfterSave === 'dashboard') {
            $sUrl = '/';
        } else {
            if ($sDoAfterSave) {
                $sUrl = DeferredAction::get($sDoAfterSave);
            } else {
                $aArguments = [
                    'module'  => $sModule,
                    'manager' => $sManager,
                    'name'    => $sName,
                    'title'   => $sTitle,
                ];
                $sUrl = $this->makeUrl('/generic/crud/editor_edit', $aArguments);
            }
        }
        $this->redirect($sUrl);
        exit();
    }

    /**
     * @throws PropelException
     */
    function doDeleteBlock() {
        $iCrudEditorBlockId = $this->get('crud_editor_block_id');
        $oCrudEditorBlock = CrudEditorBlockQuery::create()->findOneById($iCrudEditorBlockId);
        $oCrudEditorBlock->delete();
        StatusMessage::success("Formulier blok verwijderd");
        $this->redirect(DeferredAction::get('after_crud_block_delete_url'));
    }

    function track(int $iCrudEditorId, $mData) {
        // Just logging
        $sKey = "{$iCrudEditorId}";
        $iCounter = Setting::get($sKey, 1);
        Utils::store('crud_editor_config', "{$sKey}_{$iCounter}.json", $mData);
        Setting::store($sKey, ++$iCounter);
    }

    /**
     * @return array
     * @throws PropelException
     * @throws ReflectionException
     */
    function run() {
        DeferredAction::register('after_field_configuration_edit', $this->getRequestUri());
        $sViewName = $this->get('name', null, true);
        $sCrudTitle = $this->get('title', null, true);
        $sDoAfter = $this->get('do_after_save', null, false);
        $sSingleBlock = ($this->get('single_block') == 'true');

        DeferredAction::register('after_crud_block_delete_url', $this->getRequestUri());
        CrudViewManager::init($this->oCrudManager);

        if (!$this->oCrudManager instanceof BaseManager) {
            throw new ClassNotFoundException("Could not find CrudManager class.");
        }

        $oCrudEditor = EditviewHelper::loadEditorByName($this->oCrudManager, $sViewName, $sCrudTitle);

        $aAllCrudItems = $this->oCrudManager->getAllAvailableFields();

        $aAvailableFields = [];

        EditViewHelper::getCrudfieldsInUse($oCrudEditor);
        $aFieldsInUse = EditviewHelper::getCrudfieldsInUse($oCrudEditor);

        foreach ($aAllCrudItems as $sCrudItem) {
            $oCrudItemObject = $this->getField($sCrudItem);
            $sFieldClassName = get_class($oCrudItemObject);
            $sFieldShortClassName = (new ReflectionClass($oCrudItemObject))->getShortName();

            if (!$oCrudItemObject->isEditorField()) {
                continue;
            }
            if ($oCrudItemObject instanceof IEventField) {
                continue;
            }

            if ($this->get('form_type') !== 'app' && $oCrudItemObject instanceof IAppOnlyField) {
                continue;
            }

            if (in_array($sFieldShortClassName, [
                'Edit',
                'Delete',
            ])) {
                continue;
            }

            // De foreach loop hieronder is een zeer omslachtige methode voor iets wat ook met in_array moet kunnen.
            // Om een of andere reden gaf die alleen false af bij iets wat true lijkt te zijn.
            $bPlaceOnScreen = true;

            if (!$oCrudItemObject instanceof IRecurringField) {
                foreach ($aFieldsInUse as $sFieldInUse) {

                    $sSomeClassThatIsAlreadyOnScreen = array_reverse(explode('\\', $sFieldInUse))[0];

                    if ($sSomeClassThatIsAlreadyOnScreen === $sFieldShortClassName) {
                        $bPlaceOnScreen = false;
                        break;
                    }
                }
            }

            if ($bPlaceOnScreen) {
                $sIcon = '';
                if ($oCrudItemObject instanceof IAppOnlyField) {
                    $sIcon = 'mobile';
                }
                $aAvailableFields[] = [
                    'icon'           => $sIcon,
                    'object'         => $oCrudItemObject,
                    'full_block'     => ($oCrudItemObject instanceof IAppOnlyField),
                    'inline_editor'  => ($oCrudItemObject instanceof IInlineEditor),
                    'classname'      => $sCrudItem,
                    'full_classname' => $sFieldClassName,
                ];
            }
        }

        usort($aAvailableFields, function (array $a, array $b) {

            if (!$a['object'] instanceof CrudElement) {
                throw new LogicException("Field should be an instance of Field");
            }
            if (!$b['object'] instanceof CrudElement) {
                throw new LogicException("Field should be an instance of Field");
            }

            return strcasecmp($a['object']->getTranslatedTitle(), $b['object']->getTranslatedTitle());
        });

        $aContentData = [
            'available_fields' => $aAvailableFields,
            'crud_editor'      => $oCrudEditor,
            'do_after_save'    => $sDoAfter,
            'single_block'     => $sSingleBlock,
            'request_uri'      => Utils::getRequestUri(),
        ];

        DeferredAction::register('back_to_editor', $this->getRequestUri());

        $aTopNavVars = [
            'crud_editor_return_url' => DeferredAction::get('crud_editor_return_url'),
            'current_editor_id'      => $oCrudEditor->getId(),
            'similar_editors'        => $oCrudEditor->getCrudConfig()->getCrudEditors(),
        ];
        return [
            'title'   => Translate::fromCode("Scherm weergave aanpassen"),
            'top_nav' => $this->parse('Generic/Crud/editor_edit_top_nav.twig', $aTopNavVars),
            'content' => $this->parse('Generic/Crud/editor_edit.twig', $aContentData),
        ];
    }

    /**
     * @param $sFieldName
     * @return CrudElement
     */
    private function getField($sFieldName) {
        if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($this->sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName")) {
            $sFieldClassName = $this->sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
        } else {
            $sFieldClassName = $this->sCrudNamespace . "\\Field\\$sFieldName";
        }

        $oField = new $sFieldClassName;
        if (!$oField instanceof CrudElement) {
            throw new LogicException("All fields inside this editor must derive from CrudElement");
        }
        return $oField;
    }
}
