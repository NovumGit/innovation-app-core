<?php
namespace AdminModules\Generic\Crud;

use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Setting\CrudManager\Base\CrudEditorQuery;

class Editor_copyController extends MainController
{
    function doCopy()
    {
        $iSource = $this->get('source', null, true, 'numeric');
        $iDestination = $this->get('dest', null, true, 'numeric');
        $sR = $this->get('r', null, true);

        $oSouceEditor = CrudEditorQuery::create()->findOneById($iSource);
        $oDestEditor = CrudEditorQuery::create()->findOneById($iDestination);

        $oDestEditor->getCrudEditorBlocks()->delete();

        $aCrudEditorBlocks = $oSouceEditor->getCrudEditorBlocks();

        foreach($aCrudEditorBlocks as $oCrudEditorBlock)
        {


            $oNewCrudEditorBlock = $oCrudEditorBlock->copy(true);
            $oNewCrudEditorBlock->setCrudEditorId($oDestEditor->getId());
            $oNewCrudEditorBlock->save();
        }

        $sNextUrl = DeferredAction::get($sR);
        StatusMessage::success(Translate::fromCode("The editor settings are copied over."));
        $this->redirect($sNextUrl);

    }
    function run()
    {
        $aTopNavData = [
            'back_url' =>  DeferredAction::get($this->get('r'))
        ];
        $aContentData = [
            'proceed_url' => $this->getRequestUri().'&_do=Copy'
        ];
        $aView =
            [
                'title' => Translate::fromCode("Weet u het heel zeker?"),
                'top_nav' => $this->parse('Generic/Crud/editor_copy_sure_top_nav.twig' , $aTopNavData),
                'content' =>  $this->parse('Generic/Crud/editor_copy_sure.twig' , $aContentData)
            ];
        return $aView;
    }
}