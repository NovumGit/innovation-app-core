<?php
namespace AdminModules\Generic\Crud\Button\Overview;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\MainController;
use Core\Translate;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudViewButtonEventQuery;
use Model\Setting\CrudManager\CrudViewButtonEvent;

class Event_configController extends MainController{

    private function getButtonEvent()
    {
        $iEventId = $this->get('event_id', null, true, 'numeric');

        $oCrudViewButtonEventQuery = CrudViewButtonEventQuery::create();
        $oCrudViewButtonEvent = $oCrudViewButtonEventQuery->findOneById($iEventId);

        if(!$oCrudViewButtonEvent instanceof CrudViewButtonEvent)
        {
            throw new LogicException("\$oCrudEditorButtonEvent should be an instance of CrudEditorButtonEvent");
        }
        $sEventClass = $oCrudViewButtonEvent->getEventClass();
        $oEventClass = new $sEventClass($_GET, $_POST);

        if(!$oEventClass instanceof AbstractConfigurableEvent )
        {
            throw new LogicException("\$oEventClass should be an instance of AbstractConfigurableEvent");
        }

        return $oEventClass;
    }
    function doSaveChanges()
    {
        $oEventClass = $this->getButtonEvent();
        if(!$oEventClass instanceof AbstractEvent || !$oEventClass instanceof AbstractConfigurableEvent )
        {
            throw new LogicException("\$oEventClass should be an instance of AbstractEvent");
        }
        $oEventClass->saveConfiguration('overview');
    }
    function run()
    {
        $iEventId = $this->get('event_id', null, true, 'numeric');
        $oEventClass = $this->getButtonEvent();

        if(!$oEventClass instanceof AbstractConfigurableEvent)
        {
            throw new LogicException("\$oEventClass must implement AbstractConfigurableEvent");
        }
        $aContentData = [
            'configuration_html' => $oEventClass->getConfiguratorHtml($iEventId, 'overview')
        ];
        $aView = [
            'title'=> Translate::fromCode("Event configureren."),
            'content' =>  $this->parse('Generic/Crud/Button/Edit/event_config.twig' , $aContentData)
        ];
        return $aView;
    }
}