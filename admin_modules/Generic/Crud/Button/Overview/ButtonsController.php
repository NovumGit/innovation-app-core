<?php
namespace AdminModules\Generic\Crud\Button\Overview;

use AdminModules\Generic\Crud\LoadManagerTrait;
use Core\Icons;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\IFilterableField;
use Crud\FormManager;
use Exception\LogicException;
use Helper\CrudManagerHelper;
use Helper\OverviewButtonHelper;
use Helper\EventHelper;
use Model\Setting\CrudManager\Base\CrudViewButtonEventQuery;
use Model\Setting\CrudManager\Base\CrudViewButtonQuery;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery;
use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonEvent;
use Model\Setting\CrudManager\FilterDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class ButtonsController extends MainController{

    use LoadManagerTrait;

    private $sCrudNamespace;
    private $oCrudManager;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
        /*
        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager', null, true);
        $sCrudNamespace = "Crud\\$sModule";
        $this->sCrudFqClassname = $sCrudNamespace."\\Crud$sManager";
        */
    }

    /**
     * @return array
     */
    function run()
    {
        $this->addJsFile('/admin_modules/Generic/Crud/Button/editor_buttons_criteria.js');
        $this->addJsFile('/admin_modules/Generic/Crud/Button/Find/events.js');

        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $iTabId = $this->get('tab');
        $sEventScope = $this->get('event_scope', null, true);

        if(!$this->getManager() instanceof FormManager)
        {
            throw new LogicException(Translate::fromCode("Crud editor niet gevonden."));
        }

        $aCrudFields = CrudManagerHelper::getAllFieldObjects($this->oCrudManager, $sModule);
        $aCrudOverviewButtons = OverviewButtonHelper::getViewButtons($iTabId);


        $aIcons = Icons::getAll();
        $aAllEvents = EventHelper::getAllByScope($sEventScope);
        $sFormType = $this->get('form_type');

        if($sFormType == 'app')
        {
            $sTitle  = 'Logica toevoegen';
            $sConfigureTabLabel  = 'Flow instellingen';
            $sActionTabLabel  = 'Als de flow start dan';
            $sNewItemPlaceholder  = 'Voer een flow titel in en druk op enter';
        }
        else
        {
            $sTitle  = 'Knoppen toevoegen aan overzicht';
            $sConfigureTabLabel  = 'Knop instellingen';
            $sActionTabLabel  = 'Als er op de knop wordt geklikt dan';
            $sNewItemPlaceholder = 'Voer een knop titel in en druk op enter';
        }

        $aContentData = [
            'title' => $sTitle,
            'new_item_placeholder' => $sNewItemPlaceholder,
            'action_tab_label' => $sActionTabLabel,
            'configure_tab_label' => $sConfigureTabLabel,
            'buttons' => $aCrudOverviewButtons,
            'event_scope' => $sEventScope,
            'events' => $aAllEvents,
            'icons' => $aIcons,
            'crud_fields' => $aCrudFields,
            'module' => $sModule,
            'manager' => $sManager,
            'tab' => $iTabId
        ];


        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($this->oCrudManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);

            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }

        $aTopNavData = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'site' => $this->get('site'),
            'module' => $sModule,
            'manager' => $sManager,
            'has_create_custom_buttons' => true,
            'event_scope' => $sEventScope,
            'overview_url' => $this->oCrudManager->getOverviewUrl(),
            'new_url' => $this->oCrudManager->getCreateNewUrl(),
            'new_title' => $this->oCrudManager->getNewFormTitle()
        ];


        $aView = [
            'top_nav' =>  $this->parse('top_nav_overview.twig' , $aTopNavData),
            'title' => Translate::fromCode('Knoppen toevoegen aan overzicht.'),
            'content' =>  $this->parse('Generic/Crud/Button/Find/buttons.twig' , $aContentData)
        ];
        return $aView;
    }

    /**
     * @throws \ReflectionException
     */
    function doDelete()
    {
        $iCrudEditorButtonId = $this->get('crud_editor_button_id', null, true, 'numeric');
        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $iTabId = $this->get('tab');
        $sEventScope = $this->get('event_scope', null, true);

        OverviewButtonHelper::deleteButton($this->getManager()->getFqClassName(), $iTabId,  $iCrudEditorButtonId);
        $sUrlTpl = '/generic/crud/button/overview/buttons?module=%s&manager=%s&tab=%s&event_scope=%s';
        $sUrl = sprintf($sUrlTpl, $sModule, $sManager, $iTabId, $sEventScope);
        StatusMessage::success(Translate::fromCode("De knop is verwijderd."));
        $this->redirect($sUrl);
    }

    /**
     * @throws \ReflectionException
     */
    function doCreateCrudEditorButton()
    {
        $sTitle = $this->post('title', null, false);

        $sModule = $this->get('module', null, true);
        $sManager =  $this->get('manager');
        $iTabId = $this->get('tab');

        $sEventScope = $this->get('event_scope', null, true);

        $sUrlTpl = '/generic/crud/button/overview/buttons?module=%s&manager=%s&tab=%s&event_scope=%s';
        $sUrl = sprintf($sUrlTpl, $sModule, $sManager, $iTabId, $sEventScope);

        if(empty($sTitle))
        {
            StatusMessage::warning(Translate::fromCode("U moet verplicht een titel opgeven voor de knop"));
            $this->redirect($sUrl);
        }
        else if (strlen($sTitle) < 4 && strlen($sTitle) > 100)
        {
            StatusMessage::warning(Translate::fromCode("De titel van de knop moet minimaal 4 tekens lang zijn en mag maximaal 100 tekens lang zijn."));

        }
        StatusMessage::success(Translate::fromCode("De knop is aangemaakt, u kunt de knop nu instellen."));

        OverviewButtonHelper::createBlankButton($this->getManager()->getFqClassName(), $iTabId, $sTitle);
        $this->redirect($sUrl);
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doGetFieldOperators()
    {
        $sSelectedFieldDatatype=null;
        $iOperator = $this->get('operator');
        $sFilterField = $this->post('field');
        $sModule = $this->get('module', null, true);

        $aCrudFields = CrudManagerHelper::getAllFieldObjects($this->getManager(), $sModule, $sFilterField);
        $oAllFilterOperatorQuery = FilterOperatorQuery::create();

        if($aCrudFields['selected_field'] instanceof IFilterableField)
        {
            $oFilterDatatype = FilterDatatypeQuery::create()->findOneByName($aCrudFields['selected_field']->getDataType());
            $oFilterOperatorDatatype = FilterOperatorDatatypeQuery::create();
            $oFilterOperatorDatatype->filterByFilterDatatypeId($oFilterDatatype->getId());
            $aOperators = $oFilterOperatorDatatype->find();

            $oAllFilterOperatorQuery->filterByFilterOperatorDatatype($aOperators);
            $sSelectedFieldDatatype = $aCrudFields['selected_field']->getDataType();
        }
        $aAllFilterOperators = $oAllFilterOperatorQuery->find();

        $aOperatorFilterSelectItems = [];
        if(!$aAllFilterOperators->isEmpty())
        {
            foreach($aAllFilterOperators as $oOperator)
            {
                $sSelected = '';
                if($iOperator == $oOperator->getId())
                {
                    $sSelected = 'selected="selected"';
                }
                $aOperatorFilterSelectItems[] = ['id' => $oOperator->getId(), 'selected' => $sSelected ,'label' => $oOperator->getDescriptionSentence(), 'name' => $oOperator->getName()];
            }
        }

        $aResponse = [
            'selected_field_datatype' => $sSelectedFieldDatatype,
            'selected_field_lookups' => $aCrudFields['selected_field_lookups'],
            'operator_filter_select_items' => $aOperatorFilterSelectItems,
        ];
        exit(json_encode($aResponse));
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doDeleteVisibilityCriteria()
    {
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');
        $iCriteriaId = $this->post('crud_editor_button_visibile_filter_id', null, true, 'numeric');

        CrudEditorButtonVisibileFilterQuery::create()->filterByCrudEditorButtonId($iCrudEditorButtonId)->filterById($iCriteriaId)->findOne()->delete();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doSaveButtonVisibilityCriteria()
    {
        $sModule = $this->get('module', null, true);
        $iOperatorId = $this->post('operator', null, true, 'numeric');
        $sFieldValue = $this->post('field_value', null);
        $sField = $this->post('field');
        $iCrudEditorButtonId = $this->post('crud_editor_button_id', null, true, 'numeric');

        $sFieldName = '\\Crud\\'.$sModule.'\\Field\\'.$sField;

        $oCrudEditorButtonVisibileFilter = CrudEditorButtonVisibileFilterQuery::create()->filterByCrudEditorButtonId($iCrudEditorButtonId)->orderBySorting(Criteria::DESC)->findOne();
        $iNextSorting = 0;
        if($oCrudEditorButtonVisibileFilter)
        {
            $iNextSorting = $oCrudEditorButtonVisibileFilter->getSorting() + 1;
        }

        $oCrudEditorButtonVisibileFilter = new CrudEditorButtonVisibileFilter();
        $oCrudEditorButtonVisibileFilter->setCrudEditorButtonId($iCrudEditorButtonId);
        $oCrudEditorButtonVisibileFilter->setSorting($iNextSorting);
        $oCrudEditorButtonVisibileFilter->setFilterOperatorId($iOperatorId);
        $oCrudEditorButtonVisibileFilter->setFilterValue($sFieldValue);
        $oCrudEditorButtonVisibileFilter->setFilterName($sFieldName);
        $oCrudEditorButtonVisibileFilter->save();

        $aJsonView = $this->run();
        $aJsonView['crud_editor_button_id'] = $iCrudEditorButtonId;
        exit(json_encode($aJsonView));
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doDeleteEvent()
    {
        $iCrudViewButtonId = $this->post('crud_view_button_id', null, true, 'numeric');
        $iCrudViewButtonEventId = $this->post('crud_view_button_event_id', null, true, 'numeric');

        $oCrudViewButtonEventQuery = CrudViewButtonEventQuery::create();

        $oCrudViewButtonEventQuery->filterById($iCrudViewButtonEventId);
        $oCrudViewButtonEventQuery->filterByCrudViewButtonId($iCrudViewButtonId);
        $oCrudViewButtonEventQuery->findOne()->delete();

        $aJsonView = $this->run();
        $aJsonView['crud_view_button_id'] = $iCrudViewButtonId;
        exit(json_encode($aJsonView));
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doAddEvent()
    {
        $iCrudViewButtonId = $this->post('crud_overview_button_id', null, true, 'numeric');
        $sEventClass = $this->post('event_class', null, true);

        $oCrudOverviewButtonEventQuery = CrudViewButtonEventQuery::create();
        $oCrudOverviewButtonEventQuery->filterByCrudViewButtonId($iCrudViewButtonId);
        $oCrudOverviewButtonEventQuery->orderBySorting(Criteria::DESC);

        $oLatestCrudViewButtonEvent = $oCrudOverviewButtonEventQuery->findOne();

        $iSorting = 0;
        if($oLatestCrudViewButtonEvent instanceof CrudViewButtonEvent)
        {
            $iSorting = $oLatestCrudViewButtonEvent->getSorting() + 1;
        }
        $oCrudViewButtonEvent = new CrudViewButtonEvent();
        $oCrudViewButtonEvent->setSorting($iSorting);
        $oCrudViewButtonEvent->setEventClass($sEventClass);
        $oCrudViewButtonEvent->setCrudViewButtonId($iCrudViewButtonId);
        $oCrudViewButtonEvent->save();

        $aJsonView = $this->run();
        $aJsonView['crud_overview_button_id'] = $iCrudViewButtonId;
        exit(json_encode($aJsonView));
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doStoreOverviewButtonSorting()
    {
        $aIds = $this->post('aIds');

        if(!empty($aIds))
        {
            foreach($aIds as $aItem)
            {
                $oCrudButton = CrudViewButtonQuery::create()->findOneById($aItem['id']);
                $oCrudButton->setSorting($aItem['sorting']);
                $oCrudButton->save();
            }
        }
        StatusMessage::success(Translate::fromCode("Volgorde gewijzigd"));
        Utils::jsonOk();
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doStoreButtonSorting()
    {
        $aIds = $this->post('aIds');

        if(!empty($aIds))
        {
            foreach($aIds as $aItem)
            {
                $oCrudButton = CrudEditorButtonQuery::create()->findOneById($aItem['id']);
                $oCrudButton->setSorting($aItem['sorting']);
                $oCrudButton->save();
            }
        }
        StatusMessage::success(Translate::fromCode("Volgorde gewijzigd"));
        Utils::jsonOk();
    }

    /**
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function doStoreButtonSetting()
    {
        $iButtonId = $this->post('id', null, true, 'numeric');
        $sFieldName = $this->post('field', null, true);
        $sFieldValue = $this->post('value', null, true);

        $oCrudViewButton = CrudViewButtonQuery::create()->findOneById($iButtonId);

        if($oCrudViewButton instanceof CrudViewButton)
        {
            if($sFieldName == 'icon_after_click')
            {
                $oCrudViewButton->setIconAfterClick($sFieldValue);
            }
            else if($sFieldName == 'icon_before_click')
            {
                $oCrudViewButton->setIconBeforeClick($sFieldValue);
            }
            else if($sFieldName == 'color_after_click')
            {
                $oCrudViewButton->setColorAfterClick($sFieldValue);
            }
            else if($sFieldName == 'color_before_click')
            {
                $oCrudViewButton->setColorBeforeClick($sFieldValue);
            }
            else if($sFieldName == 'title')
            {
                if(strlen($sFieldValue) > 4)
                {
                    $oCrudViewButton->setTitle($sFieldValue);
                }
            }
            $oCrudViewButton->save();
        }
        Utils::jsonOk();
    }
}
