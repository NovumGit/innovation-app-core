
function bindEventEvents()
{
    $('.add_event').unbind('click');
    $('.add_event').on('click', function(e)
    {
        sToBeAddedEvent = $('.to_be_added_event', $(this).parent()).val();
        iButtonId = $(this).data('id');

        aData = {
            _do : 'AddEvent',
            crud_overview_button_id : iButtonId,
            event_class : sToBeAddedEvent
        };
        $.post(window.location, aData, function(data){

                sSelector = '#panel_event_body_'+data.crud_overview_button_id;

                newPanel = $(sSelector, data.content).html();

                $(sSelector).html(newPanel);
                bindEventEvents();
            },
            'json');
    });

    $('.delete_event').unbind('click');
    $('.delete_event').on('click', function(e)
    {
        e.preventDefault();
        iButtonId = $(this).data('id');
        iEventId = $(this).data('event-id');
        aData = {
            _do : 'DeleteEvent',
            crud_view_button_id : iButtonId,
            crud_view_button_event_id : iEventId,
        };
        $.post(window.location, aData, function(data){

            sSelector = '#panel_event_body_'+data.crud_view_button_id;
            newPanel = $(sSelector, data.content).html();

            $(sSelector).html(newPanel);
            bindEventEvents();
        }, 'json');
    });
}


bindEventEvents();