<?php
namespace AdminModules\Generic\Crud\Button\Edit;

use Core\AbstractConfigurableEvent;
use Core\AbstractEvent;
use Core\MainController;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudEditorButtonEventQuery;
use Model\Setting\CrudManager\CrudEditorButtonEvent;

class Event_configController extends MainController{

    private function getButtonEvent()
    {
        $iCrudEditorButtonEventId = $this->get('event_id', null, true, 'numeric');
        $oCrudEditorButtonEventQuery = CrudEditorButtonEventQuery::create();
        $oCrudEditorButtonEvent = $oCrudEditorButtonEventQuery->findOneById($iCrudEditorButtonEventId);

        $_GET['event_id'] = $iCrudEditorButtonEventId;

        if(!$oCrudEditorButtonEvent instanceof CrudEditorButtonEvent)
        {
            throw new LogicException("\$oCrudEditorButtonEvent should be an instance of CrudEditorButtonEvent");
        }
        $sEventClass = $oCrudEditorButtonEvent->getEventClass();
        $oEventClass = new $sEventClass($_GET, $_POST);

        if(!$oEventClass instanceof AbstractConfigurableEvent )
        {
            throw new LogicException("\$oEventClass should be an instance of AbstractConfigurableEvent");
        }

        return $oEventClass;
    }
    function doSaveChanges()
    {
        $oEventClass = $this->getButtonEvent();

        if(!$oEventClass instanceof AbstractEvent || !$oEventClass instanceof AbstractConfigurableEvent )
        {
            throw new LogicException("\$oEventClass should be an instance of AbstractEvent");
        }

        $oEventClass->saveConfiguration('edit');
    }

    function run()
    {
        $iCrudEditorButtonEventId = $this->get('event_id', null, true, 'numeric');
        $oEventClass = $this->getButtonEvent();

        if(!$oEventClass instanceof AbstractConfigurableEvent)
        {
            throw new LogicException("\$oEventClass must implement AbstractConfigurableEvent");
        }
        $aContentData = [
            'configuration_html' => $oEventClass->getConfiguratorHtml($iCrudEditorButtonEventId, 'edit')
        ];
        $aView =
        [
            'content' =>  $this->parse('Generic/Crud/Button/Edit/event_config.twig' , $aContentData)
        ];
        return $aView;
    }
}