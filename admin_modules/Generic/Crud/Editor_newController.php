<?php
namespace Modules\Generic\Crud;

use Core\MainController;
use Core\Utils;
use model\Setting\CrudManager\CrudEditor;
use model\Setting\CrudManager\CrudEditorQuery;

class Editor_newController extends MainController{

    function doCreateForm()
    {
        $sTitle = $this->get('title', null, true);
        $sModule = $this->get('module', null, true);
        $sManager = $this->get('manager', null, true);
        $sDoAfterSave = $this->get('do_after_save', null, true);

        $sName = Utils::slugify($sTitle);

        $oCrudEditor = CrudEditorQuery::create()->findOneByName($sName);
        if($oCrudEditor instanceof CrudEditor){
            $i = 1;
            while(($oCrudEditor = CrudEditorQuery::create()->findOneByName($sName.$i)) instanceof CrudEditor)
            {
                ++$i;
            }
            $sName .= $i;
        }
        $aContentData = [
            'do_after_save' => $sDoAfterSave,
            'manager' => $sModule,
            'module' => $sManager,
            'title' => $sTitle,
            'name' => $sName
        ];
        $sUrl = $this->makeUrl('/generic/crud/editor_edit', $aContentData);
        $this->redirect($sUrl);
    }
    function run()
    {
        $sModule = $this->get('module', null, true);
        $sManager = $this->get('manager', null, true);
        $sDoAferSave = $this->get('do_after_save', null, true);

        $aContentData = [
            'do_after_save' => $sDoAferSave,
            'manager' => $sModule,
            'module' => $sManager
        ];
        $aView =
            [
                'top_nav' => $this->parse('Generic/Crud/editor_edit_top_nav.twig' , []),
                'content' =>  $this->parse('Generic/Crud/editor_new.twig' , $aContentData)
            ];
        return $aView;
    }
}