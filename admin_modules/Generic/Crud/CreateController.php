<?php

namespace AdminModules\Generic\Crud;

use Core\Cfg;
use Core\DeferredAction;
use Core\LogActivity;
use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Core\User;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\Field;
use Crud\FormManager;
use Crud\Generic\Field\GenericDelete;
use Crud\IConfigurableCrud;
use Crud\IEventField;
use Crud\IInlineEditor;
use InvalidArgumentException;
use LogicException;
use Model\Setting\CrudManager\CrudViewQuery;

class CreateController extends MainController {
    use LoadManagerTrait;

    private FormManager $oCrudManager;
    private $sCrudNamespace;

    function __construct($aGet, $aPost) {
        parent::__construct($aGet, $aPost);
        $this->loadManagerAndNamespace();
    }

    function doCreateCrud() {

        $iCrudId = $this->get('tab');
        $sR = $this->get('r');
        if (!$this->oCrudManager instanceof FormManager) {
            throw new InvalidArgumentException(Translate::fromCode("Probeerde een CRUD te instantieren maar klassenamen klopt niet."));
        }

        if (!$this->oCrudManager instanceof IConfigurableCrud) {
            throw new LogicException("Crud must implement IConfigurableCrud for this functionality in " . __METHOD__ . " " . get_class($this->oCrudManager));
        }

        $aData = $this->post('data');

        if (empty($aData['title'])) {
            $aData['title'] = Translate::fromCode('Nieuw tabblad');
        }

        $aArguments = [
            'default_order_by'  => $aData['default_order_by'],
            'default_order_dir' => $aData['default_order_dir'],
            'show_quantity'     => $aData['show_quantity'],
            'tab_color_logic'   => $aData['tab_color_logic'],
        ];

        $iCrudId = CrudViewManager::createOrEditView($this->oCrudManager, $aData['title'], explode(',', $aData['fields']), $iCrudId, $aArguments);

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen."));

        if (User::isSignedIn()) {
            LogActivity::register('Crud', 'Creator', Translate::fromCode("Nieuw overzicht aangemaakt") . " " . $aData['title'] . " " . Translate::fromCode("in de") . " " . $this->get('module') . " " . Translate::fromCode("module") . ".");
        }
        if ($sR) {
            $sReturnUrl = DeferredAction::get($sR);
        } else {
            $sRedirectToOverviewUrl = $this->oCrudManager->getOverviewUrl();
            // Soms komen er uit de crudmanager al get variabelen mee.
            $sConcatSign = (strpos($sRedirectToOverviewUrl, '?')) ? '&' : '?';
            $sReturnUrl = $sRedirectToOverviewUrl . $sConcatSign . "tab=$iCrudId";
        }
        $this->redirect($sReturnUrl);
    }

    /**
     * @param $sFieldName
     * @return Field
     */
    private function getField($sFieldName) {
        if (Cfg::get('CUSTOM_NAMESPACE') && class_exists($this->sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName")) {
            $sFieldClassName = $this->sCrudNamespace . "\\Field\\" . Cfg::get('CUSTOM_NAMESPACE') . "\\$sFieldName";
        } else {
            $sFieldClassName = $this->sCrudNamespace . "\\Field\\$sFieldName";
        }

        $oField = new $sFieldClassName;

        if (!$oField instanceof Field) {
            $sErrorMessage = Translate::fromCode("Propbeerde de CRUD velden van schijf te laden om de bijbehorende titelatuur op te halen maar bij laden krijg ik een onjuist object terug.");
            throw new InvalidArgumentException($sErrorMessage);
        }
        return $oField;
    }

    function run() {

        $iTabId = $this->get('tab');
        $sLayoutKey = $this->get('layout_key');

        if (!$this->oCrudManager instanceof FormManager) {
            throw new InvalidArgumentException(Translate::fromCode("Probeerde een CRUD te instantieren maar klassenamen klopt niet."));
        }
        if (!$this->oCrudManager instanceof IConfigurableCrud) {
            throw new LogicException(Translate::fromCode("Crud must implement IConfigurableCrud for this functionality in") . " " . __METHOD__ . " " . get_class($this->oCrudManager));
        }

        $sOverviewUrl = $this->oCrudManager->getOverviewUrl();

        if($sLayoutKey)
        {
            $this->oCrudManager->setCrudLayoutKey($sLayoutKey);
        }

        $oCrudView = CrudViewManager::init($this->oCrudManager);
        $aEnabledFieldNames = $oCrudView->getFields();//  CrudViewManager::getFields($oCrudView);

        $aAllAvailableFields = $this->oCrudManager->getAllAvailableFields();
        $aAllAvailableFieldsFilled = [];
        $aFields = [];
        foreach ($aAllAvailableFields as $sFieldName) {
            $oField = $this->getField($sFieldName);

            if ($oField instanceof IInlineEditor) {
                continue;
            }

            if ($oField->isOverviewField()) {
                $bIsEventField = ($oField instanceof IEventField);
                $aField = [
                    'is_delete_field' => $oField instanceof GenericDelete,
                    'name'            => $sFieldName,
                    'title'           => $oField->getFieldTitle(),
                    'event_field'     => $bIsEventField,
                ];

                $aAllAvailableFieldsFilled[] = $aField;

                if (!in_array($sFieldName, $aEnabledFieldNames)) {
                    $aFields[] = $aField;
                }
            }
        }
        $aEnabledFields = [];
        foreach ($aEnabledFieldNames as $sEnabledField) {
            $oField = $this->getField($sEnabledField);

            $bIsEventField = ($oField instanceof IEventField);

            $aEnabledFields[] = [
                'is_delete_field' => $oField instanceof GenericDelete,
                'name'            => $sEnabledField,
                'title'           => $oField->getFieldTitle(),
                'event_field'     => $bIsEventField,
            ];
        }

        Utils::usort_strcmp($aFields, 'title');
        Utils::usort_strcmp($aAllAvailableFieldsFilled, 'title');

        $sModule = $this->get('module');
        $sManager = $this->get('manager');

        $oCrudView->getDefaultOrderDir();

        if (!empty($aAllAvailableFieldsFilled)) {
            foreach ($aAllAvailableFieldsFilled as &$aAllAvailableField) {
                if ($aAllAvailableField['name'] == $oCrudView->getDefaultOrderBy()) {
                    $aAllAvailableField['selected'] = 'selected="selected"';
                }
            }
        }

        $aViewData = [
            'fields'               => $aFields,
            'enabled_fields'       => $aEnabledFields,
            'all_available_fields' => $aAllAvailableFieldsFilled,
            'tab_id'               => $iTabId,
            'module'               => $sModule,
            'manager'              => $sManager,
            'crud_view'            => $oCrudView,
        ];

        if (!$this->oCrudManager instanceof FormManager) {
            throw new InvalidArgumentException(Translate::fromCode("Probeerde een CRUD te instantieren maar klassenamen klopt niet."));
        }
        $aTopNavData = [
            'crud_views'   => CrudViewManager::getViews($this->oCrudManager),
            'overview_url' => $sOverviewUrl,
            'module'       => $sModule,
            'manager'      => $sManager,
            'tab_id'       => $iTabId,
        ];

        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavData);
        if ($this->get('hide_topnav')) {
            $sTopNav = '';
        }
        $aView = [
            'top_nav' => $sTopNav,
            'content' => $this->parse('Generic/Crud/create.twig', $aViewData),
            'title'   => Translate::fromCode('Tabblad instellingen'),
        ];
        return $aView;
    }
}
