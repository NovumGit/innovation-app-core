<?php
namespace Modules\Generic\Crud;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Crud\FormManager;
use Crud\IConfigurableCrud;
use Model\Setting\CrudManager\CrudViewQuery;
use Propel\Runtime\Exception\LogicException;


class DeleteController extends MainController
{

    function run()
    {
        $iTabId = $this->get('tab');
        $oCrudView = CrudViewQuery::create()->findOneById($iTabId);
        $oCrudConfig = $oCrudView->getCrudConfig();


        $sManagerClassName = $oCrudConfig->getManagerName();
        $oCrudManager = new $sManagerClassName();
        $oCrudView->delete();

        if(!$oCrudManager instanceof FormManager)
        {
            throw new LogicException("Expected an instance of Manager at ".__METHOD__." got ".get_class($oCrudManager));
        }
        if(!$oCrudManager instanceof IConfigurableCrud)
        {
            throw new LogicException("Crud must implement IConfigurableCrud for this functionality in ".__METHOD__." ".get_class($oCrudManager));
        }

        StatusMessage::success(Translate::fromCode("Het overzicht is verwijderd"));
        $this->redirect($oCrudManager->getOverviewUrl());
    }
}
