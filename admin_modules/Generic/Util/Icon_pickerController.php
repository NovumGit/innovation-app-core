<?php

namespace AdminModules\Generic;

use Core\MainController;
use Core\UserRegistry;
use Core\Utils;
use LogicException;

class Icon_pickerController extends MainController
{
    function run()
    {
        echo $this->parse('x', []);
    }
}
