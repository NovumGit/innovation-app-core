<?php
namespace AdminModules\Supplier;

use Core\Cfg;
use Core\DeferredAction;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\StatusModal;
use Core\Translate;
use Core\Utils;
use Crud\CrudViewManager;
use Crud\Supplier\CrudSupplierManager;
use Helper\FilterHelper;
use LogicException;
use Model\Logging\Except_log;
use Model\CustomerQuery;
use model\Setting\CrudManager\CrudView;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierProperty;
use Model\Supplier\SupplierPropertyQuery;
use Model\Supplier\SupplierQuery;
use Propel\Runtime\Exception\PropelException;

class OverviewController extends MainController{

    function doConfirmDeleteRecursive()
    {
        $iSupplierId = $this->get('supplier_id', null, true, 'numeric');
        $aMessage = [
            "U staat op het punt om deze leverancier en alle bijbehorende voertuigen met afbeeldingen ",
            "van het systeem te verwijderen. ",
            "Als de leverancier nog in het systeem van Hexon voorkomt dan kan het zijn dat deze actie ",
            "vanzelf ongedaan wordt gemaakt."
        ];

        $sMessage = Translate::fromCode(join('', $aMessage));
        $sTitle = Translate::fromCode("Zeker weten?");

        $sOkUrl = "/supplier/overview?_do=DeleteRecursive&supplier_id={$iSupplierId}";
        $sBeforeDeleteRecursiveUrl = DeferredAction::get('before_delete_recursive_url');
        $sNOUrl = $sBeforeDeleteRecursiveUrl;

        DeferredAction::register('after_delete_recursive_url', $sBeforeDeleteRecursiveUrl);

        $sYes = Translate::fromCode("Ja");
        $sCancel = Translate::fromCode("Annuleren");
        $aButtons  = [
            new StatusMessageButton($sYes, $sOkUrl, 'Ja verwijder alles van deze leverancier', 'warning'),
            new StatusMessageButton($sCancel, $sNOUrl, 'Nee, dat was niet de bedoeling', 'info'),
        ];
        StatusModal::warning($sMessage, $sTitle, $aButtons);
    }

    /**
     * Deletes the supplier and all supplier related data.
     */

    function doDeleteRecursive()
    {
        $iSupplierId = $this->get('supplier_id', null, true, 'numeric');

        $oProductQuery = CustomerQuery::create();
        $aProducts = $oProductQuery->findBySupplierId($iSupplierId);

        if(!$aProducts->isEmpty())
        {
            foreach ($aProducts as $oProduct)
            {
                $oProduct->deleteWithImages();
            }
        }

        $sDoAfterUrl = DeferredAction::get('after_delete_recursive_url');
        try
        {
            $this->doDelete($sDoAfterUrl);
        }
        catch (PropelException $oException)
        {
            Except_log::register($oException, false);
            $this->redirect($sDoAfterUrl);
        }
    }

    /**
     * @param null $sDoAfterUrl
     * @throws PropelException
     */
    function doDelete($sDoAfterUrl = null)
    {

        $iSupplierId = $this->get('supplier_id', null, true, 'numeric');
        $oSupplier = SupplierQuery::create()->findOneById($iSupplierId);

        if($oSupplier instanceof Supplier)
        {
            StatusMessage::success("Leverancier verwijderd.");
            $oSupplier->delete();
        }
        else
        {
            StatusMessage::warning("Leverancier niet gevonden.");
        }
        if($sDoAfterUrl)
        {
            $this->redirect($sDoAfterUrl);
        }
        $this->redirect('/supplier/overview');
    }

    /**
     * @throws PropelException
     */
    function doSetSupplierProperty()
    {
        $sPropertyName = $this->post('property_name');
        $iSupplierId = $this->post('supplier_id');
        $sPropertyValue = $this->post('property_value');

        $oSupplierPropertyQuery = SupplierPropertyQuery::create();
        $oSupplierPropertyQuery->filterByPropertyKey($sPropertyName);
        $oSupplierPropertyQuery->filterBySupplierId($iSupplierId);
        $oSupplierProperty = $oSupplierPropertyQuery->findOne();

        if(!$oSupplierProperty instanceof SupplierProperty)
        {
            $oSupplierProperty = new SupplierProperty();
            $oSupplierProperty->setPropertyKey($sPropertyName);
            $oSupplierProperty->setSupplierId($iSupplierId);
        }
        $oSupplierProperty->setPropertyValue($sPropertyValue);
        $oSupplierProperty->save();

        Utils::jsonOk();
    }

    function run(){

        $this->addJsFile('/admin_modules/Supplier/overview.js');

        $iTabId = $this->get('tab');
        $aGet = $this->get();
        $sSortField = isset($aGet['sort']) ? $aGet['sort'] : 'number';
        $sSortDirection = isset($aGet['dir']) ? $aGet['dir'] : 'desc';
        $iCurrentPage = $this->get('p');
        $iCurrentPage = is_numeric($iCurrentPage) ? $iCurrentPage : 1;

        $oSupplierQuery = SupplierQuery::create();
        $aCustomSortFiedls = Cfg::get('CUSTOM_SUPPLIER_SORT_FIELDS');

        if(!empty($aCustomSortFiedls) && in_array($sSortField, $aCustomSortFiedls))
        {
            $aCustomSupplierSearchFunction =  Cfg::get('CUSTOM_SUPPLIER_SORT_FUNCTION');
            if(is_callable($aCustomSupplierSearchFunction))
            {
                $oSupplierQuery = $aCustomSupplierSearchFunction($oSupplierQuery, $sSortField, $sSortDirection);
            }
        }
        else
        {
            $oSupplierQuery->orderBy($sSortField, $sSortDirection);
        }


        $oCrudSupplierManager = new CrudSupplierManager();

        // Dit is de top bar waar de verschillende crud weergaven in zijn vastgelegd.
        $aCrudViews = CrudViewManager::getViews($oCrudSupplierManager);

        if(!$iTabId)
        {
            $oCrudview = current($aCrudViews);
            if(!$oCrudview instanceof CrudView)
            {
                throw new LogicException("Was looking for a CrudView but got ".get_class($oCrudview));
            }
            $iTabId = $oCrudview->getId();
        }
        else
        {
            $oCrudview = $aCrudViews[$iTabId];
        }


        foreach($aCrudViews as $o)
        {
            if($o->getShowQuantity())
            {
                $oCountSupplierQuery = SupplierQuery::create();
                $oCountSupplierQuery= FilterHelper::applyFilters($oCountSupplierQuery, $o->getId());
                $o->_sz = $oCountSupplierQuery->count();
            }
        }


        $oSupplierQuery = FilterHelper::applyFilters($oSupplierQuery, $iTabId);

        $aFilters = isset($aGet['filter']) ? $aGet['filter'] : FilterHelper::getDefaultVisibleFilterConfig($iTabId);

        $aCustomSupplierSearchFields = Cfg::get('CUSTOM_SUPPLIER_SEARCH_FIELDS');
        $aCustomSupplierSearchFunction =  Cfg::get('CUSTOM_SUPPLIER_SEARCH_FUNCTION');

        $oSupplierQuery = FilterHelper::generateVisibleFilters($oSupplierQuery, $aFilters, $aCustomSupplierSearchFunction, $aCustomSupplierSearchFields);

        $oCrudSupplierManager->setOverviewData($oSupplierQuery, $iCurrentPage);
        $aFields = CrudViewManager::getFields($oCrudview);

        $aViewData['filter_html'] = FilterHelper::renderHtml($iTabId);
        $aViewData['overview_table'] = $oCrudSupplierManager->getOverviewHtml($aFields);
     //   $aViewData['title'] = Translate::fromCode('Klanten');

        $aTopNavVars = [
            'crud_views' => $aCrudViews,
            'tab_id' => $iTabId,
            'module' => 'Supplier',
            'manager' => 'SupplierManager',
            'overview_url' => $oCrudSupplierManager->getOverviewUrl(),
            'new_url' => $oCrudSupplierManager->getCreateNewUrl(),
            'new_title' => $oCrudSupplierManager->getNewFormTitle()
        ];
        $sTopNav = $this->parse('top_nav_overview.twig', $aTopNavVars);
        $sOverview = $this->parse('generic_overview.twig', $aViewData);
        $aView =
            [
                'title' => Translate::fromCode('Leveranciers'),
                'top_nav' => $sTopNav,
                'content' => $sOverview
            ];
        return $aView;
    }
}
