<?php
namespace AdminModules\Supplier;

use AdminModules\GenericEditController;
use Core\Cfg;
use Core\Translate;
use Crud\FormManager;
use Crud\Supplier\CrudSupplierManager;

class EditController extends GenericEditController
{
    function getAfterSaveUrl($iSupplierId)
    {
        $sAfterSaveUrl = Cfg::get('AFTER_SUPPLIER_SAVE_URL');
        if($sAfterSaveUrl)
        {
            $sDelim = strpos($sAfterSaveUrl, '?') ? '&' : '?';
            $this->redirect($sAfterSaveUrl . $sDelim . 'supplier_id=' . $iSupplierId);
            exit();
        }
        return null;
    }

    function getTopNav()
    {
        $aTopNavVars = [
            'module' => 'Supplier',
            'manager' => 'SupplierManager',
            'crud_return_url' => $this->getRequestUri(),
            'name' => $this->getFormLayoutKey(),
            'title' => $this->getPageTitle()
        ];

        return $this->parse('Supplier/top_nav_edit.twig', $aTopNavVars);
    }
    function getPageTitle()
    {
        return Translate::fromCode('Leveranciers');
    }

    function getCrudManager(): FormManager
    {
        return new CrudSupplierManager();
    }
}